// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentTitleDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CreateCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.PkpZuleitungDokumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserRightsDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.enums.VerfahrensType;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.AbstimmungId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;

import java.util.List;
import java.util.UUID;

public interface CompoundDocumentRestPort {

	CompoundDocumentDTO createCompoundDocument(CreateCompoundDocumentDTO createCompoundDocumentDTO,
		List<ServiceState> status);

	List<CompoundDocument> getAllCompoundDocumentsByAuthorAndRegulatoryProposal(UUID regulatoryProposalId);

	List<CompoundDocument> getAllCompoundDocumentsRestrictedByAccessRights();

	List<CompoundDocumentTitleDTO> getAllTitles(List<ServiceState> status);

	CompoundDocument getCompoundDocumentById(CompoundDocumentId compoundDocumentId, List<ServiceState> status);

	CompoundDocument getCompoundDocumentDomainEntity(CompoundDocumentId compoundDocumentId);

	CompoundDocumentSummaryDTO getDocumentsList(CompoundDocumentId compoundDocumentId, List<ServiceState> status);

	CompoundDocumentDTO addDocument(CompoundDocumentId compoundDocumentId, Document document,
		List<ServiceState> status);

	CompoundDocumentDTO addDocumentForUser(CompoundDocumentId compoundDocumentId, Document document,
		User userEntity, List<ServiceState> status);

	void update(CompoundDocumentId compoundDocumentId, String title, VerfahrensType verfahrensType,
		DocumentState compoundDocumentState,
		List<ServiceState> status);

	CompoundDocumentSummaryDTO copyCompoundDocument(CompoundDocumentId compoundDocumentId, String title,
		List<DocumentId> documentIds, List<ServiceState> status);

	List<CompoundDocument> getDokumentenmappenFuerEinRegelungsvorhabenMitStatus(
		User userEntity, RegelungsVorhabenId regelungsvorhabenId, DocumentState compoundDocumentState);

	/**
	 * Called by Plattform
	 */
	List<CompoundDocument> getAllDocumentsFromCompoundDocuments(String gid, List<String> compoundDocumentId,
		List<ServiceState> status);

	/**
	 * Called by Plattform
	 */
	List<CompoundDocumentSummaryDTO> getCompoundDocumentsForRegelungsvorhaben(
		String gid, UUID fromString, List<ServiceState> status);

	/**
	 * Called by Plattform
	 */
	ServiceState changeCompoundDocumentStateByPlatform(String gid, CompoundDocumentId compoundDocumentId,
		DocumentState state);

	/**
	 * Changes rights for Users
	 *
	 * @return state of operation
	 */
	ServiceState changeAccessRightsForUsersOnCompoundDocuments(CompoundDocumentId compoundDocumentId,
		List<UserRightsDTO> userRightsDTO);

	/**
	 * Changes write rights for Users
	 *
	 * @param compoundDocumentId Id of compound document
	 * @param userRightsDTO
	 * @param status
	 * @return State of operation
	 */
	CompoundDocumentSummaryDTO changeWritePermissionsOnCompoundDocuments(CompoundDocumentId compoundDocumentId, UserRightsDTO userRightsDTO,
		List<ServiceState> status);

	/**
	 * Called by Plattform
	 */
	List<CompoundDocumentSummaryDTO> getCompoundDocumentsForAbstimmung(String gid,
		RegelungsVorhabenId regelungsVorhabenId,
		CompoundDocumentId compoundDocumentId, AbstimmungId abstimmungId, List<ServiceState> status);

	/**
	 * Called by Plattform
	 */
	List<PkpZuleitungDokumentDTO> getCompoundDocumentsForKabinett(String gid, RegelungsVorhabenId regelungsVorhabenId,
		List<ServiceState> status);

}
