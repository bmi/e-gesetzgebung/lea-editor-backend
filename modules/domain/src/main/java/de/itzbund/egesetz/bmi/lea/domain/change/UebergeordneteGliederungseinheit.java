// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.change;

import de.itzbund.egesetz.bmi.lea.core.json.TraversableJSONDocument;
import de.itzbund.egesetz.bmi.lea.domain.change.linguistics.Artikel;
import lombok.Getter;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_EID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_HREF;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_REF;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getStringAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getTextWrapper;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewJSONObjectWithText;
import static de.itzbund.egesetz.bmi.lea.domain.change.AenderungsbefehlUtils.getAenderungsbefehlSkeleton;
import static de.itzbund.egesetz.bmi.lea.domain.change.AenderungsbefehlUtils.getQuotedStructure;
import static de.itzbund.egesetz.bmi.lea.domain.change.linguistics.Definitheit.DEFINIT;
import static de.itzbund.egesetz.bmi.lea.domain.change.linguistics.Numerus.SINGULAR;

@Getter
public class UebergeordneteGliederungseinheit implements AenderungsfaehigeGliederungseinheit {

    private static final Set<String> NEUTER_LEVELS = Stream.of(
            "buch", "kapitel", "unterkapitel")
        .collect(Collectors.toCollection(HashSet::new));

    private Aenderungsart aenderungsart;

    private JSONObject introObject;

    private final List<JSONObject> aenderungsbefehle;

    private String aenderungsstelle;

    private final TraversableJSONDocument baseDocument;

    private final TraversableJSONDocument versionDocument;

    public UebergeordneteGliederungseinheit(TraversableJSONDocument baseDocument, TraversableJSONDocument versionDocument,
        JSONObject baseLevel, JSONObject versionLevel, DiffMatchPatch.Operation op, String predecessorEId) {
        this.baseDocument = baseDocument;
        this.versionDocument = versionDocument;
        this.aenderungsbefehle = new ArrayList<>();

        switch (op) {
            case DELETE:
                streichen(baseLevel);
                break;
            case INSERT:
                einfuegen(versionLevel, predecessorEId);
                break;
            default:
                this.aenderungsart = compareLevels();
        }
    }


    private void streichen(JSONObject baseLevel) {
        aenderungsart = Aenderungsart.STREICHEN;
        aenderungsstelle = AenderungsbefehlUtils.getAenderungsstelleUebergeordneteGliederungseinheit(baseDocument, baseLevel);

        JSONObject aenderungsbefehl = getAenderungsbefehlSkeleton(aenderungsart);
        JSONObject refObject = makeNewJSONObjectWithText(ELEM_REF, aenderungsstelle, true, false);
        addAttribute(refObject, Pair.of(ATTR_HREF, ""));

        addChildren(aenderungsbefehl,
            refObject,
            getTextWrapper(" wird gestrichen.")
        );
        aenderungsbefehle.add(aenderungsbefehl);
    }


    private void einfuegen(JSONObject versionLevel, String predecessorEId) {
        aenderungsart = Aenderungsart.EINFUEGEN;
        aenderungsstelle = AenderungsbefehlUtils.getAenderungsstelle(predecessorEId);

        String insertedLevel = AenderungsbefehlUtils.getAenderungsstelle(getStringAttribute(versionLevel, ATTR_EID));
        Gliederungseinheit gliederungseinheit = Gliederungseinheit.fromReference(insertedLevel);
        Artikel artikel = gliederungseinheit == null
            ? Artikel.DER
            : Artikel.getFor(DEFINIT, gliederungseinheit.getGenus(), SINGULAR);
        assert artikel != null;
        String artikelText = artikel.getValue();

        JSONObject aenderungsbefehl = getAenderungsbefehlSkeleton(aenderungsart);
        JSONObject refObject = makeNewJSONObjectWithText(ELEM_REF, aenderungsstelle, true, false);
        addAttribute(refObject, Pair.of(ATTR_HREF, ""));

        addChildren(aenderungsbefehl,
            getTextWrapper("Nach "),
            refObject,
            getTextWrapper(String.format(" wird %s folgende %s eingefügt:", artikelText, insertedLevel)),
            getQuotedStructure(versionLevel)
        );
        aenderungsbefehle.add(aenderungsbefehl);
    }


    private Aenderungsart compareLevels() {
        return Aenderungsart.KEINE;
    }

}
