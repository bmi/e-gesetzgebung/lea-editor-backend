// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.change;

import lombok.Getter;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.json.simple.JSONObject;

import java.util.List;

@Getter
public class DokumentUeberschrift implements AenderungsfaehigeGliederungseinheit {

	private final Aenderungsart aenderungsart;

	public DokumentUeberschrift(JSONObject baseLongTitle, JSONObject versionLongTitle, DiffMatchPatch.Operation op) {
		//not yet implemented
		this.aenderungsart = Aenderungsart.KEINE;
	}

	@Override
	public String getAenderungsstelle() {
		return "";
	}

	@Override
	public List<JSONObject> getAenderungsbefehle() {
		//not yet implemented
		return null;
	}

	@Override
	public JSONObject getIntroObject() {
		//not yet implemented
		return null;
	}

}
