// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;

@Data
@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString(callSuper = true)
public class CopyCompoundDocumentDTO {

    @NotNull
    @JsonProperty("title")
    @Schema(name = "title", example = "Title for compound document", required = true)
    protected String title;

    // Not used as transfer object, but programmatically

    @JsonIgnore
    protected CompoundDocumentId compoundDocumentId;

    @JsonIgnore
    @JsonProperty("version")
    @Schema(name = "version", example = "1")
    protected String version;

    @JsonIgnore
    private CompoundDocumentId inheritFromId;

    @JsonIgnore
    private List<Document> documents;

    @JsonIgnore
    @Schema(name = "state", example = "BEREIT_FUER_KABINETTVERFAHREN")
    protected DocumentState state = DocumentState.DRAFT;

    @JsonIgnore
    @Schema(name = "regelungsVorhabenId", example = "00000000-0000-0000-0000-000000000005")
    private RegelungsVorhabenId regelungsVorhabenId;

    @JsonIgnore
    protected CompoundDocumentTypeVariant type = CompoundDocumentTypeVariant.STAMMGESETZ;

    @JsonIgnore
    protected Instant createdAt;

    @JsonIgnore
    protected Instant updatedAt;

}