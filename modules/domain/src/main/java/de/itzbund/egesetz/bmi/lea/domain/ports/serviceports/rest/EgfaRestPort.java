// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest;

import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.ImportEgfaDataDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeParentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaStatusDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public interface EgfaRestPort {

	void egfaDatenSpeicherung(String gid, UUID regelungsvorhabenId,
		EgfaDatenuebernahmeParentDTO egfaDatenuebernahmeParentDTO, List<ServiceState> status);

	HashMap<String, HttpStatus> egfaUebernahmeInDokumentenmappe(UUID compoundDocumentId,
		ImportEgfaDataDTO importEgfaDataDTO, List<ServiceState> status);

	void egfaDatenuebernahme(String gid, UUID regelungsvorhabenId, EgfaDatenuebernahmeDTO egfaDatenuebernahmeDTO,
		List<ServiceState> status);

	List<EgfaStatusDTO> getEgfaStatusByCompoundDocumentId(List<ServiceState> status, UUID regulatoryProposalId);
}
