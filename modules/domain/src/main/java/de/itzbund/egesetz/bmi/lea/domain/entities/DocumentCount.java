// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.entities;

import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigInteger;
import java.util.UUID;

@Builder
@Data
@AllArgsConstructor
public class DocumentCount {

    final String documentId;

    final String type;

    final BigInteger count;

    public DocumentId getDocumentId() {
        return new DocumentId(UUID.fromString(documentId));
    }

    public DocumentType getType() {
        return DocumentType.valueOf(type);
    }

    public int getCount() {
        return count.intValue();
    }

}
