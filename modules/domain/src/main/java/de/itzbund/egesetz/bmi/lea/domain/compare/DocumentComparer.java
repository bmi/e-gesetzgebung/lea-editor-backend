// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.compare;

import lombok.NonNull;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Map;

public interface DocumentComparer {

    /**
     * Compares two documents and amends them by change markers.
     *
     * @param baseDocumentObject    the base document or first version
     * @param baseMetadata some describing data about the base document, implementation-dependant
     * @param versionDocumentObject the version document, i.e. another version
     * @param versionMetadata some describing data about the version document, implementation-dependant
     */
    void compare(@NonNull JSONObject baseDocumentObject, Map<String, Object> baseMetadata,
        @NonNull JSONObject versionDocumentObject, Map<String, Object> versionMetadata);

    /**
     * Returns the base document amended by change annotations.
     *
     * @return a valid document for Slate.js
     */
    JSONArray getBaseDocument();

    /**
     * Returns the version document amended by change annotations.
     *
     * @return a valid document for Slate.js
     */
    JSONArray getVersionDocument();

}
