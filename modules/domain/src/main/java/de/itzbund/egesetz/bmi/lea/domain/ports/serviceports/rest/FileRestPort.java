// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest;

import de.itzbund.egesetz.bmi.lea.domain.dtos.media.MediaSummaryDTO;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public interface FileRestPort {

    UUID save(String fileName, MultipartFile data);

    MediaSummaryDTO loadFromDB(UUID mediaId);

    void deleteFromDB(UUID mediaId);
}
