// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class CompoundDocumentTitleDTO extends CompoundDocumentAllDTO {

    @NotNull
    @JsonProperty("id")
    @Valid
    @Schema(name = "id", required = true)
    private UUID id;

    @Setter
    @JsonProperty("anzahlAnlagen")
    @Schema(name = "anzahlAnlagen", example = "2", required = true)
    private int numberAttachments;
}
