// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.PermissionDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;
import java.time.Instant;

@Data
@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public abstract class CompoundDocumentBasicsDTO extends CompoundDocumentAllDTO {

	@NotNull
	@JsonProperty("version")
	@Schema(name = "version", example = "1", required = true)
	protected String version;

	@Schema(name = "state", example = "BEREIT_FUER_KABINETTVERFAHREN", required = true)
	@Builder.Default
	protected DocumentState state = DocumentState.DRAFT;

	@Schema(name = "createdAt", example = "19.02.2022", required = true)
	protected Instant createdAt;

	@Schema(name = "updatedAt", example = "19.02.2022", required = true)
	protected Instant updatedAt;

	@Schema(name = "documentPermissions")
	protected PermissionDTO documentPermissions;

	@Schema(name = "commentPermissions")
	protected PermissionDTO commentPermissions;

}
