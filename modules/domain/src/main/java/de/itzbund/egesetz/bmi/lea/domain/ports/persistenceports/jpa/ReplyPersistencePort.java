// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa;

import java.util.List;
import java.util.Optional;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.ReplyId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;

public interface ReplyPersistencePort {

	Reply save(Reply replyEntity);

	List<Reply> findByCommentIdAndDeleted(CommentId commentId, boolean b);

	Optional<Reply> findByReplyIdAndDeletedFalse(ReplyId replyId);

	Optional<Reply> findByParentIdAndDeletedFalse(String parentId);

	Optional<Reply> findByReplyId(ReplyId parentId);

	boolean existsReplyByReplyId(ReplyId replyId);

	boolean existsRepliesCreatedOrUpdatedByUser(UserId userId);
}
