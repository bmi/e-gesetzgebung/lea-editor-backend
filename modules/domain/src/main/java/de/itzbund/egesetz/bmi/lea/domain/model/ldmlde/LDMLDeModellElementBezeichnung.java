// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model.ldmlde;

import lombok.AllArgsConstructor;
import lombok.Getter;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ARTICLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BLOCKCONTAINER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DATE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DOC;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_EVENTREF;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FORMULA;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_HCONTAINER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_INLINE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_MOD;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SHORTTITLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TBLOCK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TIMEINTERVAL;

@Getter
@AllArgsConstructor
public enum LDMLDeModellElementBezeichnung {

    AENDERUNGSBEFEHL("aenderungsbefehl", ELEM_MOD),
    BEGRUENDUNG("begruendung", ELEM_DOC),
    BEGRUENDUNGSTEIL("begruendungsteil", ELEM_HCONTAINER),
    BEGRUENDUNGSTEILABSCHNITT("begruendungsteilAbschnitt", ELEM_HCONTAINER),
    DATUM("datum", ELEM_DATE),
    DENKSCHRIFTABSCHNITT("denkschriftAbschnitt", ELEM_HCONTAINER),
    DENKSCHRIFTTEIL("denkschriftTeil", ELEM_HCONTAINER),
    EINGANGSFORMEL("eingangsformel", ELEM_FORMULA),
    EINZELVORSCHRIFT("einzelvorschrift", ELEM_ARTICLE),
    EREIGNISREFERENZ("ereignisReferenz", ELEM_EVENTREF),
    GELTUNGSZEITINTERVALL("geltungszeitIntervall", ELEM_TIMEINTERVAL),
    INHALTSABSCHNITT("inhaltsabschnitt", ELEM_TBLOCK),
    INLINE("inline", ELEM_INLINE),
    KURZTITEL("kurztitel", ELEM_SHORTTITLE),
    SCHLUSSFORMEL("schlussformel", ELEM_FORMULA),
    SPRACHFASSUNGPRAEAMBELINHALTSUEBERSICHT("sprachfassungPraeambelInhaltsuebersicht", ELEM_HCONTAINER),
    VEREINBARUNGVERTRAHAUPTTEIL("vereinbarungVertRAHauptteil", ELEM_BODY),
    VEREINBARUNGVERTRASPRACHFASSUNG("vereinbarungVertRASprachfassung", ELEM_HCONTAINER),
    VERZEICHNISCONTAINER("verzeichniscontainer", ELEM_BLOCKCONTAINER),
    VORBLATTABSCHNITT("vorblattabschnitt", ELEM_HCONTAINER);

    private final String literal;
    private final String aknName;

}
