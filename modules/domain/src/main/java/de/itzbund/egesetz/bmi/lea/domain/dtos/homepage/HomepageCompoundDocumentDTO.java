// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.homepage;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.EntityType;
import de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
@SuppressWarnings("unused")
public class HomepageCompoundDocumentDTO {

	private UUID id;
	@EqualsAndHashCode.Exclude
	private UUID regulatoryProposalId;
	@EqualsAndHashCode.Exclude
	private VorhabenStatusType regulatoryProposalState;
	@EqualsAndHashCode.Exclude
	private String title;
	@EqualsAndHashCode.Exclude
	private CompoundDocumentTypeVariant type;
	@EqualsAndHashCode.Exclude
	private DocumentState state;
	@EqualsAndHashCode.Exclude
	private Boolean isNewest;
	@EqualsAndHashCode.Exclude
	private Boolean isPinned;

	@EqualsAndHashCode.Exclude
	private String version;

	// the name 'children' is required by frontend table component
	@EqualsAndHashCode.Exclude
	private List<HomepageSingleDocumentDTO> children;

	@EqualsAndHashCode.Exclude
	private PermissionDTO documentPermissions;

	@EqualsAndHashCode.Exclude
	private PermissionDTO commentPermissions;


	@JsonProperty
	public EntityType getEntityType() {
		return EntityType.COMPOUND_DOCUMENT;
	}
}
