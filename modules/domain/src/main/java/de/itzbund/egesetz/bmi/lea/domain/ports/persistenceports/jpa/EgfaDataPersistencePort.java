// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa;

import java.util.List;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.enums.egfa.datenuebernahme.EgfaModuleType;
import de.itzbund.egesetz.bmi.lea.domain.model.EgfaData;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;

@SuppressWarnings("unused")
public interface EgfaDataPersistencePort {

	EgfaData save(EgfaData egfaData, List<ServiceState> status);

	EgfaData findNewestByRegelungsVorhabenIdAndModuleName(RegelungsVorhabenId regelungsVorhabenId,
		EgfaModuleType moduleName);

	boolean existsEgfaDataTransmittedByUser(UserId userId);
}
