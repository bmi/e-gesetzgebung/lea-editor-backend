// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa;

import java.util.Optional;
import de.itzbund.egesetz.bmi.lea.domain.enums.egfa.datenuebernahme.EgfaModuleType;
import de.itzbund.egesetz.bmi.lea.domain.model.EgfaMetadata;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;

@SuppressWarnings("unused")
public interface EgfaMetadataPersistencePort {

	EgfaMetadata save(EgfaMetadata egfaMetadata);

	Optional<EgfaMetadata> findNewestByCompoundDocumentIdAndModuleName(CompoundDocumentId compoundDocumentId,
		EgfaModuleType moduleName);

	boolean existsEgfaMetadataCreatedByUser(UserId userId);
}
