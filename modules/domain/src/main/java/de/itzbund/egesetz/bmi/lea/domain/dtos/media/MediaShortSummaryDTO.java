// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.media;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.springframework.http.MediaType;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
public class MediaShortSummaryDTO {

    @JsonProperty("id")
    @Schema(name = "id", required = true)
    private UUID id;

    @NotNull
    @JsonProperty("mediaType")
    @Schema(name = "mediaType", example = "IMAGE_JPEG", required = true)
    private MediaType mediaType;

    @NotNull
    @JsonProperty("length")
    @Schema(name = "length", example = "1024", required = true)
    private long length;

    @NotNull
    @JsonProperty("name")
    @Schema(name = "name", example = "myPicture.jpg", required = true)
    private String name;

}
