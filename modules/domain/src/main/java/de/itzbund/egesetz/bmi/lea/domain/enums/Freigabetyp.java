// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums;

import lombok.Getter;

/**
 * Gets or Sets freigabetyp
 */
@Getter
@SuppressWarnings("unused")
public enum Freigabetyp {

    LESERECHTE("LESERECHTE"),
    SCHREIBRECHTE("SCHREIBRECHTE");

    private final String value;


    Freigabetyp(String value) {
        this.value = value;
    }

    public static Freigabetyp fromValue(String value) {
        for (Freigabetyp b : Freigabetyp.values()) {
            if (b.value.equals(value)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

}