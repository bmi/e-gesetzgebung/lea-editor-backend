// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest;

import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.DMBestandsrechtLinkDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtListDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtShortDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;

import java.util.List;
import java.util.UUID;

public interface BestandsrechtRestPort {

    void bestandsrechtDatenSpeicherung(String gid, UUID regelungsvorhabenId,
        BestandsrechtDTO bestandsrechtDTO, List<ServiceState> status);

    List<BestandsrechtListDTO> getBestandsrechtForCompoundDocumentId(UUID compoundDocumentId, List<ServiceState> status);

    List<DMBestandsrechtLinkDTO> getBestandsrechtLinks(UUID compoundDocumentId, List<ServiceState> status);

    List<BestandsrechtListDTO> getBestandsrechteFromRegelungsvorhabenEnhancedByCompoundDocumentAssignments(CompoundDocumentId compoundDocumentId,
        List<ServiceState> errorStates);

    List<BestandsrechtListDTO> setBestandsrechteForCompoundDocumentByCompoundDocumentId(CompoundDocumentId compoundDocumentId,
        List<BestandsrechtShortDTO> bestandsrechtDTOs, List<ServiceState> errorStates);
}
