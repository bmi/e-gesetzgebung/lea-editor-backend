// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa;

import java.util.List;
import java.util.Optional;
import de.itzbund.egesetz.bmi.lea.domain.entities.DocumentCount;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;

public interface DokumentPersistencePort {

	List<DocumentDomain> findByCreatedBy(User entity);

	List<DocumentDomain> findByCreatedByAndDocumentIdIn(User entity, List<DocumentId> ids);

	List<DocumentDomain> findByCreatedByAndCompoundDocumentId(User entity, CompoundDocumentId compoundDocumentId);

	List<DocumentDomain> findByCreatedByAndCompoundDocumentIdIn(User entity, List<CompoundDocumentId> compoundDocumentIds);

	Optional<DocumentDomain> findByDocumentId(DocumentId id);

	List<DocumentDomain> findByCompoundDocumentIdIn(List<CompoundDocumentId> compoundDocumentIds);

	DocumentDomain save(DocumentDomain documentEntity);

	Optional<List<DocumentDomain>> findByCompoundDocumentId(CompoundDocumentId compoundDocumentId);

	int countByCompoundDocumentIdAndTypeIs(CompoundDocumentId compoundDocumentId, DocumentType type);

	// --------------------------------------------------

	List<DocumentCount> getDocumentCount(CompoundDocumentId compoundDocumentId);

	boolean existsDokumentCreatedOrUpdatedByUser(UserId userId);
}
