// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.eli;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRALIAS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRAUTHOR;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRCOUNTRY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRDATE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRFORMAT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRLANGUAGE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRNAME;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRNUMBER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRSUBTYPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRTHIS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRURI;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRVERSIONNUMBER;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRAttribute.FRBR_DATE;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRAttribute.FRBR_HREF;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRAttribute.FRBR_LANG;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRAttribute.FRBR_NAME;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRAttribute.FRBR_VALUE;

@Getter
@RequiredArgsConstructor
public enum FRBRElement {

    WORK_FRBR_THIS(Group1FRBREntity.WORK, ELEM_FRBRTHIS, Set.of(FRBR_VALUE)),
    WORK_FRBR_URI(Group1FRBREntity.WORK, ELEM_FRBRURI, Set.of(FRBR_VALUE)),
    WORK_FRBR_ALIAS(Group1FRBREntity.WORK, ELEM_FRBRALIAS, Set.of(FRBR_VALUE, FRBR_NAME)),
    WORK_FRBR_DATE(Group1FRBREntity.WORK, ELEM_FRBRDATE, Set.of(FRBR_DATE, FRBR_NAME)),
    WORK_FRBR_AUTHOR(Group1FRBREntity.WORK, ELEM_FRBRAUTHOR, Set.of(FRBR_HREF)),
    WORK_FRBR_COUNTRY(Group1FRBREntity.WORK, ELEM_FRBRCOUNTRY, Set.of(FRBR_VALUE)),
    WORK_FRBR_NUMBER(Group1FRBREntity.WORK, ELEM_FRBRNUMBER, Set.of(FRBR_VALUE)),
    WORK_FRBR_NAME(Group1FRBREntity.WORK, ELEM_FRBRNAME, Set.of(FRBR_VALUE)),
    WORK_FRBR_SUBTYPE(Group1FRBREntity.WORK, ELEM_FRBRSUBTYPE, Set.of(FRBR_VALUE)),

    EXPRESSION_FRBR_THIS(Group1FRBREntity.EXPRESSION, ELEM_FRBRTHIS, Set.of(FRBR_VALUE)),
    EXPRESSION_FRBR_URI(Group1FRBREntity.EXPRESSION, ELEM_FRBRURI, Set.of(FRBR_VALUE)),
    EXPRESSION_FRBR_AUTHOR(Group1FRBREntity.EXPRESSION, ELEM_FRBRAUTHOR, Set.of(FRBR_HREF)),
    EXPRESSION_FRBR_DATE(Group1FRBREntity.EXPRESSION, ELEM_FRBRDATE, Set.of(FRBR_DATE, FRBR_NAME)),
    EXPRESSION_FRBR_LANGUAGE(Group1FRBREntity.EXPRESSION, ELEM_FRBRLANGUAGE, Set.of(FRBR_LANG)),
    EXPRESSION_FRBR_VERSION_NUMBER(Group1FRBREntity.EXPRESSION, ELEM_FRBRVERSIONNUMBER, Set.of(FRBR_VALUE)),

    MANIFESTATION_FRBR_THIS(Group1FRBREntity.MANIFESTATION, ELEM_FRBRTHIS, Set.of(FRBR_VALUE)),
    MANIFESTATION_FRBR_URI(Group1FRBREntity.MANIFESTATION, ELEM_FRBRURI, Set.of(FRBR_VALUE)),
    MANIFESTATION_FRBR_DATE(Group1FRBREntity.MANIFESTATION, ELEM_FRBRDATE, Set.of(FRBR_DATE, FRBR_NAME)),
    MANIFESTATION_FRBR_AUTHOR(Group1FRBREntity.MANIFESTATION, ELEM_FRBRAUTHOR, Set.of(FRBR_HREF)),
    MANIFESTATION_FRBR_FORMAT(Group1FRBREntity.MANIFESTATION, ELEM_FRBRFORMAT, Set.of(FRBR_VALUE));

    private static final Map<Group1FRBREntity, Map<String, FRBRElement>> elements;

    static {
        elements = new EnumMap<>(Group1FRBREntity.class);
        elements.put(Group1FRBREntity.WORK, new HashMap<>());
        elements.put(Group1FRBREntity.EXPRESSION, new HashMap<>());
        elements.put(Group1FRBREntity.MANIFESTATION, new HashMap<>());

        Arrays.stream(values()).forEach(e -> elements.get(e.getFrbrEntity()).put(e.getLdmlTagName(), e));
    }

    final Group1FRBREntity frbrEntity;
    final String ldmlTagName;
    final Set<FRBRAttribute> attributes;

    public static FRBRElement getFRBRElement(Group1FRBREntity level, String tagName) {
        return elements.get(level).get(tagName);
    }

}
