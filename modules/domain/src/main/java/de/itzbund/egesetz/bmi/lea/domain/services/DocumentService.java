// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.core.json.JSONUtils;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.core.xml.LegalDocMLdeMetadatenFeld;
import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import de.itzbund.egesetz.bmi.lea.core.xml.XmlDocumentContentExtractor;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.EditorRollenUndRechte;
import de.itzbund.egesetz.bmi.lea.domain.dtos.bestand.CreateBestandsrechtDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentImportDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentMetadataDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.UpdateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.PermissionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentForm;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import de.itzbund.egesetz.bmi.lea.domain.mappers.DocumentMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.Bestandsrecht;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.BestandsrechtId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.BestandsrechtPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.DocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.services.filters.ChangeAnnotationFilter;
import de.itzbund.egesetz.bmi.lea.domain.services.filters.FilterChain;
import de.itzbund.egesetz.bmi.lea.domain.services.logging.DomainLoggingService;
import de.itzbund.egesetz.bmi.lea.domain.services.logging.LogActivity;
import de.itzbund.egesetz.bmi.lea.domain.services.meta.MetadatenUpdateService;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.core.Constants.KIBIBYTE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RECHT_KOMMENTARE_LESEN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RECHT_KOMMENTIEREN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RECHT_LESEN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RECHT_SCHREIBEN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RESSORT_KURZBEZEICHNUNG_BT;
import static de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType.REGELUNGSTEXT_STAMMGESETZ;
import static de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState.COMPOUND_DOCUMENT_NOT_FOUND;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState.DOCUMENT_COULD_NOT_BE_PERSISTED;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState.DOCUMENT_NOT_FOUND;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState.OK;

@Service
@RequiredArgsConstructor
@Log4j2
@SuppressWarnings("unused")
public class DocumentService implements DocumentRestPort {

	private static final int BUFFER_SIZE = KIBIBYTE;
	private static final long SIZE_20MB = 20L * KIBIBYTE * KIBIBYTE;
	private static final String DBG_MSG = "Got documentEntity: {} for UUID {}.";

	// ----- Services ---------
	@Autowired
	private CompoundDocumentService compoundDocumentService;
	@Autowired
	private ConverterService converterService;
	@Autowired
	private PlategRequestService plategRequestService;
	@Autowired
	private PropositionService propositionService;
	@Autowired
	private RegelungsvorhabenService regelungsvorhabenService;
	@Autowired
	private AuxiliaryService auxiliaryService;
	@Autowired
	private MetadatenUpdateService metadatenUpdateService;
	// ----- Repositories -----
	@Autowired
	private DokumentPersistencePort documentRepository;
	@Autowired
	private BestandsrechtPersistencePort bestandsrechtRepository;
	// ----- Mapper -----------
	@Autowired
	private DocumentMapper documentMapper;
	@Autowired
	private PropositionMapper propositionMapper;
	// ----- Other ------------
	@Autowired
	private UserService userService;
	@Autowired
	private DomainLoggingService domainLoggingService;
	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private EditorRollenUndRechte editorRollenUndRechte;
	@Autowired
	private LeageSession session;


	/**
	 * Retrieves the document including metadata specified by its ID.
	 *
	 * @param id the ID of the metadata container of the document to be retrieved
	 * @return instance of {@link DocumentDTO} comprising metadata and the document  content
	 */
	// by DocumentController
	@Override
	public DocumentDTO getDocumentById(UUID id, boolean clearAnnotations) {
		DocumentDomain documentEntity = getDocumentEntityById(new DocumentId(id));
		log.debug(DBG_MSG, documentEntity, id);

		if (documentEntity != null) {
			if (clearAnnotations) {
				FilterChain filterChain = new FilterChain();
				filterChain.addFilter(new ChangeAnnotationFilter());
				JSONObject documentJson = JSONUtils.getDocumentObject(documentEntity.getContent());
				filterChain.doFilter(documentJson);

				if (documentJson != null) {
					String updatedContent = JSONUtils.wrapUp(documentJson)
						.toJSONString();
					documentEntity.setContent(updatedContent);
				}
			}
			DocumentDTO documentDTO = buildDocumentDTO(documentEntity);
			documentDTO = enrichDocumentDTOWithPermissions(documentDTO);
			if (documentDTO != null) {
				return documentDTO;
			}

			log.error("PropositionId is null!");
		}

		// indicates error
		log.error("DocumentEntity '{}' should not be null.",
			documentEntity);
		return null;
	}


	public DocumentDTO enrichDocumentDTOWithPermissions(DocumentDTO documentDTO) {
		if (documentDTO == null) {
			return null;
		}

		CompoundDocumentId compoundDocumentId = new CompoundDocumentId(documentDTO.getCompoundDocumentId());
		Map<String, Boolean> rechte = compoundDocumentService.getAccessRightsForCompoundDocument(compoundDocumentId);

		PermissionDTO documentPermissions = PermissionDTO.builder()
			.hasRead(false)
			.hasWrite(false)
			.build();

		PermissionDTO commentPermissions = PermissionDTO.builder()
			.hasRead(false)
			.hasWrite(false)
			.build();

		if (rechte.containsKey(RECHT_LESEN)) {
			documentPermissions.setHasRead(rechte.get(RECHT_LESEN));
		}
		if (rechte.containsKey(RECHT_SCHREIBEN)) {
			documentPermissions.setHasWrite(rechte.get(RECHT_SCHREIBEN));
		}
		if (rechte.containsKey(RECHT_KOMMENTIEREN)) {
			commentPermissions.setHasWrite(rechte.get(RECHT_KOMMENTIEREN));
			commentPermissions.setHasRead(rechte.get(RECHT_KOMMENTIEREN));
		}
		if (rechte.containsKey(RECHT_KOMMENTARE_LESEN)) {
			commentPermissions.setHasRead(rechte.get(RECHT_KOMMENTARE_LESEN));
		}

		//Übergangslösung für BT-User
		String ressort = editorRollenUndRechte.getRessortKurzbezeichnungForNutzer(session.getUser().getGid().getId());
		if (ressort != null && ressort.equals(RESSORT_KURZBEZEICHNUNG_BT)) {
			commentPermissions.setHasRead(true);
			documentPermissions.setHasRead(true);
			if (documentDTO.getCreatedBy().getGid().equals(session.getUser().getGid().getId())) {
				commentPermissions.setHasWrite(true);
				documentPermissions.setHasWrite(true);
			}
		}

		documentDTO.setDocumentPermissions(documentPermissions);
		documentDTO.setCommentPermissions(commentPermissions);
		return documentDTO;
	}


	@Override
	public DocumentSummaryDTO createBestandsrecht(DocumentId documentId, CreateBestandsrechtDTO createBestandsrechtDTO, List<ServiceState> status) {
		if (documentId == null || createBestandsrechtDTO == null) {
			status.add(ServiceState.INVALID_ARGUMENTS);
			return null;
		}

		if (createBestandsrechtDTO.getDokumentenmappenId() == null
			|| createBestandsrechtDTO.getRegelungsvorhabenId() == null
			|| Utils.isMissing(createBestandsrechtDTO.getKurzTitel())
			|| Utils.isMissing(createBestandsrechtDTO.getLangTitel())) {
			status.add(ServiceState.INVALID_ARGUMENTS);
			return null;
		}

		CompoundDocumentId compoundDocumentId = new CompoundDocumentId(createBestandsrechtDTO.getDokumentenmappenId());
		RegelungsVorhabenId regelungsVorhabenId = new RegelungsVorhabenId(createBestandsrechtDTO.getRegelungsvorhabenId());

		DocumentDomain document = loadDocumentForBestandsrecht(documentId, status);
		if (document == null) {
			return null;
		}

		CompoundDocument compoundDocument = compoundDocumentService.getCompoundDocumentDomainEntity(compoundDocumentId);
		if (compoundDocument == null) {
			status.add(COMPOUND_DOCUMENT_NOT_FOUND);
			return null;
		}

		return createBestandsrecht(regelungsVorhabenId, compoundDocument, document,
			createBestandsrechtDTO.getKurzTitel(), createBestandsrechtDTO.getLangTitel(), status);
	}


	private DocumentDomain loadDocumentForBestandsrecht(DocumentId documentId, List<ServiceState> status) {
		DocumentDomain document = documentRepository.findByDocumentId(documentId).orElse(null);
		if (document == null) {
			status.add(DOCUMENT_NOT_FOUND);
			return null;
		}

		DocumentType documentType = document.getType();
		if (!(documentType == REGELUNGSTEXT_STAMMGESETZ || documentType == REGELUNGSTEXT_STAMMVERORDNUNG)) {
			status.add(ServiceState.INVALID_ARGUMENTS);
			return null;
		}

		return document;
	}


	private DocumentSummaryDTO createBestandsrecht(
		RegelungsVorhabenId regelungsVorhabenId, CompoundDocument compoundDocument, DocumentDomain document,
		String kurzTitel, String langTitel, List<ServiceState> status) {
		UUID documentId = document.getDocumentId().getId();
		String content = document.getContent();
		Bestandsrecht bestandsrecht = Bestandsrecht.builder()
			.bestandsrechtId(new BestandsrechtId(documentId))
			.eli(String.format("eli/bestand/%s", documentId))
			.titelKurz(kurzTitel)
			.titelLang(langTitel)
			.contentJson(content)
			.build();
		List<ServiceState> status2 = new ArrayList<>();
		bestandsrechtRepository.save(bestandsrecht, regelungsVorhabenId, status2);

		if (status2.isEmpty() || !status2.get(0).equals(OK)) {
			status.add(DOCUMENT_COULD_NOT_BE_PERSISTED);
			return null;
		}

		CompoundDocumentDomain compoundDocumentDomain = compoundDocument.getCompoundDocumentEntity();
		compoundDocumentService.update(
			compoundDocumentDomain.getCompoundDocumentId(),
			compoundDocumentDomain.getTitle(),
			compoundDocumentDomain.getVerfahrensType(),
			DocumentState.BESTANDSRECHT,
			new ArrayList<>());

		DocumentSummaryDTO documentSummaryDTO = documentMapper.mapSummaryDomain(document);
		documentSummaryDTO.setState(DocumentState.BESTANDSRECHT);
		documentSummaryDTO.setVersion(compoundDocumentDomain.getVersion());
		documentSummaryDTO.setDocumentPermissions(PermissionDTO.builder()
			.hasRead(true)
			.hasWrite(false)
			.build());
		documentSummaryDTO.setCommentPermissions(PermissionDTO.builder()
			.hasRead(true)
			.hasWrite(false)
			.build());
		status.add(OK);
		return documentSummaryDTO;
	}


	// by CommentService
	public Document getDocumentByIdNEW(DocumentId id) {

		DocumentDomain documentEntity = getDocumentEntityById(id);
		log.debug(DBG_MSG, documentEntity, id);

		return new Document(documentEntity);
	}


	// by THIS and often
	public DocumentDomain getDocumentEntityById(DocumentId id) {
		Optional<DocumentDomain> optionalDocumentEntity = documentRepository.findByDocumentId(id);
		return optionalDocumentEntity.orElse(null);
	}


	/**
	 * Updates an existing document in the database, effectively replacing the former content completely.
	 *
	 * @param id                the ID of the metadata container of the updated document
	 * @param updateDocumentDTO the current document content
	 */
	// by DocumentController
	@Override
	public DocumentDTO updateDocument(UUID id, UpdateDocumentDTO updateDocumentDTO,
		List<ServiceState> states) {
		DocumentDomain documentEntity = getDocumentEntityById(new DocumentId(id));

		if (documentEntity == null) {
			states.add(DOCUMENT_NOT_FOUND);
			return null;
		}

		DocumentState compoundDocumentState = compoundDocumentService.getCompoundDocumentState(documentEntity.getCompoundDocumentId());

		if (compoundDocumentState == DocumentState.DRAFT
			|| (compoundDocumentState == DocumentState.DRAFT.BEREIT_FUER_BUNDESTAG && documentEntity.getCreatedBy().equals(session.getUser()))
		) {
			User userEntity = userService.getUser();
			DocumentDomain updatedDocumentEntity = updateDocument(documentEntity, updateDocumentDTO, userEntity);

			states.add(ServiceState.OK);

			UUID propositionUuid = propositionService.getPropositionUuid(updatedDocumentEntity);
			DocumentDTO documentDTO = getDocumentDTO(documentEntity, propositionUuid);

			documentDTO.setState(compoundDocumentService.getCompoundDocumentState(new CompoundDocumentId(documentDTO.getCompoundDocumentId())));
			return enrichDocumentDTOWithPermissions(documentDTO);
		}

		states.add(ServiceState.DOCUMENT_IS_NOT_IN_DRAFT);

		return null;
	}

	private DocumentDomain updateDocument(DocumentDomain documentEntity, UpdateDocumentDTO updateDocumentDTO, User userEntity) {
		String newContent = updateDocumentDTO.getContent();
		String newTitle = updateDocumentDTO.getTitle();

		if (!Utils.isMissing(newContent)) {
			documentEntity.setContent(newContent);
		}

		if (!Utils.isMissing(newTitle)) {
			documentEntity.setTitle(newTitle);
		}

		documentEntity.setUpdatedBy(userEntity);
		return documentRepository.save(documentEntity);
	}

	public DocumentDTO buildDocumentDTO(DocumentDomain documentEntity) {
		UUID propositionId = propositionService.getPropositionUuid(documentEntity);

		if (propositionId != null) {
			DocumentDTO documentDTO = getDocumentDTO(documentEntity, propositionId);
			log.debug("The documentDTO is: {}.", documentDTO);
			return documentDTO;
		}

		return null;
	}


	public DocumentDTO getDocumentDTO(DocumentDomain documentEntity, UUID propositionId) {

		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO;

		CompoundDocumentId compoundDocumentId = documentEntity.getCompoundDocumentId();
		CompoundDocument compoundDocument = compoundDocumentService.getCompoundDocumentById(compoundDocumentId, new ArrayList<>());

		DocumentState documentState = compoundDocument.getState();

		if (documentState.equals(DocumentState.DRAFT)) {
			regelungsvorhabenEditorDTO = getChangeableRegelungsvorhabenForDocument(compoundDocument, propositionId);
		} else {
			regelungsvorhabenEditorDTO = getUnchangeableRegelungsvorhabenForDocument(compoundDocument, propositionId);
		}

		//Übergangslösung für BT-User
		String ressort = editorRollenUndRechte.getRessortKurzbezeichnungForNutzer(session.getUser().getGid().getId());
		if (!(ressort != null && ressort.equals(RESSORT_KURZBEZEICHNUNG_BT))) {
			updateProprietaryMetdata(documentEntity, regelungsvorhabenEditorDTO);
		}

		DocumentDTO documentDTO = documentMapper.mapToDocumentDTO(documentEntity, regelungsvorhabenEditorDTO);
		documentDTO.setState(documentState);
		documentDTO.setVersion(compoundDocument.getCompoundDocumentEntity().getVersion());
		documentDTO.setMappenName(compoundDocument.getCompoundDocumentEntity().getTitle());

		return documentDTO;
	}

	private RegelungsvorhabenEditorDTO getChangeableRegelungsvorhabenForDocument(CompoundDocument compoundDocument, UUID propositionId) {
		List<RegelungsvorhabenEditorDTO> regelungsvorhabenEditorDTOList = regelungsvorhabenService.getRegulatoryProposalsByIdList(
			List.of(propositionId.toString()));
		if (regelungsvorhabenEditorDTOList.isEmpty()) {
			User createdBy = compoundDocument.getCreatedBy();
			return plategRequestService.getRegulatoryProposalUserCanAccess(createdBy, propositionId);
		} else {
			return regelungsvorhabenEditorDTOList.get(0);
		}
	}

	private RegelungsvorhabenEditorDTO getUnchangeableRegelungsvorhabenForDocument(CompoundDocument compoundDocument, UUID propositionId) {

		UUID fixedRegelungsvorhabenReferenzId = compoundDocument.getCompoundDocumentEntity().getFixedRegelungsvorhabenReferenzId();

		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = getChangeableRegelungsvorhabenForDocument(compoundDocument, propositionId);
		if (regelungsvorhabenEditorDTO == null) {
			return null;
		}

		RegelungsvorhabenEditorDTO noChangeableForGivenRegelungsvorhabenId = regelungsvorhabenService.getNoChangeableForGivenRegelungsvorhabenId(
			new RegelungsVorhabenId(regelungsvorhabenEditorDTO.getId()), fixedRegelungsvorhabenReferenzId);

		// Fallback at missing data
		if (noChangeableForGivenRegelungsvorhabenId == null) {
			return regelungsvorhabenEditorDTO;
		}

		return noChangeableForGivenRegelungsvorhabenId;
	}


	@SuppressWarnings("unchecked")
	private void updateProprietaryMetdata(DocumentDomain documentEntity,
		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO) {
		Proposition proposition = propositionMapper.mapD(regelungsvorhabenEditorDTO);
		JSONObject dokument;

		try {
			dokument = (JSONObject) ((JSONArray) new JSONParser().parse(documentEntity.getContent())).get(0);
			dokument = metadatenUpdateService.update(dokument, proposition);
			JSONArray wrapper = new JSONArray();
			wrapper.add(dokument);
			documentEntity.setContent(wrapper.toJSONString());
		} catch (ParseException e) {
			log.error(e);
		}
	}


	public DocumentState getFromCompoundDocumentInheritedState(DocumentDomain documentEntity) {
		return compoundDocumentService.getCompoundDocumentState(documentEntity.getCompoundDocumentId());
	}


	/**
	 * We want to find out, if document exists. But to prevent another request of fetching the document, we fill the object by reference as we fetched it
	 * anyway
	 *
	 * @param document the document
	 * @return true if document exists
	 */
	public boolean isDocumentExisting(Document document) {
		DocumentId documentId = document.getDocumentEntity()
			.getDocumentId();
		DocumentDomain documentEntityById = getDocumentEntityById(documentId);
		document.setDocumentEntity(documentEntityById);

		return documentEntityById != null;
	}


	@Override
	public DocumentMetadataDTO importDocument(CompoundDocumentId compoundDocumentId,
		DocumentImportDTO documentImportDTO,
		long contentLength, List<ServiceState> status) {
		String xmlContent = documentImportDTO.getXmlContent();
		String title = documentImportDTO.getTitle();

		// Check validity

		if (contentLength > SIZE_20MB
			|| xmlContent.isBlank()
			|| !Utils.isXMLValid(xmlContent)) {
			status.add(ServiceState.INVALID_ARGUMENTS);
			return null;
		}

		ReusableInputSource ris =
			new ReusableInputSource(new ByteArrayInputStream(xmlContent.getBytes(StandardCharsets.UTF_8)));
		DocumentType documentType = getDocumentType(ris);
		if (documentType == null) {
			status.add(ServiceState.INVALID_ARGUMENTS);
			return null;
		}

		// Get the compound document

		CompoundDocument compoundDocument = compoundDocumentService.getCompoundDocument(compoundDocumentId, false);

		if (compoundDocument == null) {
			status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
			return null;
		}

		// check state

		DocumentState state = compoundDocument.getCompoundDocumentEntity()
			.getState();
		if (state != DocumentState.DRAFT) {
			status.add(ServiceState.INVALID_STATE);
			return null;
		}

		// Try to transform to JSON -> this includes XML validation!

		String jsonContent = null;
		try {
			jsonContent = converterService.convertXmlToJson(xmlContent);
		} catch (RuntimeException e) {
			log.error(e);
		}
		if (jsonContent == null || jsonContent.isBlank() || !Utils.isJSONValid(jsonContent)) {
			status.add(ServiceState.DOCUMENT_NOT_VALID);
			return null;
		}

		// Get the document
		return getDocumentMetadataDTO(title, auxiliaryService.appendEmptyTextNodes(jsonContent), documentType,
			compoundDocument, compoundDocumentId, status);
	}


	private DocumentType getDocumentType(ReusableInputSource ris) {
		XmlDocumentContentExtractor extractor = ris.getExtractor();
		RechtsetzungsdokumentArt art =
			RechtsetzungsdokumentArt.fromLiteral(extractor.getMetadatum(LegalDocMLdeMetadatenFeld.META_ART));
		RechtsetzungsdokumentForm form =
			RechtsetzungsdokumentForm.fromLiteral(extractor.getMetadatum(LegalDocMLdeMetadatenFeld.META_FORM));
		RechtsetzungsdokumentTyp typ =
			RechtsetzungsdokumentTyp.fromLiteral(extractor.getMetadatum(LegalDocMLdeMetadatenFeld.META_TYP));

		if (art == null || form == null || typ == null) {
			return null;
		}

		return DocumentType.getFromTypology(art, form, typ);
	}


	private DocumentMetadataDTO getDocumentMetadataDTO(String title, String jsonContent, DocumentType documentType,
		CompoundDocument compoundDocument, CompoundDocumentId compoundDocumentId, List<ServiceState> status) {
		User user = userService.getUser();
		// Get the document
		Document document = new Document(DocumentDomain.builder()
			.documentId(new DocumentId(UUID.randomUUID()))
			.title(title)
			.content(jsonContent)
			.type(documentType)
			.compoundDocumentId(compoundDocument.getCompoundDocumentId())
			.createdBy(user)
			.updatedBy(user)
			.createdAt(Instant.now())
			.updatedAt(Instant.now())
			.build());
		document.setDocumentRepository(documentRepository);
		document.setMetadatenUpdateService(metadatenUpdateService);

		// Add the document, if allowed, and return the metadata
		// if successful, this method already sets status=OK
		compoundDocumentService.addDocument(compoundDocumentId, document, status);
		if (!status.isEmpty() && ServiceState.OK == status.get(0)) {
			domainLoggingService.logActivity(LogActivity.IMPORT_LDML, user.getGid().getId());
			return documentMapper.mapMetaData(documentMapper.mapToDto(document));
		} else {
			return null;
		}
	}

	// --- Moved und must be redesigned

	public List<DocumentDomain> findByCompoundDocumentIdIn(List<CompoundDocumentId> compoundDocumentIds) {
		return documentRepository.findByCompoundDocumentIdIn(compoundDocumentIds);
	}

	public List<DocumentDomain> findByCreatedByAndCompoundDocumentId(User user, CompoundDocumentId compoundDocumentId) {
		return documentRepository.findByCreatedByAndCompoundDocumentId(user, compoundDocumentId);
	}

}
