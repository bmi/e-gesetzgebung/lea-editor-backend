// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.export;

import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.mappers.BaseMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public class ExportContext {

    private DokumentPersistencePort documentRepository;

    private Proposition proposition;

    private CompoundDocumentDomain compoundDocumentDomain;

    private DocumentDomain documentEntity;

    public PropositionDTO getPropositionDTO() {
        PropositionMapper propositionMapper = new PropositionMapperImpl(new BaseMapperImpl());
        RegelungsvorhabenEditorDTO tmp = propositionMapper.mapD(proposition);
        return propositionMapper.map(tmp);
    }

}
