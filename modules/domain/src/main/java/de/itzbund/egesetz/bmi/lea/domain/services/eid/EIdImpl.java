// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.eid;

import de.itzbund.egesetz.bmi.lea.domain.ports.eid.Bezeichner;
import de.itzbund.egesetz.bmi.lea.domain.ports.eid.EId;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
public final class EIdImpl implements EId {

    private static final String DLM = "_";

    @Size(min = 1)
    private final List<Bezeichner> bezeichnerListe;


    private EIdImpl(@NonNull Bezeichner bezeichner) {
        bezeichnerListe = List.of(bezeichner);
    }


    private EIdImpl(@NonNull EIdImpl eId, Bezeichner bezeichner) {
        List<Bezeichner> tmp = new ArrayList<>(eId.bezeichnerListe);
        tmp.add(bezeichner);
        bezeichnerListe = Collections.unmodifiableList(tmp);
    }


    private EIdImpl(@NonNull List<Bezeichner> bezeichnerListe) {
        this.bezeichnerListe = bezeichnerListe;
    }


    public static EIdImpl newInstance(Bezeichner bezeichner) {
        if (((BezeichnerImpl) bezeichner).isValid()) {
            return new EIdImpl(bezeichner);
        } else {
            return null;
        }
    }


    @SuppressWarnings({"java:S5998", "java:S5843"})
    public static EId fromString(
        @NonNull
        @Pattern(regexp = "([a-zäöü]+)-(([1-9]\\d*([a-z])?)|([a-z]{1,3})([.]\\d)?)"
            + "(_(([a-zäöü]+)-(([1-9]\\d*([a-z])?)|([a-z]{1,3})([.]\\d)?)))*")
        String eidString) {
        List<Bezeichner> bezeichnerList =
            Arrays.stream(eidString.split(DLM)).map(BezeichnerImpl::fromString).collect(Collectors.toList());
        EId result = EIdImpl.newInstance(bezeichnerList.get(0));
        bezeichnerList.remove(0);

        for (Bezeichner b : bezeichnerList) {
            if (result != null) {
                result = result.extendBy(b);
            } else {
                return null;
            }
        }

        return result;
    }


    @Override
    public EId getPraefix() {
        if (bezeichnerListe.size() == 1) {
            return null;
        }

        List<Bezeichner> tmp = new ArrayList<>(bezeichnerListe);
        tmp.remove(tmp.size() - 1);
        return new EIdImpl(tmp);
    }


    @Override
    public EId extendBy(Bezeichner bezeichner) {
        if (((BezeichnerImpl) bezeichner).isValid()) {
            return new EIdImpl(this, bezeichner);
        } else {
            return null;
        }
    }


    @Override
    public String toString() {
        return bezeichnerListe.stream().map(Object::toString).collect(Collectors.joining(DLM));
    }


    @Override
    public boolean equals(Object other) {
        if (!(other instanceof EIdImpl)) {
            return false;
        }

        return toString().equals(other.toString());
    }


    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(toString())
            .toHashCode();
    }

}
