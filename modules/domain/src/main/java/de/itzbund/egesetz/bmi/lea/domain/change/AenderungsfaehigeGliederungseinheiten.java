// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.change;

import de.itzbund.egesetz.bmi.lea.domain.change.AenderungsbefehlUtils.Gliederungsebene;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CONTENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_INTRO;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LIST;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_P;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_POINT;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewDefaultJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewDefaultJSONObjectWithText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNumObject;
import static de.itzbund.egesetz.bmi.lea.domain.change.AenderungsbefehlUtils.Gliederungsebene.BUCHSTABE;
import static de.itzbund.egesetz.bmi.lea.domain.change.AenderungsbefehlUtils.getListPointNumValue;

/**
 * Siehe HdR, 4. Auflage, Rn. 463
 */
public class AenderungsfaehigeGliederungseinheiten {

	private final List<AenderungsfaehigeGliederungseinheit> aenderungen = new ArrayList<>();


	public void addAenderungsfaehigeGliederungseinheit(AenderungsfaehigeGliederungseinheit gliederungseinheit) {
		if (gliederungseinheit != null && gliederungseinheit.getAenderungsart() != Aenderungsart.KEINE) {
			aenderungen.add(gliederungseinheit);
		}
	}


	@SuppressWarnings("unchecked")
	public void makeAenderungsbefehle(JSONObject articlePara) {
		JSONObject listObject = makeNewDefaultJSONObject(ELEM_LIST);
		JSONArray listChildren = new JSONArray();

		// Eingangssatz
		JSONObject introObject = makeNewDefaultJSONObject(ELEM_INTRO);
		addChildren(introObject, getEingangssatz());
		listChildren.add(introObject);

		// Änderungsbefehle
		AtomicInteger num = new AtomicInteger(1);
		aenderungen.forEach(gliederungseinheit -> {
			JSONObject listPoint = makeNewDefaultJSONObject(ELEM_POINT);
			String number = String.valueOf(num.getAndIncrement());
			JSONObject numObject = makeNumObject(number, String.format("%s.", number));
			List<JSONObject> aenderungsbefehle = gliederungseinheit.getAenderungsbefehle();
			JSONObject pointContentObject;

			if (aenderungsbefehle.size() == 1 && !gliederungseinheit.getAenderungsart().equals(Aenderungsart.AENDERN)) {
				pointContentObject = getContentElement(aenderungsbefehle.get(0));
			} else {
				pointContentObject = getListElement(aenderungsbefehle, gliederungseinheit.getIntroObject(), BUCHSTABE);
			}

			addChildren(listPoint, numObject, pointContentObject);
			listChildren.add(listPoint);
		});

		addChildren(listObject, listChildren);
		addChildren(articlePara, listObject);
	}


	private JSONObject getContentElement(JSONObject aenderungsbefehl) {
		JSONObject contentObject = makeNewDefaultJSONObject(ELEM_CONTENT);
		JSONObject contentP = makeNewDefaultJSONObject(ELEM_P);
		addChildren(contentP, aenderungsbefehl);
		addChildren(contentObject, contentP);
		return contentObject;
	}


	@SuppressWarnings("unchecked")
	private JSONObject getListElement(List<JSONObject> aenderungsbefehle, JSONObject introObject, Gliederungsebene gliederungsebene) {
		JSONObject listObject = makeNewDefaultJSONObject(ELEM_LIST);
		JSONArray listChildren = new JSONArray();
		listChildren.add(introObject);

		AtomicInteger num = new AtomicInteger(1);
		aenderungsbefehle.forEach(aenderungsbefehl -> {
			JSONObject listPoint = makeNewDefaultJSONObject(ELEM_POINT);
			String number = getListPointNumValue(num.getAndIncrement(), gliederungsebene);
			JSONObject numObject = makeNumObject(number, String.format("%s)", number));
			addChildren(listPoint, numObject, aenderungsbefehl);
			listChildren.add(listPoint);
		});

		addChildren(listObject, listChildren);
		return listObject;
	}


	private JSONObject getEingangssatz() {
		return makeNewDefaultJSONObjectWithText(ELEM_P, "(Eingangssatz)");
	}

}
