// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.synopse;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.compare.RegulatoryTextComparer;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.PermissionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.EIdGeneratorService;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.IllegalDocumentType;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_NAME;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_REFERSTO;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_STATUS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_STYLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.DATE_TIME_FORMATTER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BLOCK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_HEADING;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_MAINBODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SPAN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TABLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TBLOCK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TD;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TH;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TR;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.getChangeMarker;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.toTextChangeMarkerLiteral;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addAttributes;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDescendant;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getEffectiveTextValue;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getLocalName;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.isTextWrapper;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewDefaultJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.replaceChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.setText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.withDefaultPrefix;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.wrapUp;
import static de.itzbund.egesetz.bmi.lea.domain.assertion.DomainAssertions.assertTableObjectNotNull;


@UtilityClass
@SuppressWarnings("java:S1192")
public class SynopsisUtil {

    private static final String REFERS_TO_STRUCTURE = "synopse-struktur";
    private static final String REFERS_TO_TEXT = "synopse-text";
    private static final String DEFAULT_NAME_ATTRIBUTE = "attributsemantik-noch-undefiniert";

    @SuppressWarnings("java:S1160")
    public static DocumentDTO getSynopseDocument(List<Pair<JSONObject, JSONObject>> table, String template, UserDTO userDTO)
        throws ParseException, IllegalDocumentType {
        String timestamp = LocalDateTime.now().format(DATE_TIME_FORMATTER);
        JSONObject docObject = (JSONObject) ((JSONArray) new JSONParser().parse(template)).get(0);
        DocumentDTO documentDTO = DocumentDTO.builder()
            .id(UUID.randomUUID())
            .createdBy(userDTO)
            .createdAt(timestamp)
            .updatedBy(userDTO)
            .updatedAt(timestamp)
            .type(DocumentType.SYNOPSE)
            .state(DocumentState.FINAL)
            .title("Synopse")
            .version("1")
            .documentPermissions(PermissionDTO.builder()
                .hasRead(true)
                .hasWrite(false)
                .build())
            .commentPermissions(PermissionDTO.builder()
                .hasRead(true)
                .hasWrite(false)
                .build())
            .build();

        documentDTO.setContent(wrapUp(getFilledTable(table, docObject)).toJSONString());
        return documentDTO;
    }

    private static JSONObject getFilledTable(@NonNull List<Pair<JSONObject, JSONObject>> table, @NonNull JSONObject document) throws IllegalDocumentType {
        JSONObject tableObject = getDescendant(document, true,
            "*", ELEM_MAINBODY, ELEM_TBLOCK, ELEM_TABLE);
        assertTableObjectNotNull(tableObject);

        for (int i = 0; i < table.size(); i++) {
            Pair<JSONObject, JSONObject> pair = table.get(i);
            makeTableRow(tableObject, pair.getLeft(), pair.getRight(), i == 0);
        }

        EIdGeneratorService eIdGeneratorService = new EIdGeneratorService();
        document = eIdGeneratorService.erzeugeEIdsFuerDokument(document);
        return document;
    }

    private static void makeTableRow(JSONObject tableObject, JSONObject leftObject, JSONObject rightObject, boolean isHeader) {
        JSONObject row = makeNewDefaultJSONObject(ELEM_TR);
        JSONObject left = makeTableCell(leftObject, isHeader, getChangeMarker(leftObject));
        JSONObject right = makeTableCell(rightObject, isHeader, getChangeMarker(rightObject));

        replaceChildren(row, left, right);
        addChildren(tableObject, row);
    }

    @SuppressWarnings("unchecked")
    private static JSONObject makeTableCell(JSONObject jsonObject, boolean isHeader, String marker) {
        JSONObject cellObject = makeNewDefaultJSONObject(isHeader ? ELEM_TH : ELEM_TD);

        String style = getChangeMarker(jsonObject);
        if (style.isBlank()) {
            style = marker;
        }

        String refersTo = REFERS_TO_STRUCTURE;
        String text = getEffectiveTextValue(jsonObject);
        String type = getSynopseType(jsonObject, text);
        if (!Utils.isMissing(type) && !Utils.isMissing(text)) {
            refersTo = String.format("%s|%s", refersTo, type);
        }

        addAttributes(cellObject, Pair.of(ATTR_REFERSTO, refersTo), Pair.of(ATTR_STYLE, style));

        JSONObject blockObject = makeNewDefaultJSONObject(ELEM_BLOCK);
        addAttribute(blockObject, Pair.of(ATTR_NAME, DEFAULT_NAME_ATTRIBUTE));

        JSONArray children = getChildren(jsonObject);
        if (children != null && !children.isEmpty()) {
            Iterator<Object> iterator = children.iterator();
            while (iterator.hasNext()) {
                Object object = iterator.next();
                JSONObject child = (JSONObject) object;
                String childText = getEffectiveTextValue(child);
                if (!(Utils.isMissing(childText) && iterator.hasNext())) {
                    addTextFragment(blockObject, child, null);
                }
            }
        } else {
            setText(blockObject, "");
        }

        replaceChildren(cellObject, blockObject);
        return cellObject;
    }

    private static String getSynopseType(JSONObject jsonObject, String text) {
        String type = getType(jsonObject);
        if (jsonObject == null || Utils.isMissing(type)) {
            return "";
        }

        String header = RegulatoryTextComparer.STRING_FMT_SYNOPSE_HEADING;

        if (getLocalName(type).equals(ELEM_BLOCK) && firstWord(header).equals(firstWord(text))) {
            type = withDefaultPrefix(ELEM_HEADING);
        }

        return type;
    }

    private static String firstWord(String text) {
        return text.split(" ")[0];
    }

    @SuppressWarnings("unchecked")
    private static void addTextFragment(JSONObject parent, JSONObject child, String marker) {
        if (isTextWrapper(child)) {
            JSONObject spanObject = makeNewJSONObject(ELEM_SPAN, true, false);
            String childMarker = getChangeMarker(child);
            if (Utils.isMissing(childMarker)) {
                childMarker = toTextChangeMarkerLiteral(marker);
            }
            addAttributes(spanObject, Pair.of(ATTR_REFERSTO, REFERS_TO_TEXT), Pair.of(ATTR_STATUS, childMarker));
            setText(spanObject, getText(child));
            addChildren(parent, spanObject);
        } else {
            String childMarker = getChangeMarker(child);
            JSONArray children = getChildren(child);
            if (children != null) {
                children.forEach(o -> {
                    JSONObject grandChild = (JSONObject) o;
                    addTextFragment(parent, grandChild, childMarker);
                });
            }
        }
    }

    public static String getHtmlPage(List<DiffMatchPatch.Diff> diffs) {

        return "<!DOCTYPE html>"
            + "<html lang=\"de\">"
            + "<head>"
            + "<meta charset=\"UTF-8\"/>"
            + "<title>Textvergleich</title>"
            + "</head>"
            + "<body>"
            + "<h1>Textvergleich</h1>"
            + "<h2>Änderungsmarkierungen</h2>"
            + "<table style=\"border: 1px solid black; border-collapse: collapse\">"
            + "<tr style=\"border: 1px solid black; border-collapse: collapse\">"
            + "<td style=\"border: 1px solid black; border-collapse: collapse; vertical-align: text-top; padding: 10px\">"
            + getBaseTextWithDiffs(diffs)
            + "</td>"
            + "<td style=\"border: 1px solid black; border-collapse: collapse; vertical-align: text-top; padding: 10px\">"
            + getVersionTextWithDiffs(diffs)
            + "</td>"
            + "</tr>"
            + "</table>"
            + "<h2>Liste der Differenzen</h2>"
            + getDiffListAsOL(diffs)
            + "</body>"
            + "</html>";
    }


    private static String getBaseTextWithDiffs(List<DiffMatchPatch.Diff> diffs) {
        StringBuilder sb = new StringBuilder();
        diffs.forEach(diff -> {
            if (diff.operation == DiffMatchPatch.Operation.DELETE) {
                sb.append("<del style=\"color: red\">")
                    .append(diff.text)
                    .append("</del>");
            } else if (diff.operation == DiffMatchPatch.Operation.EQUAL) {
                sb.append("<span>")
                    .append(diff.text)
                    .append("</span>");
            }
        });
        return sb.toString();
    }


    private static String getVersionTextWithDiffs(List<DiffMatchPatch.Diff> diffs) {
        StringBuilder sb = new StringBuilder();
        diffs.forEach(diff -> {
            if (diff.operation == DiffMatchPatch.Operation.INSERT) {
                sb.append("<ins style=\"color: green\">")
                    .append(diff.text)
                    .append("</ins>");
            } else if (diff.operation == DiffMatchPatch.Operation.EQUAL) {
                sb.append("<span>")
                    .append(diff.text)
                    .append("</span>");
            }
        });
        return sb.toString();
    }


    private static String getDiffListAsOL(List<DiffMatchPatch.Diff> diffs) {
        StringBuilder sb = new StringBuilder();
        sb.append("<ol style=\"font-family: Monospace; font-size: 11pt\">");
        diffs.forEach(diff -> {
            String color;
            switch (diff.operation) {
                case INSERT:
                    color = "green";
                    break;
                case DELETE:
                    color = "red";
                    break;
                default:
                    color = "black";
            }
            sb.append("<li>")
                .append(String.format("<span style=\"color: %s; font-weight: bold\">", color))
                .append(getOperationText(diff))
                .append("</span>&nbsp;&nbsp;<span>")
                .append("<span style=\"font-weight: lighter;color: violet;\">[</span>")
                .append(diff.text.replace(" ", "⎵"))
                .append("<span style=\"font-weight: lighter;color: violet;\">]</span>")
                .append("</span></li>");
        });
        sb.append("</ol>");
        return sb.toString();
    }


    private static String getOperationText(DiffMatchPatch.Diff diff) {
        DiffMatchPatch.Operation op = diff.operation;
        String text = op.name();
        return op == DiffMatchPatch.Operation.EQUAL ? text + " " : text;
    }

}
