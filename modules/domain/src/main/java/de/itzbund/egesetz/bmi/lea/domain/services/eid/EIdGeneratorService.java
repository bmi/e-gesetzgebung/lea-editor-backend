// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.eid;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentForm;
import de.itzbund.egesetz.bmi.lea.domain.ports.eid.Bezeichner;
import de.itzbund.egesetz.bmi.lea.domain.ports.eid.EId;
import de.itzbund.egesetz.bmi.lea.domain.ports.eid.JSONContext;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.EIdGeneratorRestPort;
import lombok.extern.log4j.Log4j2;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_NAME;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ARTICLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_CHILDREN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_EID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_TEXT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_VALUE;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.findChild;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getFirstChild;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getMarker;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;

@Service
@Log4j2
public class EIdGeneratorService implements EIdGeneratorRestPort {

    private static final Pattern PTN_LI_VALUE = Pattern.compile("\\p{P}*\\s*(\\w+)\\s*\\p{P}*");

    private static final String[] DOC_TYPES = new String[]{"akn:doc", "akn:bill", "akn:documentCollection"};

    private static final String DOC_META = "akn:meta";

    private static final String[][] TYPES = new String[][]{
        new String[]{DOC_META},
        new String[]{"akn:proprietary"},
        new String[]{"meta:legalDocML.de_metadaten"},
        new String[]{"meta:form"},
        new String[]{"lea:textWrapper"}
    };


    @Override
    public JSONObject erzeugeEIdsFuerDokument(JSONObject dokument) throws IllegalDocumentType {
        RechtsetzungsdokumentForm form = getRechtssetzungsdokumentForm(dokument);
        if (form == RechtsetzungsdokumentForm.NICHT_VORHANDEN) {
            throw new IllegalDocumentType("Unsupported document type");
        }

        JSONContextImpl jsonContext = new JSONContextImpl();
        jsonContext.setPraefix("");
        return erzeugeEIdsFuerTeilbaum(dokument, jsonContext, form);
    }


    @Override
    public JSONObject erzeugeEIdsFuerTeilbaum(JSONObject baum, JSONContext jsonContext, RechtsetzungsdokumentForm form) {
        amendEIds(baum, jsonContext, form);
        return baum;
    }


    @SuppressWarnings("unchecked")
    private void amendEIds(JSONObject baum, JSONContext jsonContext, RechtsetzungsdokumentForm form) {
        // set eid of current object
        String type = getType(baum);
        String praefix = jsonContext.getPraefix();
        if (!Utils.isMissing(type)) {
            String literal = Abkuerzungsliteral.getLiteral(type,
                type.contains(ELEM_ARTICLE)
                    ? form
                    : RechtsetzungsdokumentForm.STAMMFORM);
            if (!Utils.isMissing(literal)) {
                String marker = getMarker(baum);
                Bezeichner bezeichner = berechneBezeichner(baum, jsonContext, literal, marker, type);
                EId eId = ("".equals(praefix))
                    ? EIdImpl.newInstance(bezeichner)
                    : Objects.requireNonNull(EIdImpl.fromString(praefix)).extendBy(bezeichner);
                if (eId != null) {
                    praefix = eId.toString();
                    baum.put(JSON_KEY_EID, praefix);
                }
            }
        }

        // iterate over children (except for Meta-Container)
        processChildren(baum, type, praefix, form);
    }


    private Bezeichner berechneBezeichner(JSONObject baum, JSONContext jsonContext, String literal, String marker,
        String type) {
        if (jsonContext.isProcessingOrderedList()) {
            return getOlLiBezeichner(baum, jsonContext, literal);
        } else if (!Utils.isMissing(marker)) {
            return BezeichnerImpl.newInstance(literal, marker);
        } else {
            return BezeichnerImpl.newInstance(literal, jsonContext.getCurrentTypeCount(type));
        }
    }


    @SuppressWarnings("unchecked")
    private void processChildren(JSONObject baum, String type, String praefix, RechtsetzungsdokumentForm form) {
        // iterate over children
        JSONArray children = (JSONArray) baum.get(JSON_KEY_CHILDREN);
        if (children != null) {
            JSONContext newContext = new JSONContextImpl();
            newContext.setPraefix(praefix);

            if ("akn:ol".equals(type)) {
                newContext.initializeOrderedList(children.size() + 1);
            }

            children.forEach(obj -> {
                JSONObject child = (JSONObject) obj;
                newContext.addTypeOccurrence(getType(child));
                amendEIds(child, newContext, form);
            });
        }
    }


    @SuppressWarnings("java:S1854")
    private RechtsetzungsdokumentForm getRechtssetzungsdokumentForm(JSONObject dokument) {
        JSONObject currentObject = findChild(dokument, DOC_TYPES);
        String docName = currentObject == null ? "" : (String) currentObject.get(ATTR_NAME);

        for (String[] names : TYPES) {
            if (currentObject == null) {
                return RechtsetzungsdokumentForm.NICHT_VORHANDEN;
            }
            currentObject = findChild(currentObject, names);
        }

        currentObject = getFirstChild(currentObject);
        String literal = currentObject == null ? null : (String) currentObject.get(JSON_KEY_TEXT);

        RechtsetzungsdokumentForm form = RechtsetzungsdokumentForm.fromLiteral(literal);
        if (!RechtsetzungsdokumentArt.REGELUNGSTEXT.getLiteral().equals(docName)) {
            form = RechtsetzungsdokumentForm.STAMMFORM;
        }

        return form;
    }


    private Bezeichner getOlLiBezeichner(JSONObject jsonObject, JSONContext jsonContext, String literal) {
        Bezeichner bezeichner = null;
        String value = (String) jsonObject.get(JSON_KEY_VALUE);
        if (Utils.isMissing(value)) {
            log.error("list items in LDML.de must have a @value");
        }

        Matcher m = PTN_LI_VALUE.matcher(value.trim().toLowerCase());
        if (m.matches()) {
            String pos = m.group(1);
            bezeichner = BezeichnerImpl.newInstance(literal, pos);
        } else {
            log.error("unrecognized format of list item @value: " + value);
        }

        jsonContext.decreaseList();
        return bezeichner;
    }

}
