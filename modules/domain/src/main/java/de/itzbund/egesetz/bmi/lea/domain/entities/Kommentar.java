// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.entities;

import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * A DDD entity representing the set of all comments of a document.
 */
@Getter
@Setter
@SuperBuilder
public class Kommentar extends Reply {
    // Benutzt die Felder fuer Kommentar, Dokument und Antwort
}
