// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.session;

import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

/**
 * holds reference to the current user to avoid database access
 */
@Component
@SessionScope
@Log4j2
public class UserHolder {

    private final AtomicReference<User> currentUser = new AtomicReference<>();


    protected Optional<User> validate(final UserId gid) {
        User userEntity = currentUser.get();
        if (userEntity != null && StringUtils.isNotBlank(gid.getId())) {
            log.trace("will check principal's gid '{}' against current user's gid '{}'...", gid, userEntity.getGid());
            if (StringUtils.equals(gid.getId(), userEntity.getGid().getId())) {
                return Optional.of(userEntity);
            }
        }
        return Optional.empty();
    }


    protected void set(User userEntity) {
        currentUser.set(userEntity);
    }
}
