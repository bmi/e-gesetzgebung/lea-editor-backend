// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Klassifizierung bzw. Art der Freigabe: Gibt an, ob der aktuelle User als Teilnehmer einer Abstimmung auf die Dokumentenmappe zugreifen darf oder als
 * Einleiter einer Abstimmung die Mappe als Antwort des Teilnehmers sehen darf
 */
@SuppressWarnings("unused")
public enum CompoundDocumentAccessType {

    EINLADUNG_AN_TEILNEHMER("Einladung an Teilnehmer"),
    ANTWORT_AN_EINLEITER("Antwort an Einleiter");

    private final String value;


    CompoundDocumentAccessType(String value) {
        this.value = value;
    }


    @JsonCreator
    public static CompoundDocumentAccessType fromValue(String text) {
        for (CompoundDocumentAccessType b : CompoundDocumentAccessType.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        return null;
    }


    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

}
