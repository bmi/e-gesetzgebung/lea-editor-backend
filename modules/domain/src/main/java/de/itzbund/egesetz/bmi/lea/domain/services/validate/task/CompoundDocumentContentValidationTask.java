// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.task;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentForm;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.ValidationResult;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.CompoundDocumentValidationResult;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationData;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationException;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Log4j2
@Component
// to avoid reusing this class
@Scope("prototype")
public class CompoundDocumentContentValidationTask extends AbstractValidationTask
    implements CompoundDocumentValidationTask {

    private static final String TASK_DESC = "validates the content inside a compound document";
    private CompoundDocument compoundDocument;
    private List<Document> documents;
    private DocumentType newDocumentType;
    private ValidationResult result = CompoundDocumentValidationResult.VALIDATION_NOT_PERFORMED;


    @Override
    public String getDescription() {
        return TASK_DESC;
    }


    @Override
    public void setInputSource(CompoundDocument compoundDocument, DocumentType newDocumentType) {
        this.compoundDocument = compoundDocument;
        this.documents = compoundDocument.getDocuments();
        this.newDocumentType = newDocumentType;
    }


    @Override
    public ValidationResult run(ValidationData data) throws ValidationException {
        if ((documents == null) || (newDocumentType == null)) {
            throw new ValidationException("Parameters not initialized");
        }

        result = getCompoundDocumentValidationResult();
        return result;
    }


    private CompoundDocumentValidationResult getCompoundDocumentValidationResult() {
        if (compoundDocument.isWriteProtected()) {
            return CompoundDocumentValidationResult.COMPOUND_DOCUMENT_IS_PROTECTED;

        } else if (!isOfSuitableType(compoundDocument.getCompoundDocumentEntity().getType(), newDocumentType)) {
            return CompoundDocumentValidationResult.INVALID_DOCUMENT_TYPE;
        } else if (newDocumentType != DocumentType.ANLAGE) {
            Optional<Document> first = documents.stream()
                .filter(document -> newDocumentType.equals(document.getDocumentTyp())).findFirst();
            if (first.isPresent()) {
                return CompoundDocumentValidationResult.TO_MUCH_DOCUMENTS_OF_GIVEN_TYPE;
            }
        }

        return CompoundDocumentValidationResult.VALIDATION_SUCCESS;
    }


    @SuppressWarnings("java:S1541")
    private boolean isOfSuitableType(CompoundDocumentTypeVariant compoundDocumentType, DocumentType documentType) {
        if (documentType == DocumentType.ANLAGE) {
            return true;
        }

        RechtsetzungsdokumentTyp typ = documentType.getTyp();
        RechtsetzungsdokumentForm form = documentType.getForm();

        switch (compoundDocumentType) {
            case AENDERUNGSVERORDNUNG:
                return form == RechtsetzungsdokumentForm.MANTELFORM && typ == RechtsetzungsdokumentTyp.VERORDNUNG;
            case MANTELGESETZ:
                return form == RechtsetzungsdokumentForm.MANTELFORM && typ == RechtsetzungsdokumentTyp.GESETZ;
            case STAMMGESETZ:
                return form == RechtsetzungsdokumentForm.STAMMFORM && typ == RechtsetzungsdokumentTyp.GESETZ;
            case STAMMVERORDNUNG:
                return form == RechtsetzungsdokumentForm.STAMMFORM && typ == RechtsetzungsdokumentTyp.VERORDNUNG;
            default:
                return false;
        }
    }


    @Override
    public ValidationResult getResult() {
        return result;
    }

}
