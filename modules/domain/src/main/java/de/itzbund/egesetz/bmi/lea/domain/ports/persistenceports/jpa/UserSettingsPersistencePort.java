// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa;

import java.util.List;
import java.util.Optional;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.UserSettings;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;

@SuppressWarnings("unused")
public interface UserSettingsPersistencePort {

	UserSettings save(UserSettings userSettings, List<ServiceState> status);

	Optional<UserSettings> get(User user);

	boolean existsUserSettingsByUser(UserId userId);
}
