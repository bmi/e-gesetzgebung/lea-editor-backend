// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.synopse;

public class SynopsisConstants {

    public static final int MAX_DOCS_SUPPORTED = 2;

    public static final String ERR_MSG_DTO_MISSING = "no comparison results delivered";

    public static final String ERR_MSG_DOCS_MISSING = "comparison result has no docs included";

    public static final String ERR_MSG_DOCS_COUNT =
        String.format("the number of resulting documents from comparison should be %s, but was {0}", MAX_DOCS_SUPPORTED);

    public static final String ERR_MSG_DOCS_NO_CONTENT = "at least one of the documents has no content";

    public static final String ERR_MSG_CONTENT_NOT_PARSABLE = "content could not be read as valid JSON";

    public static final String ERR_MSG_TEMPLATE_NOT_FOUND = "the HTML template could not be found";

    private SynopsisConstants() {
    }

}
