// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.eli;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_DATE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_HREF;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_LANGUAGE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_NAME;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_VALUE;

@Getter
@RequiredArgsConstructor
public enum FRBRAttribute {

    FRBR_DATE(ATTR_DATE),
    FRBR_HREF(ATTR_HREF),
    FRBR_LANG(ATTR_LANGUAGE),
    FRBR_NAME(ATTR_NAME),
    FRBR_VALUE(ATTR_VALUE);

    final String ldmlName;

}
