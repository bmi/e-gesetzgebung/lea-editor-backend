// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde;

import de.itzbund.egesetz.bmi.lea.domain.model.ldmlde.LDMLDeModellElementBezeichnung;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum BegruendungsteilRefersToLiteral implements RefersToLiteral {

    BEGRUENDUNG_ALLGEMEINER_TEIL("begruendung-allgemeiner-teil"),
    BEGRUENDUNG_BESONDERER_TEIL("begruendung-besonderer-teil");

    private static final Map<String, BegruendungsteilRefersToLiteral> literals;
    static {
        literals = new HashMap<>();
        Arrays.stream(values()).forEach(v -> literals.put(v.getLiteral(), v));
    }

    private final String literal;


    @Override
    public LDMLDeModellElementBezeichnung getContext() {
        return LDMLDeModellElementBezeichnung.BEGRUENDUNGSTEIL;
    }


    public static BegruendungsteilRefersToLiteral fromLiteral(String literal) {
        return literals.get(literal);
    }

}
