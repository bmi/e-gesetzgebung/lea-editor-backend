// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * indicates different types of Regelungsvorhaben
 */
@Schema(enumAsRef = true)
@AllArgsConstructor
@Getter
public enum RechtsetzungsdokumentTyp {

    GESETZ("Gesetz", "gesetz"),
    SATZUNG("Satzung", "satzung"),
    SONSTIGE_BEKANNTMACHUNG("Sonstige Bekanntmachung", "sonstige-bekanntmachung"),
    SYNOPSE("Synopse", "synopse"),
    VERORDNUNG("Verordnung", "verordnung"),
    VERTRAGSGESETZ("Vertragsgesetz", "vertragsgesetz"),
    VERTRAGSVERORDNUNG("Vertragsverordnung", "vertragsverordnung"),
    VERWALTUNGSVORSCHRIFT("Verwaltungsvorschrift", "verwaltungsvorschrift"),
    EXTERNES_DOKUMENT("Externes Dokument", "externes-dokument");

    private static final Map<String, RechtsetzungsdokumentTyp> TYP_LITERALS = new HashMap<>();

    static {
        Arrays.stream(values()).forEach(typ -> TYP_LITERALS.put(typ.getLiteral(), typ));
    }

    private final String label;
    private final String literal;


    public static RechtsetzungsdokumentTyp fromLiteral(String literal) {
        if (Utils.isMissing(literal)) {
            return null;
        }

        return TYP_LITERALS.get(literal);
    }

}
