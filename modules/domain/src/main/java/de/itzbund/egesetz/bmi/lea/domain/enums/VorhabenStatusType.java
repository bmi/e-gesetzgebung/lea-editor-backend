// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Copied from PLATEG: plateg-backend/src/main/java/de/itzbund/egesetz/bmi/plateg/enums/regelungsvorhaben indicates different types of Vorhabenstatus
 */
@Schema(enumAsRef = true)
public enum VorhabenStatusType {

    // zuletzt geprüft: 14.05.2024
    ARCHIVIERT("Archiviert"),
    IN_BEARBEITUNG("In Bearbeitung"),
    ENTWURF("Entwurf"),
    BUNDESTAG("An Bundestag weitergeleitet");

    public final String label;
    
    VorhabenStatusType(String label) {
        this.label = label;
    }

}
