// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa;

import java.util.List;
import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;

public interface CommentPersistencePort {

	Comment save(Comment comment);

	boolean existsCommentByCommentId(CommentId commentId);

	boolean existsCommentsCreatedOrUpdatedByUser(UserId userId);

	List<Comment> findByDocumentId(DocumentId documentId);

	List<Comment> findByDocumentIdAndDeleted(DocumentId documentId, boolean b);

	List<Comment> findByDocumentIdAndCreatedByAndDeleted(DocumentId documentId, User currentUser, boolean b);

	Comment findByCommentId(CommentId commentId);

	List<Comment> findByDocumentIdAndCreatedBy(DocumentId documentId, User user);
}
