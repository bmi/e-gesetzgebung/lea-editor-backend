// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde;

import de.itzbund.egesetz.bmi.lea.domain.model.ldmlde.LDMLDeModellElementBezeichnung;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum BegruendungsteilAbschnittRefersToLiteral implements RefersToLiteral {

    BEGRUENDUNGSTEIL_ABSCHNITT_ZIELSETZUNG_UND_NOTWENDIGKEIT("begruendungsabschnitt-zielsetzung-und-notwendigkeit"),
    BEGRUENDUNGSTEIL_ABSCHNITT_WESENTLICHER_INHALT("begruendungsabschnitt-wesentlicher-inhalt"),
    BEGRUENDUNGSTEIL_ABSCHNITT_ALTERNATIVEN("begruendungsabschnitt-alternativen"),
    BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSKOMPETENZ("begruendungsabschnitt-regelungskompetenz"),
    BEGRUENDUNGSTEIL_ABSCHNITT_VEREINBARKEIT("begruendungsabschnitt-vereinbarkeit"),
    BEGRUENDUNGSTEIL_ABSCHNITT_BEFRISTUNG_EVALUIERUNG("begruendungsabschnitt-befristung-evaluierung"),
    BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN("begruendungsabschnitt-regelungsfolgen"),
    BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSTEXT("begruendungsabschnitt-regelungstext");

    private static final Map<String, BegruendungsteilAbschnittRefersToLiteral> literals;
    static {
        literals = new HashMap<>();
        Arrays.stream(values()).forEach(v -> literals.put(v.getLiteral(), v));
    }

    private final String literal;


    @Override
    public LDMLDeModellElementBezeichnung getContext() {
        return LDMLDeModellElementBezeichnung.BEGRUENDUNGSTEILABSCHNITT;
    }


    public static BegruendungsteilAbschnittRefersToLiteral fromLiteral(String literal) {
        return literals.get(literal);
    }

}
