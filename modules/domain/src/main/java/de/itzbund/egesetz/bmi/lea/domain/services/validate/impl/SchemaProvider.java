// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.impl;

import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.xerces.jaxp.validation.XMLSchemaFactory;
import org.springframework.stereotype.Component;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import de.itzbund.egesetz.bmi.lea.core.Constants;
import de.itzbund.egesetz.bmi.lea.core.util.ApplicationRuntimeException;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import de.itzbund.egesetz.bmi.lea.core.xml.XmlUtils;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.SchemaInstanceName;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

/**
 * A schema provider registers certain schemas under some {@link SchemaInstanceName} and provides them by request as precompiled objects. It works like a
 * {@link Schema} cache.
 */

@Log4j2
@Component
public class SchemaProvider {

    private static final String XML_SCHEMA_LOCATION = "/schemas/xml.xsd";
    private static final String BAUKASTEN_SCHEMA_LOCATION = "/schemas/akn4de/legalDocML.de-1.6/legalDocML.de-baukasten.xsd";

    private final Map<SchemaInstanceName, Schema> schemas = new HashMap<>();

    private enum SchemaSourceType {
        DOC, BAUKASTEN, META_BREG, META_BT, XML
    }


    public SchemaProvider() {
        setup();
    }


    // String location is the content of attribute xsi:schemaLocation which might contain several pairs of URIs.
    // Here we need to find the location for AKN namespace.
    // Hint: later, when TP Inhaltsdaten defined virtual URIs as system IDs for the various schemas, then make use of
    // de.itzbund.egesetz.lea.model.xml.SchemaLocations.fromString(String)
    private static String getFilename(String location) {
        if (location == null || location.trim().isBlank()) {
            return "";
        } else {
            int pos = location.indexOf(Constants.TARGET_NS);
            String[] parts = location.substring(pos).split("\\s+");
            if (parts.length > 1) {
                return parts[1].substring(parts[1].lastIndexOf('/') + 1);
            } else {
                log.error("malformed schema location: {}", location);
                return "";
            }
        }
    }


    private static List<String> getFilenames(String schemaLocation) {
        List<String> resultList = new ArrayList<>();
        List<Pair<String, String>> locationMappings = XmlUtils.getNamespaceMappings(schemaLocation);

        if (!locationMappings.isEmpty()) {
            locationMappings.forEach(p -> resultList.add(p.getRight()));
        }

        return resultList;
    }


    private void setup() {
        try {
            SchemaFactory sf = getSchemaFactory(null);
            for (SchemaInstance schemaInstance : SchemaInstance.values()) {
                schemas.put(schemaInstance,
                    sf.newSchema(SchemaProvider.class.getResource(schemaInstance.getSchemaFilePath())));
            }
        } catch (Exception e) {
            log.fatal("error while instantiating schema objects", e);
        }
    }


    /**
     * Returns the {@link Schema} registered under the given {@link SchemaInstanceName}.
     *
     * @param instance the name this schema is registered in this schema provider.
     * @return the schema registered by the given name
     */
    public Schema getSchema(SchemaInstanceName instance) {
        return schemas.get(instance);
    }


    /**
     * Returns the {@link Schema} instance associated with the XML content within the given {@link InputSource} when it is some kind of LegalDocML.
     *
     * @param xmlSource an XML document
     * @return the associated schema
     */
    public Schema getSchema(ReusableInputSource xmlSource) {
        SchemaInstanceName schemaInstanceName = null;
        String schemaLocation = null;

        try {
            schemaLocation = xmlSource.getExtractor().getXsiSchemaLocation();

            if (!schemaLocation.isBlank()) {
                schemaInstanceName = SchemaInstance.getInstanceByPath(schemaLocation);
            }

        } catch (RuntimeException e) {
            log.error("could not parse xml source", e);
            String xmlContent = Utils.getContent(xmlSource);
            log.debug("xmlSource: {}", xmlContent);
        }

        if (schemaInstanceName == null) {
            log.error("received a document not recognizable as LegalDocML; schema location: {}", schemaLocation);
            return null;
        }

        Source[] schemaSources = getRelatedSchemas(schemaLocation);
        SchemaFactory schemaFactory = getSchemaFactory(null);
        schemaFactory.setResourceResolver(new ClasspathResourceResolver());
        Schema schema;

        try {
            schema = schemaFactory.newSchema(schemaSources);
            return schema;
        } catch (SAXException e) {
            log.error(e);
            throw new ApplicationRuntimeException("XML Validation: Schema could not be initialized");
        }
    }


    private Source[] getRelatedSchemas(String schemaLocation) {
        List<String> files = getFilenames(schemaLocation);
        Map<SchemaSourceType, String> sourceCollection = new HashMap<>();
        List<Source> sources = new ArrayList<>();

        sourceCollection.put(SchemaSourceType.XML, XML_SCHEMA_LOCATION);
        sourceCollection.put(SchemaSourceType.BAUKASTEN, BAUKASTEN_SCHEMA_LOCATION);

        files.forEach(file -> {
            String fileName = file.substring(file.lastIndexOf('/') + 1);
            SchemaInstance schemaInstance = SchemaInstance.getInstanceByName(fileName);
            SchemaSourceType schemaSourceType = SchemaSourceType.DOC;
            if (fileName.endsWith("metadaten.xsd")) {
                schemaSourceType = SchemaSourceType.META_BREG;
            } else if (fileName.endsWith("metadaten-bundestag.xsd")) {
                schemaSourceType = SchemaSourceType.META_BT;
            }
            sourceCollection.put(schemaSourceType, schemaInstance.getSchemaFilePath());
        });

        addSourceFor(sourceCollection.get(SchemaSourceType.DOC), sources);
        addSourceFor(sourceCollection.get(SchemaSourceType.META_BREG), sources);
        addSourceFor(sourceCollection.get(SchemaSourceType.META_BT), sources);

        return sources.toArray(new Source[0]);
    }


    private void addSourceFor(String resourcePath, List<Source> sources) {
        if (!Utils.isMissing(resourcePath)) {
            InputStream in = SchemaProvider.class.getResourceAsStream(resourcePath);
            StreamSource src = new StreamSource(in);
            src.setSystemId(resourcePath);
            sources.add(src);
        }
    }


    /**
     * Registers a precompiled XML schema under the given name.
     *
     * @param instance the name this schema is associated with
     * @param schema   the {@link Schema} object
     */
    public void registerSchema(SchemaInstanceName instance, Schema schema) {
        schemas.put(instance, schema);
    }


    /**
     * Registers an XML schema located at {@code filepath} under the given {@link SchemaInstanceName}. The second parameter defines the schema language.
     *
     * @param instance   the name this schema is associated with
     * @param schemaLang one of {@link XMLConstants#W3C_XML_SCHEMA_NS_URI} or {@link XMLConstants#RELAXNG_NS_URI}
     * @param filepath   location of the schema file
     */
    public void registerSchema(SchemaInstanceName instance, String schemaLang, String filepath) {
        SchemaFactory sf = getSchemaFactory(schemaLang);
        Schema schema = null;
        try {
            schema = sf.newSchema(SchemaProvider.class.getResource(filepath));
        } catch (SAXException e) {
            log.error("could not instantiate schema for file path: {}", filepath, e);
        }
        if (schema != null) {
            schemas.put(instance, schema);
        }
    }


    /**
     * Extendable Enum for name constants of precompiled XML schemas.
     */
    @Getter
    @RequiredArgsConstructor
    public enum SchemaInstance implements SchemaInstanceName {
        OASIS_LEGAL_DOC_ML("/schemas/oasis/akomantoso30.xsd"),
        LEGAL_DOC_ML_DE_ANSCHREIBEN("/schemas/akn4de/legalDocML.de-1.6/legalDocML.de-anschreiben.xsd"),
        LEGAL_DOC_ML_DE_BEGRUENDUNG("/schemas/akn4de/legalDocML.de-1.6/legalDocML.de-begruendung.xsd"),
        LEGAL_DOC_ML_DE_BEKANNTMACHUNG("/schemas/akn4de/legalDocML.de-1.6/legalDocML.de-bekanntmachung.xsd"),
        LEGAL_DOC_ML_DE_DENKSCHRIFT("/schemas/akn4de/legalDocML.de-1.6/legalDocML.de-denkschrift.xsd"),
        LEGAL_DOC_ML_DE_METADATEN("/schemas/akn4de/legalDocML.de-1.6/legalDocML.de-metadaten.xsd"),
        LEGAL_DOC_ML_DE_METADATEN_BT("/schemas/akn4de/legalDocML.de-1.6/legalDocML.de-metadaten-bundestag.xsd"),
        LEGAL_DOC_ML_DE_OFFENE_STRUKTUR("/schemas/akn4de/legalDocML.de-1.6/legalDocML.de-offenestruktur.xsd"),
        LEGAL_DOC_ML_DE_RECHTSSETZUNGSDOKUMENT("/schemas/akn4de/legalDocML.de-1.6/legalDocML.de-rechtsetzungsdokument.xsd"),
        LEGAL_DOC_ML_DE_REGELUNGSTEXT_ENTWURF("/schemas/akn4de/legalDocML.de-1.6/legalDocML.de-regelungstextentwurfsfassung.xsd"),
        LEGAL_DOC_ML_DE_REGELUNGSTEXT_VERKUENDUNG("/schemas/akn4de/legalDocML.de-1.6/legalDocML.de-regelungstextverkuendungsfassung.xsd"),
        LEGAL_DOC_ML_DE_VEREINBARUNG("/schemas/akn4de/legalDocML.de-1.6/legalDocML.de-vereinbarung.xsd"),
        LEGAL_DOC_ML_DE_VORBLATT_RT("/schemas/akn4de/legalDocML.de-1.6/legalDocML.de-vorblattregelungstext.xsd");

        private static final Map<String, SchemaInstance> instancesByFilePath = new HashMap<>();
        private static final Map<String, SchemaInstance> instancesByFileName = new HashMap<>();

        static {
            for (SchemaInstance instance : values()) {
                String filePath = instance.getSchemaFilePath();
                filePath = filePath.substring(filePath.lastIndexOf('/') + 1);
                instancesByFilePath.put(filePath, instance);

                String fileName = filePath.substring(filePath.lastIndexOf('/') + 1);
                instancesByFileName.put(fileName, instance);
            }
        }

        @NonNull
        private final String schemaFilePath;


        /**
         * Returns the name constant of an XML schema associated to the given schema location URI.
         *
         * @param location xsi:schemaLocation URI
         * @return the associated name constant
         */
        public static SchemaInstance getInstanceByPath(String location) {
            return instancesByFilePath.get(getFilename(location));
        }

        public static SchemaInstance getInstanceByName(String filename) {
            return instancesByFileName.get(filename);
        }
    }


    private static SchemaFactory getSchemaFactory(String schemaLanguage) {
        SchemaFactory schemaFactory;

        if (Utils.isMissing(schemaLanguage)) {
            schemaFactory = new XMLSchemaFactory();
        } else {
            schemaFactory = SchemaFactory.newInstance(schemaLanguage,
                "org.apache.xerces.jaxp.validation.XMLSchemaFactory",
                ClassLoader.getSystemClassLoader());
        }

        try {
            schemaFactory.setProperty(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            schemaFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, false);
            schemaFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, false);
        } catch (SAXNotRecognizedException | SAXNotSupportedException e) {
            log.debug(e);
        }

        return schemaFactory;
    }


    // Custom ResourceResolver to resolve schema includes and imports from the classpath
    static class ClasspathResourceResolver implements LSResourceResolver {

        @Override
        public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId, String baseURI) {
            // Use ClassLoader to resolve the included/imported schema by its systemId
            InputStream resourceAsStream = null;
            if (systemId.endsWith("xml.xsd")) {
                resourceAsStream = SchemaProvider.class.getResourceAsStream(XML_SCHEMA_LOCATION);
            } else if (systemId.endsWith("baukasten.xsd")) {
                resourceAsStream = SchemaProvider.class.getResourceAsStream(BAUKASTEN_SCHEMA_LOCATION);
            }

            if (resourceAsStream != null) {
                return new Input(resourceAsStream, systemId, publicId, baseURI);
            }

            // Return null if schema is not found, which will cause an error
            return null;
        }
    }

    // Helper class to represent the schema input
    @Data
    static class Input implements LSInput {

        private InputStream byteStream;
        private String systemId;
        private String publicId;
        private String baseURI;
        private String stringData;
        private Reader characterStream;
        private String encoding;
        private boolean certifiedText;

        public Input(InputStream byteStream, String systemId, String publicId, String baseURI) {
            this.byteStream = byteStream;
            this.systemId = systemId;
            this.publicId = publicId;
            this.baseURI = baseURI;
        }

        @Override
        public boolean getCertifiedText() {
            return this.certifiedText;
        }
    }

}

