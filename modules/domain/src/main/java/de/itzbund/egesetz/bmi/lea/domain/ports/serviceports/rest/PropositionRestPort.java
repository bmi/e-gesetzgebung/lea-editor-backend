// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest;

import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.DokumentenmappeTableDTOs;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.HomepageRegulatoryProposalDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.RegelungsvorhabengTableDTOs;
import de.itzbund.egesetz.bmi.lea.domain.dtos.paging.PaginierungDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PropositionRestPort {

	List<PropositionDTO> getAllPropositionDescriptionsByUser();

	List<HomepageRegulatoryProposalDTO> getRegulatoryProposals(
		List<ServiceState> status);

	RegelungsvorhabengTableDTOs getStartseiteMeineDokumente(List<ServiceState> status, Optional<PaginierungDTO> paginierungDTO);

	List<HomepageRegulatoryProposalDTO> getStartseiteDokumenteAusAbstimmungen(List<ServiceState> status);

	DokumentenmappeTableDTOs getVersionshistorie(List<ServiceState> status, UUID regulatoryProposalId, Optional<PaginierungDTO> paginierungDTO);

	void bestandsrechtDatenSpeicherung(String gid, UUID regelungsvorhabenId, BestandsrechtDTO bestandsrechtDTO, List<ServiceState> status);

}
