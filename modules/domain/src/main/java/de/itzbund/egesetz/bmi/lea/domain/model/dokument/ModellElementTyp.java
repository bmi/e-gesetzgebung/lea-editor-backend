// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model.dokument;

import lombok.AllArgsConstructor;
import lombok.Getter;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_COLLECTIONBODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_COMPONENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DOCUMENTREF;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_MOD;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.withDefaultPrefix;

@Getter
@AllArgsConstructor
public enum ModellElementTyp {

    RECHTSETZUNGSDOKUMENT_HAUPTTEIL(withDefaultPrefix(ELEM_COLLECTIONBODY)),
    TEILDOKUMENT_VERWEIS(withDefaultPrefix(ELEM_DOCUMENTREF)),
    TEILDOKUMENT_VERWEIS_CONTAINER(withDefaultPrefix(ELEM_COMPONENT)),
    AENDERUNGSBEFEHL(withDefaultPrefix(ELEM_MOD));

    private final String xmlType;

}
