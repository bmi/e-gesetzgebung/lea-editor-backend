// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.bestand;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.Valid;
import java.util.UUID;

@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
public class CreateBestandsrechtDTO {

    // Until working with NeuRis: this is the id of the regulatory proposal
    @Valid
    @JsonProperty("regelungsvorhabenId")
    @Schema(name = "regelungsvorhabenId", example = "00000000-0000-0000-0003-000000000005", required = true)
    private UUID regelungsvorhabenId;

    // Until working with NeuRis: this is the id of the compound document
    @Valid
    @JsonProperty("dokumentenmappenId")
    @Schema(name = "dokumentenmappenId", example = "00000000-0000-0000-0003-000000000005", required = true)
    private UUID dokumentenmappenId;

    // Until working with NeuRis: this is the title of the regulatory text
    @Valid
    @JsonProperty("kurzTitel")
    @Schema(name = "kurzTitel", required = true)
    private String kurzTitel;

    // Until working with NeuRis: this is the title of the compound document
    @Valid
    @JsonProperty("langTitel")
    @Schema(name = "langTitel", required = true)
    private String langTitel;

}
