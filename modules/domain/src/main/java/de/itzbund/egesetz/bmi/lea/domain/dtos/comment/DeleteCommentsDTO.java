// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.comment;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.UpdateDocumentDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@SuperBuilder
@RequiredArgsConstructor
public class DeleteCommentsDTO extends UpdateDocumentDTO {

	@JsonProperty("commentIds")
	@Schema(name = "commentIds")
	private List<String> commentIds;

}
