// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.CreateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.AuxiliaryRestPort;
import de.itzbund.egesetz.bmi.lea.domain.services.eli.EliMetadatenUtil;
import de.itzbund.egesetz.bmi.lea.domain.services.export.ExportContext;
import de.itzbund.egesetz.bmi.lea.domain.services.filters.EliMetaDataFilter;
import de.itzbund.egesetz.bmi.lea.domain.services.filters.FilterChain;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.core.Constants.INSTANT_TO_DATE_FORMATTER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_TEXT_WRAPPER;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.jsonTreeToFlatList;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDocumentObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;

@Service
@Log4j2
@SuppressWarnings("unused")
public class AuxiliaryService implements AuxiliaryRestPort {

    private static final JSONParser JSON_PARSER = new JSONParser();

    // ----- Repositories -----
    @Autowired
    private DokumentPersistencePort documentRepository;

    @Autowired
    private CompoundDocumentPersistencePort compoundDocumentRepository;
    @Autowired
    private PlategRequestService plategRequestService;

    // ----- Services ---------
    @Autowired
    private TemplateService templateService;
    // ----- Other ------------
    @Autowired
    private PropositionMapper propositionMapper;
    @Autowired
    private LeageSession session;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public Document makeDocument(CompoundDocumentId compoundDocumentId, CreateDocumentDTO createDocumentDTO) {
        // get the entity regarding the current user
        User userEntity = session.getUser();
        Pair<PropositionDTO, CompoundDocumentDomain> pair = getPropositionAndCompoundDoc(compoundDocumentId);

        return makeDocumentForUser(pair.getRight(), createDocumentDTO, userEntity, pair.getLeft());
    }


    @Override
    public Document makeDocument(CompoundDocumentId compoundDocumentId, CreateDocumentDTO createDocumentDTO, PropositionDTO propositionDTO) {
        // get the entity regarding the current user
        User userEntity = session.getUser();
        CompoundDocumentDomain compoundDocument = getCompoundDocument(compoundDocumentId);

        return makeDocumentForUser(compoundDocument, createDocumentDTO, userEntity, propositionDTO);
    }


    // by CompoundDocumentController and THIS
    @Override
    @SuppressWarnings("unchecked")
    public Document makeDocumentForUser(CompoundDocumentDomain compoundDocument, CreateDocumentDTO createDocumentDTO, User userEntity,
        PropositionDTO propositionDTO) {
        Instant creationDate = Instant.now();
        DocumentDomain documentEntity = DocumentDomain.builder()
            .documentId(new DocumentId(UUID.randomUUID()))
            .title(createDocumentDTO.getTitle())
            .type(createDocumentDTO.getType())
            .createdBy(userEntity)
            .updatedBy(userEntity)
            .createdAt(creationDate)
            .updatedAt(creationDate)
            .build();

        // load the required template and store it as content
        String template = templateService.loadTemplate(createDocumentDTO.getType(),
            createDocumentDTO.getInitialNumberOfLevels());

        if ((propositionDTO != null) && (template != null)) {
            documentEntity.setContent(template);
            String federfuehrer = propositionDTO.getProponentDTO().getDesignationNominative();
            JSONObject documentObject = getDocumentObject(template);

            FilterChain chain = new FilterChain();
            // Generate EliMetaData
            Proposition proposition = propositionMapper.mapDto(propositionDTO);
            ExportContext exportContext = new ExportContext(
                documentRepository, proposition, compoundDocument, documentEntity
            );
            EliMetaDataFilter eliMetaDataFilter = new EliMetaDataFilter(EliMetadatenUtil.collectEliMetaDaten(exportContext));
            eliMetaDataFilter.setDateValue(INSTANT_TO_DATE_FORMATTER.format(creationDate));
            eliMetaDataFilter.setFederfuehrer(federfuehrer);
            chain.addFilter(eliMetaDataFilter);

            chain.doFilter(documentObject);

            JSONArray wrapper = new JSONArray();
            wrapper.add(documentObject);
            template = wrapper.toJSONString();
        }

        if ((userEntity == null) || (template == null) || (template.isEmpty())) {
            return null;
        }

        documentEntity.setContent(template);

        Document document = applicationContext.getBean(Document.class);
        document.setDocumentEntity(documentEntity);

        return document;
    }


    private CompoundDocumentDomain getCompoundDocument(CompoundDocumentId compoundDocumentId) {
        return compoundDocumentRepository.findByCompoundDocumentId(compoundDocumentId).orElse(null);
    }


    private Pair<PropositionDTO, CompoundDocumentDomain> getPropositionAndCompoundDoc(CompoundDocumentId compoundDocumentId) {
        CompoundDocumentDomain compoundDocument = getCompoundDocument(compoundDocumentId);

        UUID rvId = compoundDocument.getRegelungsVorhabenId().getId();
        RegelungsvorhabenEditorDTO regelungsvorhaben = plategRequestService.getRegulatoryProposalById(rvId);
        PropositionDTO proposition = propositionMapper.map(regelungsvorhaben);

        return Pair.of(proposition, compoundDocument);
    }


    @Override
    @SuppressWarnings("unchecked")
    public String appendEmptyTextNodes(String jsonContent) {
        JSONObject document = getDocumentObject(jsonContent);
        if (document != null) {
            List<JSONObject> textualElements = jsonTreeToFlatList(document);
            textualElements.forEach(e -> {
                if (!hasTextAsLastChild(e)) {
                    addText(e, "");
                }
            });
        }

        JSONArray wrapper = new JSONArray();
        wrapper.add(document);
        return wrapper.toJSONString();
    }


    private boolean hasTextAsLastChild(JSONObject jsonObject) {
        JSONArray children = getChildren(jsonObject);
        if (children != null && !children.isEmpty()) {
            JSONObject lastChild = (JSONObject) children.get(children.size() - 1);
            return JSON_VAL_TEXT_WRAPPER.equals(getType(lastChild));
        } else {
            return false;
        }
    }

}
