// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.task;

import de.itzbund.egesetz.bmi.lea.domain.services.validate.ValidationResult;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationData;

import java.util.Set;

/**
 * This is a piece of validation work. {@link ValidationTask}s can be assembled into a validation workflow which is run by the
 * {@link de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.Validator}.
 */
public interface ValidationTask {

    /**
     * Each kind of {@link ValidationTask}, i.e. the implementing and instantiable class, has a certain name.
     *
     * @return the name of the task (class)
     */
    String getName();

    void setName(String name);

    /**
     * Each kind of {@link ValidationTask}, i.e. the implementing and instantiable class, has a description, shortly stating its purpose or inner working.
     *
     * @return the description of the task (class)
     */
    String getDescription();

    /**
     * Each task object has its own id, making it possible to track it in the validation workflow.
     *
     * @return the id of this task (object)
     */
    String getId();

    /**
     * Calling this method will run the validation task defined by its class and report the validation result.
     *
     * @param data the validation data object to store validation messages
     * @return the result of the validation performed by this task
     */
    @SuppressWarnings("java:S112")
    ValidationResult run(ValidationData data) throws Exception;

    /**
     * Returns the result of the validation
     *
     * @return the validation result
     */
    ValidationResult getResult();

    /**
     * Adds a predecessor task, i.e. a validation task that needs to be performed before this one.
     *
     * @param task the predecessor
     */
    void addPredecessor(ValidationTask task);

    /**
     * Returns the set of predecessors of this task.
     *
     * @return the set of predecessors
     */
    Set<ValidationTask> getPredecessors();

}
