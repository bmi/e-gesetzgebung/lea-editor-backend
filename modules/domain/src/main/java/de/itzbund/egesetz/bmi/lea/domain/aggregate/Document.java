// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.aggregate;

import de.itzbund.egesetz.bmi.lea.core.json.JSONUtils;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.Rights;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.services.CommentService;
import de.itzbund.egesetz.bmi.lea.domain.services.meta.MetadatenUpdateService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * A DDD aggregate representing the whole of a document.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Log4j2
@Getter
@Setter
@AllArgsConstructor
@ToString
public class Document {

	@ToString.Include
	private DocumentDomain documentEntity;

	/**
	 * Derived from compound document
	 */
	private DocumentState documentState;

	private DokumentPersistencePort documentRepository;

	private MetadatenUpdateService metadatenUpdateService;

	private CommentService commentService;

	// Enthält die Zugriffsrechte
	private List<Rights> rechte = new ArrayList<>();

	@Autowired
	public Document(final DokumentPersistencePort dokumentPersistencePort,
		final MetadatenUpdateService metadatenUpdateService, final CommentService commentService) {
		this.documentRepository = dokumentPersistencePort;
		this.metadatenUpdateService = metadatenUpdateService;
		this.commentService = commentService;
	}


	public Document(DocumentDomain documentEntity) {
		this.documentEntity = documentEntity;
	}

	// ---------------- Wrappers --------------------------

	/**
	 * Short version to get document's id and also a wrapper for changing this pattern.
	 *
	 * @return The document's id
	 */
	public DocumentId getDocumentId() {
		if (documentEntity != null) {
			return documentEntity.getDocumentId();
		}

		return null;
	}

	/**
	 * Short version to set document's id and also a wrapper for changing this pattern.
	 *
	 * @param documentId Id of the document
	 */
	public void setDocumentId(DocumentId documentId) {
		if (documentEntity == null) {
			documentEntity = DocumentDomain.builder().build();
		}

		documentEntity.setDocumentId(documentId);
	}

	// ----------------------------------------------------

	public List<Document> getDocumentsForCompoundDocument(CompoundDocumentId compoundDocumentId, DocumentState derivedStateOfCompoundDocument) {

		// Um die Rechte zu setzen, vgl. CompoundDocument::getCompoundDocumentWithDocuments
		log.debug("Get documents from compound document with id: {}", compoundDocumentId);
		documentState = derivedStateOfCompoundDocument;

		List<Document> documentList = new ArrayList<>();
		Optional<List<DocumentDomain>> documentEntities = documentRepository.findByCompoundDocumentId(
			compoundDocumentId);
		log.debug("Found {} documents in repository for compound document with id: {}", documentEntities,
			compoundDocumentId);

		if (documentEntities.isPresent()) {
			List<DocumentDomain> documentEntityList = documentEntities.get();
			documentEntityList.forEach(
				docEntity -> {
					Document document = new Document(documentRepository, metadatenUpdateService, commentService);
					document.setDocumentEntity(docEntity);
					document.setDocumentState(derivedStateOfCompoundDocument);
					documentList.add(document);
				}
			);
		}

		log.debug("Returning following documents: {}", documentList);
		return documentList;
	}


	/**
	 * Adds document to compound document and stores the document in DB
	 *
	 * @param document The document (as aggregate)
	 * @return success or fail
	 */
	public Document addDocumentToCompoundDocument(Document document, CompoundDocument compoundDocument) {
		CompoundDocumentId compoundDocumentId = compoundDocument.getCompoundDocumentId();

		if (compoundDocumentId != null) {
			DocumentDomain documentDomain = document.getDocumentEntity();
			documentDomain.setCompoundDocumentId(compoundDocumentId);
			if (documentRepository != null) {
				DocumentType documentType = documentDomain.getType();
				if (documentType == DocumentType.ANLAGE) {
					documentDomain.setContent(
						adjustMetadata(Utils.unmask(documentDomain.getContent()),
							compoundDocument));
				}

				documentDomain = documentRepository.save(documentDomain);
				document.setDocumentEntity(documentDomain);
				return document;
			} else {
				log.error("Document repository was not available");
			}
		} else {
			log.error("Id of compound document in database was missing.");
		}

		return null;
	}


	@SuppressWarnings("unchecked")
	private String adjustMetadata(String content, CompoundDocument compoundDocument) {
		try {
			JSONObject jsonObject = (JSONObject) ((JSONArray) new JSONParser().parse(content))
				.get(0);
			jsonObject = metadatenUpdateService.update(jsonObject,
				compoundDocument.getCompoundDocumentEntity().getType());
			JSONArray wrapper = new JSONArray();
			wrapper.add(jsonObject);
			return wrapper.toJSONString();
		} catch (ParseException e) {
			log.error(e);
			return content;
		}
	}


	/**
	 * Makes a copy of documents in List and assign them to the given compound document - Hint: This list should be consistent in the way that only the
	 * documents belonging to the compound document should be provided. That is at least guarantied by CompoundDocumentService::copyCompoundDocument.
	 *
	 * @param compoundDocumentId The id of the compound document
	 * @param documents          A list of documents
	 * @return A list of copied documents
	 */
	public List<Document> copyDocuments(CompoundDocumentId compoundDocumentId, List<Document> documents,
		User currentUser) {
		List<Document> copyedDocuments = new ArrayList<>();

		if (documentRepository != null) {
			documents.forEach(
				document -> {
					Optional<DocumentDomain> optionalDocumentEntity = documentRepository.findByDocumentId(
						document.getDocumentEntity().getDocumentId());

					if (optionalDocumentEntity.isPresent()) {
						DocumentDomain documentDomain = optionalDocumentEntity.get();
						copyedDocuments.add(
							new Document(makeDocumentCopy(compoundDocumentId, documentDomain, currentUser)));
					}

				}
			);
		}
		return copyedDocuments;
	}


	private DocumentDomain makeDocumentCopy(CompoundDocumentId compoundDocumentId, DocumentDomain documentEntity,
		User currentUser) {

		JSONObject documentJson = JSONUtils.getDocumentObject(documentEntity.getContent());
		commentService.deleteAllCommentAttributes(documentJson);
		commentService.deleteAllCommentWrappers(documentJson);
		String updatedContent = JSONUtils.wrapUp(documentJson).toJSONString();
		documentEntity.setContent(updatedContent);

		DocumentDomain newDocumentEntity = DocumentDomain.builder()
			.documentId(new DocumentId(UUID.randomUUID()))
			.inheritFromId(documentEntity.getDocumentId())
			.compoundDocumentId(compoundDocumentId)
			.title(documentEntity.getTitle())
			.content(documentEntity.getContent())
			.type(documentEntity.getType())
			.createdBy(currentUser)
			.updatedBy(currentUser)
			.createdAt(Instant.now())
			.updatedAt(Instant.now())
			.build();

		return documentRepository.save(newDocumentEntity);
	}


	public DocumentType getDocumentTyp() {
		return getDocumentEntity().getType();
	}


	public CompoundDocumentId getContainingCompoundDocumentId(DocumentId documentId) {
		Optional<DocumentDomain> document = documentRepository.findByDocumentId(documentId);
		return document.map(DocumentDomain::getCompoundDocumentId)
			.orElse(null);

	}

}
