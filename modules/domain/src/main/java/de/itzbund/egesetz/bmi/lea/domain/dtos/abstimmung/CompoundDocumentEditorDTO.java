// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.abstimmung;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
public class CompoundDocumentEditorDTO {

    /**
     * ID der Dokumentenmappe
     */
    @NotNull
    @JsonProperty("cdId")
    @Valid
    @Schema(name = "cdId", required = true)
    UUID compoundDocumentId;

    /**
     * ID des Regelungsvorhaben
     */
    @NotNull
    @JsonProperty("rvId")
    @Valid
    @Schema(name = "rvId", required = true)
    UUID regelungsvorhabenId;

    /**
     * Abstimmungen zu dieser Dokumentenmappe für den aktuelle Use
     */
    @JsonProperty("abstimmungen")
    @Schema(name = "abstimmungen")
    @Valid
    List<AbstimmungEditorDTO> abstimmungen;

}
