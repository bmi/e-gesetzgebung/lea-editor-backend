// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest;

import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.Drucksache;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;

import java.util.List;

public interface DrucksacheRestPort {

    List<ServiceState> drucksachenNummerAnlegen(RegelungsVorhabenId regelungsvorhabenId, String drucksachenNummer);

    Drucksache getDrucksache(RegelungsVorhabenId regelungsvorhabenId);

}
