// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.BestandsrechtId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class DMBestandsrechtLinkDTO {

    private BestandsrechtId bestandsrechtId;

    private String bestandsrechtTitel;

    @Singular("verlinkteMappe")
    private final List<CompoundDocumentShortDTO> verlinkteMappen;

}
