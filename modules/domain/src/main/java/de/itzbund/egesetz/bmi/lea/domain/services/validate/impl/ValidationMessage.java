// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.impl;

import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.ValidationTask;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * A message considered for information of the user about validation errors regarding some document. It contains a textual message, a {@link SeverityLevel} and
 * is related to a specific kind of validation represented as {@link ValidationTask}.
 */

@Getter
@Component
@Scope("prototype")
@AllArgsConstructor
public class ValidationMessage {

    private final ValidationTask task;
    private final SeverityLevel level;
    private final String message;


    @Override
    public String toString() {
        return String.format("%s: %s (%s)", level, message, task);
    }

}
