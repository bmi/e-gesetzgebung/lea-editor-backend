// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model.dokument;

import de.itzbund.egesetz.bmi.lea.core.Constants;
import de.itzbund.egesetz.bmi.lea.domain.model.version.LegalDocMLDeVersion;
import de.itzbund.egesetz.bmi.lea.domain.ports.eid.EId;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.EIdImpl;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.json.simple.JSONObject;

import java.util.List;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.core.Constants.DEFAULT_UUID;

/**
 * Verweis auf ein Teildokument eines Rechtsetzungsdokuments.
 */
@Getter
@EqualsAndHashCode(callSuper = true)
public class TeildokumentVerweis extends ModellElement {

    private final UUID guid;
    @Setter
    private EId eId;
    @Setter
    private String href;

    @Setter
    private TeildokumentVerweisShowAsLiteral showAsLiteral;


    public TeildokumentVerweis(UUID guid) {
        super(List.of(LegalDocMLDeVersion.LDML_DE_V1_04), ModellElementTyp.TEILDOKUMENT_VERWEIS);
        this.guid = guid;
    }


    public static TeildokumentVerweis createFromJSON(@NonNull JSONObject jsonObject) {
        return (new TeildokumentVerweis(DEFAULT_UUID)).fromJSON(jsonObject);
    }


    @Override
    @SuppressWarnings("java:S1541")
    public TeildokumentVerweis fromJSON(@NonNull JSONObject jsonObject) {
        if (jsonObject.isEmpty() || !typ.getXmlType()
            .equals(jsonObject.get(Constants.JSON_KEY_TYPE))) {
            return null;
        }

        String value = (String) jsonObject.get(Constants.JSON_KEY_ID_GUID);
        UUID guid1 = value == null || value.isBlank() ? null : UUID.fromString(value);
        if (guid1 == null) {
            return null;
        }

        TeildokumentVerweis teildokumentVerweis = new TeildokumentVerweis(guid1);

        value = (String) jsonObject.get(Constants.JSON_KEY_ID_EID);
        EId eId1 = value == null || value.isBlank() ? null : EIdImpl.fromString(value);
        if (eId1 != null) {
            teildokumentVerweis.setEId(eId1);
        }

        value = (String) jsonObject.get(Constants.JSON_KEY_HREF);
        if (value != null) {
            teildokumentVerweis.setHref(value);
        }

        value = (String) jsonObject.get(Constants.JSON_KEY_SHOWAS);
        TeildokumentVerweisShowAsLiteral literal = value == null || value.isBlank()
            ? null
            : TeildokumentVerweisShowAsLiteral.fromString(value);
        if (literal != null) {
            teildokumentVerweis.setShowAsLiteral(literal);
        }

        return teildokumentVerweis;
    }


    @Override
    public JSONObject toJSONObject() {
        JSONObject jsonObject = new JSONObject();
        addJSONAttribute(jsonObject, Constants.JSON_KEY_TYPE, typ.getXmlType());
        addJSONAttribute(jsonObject, Constants.JSON_KEY_ID_GUID, guid.toString());
        addJSONAttribute(jsonObject, Constants.JSON_KEY_ID_EID, eId == null ? null : eId.toString());
        addJSONAttribute(jsonObject, Constants.JSON_KEY_HREF, href == null ? "" : href);
        addJSONAttribute(jsonObject, Constants.JSON_KEY_SHOWAS,
            showAsLiteral == null ? null : showAsLiteral.getLiteral());

        return jsonObject;
    }

}
