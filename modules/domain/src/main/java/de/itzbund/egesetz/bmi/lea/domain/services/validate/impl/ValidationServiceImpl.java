// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.impl;

import com.helger.schematron.ISchematronResource;
import com.helger.schematron.xslt.SchematronResourceXSLT;
import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.ValidationService;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.SchematronValidationTask;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.XmlSchemaValidationTask;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;

import javax.xml.validation.Schema;
import java.io.InputStream;

/**
 * Implementation of a {@link ValidationService}.
 */

@Log4j2
@Service
@SuppressWarnings("unused")
public class ValidationServiceImpl implements ValidationService {

    @Autowired
    private SchemaProvider schemaProvider;

    @Autowired
    private Validator validator;

    @Autowired
    private XmlSchemaValidationTask xmlSchemaValidationTask;

    @Autowired
    private SchematronValidationTask schematronValidationTask;

    @Autowired
    private ValidationData validationData;


    @Override
    public ValidationData validateLDML(InputSource xml) {

        if (xml == null) {
            log.error("no input source defined");
            return null;
        }

        // prepare multiple use of input source and convert to pretty-printed XML
        // pretty-printed XML is necessary for XmlContentLocator to work
        ReusableInputSource ris = new ReusableInputSource(xml.getByteStream());

        // XML Schema validation

        Schema schema = schemaProvider.getSchema(ris);
        xmlSchemaValidationTask.setSchema(schema);
        xmlSchemaValidationTask.setInputSource(ris);
        validator.clearTasks();
        validator.addValidationTask(xmlSchemaValidationTask);

        // Schematron validation

        schematronValidationTask.addPredecessor(xmlSchemaValidationTask);
        InputStream inputStream = ValidationServiceImpl.class.getResourceAsStream("/xslt/legalDocML.de.xsl");
        ISchematronResource ruleset = inputStream == null ? null
            : SchematronResourceXSLT.fromInputStream("", inputStream);
        schematronValidationTask.setInputSource(ris);
        schematronValidationTask.setRuleset(ruleset);
        validator.addValidationTask(schematronValidationTask);

        // execute validation chain

        validator.validate();

        return validator.getValidationData();
    }

}
