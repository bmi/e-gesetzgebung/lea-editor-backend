// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.CreateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;

public interface AuxiliaryRestPort {

    Document makeDocument(CompoundDocumentId compoundDocumentId, CreateDocumentDTO createDocumentDTO);

    Document makeDocument(CompoundDocumentId compoundDocumentId, CreateDocumentDTO createDocumentDTO, PropositionDTO propositionDTO);

    Document makeDocumentForUser(CompoundDocumentDomain compoundDocument, CreateDocumentDTO createDocumentDTO, User userEntity, PropositionDTO propositionDTO);

    String appendEmptyTextNodes(String jsonContent);

}
