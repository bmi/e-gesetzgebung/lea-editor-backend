// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest;

import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.synopsis.SynopsisRequestDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.synopsis.SynopsisResponseDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;

import java.util.List;
import java.util.UUID;

public interface SynopsisRestPort {

    SynopsisResponseDTO getSynopse(SynopsisRequestDTO synopsisRequestDTO, List<ServiceState> status);

    DocumentDTO erzeugeAenderungsbefehle(UUID regulatoryProposalId, UUID bestandsrechtId, UUID regelungstextId, List<ServiceState> status);

}
