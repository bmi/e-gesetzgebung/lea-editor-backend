// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.mappers;

import com.nimbusds.oauth2.sdk.util.MapUtils;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserPersistencePort;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

import java.util.Map;
import java.util.Optional;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true))
@Log4j2
@SuppressWarnings("unused")
public abstract class UserMapper {

    @Autowired
    UserPersistencePort userRepository;


    @Mapping(target = "name", source = "fullName")
    public abstract User map(OidcUser user, @MappingTarget User userEntity);


    public UserDTO map(User user) {
        if (user == null) {
            return null;
        }

        return UserDTO.builder()
            .gid(user.getGid()
                .getId())
            .name(user.getName())
            .email(user.getEmail())
            .build();
    }


    public UserId getGid(OidcUser user) {
        String gid = Optional.ofNullable(user)
            .map(u -> getUserAttributeStrValue(u.getAttributes(), "gid"))
            .orElse(null);
        if (StringUtils.isBlank(gid)) {
            if (user == null) {
                log.warn("cannot get gid, because user is null!");
            } else {
                log.warn(
                    "cannot get gid, because either user has no attributes or gid is actually not set!");
            }
        }
        return new UserId(gid);
    }


    public String getUserAttributeStrValue(final Map<String, Object> userAttributes, final String key) {
        return getUserAttributeValue(userAttributes, key).map(object -> {
                String value = null;
                if (object instanceof String) {
                    value = (String) object;
                } else {
                    log.warn("unable to handle value because it is not of type String: {}", object);
                }
                return value;
            })
            .orElse(null);
    }


    private Optional<Object> getUserAttributeValue(final Map<String, Object> userAttributes, final String key) {
        if (MapUtils.isEmpty(userAttributes)) {
            return Optional.empty();
        }
        Object object = userAttributes.get(key);
        if (object == null) {
            return Optional.empty();
        }
        return Optional.of(object);
    }


    @AfterMapping
    public void mapAttributes(@MappingTarget User entity, OidcUser user) {
        entity.setGid(getGid(user));
    }


    String map(UserId userId) {
        return userId.getId();
    }

}
