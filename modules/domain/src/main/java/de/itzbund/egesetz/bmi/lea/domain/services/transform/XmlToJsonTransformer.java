// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.transform;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import lombok.extern.log4j.Log4j2;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_TYPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_CHILDREN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_TEXT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_TYPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_TEXT_WRAPPER;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.withDefaultPrefix;

/**
 * A transformer that converts LegalDocML.de compliant XML documents into JSON documents that can be displayed in the editor.
 */

@Component
@Log4j2
@SuppressWarnings("unchecked")
public class XmlToJsonTransformer {

    private static final String XML_IS_COALESCING = "javax.xml.stream.isCoalescing";


    public String transform(String xml) throws XMLStreamException {
        XMLStreamReader reader;

        InputStream inputStream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
        XMLInputFactory factory = createFactory();

        try {
            reader = factory.createXMLStreamReader(inputStream);
        } catch (XMLStreamException e) {
            log.error(e);
            throw e;
        }

        JSONObject documentJson = new JSONObject();
        JSONArray children = new JSONArray();
        boolean start = true;
        while (reader.hasNext()) {
            int readerNext = reader.next();
            if (readerNext == XMLStreamConstants.START_ELEMENT && start) {
                start = false;
                writeElement(reader, documentJson);
                writeAttribute(reader, documentJson);
                writeNameSpace(reader, documentJson);
            } else if (readerNext == XMLStreamConstants.START_ELEMENT) {
                children.add(writeJson(reader));
                documentJson.put(JSON_KEY_CHILDREN, children);
                break;
            } else if (readerNext == XMLStreamConstants.PROCESSING_INSTRUCTION) {
                documentJson.put(reader.getPITarget(), reader.getPIData());
            }
        }

        JSONArray jsonArray = new JSONArray();
        jsonArray.add(documentJson);
        return jsonArray.toJSONString();
    }


    private XMLInputFactory createFactory() {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        // unexplainable leads to error when accessed, instead use IS_SUPPORTING_EXTERNAL_ENTITIES
        // see https://stackoverflow.com/questions/67336607/how-to-be-sonar-compliant-with-the-xmlinputfactory-and-woodstox-library-register
        // factory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, ""); NOSONAR
        factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
        factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        factory.setProperty(XML_IS_COALESCING, true);
        return factory;
    }


    private JSONObject writeJson(XMLStreamReader reader) throws XMLStreamException {
        JSONArray children = new JSONArray();
        JSONObject child = new JSONObject();
        writeElement(reader, child);
        writeAttribute(reader, child);
        writeNameSpace(reader, child);

        while (reader.hasNext()) {
            int readerNext = reader.next();
            if (readerNext == XMLStreamConstants.CHARACTERS) {
                handleTextContent(reader, children);
            } else if (readerNext == XMLStreamConstants.START_ELEMENT) {
                children.add(writeJson(reader));
            } else if (readerNext == XMLStreamConstants.END_ELEMENT) {

                if (children.isEmpty()) {
                    JSONObject emptyTextChild = makeTextWrapper("");
                    children.add(emptyTextChild);
                }
                child.put(JSON_KEY_CHILDREN, children);
                return child;
            }
        }
        return child;
    }


    private void handleTextContent(XMLStreamReader reader, JSONArray children) {
        if (!reader.isWhiteSpace()) {
            String text = reader.getText();
            if (!text.isBlank()) {
                text = Utils.xmlNormalize(text);
                JSONObject textChild = makeTextWrapper(text);
                children.add(textChild);
            }
        }
    }


    private JSONObject makeTextWrapper(String text) {
        JSONObject wrapper = new JSONObject();
        wrapper.put(JSON_KEY_TYPE, JSON_VAL_TEXT_WRAPPER);

        JSONObject textChild = new JSONObject();
        textChild.put(JSON_KEY_TEXT, text);

        JSONArray children = new JSONArray();
        children.add(textChild);
        wrapper.put(JSON_KEY_CHILDREN, children);

        return wrapper;
    }


    private void writeElement(XMLStreamReader reader, JSONObject element) {

        element.put(JSON_KEY_TYPE, getName(reader.getName()));
        if (reader.getAttributeCount() > 0) {
            writeAttribute(reader, element);
        }
    }


    private void writeAttribute(XMLStreamReader reader, JSONObject element) {
        int attributeIndex = reader.getAttributeCount();
        for (int i = 0; i <= attributeIndex - 1; i++) {
            String name = getName(reader.getAttributeName(i));
            if (ATTR_TYPE.equals(name)) {
                name = withDefaultPrefix(name);
            }
            element.put(name, reader.getAttributeValue(i));
        }
    }


    private void writeNameSpace(XMLStreamReader reader, JSONObject element) {
        int nameSpaceIndex = reader.getNamespaceCount();
        for (int i = 0; i <= nameSpaceIndex - 1; i++) {
            String nameSpace = "xmlns:" + reader.getNamespacePrefix(i);
            element.put(nameSpace, reader.getNamespaceURI(i));
        }

    }


    private String getName(QName qName) {
        String name = qName.getLocalPart();
        if (!qName.getPrefix().isBlank() || !qName.getPrefix().isEmpty()) {
            name = qName.getPrefix() + ":" + name;
        }
        return name;
    }

}
