// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.filters;

import lombok.extern.log4j.Log4j2;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;

@Log4j2
public class FilterChain {

    private final List<XmlFilter> filters = new ArrayList<>();
    private int currentIndex = 0;


    public void addFilter(XmlFilter filter) {
        filters.add(filter);
    }


    public void doFilter(JSONObject jsonObject) {
        if (currentIndex < filters.size()) {
            XmlFilter currentFilter = filters.get(currentIndex);
            currentIndex++;
            currentFilter.doFilter(jsonObject, this);
        } else {
            // Wenn alle Filter durchlaufen wurden, kann hier die endgültige Aktion erfolgen.
            log.debug("Bearbeitete Anfrage: {}", jsonObject);
        }
    }

    public void reset() {
        currentIndex = 0;
    }

}
