// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model.vo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

/**
 * A domain related id for Bestandsrecht.
 */
public class BestandsrechtId extends AbstractId {

    @JsonCreator
    public BestandsrechtId(@JsonProperty("id") String id) {
        super(UUID.fromString(id));
    }

    public BestandsrechtId(UUID id) {
        super(id);
    }

}
