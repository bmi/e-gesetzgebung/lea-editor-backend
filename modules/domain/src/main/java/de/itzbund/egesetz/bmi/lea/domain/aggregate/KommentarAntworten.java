// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.aggregate;

import de.itzbund.egesetz.bmi.lea.domain.entities.Kommentar;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.ReplyId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.ReplyPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

/**
 * A DDD aggregate
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Getter
@Setter
public class KommentarAntworten {

    private final ReplyPersistencePort replyPersistencePort;

    @Autowired
    private LeageSession session;


    @Autowired
    public KommentarAntworten(final ReplyPersistencePort replyPersistencePort) {
        this.replyPersistencePort = replyPersistencePort;
    }


    /**
     * Fügt dem Kommentar eine Antwort an. Gibt es keinen Vorgänger, einfach anfügen. Gibt es einen Vorgänger, so wird der übernommen; das FE muss aufpassen,
     * dass nicht eine gelöschte Antwort als Vorgänger angegeben wird.
     *
     * @param kommentar comment object
     * @param antwort   reply object
     * @return Ob es funktioniert hat
     */
    public boolean antwortAufKommentarErstellen(Kommentar kommentar, Kommentar vorgaengerKommentar, Reply antwort) {
        ReplyId vorgaengerKommentarId;

        if (vorgaengerKommentar == null) {
            // Die erste Kommentarantwort bekommt als 'ParentId' die CommentId
            vorgaengerKommentarId = new ReplyId(kommentar.getCommentId()
                .getId());
        } else {
            vorgaengerKommentarId = vorgaengerKommentar.getReplyId();
        }

        Reply gespeicherteAntwort = speichereKommentarAntwort(kommentar.getCommentId(), kommentar.getDocumentId(),
            vorgaengerKommentarId, antwort);
        return gespeicherteAntwort != null;
    }


    /**
     * Ändert die bestehende Antwort eines Kommentars
     *
     * @param antwort            old reply object
     * @param veraenderteAntwort new reply object
     * @return Ob es funktioniert hat
     */
    public boolean kommentarAntwortAendern(Reply antwort, Reply veraenderteAntwort) {
        antwort.setContent(veraenderteAntwort.getContent());
        antwort.setUpdatedAt(Instant.now());

        Reply gespeicherteAntwort = replyPersistencePort.save(antwort);
        return gespeicherteAntwort != null;
    }


    /**
     * Löscht eine Antwort auf einen Kommentar indem die ANtwort als gelöscht markiert wird.
     *
     * @param zuLoeschendeAntwort reply object to be deleted
     * @return Ob es funktioniert hat
     */
    public boolean kommentarAntwortLoeschen(Reply zuLoeschendeAntwort) {
        zuLoeschendeAntwort.setDeleted(true);

        Reply gespeicherteAntwort = replyPersistencePort.save(zuLoeschendeAntwort);
        if (gespeicherteAntwort != null) {
            adjustPointers(zuLoeschendeAntwort);
            return true;
        }

        return false;
    }


    private Reply speichereKommentarAntwort(CommentId commentId, DocumentId documentId, ReplyId parentId, Reply reply) {
        reply.setReplyId(new ReplyId(UUID.randomUUID()));
        reply.setCommentId(commentId);
        reply.setParentId(parentId.getId()
            .toString());
        reply.setDocumentId(documentId);
        reply.setCreatedBy(session.getUser());
        reply.setUpdatedBy(session.getUser());
        reply.setCreatedAt(Instant.now());
        reply.setUpdatedAt(Instant.now());

        return replyPersistencePort.save(reply);
    }


    /**
     * Beim Löschen der Kommentarantworten müssen die Zeiger zwischen den Antworten angepasst werden. Für den Test ist die Methode package-private.
     *
     * @param antwort Die Kommentarantwort
     */
    void adjustPointers(Reply antwort) {
        ReplyId replyId = antwort.getReplyId();
        CommentId commentId = antwort.getCommentId();
        ReplyId parentId = new ReplyId(UUID.fromString(antwort.getParentId()));

        Optional<Reply> successor = replyPersistencePort.findByParentIdAndDeletedFalse(replyId.getId()
            .toString());

        // There is a successor -> we are not at the end of the list
        // (else we would have nothing to do)
        if (successor.isPresent()) {

            Reply successorReplyEntity = successor.get();

            UUID commentUuid = commentId.getId();
            if (commentUuid.equals(parentId.getId())) {
                // we are the start point of the list, so we have to point the successor to
                // the root (which is in our definition the comment id)
                successorReplyEntity.setParentId(commentUuid.toString());
            } else {
                // we are somewhere inside the list, so we have to point the successor to
                // our parent
                Optional<Reply> parent = replyPersistencePort.findByReplyId(parentId);
                if (parent.isPresent()) {
                    Reply parentReplyEntity = parent.get();
                    successorReplyEntity.setParentId(parentReplyEntity.getReplyId()
                        .getId()
                        .toString());
                }
            }

            replyPersistencePort.save(successorReplyEntity);
        }

    }
}
