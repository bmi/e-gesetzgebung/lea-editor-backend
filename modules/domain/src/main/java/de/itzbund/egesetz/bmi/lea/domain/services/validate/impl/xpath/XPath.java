// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.xpath;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for internal representation of an XPath. Basically its a list of {@link XPathComponent}s. Some convenience methods are provided.
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class XPath {

    private List<XPathComponent> path = new ArrayList<>();


    /**
     * Parses an XPath string into the internal model.
     *
     * @param xpathString an XPath as string
     * @return an XPath object
     */
    public static XPath parse(String xpathString) {
        List<XPathComponent> path = new ArrayList<>();
        String[] parts = xpathString.split("/");

        for (String part : parts) {
            if (!part.isBlank()) {
                path.add(XPathComponent.parse(part));
            }
        }

        return new XPath(path);
    }


    /**
     * Returns an XPath string of the parent component of the node represented by this string.
     *
     * @param xpathString an XPath string of a node
     * @return an XPath string of the parent of the node
     */
    public static String getParentString(String xpathString) {
        return XPath.parse(xpathString).getParent().toString();
    }


    /**
     * Adds another component to the current XPath object.
     *
     * @param component the {@link XPathComponent} to add
     * @return the extended XPath object
     */
    public XPath addComponent(XPathComponent component) {
        path.add(component);
        return this;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < path.size(); i++) {
            XPathComponent com = path.get(i);
            if (com != null) {
                sb.append('/').append(com.getName());
                if (i > 0) {
                    sb.append('[').append(com.getPosition()).append(']');
                }
            }
        }

        return sb.toString();
    }


    /**
     * Returns the parent XPath object of this XPath.
     *
     * @return the parent XPath
     */
    public XPath getParent() {
        List<XPathComponent> newPath = new ArrayList<>(this.path);
        newPath.remove(this.path.size() - 1);
        return new XPath(newPath);
    }

}
