// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest;

import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;

public interface ExportRestPort {

    void exportCompoundDocument(HttpServletResponse response, UUID compoundDocumentId, List<ServiceState> status);

    String exportSingleDocument(String decodedJson, List<ServiceState> status);

}
