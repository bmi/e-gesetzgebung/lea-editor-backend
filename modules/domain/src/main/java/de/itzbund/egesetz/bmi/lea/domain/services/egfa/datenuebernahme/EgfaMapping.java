// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.egfa.datenuebernahme;

import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeContentBlockDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeContentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaId;
import de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.InhaltsabschnittRefersToLiteral;
import de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.VorblattabschnittRefersToLiteral;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

@Log4j2
public class EgfaMapping {

	private static final String WARN_MSG = "Unknown ID in eGFA module: {}";

	public static final List<EgfaId> vorblattSectionsInOrder = List.of(
		//Abschnitt D
		EgfaId.TEXT_VORBLATT_D,
		//            EgfaId.VORBLATT_AUSGABEN,
		EgfaId.VORBLATT_AUSGABEN_BUND_CONTENT_00_REGELUNG_TABLE,
		EgfaId.VORBLATT_AUSGABEN_LAENDER_CONTENT_00_REGELUNG_TABLE,
		EgfaId.VORBLATT_AUSGABEN_GEMEINDEN_CONTENT_00_REGELUNG_TABLE,
		EgfaId.VORBLATT_AUSGABEN_GESAMTSUMME_CONTENT_00_REGELUNG_TABLE,
		//            EgfaId.VORBLATT_EINNAHMEN,
		EgfaId.VORBLATT_EINNAHMEN_BUND_CONTENT_00_REGELUNG_TABLE,
		EgfaId.VORBLATT_EINNAHMEN_LAENDER_CONTENT_00_REGELUNG_TABLE,
		EgfaId.VORBLATT_EINNAHMEN_GEMEINDEN_CONTENT_00_REGELUNG_TABLE,
		EgfaId.VORBLATT_EINNAHMEN_GESAMTSUMME_CONTENT_00_REGELUNG_TABLE,

		//Abschnitt E
		EgfaId.TEXT_VORBLATT_E,
		EgfaId.VORBLATT_BUERGER_TABLE_DATA,
		EgfaId.VORBLATT_WIRTSCHAFT_TABLE_DATA,
		EgfaId.VORBLATT_VERWALTUNG_TABLE_DATA,

		//Abschnitt F
		EgfaId.KMU_AFFECTED_TEXT_VORBLATT,
		EgfaId.KMU_SUMMARY_VORBLATT,
		EgfaId.PREIS_SUMMARY_VORBLATT,
		EgfaId.SONSTIGE_KOSTEN_SUMMARY_VORBLATT
	);

	public static final List<EgfaId> begruendungSectionsInOrder = List.of(
		//Abschnitt VI.2: Nachhaltigkeitsaspekte
		EgfaId.ENAP_SUMMARY,

		//Abschnitt VI.3: Öffentliche Haushalte
		//--> Zweckausgaben
		EgfaId.ZWECKAUSGABEN_PATTERN_SUMMARY_TEXT,
		EgfaId.ZWECKAUSGABEN_PATTERN_SUMMARY_JAHRESWIRKUNG_TEXT,
		EgfaId.ZWECKAUSGABEN_PATTERN_SUMMARY_WIDGET_BUND_TABLE,
		EgfaId.ZWECKAUSGABEN_PATTERN_SUMMARY_WIDGET_LAENDER_TABLE,
		EgfaId.ZWECKAUSGABEN_PATTERN_SUMMARY_WIDGET_GEMEINDEN_TABLE,
		EgfaId.ZWECKAUSGABEN_PATTERN_SUMMARY_WIDGET_GESAMTSUMME_TABLE,
		//--> Vollzugsaufwand
		EgfaId.VOLLZUGSAUFWAND_PATTERN_SUMMARY_TEXT,
		EgfaId.VOLLZUGSAUFWAND_PATTERN_SUMMARY_WIDGET_BUND_TABLE,
		EgfaId.VOLLZUGSAUFWAND_PATTERN_SUMMARY_WIDGET_LAENDER_TABLE,
		EgfaId.VOLLZUGSAUFWAND_PATTERN_SUMMARY_WIDGET_GEMEINDEN_TABLE,
		EgfaId.VOLLZUGSAUFWAND_PATTERN_SUMMARY_WIDGET_GESAMTSUMME_TABLE,
		EgfaId.VOLLZUGSAUFWAND_INFLUENCE_SUMMARY_TEXT,
		EgfaId.VOLLZUGSAUFWAND_PATTERN_SUMMARY_WIDGET_PLANSTELLEN_TABLE,
		EgfaId.VOLLZUGSAUFWAND_PATTERN_SUMMARY_WIDGET_PLANSTELL_GESAMTEN_TABLE,
		//--> Normadressat
		EgfaId.NORMADRESSAT_SUMMARY_TEXT,
		EgfaId.NORMADRESSAT_AUSGABEN_DURCH_TABLE,
		EgfaId.NORMADRESSAT_AUSGABEN_GESAMT_TABLE,
		EgfaId.STELLENPLANEN_SUMMARY,
		EgfaId.NORMADRESSAT_PLANSTELLEN_TABLE,
		EgfaId.NORMADRESSAT_PLANSTELLEN_GESAMT_TABLE,
		//--> Verwaltungs und Sonstige
		EgfaId.VERWALTUNGS_UND_SONSTIGE_PATTERN_SUMMARY_TEXT,
		EgfaId.VERWALTUNGS_UND_SONSTIGE_PATTERN_SUMMARY_JAHRESWIRKUNG_TEXT,
		EgfaId.VERWALTUNGS_UND_SONSTIGE_PATTERN_SUMMARY_WIDGET_BUND_TABLE,
		EgfaId.VERWALTUNGS_UND_SONSTIGE_PATTERN_SUMMARY_WIDGET_LAENDER_TABLE,
		EgfaId.VERWALTUNGS_UND_SONSTIGE_PATTERN_SUMMARY_WIDGET_GEMEINDEN_TABLE,
		EgfaId.VERWALTUNGS_UND_SONSTIGE_PATTERN_SUMMARY_WIDGET_GESAMTSUMME_TABLE,
		//--> Steuereinnahmen
		EgfaId.STEUEREINNAHMEN_PATTERN_SUMMARY_TEXT,
		EgfaId.STEUEREINNAHMEN_PATTERN_SUMMARY_JAHRESWIRKUNG_TEXT,
		EgfaId.STEUEREINNAHMEN_PATTERN_SUMMARY_WIDGET_BUND_TABLE,
		EgfaId.STEUEREINNAHMEN_PATTERN_SUMMARY_WIDGET_LAENDER_TABLE,
		EgfaId.STEUEREINNAHMEN_PATTERN_SUMMARY_WIDGET_GEMEINDEN_TABLE,
		EgfaId.STEUEREINNAHMEN_PATTERN_SUMMARY_WIDGET_GESAMTSUMME_TABLE,
		//--> Sonstige Bereiche
		EgfaId.SONSTIGE_BEREICHE_TEXT,
		EgfaId.SONSTIGE_BEREICHE_VOLLE_JAHRESWIRKUNG_SUMMARY_TEXT,
		EgfaId.SONSTIGE_BEREICHE_TABLE,
		EgfaId.SONSTIGE_BEREICHE_GESAMT_TABLE,
		EgfaId.SONSTIGE_BEREICHE_EINNAHMEN_TABLE,
		EgfaId.SONSTIGE_BEREICHE_EINNAHMEN_GESAMT_TABLE,
		//--> Kompensation Kosten
		EgfaId.KOMPENSATION_KOSTEN_TEXT,

		//Abschnitt VI.4: Erfuellungsaufwand
		EgfaId.TEXT_ZUSAMMENFASSUNG_BEGRUENDUNGSTEIL,
		EgfaId.TEXT_ZUSAMMENFASSUNG_BEGRUENDUNG_BUERGER,
		EgfaId.ZUSAMMENFASSUNG_BEGRUENDUNG_BUERGER_GESAMTSUMMEN_TABLE_DATA,
		EgfaId.ZUSAMMENFASSUNG_BEGRUENDUNG_BUERGER_JAEHRLICHEN_TABLE_DATA,
		EgfaId.ZUSAMMENFASSUNG_BEGRUENDUNG_BUERGER_EINMALIGER_TABLE_DATA,
		EgfaId.TEXT_ZUSAMMENFASSUNG_BEGRUENDUNG_WIRTSCHAFT,
		EgfaId.ZUSAMMENFASSUNG_BEGRUENDUNG_WIRTSCHAFT_GESAMTSUMMEN_TABLE_DATA,
		EgfaId.ZUSAMMENFASSUNG_BEGRUENDUNG_WIRTSCHAFT_JAEHRLICHEN_TABLE_DATA,
		EgfaId.ZUSAMMENFASSUNG_BEGRUENDUNG_WIRTSCHAFT_INFORMATIONSPFLICHTEN_TABLE_DATA,
		EgfaId.ZUSAMMENFASSUNG_BEGRUENDUNG_WIRTSCHAFT_EINMALIGER_TABLE_DATA,
		EgfaId.TEXT_ZUSAMMENFASSUNG_BEGRUENDUNG_VERWALTUNG,
		EgfaId.ZUSAMMENFASSUNG_BEGRUENDUNG_VERWALTUNG_GESAMTSUMMEN_TABLE_DATA,
		EgfaId.ZUSAMMENFASSUNG_BEGRUENDUNG_VERWALTUNG_JAERLICHEN_TABLE_DATA,
		EgfaId.ZUSAMMENFASSUNG_BEGRUENDUNG_VERWALTUNG_EINMALIGER_TABLE_DATA,

		//Abschnitt VI.5: Weitere Kosten
		EgfaId.KMU_AFFECTED_TEXT_BEGRUENDUNG,
		EgfaId.KMU_SUMMARY_BEGRUENDUNG,
		EgfaId.PREIS_SUMMARY_BEGRUENDUNG,
		EgfaId.SONSTIGE_KOSTEN_SUMMARY_BEGRUENDUNG,

		//Abschnitt VI.6: Gleichstellungspolitische Relevanzprüfung
		EgfaId.GLEICHSTELLUNGSORIENTIERTE_DATA_SUMMARY,
		EgfaId.DISABILITY_DATA_SUMMARY,
		EgfaId.DEMOGRAFIE_CHECK_DATA_SUMMARY,
		EgfaId.GLEICHWERTIGKEITS_CHECK_SUMMARY,
		EgfaId.VERBRAUCHER_SUMMARY,
		EgfaId.WEITERE_SUMMARY,

		//Abschnitt VII: Befristung, Evaluierung
		EgfaId.EVALUIERUNG_DURCHFUEHRUNG_TEXT,
		EgfaId.NO_EVALUIERUNG
	);

	private final Map<EgfaId, EgfaDatenuebernahmeContentDTO> contents;
	private final Map<EgfaId, String> parentTitles;

	@Getter
	private boolean concerningVorblatt = false;
	@Getter
	private boolean concerningBegruendung = false;


	public EgfaMapping(EgfaDatenuebernahmeDTO egfaDatenuebernahmeDTO) {
		contents = new EnumMap<>(EgfaId.class);
		parentTitles = new EnumMap<>(EgfaId.class);

		if (egfaDatenuebernahmeDTO != null) {
			processData(egfaDatenuebernahmeDTO);
		}
	}


	public EgfaDatenuebernahmeContentDTO getContentDTO(EgfaId egfaId) {
		return contents.get(egfaId);
	}


	public String getParentTitle(EgfaId egfaId) {
		return parentTitles.get(egfaId);
	}


	private void processData(@NotNull EgfaDatenuebernahmeDTO egfaDatenuebernahmeDTO) {
		String title = egfaDatenuebernahmeDTO.getTitle();
		List<? extends EgfaDatenuebernahmeContentDTO> contentDTOS = egfaDatenuebernahmeDTO.getContent();

		if (contentDTOS.isEmpty()) {
			//Hier wird für die Sonderfälle aus "Oeffentliche Haushalte" ein ContentBlock angelegt
			checkContentBlock(egfaDatenuebernahmeDTO);
		} else {
			checkContent(title, contentDTOS);
		}

		List<EgfaDatenuebernahmeDTO> children = egfaDatenuebernahmeDTO.getChildren();
		if (children != null) {
			children.forEach(this::processData);
		}
	}


	@SuppressWarnings("java:S3776")
	private void checkContent(String title, List<? extends EgfaDatenuebernahmeContentDTO> contentDTOS) {
		contentDTOS.forEach(dto -> {
			List<EgfaId> egfaIds = EgfaId.fromLiteral(dto.getId());
			if (egfaIds == null) {
				log.warn(WARN_MSG, dto.getId());
				return;
			}
			for (EgfaId egfaId : egfaIds) {
				if (egfaId != null) {
					if (!concerningVorblatt && egfaId.getRegardingDocType() == RechtsetzungsdokumentArt.VORBLATT) {
						concerningVorblatt = true;
					} else if (!concerningBegruendung
						&& egfaId.getRegardingDocType() == RechtsetzungsdokumentArt.BEGRUENDUNG) {
						concerningBegruendung = true;
					}
					if (title != null && egfaId.isHasLabel()) {
						dto.setLabel(title);
					}
					contents.put(egfaId, dto);
					parentTitles.put(egfaId, title == null ? "" : title);
				} else {
					log.warn(WARN_MSG, dto.getId());
				}
			}
		});
	}


	private void checkContentBlock(EgfaDatenuebernahmeDTO egfaDatenuebernahmeDTO) {
		List<EgfaId> egfaIds = EgfaId.fromLiteral(egfaDatenuebernahmeDTO.getId());
		if (egfaIds == null) {
			return;
		}
		for (EgfaId egfaId : egfaIds) {
			if (egfaId.isHasLabel()) {
				if (!(egfaId.getParentRef()
					== VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND
					||
					egfaId.getParentRef()
						== InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND
				)) {
					log.warn("Only mapping from \"Oeffentliche Haushalte\" should contain blocks!");
				}
				processEgfaIdContentBlock(egfaDatenuebernahmeDTO, egfaId);
			}
		}
	}


	private void processEgfaIdContentBlock(EgfaDatenuebernahmeDTO egfaDatenuebernahmeDTO, EgfaId egfaId) {
		List<EgfaDatenuebernahmeContentDTO> contentList = new ArrayList<>();
		if (egfaDatenuebernahmeDTO.getChildren() == null) {
			return;
		}
		for (EgfaDatenuebernahmeDTO dtoChildren : egfaDatenuebernahmeDTO.getChildren()) {
			List<? extends EgfaDatenuebernahmeContentDTO> contentDTOs = dtoChildren.getContent();
			for (EgfaDatenuebernahmeContentDTO contentDTO : contentDTOs) {

				if (dtoChildren.getTitle() != null) {
					contentDTO.setLabel(dtoChildren.getTitle());
				}
				contentList.add(contentDTO);
			}
		}
		EgfaDatenuebernahmeContentBlockDTO ret = new EgfaDatenuebernahmeContentBlockDTO(contentList);
		ret.setLabel(egfaDatenuebernahmeDTO.getTitle());

		contents.put(egfaId, ret);
	}

}
