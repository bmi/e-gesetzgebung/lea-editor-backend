// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.mappers;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.ReplyId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.core.Constants.DATE_TIME_FORMATTER;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true)
)
public interface BaseMapper {

    default String map(UUID value) {
        return Optional.ofNullable(value)
            .map(UUID::toString)
            .orElse(null);
    }

    // -----------

    default UUID mapToUUID(String value) {
        return UUID.fromString(value);
    }

    default String map(Instant instant) {
        return instant == null ? null : DATE_TIME_FORMATTER.format(instant);
    }

    default Instant mapToInstant(String value) {
        return Instant.parse(value);
    }

    // -----------

    @Named("documentIdStringMapping")
    default String mapFromDocumentId(DocumentId documentId) {
        return documentId == null ? null : documentId.getId().toString();
    }

    // -----------

    @Named("commentIdStringMapping")
    default String mapFromCommentId(CommentId commentId) {
        return commentId == null ? null : commentId.getId().toString();
    }

    // -----------

    @Named("userIdStringMapping")
    default String mapFromUserId(UserId userId) {
        return userId == null ? null : userId.getId();
    }

    @Named("userIdStringMapping")
    default UserId mapToUserId(String uuidAsString) {
        return new UserId(uuidAsString);
    }

    // -----------

    @Named("replyIdUuidMapping")
    default UUID mapFromReplyId(ReplyId replyId) {
        return replyId == null ? null : replyId.getId();
    }

    @Named("replyIdUuidMapping")
    default ReplyId mapToReplyId(UUID uuid) {
        return new ReplyId(uuid);
    }

    // -----------

    @Named("compoundDocumentIdUuidMapping")
    default UUID mapFromCompoundDocumentId(CompoundDocumentId compoundDocumentId) {
        if (compoundDocumentId == null) {
            return null;
        }

        return compoundDocumentId.getId();
    }

    // -----------
    @Named("propositionIdUuidMapping")
    default RegelungsVorhabenId mapFromRFromUuid(UUID uuid) {
        return new RegelungsVorhabenId(uuid);
    }

    @Named("propositionIdUuidMapping")
    default UUID mapToRF(RegelungsVorhabenId rvId) {
        return rvId == null ? null : rvId.getId();
    }
}
