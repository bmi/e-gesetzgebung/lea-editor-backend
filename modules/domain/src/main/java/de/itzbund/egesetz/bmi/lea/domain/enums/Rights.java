// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;


@SuppressWarnings("unused")
public enum Rights {

	LESEN("LESEN"),
	SCHREIBEN("SCHREIBEN"),
	KOMMENTIEREN("KOMMENTIEREN"),
	KOMMENTARE_LESEN("KOMMENTARE_LESEN");

	private final String value;


	Rights(String value) {
		this.value = value;
	}


	@JsonCreator
	public static Rights fromValue(String text) {
		for (Rights b : Rights.values()) {
			if (String.valueOf(b.value).equals(text)) {
				return b;
			}
		}
		return null;
	}


	@Override
	@JsonValue
	public String toString() {
		return String.valueOf(value);
	}
}
