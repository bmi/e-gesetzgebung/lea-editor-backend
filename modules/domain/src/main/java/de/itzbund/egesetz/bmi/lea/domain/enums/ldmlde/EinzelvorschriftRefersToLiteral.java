// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde;

import de.itzbund.egesetz.bmi.lea.domain.model.ldmlde.LDMLDeModellElementBezeichnung;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum EinzelvorschriftRefersToLiteral implements RefersToLiteral {

    EINZELVORSCHRIFT_STAMMFORM("stammform"),
    EINZELVORSCHRIFT_GELTUNGSZEITREGEL("geltungszeitregel"),
    EINZELVORSCHRIFT_MANTELFORM("mantelform"),
    EINZELVORSCHRIFT_MANTELFORM_HAUPTAENDERUNG("hauptaenderung"),
    EINZELVORSCHRIFT_MANTELFORM_FOLGEAENDERUNG("folgeaenderung"),
    EINZELVORSCHRIFT_MANTELFORM_EINGEBUNDENE_STAMMFORM("eingebundene-stammform");

    private static final Map<String, EinzelvorschriftRefersToLiteral> literals;

    static {
        literals = new HashMap<>();
        Arrays.stream(values()).forEach(v -> literals.put(v.getLiteral(), v));
    }

    private final String literal;

    @Override
    public LDMLDeModellElementBezeichnung getContext() {
        return LDMLDeModellElementBezeichnung.EINZELVORSCHRIFT;
    }


    public static EinzelvorschriftRefersToLiteral fromLiteral(String literal) {
        return literals.get(literal);
    }

}
