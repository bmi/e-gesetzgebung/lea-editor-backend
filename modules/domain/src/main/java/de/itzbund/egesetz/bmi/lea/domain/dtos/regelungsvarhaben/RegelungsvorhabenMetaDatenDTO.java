// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben;

import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentFassung;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentForm;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentInitiant;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
public class RegelungsvorhabenMetaDatenDTO {

    /**
     * Possible values: - gesetz - verordnung - satzung - verwaltungsvorschrift - vertragsgesetz - vertragsverordnung - sonstige-bekanntmachung
     */
    @NotNull
    private RechtsetzungsdokumentTyp typ;

    /**
     * @see de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType Possible values: - mantelform - stammform - eingebundene-stammform - nicht-vorhanden
     */
    @NotNull
    private RechtsetzungsdokumentForm form;

    /**
     * Possible values: - entwurfsfassung - verkuendungsfassung (unwahrscheinlich) - neufassung (unwahrscheinlich)
     */
    @NotNull
    private RechtsetzungsdokumentFassung fassung = RechtsetzungsdokumentFassung.ENTWURFSFASSUNG;

    /**
     * @see de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType Possible values: - rechtsetzungsdokument - regelungstext - vorblatt - begruendung - anschreiben
     * - denkschrift - vereinbarung - offene-struktur - bekanntmachungstext
     */
    @NotNull
    private RechtsetzungsdokumentArt art;

    /**
     * Possible values: - bundesregierung - bundestag - bundesrat - bundespräsident - bundeskanzler - nicht-vorhanden
     */
    @NotNull
    private RechtsetzungsdokumentInitiant initiant;

    /**
     * Not tracked by Platform at the moment
     */
    @NotNull
    private RechtsetzungsdokumentInitiant bearbeitendeInstitution = RechtsetzungsdokumentInitiant.BUNDESREGIERUNG;

}
