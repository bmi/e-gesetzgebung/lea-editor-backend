// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.document;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Data
@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DocumentDTO extends DocumentMetadataDTO {

    @ToString.Exclude
    @JsonProperty("content")
    @Schema(name = "content", example = "A BASE64 coded JSON, e.g. RGFzIGthbm5zdCBkdSBkb2NoIGVoIG5pY2h0IGxlc2VuLg==")
    private String content;

    @ToString.Exclude
    @JsonProperty("cdName")
    @Schema(name = "cdName", example = "Dokumentenmappenname")
    private String mappenName;

}


