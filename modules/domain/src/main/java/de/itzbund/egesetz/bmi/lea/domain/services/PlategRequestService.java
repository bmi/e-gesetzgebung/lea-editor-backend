// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.itzbund.egesetz.bmi.lea.domain.dtos.abstimmung.CompoundDocumentEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorShortDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType;
import de.itzbund.egesetz.bmi.lea.domain.mappers.RegelungsvorhabenMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.PlatformMessage;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.PlategRestPort;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static de.itzbund.egesetz.bmi.lea.core.Constants.API_KEY_NAME;

@Service
@RequiredArgsConstructor
@Log4j2
@SuppressWarnings("unused")
public class PlategRequestService implements PlategRestPort {

	private static final String GOT_RESPONSE = "Got response: {}";
	private static final String ERR_MSG = "Error occurred while sending request to Plattform";
	private static final String DBG_MSG = "Plattform has no data for us";
	private static final String WARN_NO_RV = "List of regulatory proposals is empty";

	@Value("${plateg.x-api-key}")
	private String apiKey;

	@Value("${plateg.url}")
	private String baseUrl;

	// Autowired by Constructor
	private final ApplicationContext applicationContext;
	private final RestTemplate restTemplate;
	private final LeageSession session;
	private final RegelungsvorhabenMapper regelungsvorhabenMapper;

	/**
	 * Get all 'Regelungsvorhaben' of a user. It replaces the calls from plattform, but uses it for fallback.
	 *
	 * @return A list of Regelungsvorhaben encapsulated in a DTO
	 */
	public List<RegelungsvorhabenEditorDTO> getAllRegulatoryProposalsForUserHeIsAuthorOf() {
		List<RegelungsvorhabenEditorDTO> regelungsvorhabenEditorDTOList = new ArrayList<>();

		// RBAC liefert uns die Ids der Regelungsvorhaben des Benutzers
		RBACPermissionService rbacPermissionService = applicationContext.getBean(RBACPermissionService.class);
		List<UUID> regelungsvorhabenIdsMitNutzerZugriff = rbacPermissionService.getRegelungsvorhabenIdsMitNutzerZugriff(session.getUser());
		// Vom Service holen wir die Daten
		return getRegelungsvorhabenEditorDtosForIdList(regelungsvorhabenIdsMitNutzerZugriff, regelungsvorhabenEditorDTOList);
	}


	@SuppressWarnings({"squid:S1135", "squid:S125"})
	private List<RegelungsvorhabenEditorDTO> getRegelungsvorhabenEditorDtosForIdList(List<UUID> regelungsvorhabenIdsMitNutzerZugriff,
		List<RegelungsvorhabenEditorDTO> regelungsvorhabenEditorDTOList) {
		// Vom Service holen wir die Daten
		RegelungsvorhabenService regelungsvorhabenService = applicationContext.getBean(RegelungsvorhabenService.class);

		regelungsvorhabenIdsMitNutzerZugriff.forEach(
			rv -> regelungsvorhabenEditorDTOList.add(regelungsvorhabenService.getRegelungsvorhabenEditorDTO(new RegelungsVorhabenId(rv)))
		);

		// wir müssen sortieren und filtern
		List<RegelungsvorhabenEditorDTO> sortedList = regelungsvorhabenEditorDTOList.stream()
			.filter(Objects::nonNull) // Nullwerte filtern
			.filter(regelungsvorhabenEditorDTO -> !Objects.equals(regelungsvorhabenEditorDTO.getStatus(), VorhabenStatusType.ARCHIVIERT))
			.filter(regelungsvorhabenEditorDTO -> regelungsvorhabenEditorDTO.getBearbeitetAm() != null) // Nullwerte von 'bearbeitetAM' filtern
			.sorted(Comparator.comparing(RegelungsvorhabenEditorDTO::getBearbeitetAm).reversed())
			.collect(Collectors.toList());

		return sortedList;
	}

	/**
	 * Get a 'Regelungsvorhaben' of a user by calling platform's backend
	 *
	 * @return The Regelungsvorhaben encapsulated in a DTO
	 */
	public RegelungsvorhabenEditorDTO getRegulatoryProposalById(UUID regulatoryProposalId) {
		// Vom Service holen wir die Daten
		RegelungsvorhabenService regelungsvorhabenService = applicationContext.getBean(RegelungsvorhabenService.class);
		return regelungsvorhabenService.getRegelungsvorhabenEditorDTO(new RegelungsVorhabenId(regulatoryProposalId));
	}

	/**
	 * Get a list of 'Regelungsvorhaben' of a user by calling platform's backend
	 *
	 * @return The list of Regelungsvorhaben, each one encapsulated in a DTO
	 */
	@SuppressWarnings({"squid:S1135", "squid:S125"})
	public List<RegelungsvorhabenEditorDTO> getRegulatoryProposalsByIdList(List<String> regulatoryProposalIds) {
		return getRegulatoryProposalsByIdListPlattform(regulatoryProposalIds);
	}

	/**
	 * Get a list of 'Regelungsvorhaben' of a user by calling platform's backend
	 *
	 * @return The list of Regelungsvorhaben, each one encapsulated in a DTO
	 */
	// Nun: SinglePointOfTrust
	public List<RegelungsvorhabenEditorDTO> getRegulatoryProposalsByIdListPlattform(List<String> regulatoryProposalIds, Optional<UserId> userid) {
		EndpointURL endpointURL = EndpointURL.GET_REGULATORY_PROPOSAL_LIST_IF_ACCESS_ALLOWED;

		Optional<String> user = Optional.empty();

		if (userid.isPresent()) {
			user = Optional.of(userid.get().getId());
		}

		StringBuilder url = new StringBuilder(createUrl(endpointURL, user));
		for (int i = 0; i < regulatoryProposalIds.size(); i++) {
			if (i == 0) {
				url.append("?");
			} else {
				url.append("&");
			}
			url.append("rvIds=").append(regulatoryProposalIds.get(i));
		}
		HttpEntity<?> requestEntity = new HttpEntity<>(null, setHeaderWithXApiKey());

		ResponseEntity<List<RegelungsvorhabenEditorDTO>> response = null;
		try {
			response = restTemplate.exchange(
				url.toString(), endpointURL.getHttpMethod(), requestEntity,
				new ParameterizedTypeReference<>() {
				}
			);
		} catch (RestClientException e) {
			log.debug(DBG_MSG, e);
		}

		if ((response != null) && (response.getStatusCode().is2xxSuccessful())) {
			HttpStatus httpStatus = response.getStatusCode();
			log.debug(GOT_RESPONSE, httpStatus);
			return response.getBody();
		}

		return Collections.emptyList();
	}

	/**
	 * Get a list of 'Regelungsvorhaben' of a user by calling platform's backend
	 *
	 * @return The list of Regelungsvorhaben, each one encapsulated in a DTO
	 */
	public List<RegelungsvorhabenEditorDTO> getRegulatoryProposalsByIdListPlattform(List<String> regulatoryProposalIds) {
		return getRegulatoryProposalsByIdListPlattform(regulatoryProposalIds, Optional.of(session.getUser().getGid()));
	}

	/**
	 * Get the 'Regelungsvorhaben' of a user specified by Regelungsvorhaben's UUID by calling platform's backend
	 *
	 * @return The specific Regelungsvorhaben encapsulated in a DTO
	 */
	@Override
	public RegelungsvorhabenEditorDTO getRegulatoryProposalUserCanAccess(UUID regelungsvorhabenId) {
		return getRegulatoryProposalUserCanAccess(session.getUser(), regelungsvorhabenId);
	}

	/**
	 * Get the 'Regelungsvorhaben' of a user specified by Regelungsvorhaben's UUID by calling platform's backend
	 *
	 * @return The specific Regelungsvorhaben encapsulated in a DTO
	 */
	@Override
	public RegelungsvorhabenEditorDTO getRegulatoryProposalUserCanAccess(User createdBy, UUID regelungsvorhabenId) {

		// RBAC liefert uns die Ids der Regelungsvorhaben des Benutzers
		RBACPermissionService rbacPermissionService = applicationContext.getBean(RBACPermissionService.class);
		List<UUID> regelungsvorhabenIdsMitNutzerZugriff = rbacPermissionService.getRegelungsvorhabenIdsMitNutzerZugriff(createdBy);
		if (regelungsvorhabenIdsMitNutzerZugriff.contains(regelungsvorhabenId)) {
			// Vom Service holen wir die Daten
			RegelungsvorhabenService regelungsvorhabenService = applicationContext.getBean(RegelungsvorhabenService.class);
			return regelungsvorhabenService.getRegelungsvorhabenEditorDTO(new RegelungsVorhabenId(regelungsvorhabenId));
		}

		return null;
	}

	/**
	 * @param compoundDocumentId Id of compound document
	 */
	@Override
	public CompoundDocumentEditorDTO getAbstimmungenEinerDokumentenmappe(CompoundDocumentId compoundDocumentId) {
		EndpointURL endpointURL = EndpointURL.GET_ABSTIMMUNG_FUER_DOKUMENTENMAPPE;
		String url = createUrl(endpointURL, Optional.of(compoundDocumentId.getId().toString()));
		HttpEntity<?> requestEntity = new HttpEntity<>(null, setHeaderWithXApiKey());

		ResponseEntity<CompoundDocumentEditorDTO> response = null;

		try {
			response = restTemplate.exchange(
				url, endpointURL.getHttpMethod(), requestEntity, CompoundDocumentEditorDTO.class
			);
		} catch (RestClientException e) {
			log.debug(DBG_MSG, e);
		}

		if ((response != null) && (response.getStatusCode()
			.is2xxSuccessful())) {
			log.debug("Data from plattform: {}", response.getBody());
			return response.getBody();
		}

		return null;
	}

	@Override
	public List<CompoundDocumentEditorDTO> getAbstimmungUndDokumentenmappe() {
		EndpointURL endpointURL = EndpointURL.GET_ABSTIMMUNG_UND_DOKUMENTENMAPPE;
		String url = createUrl(endpointURL, Optional.empty());
		HttpEntity<?> requestEntity = new HttpEntity<>(null, setHeaderWithXApiKey());
		ResponseEntity<List<CompoundDocumentEditorDTO>> response = restTemplate.exchange(
			url, endpointURL.getHttpMethod(), requestEntity,
			new ParameterizedTypeReference<>() {
			}
		);
		HttpStatus httpStatus = response.getStatusCode();
		log.debug(GOT_RESPONSE, httpStatus);

		if (httpStatus.is2xxSuccessful()) {
			return response.getBody();
		} else if (httpStatus.equals(HttpStatus.NOT_FOUND)) {
			log.warn("Abstimmungen not found");
			return List.of();
		} else {
			throw new ResponseStatusException(httpStatus, ERR_MSG);
		}
	}


	/**
	 * Setze eine Nachricht zu Plattform ab, die dann dem Nutzer angezeigt wird
	 *
	 * @return Success or not
	 */
	public boolean setMessageInPlatform(PlatformMessage platformMessage) {
		EndpointURL endpointURL = EndpointURL.SET_NEUIGKEITEN;
		String url = createUrl(endpointURL, Optional.empty());

		HttpHeaders httpHeaders = setHeaderWithXApiKey();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);

		ObjectMapper objectMapper = new ObjectMapper();

		try {
			HttpEntity<?> requestEntity = new HttpEntity<>(objectMapper.writeValueAsString(platformMessage), httpHeaders);

			ResponseEntity<Void> response = restTemplate.exchange(url, endpointURL.getHttpMethod(), requestEntity, Void.class);

			if ((response != null) && (response.getStatusCode().is2xxSuccessful())) {
				HttpStatus httpStatus = response.getStatusCode();
				log.debug(GOT_RESPONSE, httpStatus);
				return true;
			}

		} catch (RestClientException e) {
			log.debug(DBG_MSG, e);
		} catch (JsonProcessingException e) {
			log.error("JSON Conversation failed for: ", platformMessage, e);
		}

		return false;
	}

	// =================================================================

	public boolean isUserParticipantForCompoundDocument(CompoundDocumentId compoundDocumentId) {
		CompoundDocumentEditorDTO result = getAbstimmungenEinerDokumentenmappe(compoundDocumentId);
		return (result != null) && (!result.getAbstimmungen()
			.isEmpty());
	}

	/**
	 * Get's a DTO for a 'Regelungsvorhaben'
	 *
	 * @param propositionId Id of the 'Regelungsvorhaben'
	 * @return A RegelungsvorhabenEditorDTO
	 */
	public RegelungsvorhabenEditorShortDTO getRegelungsvorhabenEditorDTO(UUID propositionId) {
		RegelungsvorhabenEditorShortDTO proposition = null;
		if (propositionId != null) { // Problem!
			proposition = getRegulatoryProposalUserCanAccess(propositionId);
		}
		return proposition;
	}

	private HttpHeaders setHeaderWithXApiKey() {
		HttpHeaders headers = new HttpHeaders();
		headers.set(API_KEY_NAME, apiKey);
		return headers;
	}

	public String createUrl(EndpointURL endpointURL, Optional<String> someId) {
		return createUrl(endpointURL, session.getUser(), someId);
	}

	public String createUrl(EndpointURL endpointURL, User user, Optional<String> someId) {
		String gid = user.getGid()
			.getId();
		String url = baseUrl + endpointURL.getUriPattern()
			.replace("{gid}", gid);
		if (someId.isPresent()) {
			url = url.replace("{id}", someId.get());
		}
		log.debug("try to call {}", url);
		return url;
	}

	@RequiredArgsConstructor
	@Getter
	public enum EndpointURL {
		GET_LIST_OF_REGULATORY_PROPOSALS(HttpMethod.GET, "/editor/user/{gid}/regelungsvorhaben"),
		GET_REGULATORY_PROPOSAL_IF_ACCESS_ALLOWED(HttpMethod.GET, "/editor/user/{gid}/regelungsvorhaben/{id}"),
		GET_REGULATORY_PROPOSAL_LIST_IF_ACCESS_ALLOWED(HttpMethod.POST, "/editor/user/{gid}/regelungsvorhaben/list"),
		GET_REGELUNGSVORHABEN_METADATEN(HttpMethod.GET, "/editor/user/{gid}/regelungsvorhaben/{id}/metadaten"),
		GET_COMPOUND_DOCUMENTS_FOR_REGELUNGSVORHABEN(HttpMethod.GET,
			"/editor/user/{gid}/regelungsvorhaben/{id}/compounddocuments"),
		GET_ABSTIMMUNG_FUER_DOKUMENTENMAPPE(HttpMethod.GET, "/editor/user/{gid}/compoundDocuments/{id}/list"),
		GET_ABSTIMMUNG_UND_DOKUMENTENMAPPE(HttpMethod.GET, "/editor/user/{gid}/compoundDocuments/list"),
		SET_NEUIGKEITEN(HttpMethod.POST, "/editor/user/{gid}/neuigkeiten");
		private final HttpMethod httpMethod;
		private final String uriPattern;
	}

}
