// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;

/**
 * represents root DTO for Bestandsrecht-Übernahme to Editor
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class BestandsrechtDTO {

	@NotNull
	private String eli;
	@NotNull
	private String titelKurz;
	@NotNull
	private String titelLang;
	@NotNull
	private String content;
}
