// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.task;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Abstract default implementation of a {@link ValidationTask}.
 */
public abstract class AbstractValidationTask implements ValidationTask {

    private UUID id;

    private String name;

    private Set<ValidationTask> predecessors;


    @Override
    public String getName() {
        if (name == null) {
            name = this.getClass().getSimpleName();
        }
        return name;
    }


    @Override
    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String getId() {
        if (id == null) {
            id = UUID.randomUUID();
        }
        return id.toString();
    }


    @Override
    public void addPredecessor(ValidationTask task) {
        getPredecessors().add(task);
    }


    @Override
    public Set<ValidationTask> getPredecessors() {
        if (predecessors == null) {
            predecessors = new HashSet<>();
        }
        return predecessors;
    }


    @Override
    public String toString() {
        return String.format("%s_%s", getName(), getId());
    }

}
