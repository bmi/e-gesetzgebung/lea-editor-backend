// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.paging;


public enum FilterPropertyType {

    REGELUNGSVORHABEN("regelungsvorhaben"),
    DOKUMENTENMAPPE("dokumentenmappe"),
    DOKUMENT("dokument"),
    VERSION("version"),
    NONE("none");

    public final String label;

    FilterPropertyType(String label) {
        this.label = label;
    }

}