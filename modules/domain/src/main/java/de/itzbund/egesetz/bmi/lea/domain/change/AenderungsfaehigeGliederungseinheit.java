// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.change;

import org.json.simple.JSONObject;

import java.util.List;

public interface AenderungsfaehigeGliederungseinheit {

    Aenderungsart getAenderungsart();

    String getAenderungsstelle();

    List<JSONObject> getAenderungsbefehle();

    JSONObject getIntroObject();

}
