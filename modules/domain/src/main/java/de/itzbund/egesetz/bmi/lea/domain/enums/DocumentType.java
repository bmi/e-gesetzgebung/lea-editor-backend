// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentForm;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt.ANSCHREIBEN;
import static de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt.BEGRUENDUNG;
import static de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt.DRUCKSACHE;
import static de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt.OFFENE_STRUKTUR;
import static de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt.RECHTSETZUNGSDOKUMENT;
import static de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt.REGELUNGSTEXT;
import static de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt.VORBLATT;
import static de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentForm.MANTELFORM;
import static de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentForm.NICHT_VORHANDEN;
import static de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentForm.STAMMFORM;
import static de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp.EXTERNES_DOKUMENT;
import static de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp.GESETZ;
import static de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp.SONSTIGE_BEKANNTMACHUNG;
import static de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp.VERORDNUNG;

/**
 * Enumeration of different types of document that the editor supports.
 */
@RequiredArgsConstructor
@Getter
public enum DocumentType {

    ANLAGE(OFFENE_STRUKTUR, NICHT_VORHANDEN, SONSTIGE_BEKANNTMACHUNG),
    ANSCHREIBEN_MANTELGESETZ(ANSCHREIBEN, MANTELFORM, GESETZ),
    ANSCHREIBEN_MANTELVERORDNUNG(ANSCHREIBEN, MANTELFORM, VERORDNUNG),
    ANSCHREIBEN_STAMMGESETZ(ANSCHREIBEN, STAMMFORM, GESETZ),
    ANSCHREIBEN_STAMMVERORDNUNG(ANSCHREIBEN, STAMMFORM, VERORDNUNG),
    BEGRUENDUNG_MANTELGESETZ(BEGRUENDUNG, MANTELFORM, GESETZ),
    BEGRUENDUNG_MANTELVERORDNUNG(BEGRUENDUNG, MANTELFORM, VERORDNUNG),
    BEGRUENDUNG_STAMMGESETZ(BEGRUENDUNG, STAMMFORM, GESETZ),
    BEGRUENDUNG_STAMMVERORDNUNG(BEGRUENDUNG, STAMMFORM, VERORDNUNG),
    RECHTSETZUNGSDOKUMENT_MANTELGESETZ(RECHTSETZUNGSDOKUMENT, MANTELFORM, GESETZ),
    RECHTSETZUNGSDOKUMENT_MANTELVERORDNUNG(RECHTSETZUNGSDOKUMENT, MANTELFORM, VERORDNUNG),
    RECHTSETZUNGSDOKUMENT_STAMMGESETZ(RECHTSETZUNGSDOKUMENT, STAMMFORM, GESETZ),
    RECHTSETZUNGSDOKUMENT_STAMMVERORDNUNG(RECHTSETZUNGSDOKUMENT, STAMMFORM, VERORDNUNG),
    REGELUNGSTEXT_MANTELGESETZ(REGELUNGSTEXT, MANTELFORM, GESETZ),
    REGELUNGSTEXT_MANTELVERORDNUNG(REGELUNGSTEXT, MANTELFORM, VERORDNUNG),
    REGELUNGSTEXT_STAMMGESETZ(REGELUNGSTEXT, STAMMFORM, GESETZ),
    REGELUNGSTEXT_STAMMVERORDNUNG(REGELUNGSTEXT, STAMMFORM, VERORDNUNG),
    SYNOPSE(RechtsetzungsdokumentArt.SYNOPSE, RechtsetzungsdokumentForm.SYNOPSE, RechtsetzungsdokumentTyp.SYNOPSE),
    VORBLATT_MANTELGESETZ(VORBLATT, MANTELFORM, GESETZ),
    VORBLATT_MANTELVERORDNUNG(VORBLATT, MANTELFORM, VERORDNUNG),
    VORBLATT_STAMMGESETZ(VORBLATT, STAMMFORM, GESETZ),
    VORBLATT_STAMMVERORDNUNG(VORBLATT, STAMMFORM, VERORDNUNG),
    BT_DRUCKSACHE(DRUCKSACHE, NICHT_VORHANDEN, EXTERNES_DOKUMENT);

    private static final Pattern PTN_TEMPLATE_ID_WITH_LEVEL = Pattern.compile("(\\w+-\\w+)-\\d+");

    private static final Map<String, DocumentType> TYPES_FOR_TEMPLATE_ID = new HashMap<>();
    private static final Map<String, DocumentType> TYPES_FOR_HASH = new HashMap<>();

    static {
        Arrays.stream(DocumentType.values())
            .forEach(t -> {
                TYPES_FOR_TEMPLATE_ID.put(t.getTemplateID(), t);
                TYPES_FOR_HASH.put(getHashKey(t.art, t.form, t.typ), t);
            });
    }

    private final RechtsetzungsdokumentArt art;
    private final RechtsetzungsdokumentForm form;
    private final RechtsetzungsdokumentTyp typ;


    public static DocumentType getFromTemplateID(String templateID) {
        if (templateID == null || templateID.isBlank()) {
            return null;
        }

        Matcher matcher = PTN_TEMPLATE_ID_WITH_LEVEL.matcher(templateID);
        if (matcher.matches()) {
            return TYPES_FOR_TEMPLATE_ID.get(matcher.group(1));
        } else {
            return TYPES_FOR_TEMPLATE_ID.get(templateID);
        }
    }

    private static String getHashKey(RechtsetzungsdokumentArt art, RechtsetzungsdokumentForm form,
        RechtsetzungsdokumentTyp typ) {
        RechtsetzungsdokumentForm simplifiedForm = form == RechtsetzungsdokumentForm.MANTELFORM
            ? RechtsetzungsdokumentForm.MANTELFORM
            : RechtsetzungsdokumentForm.STAMMFORM;
        String key = art == OFFENE_STRUKTUR
            ? ANLAGE.name()
            : String.format("%s|%s|%s", art, simplifiedForm, typ);
        return Utils.bytesToHex(Utils.getHash(Utils.DigestAlgorithm.SHA1, key, StandardCharsets.UTF_8));
    }

    public static DocumentType getFromTypology(RechtsetzungsdokumentArt art, RechtsetzungsdokumentForm form,
        RechtsetzungsdokumentTyp typ) {
        return TYPES_FOR_HASH.get(getHashKey(art, form, typ));
    }

    public String getTemplateID() {
        switch (this) {
            case ANLAGE:
            case SYNOPSE:
                return this.name()
                    .toLowerCase();
            default:
                return String.format("%s%s-%s", getPrefix(form.getLiteral()), typ.getLiteral(), art.getLiteral()); //NOSONAR
        }
    }

    public String getReadableName() {
        switch (this) {
            case ANLAGE:
                return Utils.getTitleCase(this.name(), true);
            case RECHTSETZUNGSDOKUMENT_MANTELGESETZ:
            case RECHTSETZUNGSDOKUMENT_STAMMGESETZ:
            case RECHTSETZUNGSDOKUMENT_STAMMVERORDNUNG:
                return art.getLabel();
            default:
                return String.format("%s%s-%s", getPrefix(form.getLabel()), typ.getLiteral(), art.getLabel()); //NOSONAR
        }
    }

    // to be used in creation of collection document ('Rechtsetzungsdokument') and identifying document type on import
    public String getShowAsLiteral() {
        if (this == DocumentType.ANLAGE) {
            return OFFENE_STRUKTUR.getLiteral();
        }
        return this.getArt().getLiteral();
    }

    private String getPrefix(String form) {
        return form.replace("form", "");
    }

}
