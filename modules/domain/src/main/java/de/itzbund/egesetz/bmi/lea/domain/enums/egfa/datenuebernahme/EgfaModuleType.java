// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums.egfa.datenuebernahme;

import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaId;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * indicates different eGFA module types (order of eGFA modules is defined here!)
 */
@Getter
@AllArgsConstructor
public enum EgfaModuleType {

    ERFUELLUNGSAUFWAND(EgfaId.ERFUELLUNGSAUFWAND_ERGEBNISDOKUMENTATION),
    EA_OEHH(EgfaId.AUSWIRKUNGEN_EINNAHMEN_AUSGABEN_ERGEBNISDOKUMENTATION),
    PREISE(EgfaId.PREIS_ERGEBNISDOKUMENTATION_BEGRUENDUNG),
    KMU(EgfaId.KMU_ERGEBNISDOKUMENTATION_BEGRUENDUNG),
    SONSTIGE_KOSTEN(EgfaId.SONSTIGE_KOSTEN_ERGEBNISDOKUMENTATION_BEGRUENDUNG),
    ENAP(EgfaId.ENAP_ERGEBNISDOKUMENTATION),
    GLEICHWERTIGKEIT(EgfaId.GLEICHWERTIGKEITS_CHECK_ERGEBNISDOKUMENTATION),
    DEMOGRAFIE(EgfaId.DEMOGRAFIE_CHECK_ERGEBNISDOKUMENTATION),
    GLEICHSTELLUNG(EgfaId.GLEICHSTELLUNGSORIENTIERTE_ERGEBNISDOKUMENTATION),
    DISABILITY(EgfaId.DISABILITY_ERGEBNISDOKUMENTATION),
    VERBRAUCHER(EgfaId.VERBRAUCHER_ERGEBNISDOKUMENTATION),
    WEITERE(EgfaId.WEITERE_ERGEBNISDOKUMENTATION),
    EVALUIERUNG(EgfaId.EVALUIERUNG_ERGEBNISDUKUMENTATION);

    private final EgfaId egfaId;

}
