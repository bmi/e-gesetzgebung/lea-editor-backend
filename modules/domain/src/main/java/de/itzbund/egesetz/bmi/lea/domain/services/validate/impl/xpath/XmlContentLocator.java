// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.xpath;

import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * An object of {@link XmlContentLocator} takes <b>pretty-printed</b> XML content and generates a Map that assigns for all elements their XPath to the line
 * number they occur in an virtual text file.
 */
@Log4j2
@SuppressWarnings({"java:S5852", "java:S5843"})
public class XmlContentLocator {

    private static final Pattern PATT_XML_ELEMENT_START = Pattern.compile("\\s*<(([\\w.]+:)?[\\w.]+).*");
    private static final Pattern PATT_XML_ELEMENT_END =
        Pattern.compile("\\s*(<((([\\w.]+:)?[\\w.]+).*/)>|</(([\\w.]+:)?[\\w.]+)>)");

    private final Map<Integer, String> locationsMapping = new HashMap<>();

    private final Map<String, Map<String, Integer>> children = new HashMap<>();


    /**
     * During initialization the internal Map of line numbers and XPaths is created.
     *
     * @param ris the XML content, must be pretty-printed to have all elements on their own line
     */
    public void initialize(ReusableInputSource ris) {
        LineNumberReader reader = new LineNumberReader(new InputStreamReader(ris.getByteStream()));
        String line;
        String xpathStr = "";

        try {
            while ((line = reader.readLine()) != null) {
                Matcher matcher = PATT_XML_ELEMENT_START.matcher(line);
                if (matcher.find()) {
                    String componentName = matcher.group(1);
                    int position = getPosition(xpathStr, componentName);
                    xpathStr = XPath.parse(xpathStr).addComponent(
                        new XPathComponent(componentName, position)).toString();
                    locationsMapping.put(reader.getLineNumber(), xpathStr);
                }

                matcher = PATT_XML_ELEMENT_END.matcher(line);
                if (matcher.find()) {
                    xpathStr = XPath.getParentString(xpathStr);
                }
            }
        } catch (IOException e) {
            log.error(e);
        }
    }


    /**
     * Returns the XPath of the element to be found at the given line number.
     *
     * @param lineNumber the line number of the element
     * @return the XPath string of the element
     */
    public String getXPath(int lineNumber) {
        return locationsMapping.get(lineNumber);
    }


    private int getPosition(String parentPath, String childName) {
        Map<String, Integer> counters = children.get(parentPath);

        if (counters == null) {
            counters = new HashMap<>();
            children.put(parentPath, counters);
            counters.put(childName, 1);
            return 1;
        } else {
            Integer counter = counters.get(childName);
            if (counter == null) {
                counters.put(childName, 1);
                return 1;
            } else {
                int newValue = counter + 1;
                counters.put(childName, newValue);
                return newValue;
            }
        }
    }

}
