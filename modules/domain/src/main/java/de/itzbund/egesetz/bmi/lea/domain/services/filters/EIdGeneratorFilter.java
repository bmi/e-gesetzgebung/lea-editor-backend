// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.filters;

import de.itzbund.egesetz.bmi.lea.domain.services.eid.EIdGeneratorService;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.IllegalDocumentType;
import lombok.extern.log4j.Log4j2;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
@SuppressWarnings("unused")
public class EIdGeneratorFilter implements XmlFilter {

    @Autowired
    private EIdGeneratorService eIdGeneratorService;


    @Override
    public void doFilter(JSONObject jsonObject, FilterChain chain) {
        // Hier wird eine Aktion auf der Anfrage durchgeführt, bevor die FilterChain fortgesetzt wird.
        log.info("EIdGeneratorFilter called.");

        try {
            jsonObject = eIdGeneratorService.erzeugeEIdsFuerDokument(jsonObject);
        } catch (IllegalDocumentType e) {
            log.error("EIdGeneratorFilter: ", e);
        }

        chain.doFilter(jsonObject);
    }
}