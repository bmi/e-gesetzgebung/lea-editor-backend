// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.change;

import de.itzbund.egesetz.bmi.lea.core.json.TraversableJSONDocument;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.AenderungsbefehlRefersToLiteral;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.json.simple.JSONObject;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_ENDQUOTE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_GUID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_HREF;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_REFERSTO;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_STARTQUOTE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BILL;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_INTRO;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_MAINBODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_META;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_MOD;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_P;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PROPRIETARY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_QUOTEDSTRUCTURE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_REF;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_TYPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_TAGNAME_LDML_DE_METADATEN;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addAttributes;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDescendant;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getEffectiveTextValue;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getLocalName;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getNumElement;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getTextWrapper;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewDefaultJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewJSONObjectWithText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.withDefaultPrefix;
import static de.itzbund.egesetz.bmi.lea.domain.change.Gliederungseinheit.GE_ABSATZ;
import static de.itzbund.egesetz.bmi.lea.domain.change.Gliederungseinheit.GE_ABSCHNITT;
import static de.itzbund.egesetz.bmi.lea.domain.change.Gliederungseinheit.GE_ARTIKEL;
import static de.itzbund.egesetz.bmi.lea.domain.change.Gliederungseinheit.GE_BUCH;
import static de.itzbund.egesetz.bmi.lea.domain.change.Gliederungseinheit.GE_BUCHSTABE;
import static de.itzbund.egesetz.bmi.lea.domain.change.Gliederungseinheit.GE_DOPPELBUCHSTABE;
import static de.itzbund.egesetz.bmi.lea.domain.change.Gliederungseinheit.GE_DREIFACHBUCHSTABE;
import static de.itzbund.egesetz.bmi.lea.domain.change.Gliederungseinheit.GE_KAPITEL;
import static de.itzbund.egesetz.bmi.lea.domain.change.Gliederungseinheit.GE_NUMMER;
import static de.itzbund.egesetz.bmi.lea.domain.change.Gliederungseinheit.GE_PARAGRAPH;
import static de.itzbund.egesetz.bmi.lea.domain.change.Gliederungseinheit.GE_TEIL;
import static de.itzbund.egesetz.bmi.lea.domain.change.Gliederungseinheit.GE_TITEL;
import static de.itzbund.egesetz.bmi.lea.domain.change.Gliederungseinheit.GE_UNTERABSCHNITT;
import static de.itzbund.egesetz.bmi.lea.domain.change.Gliederungseinheit.GE_UNTERKAPITEL;
import static de.itzbund.egesetz.bmi.lea.domain.change.Gliederungseinheit.GE_UNTERTITEL;
import static de.itzbund.egesetz.bmi.lea.domain.services.eid.Abkuerzungsliteral.ABSCHNITT;
import static de.itzbund.egesetz.bmi.lea.domain.services.eid.Abkuerzungsliteral.ARTIKEL;
import static de.itzbund.egesetz.bmi.lea.domain.services.eid.Abkuerzungsliteral.BUCH;
import static de.itzbund.egesetz.bmi.lea.domain.services.eid.Abkuerzungsliteral.JURISTISCHER_ABSATZ;
import static de.itzbund.egesetz.bmi.lea.domain.services.eid.Abkuerzungsliteral.KAPITEL;
import static de.itzbund.egesetz.bmi.lea.domain.services.eid.Abkuerzungsliteral.LISTENELEMENT_UNTERGLIEDERUNG;
import static de.itzbund.egesetz.bmi.lea.domain.services.eid.Abkuerzungsliteral.PARAGRAPH;
import static de.itzbund.egesetz.bmi.lea.domain.services.eid.Abkuerzungsliteral.TEIL;
import static de.itzbund.egesetz.bmi.lea.domain.services.eid.Abkuerzungsliteral.TITEL;
import static de.itzbund.egesetz.bmi.lea.domain.services.eid.Abkuerzungsliteral.UNTERABSCHNITT;
import static de.itzbund.egesetz.bmi.lea.domain.services.eid.Abkuerzungsliteral.UNTERKAPITEL;
import static de.itzbund.egesetz.bmi.lea.domain.services.eid.Abkuerzungsliteral.UNTERTITEL;

@Log4j2
public class AenderungsbefehlUtils {

    public static final String GERMAN_START_QUOTE = "„";
    public static final String GERMAN_END_QUOTE = "“";

    private static final Pattern PTN_PARA_NUM = Pattern.compile("\\((\\d+)[a-z]?\\)");

    private static final Pattern PTN_POINT_NUM = Pattern.compile("listenelem-((\\d+[a-z]?)|([a-z]+))");

    private static final Set<String> BODY_ELEMENTS = Stream.of(ELEM_BODY, ELEM_MAINBODY)
        .collect(Collectors.toCollection(HashSet::new));

    private AenderungsbefehlUtils() {
    }


    @Getter
    @RequiredArgsConstructor
    public enum Gliederungsebene {
        BUCHSTABE(1, GE_BUCHSTABE.getBezeichnung()),
        DOPPELBUCHSTABE(2, GE_DOPPELBUCHSTABE.getBezeichnung()),
        DREIFACHBUCHSTABE(3, GE_DREIFACHBUCHSTABE.getBezeichnung());

        private static final Gliederungsebene[] gliederungsebenen = new Gliederungsebene[]{
            BUCHSTABE, DOPPELBUCHSTABE, DREIFACHBUCHSTABE
        };

        final int repeatCount;
        final String name;

        public static String getNameForCount(int count) {
            if (0 < count && count < 4) { //NOSONAR
                return gliederungsebenen[count - 1].getName();
            } else {
                return BUCHSTABE.getName();
            }
        }
    }


    public static String getAenderungsstelle(String eId) {
        String result = eId;

        if (Utils.isMissing(eId)) {
            return "";
        }

        int pos = eId.lastIndexOf("_");
        if (pos >= 0) {
            result = eId.substring(pos + 1);
        }

        if (result.startsWith(LISTENELEMENT_UNTERGLIEDERUNG.getAbkuerzung())) {
            Matcher matcher = PTN_POINT_NUM.matcher(result);
            if (matcher.matches()) {
                String num = matcher.group(1);
                String bez = getListElemName(matcher);
                return String.format("%s %s", bez, num);
            } else {
                log.error("invalid eId: {}", eId);
                return result;
            }
        } else {
            return result.replace("-", " ")
                .replace(BUCH.getAbkuerzung(), GE_BUCH.getBezeichnung())
                .replace(TEIL.getAbkuerzung(), GE_TEIL.getBezeichnung())
                .replace(ABSCHNITT.getAbkuerzung(), GE_ABSCHNITT.getBezeichnung())
                .replace(UNTERABSCHNITT.getAbkuerzung(), GE_UNTERABSCHNITT.getBezeichnung())
                .replace(KAPITEL.getAbkuerzung(), GE_KAPITEL.getBezeichnung())
                .replace(UNTERKAPITEL.getAbkuerzung(), GE_UNTERKAPITEL.getBezeichnung())
                .replace(TITEL.getAbkuerzung(), GE_TITEL.getBezeichnung())
                .replace(UNTERTITEL.getAbkuerzung(), GE_UNTERTITEL.getBezeichnung())
                .replace(ARTIKEL.getAbkuerzung(), GE_ARTIKEL.getBezeichnung())
                .replace(PARAGRAPH.getAbkuerzung(), GE_PARAGRAPH.getBezeichnung())
                .replace(JURISTISCHER_ABSATZ.getAbkuerzung(), GE_ABSATZ.getBezeichnung());
        }
    }


    @SuppressWarnings("java:S109")
    private static String getListElemName(Matcher matcher) {
        String name = GE_NUMMER.getBezeichnung();
        if (matcher.group(2) == null) {
            String alphas = matcher.group(3);
            int count = alphas.length();
            name = Gliederungsebene.getNameForCount(count);
        }
        return name;
    }


    public static int getPrecedingParaNum(String paraNumText) {
        if (Utils.isMissing(paraNumText)) {
            return 0;
        } else {
            Matcher m = PTN_PARA_NUM.matcher(paraNumText);
            if (m.matches()) {
                int number = Integer.parseInt(m.group(1));
                return number - 1;
            } else {
                return 0;
            }
        }
    }


    public static JSONObject getModIntroObject(String location) {
        JSONObject introObject = makeNewDefaultJSONObject(ELEM_INTRO);
        JSONObject pObject = makeNewDefaultJSONObject(ELEM_P);
        JSONObject refObject = makeNewJSONObjectWithText(ELEM_REF, location, true, false);
        addAttribute(refObject, Pair.of(ATTR_HREF, ""));
        addChildren(pObject, refObject, getTextWrapper(" wird wie folgt geändert:"));
        addChildren(introObject, pObject);
        return introObject;
    }


    @SuppressWarnings("unchecked")
    public static JSONObject getAenderungsbefehlSkeleton(Aenderungsart aenderungsart) {
        JSONObject aenderungsbefehlObject = new JSONObject();
        addAttributes(aenderungsbefehlObject,
            Pair.of(JSON_KEY_TYPE, withDefaultPrefix(ELEM_MOD)),
            Pair.of(ATTR_GUID, UUID.randomUUID().toString()),
            Pair.of(ATTR_REFERSTO, getRefersToLiteral(aenderungsart))
        );
        return aenderungsbefehlObject;
    }


    private static String getRefersToLiteral(Aenderungsart aenderungsart) {
        switch (aenderungsart) {
            case EINFUEGEN:
                return AenderungsbefehlRefersToLiteral.AENDERUNGSBEFEHL_NEUFASSUNG.getLiteral();
            case STREICHEN:
                return AenderungsbefehlRefersToLiteral.AENDERUNGSBEFEHL_STREICHEN.getLiteral();
            default:
                return AenderungsbefehlRefersToLiteral.AENDERUNGSBEFEHL_ERSETZEN.getLiteral();
        }
    }


    public static RechtsetzungsdokumentTyp getDocumentType(JSONObject documentObject) {
        JSONObject metaTypeObject = getDescendant(documentObject, false,
            p(ELEM_BILL), p(ELEM_META), p(ELEM_PROPRIETARY), m(JSON_TAGNAME_LDML_DE_METADATEN), m("typ"));
        String typeText = getText(metaTypeObject);
        return RechtsetzungsdokumentTyp.fromLiteral(typeText);
    }


    public static String getListPointNumValue(int count, Gliederungsebene gliederungsebene) {
        return String.valueOf((char) (((count - 1) % 26) + 97)).repeat(gliederungsebene.getRepeatCount()); //NOSONAR
    }


    private static String p(String element) {
        return withDefaultPrefix(element);
    }


    private static String m(String element) {
        return String.format("meta:%s", element);
    }


    public static String getAenderungsstelleUebergeordneteGliederungseinheit(TraversableJSONDocument document, JSONObject gliederungseinheit) {
        Deque<String> artUndZaehlBezeichnungen = new ArrayDeque<>();
        JSONObject child = gliederungseinheit;
        JSONObject ancestor;

        artUndZaehlBezeichnungen.push(getEffectiveTextValue(getNumElement(gliederungseinheit)));

        while ((ancestor = document.getParent(child)) != null && !BODY_ELEMENTS.contains(getLocalName(getType(ancestor)))) {
            artUndZaehlBezeichnungen.push(getEffectiveTextValue(getNumElement(ancestor)));
            child = ancestor;
        }

        return String.join(" ", artUndZaehlBezeichnungen);
    }


    @SuppressWarnings("unchecked")
    public static JSONObject getQuotedStructure(JSONObject... structureElements) {
        JSONObject quotedStructure = makeNewJSONObject(ELEM_QUOTEDSTRUCTURE, true, false);
        addAttributes(quotedStructure,
            Pair.of(ATTR_STARTQUOTE, GERMAN_START_QUOTE),
            Pair.of(ATTR_ENDQUOTE, GERMAN_END_QUOTE)
        );
        addChildren(quotedStructure, structureElements);
        return quotedStructure;
    }

}
