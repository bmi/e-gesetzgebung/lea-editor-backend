// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.eli;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.entities.DocumentCount;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.services.export.ExportContext;
import de.itzbund.egesetz.bmi.lea.domain.services.meta.MetadatenUpdateService;
import lombok.experimental.UtilityClass;
import org.json.simple.JSONObject;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static de.itzbund.egesetz.bmi.lea.core.Constants.META_LDMLDE_INITIANT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.META_LDMLDE_INSTITUTION;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDocumentObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getText;

@UtilityClass
public class EliMetadatenUtil {

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    // Definiere das gewünschte Datumsformat (Jahr-Monat-Tag)
    private static final DateTimeFormatter DT_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);

    private static final String AUTHOR_HREF_LITERAL_PREFIX = "recht.bund.de/institution";
    private static final String COMMON_ELI_PREFIX = "eli/dl";


    /**
     * Collects the EliMetadata
     */
    public Map<FRBRElement, Map<FRBRAttribute, String>> collectEliMetaDaten(ExportContext exportContext) {
        DokumentPersistencePort documentRepository = exportContext.getDocumentRepository();
        PropositionDTO proposition = exportContext.getPropositionDTO();
        CompoundDocumentDomain compoundDocument = exportContext.getCompoundDocumentDomain();
        DocumentDomain document = exportContext.getDocumentEntity();

        // Die Tabelle
        Map<FRBRElement, Map<FRBRAttribute, String>> eliMetaData = new EnumMap<>(FRBRElement.class);
        initializeWithConstants(eliMetaData);

        // Dokumentinhalt
        JSONObject documentObject = getDocumentObject(document.getContent());
        List<DocumentCount> documentCountList = documentRepository.getDocumentCount(compoundDocument.getCompoundDocumentId());
        Map<DocumentId, Integer> documentCounts = documentCountList.stream()
            .collect(Collectors.toMap(DocumentCount::getDocumentId, DocumentCount::getCount));

        collectWorkData(eliMetaData, proposition, documentObject, document, documentCounts);
        collectExpressionData(eliMetaData, compoundDocument, documentObject);
        collectManifestationData(eliMetaData, document);

        return eliMetaData;
    }


    private void collectWorkData(Map<FRBRElement, Map<FRBRAttribute, String>> eliMetaData,
        PropositionDTO proposition, JSONObject documentObject, DocumentDomain document, Map<DocumentId, Integer> documentCounts) {
        String aliasValue = proposition == null || proposition.getId() == null ? "" : proposition.getId().toString();
        eliMetaData.get(FRBRElement.WORK_FRBR_ALIAS).put(FRBRAttribute.FRBR_VALUE, aliasValue);

        String dateDate = proposition == null || proposition.getBearbeitetAm() == null ? "" : getFormattedDate(proposition.getBearbeitetAm());
        eliMetaData.get(FRBRElement.WORK_FRBR_DATE).put(FRBRAttribute.FRBR_DATE, dateDate);

        String authorHRef = getAuthorHRef(documentObject, META_LDMLDE_INITIANT);
        eliMetaData.get(FRBRElement.WORK_FRBR_AUTHOR).put(FRBRAttribute.FRBR_HREF, authorHRef);

        String subTypeValue = getSubtypeValue(document, documentCounts);
        eliMetaData.get(FRBRElement.WORK_FRBR_SUBTYPE).put(FRBRAttribute.FRBR_VALUE, subTypeValue);

        setFRBRuriValue(FRBRElement.WORK_FRBR_URI, eliMetaData);
        setFRBRthisValue(FRBRElement.WORK_FRBR_THIS, eliMetaData);
    }


    private void collectExpressionData(Map<FRBRElement, Map<FRBRAttribute, String>> eliMetaData,
        CompoundDocumentDomain compoundDocument, JSONObject documentObject) {
        String authorHRef = getAuthorHRef(documentObject, META_LDMLDE_INSTITUTION);
        eliMetaData.get(FRBRElement.EXPRESSION_FRBR_AUTHOR).put(FRBRAttribute.FRBR_HREF, authorHRef);

        String dateDate = compoundDocument == null || compoundDocument.getCreatedAt() == null ? "" : getFormattedDate(compoundDocument.getCreatedAt());
        eliMetaData.get(FRBRElement.EXPRESSION_FRBR_DATE).put(FRBRAttribute.FRBR_DATE, dateDate);

        setFRBRuriValue(FRBRElement.EXPRESSION_FRBR_URI, eliMetaData);
        setFRBRthisValue(FRBRElement.EXPRESSION_FRBR_THIS, eliMetaData);
    }


    private void collectManifestationData(Map<FRBRElement, Map<FRBRAttribute, String>> eliMetaData, DocumentDomain document) {
        String dateDate = document == null || document.getCreatedAt() == null ? "" : getFormattedDate(document.getCreatedAt());
        eliMetaData.get(FRBRElement.MANIFESTATION_FRBR_DATE).put(FRBRAttribute.FRBR_DATE, dateDate);

        setFRBRuriValue(FRBRElement.MANIFESTATION_FRBR_URI, eliMetaData);
        setFRBRthisValue(FRBRElement.MANIFESTATION_FRBR_THIS, eliMetaData);
    }


    private void initializeWithConstants(Map<FRBRElement, Map<FRBRAttribute, String>> eliMetaData) {
        Arrays.stream(FRBRElement.values()).forEach(e -> eliMetaData.put(e, new EnumMap<>(FRBRAttribute.class)));
        eliMetaData.get(FRBRElement.WORK_FRBR_ALIAS).put(FRBRAttribute.FRBR_NAME, "übergreifende-id");
        eliMetaData.get(FRBRElement.WORK_FRBR_DATE).put(FRBRAttribute.FRBR_NAME, "entwurfsfassung");
        eliMetaData.get(FRBRElement.WORK_FRBR_COUNTRY).put(FRBRAttribute.FRBR_VALUE, "de");
        eliMetaData.get(FRBRElement.WORK_FRBR_NUMBER).put(FRBRAttribute.FRBR_VALUE, "1");
        eliMetaData.get(FRBRElement.WORK_FRBR_NAME).put(FRBRAttribute.FRBR_VALUE, "regelungsentwurf");
        eliMetaData.get(FRBRElement.EXPRESSION_FRBR_DATE).put(FRBRAttribute.FRBR_NAME, "version");
        eliMetaData.get(FRBRElement.EXPRESSION_FRBR_LANGUAGE).put(FRBRAttribute.FRBR_LANG, "deu");
        eliMetaData.get(FRBRElement.EXPRESSION_FRBR_VERSION_NUMBER).put(FRBRAttribute.FRBR_VALUE, "1");
        eliMetaData.get(FRBRElement.MANIFESTATION_FRBR_DATE).put(FRBRAttribute.FRBR_NAME, "generierung");
        eliMetaData.get(FRBRElement.MANIFESTATION_FRBR_AUTHOR).put(FRBRAttribute.FRBR_HREF, "recht.bund.de");
        eliMetaData.get(FRBRElement.MANIFESTATION_FRBR_FORMAT).put(FRBRAttribute.FRBR_VALUE, "xml");
    }


    private void setFRBRuriValue(FRBRElement frbrElement, Map<FRBRElement, Map<FRBRAttribute, String>> eliMetaData) {
        String uri;
        Group1FRBREntity level = frbrElement.getFrbrEntity();
        switch (level) {
            case WORK:
                uri = getWorkUri(eliMetaData);
                break;
            case EXPRESSION:
                uri = getExpressionUri(eliMetaData);
                break;
            default:
                uri = getManifestationUri(eliMetaData);
        }
        eliMetaData.get(frbrElement).put(FRBRAttribute.FRBR_VALUE, uri);
    }


    private void setFRBRthisValue(FRBRElement frbrElement, Map<FRBRElement, Map<FRBRAttribute, String>> eliMetaData) {
        String uri;
        Group1FRBREntity level = frbrElement.getFrbrEntity();
        String subtype = eliMetaData.get(FRBRElement.WORK_FRBR_SUBTYPE).get(FRBRAttribute.FRBR_VALUE);
        switch (level) {
            case WORK:
                uri = String.join("/", eliMetaData.get(FRBRElement.WORK_FRBR_URI).get(FRBRAttribute.FRBR_VALUE), subtype);
                break;
            case EXPRESSION:
                uri = String.join("/", eliMetaData.get(FRBRElement.EXPRESSION_FRBR_URI).get(FRBRAttribute.FRBR_VALUE), subtype);
                break;
            default:
                uri = eliMetaData.get(FRBRElement.MANIFESTATION_FRBR_URI).get(FRBRAttribute.FRBR_VALUE);
        }
        eliMetaData.get(frbrElement).put(FRBRAttribute.FRBR_VALUE, uri);
    }


    private String getWorkUri(Map<FRBRElement, Map<FRBRAttribute, String>> eliMetaData) {
        String dateDate = eliMetaData.get(FRBRElement.WORK_FRBR_DATE).get(FRBRAttribute.FRBR_DATE);
        String year = getYearFromDateString(dateDate);
        String authorHRef = eliMetaData.get(FRBRElement.WORK_FRBR_AUTHOR).get(FRBRAttribute.FRBR_HREF);
        String agent = authorHRef.substring(authorHRef.lastIndexOf('/') + 1);
        String processId = eliMetaData.get(FRBRElement.WORK_FRBR_NUMBER).get(FRBRAttribute.FRBR_VALUE);
        String docType = eliMetaData.get(FRBRElement.WORK_FRBR_NAME).get(FRBRAttribute.FRBR_VALUE);
        return String.join("/", COMMON_ELI_PREFIX, year, agent, processId, docType);
    }


    private String getExpressionUri(Map<FRBRElement, Map<FRBRAttribute, String>> eliMetaData) {
        String authorHRef = eliMetaData.get(FRBRElement.EXPRESSION_FRBR_AUTHOR).get(FRBRAttribute.FRBR_HREF);
        String subagent = authorHRef.substring(authorHRef.lastIndexOf('/') + 1);
        String dateDate = eliMetaData.get(FRBRElement.EXPRESSION_FRBR_DATE).get(FRBRAttribute.FRBR_DATE);
        String versionNum = eliMetaData.get(FRBRElement.EXPRESSION_FRBR_VERSION_NUMBER).get(FRBRAttribute.FRBR_VALUE);
        String lang = eliMetaData.get(FRBRElement.EXPRESSION_FRBR_LANGUAGE).get(FRBRAttribute.FRBR_LANG);
        String workUri = eliMetaData.get(FRBRElement.WORK_FRBR_URI).get(FRBRAttribute.FRBR_VALUE);
        return String.join("/", workUri, subagent, dateDate, versionNum, lang);
    }


    private String getManifestationUri(Map<FRBRElement, Map<FRBRAttribute, String>> eliMetaData) {
        String exprUri = eliMetaData.get(FRBRElement.EXPRESSION_FRBR_THIS).get(FRBRAttribute.FRBR_VALUE);
        String format = eliMetaData.get(FRBRElement.MANIFESTATION_FRBR_FORMAT).get(FRBRAttribute.FRBR_VALUE);
        return String.join(".", exprUri, format);
    }


    private String getFormattedDate(Instant dateTimeValue) {
        // Konvertiere Instant in LocalDateTime
        LocalDateTime localDateTime = LocalDateTime.ofInstant(dateTimeValue, ZoneId.systemDefault());
        return localDateTime.format(DT_FORMATTER);
    }


    private String getYearFromDateString(String dateString) {
        return String.valueOf(LocalDate.parse(dateString).getYear());
    }


    private String getAuthorHRef(JSONObject documentObject, String field) {
        JSONObject metaObject = MetadatenUpdateService.getMetaDataField(documentObject, field);
        String metadata = getText(metaObject);
        return Utils.isMissing(metadata) ? "" : String.format("%s/%s", AUTHOR_HREF_LITERAL_PREFIX, metadata);
    }


    private String getSubtypeValue(DocumentDomain document, Map<DocumentId, Integer> documentCounts) {
        DocumentId documentId = document.getDocumentId();
        DocumentType documentType = document.getType();
        RechtsetzungsdokumentArt art = documentType.getArt();
        int count = documentCounts.getOrDefault(documentId, 0);

        if (count == 0 && RechtsetzungsdokumentArt.RECHTSETZUNGSDOKUMENT.equals(documentType.getArt())) {
            count = 1;
        }

        String docArt = art.getLiteral();
        if (RechtsetzungsdokumentArt.VORBLATT == art) {
            docArt = "vorblattregelungstext";
        }

        return String.format("%s-%s", docArt, count);
    }

}
