// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa;

import de.itzbund.egesetz.bmi.lea.domain.model.Drucksache;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;

import java.util.List;

public interface DrucksachenPersistencePort {

    List<Drucksache> findAll();

    Drucksache findByDrucksachenNr(String drucksachenNummer);

    Drucksache findByRegelungsvorhabenId(RegelungsVorhabenId regelungsVorhabenId);

    Drucksache save(Drucksache drucksache);

}
