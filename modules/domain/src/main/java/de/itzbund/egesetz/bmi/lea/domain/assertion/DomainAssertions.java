// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.assertion;

import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CONTENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_INTRO;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LIST;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_P;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TABLE;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getLocalName;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.withDefaultPrefix;

@SuppressWarnings("java:S106")
public class DomainAssertions {

    private static final HashSet<String> PARA_CONTENT_TYPES = Stream.of(ELEM_CONTENT, ELEM_LIST).collect(Collectors.toCollection(HashSet::new));

    private static final String CONTENT_TYPE = withDefaultPrefix(ELEM_CONTENT);
    private static final String INTRO_TYPE = withDefaultPrefix(ELEM_INTRO);

    private DomainAssertions() {
    }

    public static void assertNotEmptyChildrenArray(JSONArray children) {
        assert children != null && !children.isEmpty();
    }

    public static void assertSizeIsAtLeast(JSONArray children, int min) {
        assert children != null && children.size() >= min;
    }

    public static void assertWrapUpHasExactlyOneP(JSONObject wrapUpObject) {
        JSONArray children;
        assert wrapUpObject != null
            && (children = getChildren(wrapUpObject)) != null
            && children.size() == 1
            && ELEM_P.equals(getLocalName(getType((JSONObject) children.get(0))));
    }

    @SuppressWarnings({"java:S1067", "java:S1541"})
    public static void assertComparableTypes(String baseType, String versionType) {
        assert baseType != null
            && versionType != null
            && (
            (baseType.isBlank() && !versionType.isBlank())
                || (!baseType.isBlank() && versionType.isBlank())
                || baseType.equals(versionType)
                || (baseType.equals(CONTENT_TYPE) && versionType.equals(INTRO_TYPE))
                || (baseType.equals(INTRO_TYPE) && versionType.equals(CONTENT_TYPE))
        );
    }

    @SuppressWarnings({"java:S1067", "java:S125"})
    public static void assertConsistentCompareResult(JSONObject baseObject, JSONObject versionObject, DiffMatchPatch.Operation operation) {
        /*assert (baseObject == null && operation == DiffMatchPatch.Operation.INSERT)
            || (versionObject == null && operation == DiffMatchPatch.Operation.DELETE)
            || (operation == DiffMatchPatch.Operation.EQUAL);*/
    }

    public static void assertTableObjectNotNull(JSONObject tableObject) {
        assert tableObject != null
            && !tableObject.isEmpty()
            && withDefaultPrefix(ELEM_TABLE).equals(getType(tableObject))
            && getChildren(tableObject).isEmpty();
    }

    public static void assertValidParaContent(String baseType, String versionType) {
        assert PARA_CONTENT_TYPES.contains(baseType) && PARA_CONTENT_TYPES.contains(versionType);
    }

}
