// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model.dokument;

import lombok.NonNull;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public interface DeserializableFromJSON {

    Object fromJSON(@NonNull JSONObject jsonObject);

    @SuppressWarnings("unchecked")
    default List<Object> fromJSON(@NonNull JSONArray jsonArray) {
        return new ArrayList<>(jsonArray);
    }

    default Object fromJSON(@NonNull String jsonString) {
        JSONParser jsonParser = new JSONParser();
        Object obj;

        try {
            obj = jsonParser.parse(jsonString);
        } catch (ParseException e) {
            throw new IllegalArgumentException("string is not parsable as JSON: " + jsonString, e);
        }

        if (obj instanceof JSONObject) {
            return fromJSON((JSONObject) obj);
        } else if (obj instanceof JSONArray) {
            return fromJSON((JSONArray) obj);
        } else {
            return null;
        }
    }

}
