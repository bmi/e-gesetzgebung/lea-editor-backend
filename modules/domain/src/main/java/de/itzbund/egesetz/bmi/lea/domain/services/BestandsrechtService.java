// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentShortDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.DMBestandsrechtLinkDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtListDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtShortDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.Rights;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.Bestandsrecht;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.BestandsrechtId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.BestandsrechtPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.BestandsrechtRestPort;
import de.itzbund.egesetz.bmi.lea.domain.services.exceptions.DokumentenmappeNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static de.itzbund.egesetz.bmi.lea.core.Constants.RESSOURCENTYP_DOKUMENTENMAPPE;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.isEffectivelyEmpty;
import static de.itzbund.egesetz.bmi.lea.domain.enums.Rights.SCHREIBEN;

@Service
@RequiredArgsConstructor
@Log4j2
@SuppressWarnings("unused")
public class BestandsrechtService implements BestandsrechtRestPort {

	private static final int FIRST_ELEMENT = 0;

	@Autowired
	private BestandsrechtPersistencePort bestandsrechtRepository;
	@Autowired
	private ConverterService converterService;
	@Autowired
	private CompoundDocumentService compoundDocumentService;
	@Autowired
	private UserService userService;
	@Autowired
	private RBACPermissionService rbacPermissionService;
	@Autowired
	private ApplicationContext applicationContext;

	public void bestandsrechtDatenSpeicherung(String gid, UUID regelungsvorhabenId,
		BestandsrechtDTO bestandsrechtDTO, List<ServiceState> status) {

		// Try to transform to JSON (includes the XML validation)
		String xmlContent = bestandsrechtDTO.getContent();
		String jsonContent = null;
		String transformationMessage = null;
		try {
			jsonContent = converterService.convertXmlToJson(xmlContent);
		} catch (RuntimeException e) {
			log.error(e);
			transformationMessage = e.toString();
		}
		if (jsonContent == null || jsonContent.isBlank() || !Utils.isJSONValid(jsonContent)) {
			status.add(ServiceState.DOCUMENT_NOT_VALID);
			jsonContent = null;
		}
		Bestandsrecht bestandsrecht = Bestandsrecht.builder().bestandsrechtId(new BestandsrechtId(UUID.randomUUID())).eli(
				bestandsrechtDTO.getEli()
			).titelKurz(bestandsrechtDTO.getTitelKurz()).titelLang(bestandsrechtDTO.getTitelLang()).contentOriginal(bestandsrechtDTO.getContent())
			.contentJson(jsonContent).transformationMessages(transformationMessage).build();

		bestandsrechtRepository.save(bestandsrecht, new RegelungsVorhabenId(regelungsvorhabenId), status);
		if (status.isEmpty()) {
			status.add(ServiceState.OK);
		}
	}

	/**
	 * Retrieves all Bestandsrecht associated to the RV of the CompoundDocument
	 *
	 * @param compoundDocumentId The Id of the Regelungsvorhaben
	 * @param status             A status field
	 */
	@Override
	public List<BestandsrechtListDTO> getBestandsrechtForCompoundDocumentId(UUID compoundDocumentId, List<ServiceState> status) {
		RegelungsVorhabenId regelungsVorhabenId = getRegelungsvorhabenIdFor(new CompoundDocumentId(compoundDocumentId));
		if (regelungsVorhabenId == null) {
			throw new DokumentenmappeNotFoundException("Dokumentenmappe with id '" + compoundDocumentId + "' not found");
		}

		List<BestandsrechtId> bestandsrechtIds = bestandsrechtRepository.getBestandsrechtAssociationsByRvId(regelungsVorhabenId);
		List<BestandsrechtListDTO> bestandsrechtListDTOs = bestandsrechtRepository.getBestandsrechtEntitiesById(bestandsrechtIds);

		status.add(ServiceState.OK);
		return bestandsrechtListDTOs;
	}


	@Override
	public List<DMBestandsrechtLinkDTO> getBestandsrechtLinks(UUID compoundDocumentId, List<ServiceState> status) {
		List<BestandsrechtListDTO> bestandsrechtList = getBestandsrechtForCompoundDocumentId(compoundDocumentId, status);
		if (isEffectivelyEmpty(bestandsrechtList)) {
			if (status.isEmpty()) {
				status.add(ServiceState.INVALID_STATE);
			}
			return Collections.emptyList();
		}

		status.clear();
		User userEntity = userService.getUser();
		RegelungsVorhabenId regelungsVorhabenId = getRegelungsvorhabenIdFor(new CompoundDocumentId(compoundDocumentId));
		CompoundDocument compoundDocument = applicationContext.getBean(CompoundDocument.class);
		List<CompoundDocument> dokumentMappen = compoundDocumentService.getDokumentenmappenFuerEinRegelungsvorhabenMitStatus(
			userEntity, regelungsVorhabenId, DocumentState.DRAFT);

		List<CompoundDocumentShortDTO> dmList = dokumentMappen.stream()
			.filter(cd -> {
				List<Rights> rechte = rbacPermissionService.getAccessRightsForCurrentUserOnGivenResource(
					RESSOURCENTYP_DOKUMENTENMAPPE, cd.getCompoundDocumentId().getId());
				return rechte.contains(SCHREIBEN);
			})
			.map(cd -> CompoundDocumentShortDTO.builder()
				.compoundDocumentId(cd.getCompoundDocumentId())
				.compoundDocumentTitle(cd.getCompoundDocumentEntity().getTitle())
				.build())
			.collect(Collectors.toList());

		List<DMBestandsrechtLinkDTO> resultList = bestandsrechtList.stream()
			.map(b -> DMBestandsrechtLinkDTO.builder()
				.bestandsrechtId(new BestandsrechtId(b.getId()))
				.bestandsrechtTitel(b.getTitelKurz())
				.verlinkteMappen(dmList)
				.build())
			.collect(Collectors.toList());

		if (isEffectivelyEmpty(resultList)) {
			status.add(ServiceState.INVALID_STATE);
			return Collections.emptyList();
		} else {
			status.add(ServiceState.OK);
			return resultList;
		}
	}

	@Override
	public List<BestandsrechtListDTO> getBestandsrechteFromRegelungsvorhabenEnhancedByCompoundDocumentAssignments(CompoundDocumentId compoundDocumentId,
		List<ServiceState> errorStates) {

		Optional<CompoundDocumentDomain> compoundDocument = compoundDocumentService.findByCompoundDocumentId(compoundDocumentId);
		if (compoundDocument.isEmpty()) {
			errorStates.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
			return Collections.emptyList();
		}

		List<BestandsrechtId> bestandsrechteIdsDesRegelungsvorhabens = bestandsrechtRepository.getBestandsrechtAssociationsByRvId(
			compoundDocument.get().getRegelungsVorhabenId());
		List<DMBestandsrechtLinkDTO> linkedCompoundDocuments = bestandsrechtRepository.getLinkedCompoundDocuments(bestandsrechteIdsDesRegelungsvorhabens);

		List<BestandsrechtListDTO> bestandsrechtListe = getBestandsrechtList(linkedCompoundDocuments, compoundDocument.get().getCompoundDocumentId());
		errorStates.add(ServiceState.OK);

		return bestandsrechtListe;
	}

	private List<BestandsrechtListDTO> getBestandsrechtList(List<DMBestandsrechtLinkDTO> linkedCompoundDocuments, CompoundDocumentId filter) {
		List<BestandsrechtListDTO> collect = linkedCompoundDocuments.stream().map(
				lcd -> {

					BestandsrechtId bestandsrechtId = lcd.getBestandsrechtId();

					List<BestandsrechtListDTO> bestandsrechtEntities = bestandsrechtRepository.getBestandsrechtEntitiesById(List.of(bestandsrechtId));
					if (bestandsrechtEntities.isEmpty()) {
						return null;
					}

					UUID verlinkteMappe = null;
					List<CompoundDocumentShortDTO> verlinkteMappen = lcd.getVerlinkteMappen();

					List<CompoundDocumentShortDTO> zielMappe = verlinkteMappen.stream()
						.filter(m -> m.getCompoundDocumentId().getId().equals(filter.getId()))
						.collect(Collectors.toList());

					if (!zielMappe.isEmpty()) {
						// Nach fachlicher Definition nur eine
						verlinkteMappe = zielMappe.get(0).getCompoundDocumentId().getId();
					}

					return BestandsrechtListDTO.builder()
						.id(bestandsrechtId.getId())
						.eli(bestandsrechtEntities.get(0).getEli())
						.titelLang(bestandsrechtEntities.get(0).getTitelLang())
						.titelKurz(bestandsrechtEntities.get(0).getTitelKurz())
						.verknuepfteDokumentenMappeId(verlinkteMappe)
						.build();

				}
			)
			.filter(Objects::nonNull)
			.collect(Collectors.toList());
		return collect;
	}

	@Override
	public List<BestandsrechtListDTO> setBestandsrechteForCompoundDocumentByCompoundDocumentId(CompoundDocumentId compoundDocumentId,
		List<BestandsrechtShortDTO> bestandsrechtDTOs, List<ServiceState> errorStates) {

		List<BestandsrechtId> failedBestandsrechtIds = bestandsrechtDTOs.stream().map(br ->
			assignBestandsrechtWithCompoundDocument(br, compoundDocumentId)
		).collect(Collectors.toList());

		// Fehler auswerten
		List<BestandsrechtId> errors = failedBestandsrechtIds.stream()
			.filter(result -> result != null)
			.collect(Collectors.toList());
		if (!errors.isEmpty()) {
			log.debug("Von {} wurden keine Bestandsrechte gefunden.", errors);
			errorStates.add(ServiceState.BESTANDSRECHT_NICHT_VORHANDEN);
		} else {
			errorStates.add(ServiceState.OK);
		}

		return getBestandsrechteFromRegelungsvorhabenEnhancedByCompoundDocumentAssignments(compoundDocumentId, new ArrayList<>());
	}

	private BestandsrechtId assignBestandsrechtWithCompoundDocument(BestandsrechtShortDTO br, CompoundDocumentId compoundDocumentId) {
		CompoundDocumentId verknuepfteDokumentenMappeId = null;
		BestandsrechtId bestandsrechtId = new BestandsrechtId(br.getId());
		if (br.getVerknuepfteDokumentenMappeId() != null) {
			verknuepfteDokumentenMappeId = new CompoundDocumentId(br.getVerknuepfteDokumentenMappeId());
		}

		if (verknuepfteDokumentenMappeId == null) {
			bestandsrechtRepository.deleteAssignmentForDokumentenMappeAndBestandsrecht(compoundDocumentId, new BestandsrechtId(br.getId()));
			return null;
		}

		CompoundDocument compoundDocument = compoundDocumentService.getCompoundDocumentDomainEntity(verknuepfteDokumentenMappeId);
		if (compoundDocument == null) {
			// Fehler: Dokumentenmappe existiert nicht
			return bestandsrechtId;
		}

		return insertLinkBestandsrechtDokumentenmappe(compoundDocument, br, new ArrayList<>());
	}


	private BestandsrechtId insertLinkBestandsrechtDokumentenmappe(CompoundDocument compoundDocument, BestandsrechtShortDTO bestandsrechtLinkDTO,
		List<BestandsrechtListDTO> successBestandsrecht) {

		// Check: Existing Bestandsrecht
		List<BestandsrechtListDTO> bestandsrechtEntities = bestandsrechtRepository.getBestandsrechtEntitiesById(
			List.of(new BestandsrechtId(bestandsrechtLinkDTO.getId())));
		if (bestandsrechtEntities.isEmpty()) {
			return new BestandsrechtId(bestandsrechtLinkDTO.getId());
		}

		try {
			bestandsrechtRepository.save(compoundDocument.getCompoundDocumentId(),
				compoundDocument.getCompoundDocumentEntity().getTitle(),
				new BestandsrechtId(bestandsrechtLinkDTO.getId()),
				// Nicht so gut
				bestandsrechtLinkDTO.getId().toString()
			);
			successBestandsrecht.add(bestandsrechtEntities.get(FIRST_ELEMENT));
		} catch (DataIntegrityViolationException e) {
			log.debug("Prevented double insertions", e);
		}

		return null;
	}

	private RegelungsVorhabenId getRegelungsvorhabenIdFor(CompoundDocumentId compoundDocumentId) {
		CompoundDocument compoundDocument = compoundDocumentService.getCompoundDocumentDomainEntity(compoundDocumentId);
		return compoundDocument == null ? null : compoundDocument.getRegelungsVorhabenId();
	}

}
