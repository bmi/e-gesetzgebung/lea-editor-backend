// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate;

import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationData;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;

/**
 * An abstract definition of an XML validation service.
 */
@Service
public interface ValidationService {

    /**
     * Validates the given XML content and stores validaten results in a {@link ValidationData} object.
     *
     * @param xml XML input encapsulated in an {@link InputSource}
     * @return results of the validation
     */
    ValidationData validateLDML(InputSource xml);

}
