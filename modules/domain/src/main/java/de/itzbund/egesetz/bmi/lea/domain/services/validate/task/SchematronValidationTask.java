// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.task;

import com.helger.commons.io.streamprovider.ByteArrayInputStreamProvider;
import com.helger.schematron.ISchematronResource;
import com.helger.schematron.svrl.AbstractSVRLMessage;
import com.helger.schematron.svrl.SVRLFailedAssert;
import com.helger.schematron.svrl.SVRLHelper;
import com.helger.schematron.svrl.jaxb.SchematronOutputType;
import com.helger.schematron.xslt.SchematronResourceXSLT;
import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.ValidationResult;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.LeaValidationResult;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.SeverityLevel;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationData;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationException;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationMessage;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

import static de.itzbund.egesetz.bmi.lea.core.Constants.SVRL_FAILED_ASSERT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.SVRL_SUCCESSFUL_REPORT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.VALIDATION_WARN;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.xmlNormalize;

/**
 * This kind of {@link XmlValidationTask} is based on a rule-based validation method called
 * <a href="https://schematron.com/">ISO Schematron</a>.
 */

@Log4j2
@Component
@Qualifier("schematron")
@Scope("prototype")
public class SchematronValidationTask extends AbstractValidationTask
    implements RuleBasedValidationTask, XmlValidationTask {

    private static final String TASK_DESC = "validates an XML instance against a ISO Schematron Schema";

    private ReusableInputSource input;

    private ISchematronResource ruleset;

    private ValidationResult result = LeaValidationResult.VALIDATION_NOT_PERFORMED;


    @Override
    public String getDescription() {
        return TASK_DESC;
    }


    @Override
    public ValidationResult run(ValidationData data) throws ValidationException {
        if (ruleset == null) {
            throw new ValidationException("no ruleset defined");
        } else if (!ruleset.isValidSchematron()) {
            throw new ValidationException("no valid schematron ruleset provided");
        } else if (input == null || input.getByteStream() == null) {
            throw new ValidationException("no input provided");
        } else {
            SchematronOutputType svrl;

            try {
                svrl = ruleset.applySchematronValidationToSVRL(
                    new ByteArrayInputStreamProvider(input.getByteStream().readAllBytes()));
            } catch (Exception e) {
                throw new ValidationException("schematron validation failed for technical reasons", e);
            }

            if (svrl == null) {
                result = LeaValidationResult.VALIDATION_ERROR;
            } else {
                result = determineValidationResult(svrl, data);
            }
        }
        return result;
    }


    private ValidationResult determineValidationResult(SchematronOutputType svrl, ValidationData data) {
        List<AbstractSVRLMessage> failures = SVRLHelper.getAllFailedAssertionsAndSuccessfulReports(svrl);
        if (failures.isEmpty()) {
            return LeaValidationResult.VALIDATION_SUCCESS;
        }

        for (AbstractSVRLMessage failure : failures) {
            String test = xmlNormalize(failure.getTest());
            Optional<String> role = Optional.ofNullable(failure.getRole());
            String text = xmlNormalize(failure.getText());
            String loc = xmlNormalize(failure.getLocation());
            String type = failure instanceof SVRLFailedAssert ? SVRL_FAILED_ASSERT : SVRL_SUCCESSFUL_REPORT;
            String msg = String.format("Schematron: (%s) %s at location %s for test %s", type, text, loc, test);
            if (role.isPresent() && role.get().equals(VALIDATION_WARN)) {
                data.addMessage(new ValidationMessage(this, SeverityLevel.WARNING, msg));

            } else {
                data.addMessage(new ValidationMessage(this, SeverityLevel.ERROR, msg));
            }
        }

        return LeaValidationResult.VALIDATION_FAILURE;
    }


    @Override
    public ValidationResult getResult() {
        return result;
    }


    @Override
    public void setInputSource(ReusableInputSource input) {
        this.input = input;
    }


    @Override
    public void setRuleset(Object ruleset) {
        if (ruleset instanceof SchematronResourceXSLT) {
            this.ruleset = (SchematronResourceXSLT) ruleset;
        } else {
            log.error("wrong format of schematron ruleset, expected is XSLT");
        }
    }
}
