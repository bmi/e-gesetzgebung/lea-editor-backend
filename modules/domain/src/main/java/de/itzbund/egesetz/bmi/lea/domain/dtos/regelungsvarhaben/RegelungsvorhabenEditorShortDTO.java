// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben;

import de.itzbund.egesetz.bmi.lea.domain.enums.FarbeType;
import de.itzbund.egesetz.bmi.lea.domain.enums.RegelungsvorhabenTypType;
import de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

/**
 * represents Regelungsvorhaben with very basic information for Editor
 */
@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@ToString
public class RegelungsvorhabenEditorShortDTO {

    @NotNull
    @Schema(name = "id", example = "00000000-0000-0000-0000-000000000005", required = true)
    private UUID id;

    @NotBlank
    @Schema(name = "abkuerzung", example = "RR Verwaltungsvorschrift: Abkürzung", required = true)
    private String abkuerzung;

    @NotBlank
    @Schema(name = "kurzbezeichnung", example = "RR Verwaltungsvorschrift: Titel", required = true)
    private String kurzbezeichnung;

    @Schema(name = "kurzbeschreibung", example = "RR Verwaltungsvorschrift: Kurzbeschreibung", required = true)
    private String kurzbeschreibung;

    @NotNull
    @Schema(name = "status", example = "IN_BEARBEITUNG", required = true)
    private VorhabenStatusType status;

    @NotNull
    @Schema(name = "vorhabenart", example = "VERWALTUNGSVORSCHRIFT", required = true)
    private RegelungsvorhabenTypType vorhabenart;

    @NotNull
    @Schema(name = "bearbeitetAm", example = "2022-07-14T16:09:57.637388Z", required = true)
    private Instant bearbeitetAm;

    private FarbeType farbe;

}
