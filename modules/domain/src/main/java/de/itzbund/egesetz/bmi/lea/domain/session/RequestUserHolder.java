// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.session;

import de.itzbund.egesetz.bmi.lea.domain.model.User;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import java.util.concurrent.atomic.AtomicReference;

/**
 * holds reference to the current user for editor
 */
@Component
@RequestScope
@Log4j2
class RequestUserHolder {

    private final AtomicReference<User> currentUser = new AtomicReference<>();


    protected void set(User userEntity) {
        currentUser.set(userEntity);
    }


    protected User get() {
        return currentUser.get();
    }

}
