// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate;

/**
 * Interface for extendable Enum definitions of constant names for XML Schemas. It defines the common type of Objects that store a reference to a schema file
 * under a certain name.
 */
public interface SchemaInstanceName {

    String getSchemaFilePath();

}
