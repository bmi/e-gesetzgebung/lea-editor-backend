// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.eid;

import de.itzbund.egesetz.bmi.lea.domain.ports.eid.JSONContext;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.mutable.MutableInt;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class JSONContextImpl implements JSONContext {

    private final Map<String, MutableInt> typeCounts = new HashMap<>();

    @Getter
    @Setter
    private String praefix = "";

    private int orderedListElemCount = 0;


    public static JSONContext createFrom(Map<String, Integer> map) {
        JSONContextImpl jsonContext = new JSONContextImpl();
        map.keySet().forEach(s -> jsonContext.copyCount(s, new MutableInt(map.get(s))));

        return jsonContext;
    }


    @Override
    public void addTypeOccurrence(String type) {
        MutableInt count = typeCounts.get(type);
        if (count == null) {
            count = new MutableInt(1);
            typeCounts.put(type, count);
        } else {
            count.increment();
        }
    }


    @Override
    public int getCurrentTypeCount(String type) {
        MutableInt count = typeCounts.get(type);
        return count == null ? 0 : count.getValue();
    }


    @Override
    public JSONContext copyWithIncrement(String type) {
        JSONContextImpl copy = new JSONContextImpl();
        copy.setPraefix(this.getPraefix());

        this.typeCounts.keySet().forEach(t -> {
            MutableInt oldCount = this.typeCounts.get(t);
            if (Objects.equals(t, type)) {
                oldCount.increment();
                copy.copyCount(t, oldCount);
            } else {
                copy.copyCount(t, oldCount);
            }
        });

        if (!this.typeCounts.containsKey(type) && type != null) {
            copy.copyCount(type, new MutableInt(1));
        }

        return copy;
    }


    @Override
    public void initializeOrderedList(int count) {
        orderedListElemCount = Math.max(count, 0);
    }


    @Override
    public void decreaseList() {
        orderedListElemCount--;
    }


    @Override
    public boolean isProcessingOrderedList() {
        return orderedListElemCount > 0;
    }


    private void copyCount(String type, MutableInt count) {
        typeCounts.put(type, count);
    }

}
