// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.synopsis;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserDTO;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import java.time.OffsetDateTime;

/**
 * SynopsisResponseDTO
 */

@Data
public class SynopsisResponseDTO {

    @JsonProperty("type")
    private TypeEnum type;
    @JsonProperty("erstellt")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private OffsetDateTime erstellt;
    @JsonProperty("ersteller")
    private UserDTO ersteller;
    @JsonProperty("document")
    @Valid
    private DocumentDTO document = null;

    public SynopsisResponseDTO type(TypeEnum type) {
        this.type = type;
        return this;
    }

    public SynopsisResponseDTO erstellt(OffsetDateTime erstellt) {
        this.erstellt = erstellt;
        return this;
    }

    public SynopsisResponseDTO ersteller(UserDTO ersteller) {
        this.ersteller = ersteller;
        return this;
    }

    public SynopsisResponseDTO document(DocumentDTO document) {
        this.document = document;
        return this;
    }


    /**
     * Gets or Sets type
     */
    public enum TypeEnum {
        SYNOPSE("SYNOPSE");

        private final String value;


        TypeEnum(String value) {
            this.value = value;
        }

        @JsonCreator
        public static TypeEnum fromValue(String value) {
            for (TypeEnum b : TypeEnum.values()) {
                if (b.value.equals(value)) {
                    return b;
                }
            }
            throw new IllegalArgumentException("Unexpected value '" + value + "'");
        }

        @JsonValue
        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
    }

}
