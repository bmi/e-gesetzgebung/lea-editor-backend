// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Status des Regelungsvorhaben
 */
public enum PropositionState {
    ARCHIVIERT("ARCHIVIERT"),
    IN_BEARBEITUNG("IN_BEARBEITUNG"),
    ENTWURF("ENTWURF"),
    BUNDESTAG("An Bundestag weitergeleitet");

    private final String value;

    PropositionState(String value) {
        this.value = value;
    }

    @JsonCreator
    @SuppressWarnings("unused")
    public static PropositionState fromValue(String text) {
        for (PropositionState b : PropositionState.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        return null;
    }
    
    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }
}
