// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.VerfahrensType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Data
@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
public class UpdateCompoundDocumentDTO {

    @JsonProperty("title")
    @Schema(name = "title", example = "New title for compound document")
    private String title;

    @JsonProperty("verfahrensTyp")
    @Schema(name = "verfahrensTyp", example = "REGIERUNGSENTWURF")
    private VerfahrensType verfahrensTyp;

    @JsonProperty("status")
    @Schema(name = "status", example = "BEREIT_FUER_KABINETTVERFAHREN")
    private DocumentState state;

}
