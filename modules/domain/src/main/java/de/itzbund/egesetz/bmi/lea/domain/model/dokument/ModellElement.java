// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model.dokument;

import de.itzbund.egesetz.bmi.lea.core.Constants;
import de.itzbund.egesetz.bmi.lea.domain.model.version.LegalDocMLDeVersion;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
@SuppressWarnings("unchecked")
public abstract class ModellElement implements SerializableAsJSON, DeserializableFromJSON {

    @NotNull
    @Size(min = 1)
    protected final List<LegalDocMLDeVersion> supportedVersions;

    @NotNull
    protected final ModellElementTyp typ;


    protected void addJSONAttribute(JSONObject jsonObject, String attribute, String value) {
        if (value != null) {
            jsonObject.put(attribute, value);
        }
    }


    protected void addChild(JSONObject parent, JSONObject child) {
        if (parent != null && child != null && !child.isEmpty()) {
            JSONArray children = (JSONArray) parent.computeIfAbsent(Constants.JSON_KEY_CHILDREN, k -> new JSONArray());
            children.add(child);
        }
    }

}
