// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model.dokument;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum TeildokumentVerweisShowAsLiteral implements ShowAsLiteral {

    REGELUNGSTEXT("regelungstext"),
    VORBLATT("vorblatt"),
    BEGRUENDUNG("begruendung"),
    ANSCHREIBEN("anschreiben"),
    VEREINBARUNG("vereinbarung"),
    DENKSCHRIFT("denkschrift"),
    BEKANNTMACHUNGSTEXT("bekanntmachungstext"),
    OFFENE_STRUKTUR("offene-struktur"),
    EXTERNES_DOKUMENT("externes-dokument");

    private static final Map<String, TeildokumentVerweisShowAsLiteral> literals = new HashMap<>();

    static {
        Arrays.stream(values()).forEach(l -> literals.put(l.getLiteral(), l));
    }

    private final String literal;


    public static TeildokumentVerweisShowAsLiteral fromString(String literal) {
        return literals.get(literal);
    }
}
