// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.eid;

import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentForm;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_A;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ABBR;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ACT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ACTIVEMODIFICATIONS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_AFFECTEDDOCUMENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_AKOMANTOSO;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ANALYSIS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ARTICLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ATTACHMENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ATTACHMENTS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_AUTHORIALNOTE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_B;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BILL;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BLOCK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BLOCKCONTAINER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BLOCKLIST;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BOOK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BR;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CAPTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CHAPTER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CITATION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CITATIONS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_COLLECTIONBODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_COMPONENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_COMPONENTREF;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_COMPONENTS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CONCLUSIONS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CONTENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DATE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DESTINATION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DOC;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DOCAUTHORITY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DOCDATE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DOCNUMBER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DOCPROPONENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DOCSTAGE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DOCTITLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DOCTYPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DOCUMENTCOLLECTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DOCUMENTREF;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_EOL;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_EOP;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_EVENTREF;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FORCE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FORCEMOD;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FOREIGN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FORMULA;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRALIAS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRAUTHOR;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRCOUNTRY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRDATE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBREXPRESSION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRFORMAT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRLANGUAGE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRMANIFESTATION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRNAME;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRNUMBER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRSUBTYPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRTHIS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRURI;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRVERSIONNUMBER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRWORK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_HCONTAINER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_HEADING;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_I;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_IDENTIFICATION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_IMG;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_INLINE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_INTRO;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ITEM;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LEGISLATURE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LI;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LIFECYCLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LIST;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LISTINTRODUCTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LISTWRAPUP;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LOCATION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LONGTITLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_MAINBODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_MARKER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_META;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_MOD;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_NEW;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_NUM;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_OL;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ORGANIZATION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_P;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PARAGRAPH;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PART;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PASSIVEMODIFICATIONS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PERSON;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_POINT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PREAMBLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PREFACE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PROPRIETARY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_QUOTEDSTRUCTURE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_QUOTEDTEXT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_RECITAL;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_RECITALS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_REF;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_RELATEDDOCUMENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ROLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SECTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SESSION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SHORTTITLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SIGNATURE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SOURCE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SPAN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_STATEMENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SUB;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SUBCHAPTER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SUBSECTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SUBTITLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SUP;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TABLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TBLOCK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TD;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TEMPORALDATA;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TEMPORALGROUP;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TEXTUALMOD;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TH;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TIMEINTERVAL;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TITLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TOC;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TOCITEM;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TR;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_U;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_UL;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_WRAPUP;
import static de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentForm.MANTELFORM;
import static de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentForm.STAMMFORM;

@RequiredArgsConstructor
@Getter
@SuppressWarnings({"java:S1640", "java:S1192", "java:S109"})
public enum Abkuerzungsliteral {

    ABKUERZUNG(p(ELEM_ABBR), "abbr", STAMMFORM),
    ABSCHNITT(p(ELEM_SECTION), "abschnitt", STAMMFORM),
    AENDERUNGSBEFEHL(p(ELEM_MOD), "ändbefehl", STAMMFORM),
    AKTIVE_AENDERUNG(p(ELEM_ACTIVEMODIFICATIONS), "activemod", STAMMFORM),
    ANALYSIS(p(ELEM_ANALYSIS), "analysis", STAMMFORM),
    ANLAGE(p(ELEM_ATTACHMENT), "anlage", STAMMFORM),
    ANLAGEN(p(ELEM_ATTACHMENTS), "anlagen", STAMMFORM),
    ARTIKEL(p(ELEM_ARTICLE), "art", MANTELFORM),
    ART_UND_ZAEHLBEZEICHNUNG(p(ELEM_NUM), "bezeichnung", STAMMFORM),
    BETROFFENES_DOKUMENT(p(ELEM_AFFECTEDDOCUMENT), "bezugsdoc", STAMMFORM),
    BEZUGSDOKUMENT(p(ELEM_RELATEDDOCUMENT), "bezugsdok", STAMMFORM),
    BILD(p(ELEM_IMG), "bild", STAMMFORM),
    BLOCK(p(ELEM_BLOCK), "block", STAMMFORM),
    BUCH(p(ELEM_BOOK), "buch", STAMMFORM),
    CONTAINER(p(ELEM_HCONTAINER), "container", STAMMFORM),
    DATUM(p(ELEM_DATE), "datum", STAMMFORM),
    DOCUMENT_AUTHORITY(p(ELEM_DOCAUTHORITY), "docauth", STAMMFORM),
    DOCUMENT_COLLECTION(p(ELEM_DOCUMENTCOLLECTION), null, STAMMFORM),
    DOCUMENT_DATUM(p(ELEM_DOCDATE), "docdatum", STAMMFORM),
    DOCUMENT_PROPONENT(p(ELEM_DOCPROPONENT), "docproponent", STAMMFORM),
    DOCUMENT_ROOT(p(ELEM_AKOMANTOSO), null, STAMMFORM),
    DOCUMENT_STAGE(p(ELEM_DOCSTAGE), "docstadium", STAMMFORM),
    DOCUMENT_TITEL(p(ELEM_DOCTITLE), "doctitel", STAMMFORM),
    DOCUMENT_TYPE(p(ELEM_DOCTYPE), "doctype", STAMMFORM),
    DOKUMENT_TITEL(p(ELEM_LONGTITLE), "doktitel", STAMMFORM),
    DRUCKSACHEN_NUMMER(p(ELEM_DOCNUMBER), "drucksachennr", STAMMFORM),
    EIGENE_METADATEN(p(ELEM_PROPRIETARY), "proprietary", STAMMFORM),
    EINGANGSFORMEL_ABSCHNITT(p(ELEM_PREAMBLE), "preambel", STAMMFORM),
    EINLEITUNG(p(ELEM_PREFACE), "einleitung", STAMMFORM),
    EREIGNIS(p(ELEM_EVENTREF), "ereignis", STAMMFORM),
    ERMAECHTIGUNGSNORM(p(ELEM_CITATION), "ernorm", STAMMFORM),
    ERMAECHTIGUNGSNORMEN(p(ELEM_CITATIONS), "ernormen", STAMMFORM),
    EXTERNES_MARKUP(p(ELEM_FOREIGN), "exmarkup", STAMMFORM),
    FETT_SCHRIFT(p(ELEM_B), "fettschrift", STAMMFORM),
    FORMEL(p(ELEM_FORMULA), "formel", STAMMFORM),
    FRBR_ALIAS(p(ELEM_FRBRALIAS), "frbralias", STAMMFORM),
    FRBR_AUTHOR(p(ELEM_FRBRAUTHOR), "frbrauthor", STAMMFORM),
    FRBR_COUNTRY(p(ELEM_FRBRCOUNTRY), "frbrcountry", STAMMFORM),
    FRBR_DATE(p(ELEM_FRBRDATE), "frbrdate", STAMMFORM),
    FRBR_EXPRESSION(p(ELEM_FRBREXPRESSION), "frbrexpression", STAMMFORM),
    FRBR_FORMAT(p(ELEM_FRBRFORMAT), "frbrformat", STAMMFORM),
    FRBR_LANGUAGE(p(ELEM_FRBRLANGUAGE), "frbrlanguage", STAMMFORM),
    FRBR_MANIFESTATION(p(ELEM_FRBRMANIFESTATION), "frbrmanifestation", STAMMFORM),
    FRBR_NAME(p(ELEM_FRBRNAME), "frbrname", STAMMFORM),
    FRBR_NUMBER(p(ELEM_FRBRNUMBER), "frbrnumber", STAMMFORM),
    FRBR_SUBTYPE(p(ELEM_FRBRSUBTYPE), "frbrsubtype", STAMMFORM),
    FRBR_THIS(p(ELEM_FRBRTHIS), "frbrthis", STAMMFORM),
    FRBR_URI(p(ELEM_FRBRURI), "frbruri", STAMMFORM),
    FRBR_VERSION_NUMBER(p(ELEM_FRBRVERSIONNUMBER), "frbrersionnumber", STAMMFORM),
    FRBR_WORK(p(ELEM_FRBRWORK), "frbrwork", STAMMFORM),
    FUNKTIONS_BEZEICHNUNG(p(ELEM_ROLE), "fktbez", STAMMFORM),
    FUSSNOTE(p(ELEM_AUTHORIALNOTE), "fnote", STAMMFORM),
    GELTUNGSZEITEN(p(ELEM_TEMPORALDATA), "geltzeiten", STAMMFORM),
    GELTUNGSZEIT_AENDERUNG(p(ELEM_FORCEMOD), "gelzeitaend", STAMMFORM),
    GELTUNGSZEIT_GRUPPE(p(ELEM_TEMPORALGROUP), "geltungszeitgr", STAMMFORM),
    GELTUNGSZEIT_INTERVALL(p(ELEM_TIMEINTERVAL), "gelzeitintervall", STAMMFORM),
    GELTUNGSZEIT_VERWEIS(p(ELEM_FORCE), "gelzeitnachw", STAMMFORM),
    GEORDNETE_LISTE(p(ELEM_OL), "listegeor", STAMMFORM),
    HOCH_STELLUNG(p(ELEM_SUP), "sup", STAMMFORM),
    HYPERLINK(p(ELEM_A), "a", STAMMFORM),
    IDENTIFIKATION(p(ELEM_IDENTIFICATION), "ident", STAMMFORM),
    INHALT(p(ELEM_CONTENT), "inhalt", STAMMFORM),
    INHALTSABSCHNITT(p(ELEM_TBLOCK), "inhaltabschnitt", STAMMFORM),
    INHALTSUEBERSICHT(p(ELEM_TOC), "inhuebs", STAMMFORM),
    INHALTSUEBERSICHT_EINTRAG(p(ELEM_TOCITEM), "eintrag", STAMMFORM),
    INLINE(p(ELEM_INLINE), "inline", STAMMFORM),
    JURISTISCHER_ABSATZ(p(ELEM_PARAGRAPH), "abs", STAMMFORM),
    JURISTISCHER_ABSATZ_UNTERGLIEDERUNG(p(ELEM_LIST), "untergl", STAMMFORM),
    KAPITEL(p(ELEM_CHAPTER), "kapitel", STAMMFORM),
    KURSIV_SCHRIFT(p(ELEM_I), "kursiv", STAMMFORM),
    KURZTITEL(p(ELEM_SHORTTITLE), "kurztitel", STAMMFORM),
    LEBENSZYKLUS(p(ELEM_LIFECYCLE), "lebzykl", STAMMFORM),
    LEGISLATUR_PERIODE(p(ELEM_LEGISLATURE), null, STAMMFORM),
    LISTE(p(ELEM_BLOCKLIST), "liste", STAMMFORM),
    LISTENELEMENT_LI(p(ELEM_LI), "listenelem", STAMMFORM),
    LISTENELEMENT_LISTE(p(ELEM_ITEM), "listenelem", STAMMFORM),
    LISTENELEMENT_UNTERGLIEDERUNG(p(ELEM_POINT), "listenelem", STAMMFORM),
    LISTE_EINGANGSSATZ(p(ELEM_LISTINTRODUCTION), "listeneing", STAMMFORM),
    LISTE_SCHLUSSSATZ(p(ELEM_LISTWRAPUP), "listenschl", STAMMFORM),
    METADATEN(p(ELEM_META), "meta", STAMMFORM),
    NEU(p(ELEM_NEW), "new", STAMMFORM),
    OFFENE_STRUKTUR(p(ELEM_DOC), null, STAMMFORM),
    OFFENE_STRUKTUR_HAUPTTEIL(p(ELEM_MAINBODY), "hauptteil", STAMMFORM),
    ORGANISATION(p(ELEM_ORGANIZATION), "org", STAMMFORM),
    ORT(p(ELEM_LOCATION), "ort", STAMMFORM),
    PARAGRAPH(p(ELEM_ARTICLE), "para", STAMMFORM),
    PASSIVE_AENDERUNG(p(ELEM_PASSIVEMODIFICATIONS), "pasmod", STAMMFORM),
    PERSON(p(ELEM_PERSON), "person", STAMMFORM),
    PRAEAMBEL(p(ELEM_RECITALS), "präambeln", STAMMFORM),
    PRAEAMBEL_INHALT(p(ELEM_RECITAL), "präambelinh", STAMMFORM),
    QUELLE(p(ELEM_SOURCE), "source", STAMMFORM),
    RECHTSETZUNGSDOKUMENT_HAUPTTEIL(p(ELEM_COLLECTIONBODY), "rdokhauptteil", STAMMFORM),
    REFERENZ(p(ELEM_REF), "ref", STAMMFORM),
    REGELUNGSTEXT_ENTWURFSFASSUNG(p(ELEM_BILL), null, STAMMFORM),
    REGELUNGSTEXT_HAUPTTEIL(p(ELEM_BODY), "hauptteil", STAMMFORM),
    REGELUNGSTEXT_VERKUENDUNGSFASSUNG(p(ELEM_ACT), null, STAMMFORM),
    SCHLUSS(p(ELEM_CONCLUSIONS), "schluss", STAMMFORM),
    SEITENENDE(p(ELEM_EOP), "eop", STAMMFORM),
    SIGNATUR(p(ELEM_SIGNATURE), "signatur", STAMMFORM),
    SITZUNG(p(ELEM_SESSION), "sitzung", STAMMFORM),
    SPAN(p(ELEM_SPAN), "span", STAMMFORM),
    STAMMFORM_VERWEIS(p(ELEM_COMPONENTREF), "stfmverweis", STAMMFORM),
    STATEMENT(p(ELEM_STATEMENT), null, STAMMFORM),
    TABELLE(p(ELEM_TABLE), "tabelle", STAMMFORM),
    TABELLENINHALT(p(ELEM_TD), "tabelleinh", STAMMFORM),
    TABELLENKOPF(p(ELEM_TH), "tabellekopf", STAMMFORM),
    TABELLENUEBERSCHRIFT(p(ELEM_CAPTION), "tblue", STAMMFORM),
    TABELLENZEILE(p(ELEM_TR), "tabellereihe", STAMMFORM),
    TEIL(p(ELEM_PART), "teil", STAMMFORM),
    TEILDOKUMENT_VERWEIS_CONTAINER(p(ELEM_COMPONENT), "tldokverweis", STAMMFORM),
    TEIL_DOKUMENT_VERWEISE(p(ELEM_COMPONENTS), "tldokverweise", STAMMFORM),
    TEXTABSATZ(p(ELEM_P), "text", STAMMFORM),
    TEXT_AENDERUNG(p(ELEM_TEXTUALMOD), "textualmod", STAMMFORM),
    TEXT_NACH_UNTERGLIEDERUNG(p(ELEM_WRAPUP), "schlusstext", STAMMFORM),
    TEXT_VOR_UNTERGLIEDERUNG(p(ELEM_INTRO), "intro", STAMMFORM),
    TIEF_STELLUNG(p(ELEM_SUB), "sub", STAMMFORM),
    TITEL(p(ELEM_TITLE), "titel", STAMMFORM),
    UEBERSCHRIFT(p(ELEM_HEADING), "überschrift", STAMMFORM),
    UNGEORDNETE_LISTE(p(ELEM_UL), "listeunge", STAMMFORM),
    UNTERABSCHNITT(p(ELEM_SUBSECTION), "uabschnitt", STAMMFORM),
    UNTERKAPITEL(p(ELEM_SUBCHAPTER), "ukapitel", STAMMFORM),
    UNTERSTRICHEN(p(ELEM_U), "u", STAMMFORM),
    UNTERTITEL(p(ELEM_SUBTITLE), "utitel", STAMMFORM),
    VERWEIS(p(ELEM_DOCUMENTREF), "verweis", STAMMFORM),
    VERZEICHNIS(p(ELEM_BLOCKCONTAINER), "blockcontainer", STAMMFORM),
    ZAEHLBEZEICHNUNG(p(ELEM_MARKER), "zaehlbez", STAMMFORM),
    ZEILENENDE(p(ELEM_EOL), "eol", STAMMFORM),
    ZEILENUMBRUCH(p(ELEM_BR), "br", STAMMFORM),
    ZIEL(p(ELEM_DESTINATION), "destination", STAMMFORM),
    ZITIERTER_TEXT(p(ELEM_QUOTEDTEXT), "quottext", STAMMFORM),
    ZITIERTE_STRUKTUR(p(ELEM_QUOTEDSTRUCTURE), "quotstruct", STAMMFORM);

    private static final Map<RechtsetzungsdokumentForm, Map<String, Abkuerzungsliteral>> literals = new HashMap<>();

    private static final Set<String> literalStrings = new HashSet<>();

    private static final String NS_PREFIX = "akn:";

    static {
        literals.put(STAMMFORM, new HashMap<>());
        literals.put(MANTELFORM, new HashMap<>());

        Arrays.stream(values()).forEach(literal -> {
                literals.get(literal.form).put(literal.elemName, literal);
                if (literal.abkuerzung != null) {
                    literalStrings.add(literal.abkuerzung);
                }
            }
        );
    }

    private final String elemName;
    private final String abkuerzung;
    private final RechtsetzungsdokumentForm form;


    public static String getLiteral(String elemName, RechtsetzungsdokumentForm rechtsetzungsdokumentForm) {
        String type = elemName.startsWith(NS_PREFIX) ? elemName : (NS_PREFIX + elemName);
        Abkuerzungsliteral literal = literals.get(rechtsetzungsdokumentForm).get(type);
        return literal == null ? "" : literal.getAbkuerzung();
    }


    public static boolean isValid(String literal) {
        return literalStrings.contains(literal);
    }


    private static String p(String localName) {
        return String.join(":", "akn", localName);
    }

}
