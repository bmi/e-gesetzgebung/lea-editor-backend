// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;

/**
 * represents content DTO for eGFA Datenübernahme to Editor
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
    @JsonSubTypes.Type(value = EgfaDatenuebernahmeContentTextDTO.class, name = "TEXT"),
    @JsonSubTypes.Type(value = EgfaDatenuebernahmeContentListDTO.class, name = "LIST"),
    @JsonSubTypes.Type(value = EgfaDatenuebernahmeContentTableDTO.class, name = "TABLE"),
    @JsonSubTypes.Type(value = EgfaDatenuebernahmeContentTableDTO.class, name = "BLOCK")
})
public abstract class EgfaDatenuebernahmeContentDTO {

    @NotNull
    private String id;
    @NotNull
    private EgfaItemType type;
    private String label;


    @NotNull
    public abstract EgfaItemType getType();
}
