// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.homepage;

import de.itzbund.egesetz.bmi.lea.domain.dtos.paging.FilterPropertyType;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.domain.Page;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
public class DokumentenmappeTableDTOs {

    @NotNull
    private Page<HomepageRegulatoryProposalDTO> dtos;
    @NotNull
    private List<DocumentType> vorhabenartFilter;
    @NotNull
    private List<FilterPropertyType> filterNames;
    @NotNull
    private boolean allContentEmpty;

}