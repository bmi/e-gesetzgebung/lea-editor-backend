// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.auth.api.enums.GetPermissionOptionType;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.EditorRollenUndRechte;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CopyCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.PermissionDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.Rights;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.core.Constants.RESSOURCENTYP_DOKUMENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RESSOURCENTYP_DOKUMENTENMAPPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RESSOURCENTYP_REGELUNGSVORHABEN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ROLLE_ERSTELLER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ROLLE_HRA_TEILNEHMER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ROLLE_LESER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ROLLE_SCHREIBER;

@Service
@Log4j2
public class RBACPermissionService {

    public static final String SCHREIBRECHTE_ENTFERNT = "SCHREIBRECHTE_ENTFERNT";
    public static final String LESERECHTE_HINZU = "LESERECHTE_HINZU";

    @Autowired
    private BenachrichtigungService benachrichtigungService;
    @Autowired
    private EditorRollenUndRechte editorRollenUndRechte;
    @Autowired
    private LeageSession session;

    // ====  Abfrage von Eigenschaften  ====

    // @formatter:off
    /**
     * Überprüft, ob die Dokumentenmappe Teil einer HRA war oder ist.
     * Wenn dies so ist, dann darf nur noch gelesen werden und die Dokumentenmappe nicht mehr
     * verändert werden. Eine neue Version darf angelegt werden.
     *
     * Wurde die Dokumentenmappe bei einer HRA verwendet, so gibt es Einträge der Teilnehmen.
     * Dieser Einträge werden nicht entfernt.
     *
     * @return
     */
    // @formatter:on
    public boolean istOderWarDieDokumentenmappeTeilEinerHRA(CompoundDocumentId compoundDocumentId) {
        List<String> listeAllerHRATeilnehmer = editorRollenUndRechte.getAlleBenutzerIdsByRessourceIdAndRolle(compoundDocumentId.getId().toString(),
            ROLLE_HRA_TEILNEHMER);
        return !listeAllerHRATeilnehmer.isEmpty();
    }

    // ====  Setzen von Rechten  ====

    // ----  Dokumentenmappen  ----

    /**
     * Fügt das Recht eines Erstellers für Dokumentenmappen hinzu
     *
     * @param copyCompoundDocumentDTO
     * @return
     */
    public boolean addCreatorPermissionsForCompoundDocument(CopyCompoundDocumentDTO copyCompoundDocumentDTO) {
        String compoundDocumentId = copyCompoundDocumentDTO.getCompoundDocumentId().getId().toString();
        String userId = session.getUser().getGid().getId();
        return addCreatorPermissionsForCompoundDocument(userId, compoundDocumentId);
    }

    /**
     * Fügt das Recht eines Erstellers für Dokumentenmappen hinzu
     *
     * @param compoundDocument
     * @return
     */
    public boolean addCreatorPermissionsForCompoundDocument(CompoundDocument compoundDocument) {
        String compoundDocumentId = compoundDocument.getCompoundDocumentId().getId().toString();
        String userId = session.getUser().getGid().getId();
        return addCreatorPermissionsForCompoundDocument(userId, compoundDocumentId);
    }

    /**
     * Fügt das Recht eines Erstellers für Dokumentenmappen hinzu
     *
     * @param userGid
     * @param compoundDocumentId
     * @return
     */
    public boolean addCreatorPermissionsForCompoundDocument(String userGid, CompoundDocumentId compoundDocumentId) {
        String compoundDocumentIdString = compoundDocumentId.getId().toString();
        return addCreatorPermissionsForCompoundDocument(userGid, compoundDocumentIdString);
    }

    private boolean addCreatorPermissionsForCompoundDocument(String userId, String compoundDocumentId) {
        List<String> alleRessourcIds = editorRollenUndRechte.getAlleRessourcenFuerBenutzerUndRolleUndRessourceTyp(
            userId, ROLLE_ERSTELLER, RESSOURCENTYP_DOKUMENTENMAPPE);

        boolean isAllowed = alleRessourcIds.contains(compoundDocumentId);

        log.debug("Add permission for DOKUMENTENMAPPE (Ersteller), was ALREADY allowed: {}", isAllowed);

        if (!isAllowed) {
            try {
                editorRollenUndRechte.addRessourcePermission(userId, RESSOURCENTYP_DOKUMENTENMAPPE, compoundDocumentId,
                    ROLLE_ERSTELLER);
            } catch (TransactionSystemException e) {
                log.debug("Constraint violation, but the entry itself is there. So it is ok.", e);
                return false;
            }
        }

        return true;
    }

    // ----  Dokumente  ----

    /**
     * Fügt das Recht eines Erstellers für Dokumente hinzu
     *
     * @param document
     * @return
     */
    public boolean addCreatorPermissionsForDocument(Document document) {
        String userId = session.getUser().getGid().getId();
        String documentId = document.getDocumentId().getId().toString();

        List<String> alleRessourcIds = editorRollenUndRechte.getAlleRessourcenFuerBenutzerUndRolleUndRessourceTyp(
            userId, ROLLE_ERSTELLER, RESSOURCENTYP_DOKUMENT);

        boolean isAllowed = alleRessourcIds.contains(documentId);

        log.debug("Add permission for DOKUMENTE (Ersteller), was allowed: {}", isAllowed);

        if (!isAllowed) {
            try {
                editorRollenUndRechte.addRessourcePermission(userId, "DOKUMENTE", documentId, "ERSTELLER");
            } catch (TransactionSystemException e) {
                log.debug("Constraint violation, but the entry itself is there. So it ist ok.", e);
                return false;
            }
        }
        return true;
    }

    // ====  Lesen von Rechten  ====

    // ----  Lese- und Schreibrechte für alle Ressourcenarten  ----

    // @formatter:off
    /**
     * Erzeugt eine List mit Rechten des Nutzers aus RBAC.
     *
     * Benutzt von:
     *   - DomainElement: CompoundDocument mit Methode: getCompoundDocumentWithDocuments(...)
     *
     * @param ressourceType
     * @param ressourceId
     * @return
     */
    // @formatter:on
    public List<Rights> getAccessRightsForCurrentUserOnGivenResource(String ressourceType, UUID ressourceId) {
        return getAccessRightsForCurrentUserOnGivenResource(ressourceType, ressourceId, session.getUser());
    }

    // @formatter:off
    /**
     * Erzeugt eine List mit Rechten des Nutzers aus RBAC.
     * Dabei wird insbesondere die Tabelle 'regeln' berücksichtigt.
     *
     * @param ressourceType
     * @param ressourceId
     * @return
     */
    // @formatter:on
    public List<Rights> getAccessRightsForCurrentUserOnGivenResource(String ressourceType, UUID ressourceId, User user) {

        List<Rights> rechte = new ArrayList<>();
        if (editorRollenUndRechte.isAllowed(user.getGid().getId(), Rights.LESEN.name(), ressourceType, ressourceId.toString())) {
            rechte.add(Rights.LESEN);
        }

        if (editorRollenUndRechte.isAllowed(user.getGid().getId(), Rights.SCHREIBEN.name(), ressourceType, ressourceId.toString())) {
            rechte.add(Rights.SCHREIBEN);
        }

        if (editorRollenUndRechte.isAllowed(user.getGid().getId(), Rights.KOMMENTIEREN.name(), ressourceType, ressourceId.toString())) {
            rechte.add(Rights.KOMMENTIEREN);
        }

        if (editorRollenUndRechte.isAllowed(user.getGid().getId(), Rights.KOMMENTARE_LESEN.name(), ressourceType, ressourceId.toString())) {
            rechte.add(Rights.KOMMENTARE_LESEN);
        }

        return rechte;
    }

    // ----  ================================================  ----

    public Set<String> getAlleResourcenForUserRessourceTypAndAktionen(User benutzer, String ressourcenTyp, List<String> ressourcenIds,
        List<String> aktionen) {
        List<de.itzbund.egesetz.bmi.auth.api.dto.PermissionDTO> permissionDTOs = editorRollenUndRechte.getPermissions(
            Optional.of(List.of(benutzer.getGid().getId())),
            Optional.of(aktionen),
            Optional.of(List.of(ressourcenTyp)),
            Optional.of(ressourcenIds),
            Optional.empty(),
            GetPermissionOptionType.FAST_NO_STATUS_NO_AKTIONEN
        );
        Set<String> ret = new HashSet<>();
        permissionDTOs.forEach(x -> ret.add(x.getRessourceId()));
        return ret;
    }

    public List<UUID> getRegelungsvorhabenIdsMitNutzerZugriff(User user) {
        return editorRollenUndRechte.getAlleRessourcenFuerBenutzerUndRessourceTypAsUUID(user.getGid().getId(), RESSOURCENTYP_REGELUNGSVORHABEN);
    }

    public Set<String> getAlleRegelungsvorhabenFuerBenutzer(UserId benutzerId) {
        return new HashSet<>(editorRollenUndRechte.getAlleRessourcenFuerBenutzerUndRessourceTyp(benutzerId.getId(), "REGELUNGSVORHABEN"));
    }

    // ----  ================================================  ----

    public List<String> getBenutzerGidsMitDokumentenmappenZugriffAlsLeser(CompoundDocumentId compoundDocumentId) {
        return editorRollenUndRechte.getAlleBenutzerIdsByRessourceIdAndRolle(compoundDocumentId.getId().toString(), ROLLE_LESER);
    }

	public List<String> getBenutzerGidsMitDokumentenmappenZugriffAlsSchreiber(CompoundDocumentId compoundDocumentId) {
		return editorRollenUndRechte.getAlleBenutzerIdsByRessourceIdAndRolle(compoundDocumentId.getId().toString(), ROLLE_SCHREIBER);
	}

	public List<String> getBenutzerGidsMitDokumentenmappenZugriffAlsErsteller(CompoundDocumentId compoundDocumentId) {
		return editorRollenUndRechte.getAlleBenutzerIdsByRessourceIdAndRolle(compoundDocumentId.getId().toString(), ROLLE_ERSTELLER);
	}

    // ----  ================================================  ----

    public List<String> getDokumentenmappenIdsFuerNutzerAlsLeser(UserId userId) {
        return editorRollenUndRechte.getAlleRessourcenFuerBenutzerUndRolleUndRessourceTyp(userId.getId(), ROLLE_LESER, RESSOURCENTYP_DOKUMENTENMAPPE);
    }

    /**
     * Extrahiert eine Liste von Ids der Dokumentenmappen bei der der angegebene Benutzer Schreibrechte hat, die gültig, also nicht deaktiviert, sind.
     *
     * @param userId Eine User id.
     * @return Eine Liste von Dokumentenmappen mit GÜLTIGEN Schreibrechten.
     */
    public List<String> getDokumentenmappenIdsFuerNutzerAlsSchreiber(UserId userId) {
        return editorRollenUndRechte.getAlleRessourcenFuerBenutzerUndRolleUndRessourceTyp(userId.getId(), ROLLE_SCHREIBER, RESSOURCENTYP_DOKUMENTENMAPPE, true);
    }

    // ----  Leserechte  ----

    public void vergibLeserechteFuerEinenNutzerZuEinerDokumentenmappe(User sourceUser, String targetGid, CompoundDocumentSummaryDTO compoundDocument) {
        editorRollenUndRechte.addRessourcePermission(targetGid, RESSOURCENTYP_DOKUMENTENMAPPE, compoundDocument.getId().toString(), ROLLE_LESER);
        benachrichtigungService.leserechteErteilt(User.builder().gid(new UserId(targetGid)).build(), sourceUser, compoundDocument);
    }

    public void vergibLeserechteFuerEinenNutzerZuEinemDokument(String userGid, DocumentId documentId) {
        editorRollenUndRechte.addRessourcePermission(userGid, RESSOURCENTYP_DOKUMENT, documentId.getId().toString(), ROLLE_LESER);
    }

    // ----  Schreibrechte  ----

    public void vergibSchreibrechteFuerEinenNutzerZuEinerDokumentenmappeMitBefristung(User sourceUser, String userGid,
        CompoundDocumentSummaryDTO compoundDocumentSummaryDTO, Instant befristung) {

        // Die Klasse regelt selber, dass kein Schreibrecht doppelt vorhanden sein darf.
        Map<String, List<String>> log = editorRollenUndRechte.addRessourcePermissionWithDetails(userGid, RESSOURCENTYP_DOKUMENTENMAPPE,
            compoundDocumentSummaryDTO.getId().toString(), ROLLE_SCHREIBER, befristung, null);

        benachrichtigungService.schreibrechteErteilt(User.builder().gid(new UserId(userGid)).build(), sourceUser, compoundDocumentSummaryDTO);
        automatischeRechteaenderungAusRBACAuswerten(sourceUser, log, compoundDocumentSummaryDTO);
    }

    private void automatischeRechteaenderungAusRBACAuswerten(User sourceUser, Map<String, List<String>> log, CompoundDocumentSummaryDTO dokumentenMappe) {
        if (log.containsKey(SCHREIBRECHTE_ENTFERNT)) {
            List<String> entfernteSchreibrechte = log.get(SCHREIBRECHTE_ENTFERNT);
            entfernteSchreibrechte.forEach(s -> {
                User user = User.builder().gid(new UserId(s)).build();
                benachrichtigungService.schreibrechteEntzogen(sourceUser, user, dokumentenMappe);
            });
        }

        if (log.containsKey(LESERECHTE_HINZU)) {
            List<String> leserechteErteilt = log.get(LESERECHTE_HINZU);
            leserechteErteilt.forEach(s -> {
                User user = User.builder().gid(new UserId(s)).build();
                benachrichtigungService.leserechteErteilt(sourceUser, user, dokumentenMappe);
            });
        }
    }

    public void vergibSchreibrechteFuerEinenNutzerZuEinemDokumentMitBefristung(String userGid, DocumentId documentId, Instant befristung) {
        // Die Klasse regelt selber, dass kein Schreibrecht doppelt vorhanden sein darf.
        editorRollenUndRechte.addRessourcePermission(userGid, RESSOURCENTYP_DOKUMENT, documentId.getId().toString(), ROLLE_SCHREIBER,
            befristung);
    }
    
    public Set<String> getAlleDokumentenmappemFuerBenutzerAlsMitarbeiter(UserId benutzerId) {
        return new HashSet<>(editorRollenUndRechte.getAlleRessourceTypenIds(benutzerId.getId(), "DOKUMENTENMAPPE", "MITARBEITER"));
    }

    // ====  Entfernen von Rechten  ====

    /**
     * Remove all additional permissions (for LESER)
     */
    public void entferneAlleZusaetzlichenLeserechteDerDokumentenmappe(CompoundDocumentId compoundDocumentId) {
        editorRollenUndRechte.removeRessourcePermission(RESSOURCENTYP_DOKUMENTENMAPPE, compoundDocumentId.getId().toString(), ROLLE_LESER);
    }

    public void entferneAlleZusaetzlichenLeserechteDesDokuments(DocumentId documentId) {
        editorRollenUndRechte.removeRessourcePermission(RESSOURCENTYP_DOKUMENT, documentId.getId().toString(), ROLLE_LESER);
    }

    public void entferneAlleZusaetzlichenLeserechteDerDokumentenmappeEinesBenutzers(CompoundDocumentId compoundDocumentId, String benutzer) {
        editorRollenUndRechte.removeRessourcePermission(benutzer, RESSOURCENTYP_DOKUMENTENMAPPE, compoundDocumentId.getId().toString(), ROLLE_LESER);
    }

    public void entferneAlleZusaetzlichenLeserechteDesDokumentsEinesBenutzers(DocumentId documentId, String benutzer) {
        editorRollenUndRechte.removeRessourcePermission(benutzer, RESSOURCENTYP_DOKUMENT, documentId.getId().toString(), ROLLE_LESER);
    }

    // ====  Konvertiermethoden  ====

    /**
     * Konvertiert die gefundenen Rechte in ein TransferObjekt
     *
     * @param rightsList
     * @return
     */
    public static PermissionDTO convertDocumentRightsToPermissionDto(List<Rights> rightsList) {
        return PermissionDTO.builder()
            .hasRead(rightsList.contains(Rights.LESEN))
            .hasWrite(rightsList.contains(Rights.SCHREIBEN))
            .build();
    }

    public static PermissionDTO convertCommentRightsToPermissionDto(List<Rights> rightsList) {
        return PermissionDTO.builder()
            .hasRead(rightsList.contains(Rights.KOMMENTARE_LESEN))
            .hasWrite(rightsList.contains(Rights.KOMMENTIEREN))
            .build();
    }

}
