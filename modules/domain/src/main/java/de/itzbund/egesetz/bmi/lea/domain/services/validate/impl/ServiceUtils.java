// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.impl;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import de.itzbund.egesetz.bmi.lea.domain.services.transform.JsonToXmlTransformer;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

/**
 * <Beschreibung>
 */
@Log4j2
public class ServiceUtils {

    private ServiceUtils() {
    }


    public static String jsonConvertToXml(String json, JsonToXmlTransformer jsonToXmlTransformer) {
        if (!Utils.isJSONValid(json)) {
            log.error("json ist not valid");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                "given text is not a valid json");
        }

        ReusableInputSource ris = jsonToXmlTransformer.transform(json);
        return ris == null ? null : convertToString(ris.getByteStream());
    }


    /**
     * Create a string taken from inputstream
     *
     * @param inputStream input with data
     * @return string created from inputstream
     */
    public static String convertToString(InputStream inputStream) {
        return new BufferedReader(
            new InputStreamReader(inputStream, StandardCharsets.UTF_8))
            .lines()
            .collect(Collectors.joining("\n"));
    }

}
