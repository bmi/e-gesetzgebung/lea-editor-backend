// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * indicates different types of Regelungsvorhaben
 */
@Schema(enumAsRef = true)
@AllArgsConstructor
@Getter
public enum RechtsetzungsdokumentForm {

    EINGEBUNDENE_STAMMFORM("Eingebundene Stammform", "eingebundene-stammform"),
    MANTELFORM("Mantelform", "mantelform"),
    NICHT_VORHANDEN("Nicht vorhanden", "nicht-vorhanden"),
    STAMMFORM("Stammform", "stammform"),
    SYNOPSE("Synopse", "synopse");

    private static final Map<String, RechtsetzungsdokumentForm> FORM_LITERALS = new HashMap<>();

    static {
        Arrays.stream(values()).forEach(form -> FORM_LITERALS.put(form.getLiteral(), form));
    }

    private final String label;
    private final String literal;


    public static RechtsetzungsdokumentForm fromLiteral(String literal) {
        if (Utils.isMissing(literal)) {
            return null;
        }

        return FORM_LITERALS.get(literal);
    }

}
