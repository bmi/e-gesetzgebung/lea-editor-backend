// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.impl;

/**
 * Application specific exception type for document validation.
 */
public class ValidationException extends Exception {

    /**
     * {@inheritDoc}
     *
     * @param message {@inheritDoc}
     */
    public ValidationException(String message) {
        super(message);
    }


    /**
     * {@inheritDoc}
     *
     * @param message {@inheritDoc}
     */
    public ValidationException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
