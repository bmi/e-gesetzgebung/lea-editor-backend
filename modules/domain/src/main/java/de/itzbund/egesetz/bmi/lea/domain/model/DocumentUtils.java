// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model;

import de.itzbund.egesetz.bmi.lea.core.json.TraversableJSONDocument;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentForm;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import org.json.simple.JSONObject;

import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getMetadata;

public class DocumentUtils {

    private DocumentUtils() {
    }


    public static DocumentType getDocumentType(TraversableJSONDocument document) {
        return getDocumentType(document.getDocumentObject());
    }


    public static DocumentType getDocumentType(JSONObject document) {
        String metaArt = getMetadata(document, "meta:art");
        RechtsetzungsdokumentArt art = RechtsetzungsdokumentArt.fromLiteral(metaArt);

        String metaForm = getMetadata(document, "meta:form");
        RechtsetzungsdokumentForm form = RechtsetzungsdokumentForm.fromLiteral(metaForm);

        String metaTyp = getMetadata(document, "meta:typ");
        RechtsetzungsdokumentTyp typ = RechtsetzungsdokumentTyp.fromLiteral(metaTyp);

        return DocumentType.getFromTypology(art, form, typ);
    }

}
