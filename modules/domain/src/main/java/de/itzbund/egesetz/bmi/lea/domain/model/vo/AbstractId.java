// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model.vo;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.UUID;

/**
 * Abstract class that assigns all identity value objects a UUID generated during construction.
 */
@Getter
@EqualsAndHashCode
@SuppressWarnings("java:S1694")
public abstract class AbstractId {

    @EqualsAndHashCode.Include
    private UUID id;


    protected AbstractId(UUID id) {
        this.setId(id);
    }


    @Override
    public String toString() {
        return String.format("%s(id=%s)", this.getClass().getSimpleName(), this.getId());
    }


    private void setId(UUID id) {
        if (this.id != null) {
            throw new IllegalStateException("The id may not be changed.");
        }

        if (id == null) {
            throw new IllegalArgumentException("The id may not be set to null.");
        }

        this.id = id;
    }
}
