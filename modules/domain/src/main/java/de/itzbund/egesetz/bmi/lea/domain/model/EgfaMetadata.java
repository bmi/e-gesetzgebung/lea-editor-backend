// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model;

import de.itzbund.egesetz.bmi.lea.domain.enums.egfa.datenuebernahme.EgfaModuleType;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.EgfaDataId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.EgfaMetadataId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class EgfaMetadata {

	private EgfaMetadataId egfaMetadataId;

	private DocumentId documentId;

	private RegelungsVorhabenId regelungsVorhabenId;

	private EgfaModuleType moduleName;

	private Instant moduleCompletedAt;

	private Instant createdAt;

	private User createdByUserId;

	private EgfaDataId egfaDataId;

	private CompoundDocumentId compoundDocumentId;
}
