// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * indicates different types of Regelungsvorhaben
 */
@Schema(enumAsRef = true)
public enum RegelungsvorhabenTypType {

    // zuletzt geprüft: 14.05.2024
    GESETZ("Gesetz"),
    RECHTSVERORDNUNG("Rechtsverordnung"),
    VERWALTUNGSVORSCHRIFT("Verwaltungsvorschrift");

    public final String label;
    
    RegelungsvorhabenTypType(String label) {
        this.label = label;
    }
}
