// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationContext;

import java.util.List;

@Getter
@Log4j2
public final class CompoundDocumentUtil {

    private CompoundDocumentUtil() {
    }

    public static boolean isUserCreaterOfCompoundDocument(CompoundDocument compoundDocument, User userEntity) {
        CompoundDocumentDomain compoundDocumentEntity = compoundDocument.getCompoundDocumentEntity();
        return isUserCreaterOfCompoundDocument(compoundDocumentEntity, userEntity);
    }


    public static boolean isUserCreaterOfCompoundDocument(CompoundDocumentDomain compoundDocument, User userEntity) {
        return compoundDocument.getCreatedBy().getGid().equals(userEntity.getGid());
    }


    public static void addPermissionsForCompoundDocumentContainingDocuments(
        RBACPermissionService rbacPermissionService, List<Document> documents) {
        documents.forEach(
            rbacPermissionService::addCreatorPermissionsForDocument
        );
    }


    public static CompoundDocument getCompoundDocument(CompoundDocumentId compoundDocumentId, User userEntity, ApplicationContext applicationContext) {
        CompoundDocument compoundDocument = applicationContext.getBean(CompoundDocument.class);
        compoundDocument = compoundDocument.getCompoundDocumentWithDocumentsForIdAndOptionalFiltered(compoundDocumentId,
            userEntity);
        return compoundDocument;
    }


    /**
     * Get a compound document by ID and enriches it with its documents
     *
     * @param user               The user
     * @param compoundDocumentId The compound document id
     * @return The compound document
     */
    public static CompoundDocument getAllDocumentsFromOneCompoundDocument(User user, CompoundDocumentId compoundDocumentId,
        ApplicationContext applicationContext) {
        CompoundDocument compoundDocument1 = applicationContext.getBean(CompoundDocument.class);
        CompoundDocument myCompoundDocument =
            compoundDocument1.getCompoundDocumentWithDocumentsForIdAndOptionalFiltered(
                compoundDocumentId, user);

        if (myCompoundDocument == null) {
            log.debug("Status {} is returned", ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
            return null;
        }

        return myCompoundDocument;
    }

}
