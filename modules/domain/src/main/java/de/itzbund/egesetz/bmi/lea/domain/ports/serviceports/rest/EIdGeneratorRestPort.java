// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest;

import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentForm;
import de.itzbund.egesetz.bmi.lea.domain.ports.eid.JSONContext;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.IllegalDocumentType;
import org.json.simple.JSONObject;

public interface EIdGeneratorRestPort {

    JSONObject erzeugeEIdsFuerDokument(JSONObject dokument) throws IllegalDocumentType;

    JSONObject erzeugeEIdsFuerTeilbaum(JSONObject baum, JSONContext jsonContext, RechtsetzungsdokumentForm form);

}
