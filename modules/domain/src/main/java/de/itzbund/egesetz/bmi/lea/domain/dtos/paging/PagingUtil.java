/*
 * SPDX-License-Identifier: MPL-2.0
 *
 * Copyright (C) 2021-2023 Bundesministerium des Innern und für Heimat, \
 * Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
 */
package de.itzbund.egesetz.bmi.lea.domain.dtos.paging;

import lombok.experimental.UtilityClass;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

@UtilityClass
public class PagingUtil {

    public static final int DEFAULT_PAGE_SIZE_VERSIONHISTORY = 5;

    public Pageable createPageable(PaginierungDTO paginierungDTO) {
        if (paginierungDTO.getSortDirection() == null || paginierungDTO.getSortBy() == null) {
            return PageRequest.of(paginierungDTO.getPageNumber(), paginierungDTO.getPageSize());
        }
        Sort.Direction direction = Sort.Direction.fromString(String.valueOf(paginierungDTO.getSortDirection()));
        Sort sort = Sort.by(direction, paginierungDTO.getSortBy().label);
        return PageRequest.of(paginierungDTO.getPageNumber(), paginierungDTO.getPageSize(), sort);
    }

    public <T> Page<T> getPaginatedList(List<T> fullList, int pageNumber, int pageSize) {
        // Erstelle Pageable-Objekt
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);

        // Berechne Start- und Endindex für die Paginierung
        int start = (int) pageRequest.getOffset();
        int end = Math.min((start + pageRequest.getPageSize()), fullList.size());

        // Erstelle eine Teilmenge (Page) der Liste basierend auf den Paginierungsinformationen
        List<T> sublist = fullList.subList(start, end);

        // Erstelle und gib die Page zurück
        return new PageImpl<>(sublist, pageRequest, fullList.size());
    }

    public Pageable createDefaultPagingForMeineDokumente() {
        PaginierungDTO paginierungDTO = PaginierungDTO.builder()
            .pageNumber(0)
            .pageSize(1)
            .sortBy(SortByType.CREATED_AT)
            .sortDirection(SortDirectionType.DESC)
            .build();
        return createPageable(paginierungDTO);
    }

    public Pageable createDefaultPagingForVersionshistorie() {
        PaginierungDTO paginierungDTO = PaginierungDTO.builder()
            .pageNumber(0)
            .pageSize(DEFAULT_PAGE_SIZE_VERSIONHISTORY)
            .sortBy(SortByType.VERSION)
            .sortDirection(SortDirectionType.DESC)
            .build();
        return createPageable(paginierungDTO);
    }
}
