// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.Singular;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.Valid;
import java.util.List;

@Data
@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class CompoundDocumentDTO extends CompoundDocumentAllWithIdDTO {

    // JSON-Property überschreibt name
    @JsonProperty("status")
    @Schema(name = "status", example = "FREEZE")
    private DocumentState state;

    @Valid
    @JsonProperty("proposition")
    @Schema(name = "proposition")
    private PropositionDTO proposition;

    // Die DokumentDTO-Liste ist von anderem Typ
    @Valid
    @Singular
    @JsonProperty("documents")
    @Schema(name = "documents")
    private List<DocumentDTO> documents;
}
