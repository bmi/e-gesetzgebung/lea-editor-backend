// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.eid;

public interface JSONContext {

    String getPraefix();

    void setPraefix(String praefix);

    void addTypeOccurrence(String type);

    int getCurrentTypeCount(String type);

    JSONContext copyWithIncrement(String type);

    void initializeOrderedList(int count);

    void decreaseList();

    boolean isProcessingOrderedList();

}
