// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * represents root DTO for eGFA Datenübernahme to Editor
 */
@Getter
@SuperBuilder
@NoArgsConstructor
public class EgfaDatenuebernahmeDTO {

    @NotNull
    private String id;
    private EgfaSectionType section;
    private String title;
    @NotNull
    private List<? extends EgfaDatenuebernahmeContentDTO> content;
    private List<EgfaDatenuebernahmeDTO> children;
}
