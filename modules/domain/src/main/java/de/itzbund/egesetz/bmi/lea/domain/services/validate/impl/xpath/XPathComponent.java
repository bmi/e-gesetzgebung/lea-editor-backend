// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.xpath;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class for internal representation of an XPath component.
 */
@AllArgsConstructor
@Data
public class XPathComponent {

    private static final Pattern PATT_COMPONENT = Pattern.compile("(([\\w.]+:)?[\\w.]+)(\\[(\\d+)\\])?");

    private String name;

    private int position;


    /**
     * Parses a string representation of a component, i.e. a QName and a position, into an object.
     *
     * @param comString string representation of the component
     * @return XPathComponent object
     */
    public static XPathComponent parse(String comString) {
        Matcher matcher = PATT_COMPONENT.matcher(comString);
        if (matcher.matches()) {
            String name = matcher.group(1);
            int position = matcher.group(4) != null ? Integer.parseInt(matcher.group(4)) : 1; //NOSONAR
            return new XPathComponent(name, position);
        } else {
            return null;
        }
    }

}
