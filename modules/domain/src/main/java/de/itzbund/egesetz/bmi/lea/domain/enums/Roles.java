// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets Roles
 */
@SuppressWarnings("unused")
public enum Roles {
    // Ersteller des Regelungsvorhabens
    ABSTIMMUNGSLEITER_HAUPTABSTIMMUNG("Abstimmunsleiter der Hauptabstimmung"),
    TEILNEHMER_HRA("Teilnehmer der Haus- und Ressortabstimmung"),
    TEILNEHMER_UNTERABSTIMMUNG("Teilnehmer der Unterabstimmung"),
    ERSTELLER_DOKUMENTENMAPPE("Ersteller der Dokumentenmappe"),
    EDITOR("Editor"),
    PLATFORM("Plattform");

    private final String value;


    Roles(String value) {
        this.value = value;
    }


    @JsonCreator
    public static Roles fromValue(String text) {
        for (Roles b : Roles.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        return null;
    }


    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }
}
