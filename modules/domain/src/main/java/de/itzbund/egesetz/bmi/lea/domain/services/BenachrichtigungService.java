// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.NeuigkeitEditorType;
import de.itzbund.egesetz.bmi.lea.domain.model.PlatformMessage;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
@Log4j2
public class BenachrichtigungService {


	private final PlategRequestService plategRequestService;
	private final UserService userService;

	// @formatter:off
    /**
     * Bei der (um-)verteilung der Schreibrechte werden der Benutzer informiert, der die Schreibrechte
     * entzogen bekommen hat und der Benutzer, der die neuen Schreibrechte hat.
     *
     * Da es nur eine Person mit Schreibrechten geben kann, ist eine Methode hinreichend.
     *
     * @return If it was successfully
     */
    // @formatter:on
	public boolean schreibrechteErteilt(User neuerBenutzer, User alterBenutzer, CompoundDocumentSummaryDTO compoundDocumentSummaryDTO) {

		boolean alterRechteAngepasst = true;
		if (alterBenutzer != null) {
			alterRechteAngepasst = schreibrechteEntzogen(neuerBenutzer, alterBenutzer, compoundDocumentSummaryDTO);
		}

		UUID empfaenger = getPlattformUserId(neuerBenutzer.getGid());

		if (empfaenger != null) {
			PlatformMessage platformMessage = PlatformMessage.builder()
				.typ(NeuigkeitEditorType.DOKUMENTMAPPE_SCHREIBEN_ERTEILT)
				.text(String.format("Sie haben die Schreibrechte von %s für %s erhalten.", alterBenutzer.getName(),
					getLinkForCompoundDocument(compoundDocumentSummaryDTO)))
				.empfaengerIds(List.of(empfaenger))
				.build();

			return alterRechteAngepasst && plategRequestService.setMessageInPlatform(platformMessage);
		}

		return false;
	}

	public boolean schreibrechteEntzogen(User neuerBenutzer, User alterBenutzer, CompoundDocumentSummaryDTO ressourcenId) {

		UUID plattformUserId = getPlattformUserId(alterBenutzer.getGid());

		if (plattformUserId != null) {
			PlatformMessage platformMessage = PlatformMessage.builder()
				.typ(NeuigkeitEditorType.DOKUMENTMAPPE_SCHREIBEN_ENTZOGEN)
				.text(String.format("Ihnen wurden die Schreibrechte von %s für %s entzogen.", neuerBenutzer.getName(),
					getLinkForCompoundDocument(ressourcenId)))
				.empfaengerIds(List.of(plattformUserId))
				.build();

			return plategRequestService.setMessageInPlatform(platformMessage);
		}

		return false;
	}

	public boolean leserechteEntzogen(User neuerBenutzer, User alterBenutzer, CompoundDocumentSummaryDTO compoundDocumentSummaryDTO) {

		UUID plattformUserId = getPlattformUserId(alterBenutzer.getGid());

		if (plattformUserId != null) {

			PlatformMessage platformMessage = PlatformMessage.builder()
				.typ(NeuigkeitEditorType.DOKUMENTMAPPE_LESEN_ENTZOGEN)
				.text(String.format("Ihnen wurden die Leserechte von %s für %s entzogen.", neuerBenutzer.getName(),
					getLinkForCompoundDocument(compoundDocumentSummaryDTO)))
				.empfaengerIds(List.of(plattformUserId))
				.build();

			return plategRequestService.setMessageInPlatform(platformMessage);
		}

		return false;
	}

	public boolean leserechteErteilt(User neuerBenutzer, User alterBenutzer, CompoundDocumentSummaryDTO compoundDocumentSummaryDTO) {

		String string1 = alterBenutzer.getName();
		String string2 = getLinkForCompoundDocument(compoundDocumentSummaryDTO);
		String string3 = String.format("Sie haben die Leserechte von %s für %s erhalten.", string1, string2);

		PlatformMessage platformMessage = PlatformMessage.builder()
			.typ(NeuigkeitEditorType.DOKUMENTMAPPE_LESEN_ERTEILT)
			.text(string3)
			.empfaengerIds(List.of(getPlattformUserId(neuerBenutzer.getGid())))
			.build();

		return plategRequestService.setMessageInPlatform(platformMessage);
	}

	private UUID getPlattformUserId(UserId userId) {
		User user = userService.getUserForGid(userId.getId());
		if (user != null) {
			return user.getPlattformUserId();
		}
		return null;
	}

	private String getLinkForCompoundDocument(CompoundDocumentSummaryDTO compoundDocumentId) {
		if (compoundDocumentId != null) {
			String title = compoundDocumentId.getTitle();
			return "<a href='#/editor/compounddocuments/" + compoundDocumentId.getId() + "'>" + title + "</a>";
		}

		return "<a href='#'>keine Referenz gefunden</a>";
	}
}