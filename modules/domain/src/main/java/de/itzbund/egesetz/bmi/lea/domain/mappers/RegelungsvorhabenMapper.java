// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.mappers;

import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorShortDTO;
import org.mapstruct.Builder;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true))
public interface RegelungsvorhabenMapper {

    RegelungsvorhabenEditorDTO map(RegelungsvorhabenEditorShortDTO regelungsvorhabenEditorShortDTO);

    @InheritInverseConfiguration
    RegelungsvorhabenEditorShortDTO map(RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO);

}
