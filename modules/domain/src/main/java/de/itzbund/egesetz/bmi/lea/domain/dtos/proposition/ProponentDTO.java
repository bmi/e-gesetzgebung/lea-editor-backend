// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.proposition;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.Valid;
import java.util.UUID;

/**
 * Ressort welches dem Regelungsvorhaben (Proposition) zugeordnet ist.
 */
@Data
@Getter
@SuperBuilder
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class ProponentDTO {

    @JsonProperty("id")
    @Valid
    @Schema(name = "id")
    private UUID id;

    @JsonProperty("title")
    @Schema(name = "title", example = "BMI")
    private String title;

    @JsonProperty("designationGenitive")
    @Schema(name = "designationGenitive", example = "Bundesministeriums des Innern, für Bau und Heimat")
    private String designationGenitive;

    @JsonProperty("designationNominative")
    @Schema(name = "designationNominative", example = "Bundesministeriums des Innern, für Bau und Heimat")
    private String designationNominative;

    @JsonProperty("active")
    @Schema(name = "active")
    private boolean active;

}
