// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.document;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

/**
 * PkpZuleitungDokumentDTO
 */
@Getter
@SuperBuilder
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class PkpZuleitungDokumentDTO {

	@Schema(name = "dokumentenMappeId", example = "00000000-0000-0000-0002-000000000005", required = true)
	@JsonProperty("dokumentenMappeId")
	private UUID dokumentenMappeId;

	@Schema(name = "pkpTyp", example = "RECHTSETZUNGSDOKUMENT_STAMMFORM", required = true)
	@JsonProperty("pkpTyp")
	private DocumentType pkpTyp;

	@Schema(name = "dokumentName", example = "Dokumentenname", required = true)
	@JsonProperty("dokumentName")
	private String dokumentName;

	@Schema(name = "dokumentInhalt", example = "W3sieG1sLW1vZGVsIjoiaHJlZj1cIjAyX3NjaGVtYVwvbGVnYWxEb2NNTC5kZVw", required = true)
	@JsonProperty("dokumentInhalt")
	private String dokumentInhalt;

}

