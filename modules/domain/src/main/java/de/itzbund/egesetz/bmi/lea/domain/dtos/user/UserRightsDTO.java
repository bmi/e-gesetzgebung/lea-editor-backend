// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.user;

import de.itzbund.egesetz.bmi.lea.domain.enums.Freigabetyp;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.time.Instant;

/**
 * UserRightsDTO
 */
@Data
@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
public class UserRightsDTO {

	@Schema(name = "gid", example = "00000000-0000-1111-0000-000000000000", required = true)
	private String gid;

	@Schema(name = "email", example = "Vorname.Nachname@email.de", required = true)
	private String email;

	@Schema(name = "name", example = "Vorname Nachname", required = true)
	private String name;

	@Schema(name = "freigabetyp", example = "LESERECHTE")
	private Freigabetyp freigabetyp;

	@Schema(name = "anmerkungen", example = "Test Kommentar 123")
	private String anmerkungen;

	// speziell für Schreibrechtevergabe

	@Schema(name = "befristung", example = "19.02.2022")
	protected Instant befristung;

	@Schema(name = "erstelleNeueVersionBeiDraft", example = "true")
	private boolean erstelleNeueVersionBeiDraft;
}

