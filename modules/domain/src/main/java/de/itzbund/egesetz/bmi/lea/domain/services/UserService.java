// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserSettingsDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.UserSettings;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserSettingsId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserSettingsPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.UserRestPort;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Component
@Log4j2
@SuppressWarnings("unused")
public class UserService implements UserRestPort {

    private final LeageSession session;

    private final UserPersistencePort userRepository;

    private final UserSettingsPersistencePort userSettingsRepository;

    /**
     * Return the current user from 'Session' which is filled by IAM
     *
     * @return The User Object
     */
    public User getUser() {
        return session.getUser();
    }

    /**
     * Get's the user from database, so the special UUID from Plattform is also set
     *
     * @param gid The gid of the user
     * @return The User object with platform's special user id
     */
    public User getUserForGid(String gid) {
        return userRepository.findFirstByGid(new UserId(gid));
    }

    public void updateUserSettings(UserSettingsDTO userSettingsDTO, List<ServiceState> status) {
        final String newConfiguration;
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            newConfiguration = objectMapper.writeValueAsString(userSettingsDTO);
        } catch (JsonProcessingException e) {
            log.debug("Json Stringify error: ", e);
            status.add(ServiceState.UNKNOWN_ERROR);
            return;
        }
        UserSettings newUserSettings = UserSettings.builder().build();
        newUserSettings.setUserSettingsId(new UserSettingsId(UUID.randomUUID()));
        newUserSettings.setRelatedUser(session.getUser());
        newUserSettings.setConfiguration(newConfiguration);
        newUserSettings.setUpdatedAt(Instant.now());
        newUserSettings.setCreatedAt(Instant.now());

        userSettingsRepository.save(newUserSettings, status);
        if (!status.contains(ServiceState.OK)) {
            status.add(ServiceState.UNKNOWN_ERROR);
        }
    }

    public UserSettingsDTO getUserSettings() {

        User currentUser = session.getUser();
        Optional<UserSettings> userSettings = userSettingsRepository.get(currentUser);
        if (userSettings.isEmpty()) {
            return null;
        }
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(userSettings.get().getConfiguration(), UserSettingsDTO.class);
        } catch (JsonProcessingException e) {
            log.debug("Json Mapping error: ", e);
            return null;
        }
    }


}
