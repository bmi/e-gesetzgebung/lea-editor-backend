// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest;

import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;

import java.util.List;
import java.util.UUID;

public interface CompoundDocumentPermissionRestPort {

    List<UserDTO> getPermissionsByRessourceIdAndAktion(UUID ressourcenId, String aktion, List<ServiceState> status);
}
