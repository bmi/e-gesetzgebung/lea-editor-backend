// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.paging;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;
import java.util.EnumMap;

@Data
@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
public class PaginierungDTO {

    @NotNull
    @Builder.Default
    private int pageNumber = 0;

    @NotNull
    @Schema(description = "Size of each page", defaultValue = "1")
    @Builder.Default
    private int pageSize = 5;

    @Schema(description = "Sorting by filed", defaultValue = "CREATED_AT")
    @Builder.Default
    private SortByType sortBy = SortByType.VERSION;

    @Schema(description = "Sort direct", defaultValue = "DESC")
    @Builder.Default
    private SortDirectionType sortDirection = SortDirectionType.DESC;

    @JsonIgnore
    @Schema(description = "Key = property, Value= filter criteria")
    @Builder.Default
    private EnumMap<FilterPropertyType, String> filters = new EnumMap<>(FilterPropertyType.class);

}



