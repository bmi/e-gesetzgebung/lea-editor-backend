// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa;

import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.Drucksache;
import de.itzbund.egesetz.bmi.lea.domain.model.DrucksacheDokument;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DruckDokumentId;

import javax.sql.rowset.serial.SerialBlob;
import java.util.List;
import java.util.Optional;

public interface DrucksacheDokumentPersistencePort {

	public ServiceState updateDokumentDrucksache(SerialBlob pdf, CompoundDocumentId compoundDocumentId, User user);

	public Optional<byte[]> getDrucksacheByCompoundDocumentId(CompoundDocumentId compoundDocumentId, List<ServiceState> status);

    public Optional<DrucksacheDokument> getDrucksacheDokument(CompoundDocumentId compoundDocumentId);

}
