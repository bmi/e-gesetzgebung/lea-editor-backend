// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.ProponentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;

public interface RegelungsvorhabenRestPort {

    /**
     * @param regelungsVorhabenId
     * @param fromPlatform
     * @return
     * @deprecated Lieber getRegelungsvorhabenEditorDTOForCompoundDocument benutzen
     */
    @Deprecated
    RegelungsvorhabenEditorDTO getRegelungsvorhabenEditorDTONEW(RegelungsVorhabenId regelungsVorhabenId, boolean fromPlatform);

    ProponentDTO getProponent(RegelungsVorhabenId regelungsVorhabenId);

    RegelungsvorhabenEditorDTO getRegelungsvorhabenEditorDTOForCompoundDocument(CompoundDocument compoundDocument);

}
