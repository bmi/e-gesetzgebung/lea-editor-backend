// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.comment;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Getter
@Setter
@EqualsAndHashCode
@SuperBuilder
@RequiredArgsConstructor
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ReplyDTO {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Schema(name = "replyId", example = "11000000-0000-0000-0000-000000000000")
    private UUID replyId;

    @NotBlank
    @Schema(name = "parentId", example = "22200000-0000-0000-0000-000000000000", required = true)
    private String parentId;

    @NotBlank
    @Schema(name = "content", example = "SmV0enQgd2Vpw58gaWNoIG1laHI=", required = true)
    private String content;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Schema(name = "createdBy", accessMode = Schema.AccessMode.READ_ONLY)
    private UserDTO createdBy;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Schema(name = "updatedBy", accessMode = Schema.AccessMode.READ_ONLY)
    private UserDTO updatedBy;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Schema(name = "createdAt", example = "2022-08-09T19:44:18.863Z")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private String createdAt;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Schema(name = "updatedAt", example = "2022-08-09T19:44:18.863Z")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private String updatedAt;

}
