// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@ToString(callSuper = true)
public class UserSettingsDTO {

	@JsonProperty("pinnedDokumentenmappen")
	private List<PinnedDokumentenmappenDTO> pinnedDokumentenmappen;
}
