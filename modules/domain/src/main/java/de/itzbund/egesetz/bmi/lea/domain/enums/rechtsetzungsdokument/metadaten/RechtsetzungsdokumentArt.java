// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * indicates different types of Regelungsvorhaben
 */
@Schema(enumAsRef = true)
@AllArgsConstructor
@Getter
public enum RechtsetzungsdokumentArt {

    ANSCHREIBEN("Anschreiben", "anschreiben"),
    BEGRUENDUNG("Begründung", "begruendung"),
    BEKANNTMACHUNGSTEXT("Bekanntmachungstext", "bekanntmachungstext"),
    DENKSCHRIFT("Denkschrift", "denkschrift"),
    OFFENE_STRUKTUR("Offene Struktur", "offene-struktur"),
    RECHTSETZUNGSDOKUMENT("Rechtsetzungsdokument", "rechtsetzungsdokument"),
    REGELUNGSTEXT("Regelungstext", "regelungstext"),
    SYNOPSE("Synopse", "synopse"),
    VEREINBARUNG("Vereinbarung", "vereinbarung"),
    VORBLATT("Vorblatt", "vorblatt"),
    DRUCKSACHE("Drucksache", "drucksache");

    private static final Map<String, RechtsetzungsdokumentArt> literals;

    static {
        literals = new HashMap<>();
        Arrays.stream(values()).forEach(f -> literals.put(f.getLiteral(), f));
    }

    private final String label;
    private final String literal;


    public static RechtsetzungsdokumentArt fromLiteral(String literal) {
        return literals.get(literal);
    }

}
