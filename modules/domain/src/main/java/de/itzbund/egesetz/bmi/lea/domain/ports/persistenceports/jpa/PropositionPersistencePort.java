// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa;

import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;

import java.util.List;
import java.util.UUID;

public interface PropositionPersistencePort {

    Proposition save(Proposition propositionEntity);

    Proposition copy(Proposition proposition);

    Proposition update(Proposition proposition);

    Proposition findByUpdateablePropositionId(RegelungsVorhabenId regelungsVorhabenId);

    List<Proposition> findByUpdateablePropositionIds(List<RegelungsVorhabenId> regelungsVorhabenIds);

    List<Proposition> findByNotUpdateablePropositionId(RegelungsVorhabenId regelungsVorhabenId);

    Proposition findByNotUpdateablePropositionIdReferableId(RegelungsVorhabenId regelungsVorhabenId, UUID referableId);

    List<Proposition> findAllByPropositionId(RegelungsVorhabenId regelungsVorhabenId);

}
