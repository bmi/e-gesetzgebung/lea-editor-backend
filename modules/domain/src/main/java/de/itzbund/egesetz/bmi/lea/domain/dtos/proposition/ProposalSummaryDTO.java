// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.proposition;

import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import java.time.Instant;
import java.util.UUID;

/**
 * Data transfer object containing informative data about a proposal.
 */
@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
public class ProposalSummaryDTO {

    @NotBlank
    private UUID proposalID;

    @NotBlank
    private String title;

    @NotBlank
    private String version;

    @NotBlank
    private String propositionID;

    @NotBlank
    private Instant createdDate;

    @NotBlank
    private Instant updatedDate;

    @NotBlank
    private DocumentType documentType;
}
