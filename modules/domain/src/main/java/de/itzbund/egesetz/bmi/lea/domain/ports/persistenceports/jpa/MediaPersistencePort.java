// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa;

import de.itzbund.egesetz.bmi.lea.domain.model.MediaData;

import java.util.Optional;

public interface MediaPersistencePort {

    MediaData saveAndFlush(MediaData dataEntity);

    Optional<MediaData> findById(String id);

    void deleteById(String id);
}
