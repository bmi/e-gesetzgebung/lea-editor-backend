// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.mappers;

import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CommentPositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CommentResponseDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import de.itzbund.egesetz.bmi.lea.domain.model.Position;
import org.mapstruct.Builder;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true),
    uses = {BaseMapper.class, UserMapper.class, ReplyMapper.class})
public interface CommentMapper {

    // qualifiedByName defined in BaseMapper
    @Mapping(target = "documentId", source = "documentId", qualifiedByName = "documentIdStringMapping")
    @Mapping(target = "commentId", source = "commentId", qualifiedByName = "commentIdStringMapping")
    @Mapping(target = "updatedBy.gid", source = "updatedBy.gid", qualifiedByName = "userIdStringMapping")
    @Mapping(target = "createdBy.gid", source = "createdBy.gid", qualifiedByName = "userIdStringMapping")
    CommentResponseDTO map(Comment comment);

    // -----------

    Position map(CommentPositionDTO commentPositionDTO);

    @InheritInverseConfiguration
    CommentPositionDTO map(Position position);

    // -----------

}
