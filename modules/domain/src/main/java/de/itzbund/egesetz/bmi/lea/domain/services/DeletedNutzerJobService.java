// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import java.util.List;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CommentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.EgfaDataPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.EgfaMetadataPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.ReplyPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserSettingsPersistencePort;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Service
@Slf4j
public class DeletedNutzerJobService {

	private final UserPersistencePort userPersistencePort;

	private final CommentPersistencePort commentPersistencePort;
	private final ReplyPersistencePort replyPersistencePort;
	private final CompoundDocumentPersistencePort compoundDocumentPersistencePort;
	private final DokumentPersistencePort dokumentPersistencePort;
	private final EgfaDataPersistencePort egfaDataPersistencePort;
	private final EgfaMetadataPersistencePort egfaMetadataPersistencePort;
	private final UserSettingsPersistencePort userSettingsPersistencePort;

	@Scheduled(cron = "0 0 2 * * *")
	void markNotReferencedUsers() {
		log.info("Starting job to mark orphaned users as not referenced");
		final List<UserId> markedAsDeleted = userPersistencePort.findMarkedAsDeleted();

		for (final UserId userId : markedAsDeleted) {
			log.info("Checking: {}", userId.getId());

			final boolean userHasComments = commentPersistencePort.existsCommentsCreatedOrUpdatedByUser(userId);
			final boolean userHasReplies = replyPersistencePort.existsRepliesCreatedOrUpdatedByUser(userId);
			final boolean userCreatedOrUpdatedCompoundDocuments = compoundDocumentPersistencePort.existsCompoundDocumentsCreatedOrUpdatedByUser(userId);
			final boolean userCreatedOrUpdatedDocuments = dokumentPersistencePort.existsDokumentCreatedOrUpdatedByUser(userId);
			final boolean userTransmittedEgfaData = egfaDataPersistencePort.existsEgfaDataTransmittedByUser(userId);
			final boolean userCreatedEgfaMetadata = egfaMetadataPersistencePort.existsEgfaMetadataCreatedByUser(userId);
			final boolean userHasSettings = userSettingsPersistencePort.existsUserSettingsByUser(userId);

			boolean userIsUsed = userHasComments || userHasReplies || userCreatedOrUpdatedCompoundDocuments
				|| userCreatedOrUpdatedDocuments || userTransmittedEgfaData || userCreatedEgfaMetadata
				|| userHasSettings;

			if (!userIsUsed) {
				log.info("User '{}' is orphaned. Marking as not referenced.", userId.getId());
				userPersistencePort.markUserAsNotReferenced(userId);
			}
		}
	}
}
