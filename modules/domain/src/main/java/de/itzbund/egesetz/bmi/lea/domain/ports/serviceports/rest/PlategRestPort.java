// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest;

import java.util.List;
import java.util.UUID;
import de.itzbund.egesetz.bmi.lea.domain.dtos.abstimmung.CompoundDocumentEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;

/**
 * Die meisten URLs werden nicht mehr aufgerufen, sondern durch Datenbankzugriffe ersetzt.
 */
public interface PlategRestPort {

    /**
     * Get all 'Regelungsvorhaben' of a user by calling platform's backend
     *
     * @return A list of Regelungsvorhaben encapsulated in a DTO
     */
    List<RegelungsvorhabenEditorDTO> getAllRegulatoryProposalsForUserHeIsAuthorOf();

    /**
     * Get the 'Regelungsvorhaben' of a user specified by Regelungsvorhaben's UUID by calling platform's backend,
     *
     * @param regelungsvorhabenId The id of Regelungsvorhaben
     * @return The specific Regelungsvorhaben encapsulated in a DTO
     */
    RegelungsvorhabenEditorDTO getRegulatoryProposalUserCanAccess(UUID regelungsvorhabenId);

    /**
     * Get the 'Regelungsvorhaben' of a user specified by Regelungsvorhaben's UUID by calling platform's backend,
     *
     * @param createdBy           User that created the corresponding dm
     * @param regelungsvorhabenId The id of Regelungsvorhaben
     * @return The specific Regelungsvorhaben encapsulated in a DTO
     */
    RegelungsvorhabenEditorDTO getRegulatoryProposalUserCanAccess(User createdBy, UUID regelungsvorhabenId);

    /**
     *
     * @param compoundDocumentId Id of compound document
     * @return The specific Dokumentenmappe as DTO
     */
    CompoundDocumentEditorDTO getAbstimmungenEinerDokumentenmappe(CompoundDocumentId compoundDocumentId);

    /**
     *
     * @return list of Dokumentenmappe
     */
    List<CompoundDocumentEditorDTO> getAbstimmungUndDokumentenmappe();

}
