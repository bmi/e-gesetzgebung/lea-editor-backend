// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.mappers;

import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.ProponentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.ProposalSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RessortEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.RegelungsvorhabenTypType;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import org.mapstruct.Builder;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true),
    uses = {BaseMapper.class}
)
public interface PropositionMapper {

    @Named("RegelungsvorhabenTypType_to_RechtsetzungsdokumentTyp")
    static RechtsetzungsdokumentTyp toRechtsetzungsdokumentTyp(RegelungsvorhabenTypType type) {
        switch (type) {
            case GESETZ:
                return RechtsetzungsdokumentTyp.GESETZ;
            case RECHTSVERORDNUNG:
                return RechtsetzungsdokumentTyp.VERORDNUNG;
            case VERWALTUNGSVORSCHRIFT:
                return RechtsetzungsdokumentTyp.VERWALTUNGSVORSCHRIFT;
            default:
                return null;
        }
    }

    @Named("RechtsetzungsdokumentTyp_to_RegelungsvorhabenTypType")
    static RegelungsvorhabenTypType toRechtsetzungsdokumentTyp(RechtsetzungsdokumentTyp typ) {
        switch (typ) {
            case GESETZ:
            case SATZUNG:
            case VERTRAGSGESETZ:
            case SONSTIGE_BEKANNTMACHUNG:
                return RegelungsvorhabenTypType.GESETZ;
            case VERORDNUNG:
            case VERTRAGSVERORDNUNG:
                return RegelungsvorhabenTypType.RECHTSVERORDNUNG;
            case VERWALTUNGSVORSCHRIFT:
                return RegelungsvorhabenTypType.VERWALTUNGSVORSCHRIFT;
            default:
                return null;
        }
    }

    // id -> automatisch
    @Mapping(target = "title", source = "bezeichnung")
    @Mapping(target = "shortTitle", source = "kurzbezeichnung")
    @Mapping(target = "abbreviation", source = "abkuerzung")
    @Mapping(target = "state", source = "status")
    @Mapping(target = "proponentDTO", source = "technischesFfRessort")
    @Mapping(target = "initiant", source = "initiant")
    @Mapping(target = "type", source = "vorhabenart",
        qualifiedByName = "RegelungsvorhabenTypType_to_RechtsetzungsdokumentTyp")
    @Mapping(target = "bearbeitetAm", source = "bearbeitetAm")
    PropositionDTO map(RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO);

    @Mapping(target = "propositionId", source = "id", qualifiedByName = "propositionIdUuidMapping")
    @Mapping(target = "title", source = "bezeichnung")
    @Mapping(target = "shortTitle", source = "kurzbezeichnung")
    @Mapping(target = "abbreviation", source = "abkuerzung")
    @Mapping(target = "state", source = "status")
    @Mapping(target = "proponentId", source = "technischesFfRessort.id")
    @Mapping(target = "proponentTitle", source = "technischesFfRessort.kurzbezeichnung")
    @Mapping(target = "proponentDesignationGenitive", source = "technischesFfRessort.bezeichnungGenitiv")
    @Mapping(target = "proponentDesignationNominative", source = "technischesFfRessort.bezeichnungNominativ")
    @Mapping(target = "proponentActive", source = "technischesFfRessort.aktiv")
    @Mapping(target = "initiant", source = "initiant")
    @Mapping(target = "type", source = "vorhabenart",
        qualifiedByName = "RegelungsvorhabenTypType_to_RechtsetzungsdokumentTyp")
    @Mapping(target = "createdAt", source = "bearbeitetAm")
    Proposition mapD(RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO);

    @InheritInverseConfiguration
    @Mapping(target = "vorhabenart", source = "type",
        qualifiedByName = "RechtsetzungsdokumentTyp_to_RegelungsvorhabenTypType")
    RegelungsvorhabenEditorDTO mapD(Proposition propostion);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "title", source = "kurzbezeichnung")
    @Mapping(target = "designationGenitive", source = "bezeichnungGenitiv")
    @Mapping(target = "designationNominative", source = "bezeichnungNominativ")
    @Mapping(target = "active", source = "aktiv")
    ProponentDTO map(RessortEditorDTO ressortEditorDTO);

    //    @Mapping(target = "proposalID", source = "???")
    @Mapping(target = "title", source = "title")
    //    @Mapping(target = "version", source = "???")
    //    @Mapping(target = "propositionID", source = "???")
    @Mapping(target = "createdDate", source = "createdAt")
    @Mapping(target = "updatedDate", source = "updatedAt")
    @Mapping(target = "documentType", source = "type")
    ProposalSummaryDTO map(DocumentDomain document);


    @Mapping(target = "propositionId", source = "id", qualifiedByName = "propositionIdUuidMapping")
    @Mapping(target = "proponentId", source = "proponentDTO.id")
    @Mapping(target = "proponentTitle", source = "proponentDTO.title")
    @Mapping(target = "proponentDesignationGenitive", source = "proponentDTO.designationGenitive")
    @Mapping(target = "proponentDesignationNominative", source = "proponentDTO.designationNominative")
    @Mapping(target = "proponentActive", source = "proponentDTO.active")
    @Mapping(target = "createdAt", source = "bearbeitetAm")
    @Mapping(target = "referenzId", ignore = true)
    Proposition mapDto(PropositionDTO propositionDTO);

}
