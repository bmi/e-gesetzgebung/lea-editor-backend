// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.EditorRollenUndRechte;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.DokumentenmappeTableDTOs;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.HomepageCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.HomepageRegulatoryProposalDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.HomepageSingleDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.PermissionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.RegelungsvorhabengTableDTOs;
import de.itzbund.egesetz.bmi.lea.domain.dtos.paging.FilterPropertyType;
import de.itzbund.egesetz.bmi.lea.domain.dtos.paging.PagingUtil;
import de.itzbund.egesetz.bmi.lea.domain.dtos.paging.PaginierungDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.PinnedDokumentenmappenDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserSettingsDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.PropositionState;
import de.itzbund.egesetz.bmi.lea.domain.enums.RegelungsvorhabenTypType;
import de.itzbund.egesetz.bmi.lea.domain.enums.Rights;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DrucksacheDokument;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DrucksacheDokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.PropositionRestPort;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.RESSORT_KURZBEZEICHNUNG_BT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RESSOURCENTYP_DOKUMENTENMAPPE;

/**
 * Service providing functionality to support PropositionController.
 */
@Service
@AllArgsConstructor
@Log4j2
@SuppressWarnings("unused")
public class PropositionService implements PropositionRestPort {

	// --- Services ---
	private BestandsrechtService bestandsrechtService;
	private CompoundDocumentService compoundDocumentService;
	private DrucksacheService drucksacheService;
	private RBACPermissionService rbacPermissionService;
	private RegelungsvorhabenService regelungsvorhabenService;
	private UserService userService;
	private ApplicationContext applicationContext;
	private EditorRollenUndRechte editorRollenUndRechte;
	private DrucksacheDokumentPersistencePort drucksacheDokumentPersistencePort;

	// -- Other ---
	private LeageSession session;
	private PropositionMapper propositionMapper;

	@Override
	public List<HomepageRegulatoryProposalDTO> getRegulatoryProposals(List<ServiceState> status) {
		List<HomepageRegulatoryProposalDTO> regulatoryProposals = getAllPropositionDescriptionsByUser().stream()
			.map(this::getRegulatoryProposalDTO)
			.collect(Collectors.toList());
		status.add(ServiceState.OK);
		return regulatoryProposals;
	}

	// @formatter:off
    /**
     * Listet genau ein Regelungsvorhaben mit genau einer Dokumentenmappe auf. Durch das Paging
     * wird das Regelungsvorhaben implizit ausgewählt.
     * (Paging funktioniert ohne Sortierungsanteil)
     *
     * Die Rechte sind abhängig von:
     *   - dem Status der Dokumentenmappe
     *   - ob (Schreib-)Rechte weitergegeben wurden
     *
     * @param status Der Status der programmatischen Bearbeitung
     * @param paginierungDTO Optional kann Paging verwendet werden
     * @return Ein Baum/Tabelle von Regelungsvorhaben mit angehängten Dokumentenmappen und Dokumenten
     */
    // @formatter:on
	@Override
	public RegelungsvorhabengTableDTOs getStartseiteMeineDokumente(List<ServiceState> status, Optional<PaginierungDTO> paginierungDTO) {

		Pageable pageableForRegelungsvorhaben;
		if (paginierungDTO.isPresent()) {
			PaginierungDTO paginierungDTO1 = paginierungDTO.get();
			pageableForRegelungsvorhaben = PagingUtil.createPageable(paginierungDTO1);
		} else {
			pageableForRegelungsvorhaben = PagingUtil.createDefaultPagingForMeineDokumente();
		}
		User currentUser = session.getUser();

		//Übergangslösung für BT-User
		String ressort = editorRollenUndRechte.getRessortKurzbezeichnungForNutzer(currentUser.getGid().getId());
		if (ressort != null && ressort.equals(RESSORT_KURZBEZEICHNUNG_BT)) {
			return getStartseiteMeineDokumenteForBTUser(status, pageableForRegelungsvorhaben);
		}

		// Alle Regelungsvorhaben des Benutzers erhalten ...
		log.debug("Regelungsvorhaben des Benutzers von PlategRequestService::getAllRegulatoryProposalsForUserHeIsAuthorOf holen.");
		List<RegelungsvorhabenEditorDTO> alleRegelungsvorhabenDesBenutzers = regelungsvorhabenService.getAllRegulatoryProposalsForUserHeIsAuthorOf();
		// ... konvertieren ....
		List<RegelungsVorhabenId> alleRegelungsvorhabenIdsDesBenutzers = alleRegelungsvorhabenDesBenutzers.stream()
			.map(rv -> new RegelungsVorhabenId(rv.getId())).collect(Collectors.toList());
		log.debug("{} Regelungsvorhaben sind dem Benutzers zugeordnet.", alleRegelungsvorhabenIdsDesBenutzers.size());

		int actualNumberOfPage = pageableForRegelungsvorhaben.getPageNumber();

		// Die dazugehörige aktuellste Mappe holen
		AtomicInteger w = new AtomicInteger();
		List<CompoundDocument> compoundDocumentDomains = compoundDocumentService.getNewestCompoundDocumentsForRegelungsvorhabenList(currentUser,
			alleRegelungsvorhabenIdsDesBenutzers, actualNumberOfPage, w);
		log.debug("{} Dokumentenmappen gehören zu den Regelungsvorhaben..", alleRegelungsvorhabenIdsDesBenutzers.size());

		int gesamtAnzahlPages = w.get();
		List<HomepageRegulatoryProposalDTO> meineDokumente = Collections.emptyList();
		if (!compoundDocumentDomains.isEmpty()) {

			CompoundDocument compoundDocumentOfPage = compoundDocumentDomains.get(0);

			// Restliche benötigte Daten zusammensuchen
			RegelungsVorhabenId regelungsVorhabenIdDerDokumentenmappe = compoundDocumentOfPage.getCompoundDocumentEntity().getRegelungsVorhabenId();
			Optional<RegelungsvorhabenEditorDTO> first = alleRegelungsvorhabenDesBenutzers.stream()
				.filter(regelungsvorhabenEditorDTO -> regelungsvorhabenEditorDTO.getId().equals(regelungsVorhabenIdDerDokumentenmappe.getId())).findFirst();
			if (first.isEmpty()) {
				log.error("Sollte aus Konstruktionsgründen nicht vorkommen");
				return RegelungsvorhabengTableDTOs.builder().build();
			}
			RegelungsvorhabenEditorDTO aktuellesRegelungsvorhaben = first.get();

			List<HomepageCompoundDocumentDTO> homepageCompoundDocumentDTOs = getHomepageCompoundDocumentDTOs(
				status, compoundDocumentOfPage, regelungsVorhabenIdDerDokumentenmappe, aktuellesRegelungsvorhaben);

			// ... das Regelungsvorhaben
			meineDokumente = List.of(
				PropositionServiceUtil.createHomepageRegulatoryProposal(aktuellesRegelungsvorhaben, homepageCompoundDocumentDTOs));
		}

		// Arbeiten für das Pageable
		Pageable newPageable = createNewPageable(actualNumberOfPage, 1);

		// Das Rückgabeobjekt erstellen
		status.add(ServiceState.OK);
		return PropositionServiceUtil.getRegelungsvorhabenTable(meineDokumente, newPageable, gesamtAnzahlPages);
	}

	private RegelungsvorhabengTableDTOs getStartseiteMeineDokumenteForBTUser(List<ServiceState> status, Pageable pageable) {

		DocumentService documentService = applicationContext.getBean(DocumentService.class);
		List<CompoundDocumentDomain> compoundDocumentDomains = compoundDocumentService.findByStateSortedByRegelungsVorhabenId(
			DocumentState.BEREIT_FUER_BUNDESTAG);
		compoundDocumentDomains.sort((dm2, dm1) ->
			dm1.getUpdatedAt().compareTo(dm2.getUpdatedAt())
		);

		Set<RegelungsVorhabenId> regelungsVorhabenIds = new HashSet<>();
		compoundDocumentDomains.forEach(x -> regelungsVorhabenIds.add(x.getRegelungsVorhabenId()));

		List<RegelungsvorhabenEditorDTO> rvList = regelungsvorhabenService.getRegelungsvorhabenEditorDTOs(new ArrayList<>(regelungsVorhabenIds));
		if (rvList.isEmpty()) {
			status.add(ServiceState.REGELUNGSVORHABEN_NOT_FOUND);
			return null;
		}
		List<HomepageRegulatoryProposalDTO> meineDokumente = new ArrayList<>();
		UserSettingsDTO userSettingsDTO = userService.getUserSettings();
		List<CompoundDocumentId> compoundDocumentIds = compoundDocumentDomains.stream().map(CompoundDocumentDomain::getCompoundDocumentId)
			.collect(Collectors.toList());
		List<DocumentDomain> allDocumentEntities = documentService.findByCompoundDocumentIdIn(compoundDocumentIds);

		//Für alle Mappen die eine Drucksache verknüpft haben: Diese finden und als Dokument mit zurückgeben
		addDrucksachenToDocumentList(compoundDocumentIds, allDocumentEntities);

		IntStream.range(0, compoundDocumentDomains.size()).forEach(index -> {
			PermissionDTO permissionDTO = PermissionDTO.builder().hasRead(true).hasWrite(false).build();
			CompoundDocumentDomain mappe = compoundDocumentDomains.get(index);
			if (mappe.getCreatedBy().equals(session.getUser())) {
				permissionDTO.setHasWrite(true);
			}
			RegelungsvorhabenEditorDTO rvDTO = rvList.stream().filter(x -> x.getId().equals(mappe.getRegelungsVorhabenId().getId())).findFirst().orElse(null);
			fuegeMappeZuListeHinzu(allDocumentEntities, meineDokumente, propositionMapper.map(rvDTO), mappe, permissionDTO, permissionDTO,
				false, userSettingsDTO);
		});

		status.add(ServiceState.OK);
		Pageable newPageable = createNewPageable(pageable.getPageNumber(), 1);
		return RegelungsvorhabengTableDTOs.builder()
			.dtos(new PageImpl<>(List.of(meineDokumente.get(pageable.getPageNumber())), newPageable, meineDokumente.size()))
			.vorhabenartFilter(List.of(RegelungsvorhabenTypType.values()))
			.filterNames(List.of(FilterPropertyType.REGELUNGSVORHABEN))
			.allContentEmpty(rvList.isEmpty())
			.build();
	}

	private void addDrucksachenToDocumentList(List<CompoundDocumentId> compoundDocumentIds, List<DocumentDomain> allDocumentEntities) {
		for (CompoundDocumentId compoundDocumentId : compoundDocumentIds) {
			Optional<DrucksacheDokument> relatedDrucksacheOptional = drucksacheDokumentPersistencePort.getDrucksacheDokument(compoundDocumentId);
			if (relatedDrucksacheOptional.isPresent()) {
				DrucksacheDokument relatedDrucksache = relatedDrucksacheOptional.get();
				DocumentDomain drucksacheDocumentDomain = DocumentDomain.builder()
					//we set the CompoundDocument id here as the Drucksache can be queried by it
					.documentId(new DocumentId(compoundDocumentId.getId()))
					.inheritFromId(null)
					.title("Drucksache")
					.content(null)
					.type(DocumentType.BT_DRUCKSACHE)
					.createdBy(relatedDrucksache.getCreatedBy())
					.updatedBy(relatedDrucksache.getUpdatedBy())
					.createdAt(relatedDrucksache.getCreatedAt())
					.updatedAt(relatedDrucksache.getUpdatedAt())
					.compoundDocumentId(compoundDocumentId)
					.build();
				allDocumentEntities.add(drucksacheDocumentDomain);
			}
		}
	}

	private List<HomepageCompoundDocumentDTO> getHomepageCompoundDocumentDTOs(List<ServiceState> status, CompoundDocument compoundDocumentOfPage,
		RegelungsVorhabenId regelungsVorhabenIdDerDokumentenmappe, RegelungsvorhabenEditorDTO aktuellesRegelungsvorhaben) {
		List<CompoundDocument> compoundDocuments = new ArrayList<>();
		compoundDocuments.add(compoundDocumentOfPage);
		Set<UUID> pinnedDmIds = new HashSet<>();

		UserSettingsDTO userSettingsDTO = userService.getUserSettings();
		if (userSettingsDTO != null) {
			for (PinnedDokumentenmappenDTO pinnedDokumentenmappenDTO : userSettingsDTO.getPinnedDokumentenmappen()) {
				if (pinnedDokumentenmappenDTO.getRvId().equals(regelungsVorhabenIdDerDokumentenmappe.getId())) {
					processPinnedCompoundDocument(status, compoundDocumentOfPage, aktuellesRegelungsvorhaben, pinnedDokumentenmappenDTO, pinnedDmIds,
						compoundDocuments);
					break;
				}
			}
		}

		// Befüllen
		List<HomepageCompoundDocumentDTO> homepageCompoundDocumentDTOs = new ArrayList<>();
		boolean first = true;
		for (CompoundDocument compoundDocument : compoundDocuments) {

			PermissionDTO documentPermissionDTO = RBACPermissionService.convertDocumentRightsToPermissionDto(compoundDocument.getRechte());
			PermissionDTO commentPermissionDTO = RBACPermissionService.convertCommentRightsToPermissionDto(compoundDocument.getRechte());
			// Die Dokumente ...
			List<HomepageSingleDocumentDTO> homepageSingleDocumentDTOList = compoundDocument.getDocuments().stream()
				.map(document ->
					PropositionServiceUtil.getDocumentDTO(compoundDocument.getState(), document, aktuellesRegelungsvorhaben.getId(),
						documentPermissionDTO, commentPermissionDTO)
				).collect(Collectors.toList());

			HomepageSingleDocumentDTO drucksache = PropositionServiceUtil.getDrucksacheDTO(
				drucksacheService,
				compoundDocument,
				propositionMapper.map(aktuellesRegelungsvorhaben));
			if (drucksache != null) {
				homepageSingleDocumentDTOList.add(0, drucksache);
			}

			// ... die Dokumentenmappe ...
			HomepageCompoundDocumentDTO homepageCompoundDocumentDTO = PropositionServiceUtil.createHomepageCompoundDocument(compoundDocument,
				aktuellesRegelungsvorhaben, homepageSingleDocumentDTOList, documentPermissionDTO, pinnedDmIds);
			if (!first) {
				homepageCompoundDocumentDTO.setIsNewest(false);
			} else {
				homepageCompoundDocumentDTO.setIsNewest(true);
				first = false;
			}

			homepageCompoundDocumentDTOs.add(homepageCompoundDocumentDTO);
		}
		return homepageCompoundDocumentDTOs;
	}

	private void processPinnedCompoundDocument(List<ServiceState> status, CompoundDocument compoundDocumentOfPage,
		RegelungsvorhabenEditorDTO aktuellesRegelungsvorhaben,
		PinnedDokumentenmappenDTO pinnedDokumentenmappenDTO, Set<UUID> pinnedDmIds, List<CompoundDocument> compoundDocuments) {
		for (UUID dmId : pinnedDokumentenmappenDTO.getDmIds()) {
			pinnedDmIds.add(dmId);
			if (!dmId.equals(compoundDocumentOfPage.getCompoundDocumentId().getId())) {
				CompoundDocument compoundDocument = compoundDocumentService.getCompoundDocumentById(new CompoundDocumentId(dmId), status);

				List<Rights> rightsList = rbacPermissionService.getAccessRightsForCurrentUserOnGivenResource("DOKUMENTENMAPPE",
					compoundDocument.getCompoundDocumentId().getId());
				compoundDocument.setRechte(rightsList);

				if (compoundDocument.getRegelungsVorhabenId().getId().equals(aktuellesRegelungsvorhaben.getId())) {
					compoundDocuments.add(compoundDocument);
				}
			}

		}
	}

	private Pageable createNewPageable(int aktuelleSeite, int anzahlElementeAufDieserSeite) {
		Sort sort = Sort.by("updatedAt").descending();
		return PageRequest.of(aktuelleSeite, anzahlElementeAufDieserSeite, sort);
	}

	// ========================================

	private Map<UUID, PropositionDTO> getAlleRegelungsvorhabenDesBenutzers(List<String> regelungsvorhabenIds) {
		Map<UUID, PropositionDTO> ret = new HashMap<>();
		log.debug("Hole Regelungsvorhaben von PlategRequestService.");
		List<RegelungsvorhabenEditorDTO> regelungsvorhabenEditorDTOList = regelungsvorhabenService.getRegulatoryProposalsByIdList(regelungsvorhabenIds);
		regelungsvorhabenEditorDTOList.forEach(x -> ret.put(x.getId(), propositionMapper.map(x)));
		log.debug("{} Reglungsvorhaben von PlategRequestService::getRegulatoryProposalsByIdList bei {} Anfragen erhalten.", ret.size(),
			regelungsvorhabenIds.size());
		return ret;
	}

	private static void putLastCreatedCompoundDocumentAtFirst(List<CompoundDocumentDomain> mappenZuEigenenRegelungsvorhaben2) {
		CompoundDocumentDomain newest = mappenZuEigenenRegelungsvorhaben2.get(0);
		for (CompoundDocumentDomain documentDomain : mappenZuEigenenRegelungsvorhaben2) {
			if (newest.getCreatedAt().isBefore(documentDomain.getCreatedAt())) {
				newest = documentDomain;
			}
		}
		mappenZuEigenenRegelungsvorhaben2.remove(newest);
		mappenZuEigenenRegelungsvorhaben2.add(0, newest);
	}

	private PermissionDTO getEditPermissionForCompoundDocument(CompoundDocumentDomain compoundDocumentDomain) {
		List<Rights> rightsList = rbacPermissionService.getAccessRightsForCurrentUserOnGivenResource(
			RESSOURCENTYP_DOKUMENTENMAPPE, compoundDocumentDomain.getCompoundDocumentId().getId());

		return PermissionDTO.builder().hasRead(rightsList.contains(Rights.LESEN)).hasWrite(rightsList.contains(Rights.SCHREIBEN)).build();
	}

	private PermissionDTO getCommentPermissionForCompoundDocument(CompoundDocumentDomain compoundDocumentDomain) {
		List<Rights> rightsList = rbacPermissionService.getAccessRightsForCurrentUserOnGivenResource(
			RESSOURCENTYP_DOKUMENTENMAPPE, compoundDocumentDomain.getCompoundDocumentId().getId());

		return PermissionDTO.builder().hasRead(rightsList.contains(Rights.KOMMENTARE_LESEN)).hasWrite(rightsList.contains(Rights.KOMMENTIEREN)).build();
	}

	/**
	 * Listet alle Dokumente auf, die der Benutzer sehen/bearbeiten darf, aber nicht von ihm erstellt worden sind.
	 *
	 * @return List
	 */
	@Override
	public List<HomepageRegulatoryProposalDTO> getStartseiteDokumenteAusAbstimmungen(List<ServiceState> status) {

		final List<HomepageRegulatoryProposalDTO> meineDokumente = new ArrayList<>();
		User currentUser = session.getUser();

		// 1. Alle meine Dokumente, die aus einer HRA und dann einer neuen Version entstanden sind
		Map<UUID, PropositionDTO> bereitsAbgefragteRegelungsvorhaben = dokumentenMappenEinerHRA(meineDokumente);
		log.debug("{} Dokumentenmappem[Ids] einer HRA", bereitsAbgefragteRegelungsvorhaben.size());

		// 2. alle Dokumente für die mir Leserechte eingeräumt wurden
		Set<String> dokumentenmappenIdsMitLeserechten = dokumentenMappenMitUebertragenenLeserechten(bereitsAbgefragteRegelungsvorhaben, meineDokumente);
		log.debug("{} Dokumentenmappem[Ids] mit eingeräumten Leserechten", dokumentenmappenIdsMitLeserechten.size());

		// 3. alle Dokumente für die mir Schreibrechte eingeräumt wurden
		Set<String> dokumentenmappenIdsMitSchreibrechten = dokumentenMappenMitUebertragenenSchreibrechten(bereitsAbgefragteRegelungsvorhaben, meineDokumente);
		log.debug("{} Dokumentenmappem[Ids] mit eingeräumten Schreibrechten", dokumentenmappenIdsMitSchreibrechten.size());

		log.debug("Beginn von Sortieroperationen: ");
		long startTime = System.currentTimeMillis();
		sortDMsAfterUpdatedAtDescending(meineDokumente);

		sortRVsAfterUpdatedAtDescending(meineDokumente);
		if (log.isDebugEnabled()) {
			long endTime = System.currentTimeMillis();
			long duration = endTime - startTime;
			log.debug("Ende von Sortieroperationen. Dauer: {} MilliSekunden.", duration);
		}

		status.add(ServiceState.OK);
		return meineDokumente;
	}

	private static void sortRVsAfterUpdatedAtDescending(List<HomepageRegulatoryProposalDTO> meineDokumente) {
		meineDokumente.sort((rv2, rv1) ->
		{
			Instant rv1Max = Instant.MIN;
			Optional<HomepageSingleDocumentDTO> newestDocumentOfRv1 = rv1.getChildren().stream().flatMap(x -> x.getChildren().stream())
				.max(Comparator.comparing(
					HomepageSingleDocumentDTO::getUpdatedAt));
			if (newestDocumentOfRv1.isPresent()) {
				rv1Max = newestDocumentOfRv1.get().getUpdatedAt();
			}

			Instant rv2Max = Instant.MIN;
			Optional<HomepageSingleDocumentDTO> newestDocumentOfRv2 = rv2.getChildren().stream().flatMap(x -> x.getChildren().stream())
				.max(Comparator.comparing(
					HomepageSingleDocumentDTO::getUpdatedAt));
			if (newestDocumentOfRv2.isPresent()) {
				rv2Max = newestDocumentOfRv2.get().getUpdatedAt();
			}

			return rv1Max
				.compareTo(rv2Max);
		});
	}

	private static void sortDMsAfterUpdatedAtDescending(List<HomepageRegulatoryProposalDTO> meineDokumente) {
		meineDokumente.forEach(regulatoryProposalDTO -> regulatoryProposalDTO.getChildren().sort(
			(dm2, dm1) ->
			{
				Instant dm1Max = Instant.MIN;
				Optional<HomepageSingleDocumentDTO> newestDocumentOf1 = dm1.getChildren().stream().max(Comparator.comparing(
					HomepageSingleDocumentDTO::getUpdatedAt));
				if (newestDocumentOf1.isPresent()) {
					dm1Max = newestDocumentOf1.get().getUpdatedAt();
				}
				Instant dm2Max = Instant.MIN;
				Optional<HomepageSingleDocumentDTO> newestDocumentOf2 = dm2.getChildren().stream().max(Comparator.comparing(
					HomepageSingleDocumentDTO::getUpdatedAt));
				if (newestDocumentOf2.isPresent()) {
					dm2Max = newestDocumentOf2.get().getUpdatedAt();
				}
				return dm1Max
					.compareTo(dm2Max);
			}));
	}

	/**
	 * Alle meine Dokumente, die aus einer HRA und dann einer neuen Version entstanden sind
	 */
	@Async
	private Map<UUID, PropositionDTO> dokumentenMappenEinerHRA(List<HomepageRegulatoryProposalDTO> meineDokumente) {
		// 1. Alle meine Dokumente, die aus einer HRA und dann einer neuen Version entstanden sind
		Set<String> alleDokumentenmappenFuerBenutzerInAbstimmung = rbacPermissionService.getAlleDokumentenmappemFuerBenutzerAlsMitarbeiter(
			session.getUser().getGid());

		List<CompoundDocument> kopieInEigeneVersion = getCompoundDocumentsFromIdList(alleDokumentenmappenFuerBenutzerInAbstimmung, DocumentState.FREEZE);

		Map<UUID, PropositionDTO> bereitsAbgefragteRegelungsvorhaben = getAlleRegelungsvorhabenDesBenutzers(
			kopieInEigeneVersion.stream().map(x -> x.getRegelungsVorhabenId().getId().toString()).collect(Collectors.toList()));

		log.debug("Neue Versionen aus HRA: {}", kopieInEigeneVersion.size());

		// Mapping - als neue Version erstellte Kopien
		// Rechte:
		// Lesen: ja, da das Dokument dem Benutzer gehören muss
		// Schreiben: Ist abhängig vom Status der Dokumentenmappe
		ordneDokumentenMappenInDTOEin(kopieInEigeneVersion, meineDokumente, bereitsAbgefragteRegelungsvorhaben, false);
		return bereitsAbgefragteRegelungsvorhaben;
	}

	/**
	 * Alle Dokumente für die mir Leserechte eingeräumt wurden
	 */
	@Async
	private Set<String> dokumentenMappenMitUebertragenenLeserechten(Map<UUID, PropositionDTO> bereitsAbgefragteRegelungsvorhaben,
		List<HomepageRegulatoryProposalDTO> meineDokumente) {
		// 2. alle Dokumente für die mir Leserechte eingeräumt wurden
		Set<String> dokumentenmappenIdsMitLeserechten = new HashSet<>(
			rbacPermissionService.getDokumentenmappenIdsFuerNutzerAlsLeser(session.getUser().getGid()));
		List<CompoundDocument> uebertrageneLeserechte = getCompoundDocumentsFromIdList(dokumentenmappenIdsMitLeserechten, null);
		log.debug("Übertragene Leserechte: {}", uebertrageneLeserechte.size());

		uebertrageneLeserechte = filterEigeneDokumentenMappenHeraus(uebertrageneLeserechte);

		// Mapping - Ordner mit übertragenen Leserechten
		// Rechte:
		// Lesen: ja, weil die Leserechte übertragen wurden
		// Schreiben: Nein, sind ja NUR Leserechte
		if (!uebertrageneLeserechte.isEmpty()) {
			ordneDokumentenMappenInDTOEin(new ArrayList<>(uebertrageneLeserechte), meineDokumente, bereitsAbgefragteRegelungsvorhaben, false);
		}
		return dokumentenmappenIdsMitLeserechten;
	}

	private List<CompoundDocument> filterEigeneDokumentenMappenHeraus(List<CompoundDocument> uebertrageneRechte) {
		int in = uebertrageneRechte.size();
		List<CompoundDocument> neueListe = uebertrageneRechte.stream()
			.filter(compoundDocument -> !compoundDocument.getCreatedBy().getGid().equals(session.getUser().getGid()))
			.collect(Collectors.toList());

		if (log.isDebugEnabled()) {
			int out = neueListe.size();
			log.debug("{} Elemente bei eigene Rechte herausgefiltert", out - in);
		}

		return neueListe;
	}

	/**
	 * Alle Dokumente für die mir Schreibrechte eingeräumt wurden
	 */
	@Async
	private Set<String> dokumentenMappenMitUebertragenenSchreibrechten(Map<UUID, PropositionDTO> bereitsAbgefragteRegelungsvorhaben,
		List<HomepageRegulatoryProposalDTO> meineDokumente) {
		// 3. alle Dokumente für die mir Schreibrechte eingeräumt wurden
		Set<String> dokumentenmappenIdsMitSchreibrechten = new HashSet<>(
			rbacPermissionService.getDokumentenmappenIdsFuerNutzerAlsSchreiber(session.getUser().getGid()));
		List<CompoundDocument> uebertrageneSchreibrechte = getCompoundDocumentsFromIdList(dokumentenmappenIdsMitSchreibrechten, null);

		log.debug("Übertragene Schreibrechte: {}", uebertrageneSchreibrechte.size());

		uebertrageneSchreibrechte = filterEigeneDokumentenMappenHeraus(uebertrageneSchreibrechte);

		// Mapping - Ordner mit übertragenen Schreibrechten
		// Rechte:
		// Lesen: ja, weil es sonst keinen Sinn ergibt
		// Schreiben: ja, weil die Schreibrechte übertragen wurden
		if (!uebertrageneSchreibrechte.isEmpty()) {
			ordneDokumentenMappenInDTOEin(new ArrayList<>(uebertrageneSchreibrechte), meineDokumente, bereitsAbgefragteRegelungsvorhaben, true);
		}
		return dokumentenmappenIdsMitSchreibrechten;
	}

	private List<CompoundDocument> getCompoundDocumentsFromIdList(Set<String> compoundDocumentIdSet, DocumentState filterDocumentState) {

		DocumentService documentService = applicationContext.getBean(DocumentService.class);
		List<CompoundDocumentId> compoundDocumentIds = compoundDocumentIdSet.stream()
			.map(id -> new CompoundDocumentId(UUID.fromString(id))).collect(Collectors.toList());

		List<CompoundDocumentDomain> compoundDocumentDomains;
		if (filterDocumentState == null) {
			compoundDocumentDomains = compoundDocumentService.findByCompoundDocumentIdIn(compoundDocumentIds);
		} else {
			compoundDocumentDomains = compoundDocumentService.findByCompoundDocumentIdInAndCompoundDocumentState(compoundDocumentIds, filterDocumentState);
		}

		List<DocumentDomain> documentDomains = documentService.findByCompoundDocumentIdIn(compoundDocumentIds);
		Map<CompoundDocumentId, List<DocumentDomain>> documentMap = documentDomains.stream()
			.collect(Collectors.groupingBy(DocumentDomain::getCompoundDocumentId));

		return compoundDocumentDomains.stream().map(cd -> {
			CompoundDocument compoundDocument = new CompoundDocument();
			compoundDocument.setCompoundDocumentEntity(cd);
			List<Document> documents;
			if (documentMap.get(cd.getCompoundDocumentId()) != null) {
				documents = documentMap.get(cd.getCompoundDocumentId()).stream().map(documentDomain -> {
					Document document = new Document(documentDomain);
					document.setDocumentState(cd.getState());
					return document;
				}).collect(Collectors.toList());
				compoundDocument.setDocuments(
					documents
				);
			}
			return compoundDocument;
		}).collect(Collectors.toList());
	}

	@Override
	public DokumentenmappeTableDTOs getVersionshistorie(List<ServiceState> status, UUID regulatoryProposalId, Optional<PaginierungDTO> paginierungDTO) {
		User currentUser = session.getUser();
		List<HomepageRegulatoryProposalDTO> meineDokumente = new ArrayList<>();

		Pageable pageable;
		if (paginierungDTO.isPresent()) {
			pageable = PagingUtil.createPageable(paginierungDTO.get());
		} else {
			pageable = PagingUtil.createDefaultPagingForVersionshistorie();
		}

		RegelungsVorhabenId rvId = new RegelungsVorhabenId(regulatoryProposalId);
		Page<CompoundDocumentDomain> mappenZuEigenenRegelungsvorhabenPage =
			compoundDocumentService.findByRegelungsvorhabenIdInSortedByUpdatedAt(List.of(rvId), pageable);
		List<CompoundDocumentDomain> mappenZuEigenenRegelungsvorhabenList = new ArrayList<>(mappenZuEigenenRegelungsvorhabenPage.getContent());
		if (mappenZuEigenenRegelungsvorhabenList.isEmpty()) {
			status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
		}

		PropositionDTO proposition;
		List<RegelungsvorhabenEditorDTO> rvForVersionHistory = regelungsvorhabenService.getRegulatoryProposalsByIdList(
			List.of(regulatoryProposalId.toString()));
		if (rvForVersionHistory.isEmpty()) {
			User createdBy = mappenZuEigenenRegelungsvorhabenList.get(0).getCreatedBy();
			proposition = getPropositionForRegelungsvorhabenIdByPlattform(createdBy, new RegelungsVorhabenId(regulatoryProposalId));
		} else {
			proposition = propositionMapper.map(rvForVersionHistory.get(0));
		}

		if (proposition == null) {
			status.add(ServiceState.REGELUNGSVORHABEN_NOT_FOUND);
			return null;
		}

		buildDokumentenmappenAndAddToList(mappenZuEigenenRegelungsvorhabenPage, pageable, proposition, meineDokumente);
		if (meineDokumente.isEmpty()) {
			status.add(ServiceState.DOCUMENT_NOT_FOUND);
			return null;
		}

		status.add(ServiceState.OK);
		return DokumentenmappeTableDTOs.builder()
			.dtos(new PageImpl<>(meineDokumente, pageable, mappenZuEigenenRegelungsvorhabenPage.getTotalElements()))
			.vorhabenartFilter(List.of(DocumentType.values()))
			.filterNames(List.of(FilterPropertyType.REGELUNGSVORHABEN))
			.allContentEmpty(mappenZuEigenenRegelungsvorhabenPage.getTotalElements() == 0)
			.build();
	}

	private void buildDokumentenmappenAndAddToList(Page<CompoundDocumentDomain> mappenZuEigenenRegelungsvorhabenPage, Pageable pageable,
		PropositionDTO proposition, List<HomepageRegulatoryProposalDTO> meineDokumente) {
		DocumentService documentService = applicationContext.getBean(DocumentService.class);
		List<CompoundDocumentDomain> mappenZuEigenenRegelungsvorhabenList = new ArrayList<>(mappenZuEigenenRegelungsvorhabenPage.getContent());
		if (!mappenZuEigenenRegelungsvorhabenList.isEmpty() && pageable.getPageNumber() == 0) {
			putLastCreatedCompoundDocumentAtFirst(mappenZuEigenenRegelungsvorhabenList);
		}
		List<CompoundDocumentId> compoundDocumentIds = mappenZuEigenenRegelungsvorhabenList.stream().map(CompoundDocumentDomain::getCompoundDocumentId)
			.collect(Collectors.toList());
		List<DocumentDomain> allDocumentEntities = documentService.findByCompoundDocumentIdIn(compoundDocumentIds);

		UserSettingsDTO userSettingsDTO = userService.getUserSettings();
		IntStream.range(0, mappenZuEigenenRegelungsvorhabenList.size()).forEach(index -> {
			CompoundDocumentDomain mappe = mappenZuEigenenRegelungsvorhabenList.get(index);
			PermissionDTO documentPermissionDTO = getEditPermissionForCompoundDocument(mappe);
			PermissionDTO commentPermissionDTO = getCommentPermissionForCompoundDocument(mappe);
			if (documentPermissionDTO.isHasRead()) {
				fuegeMappeZuListeHinzu(allDocumentEntities, meineDokumente, proposition, mappe, documentPermissionDTO, commentPermissionDTO,
					index == 0 && pageable.getPageNumber() == 0, userSettingsDTO);
			}
		});
	}

	private void ordneDokumentenMappenInDTOEin(List<CompoundDocument> quelle, List<HomepageRegulatoryProposalDTO> ziel,
		Map<UUID, PropositionDTO> bereitsAbgefragteRegelungsvorhaben, boolean hasWrite) {
		AtomicInteger hit = new AtomicInteger();
		AtomicInteger currentUserTries = new AtomicInteger();
		AtomicInteger fallbacks = new AtomicInteger();

		quelle.forEach(
			dm -> {
				PermissionDTO documentPermissions = new PermissionDTO(true, false);
				if (hasWrite) {
					documentPermissions.setHasWrite(true);
				}
				PermissionDTO commentPermissions = new PermissionDTO(true, false);
				if (hasWrite) {
					commentPermissions.setHasWrite(true);
				}

				// Try to prevent another platform request
				RegelungsVorhabenId regelungsVorhabenId = dm.getRegelungsVorhabenId();

				PropositionDTO proposition;
				if (bereitsAbgefragteRegelungsvorhaben.containsKey(regelungsVorhabenId.getId())) {
					proposition = bereitsAbgefragteRegelungsvorhaben.get(regelungsVorhabenId.getId());
					hit.getAndIncrement();
				} else {
					// Try with user of Dokumentenmappe
					User createdBy = dm.getCreatedBy();
					proposition = getPropositionForRegelungsvorhabenIdByPlattform(createdBy, regelungsVorhabenId);

					if (proposition == null) {
						// Try with current user
						proposition = getPropositionForRegelungsvorhabenIdByPlattform(regelungsVorhabenId);
						currentUserTries.getAndIncrement();

						// A hard fallback
						if (proposition == null) {
							proposition = propositionMapper.map(
								PropositionServiceUtil.dummyRegelungsvorhabenEditorDTO());
							fallbacks.getAndIncrement();
						}
					}
					bereitsAbgefragteRegelungsvorhaben.put(regelungsVorhabenId.getId(), proposition);
				}

				if (PropositionState.IN_BEARBEITUNG.equals(proposition.getState())) {
					baueHauptansichtAuf(ziel, dm, proposition, documentPermissions, commentPermissions);
				}
			}
		);

		log.debug("{} Dokumentenmappen in DTO überführt. \n"
			+ "Dabei wurden bei {} Dokumentenmappen {} Abfragen \n"
			+ "nicht ein weiteres mal durchgeführt.", ziel.size(), quelle.size(), hit);
		log.debug("{} mal musste ein anderer User benutzt werden und {} mal wurde gar nichts gefunden.",
			currentUserTries, fallbacks);
	}

	private PropositionDTO getPropositionForRegelungsvorhabenIdByPlattform(User createdBy,
		RegelungsVorhabenId regelungsVorhabenId) {
		RegelungsvorhabenEditorDTO dto = regelungsvorhabenService.getRegulatoryProposalUserCanAccess(createdBy,
			regelungsVorhabenId.getId());
		return propositionMapper.map(dto);
	}

	private PropositionDTO getPropositionForRegelungsvorhabenIdByPlattform(RegelungsVorhabenId regelungsVorhabenId) {
		return getPropositionForRegelungsvorhabenIdByPlattform(session.getUser(), regelungsVorhabenId);
	}

	private List<HomepageRegulatoryProposalDTO> baueHauptansichtAuf(List<HomepageRegulatoryProposalDTO> hauptansicht,
		CompoundDocument dokumentenmappe, PropositionDTO regelungsvorhaben, PermissionDTO documentPermissions, PermissionDTO commentPermissions) {
		// Erzeuge oder bekomme einen bestehenden Eintrag zu einem Regelungsvorhaben
		List<HomepageRegulatoryProposalDTO> root = getHomepageRegulatoryProposalDTOorNew2(hauptansicht,
			regelungsvorhaben);

		// Füge die Dokumentenmappe (mit Dokumenten) in die Liste ein
		return fuegeDokumentenMappeDerStartseiteAn(root, dokumentenmappe, documentPermissions, commentPermissions, regelungsvorhaben);
	}

	private void fuegeMappeZuListeHinzu(List<DocumentDomain> allDocumentEntities, List<HomepageRegulatoryProposalDTO> dokumente,
		PropositionDTO regelungsvorhaben, CompoundDocumentDomain mappe, PermissionDTO documentPermissionDTO, PermissionDTO commentPermissionDTO, boolean newest,
		UserSettingsDTO userSettingsDTO) {

		HomepageRegulatoryProposalDTO root = getHomepageRegulatoryProposalDTOorNew(dokumente, regelungsvorhaben);

		boolean pinned = false;
		if (userSettingsDTO != null) {
			PinnedDokumentenmappenDTO pinnedDokumentenmappenDTO = userSettingsDTO.getPinnedDokumentenmappen().stream()
				.filter(x -> x.getRvId().equals(regelungsvorhaben.getId())).findFirst().orElse(null);
			if (pinnedDokumentenmappenDTO != null) {
				pinned = pinnedDokumentenmappenDTO.getDmIds().contains(mappe.getCompoundDocumentId().getId());

			}
		}

		List<DocumentDomain> documentEntities =
			allDocumentEntities.stream()
				.filter(documentDomain ->
					documentDomain.getCompoundDocumentId()
						.equals(mappe.getCompoundDocumentId()))
				.collect(Collectors.toList());

		HomepageCompoundDocumentDTO compoundDocumentDTO = getCompoundDocumentDTO2(documentEntities, mappe,
			regelungsvorhaben.getId(), documentPermissionDTO, commentPermissionDTO, newest, pinned);

		List<HomepageCompoundDocumentDTO> children = root.getChildren();
		children.add(compoundDocumentDTO);
	}

	/**
	 * Erzeugt das Objekt für die Hauptseite und fügt ggf. ein Regelungsvorhaben an
	 *
	 * @param hauptansicht      Die Hauptansicht (call-by-reference)
	 * @param regelungsvorhaben Das Regelungsvorhaben
	 * @return Das DTO für die Hauptansicht
	 */
	private List<HomepageRegulatoryProposalDTO> getHomepageRegulatoryProposalDTOorNew2(
		List<HomepageRegulatoryProposalDTO> hauptansicht, PropositionDTO regelungsvorhaben) {
		if (hauptansicht == null) {
			hauptansicht = new ArrayList<>();
		}

		if (regelungsvorhaben == null) {
			regelungsvorhaben = PropositionDTO.builder().id(UUID.fromString("FFFFFFFF-0000-0000-0000-FFFFFFFFFFFF"))
				.abbreviation("n.a.").build();
		}

		// Durchsuchen, ob es einen Eintrag schon gibt
		for (HomepageRegulatoryProposalDTO dto : hauptansicht) {
			if (dto.getId().equals(regelungsvorhaben.getId())) {
				// Gibt's schon. Wir brauchen nichts mehr zu machen.
				return hauptansicht;
			}
		}

		// Ein Eintrag fehlt, also erzeugen wir ihn
		hauptansicht.add(
			HomepageRegulatoryProposalDTO.builder()
				.id(regelungsvorhaben.getId())
				.abbreviation(regelungsvorhaben.getAbbreviation())
				.farbe(regelungsvorhaben.getFarbe())
				.children(new ArrayList<>())
				.build()
		);

		return hauptansicht;
	}

	private List<HomepageRegulatoryProposalDTO> fuegeDokumentenMappeDerStartseiteAn(
		List<HomepageRegulatoryProposalDTO> hauptseite,
		CompoundDocument compoundDocument, PermissionDTO documentPermissions, PermissionDTO commentPermissions, PropositionDTO regulatoryProposal) {

		Optional<HomepageRegulatoryProposalDTO> regulatoryProposalEntry = hauptseite.stream()
			.filter(h -> h.getId().equals(regulatoryProposal.getId())).findFirst();

		if (regulatoryProposalEntry.isEmpty()) {
			log.error("Regelungsvorhaben {} wurde nicht gefunden.", regulatoryProposal);
			return hauptseite;
		}

		HomepageRegulatoryProposalDTO homepageRegulatoryProposalDTO = regulatoryProposalEntry.get();
		// Einträge können schon existieren
		List<HomepageCompoundDocumentDTO> compoundDocumentEntry = homepageRegulatoryProposalDTO.getChildren();

		Optional<HomepageCompoundDocumentDTO> eintragZuAktuellemRegelungsvorhaben = compoundDocumentEntry.stream()
			.filter(cd -> cd.getId().equals(compoundDocument.getCompoundDocumentId().getId())).findFirst();

		if (eintragZuAktuellemRegelungsvorhaben.isPresent()) {
			log.error("Dokumentenmappe ist nicht wie erwartet leer.");
			return hauptseite;
		}

		boolean pinned = false;
		UserSettingsDTO userSettingsDTO = userService.getUserSettings();
		if (userSettingsDTO != null) {
			PinnedDokumentenmappenDTO pinnedDokumentenmappenDTO = userSettingsDTO.getPinnedDokumentenmappen().stream()
				.filter(x -> x.getRvId().equals(regulatoryProposal.getId())).findFirst().orElse(null);
			if (pinnedDokumentenmappenDTO != null) {
				pinned = pinnedDokumentenmappenDTO.getDmIds().contains(compoundDocument.getCompoundDocumentId().getId());
			}
		}

		HomepageCompoundDocumentDTO neuesElement = HomepageCompoundDocumentDTO.builder()
			.id(compoundDocument.getCompoundDocumentId()
				.getId())
			.version(compoundDocument.getCompoundDocumentEntity().getVersion())
			.regulatoryProposalId(regulatoryProposal.getId())
			.title(compoundDocument.getCompoundDocumentEntity().getTitle())
			.type(compoundDocument.getCompoundDocumentEntity().getType())
			.state(compoundDocument.getState())
			.children(compoundDocument.getDocuments().stream()
				.map(document -> PropositionServiceUtil.getDocumentDTO(compoundDocument.getState(), document,
					homepageRegulatoryProposalDTO.getId(), documentPermissions, commentPermissions))
				.collect(Collectors.toList()))
			.documentPermissions(documentPermissions)
			.commentPermissions(commentPermissions)
			.isPinned(pinned)
			.build();

		HomepageSingleDocumentDTO drucksache = PropositionServiceUtil.getDrucksacheDTO(drucksacheService, compoundDocument, regulatoryProposal);
		if (drucksache != null) {
			neuesElement.getChildren().add(0, drucksache);
		}

		compoundDocumentEntry.add(neuesElement);

		return hauptseite;
	}

	private HomepageRegulatoryProposalDTO getHomepageRegulatoryProposalDTOorNew(List<HomepageRegulatoryProposalDTO> dokumente,
		PropositionDTO regelungsvorhaben) {
		HomepageRegulatoryProposalDTO result = null;
		for (HomepageRegulatoryProposalDTO dto : dokumente) {
			if (dto.getId()
				.equals(regelungsvorhaben.getId())) {
				result = dto;
				break;
			}
		}

		if (result == null) {
			result = HomepageRegulatoryProposalDTO.builder()
				.id(regelungsvorhaben.getId())
				.abbreviation(regelungsvorhaben.getAbbreviation())
				.farbe(regelungsvorhaben.getFarbe())
				.status(VorhabenStatusType.valueOf(regelungsvorhaben.getState().toString()))
				.children(new ArrayList<>())
				.build();
			dokumente.add(result);
		}

		return result;
	}

	private HomepageRegulatoryProposalDTO getRegulatoryProposalDTO(PropositionDTO propositionDTO) {
		User currentUser = session.getUser();
		RegelungsVorhabenId regelungsVorhabenId = new RegelungsVorhabenId(propositionDTO.getId());
		List<CompoundDocumentDomain> compoundDocumentEntities = compoundDocumentService.
			findByCreatedByAndRegelungsVorhabenId(currentUser, regelungsVorhabenId);

		return HomepageRegulatoryProposalDTO.builder()
			.id(propositionDTO.getId())
			.abbreviation(propositionDTO.getAbbreviation())
			.children(compoundDocumentEntities.stream()
				.map(entity -> getCompoundDocumentDTO(currentUser, entity, propositionDTO.getId(),
					getEditPermissionForCompoundDocument(entity), getCommentPermissionForCompoundDocument(entity)))
				.collect(Collectors.toList()))
			.build();
	}

	private HomepageCompoundDocumentDTO getCompoundDocumentDTO(User userEntity, CompoundDocumentDomain compoundDocumentEntity, UUID regulatoryProposalId,
		PermissionDTO documentPermissionDTO, PermissionDTO commentPermissionDTO) {

		DocumentService documentService = applicationContext.getBean(DocumentService.class);
		DocumentState documentState = compoundDocumentEntity.getState();

		List<DocumentDomain> documentEntities = documentService.findByCreatedByAndCompoundDocumentId(userEntity,
			compoundDocumentEntity.getCompoundDocumentId());

		return HomepageCompoundDocumentDTO.builder()
			.id(compoundDocumentEntity.getCompoundDocumentId()
				.getId())
			.version(compoundDocumentEntity.getVersion())
			.regulatoryProposalId(regulatoryProposalId)
			.title(compoundDocumentEntity.getTitle())
			.type(compoundDocumentEntity.getType())
			.state(documentState)
			.children(documentEntities.stream()
				.map(entity -> PropositionServiceUtil.getDocumentDTO(documentState, entity, regulatoryProposalId, documentPermissionDTO, commentPermissionDTO))
				.collect(Collectors.toList()))
			.build();
	}

	private HomepageCompoundDocumentDTO getCompoundDocumentDTO2(List<DocumentDomain> documentEntities,
		CompoundDocumentDomain compoundDocumentEntity, UUID regulatoryProposalId, PermissionDTO documentPermissionDTO, PermissionDTO commentPermissionDTO,
		boolean newest, boolean pinned) {
		DocumentState documentState = compoundDocumentEntity.getState();

		return HomepageCompoundDocumentDTO.builder()
			.id(compoundDocumentEntity.getCompoundDocumentId()
				.getId())
			.version(compoundDocumentEntity.getVersion())
			.regulatoryProposalId(regulatoryProposalId)
			.title(compoundDocumentEntity.getTitle())
			.type(compoundDocumentEntity.getType())
			.state(documentState)
			.isNewest(newest)
			.isPinned(pinned)
			.children(documentEntities.stream()
				.map(entity -> PropositionServiceUtil.getDocumentDTO(documentState, entity, regulatoryProposalId, documentPermissionDTO, commentPermissionDTO))
				.sorted((e2, e1) -> e1.getUpdatedAt().compareTo(e2.getUpdatedAt()))
				.collect(Collectors.toList()))
			.documentPermissions(documentPermissionDTO)
			.commentPermissions(commentPermissionDTO)
			.build();
	}

	/**
	 * Returns the list of descriptions of all propositions created by the current user
	 *
	 * @return list of {@link PropositionDTO}
	 */
	@Override
	public List<PropositionDTO> getAllPropositionDescriptionsByUser() {
		List<RegelungsvorhabenEditorDTO> dtos = regelungsvorhabenService.getAllRegulatoryProposalsForUserHeIsAuthorOf();
		return dtos.stream()
			.map(propositionMapper::map)
			.collect(Collectors.toList());
	}

	public UUID getPropositionUuid(DocumentDomain documentEntity) {
		log.debug("Try get from document's proposition field.");
		UUID propositionId = getRegelungsvorhabenIdForCompoundDocument(documentEntity.getCompoundDocumentId());
		log.debug("We got propositionId '{}' from document's compound document.", propositionId);

		if (propositionId == null) {
			log.error("PropositionId could not be retrieved");
		}

		return propositionId;
	}

	protected UUID getRegelungsvorhabenIdForCompoundDocument(CompoundDocumentId id) {
		Optional<CompoundDocumentDomain> byId = compoundDocumentService.findByCompoundDocumentId(id);
		return byId.map(compoundDocumentEntity -> compoundDocumentEntity.getRegelungsVorhabenId()
				.getId())
			.orElse(null);
	}

	public void bestandsrechtDatenSpeicherung(String gid, UUID regelungsvorhabenId, BestandsrechtDTO bestandsrechtDTO, List<ServiceState> status) {
		if (checkRvPermissions(gid, regelungsvorhabenId, status)) {
			return;
		}

		bestandsrechtService.bestandsrechtDatenSpeicherung(gid, regelungsvorhabenId, bestandsrechtDTO, status);
	}

	private boolean checkRvPermissions(String gid, UUID regelungsvorhabenId, List<ServiceState> status) {
		User user = session.getUser();
		if (user == null || !user.getGid()
			.getId()
			.equals(gid)) {
			status.add(ServiceState.NO_PERMISSION);
			return true;
		}

		List<PropositionDTO> userProposals = getAllPropositionDescriptionsByUser();
		if (!userProposals.stream()
			.map(PropositionDTO::getId)
			.collect(Collectors.toList())
			.contains(regelungsvorhabenId)) {
			status.add(ServiceState.NO_PERMISSION);
			return true;
		}
		return false;
	}

}
