// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.compare;

import de.itzbund.egesetz.bmi.lea.core.json.TraversableJSONDocument;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Arrays;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_AKOMANTOSO;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getLocalName;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.wrapUp;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.isMissing;
import static de.itzbund.egesetz.bmi.lea.domain.model.DocumentUtils.getDocumentType;

@Log4j2
public abstract class AbstractDocumentComparer implements DocumentComparer {

    protected static final DiffMatchPatch DMP = new DiffMatchPatch();

    protected TraversableJSONDocument baseDocument;
    protected TraversableJSONDocument versionDocument;


    @Override
    public JSONArray getBaseDocument() {
        return wrapUp(baseDocument.getDocumentObject());
    }


    @Override
    public JSONArray getVersionDocument() {
        return wrapUp(versionDocument.getDocumentObject());
    }


    protected void initialize(@NonNull JSONObject baseDocumentObject, @NonNull JSONObject versionDocumentObject) {
        baseDocument = new TraversableJSONDocument(baseDocumentObject);
        versionDocument = new TraversableJSONDocument(versionDocumentObject);

        log.debug("checking ...");

        if (!hasSupportedSerializationType(baseDocumentObject, versionDocumentObject)) {
            log.error("no AkomaNtoso document");
            return;
        }

        DocumentType baseDocumentType = getDocumentType(baseDocumentObject);
        DocumentType versionDocumentType = getDocumentType(versionDocumentObject);
        if (baseDocumentType != versionDocumentType) {
            log.error("different document types cannot be compared: {} vs {}",
                baseDocumentType, versionDocumentType);
            return;
        }

        log.debug("comparing ...");
        // no comparison of meta containers
        baseDocument.advanceToNextLeave();
        versionDocument.advanceToNextLeave();
    }


    private boolean hasSupportedSerializationType(JSONObject... documents) {
        final boolean[] result = {true};

        Arrays.stream(documents)
            .forEach(document -> {
                String type = getType(document);
                result[0] &= !isMissing(type) && getLocalName(type).equals(ELEM_AKOMANTOSO);
            });

        return result[0];
    }

}
