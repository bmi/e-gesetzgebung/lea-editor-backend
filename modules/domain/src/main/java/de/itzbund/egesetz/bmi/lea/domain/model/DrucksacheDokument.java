// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DruckDokumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DrucksacheDokumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@AllArgsConstructor
@Builder
@Data
public class DrucksacheDokument {

    private DrucksacheDokumentId drucksacheDokumentId;

    private CompoundDocumentId compoundDocumentId;

    private DruckDokumentId druckDokumentId;

    private User createdBy;

    private User updatedBy;

    private Instant createdAt;

    private Instant updatedAt;

}
