// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.eid;

import de.itzbund.egesetz.bmi.lea.domain.ports.eid.Bezeichner;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Set;

@Getter
@Log4j2
public final class BezeichnerImpl implements Bezeichner {

    public static final BezeichnerImpl UNGUELTIGER_BEZEICHNER = new BezeichnerImpl("_ungültig_", "1");
    private static final String DLM = "-";
    private static final Validator VALIDATOR = Validation.buildDefaultValidatorFactory()
        .getValidator();

    @NonNull
    @NotBlank
    private final String elementTypKuerzel;

    @NonNull
    @NotBlank
    @Pattern(regexp = "([1-9]\\d*([a-z])?)|([a-z]{1,3})([.]\\d)?")
    private final String position;


    private BezeichnerImpl(@NonNull String elementTypKuerzel, @NonNull String position) {
        this.elementTypKuerzel = elementTypKuerzel;
        this.position = position;
    }


    public static BezeichnerImpl newInstance(String elementTypKuerzel, int positionAsInt) {
        return newInstance(elementTypKuerzel, String.valueOf(positionAsInt));
    }


    public static BezeichnerImpl newInstance(String elementTypKuerzel, String position) {
        BezeichnerImpl b = new BezeichnerImpl(elementTypKuerzel, position);
        Set<ConstraintViolation<BezeichnerImpl>> violations = VALIDATOR.validate(b);

        if (violations.isEmpty()) {
            if (Abkuerzungsliteral.isValid(elementTypKuerzel)) {
                return b;
            } else {
                log.error("{} is not a valid eId literal", elementTypKuerzel);
                return UNGUELTIGER_BEZEICHNER;
            }
        } else {
            violations.forEach(v -> log.error(v.getMessage()));
            return UNGUELTIGER_BEZEICHNER;
        }
    }


    public static Bezeichner fromString(
        @NonNull @Pattern(regexp = "([a-zäöü]+)-(([1-9]\\d*([a-z])?)|([a-z]{1,3})([.]\\d)?)")
        String bezString
    ) {
        String[] parts = bezString.split(DLM);
        return BezeichnerImpl.newInstance(parts[0], parts[1]);
    }


    public boolean isValid() {
        return !this.equals(UNGUELTIGER_BEZEICHNER);
    }


    @Override
    public String toString() {
        return isValid() ? String.format("%s%s%s", elementTypKuerzel, DLM, position) : "";
    }
}
