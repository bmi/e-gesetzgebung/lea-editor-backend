// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.validator;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorShortDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.services.RegelungsvorhabenService;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.stream.Collectors;

@Component
@Log4j2
@Deprecated(since = "5.9.13")
public class DocumentPermissionValidator {

    @Autowired
    private LeageSession session;
    @Autowired
    private RegelungsvorhabenService plategRequestService;


    public boolean isUserCollaboratorOfDocument(Document document) {
        return plategRequestService.isUserParticipantForCompoundDocument(
            document.getDocumentEntity().getCompoundDocumentId()
        );
    }

    public boolean isUserCollaborating(DocumentDomain documentEntity) {
        if (documentEntity != null) {
            return plategRequestService.isUserParticipantForCompoundDocument(
                documentEntity.getCompoundDocumentId()
            );

        }
        return false;
    }

    // +-------------
    // |
    // v

    public boolean isUserOwnerOfDocument(User user, Document document) {
        DocumentDomain documentEntity = document.getDocumentEntity();
        return compareUsers(user, documentEntity.getCreatedBy());
    }


    public boolean isUserOwnerOfDocumentEntity(DocumentDomain documentEntity) {
        User user = session.getUser();
        return compareUsers(user, documentEntity.getCreatedBy());
    }


    public boolean isUserOwnerOfCompoundDocumentEntity(CompoundDocumentDomain compoundDocumentEntity) {
        User user = session.getUser();
        return compareUsers(user, compoundDocumentEntity.getCreatedBy());
    }


    public boolean isUserOwnerOfCommentEntity(Comment commentEntity) {
        User user = session.getUser();
        return compareUsers(user, commentEntity.getCreatedBy());
    }


    public boolean isUserOwnerOfReplyEntity(Reply replyEntity) {
        User user = session.getUser();
        return compareUsers(user, replyEntity.getCreatedBy());
    }

    private boolean compareUsers(User user1, User user2) {
        return user1.getGid().equals(user2.getGid());
    }

    // ^
    // |
    // +-------------

    public boolean isUserOwnerOfRegulatoryProposal(UUID regulatoryProposalId) {
        return plategRequestService.getAllRegulatoryProposalsForUserHeIsAuthorOf().stream()
            .map(RegelungsvorhabenEditorShortDTO::getId)
            .collect(Collectors.toList()).contains(regulatoryProposalId);
    }

}
