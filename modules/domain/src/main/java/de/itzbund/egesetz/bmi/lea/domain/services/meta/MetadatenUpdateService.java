// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.meta;

import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentBearbeitendeInstitution;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentForm;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentInitiant;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.ports.meta.MetadatenUpdatePort;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import static de.itzbund.egesetz.bmi.lea.core.Constants.META_CONTAINER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.META_LDMLDE_CONTAINER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.META_LDMLDE_FORM;
import static de.itzbund.egesetz.bmi.lea.core.Constants.META_LDMLDE_INITIANT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.META_LDMLDE_INSTITUTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.META_LDMLDE_TYP;
import static de.itzbund.egesetz.bmi.lea.core.Constants.META_PROPRIETARY_CONTAINER;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDescendant;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.updateTextContent;

@Service
public class MetadatenUpdateService implements MetadatenUpdatePort {

    @Override
    public JSONObject update(JSONObject dokument, Proposition proposition) {
        JSONObject initiant = getMetaDataField(dokument, META_LDMLDE_INITIANT);
        String updatedText = proposition.getInitiant().getLiteral();
        updateTextContent(initiant, updatedText);
        return dokument;
    }


    @Override
    public JSONObject update(JSONObject dokument, CompoundDocumentTypeVariant compoundDocumentType) {
        RechtsetzungsdokumentTyp typ;
        RechtsetzungsdokumentForm form;
        RechtsetzungsdokumentInitiant initiant;
        RechtsetzungsdokumentBearbeitendeInstitution bearbeitendeInstitution;

        switch (compoundDocumentType) {
            case AENDERUNGSVERORDNUNG:
                typ = RechtsetzungsdokumentTyp.VERORDNUNG;
                form = RechtsetzungsdokumentForm.MANTELFORM;
                initiant = RechtsetzungsdokumentInitiant.BUNDESREGIERUNG;
                bearbeitendeInstitution = RechtsetzungsdokumentBearbeitendeInstitution.BUNDESREGIERUNG;
                break;
            case MANTELGESETZ:
                typ = RechtsetzungsdokumentTyp.GESETZ;
                form = RechtsetzungsdokumentForm.MANTELFORM;
                initiant = RechtsetzungsdokumentInitiant.BUNDESREGIERUNG;
                bearbeitendeInstitution = RechtsetzungsdokumentBearbeitendeInstitution.BUNDESREGIERUNG;
                break;
            case STAMMVERORDNUNG:
                typ = RechtsetzungsdokumentTyp.VERORDNUNG;
                form = RechtsetzungsdokumentForm.STAMMFORM;
                initiant = RechtsetzungsdokumentInitiant.BUNDESREGIERUNG;
                bearbeitendeInstitution = RechtsetzungsdokumentBearbeitendeInstitution.BUNDESREGIERUNG;
                break;
            default:
                typ = RechtsetzungsdokumentTyp.GESETZ;
                form = RechtsetzungsdokumentForm.STAMMFORM;
                initiant = RechtsetzungsdokumentInitiant.BUNDESREGIERUNG;
                bearbeitendeInstitution = RechtsetzungsdokumentBearbeitendeInstitution.BUNDESREGIERUNG;
        }

        updateMetadata(dokument, META_LDMLDE_TYP, typ.getLiteral());
        updateMetadata(dokument, META_LDMLDE_FORM, form.getLiteral());
        updateMetadata(dokument, META_LDMLDE_INITIANT, initiant.getLiteral());
        updateMetadata(dokument, META_LDMLDE_INSTITUTION, bearbeitendeInstitution.getLiteral());

        return dokument;
    }


    public static JSONObject getMetaDataField(JSONObject document, String kind) {
        return getDescendant(document, false, "*", META_CONTAINER, META_PROPRIETARY_CONTAINER,
            META_LDMLDE_CONTAINER, kind);
    }


    private void updateMetadata(JSONObject dokument, String kind, String value) {
        JSONObject metadataField = getMetaDataField(dokument, kind);
        updateTextContent(metadataField, value);
    }

}
