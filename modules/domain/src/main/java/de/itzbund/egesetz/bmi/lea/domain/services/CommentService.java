// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.core.json.JSONUtils;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.EditorRollenUndRechte;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.KommentarAntworten;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CommentPositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CreateCommentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.DeleteCommentsDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.PatchCommentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.entities.Kommentar;
import de.itzbund.egesetz.bmi.lea.domain.enums.CommentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.Roles;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.mappers.CommentMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.Position;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.ReplyId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CommentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.ReplyPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CommentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.domain.validator.DocumentPermissionValidator;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_COMMENT_ID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_TYPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_CHANGE_WRAPPER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_COMMENTS_ON_ELEMENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_COMMENT_COLOR;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_COMMENT_WRAPPER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_TEXT_WRAPPER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RECHT_SCHREIBEN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RESSOURCENTYP_DOKUMENT;

@Service
@Log4j2
@SuppressWarnings("unused")
public class CommentService implements CommentRestPort {

	// ----- Repositories -----
	private final CommentPersistencePort commentPersistencePort;
	private final ReplyPersistencePort replyPersistencePort;

	@Autowired
	private DokumentPersistencePort documentRepository;
	// ----- Services ---------
	@Autowired
	private DocumentService documentService;
	@Autowired
	private PropositionService propositionService;
	// -----Mappers -----------
	@Autowired
	private CommentMapper commentMapper;

	// ----- Other ------------
	@Autowired
	private LeageSession session;
	@Autowired
	private DocumentPermissionValidator documentPermissionValidator;

	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	private EditorRollenUndRechte editorRollenUndRechte;


	@Autowired
	public CommentService(final CommentPersistencePort commentPersistencePort,
		final ReplyPersistencePort replyPersistencePort) {
		this.commentPersistencePort = commentPersistencePort;
		this.replyPersistencePort = replyPersistencePort;
	}

	// ========= Kommentare ==========================


	@Override
	public Comment createCommentOld(DocumentId documentId, CreateCommentDTO commentCreateDTO,
		List<ServiceState> status) {

		Document document = getDocumentLightFromDocumentId(documentId);
		// isDocumentExisting fills document automatically
		if (documentService.isDocumentExisting(document)) {
			return persistCommentOld(documentId, commentCreateDTO, status);
		} else {
			status.add(ServiceState.DOCUMENT_NOT_FOUND);
		}

		return null;
	}

	@Override
	public DocumentDTO createComment(DocumentId documentId, CreateCommentDTO commentCreateDTO,
		List<ServiceState> status) {

		Document document = getDocumentLightFromDocumentId(documentId);
		// isDocumentExisting fills document automatically
		if (documentService.isDocumentExisting(document)) {
			DocumentDomain documentDomain = persistComment(documentId, commentCreateDTO, status);
			DocumentDTO documentDTO = documentService.buildDocumentDTO(documentDomain);
			documentDTO = documentService.enrichDocumentDTOWithPermissions(documentDTO);
			return documentDTO;
		} else {
			status.add(ServiceState.DOCUMENT_NOT_FOUND);
		}

		return null;
	}


	private Document getDocumentLightFromDocumentId(DocumentId documentId) {
		Document document = applicationContext.getBean(Document.class);
		document.setDocumentId(documentId);
		return document;
	}


	@Override
	public List<Comment> loadAllCommentsOfUserOfDocument(DocumentId documentId, List<ServiceState> status) {
		List<Comment> commentEntities = new ArrayList<>();

		Document document = getDocumentLightFromDocumentId(documentId);
		// isDocumentExisting fills document automatically
		if (documentService.isDocumentExisting(document)) {
			List<Roles> rolesOfRequester = getRoles(document);

			if (rolesOfRequester.contains(Roles.TEILNEHMER_HRA)) {
				commentEntities = commentPersistencePort.findByDocumentIdAndCreatedByAndDeleted(documentId,
					getCurrentUser(), false);
				fuegeAntwortenAnDieKommentareAn(commentEntities);
				status.add(ServiceState.OK);
			} else if (rolesOfRequester.contains(Roles.ABSTIMMUNGSLEITER_HAUPTABSTIMMUNG)) {
				commentEntities = commentPersistencePort.findByDocumentIdAndDeleted(documentId, false);
				fuegeAntwortenAnDieKommentareAn(commentEntities);
				status.add(ServiceState.OK);
			} else {
				//Fachliche Definition prüfen
				commentEntities = commentPersistencePort.findByDocumentIdAndDeleted(documentId, false);
				fuegeAntwortenAnDieKommentareAn(commentEntities);
				status.add(ServiceState.OK);
			}
		} else {
			status.add(ServiceState.DOCUMENT_NOT_FOUND);
		}

		if (commentEntities.isEmpty() && (status.get(0) == ServiceState.OK)) {
			status.add(0, ServiceState.THERE_ARE_NO_COMMENTS);
		} else {
			status.add(ServiceState.OK);
		}

		// We always return the (maybe empty) list as we use the status codes
		return commentEntities;
	}


	@Override
	public void updateComment(DocumentId documentId, CommentId commentId, PatchCommentDTO patchCommentDTO,
		List<ServiceState> status) {
		Document document = getDocumentLightFromDocumentId(documentId);
		// isDocumentExisting fills document automatically
		if (documentService.isDocumentExisting(document)) {
			Comment commentEntity = commentPersistencePort.findByCommentId(commentId);
			if (commentEntity == null) {
				status.add(ServiceState.COMMENT_NOT_FOUND);
			} else {
				updateComment(commentEntity, patchCommentDTO);
				status.add(ServiceState.OK);
			}
		} else {
			status.add(ServiceState.DOCUMENT_NOT_FOUND);
		}
	}


	@Override
	public void deleteCommentOld(DocumentId documentId, CommentId commentId, List<ServiceState> status) {
		Document document = getDocumentLightFromDocumentId(documentId);
		// isDocumentExisting fills document automatically
		if (!documentService.isDocumentExisting(document)) {
			status.add(ServiceState.DOCUMENT_NOT_FOUND);
			return;
		}
		Comment commentEntity = commentPersistencePort.findByCommentId(commentId);
		if (commentEntity == null) {
			status.add(ServiceState.COMMENT_NOT_FOUND);
		} else {
			boolean isAllowed = commentEntity.getCreatedBy() != null;
			isAllowed &= documentPermissionValidator.isUserOwnerOfCommentEntity(commentEntity);

			if (isAllowed) {
				commentEntity.setDeleted(true);
				commentPersistencePort.save(commentEntity);
				status.add(ServiceState.OK);
			} else {
				status.add(ServiceState.NO_PERMISSION);
			}
		}
	}

	@Override
	public DocumentDTO deleteComments(DocumentId documentId, DeleteCommentsDTO deleteCommentsDTO, List<ServiceState> status) {
		Document document = getDocumentLightFromDocumentId(documentId);

		// isDocumentExisting fills document automatically
		if (!documentService.isDocumentExisting(document)) {
			status.add(ServiceState.DOCUMENT_NOT_FOUND);
			return null;
		}

		//Mark comments in db
		List<String> commentIds = deleteCommentsDTO.getCommentIds();
		ArrayList<String> deletedIds = new ArrayList<>();
		for (String commentIdString : commentIds) {
			Comment commentEntity = commentPersistencePort.findByCommentId(new CommentId(UUID.fromString(commentIdString)));
			if (commentEntity == null) {
				deletedIds.add(commentIdString);
				continue;
			}
			boolean isAllowed = commentEntity.getCreatedBy() != null;
			isAllowed &= documentPermissionValidator.isUserOwnerOfCommentEntity(commentEntity);
			if (isAllowed) {
				commentEntity.setDeleted(true);
				commentPersistencePort.save(commentEntity);
				deletedIds.add(commentIdString);
			}
		}
		DocumentDTO ret = removeCommentMarkersFromDocument(documentId, deleteCommentsDTO.getContent(), deletedIds);
		status.add(ServiceState.OK);
		return ret;
	}

	private DocumentDTO removeCommentMarkersFromDocument(DocumentId documentId, String deleteCommentsDTOContent, List<String> commentIds) {
		JSONObject documentJson;
		User user = session.getUser();
		DocumentDomain documentEntity = documentService.getDocumentEntityById(documentId);
		if (editorRollenUndRechte.isAllowed(user.getGid().getId(), RECHT_SCHREIBEN, RESSOURCENTYP_DOKUMENT, documentId.getId().toString())) {
			documentJson = JSONUtils.getDocumentObject(Utils.base64Decode(deleteCommentsDTOContent));
		} else {
			documentJson = JSONUtils.getDocumentObject(documentEntity.getContent());
		}
		for (String commentIdString : commentIds) {
			deleteCommentWrappers(documentJson, commentIdString);
			deleteCommentAttributes(documentJson, commentIdString);
		}

		String updatedContent = JSONUtils.wrapUp(documentJson)
			.toJSONString();
		documentEntity.setContent(updatedContent);
		DocumentDomain documentDomain = documentRepository.save(documentEntity);
		DocumentDTO documentDTO = documentService.buildDocumentDTO(documentDomain);
		documentDTO = documentService.enrichDocumentDTOWithPermissions(documentDTO);

		return documentDTO;
	}

	private void deleteCommentAttributes(JSONObject document, String commentId) {
		JSONArray children = JSONUtils.getChildren(document);
		if (children == null) {
			return;
		}
		Iterator i = children.iterator();
		while (i.hasNext()) {
			JSONObject jsonChild = (JSONObject) i.next();
			if (jsonChild.containsKey(JSON_KEY_TYPE) && (JSONUtils.getType(jsonChild).equals(JSON_VAL_TEXT_WRAPPER) || JSONUtils.getType(jsonChild)
				.equals(JSON_VAL_CHANGE_WRAPPER)) &&
				jsonChild.containsKey(JSON_VAL_COMMENTS_ON_ELEMENT)) {
				List<String> commentsOnElement = (List<String>) jsonChild.get(JSON_VAL_COMMENTS_ON_ELEMENT);
				commentsOnElement.remove(commentId);
				if (commentsOnElement.isEmpty()) {
					jsonChild.remove(JSON_VAL_COMMENTS_ON_ELEMENT);
					jsonChild.remove(JSON_VAL_COMMENT_COLOR);
				} else {
					jsonChild.put(JSON_VAL_COMMENTS_ON_ELEMENT, commentsOnElement);
				}
			} else {
				deleteCommentAttributes(jsonChild, commentId);
			}
		}
	}

	public void deleteAllCommentAttributes(JSONObject document) {
		JSONArray children = JSONUtils.getChildren(document);
		if (children == null) {
			return;
		}
		Iterator i = children.iterator();
		while (i.hasNext()) {
			JSONObject jsonChild = (JSONObject) i.next();
			if (jsonChild.containsKey(JSON_KEY_TYPE) && (JSONUtils.getType(jsonChild).equals(JSON_VAL_TEXT_WRAPPER) || JSONUtils.getType(jsonChild)
				.equals(JSON_VAL_CHANGE_WRAPPER))
			) {
				if (jsonChild.containsKey(JSON_VAL_COMMENTS_ON_ELEMENT)) {
					jsonChild.remove(JSON_VAL_COMMENTS_ON_ELEMENT);
				}
				if (jsonChild.containsKey(JSON_VAL_COMMENT_COLOR)) {
					jsonChild.remove(JSON_VAL_COMMENT_COLOR);
				}
			} else {
				deleteAllCommentAttributes(jsonChild);
			}
		}
	}

	private void deleteCommentWrappers(JSONObject document, String commentId) {
		JSONArray children = JSONUtils.getChildren(document);
		if (children == null) {
			return;
		}
		ListIterator i = children.listIterator();
		while (i.hasNext()) {
			JSONObject jsonChild = (JSONObject) i.next();
			if (jsonChild.containsKey(JSON_KEY_TYPE) && JSONUtils.getType(jsonChild).equals(JSON_VAL_COMMENT_WRAPPER)) {
				if (deleteCommentMarkerFromWrapper(jsonChild, commentId)) {
					i.remove();
					mergeTextWrappers(i);
				}
			} else {
				deleteCommentWrappers(jsonChild, commentId);
			}
		}
	}

	public void deleteAllCommentWrappers(JSONObject document) {
		JSONArray children = JSONUtils.getChildren(document);
		if (children == null) {
			return;
		}
		ListIterator i = children.listIterator();
		while (i.hasNext()) {
			JSONObject jsonChild = (JSONObject) i.next();
			if (jsonChild.containsKey(JSON_KEY_TYPE) && JSONUtils.getType(jsonChild).equals(JSON_VAL_COMMENT_WRAPPER)) {
				i.remove();
				mergeTextWrappers(i);

			} else {
				deleteAllCommentWrappers(jsonChild);
			}
		}
	}

	private static void mergeTextWrappers(ListIterator i) {
		if (i.hasPrevious() && i.hasNext()) {
			JSONObject prevChild = (JSONObject) i.previous();
			i.next();
			JSONObject nextChild = (JSONObject) i.next();
			if (!prevChild.containsKey(JSON_KEY_TYPE) || !nextChild.containsKey(JSON_KEY_TYPE)) {
				return;
			}
			if ((JSONUtils.getType(prevChild).equals(JSON_VAL_TEXT_WRAPPER) || JSONUtils.getType(prevChild)
				.equals(JSON_VAL_CHANGE_WRAPPER)) && JSONUtils.getType(nextChild).equals(JSONUtils.getType(prevChild))
			) {
				String newText = JSONUtils.getText(prevChild) + JSONUtils.getText(nextChild);
				JSONUtils.setText(prevChild, newText);
				i.remove();
			}
		}
	}

	private boolean deleteCommentMarkerFromWrapper(JSONObject wrapperObject, String commentId) {
		JSONArray children = JSONUtils.getChildren(wrapperObject);
		if (children == null) {
			return true;
		}
		ListIterator j = children.listIterator();
		while (j.hasNext()) {
			JSONObject jsonChild = (JSONObject) j.next();
			if (jsonChild.containsKey(JSON_KEY_COMMENT_ID) && JSONUtils.getStringAttribute(jsonChild, JSON_KEY_COMMENT_ID).equals(commentId)) {
				j.remove();
			}
		}
		return children.isEmpty();
	}


	@Override
	public void setCommentState(DocumentId documentId, CommentId commentId, CommentState newState,
		List<ServiceState> status) {
		DocumentDomain documentEntity = documentService.getDocumentEntityById(documentId);
		if (documentEntity != null) {
			Comment commentEntity = commentPersistencePort.findByCommentId(commentId);
			if (commentEntity == null) {
				status.add(ServiceState.COMMENT_NOT_FOUND);
			} else {
				boolean isAllowed = documentPermissionValidator.isUserOwnerOfDocumentEntity(documentEntity);

				if (isAllowed) {
					commentEntity.setState(newState);
					commentPersistencePort.save(commentEntity);
					status.add(ServiceState.OK);
				} else {
					status.add(ServiceState.NO_PERMISSION);
				}
			}
		} else {
			status.add(ServiceState.DOCUMENT_NOT_FOUND);
		}
	}

	private Comment persistCommentOld(DocumentId documentId, CreateCommentDTO commentCreateDTO,
		List<ServiceState> status) {
		Comment comment = Comment.builder()
			.documentId(documentId)
			.commentId(new CommentId(UUID.randomUUID()))
			.content(commentCreateDTO.getContent())
			.anchor(commentMapper.map(commentCreateDTO.getAnchor()))
			.focus(commentMapper.map(commentCreateDTO.getFocus()))
			.createdBy(session.getUser())
			.updatedBy(session.getUser())
			.createdAt(Instant.now())
			.updatedAt(Instant.now())
			.build();

		comment = commentPersistencePort.save(comment);
		status.add(ServiceState.OK);

		return comment;
	}


	private DocumentDomain persistComment(DocumentId documentId, CreateCommentDTO commentCreateDTO,
		List<ServiceState> status) {

		JSONObject documentJson;
		User user = session.getUser();
		DocumentDomain documentEntity = documentService.getDocumentEntityById(documentId);
		if (editorRollenUndRechte.isAllowed(user.getGid().getId(), RECHT_SCHREIBEN, RESSOURCENTYP_DOKUMENT, documentId.getId().toString())) {
			documentJson = JSONUtils.getDocumentObject(Utils.base64Decode(commentCreateDTO.getDocumentContent()));
		} else {
			documentJson = JSONUtils.getDocumentObject(documentEntity.getContent());
		}

		insertCommentMarker(commentCreateDTO.getCommentId(), "start", commentCreateDTO.getAnchor(), documentJson, status);
		insertCommentMarker(commentCreateDTO.getCommentId(), "end", commentCreateDTO.getFocus(), documentJson, status);

		String updatedContent = JSONUtils.wrapUp(documentJson)
			.toJSONString();
		documentEntity.setContent(updatedContent);

		Comment comment = Comment.builder()
			.documentId(documentId)
			.commentId(new CommentId(UUID.fromString(commentCreateDTO.getCommentId())))
			.content(commentCreateDTO.getContent())
			.createdBy(session.getUser())
			.updatedBy(session.getUser())
			.createdAt(Instant.now())
			.updatedAt(Instant.now())
			.build();

		if (!status.isEmpty()) {
			return null;
		}

		commentPersistencePort.save(comment);
		DocumentDomain ret = documentRepository.save(documentEntity);
		status.add(ServiceState.OK);

		return ret;
	}

	private static void insertCommentMarker(String commentId, String commentPosition, CommentPositionDTO commentPositionDTO, JSONObject documentJson,
		List<ServiceState> status) {
		JSONObject rootElement = JSONUtils.getElementByGUID(documentJson, commentPositionDTO.getElementGuid());
		long offset = commentPositionDTO.getOffset();
		JSONArray children = JSONUtils.getChildren(rootElement);
		if (children == null || children.isEmpty()) {
			status.add(ServiceState.UNKNOWN_ERROR);
			return;
		}
		int pos = 0;
		for (Object obj : children) {
			JSONObject jsonChild = (JSONObject) obj;
			if (JSONUtils.getType(jsonChild).equals(JSON_VAL_TEXT_WRAPPER) || JSONUtils.getType(jsonChild).equals(JSON_VAL_CHANGE_WRAPPER)) {
				String text = JSONUtils.getText(jsonChild);
				if (offset == 0) {
					//insert comment before and merge if there is an existing wrapper
					insertCommentAtStart(commentId, commentPosition, pos, children);
					return;
				} else if (offset == text.length()) {
					//insert comment after and merge if there is an existing wrapper
					insertCommentAtEnd(commentId, commentPosition, children, pos);
					return;
				} else if (offset < text.length()) {
					//insert comment between and split textwrapper into two elements
					insertCommentBetween(commentId, commentPosition, jsonChild, text, (int) offset, children, pos);
					return;
				} else {
					offset -= text.length();
				}
			}
			++pos;
		}
		status.add(ServiceState.UNKNOWN_ERROR);
	}

	private static void insertCommentBetween(String commentId, String commentPosition, JSONObject jsonChild, String text, int offset, JSONArray children,
		int pos) {
		JSONUtils.setText(jsonChild, text.substring(0, offset));
		JSONObject commentWrapper = JSONUtils.makeNewCommentWrapper(commentId, commentPosition);
		children.add(pos + 1, commentWrapper);
		JSONObject splitPart2 = JSONUtils.cloneJSONObject(jsonChild);
		JSONUtils.setText(splitPart2, text.substring(offset));
		children.add(pos + 2, splitPart2);
		return;
	}

	private static void insertCommentAtEnd(String commentId, String commentPosition, JSONArray children, int pos) {
		if (children.size() > pos + 1 && JSONUtils.getType((JSONObject) children.get(pos + 1)).equals(JSON_VAL_COMMENT_WRAPPER)) {
			JSONObject successor = (JSONObject) children.get(pos + 1);
			JSONArray wrapperChildren = JSONUtils.getChildren(successor);
			wrapperChildren.add(JSONUtils.makeNewCommentMarker(commentId, commentPosition));
		} else {
			children.add(pos + 1, JSONUtils.makeNewCommentWrapper(commentId, commentPosition));
			children.add(pos + 2, JSONUtils.makeNewTextNode(""));
		}
	}

	private static void insertCommentAtStart(String commentId, String commentPosition, int pos, JSONArray children) {
		if (pos > 0 && JSONUtils.getType((JSONObject) children.get(pos - 1)).equals(JSON_VAL_COMMENT_WRAPPER)) {
			JSONObject predecessor = (JSONObject) children.get(pos - 1);
			JSONArray wrapperChildren = JSONUtils.getChildren(predecessor);
			wrapperChildren.add(JSONUtils.makeNewCommentMarker(commentId, commentPosition));
		} else {
			children.add(0, JSONUtils.makeNewCommentWrapper(commentId, commentPosition));
			children.add(0, JSONUtils.makeNewTextNode(""));
		}
	}


	private void updateComment(Comment commentEntity, PatchCommentDTO patchCommentDTO) {
		boolean changed = false;
		changed |= updateContent(commentEntity, patchCommentDTO.getContent());
		changed |= updateAnchor(commentEntity, patchCommentDTO.getAnchor());
		changed |= updateFocus(commentEntity, commentMapper.map(patchCommentDTO.getFocus()));

		if (changed) {
			commentPersistencePort.save(commentEntity);
		}
	}


	private boolean updateContent(Comment commentEntity, String updatedContent) {
		if (updatedContent != null) {
			commentEntity.setContent(updatedContent);
			return true;
		}
		return false;
	}


	private boolean updateAnchor(Comment commentEntity, CommentPositionDTO anchorDto) {
		if (anchorDto != null) {
			commentEntity.setAnchor(commentMapper.map(anchorDto));
			return true;
		}
		return false;
	}


	private boolean updateFocus(Comment commentEntity, Position focusDto) {
		if (focusDto != null && !focusDto.equals(commentEntity.getFocus())) {
			commentEntity.setFocus(focusDto);
			return true;
		}
		return false;
	}


	private void fuegeAntwortenAnDieKommentareAn(List<Comment> commentEntities) {
		if (commentEntities != null) {
			commentEntities.forEach(
				ce -> ce.setReplies(
					replyPersistencePort.findByCommentIdAndDeleted(ce.getCommentId(), false)
						.stream()
						.sorted(
							Comparator.comparing(Reply::getUpdatedAt)
						)
						.collect(Collectors.toList())));
		}
	}

	// ========= Antworten auf Kommentare ============


	/**
	 * Erzeugt eine Antwort auf einen Kommentar.
	 *
	 * @param documentId ID of document
	 * @param commentId  ID of comment
	 * @param replyDTO   data of comment reply
	 * @param status     list of current status
	 */
	@Override
	public void createReply(DocumentId documentId, CommentId commentId, Reply replyDTO,
		List<ServiceState> status) {
		Kommentar kommentar = Kommentar.builder()
			.commentId(commentId)
			.documentId(documentId)
			.build();
		KommentarAntworten kommentarAntworten = applicationContext.getBean(KommentarAntworten.class);

		if (isCommentExisting(commentId)) {
			Kommentar vorgaengerKommentar = null;
			if (!replyDTO.getParentId()
				.equals(commentId.getId()
					.toString())) {
				vorgaengerKommentar = Kommentar.builder()
					.replyId(new ReplyId(UUID.fromString(replyDTO.getParentId())))
					.build();

				if (!isReplyExisting(vorgaengerKommentar.getReplyId())) {
					status.add(ServiceState.REPLY_NOT_FOUND);
					return;
				}
			}

			if (kommentarAntworten.antwortAufKommentarErstellen(kommentar, vorgaengerKommentar, replyDTO)) {
				status.add(ServiceState.OK);
			}

		} else {
			status.add(ServiceState.COMMENT_NOT_FOUND);
		}
	}


	@Override
	public void updateReply(DocumentId documentId, CommentId commentId, ReplyId replyId, Reply veraenderteAntwort,
		List<ServiceState> status) {
		if (isReplyExisting(replyId)) {
			if (isAllowedToReplyToComment(documentId, commentId)) {
				Optional<Reply> antwort = replyPersistencePort.findByReplyIdAndDeletedFalse(replyId);
				if (antwort.isPresent() && documentPermissionValidator.isUserOwnerOfReplyEntity(antwort.get())) {
					updateReply(antwort.get(), veraenderteAntwort, status);
				} else {
					status.add(ServiceState.NO_PERMISSION);
				}
			} else {
				status.add(ServiceState.NO_PERMISSION);
			}
		} else {
			status.add(ServiceState.REPLY_NOT_FOUND);
		}
	}


	private void updateReply(@NonNull Reply antwort, Reply veraenderteAntwort, List<ServiceState> status) {
		KommentarAntworten kommentarAntworten = applicationContext.getBean(KommentarAntworten.class);
		if (kommentarAntworten.kommentarAntwortAendern(antwort, veraenderteAntwort)) {
			status.add(ServiceState.OK);
		} else {
			status.add(ServiceState.COMMENT_REPLY_NOT_PERSISTED);
		}
	}

	@Override
	public void deleteCommentReply(DocumentId documentId, CommentId commentId, ReplyId replyId,
		List<ServiceState> status) {
		if (isReplyExisting(replyId)) {
			if (isAllowedToReplyToComment(documentId, commentId)) {
				Optional<Reply> antwort = replyPersistencePort.findByReplyIdAndDeletedFalse(replyId);
				if (antwort.isPresent() && documentPermissionValidator.isUserOwnerOfReplyEntity(antwort.get())) {
					deleteReply(antwort.get(), status);
				} else {
					status.add(ServiceState.NO_PERMISSION);
				}
			} else {
				status.add(ServiceState.NO_PERMISSION);
			}
		} else {
			status.add(ServiceState.COMMENT_REPLY_NOT_FOUND);
		}
	}


	private void deleteReply(@NonNull Reply antwort, List<ServiceState> status) {
		KommentarAntworten kommentarAntworten = applicationContext.getBean(KommentarAntworten.class);
		if (kommentarAntworten.kommentarAntwortLoeschen(antwort)) {
			status.add(ServiceState.OK);
		} else {
			status.add(ServiceState.COMMENT_REPLY_NOT_PERSISTED);
		}
	}

	// ========= Hilfsmethoden =======================


	private User getCurrentUser() {
		return session.getUser();
	}


	private boolean isCommentExisting(CommentId commentId) {
		return commentPersistencePort.existsCommentByCommentId(commentId);
	}


	private boolean isReplyExisting(ReplyId replyId) {
		return replyPersistencePort.existsReplyByReplyId(replyId);
	}


	public boolean isAllowedToReplyToComment(DocumentId documentId, CommentId commentId) {
		DocumentDomain documentEntity = documentService.getDocumentByIdNEW(documentId)
			.getDocumentEntity();
		Comment commentEntity = commentPersistencePort.findByCommentId(commentId);
		if (documentEntity == null || documentEntity.getCreatedBy() == null || commentEntity == null
			|| commentEntity.getCreatedBy() == null) {
			return false;
		}

		User userEntity = session.getUser();
		return userEntity.getGid().equals(commentEntity.getCreatedBy().getGid()) || userEntity.getGid().equals(documentEntity.getCreatedBy().getGid());
	}

	private List<Roles> getRoles(Document document) {

		List<Roles> rolesList = new ArrayList<>();
		rolesList.add(Roles.EDITOR);

		if (documentPermissionValidator.isUserOwnerOfDocument(getCurrentUser(), document)) {
			rolesList.add(Roles.ERSTELLER_DOKUMENTENMAPPE);
			rolesList.add(Roles.ABSTIMMUNGSLEITER_HAUPTABSTIMMUNG);
		}

		if (documentPermissionValidator.isUserCollaboratorOfDocument(document)) {
			rolesList.add(Roles.TEILNEHMER_HRA);
			rolesList.add(Roles.TEILNEHMER_UNTERABSTIMMUNG);
		}

		return rolesList;
	}
}
