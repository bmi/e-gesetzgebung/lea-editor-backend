// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model;

import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.VerfahrensType;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@AllArgsConstructor
@Builder
@Data
public class CompoundDocumentDomain {

    private CompoundDocumentId compoundDocumentId;

    @Builder.Default
    private String version = "1";

    private CompoundDocumentId inheritFromId;

    private String title;

    private RegelungsVorhabenId regelungsVorhabenId;

    private CompoundDocumentTypeVariant type;

    @Builder.Default
    private DocumentState state = DocumentState.DRAFT;

    @Builder.Default
    private VerfahrensType verfahrensType = VerfahrensType.REFERENTENENTWURF;

    private User createdBy;

    private User updatedBy;

    private Instant createdAt;

    private Instant updatedAt;

    // Eine Referenz in die Liste der Regelungsvorhaben
    private UUID fixedRegelungsvorhabenReferenzId;

}
