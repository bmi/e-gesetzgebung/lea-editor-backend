// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.comment;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@EqualsAndHashCode
@SuperBuilder
@RequiredArgsConstructor
public class CreateCommentDTO {

	@NotBlank
	private String content;

	private CommentPositionDTO anchor;

	private CommentPositionDTO focus;

	private String commentId;

	private String documentContent;

}
