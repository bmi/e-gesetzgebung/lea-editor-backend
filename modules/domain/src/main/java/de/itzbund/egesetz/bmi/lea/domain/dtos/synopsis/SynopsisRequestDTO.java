// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.synopsis;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.annotation.Generated;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * SynopsisRequestDTO
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen",
    date = "2023-06-05T13:14:01.830653100+02:00[Europe/Berlin]")
public class SynopsisRequestDTO {

    @JsonProperty("base")
    private UUID base;

    @JsonProperty("versions")
    @Valid
    private List<UUID> versions = new ArrayList<>();


    public SynopsisRequestDTO base(UUID base) {
        this.base = base;
        return this;
    }


    /**
     * Get base
     *
     * @return base
     */
    @NotNull
    @Valid
    @Schema(name = "base", required = true)
    public UUID getBase() {
        return base;
    }


    public void setBase(UUID base) {
        this.base = base;
    }


    public SynopsisRequestDTO versions(List<UUID> versions) {
        this.versions = versions;
        return this;
    }


    public SynopsisRequestDTO addVersionsItem(UUID versionsItem) {
        this.versions.add(versionsItem);
        return this;
    }


    /**
     * Get versions
     *
     * @return versions
     */
    @NotNull
    @Valid
    @Schema(name = "versions", required = true)
    public List<UUID> getVersions() {
        return versions;
    }


    public void setVersions(List<UUID> versions) {
        this.versions = versions;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SynopsisRequestDTO synopsisRequestDTO = (SynopsisRequestDTO) o;
        return Objects.equals(this.base, synopsisRequestDTO.base) &&
            Objects.equals(this.versions, synopsisRequestDTO.versions);
    }


    @Override
    public int hashCode() {
        return Objects.hash(base, versions);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class SynopsisRequestDTO {\n");
        sb.append("    base: ")
            .append(toIndentedString(base))
            .append("\n");
        sb.append("    versions: ")
            .append(toIndentedString(versions))
            .append("\n");
        sb.append("}");
        return sb.toString();
    }


    /**
     * Convert the given object to string with each line indented by 4 spaces (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString()
            .replace("\n", "\n    ");
    }
}

