// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.core.json.JSONUtils;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.ports.meta.MetadatenUpdatePort;
import de.itzbund.egesetz.bmi.lea.domain.services.eli.EliMetadatenUtil;
import de.itzbund.egesetz.bmi.lea.domain.services.export.ExportContext;
import de.itzbund.egesetz.bmi.lea.domain.services.filters.EliMetaDataFilter;
import de.itzbund.egesetz.bmi.lea.domain.services.filters.FilterChain;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.SortedMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static de.itzbund.egesetz.bmi.lea.core.Constants.INSTANT_TO_DATE_FORMATTER;

/**
 * This component comprises the logic to build the XML document for a AKN document collection with references to the single documents contained in it.
 */
@Component
@Log4j2
@SuppressWarnings("unused")
public class CollectionDocumentBuilder {

    private static final JSONParser JSON_PARSER = new JSONParser();

    private static final int IDX_VORBLATT = 0;
    private static final int IDX_REGELUNGSTEXT = 1;
    private static final int IDX_BEGRUENDUNG = 2;
    private static final int IDX_ANSCHREIBEN = 3;

    // 'vorblatt', 'regelungstext', 'begruendung' are mandatory; 'anschreiben' is optional but has a fixed index for
    // reference; all other documents, e.g. of type 'freitext', are stored in succession
    private static final int MIN_NUM_DOCUMENTS = 4;

    @Autowired
    private MetadatenUpdatePort metadatenUpdatePort;
    @Autowired
    private ConverterService converterService;
    @Autowired
    private TemplateService templateService;


    public String getCollectionDocumentContent(SortedMap<String, DocumentType> docTypes, Proposition proposition,
        CompoundDocumentTypeVariant type, ExportContext exportContext) {
        List<Pair<String, String>> orderedDocs = getOrderedDocumentData(docTypes);

        // retrieve template
        JSONObject template = getTemplate(type);
        if (template == null) {
            return "";
        }
        amendTemplate(template, proposition, exportContext);

        // include new objects for each document
        JSONObject collectionBody = JSONUtils.getDescendant(template, false,
            "akn:documentCollection", "akn:collectionBody");
        AtomicInteger count = new AtomicInteger();

        if (collectionBody != null) {
            // Pair<String, String> := (document's filename, showAs attribute value)
            orderedDocs.forEach(p -> {
                if (p == null) {
                    return;
                }
                count.getAndIncrement();
                JSONUtils.addDocumentRef(collectionBody, p.getLeft(), p.getRight(), count.get());
            });
        } else {
            log.error("could not find element '<akn:collectionBody>'");
            return "";
        }

        // update metadata
        if (proposition != null) {
            metadatenUpdatePort.update(template, proposition);
        }

        return converterService.convertJsonToXml(template);
    }


    private void amendTemplate(JSONObject templateObject, Proposition proposition, ExportContext exportContext) {
        String creationdate = null;
        String federfuehrer = null;

        if (exportContext == null) {
            return;
        }

        if (proposition != null) {
            Instant createdAt = proposition.getCreatedAt();
            if (createdAt != null) {
                creationdate = INSTANT_TO_DATE_FORMATTER.format(createdAt);
            }

            federfuehrer = proposition.getProponentDesignationNominative();
        }

        exportContext.getDocumentEntity().setContent(templateObject.toJSONString());
        FilterChain chain = new FilterChain();
        // Generate EliMetaData
        EliMetaDataFilter eliMetaDataFilter = new EliMetaDataFilter(EliMetadatenUtil.collectEliMetaDaten(exportContext));
        eliMetaDataFilter.setDateValue(creationdate);
        eliMetaDataFilter.setFederfuehrer(federfuehrer);
        chain.addFilter(eliMetaDataFilter);

        chain.doFilter(templateObject);
    }


    private JSONObject getTemplate(CompoundDocumentTypeVariant type) {
        DocumentType documentType;
        switch (type) {
            case MANTELGESETZ:
                documentType = DocumentType.RECHTSETZUNGSDOKUMENT_MANTELGESETZ;
                break;
            case AENDERUNGSVERORDNUNG:
                documentType = DocumentType.RECHTSETZUNGSDOKUMENT_MANTELVERORDNUNG;
                break;
            case STAMMVERORDNUNG:
                documentType = DocumentType.RECHTSETZUNGSDOKUMENT_STAMMVERORDNUNG;
                break;
            default:
                documentType = DocumentType.RECHTSETZUNGSDOKUMENT_STAMMGESETZ;
        }

        String template = templateService.loadTemplate(documentType, 0);
        JSONObject jsonObject = null;

        if (!((template == null) || (template.isEmpty()))) {
            try {
                JSONArray jsonArray = (JSONArray) JSON_PARSER.parse(template);
                jsonObject = (JSONObject) jsonArray.get(0);
            } catch (ParseException e) {
                log.error(e);
            }
        }

        return jsonObject;
    }


    private List<Pair<String, String>> getOrderedDocumentData(SortedMap<String, DocumentType> docTypes) {
        int size = docTypes.size() + getCountOfMissingMandatoryDocs(
            docTypes.values().stream().map(DocumentType::getArt).collect(Collectors.toSet()));
        List<Pair<String, String>> docs = getInitializedDocList(size);
        final int[] nextFreeIndex = {IDX_ANSCHREIBEN + 1};

        docTypes.forEach((fn, dt) -> {
            RechtsetzungsdokumentArt art = dt.getArt();
            Pair<String, String> pair = new ImmutablePair<>(fn, dt.getShowAsLiteral());
            switch (art) {
                case VORBLATT:
                    docs.set(IDX_VORBLATT, pair);
                    break;
                case REGELUNGSTEXT:
                    docs.set(IDX_REGELUNGSTEXT, pair);
                    break;
                case BEGRUENDUNG:
                    docs.set(IDX_BEGRUENDUNG, pair);
                    break;
                case ANSCHREIBEN:
                    docs.set(IDX_ANSCHREIBEN, pair);
                    break;
                default:
                    docs.set(nextFreeIndex[0], pair);
                    nextFreeIndex[0]++;
            }
        });

        if (docs.get(IDX_VORBLATT) == null) {
            docs.set(IDX_VORBLATT, new ImmutablePair<>("", DocumentType.VORBLATT_STAMMGESETZ.getShowAsLiteral()));
        }

        if (docs.get(IDX_REGELUNGSTEXT) == null) {
            docs.set(IDX_REGELUNGSTEXT,
                new ImmutablePair<>("", DocumentType.REGELUNGSTEXT_STAMMGESETZ.getShowAsLiteral()));
        }

        if (docs.get(IDX_BEGRUENDUNG) == null) {
            docs.set(IDX_BEGRUENDUNG, new ImmutablePair<>("", DocumentType.BEGRUENDUNG_STAMMGESETZ.getShowAsLiteral()));
        }

        return docs;
    }


    private int getCountOfMissingMandatoryDocs(Collection<RechtsetzungsdokumentArt> types) {
        int count = 0;
        if (!types.contains(RechtsetzungsdokumentArt.VORBLATT)) {
            count++;
        }
        if (!types.contains(RechtsetzungsdokumentArt.REGELUNGSTEXT)) {
            count++;
        }
        if (!types.contains(RechtsetzungsdokumentArt.BEGRUENDUNG)) {
            count++;
        }
        if (!types.contains(RechtsetzungsdokumentArt.ANSCHREIBEN)) {
            count++;
        }
        return count;
    }


    private List<Pair<String, String>> getInitializedDocList(int size) {
        int actualSize = Math.max(MIN_NUM_DOCUMENTS, size);
        List<Pair<String, String>> list = new ArrayList<>(actualSize);
        for (int i = 0; i < actualSize; i++) {
            list.add(null);
        }
        return list;
    }

}
