// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.mappers;

import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.ReplyDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import org.mapstruct.Builder;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true),
    uses = {BaseMapper.class})
public interface ReplyMapper {

    // qualifiedByName defined in BaseMapper
    @Mapping(target = "replyId", source = "replyId", qualifiedByName = "replyIdUuidMapping")
    @Mapping(target = "updatedBy.gid", source = "updatedBy.gid", qualifiedByName = "userIdStringMapping")
    @Mapping(target = "createdBy.gid", source = "createdBy.gid", qualifiedByName = "userIdStringMapping")
    ReplyDTO map(Reply replyEntity);

    @InheritInverseConfiguration
    Reply map(ReplyDTO replyDTO);

}
