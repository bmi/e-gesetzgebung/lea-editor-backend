// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.comment;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.CommentState;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.Singular;
import lombok.experimental.SuperBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.Instant;
import java.util.List;

@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
public class CommentResponseDTO {

    @NotBlank
    private String documentId;

    @NotBlank
    private String commentId;

    @NotBlank
    private String content;

    @NotNull
    private CommentState state;

    @NotNull
    private CommentPositionDTO anchor;

    @NotNull
    private CommentPositionDTO focus;

    @NotNull
    private UserDTO createdBy;

    @NotNull
    private UserDTO updatedBy;

    @NotNull
    @PastOrPresent
    private Instant createdAt;

    @NotNull
    @PastOrPresent
    private Instant updatedAt;

    @Singular
    @JsonProperty("replies")
    @Schema(name = "replies", required = true)
    @Valid
    private List<ReplyDTO> replies;

}
