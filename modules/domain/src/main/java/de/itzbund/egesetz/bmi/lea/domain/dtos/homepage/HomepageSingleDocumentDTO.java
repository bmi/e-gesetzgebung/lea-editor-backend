// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.homepage;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.EntityType;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
@SuppressWarnings("unused")
public class HomepageSingleDocumentDTO {

	private UUID id;
	@EqualsAndHashCode.Exclude
	private UUID regulatoryProposalId;
	@EqualsAndHashCode.Exclude
	private String title;
	@EqualsAndHashCode.Exclude
	private DocumentType type;
	@EqualsAndHashCode.Exclude
	private DocumentState state;
	@EqualsAndHashCode.Exclude
	private Instant createdAt;
	@EqualsAndHashCode.Exclude
	private User createdBy;
	@EqualsAndHashCode.Exclude
	private Instant updatedAt;
	@EqualsAndHashCode.Exclude
	private User updatedBy;
	@EqualsAndHashCode.Exclude
	private PermissionDTO documentPermissions;
	@EqualsAndHashCode.Exclude
	private PermissionDTO commentPermissions;


	@JsonProperty
	public EntityType getEntityType() {
		return EntityType.SINGLE_DOCUMENT;
	}
}
