// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.task;

import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.ValidationResult;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.LeaValidationResult;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationData;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationException;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.xpath.XmlContentLocator;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.Validator;
import java.io.IOException;

import static javax.xml.XMLConstants.FEATURE_SECURE_PROCESSING;

/**
 * This kind of {@link XmlValidationTask} is based on an XML Schema Definition (XSD).
 */

@Log4j2
@Component
@Qualifier("xml")
@Scope("prototype")
public class XmlSchemaValidationTask extends AbstractValidationTask implements SchemaBasedValidationTask {

    private static final String TASK_DESC = "validates an XML instance against a W3C XML Schema";

    private static final String FEATURE_DTD_NOT_ALLOWED = "http://apache.org/xml/features/disallow-doctype-decl";

    private Schema schema;

    private InputSource input;

    private ValidationResult result = LeaValidationResult.VALIDATION_NOT_PERFORMED;

    private XmlContentLocator locator;


    @Override
    public void setSchema(Schema schema) {
        this.schema = schema;
    }


    @Override
    public void setInputSource(ReusableInputSource ris) {
        locator = new XmlContentLocator();
        locator.initialize(ris);
        this.input = new InputSource(ris.getByteStream());
    }


    @Override
    public String getDescription() {
        return TASK_DESC;
    }


    @Override
    public ValidationResult run(ValidationData data)
        throws ValidationException, SAXNotSupportedException, SAXNotRecognizedException,
        ParserConfigurationException {
        result = LeaValidationResult.VALIDATION_NOT_PERFORMED;
        handleError();
        XmlSchemaValidationHandler handler = new XmlSchemaValidationHandler(this, data, locator);

        try {
            Validator validator = this.schema.newValidator();
            validator.setFeature(FEATURE_SECURE_PROCESSING, true);
            validator.setFeature(FEATURE_DTD_NOT_ALLOWED, true);
            validator.setFeature("http://apache.org/xml/features/honour-all-schemaLocations", true);
            validator.setErrorHandler(handler);
            validator.validate(new StreamSource(input.getByteStream()));
        } catch (IOException | SAXException e) {
            result = LeaValidationResult.VALIDATION_ERROR;
            throw new ValidationException("exception occurred during schema validation", e);
        }

        result = data.getErrorCount(this) > 0 ?
            LeaValidationResult.VALIDATION_FAILURE :
            LeaValidationResult.VALIDATION_SUCCESS;
        return result;
    }


    private void handleError() throws ValidationException {
        if (schema == null) {
            result = LeaValidationResult.VALIDATION_ERROR;
            throw new ValidationException("no schema defined");
        }
        if (input == null) {
            result = LeaValidationResult.VALIDATION_ERROR;
            throw new ValidationException("no content to validate defined");
        }
    }


    @Override
    public ValidationResult getResult() {
        return result;
    }

}
