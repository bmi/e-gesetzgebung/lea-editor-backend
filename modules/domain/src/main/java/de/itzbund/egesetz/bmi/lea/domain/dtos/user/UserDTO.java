// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.user;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

/**
 * represents data of user
 */

@Data
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
public class UserDTO {

    @Schema(name = "gid", example = "00000000-0000-1111-0000-000000000000")
    private String gid;

    @Schema(name = "name", example = "The legal man")
    private String name;

    @Schema(name = "email", example = "me@example.com", required = true)
    private String email;

}
