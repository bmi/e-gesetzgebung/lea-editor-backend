// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest;

import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CreateCommentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.DeleteCommentsDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.PatchCommentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.CommentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.ReplyId;

import java.util.List;

public interface CommentRestPort {

	DocumentDTO createComment(DocumentId documentId, CreateCommentDTO decodedCreateCommentDTO, List<ServiceState> status);

	void createReply(DocumentId documentId, CommentId commentId, Reply decodedReplyDTO, List<ServiceState> status);

	List<Comment> loadAllCommentsOfUserOfDocument(DocumentId docId, List<ServiceState> status);

	void updateComment(DocumentId documentId, CommentId commentId, PatchCommentDTO decodedPatchCommentDTO,
		List<ServiceState> status);

	void updateReply(DocumentId documentId, CommentId commentId, ReplyId replyId, Reply decodedReplyDTO,
		List<ServiceState> status);

	void setCommentState(DocumentId documentId, CommentId commentId, CommentState state, List<ServiceState> status);

	void deleteCommentOld(DocumentId documentId, CommentId commentId, List<ServiceState> status);

	DocumentDTO deleteComments(DocumentId documentId, DeleteCommentsDTO deleteCommentsDTO, List<ServiceState> status);

	void deleteCommentReply(DocumentId documentId, CommentId commentId, ReplyId replyId, List<ServiceState> status);

	Comment createCommentOld(DocumentId documentId, CreateCommentDTO decodedCreateCommentDTO, List<ServiceState> status);
}
