// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.task;

/**
 * A special class of {@link ValidationTask}s that is guided by some kind of ruleset, which contains business logic.
 */
public interface RuleBasedValidationTask extends ValidationTask {

    /**
     * Sets the ruleset for this task.
     *
     * @param ruleset an instance of some kind of ruleset
     */
    void setRuleset(Object ruleset);

}
