// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.egfa.datenuebernahme;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.itzbund.egesetz.bmi.lea.core.json.JSONUtils;
import de.itzbund.egesetz.bmi.lea.core.json.TraversableJSONDocument;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.ImportEgfaDataDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.CreateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeContentBlockDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeContentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeContentListDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeContentTableDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeContentTextDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeParentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaId;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaItemType;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaStatus;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaStatusDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.enums.egfa.datenuebernahme.EgfaModuleType;
import de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.BegruendungsteilAbschnittRefersToLiteral;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.UserMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.EgfaData;
import de.itzbund.egesetz.bmi.lea.domain.model.EgfaMetadata;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.EgfaDataId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.EgfaMetadataId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.EgfaDataPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.EgfaMetadataPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.AuxiliaryRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CompoundDocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.EgfaRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.PropositionRestPort;
import de.itzbund.egesetz.bmi.lea.domain.services.RegelungsvorhabenService;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_REFERSTO;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_TYPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.EGFA_ATTR_EDITABLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.EGFA_ATTR_EGFA_IMPORT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.EGFA_ATTR_FROM_EGFA;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_HEADING;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TBLOCK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_AKN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_TYPE_CONTENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.LEA_PREFIX;
import static de.itzbund.egesetz.bmi.lea.core.Constants.P_CONTAINER;

@Service
@Log4j2
@SuppressWarnings("unused")
public class EgfaService implements EgfaRestPort {

	@Autowired
	private UserMapper userMapper;

	private final LeageSession session;
	private final PropositionRestPort propositionRestPort;
	private final CompoundDocumentPersistencePort compoundDocumentRepository;
	private final DokumentPersistencePort documentRepository;
	private final EgfaDataPersistencePort egfaDataRepository;
	private final EgfaMetadataPersistencePort egfaMetadataRepository;
	private final AuxiliaryRestPort auxiliaryService;
	private final CompoundDocumentRestPort compoundDocumentService;

	@Autowired
	private RegelungsvorhabenService regelungsvorhabenService;

	@Autowired
	private PropositionMapper propositionMapper;


	public EgfaService(LeageSession session,
		PropositionRestPort propositionRestPort,
		CompoundDocumentPersistencePort compoundDocumentRepository,
		DokumentPersistencePort documentRepository, AuxiliaryRestPort auxiliaryService,
		CompoundDocumentRestPort compoundDocumentService, EgfaDataPersistencePort egfaDataRepository, EgfaMetadataPersistencePort egfaMetadataRepository
	) {
		this.session = session;
		this.propositionRestPort = propositionRestPort;
		this.compoundDocumentRepository = compoundDocumentRepository;
		this.documentRepository = documentRepository;
		this.auxiliaryService = auxiliaryService;
		this.compoundDocumentService = compoundDocumentService;
		this.egfaDataRepository = egfaDataRepository;
		this.egfaMetadataRepository = egfaMetadataRepository;
	}


	@SuppressWarnings("java:S1168")
	private static JSONObject getParentToInsert(TraversableJSONDocument document, EgfaId egfaId,
		EgfaMapping egfaMapping, List<ServiceState> status, String refersToAttr) {
		JSONObject parent = document.getObjectWithRefersTo(refersToAttr);

		parent = createParentIfNull(document, egfaId, egfaMapping, status, refersToAttr, parent);

		if (parent == null) {
			return null;
		}

		if (egfaId.getRegardingDocType()
			.equals(RechtsetzungsdokumentArt.VORBLATT) || egfaId.getParentRef()
			.equals(
				BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_BEFRISTUNG_EVALUIERUNG)) {
			JSONArray children = JSONUtils.getChildren(parent);
			for (Object e : children) {
				JSONObject jsonChild = (JSONObject) e;
				if (jsonChild.get(ATTR_TYPE)
					.equals(JSON_TYPE_CONTENT)) {
					parent = jsonChild;
				}
			}
		}
		return parent;
	}


	@SuppressWarnings({"java:S1168", "unchecked"})
	private static JSONObject createParentIfNull(TraversableJSONDocument document, EgfaId egfaId,
		EgfaMapping egfaMapping, List<ServiceState> status, String refersToAttr, JSONObject parent) {
		if (parent == null) {
			JSONObject grandParent = document.getObjectWithRefersTo(egfaId.getGrandParentRef()
				.getLiteral());
			if (grandParent == null) {
				log.error("eGFA: Element where to include content not found: {}", refersToAttr);
				status.add(ServiceState.INVALID_STATE);
				return null;
			} else {
				parent = JSONUtils.makeTBlock(
					egfaMapping.getParentTitle(egfaId),
					List.of(),
					Pair.of(ATTR_REFERSTO, refersToAttr));
				document.insertNewChild(grandParent, parent);
			}
		}
		return parent;
	}

	@Override
	public List<EgfaStatusDTO> getEgfaStatusByCompoundDocumentId(List<ServiceState> status, UUID compoundDocumentId) {
		CompoundDocumentId dokumentenMappenId = new CompoundDocumentId(compoundDocumentId);
		Optional<CompoundDocumentDomain> compoundDocument = compoundDocumentRepository.findByCompoundDocumentId(dokumentenMappenId);
		if (compoundDocument.isEmpty()) {
			status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
			return Collections.emptyList();
		}
		RegelungsVorhabenId regelungsVorhabenId = compoundDocument.get().getRegelungsVorhabenId();

		ArrayList<EgfaStatusDTO> returnEgfaStatusList = new ArrayList<>();
		for (EgfaModuleType moduleType : EgfaModuleType.values()) {
			EgfaStatusDTO statusDTO = getEgfaStatusDto(dokumentenMappenId, regelungsVorhabenId, moduleType);
			returnEgfaStatusList.add(statusDTO);
		}
		status.add(ServiceState.OK);
		return returnEgfaStatusList;
	}

	private EgfaStatusDTO getEgfaStatusDto(CompoundDocumentId compoundDocumentId, RegelungsVorhabenId regelungsVorhabenId, EgfaModuleType moduleType) {
		/*
		EgfaStatusDTO statusDTO = new EgfaStatusDTO();
		statusDTO.setEgfaModuleId(moduleType);
		*/
		EgfaData foundEgfaData = egfaDataRepository.findNewestByRegelungsVorhabenIdAndModuleName(regelungsVorhabenId, moduleType);
		if (foundEgfaData == null) {
			EgfaStatusDTO statusDTO = new EgfaStatusDTO();
			statusDTO.setEgfaModuleId(moduleType);
			statusDTO.setEgfaDataStatus(EgfaStatus.MODUL_NO_DATA);
			return statusDTO;
		}
		return egfaMetadataRepository
			.findNewestByCompoundDocumentIdAndModuleName(compoundDocumentId, moduleType)
			.map(egfaMetadata -> {
				EgfaStatusDTO statusDTO = new EgfaStatusDTO();
				statusDTO.setEgfaModuleId(moduleType);
				statusDTO.setEgfaDmUpdateDate(egfaMetadata.getCreatedAt());
				statusDTO.setEgfaDmUpdateUser(egfaMetadata.getCreatedByUserId().getName());
				if (egfaMetadata.getModuleCompletedAt().compareTo(foundEgfaData.getCompletedAt()) == 0) {
					statusDTO.setEgfaDataStatus(EgfaStatus.MODUL_NO_NEWER_DATA);
				} else if (foundEgfaData.getCompletedAt().compareTo(egfaMetadata.getModuleCompletedAt()) > 0) {
					statusDTO.setEgfaDataStatus(EgfaStatus.MODUL_DATA_NEW_UPDATE);
				} else {
					statusDTO.setEgfaDataStatus(EgfaStatus.MODUL_DATA_ERROR);
				}
				return statusDTO;
			})
			.orElseGet(() -> {
				EgfaStatusDTO statusDTO = new EgfaStatusDTO();
				statusDTO.setEgfaModuleId(moduleType);
				statusDTO.setEgfaDataStatus(EgfaStatus.MODUL_FINISHED_NOT_USED);
				return statusDTO;
			});
	}

	@Override
	public HashMap<String, HttpStatus> egfaUebernahmeInDokumentenmappe(UUID compoundDocumentId,
		ImportEgfaDataDTO importEgfaDataDTO, List<ServiceState> status) {
		User user = session.getUser();
		Optional<CompoundDocumentDomain> compoundDocument = compoundDocumentRepository.findByCompoundDocumentId(new CompoundDocumentId(compoundDocumentId));
		if (compoundDocument.isEmpty()) {
			status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
			return new HashMap<>();
		}
		RegelungsVorhabenId regelungsVorhabenId = compoundDocument.get().getRegelungsVorhabenId();
		List<String> modules = importEgfaDataDTO.getModuleNames();

		// Vorblatt and Begruendung
		List<DocumentDomain> vorblattList = new ArrayList<>();
		List<DocumentDomain> begruendungList = new ArrayList<>();

		//Retrieve Vorblatt/Begruendung or add if one has not yet been created

		List<ServiceState> retrievalStatus = new ArrayList<>();
		retrieveVorblattAndBegruendung(regelungsVorhabenId.getId(), retrievalStatus, compoundDocument.get(), vorblattList,
			begruendungList);
		if (!retrievalStatus.isEmpty() && !retrievalStatus.parallelStream().allMatch(x -> x == ServiceState.OK)) {
			status.add(ServiceState.UNKNOWN_ERROR);
			return new HashMap<>();
		}

		if (modules.contains("ALLE")) {
			modules = Arrays.stream(EgfaModuleType.values()).map(Enum::name).collect(Collectors.toList());
		}
		HashMap<String, HttpStatus> moduleResponses = new HashMap<>();
		for (String module : modules) {
			processEgfaDataTransferForModule(module, regelungsVorhabenId, moduleResponses, compoundDocument.get(), vorblattList.get(0),
				begruendungList.get(0));
		}
		status.add(ServiceState.OK);
		return moduleResponses;
	}

	private void processEgfaDataTransferForModule(String module, RegelungsVorhabenId regelungsVorhabenId,
		HashMap<String, HttpStatus> moduleResponses,
		CompoundDocumentDomain compoundDocument, DocumentDomain vorblatt, DocumentDomain begruendung) {
		User user = session.getUser();
		EgfaModuleType egfaModuleType;
		try {
			egfaModuleType = EgfaModuleType.valueOf(module);
			EgfaData egfaData = egfaDataRepository.findNewestByRegelungsVorhabenIdAndModuleName(regelungsVorhabenId, egfaModuleType);
			if (egfaData == null) {
				moduleResponses.put(module, HttpStatus.NOT_FOUND);
				return;
			}
			ObjectMapper mapper = new ObjectMapper();
			EgfaDatenuebernahmeDTO contentDTO = mapper.readValue(egfaData.getContent(), EgfaDatenuebernahmeDTO.class);
			List<ServiceState> status = new ArrayList<>();
			if (!isRequestValid(user, user.getGid().getId(), regelungsVorhabenId.getId(), contentDTO, status)) {
				moduleResponses.put(module, HttpStatus.UNAUTHORIZED);
				return;
			}
			List<DocumentId> documentIds = insertEgfaDataInCompoundDocument(contentDTO, status, vorblatt, begruendung, module);
			if (!status.isEmpty()) {
				moduleResponses.put(module, HttpStatus.INTERNAL_SERVER_ERROR);
				return;
			}
			for (DocumentId documentId : documentIds) {
				saveEgfaMetadata(documentId, regelungsVorhabenId, egfaModuleType, egfaData, compoundDocument.getCompoundDocumentId());
			}
			moduleResponses.put(module, HttpStatus.OK);
		} catch (IllegalArgumentException e) {
			log.error("Error while parsing module name: '{}' .", e.getMessage());
			moduleResponses.put(module, HttpStatus.BAD_REQUEST);
		} catch (JsonProcessingException e) {
			log.error("Error: egfa data can not pe processed: '{}' .", e.getMessage());
			moduleResponses.put(module, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private void saveEgfaMetadata(DocumentId documentId, RegelungsVorhabenId regelungsVorhabenId, EgfaModuleType egfaModuleType,
		EgfaData egfaData, CompoundDocumentId compoundDocumentId) {
		EgfaMetadata egfaMetadata = new EgfaMetadata();
		egfaMetadata.setEgfaMetadataId(new EgfaMetadataId(UUID.randomUUID()));
		egfaMetadata.setDocumentId(documentId);
		egfaMetadata.setRegelungsVorhabenId(regelungsVorhabenId);
		egfaMetadata.setModuleName(egfaModuleType);
		egfaMetadata.setModuleCompletedAt(egfaData.getCompletedAt());
		egfaMetadata.setCreatedAt(Instant.now());
		egfaMetadata.setCreatedByUserId(egfaData.getTransmittedByUser());
		egfaMetadata.setEgfaDataId(egfaData.getEgfaDataId());
		egfaMetadata.setCompoundDocumentId(compoundDocumentId);
		egfaMetadataRepository.save(egfaMetadata);
	}

	@Override
	public void egfaDatenSpeicherung(String gid, UUID regelungsvorhabenId,
		EgfaDatenuebernahmeParentDTO egfaDatenuebernahmeParentDTO, List<ServiceState> status) {
		User user = session.getUser();

		if (egfaDatenuebernahmeParentDTO.getModuleName() == null
			|| egfaDatenuebernahmeParentDTO.getModuleName().isEmpty()) {
			status.add(ServiceState.INVALID_ARGUMENTS);
			return;
		}

		ObjectMapper mapper = new ObjectMapper();
		String newContent;
		EgfaModuleType egfaModuleName;
		try {
			newContent = mapper.writeValueAsString(egfaDatenuebernahmeParentDTO.getEgfaContent());
			egfaModuleName = EgfaModuleType.valueOf(egfaDatenuebernahmeParentDTO.getModuleName());
		} catch (Exception e) {
			log.error("Error while parsing content: '{}' .", e.toString());
			status.add(ServiceState.INVALID_ARGUMENTS);
			return;
		}

		EgfaData newEgfaData = EgfaData.builder()
			.build();
		newEgfaData.setEgfaDataId(new EgfaDataId(UUID.randomUUID()));
		newEgfaData.setRegelungsVorhabenId(new RegelungsVorhabenId(regelungsvorhabenId));
		newEgfaData.setModuleName(egfaModuleName);
		newEgfaData.setContent(newContent);
		newEgfaData.setTransmittedByUser(user);
		newEgfaData.setCreatedAt(Instant.now());
		newEgfaData.setCompletedAt(egfaDatenuebernahmeParentDTO.getCompletedAt());

		egfaDataRepository.save(newEgfaData, status);
	}


	@Override
	@Deprecated
	public void egfaDatenuebernahme(String gid, UUID regelungsvorhabenId,
		EgfaDatenuebernahmeDTO egfaDatenuebernahmeDTO, List<ServiceState> status) {
		User user = session.getUser();

		if (!isRequestValid(user, gid, regelungsvorhabenId, egfaDatenuebernahmeDTO, status)) {
			return;
		}

		//check if module is implemented
		if (!isValidEgfaModule(egfaDatenuebernahmeDTO)) {
			status.add(ServiceState.NOT_IMPLEMENTED);
			return;
		}

		// Find compound document to be changed
		Optional<CompoundDocumentDomain> compoundDocument =
			getCompoundDocument(user, new RegelungsVorhabenId(regelungsvorhabenId));

		if (compoundDocument.isEmpty()) {
			status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
			return;
		}

		// Vorblatt and Begruendung
		List<DocumentDomain> vorblattList = new ArrayList<>();
		List<DocumentDomain> begruendungList = new ArrayList<>();

		//Retrieve Vorblatt/Begruendung or add if one has not yet been created
		retrieveVorblattAndBegruendung(regelungsvorhabenId, status, compoundDocument.get(), vorblattList,
			begruendungList);

		insertEgfaDataInCompoundDocument(egfaDatenuebernahmeDTO, status, vorblattList.get(0), begruendungList.get(0), "UNKNOWN");

		if (status.isEmpty()) {
			status.add(ServiceState.OK);
		}
	}

	private List<DocumentId> insertEgfaDataInCompoundDocument(EgfaDatenuebernahmeDTO egfaDatenuebernahmeDTO,
		List<ServiceState> status, DocumentDomain vorblatt, DocumentDomain begruendung, String module) {

		// start processing
		dispatchDocumentProcessing(egfaDatenuebernahmeDTO, vorblatt, begruendung, status, module);

		List<DocumentId> documentIdList = new ArrayList<>();

		documentIdList.add(vorblatt.getDocumentId());
		documentIdList.add(begruendung.getDocumentId());

		return documentIdList;
	}


	private void retrieveVorblattAndBegruendung(UUID regelungsvorhabenId, List<ServiceState> status,
		CompoundDocumentDomain compoundDocument, List<DocumentDomain> vorblattList,
		List<DocumentDomain> begruendungList) {
		User user = session.getUser();

		getDocuments(user, compoundDocument, vorblattList, begruendungList);

		retrieveOrCreateClearedDocument(regelungsvorhabenId, status, compoundDocument, vorblattList, user, RechtsetzungsdokumentArt.VORBLATT);
		retrieveOrCreateClearedDocument(regelungsvorhabenId, status, compoundDocument, begruendungList, user, RechtsetzungsdokumentArt.BEGRUENDUNG);
	}

	private void retrieveOrCreateClearedDocument(UUID regelungsvorhabenId, List<ServiceState> status, CompoundDocumentDomain compoundDocument,
		List<DocumentDomain> documentList,
		User user, RechtsetzungsdokumentArt rechtsetzungsdokumentArt) {
		if (documentList.isEmpty()) {
			Optional<DocumentDomain> newDocument =
				createNewEmptyDocument(regelungsvorhabenId, status, compoundDocument,
					rechtsetzungsdokumentArt, user);
			if (newDocument.isPresent()) {
				documentList.add(newDocument.get());
			} else {
				status.add(ServiceState.UNKNOWN_ERROR);
			}
		} else {
			Optional<TraversableJSONDocument> document = getJSONDocument(documentList.get(0));
			if (document.isEmpty()) {
				status.add(ServiceState.BEGRUENDUNG_NOT_FOUND);
				return;
			}
			documentList.get(0).setContent(JSONUtils.wrapUp(document.get().getDocumentObject())
				.toJSONString());
		}
	}


	private Optional<DocumentDomain> createNewEmptyDocument(UUID regelungsvorhabenId, List<ServiceState> status,
		CompoundDocumentDomain compoundDocument,
		RechtsetzungsdokumentArt documentArt, User user) {
		if (documentArt != RechtsetzungsdokumentArt.VORBLATT
			&& documentArt != RechtsetzungsdokumentArt.BEGRUENDUNG) {
			return Optional.empty();
		}

		CompoundDocumentTypeVariant compoundDocumentTypeVariant = compoundDocument
			.getType();
		String documentTypeString = compoundDocumentTypeVariant.getCompoundDocumentVariant()
			.toLowerCase();
		if (compoundDocumentTypeVariant == CompoundDocumentTypeVariant.AENDERUNGSVERORDNUNG) {
			documentTypeString = "mantelverordnung";
		}
		documentTypeString = documentTypeString + "-" + documentArt.getLiteral();

		CreateDocumentDTO createDocumentDTO =
			CreateDocumentDTO.builder()
				.title(documentArt.getLabel())
				.regelungsvorhabenId(regelungsvorhabenId)
				.type(DocumentType.getFromTemplateID(documentTypeString))
				.initialNumberOfLevels(1)
				.build();

		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO =
			regelungsvorhabenService.getRegelungsvorhabenEditorDTONEW(
				new RegelungsVorhabenId(regelungsvorhabenId), true);

		Document document = auxiliaryService.makeDocumentForUser(compoundDocument, createDocumentDTO, user,
			propositionMapper.map(regelungsvorhabenEditorDTO));
		CompoundDocumentDTO compoundDocumentDTO = compoundDocumentService.addDocumentForUser(
			compoundDocument.getCompoundDocumentId(),
			document, user, status);

		return documentRepository.findByDocumentId(document.getDocumentId());
	}


	private void dispatchDocumentProcessing(EgfaDatenuebernahmeDTO egfaDatenuebernahmeDTO, DocumentDomain vorblatt,
		DocumentDomain begruendung, List<ServiceState> status, String module) {
		EgfaMapping egfaMapping = new EgfaMapping(egfaDatenuebernahmeDTO);

		if (egfaMapping.isConcerningBegruendung() && begruendung == null) {
			status.add(ServiceState.BEGRUENDUNG_NOT_FOUND);
			return;
		}
		if (egfaMapping.isConcerningVorblatt() && vorblatt == null) {
			status.add(ServiceState.VORBLATT_NOT_FOUND);
			return;
		}

		if (egfaMapping.isConcerningBegruendung()) {
			Optional<JSONObject> begruendungContent = updateBegruendung(begruendung, egfaMapping, status, module);
			if (begruendungContent.isPresent()) {
				String updatedContent = JSONUtils.wrapUp(begruendungContent.get())
					.toJSONString();
				begruendung.setContent(updatedContent);
				documentRepository.save(begruendung);
			}
		}

		if (egfaMapping.isConcerningVorblatt()) {
			Optional<JSONObject> vorblattContent = updateVorblatt(vorblatt, egfaMapping, status, module);
			if (vorblattContent.isPresent()) {
				String updatedContent = JSONUtils.wrapUp(vorblattContent.get())
					.toJSONString();
				vorblatt.setContent(updatedContent);
				documentRepository.save(vorblatt);
			}
		}
	}


	@SuppressWarnings("java:S1168")
	private Optional<JSONObject> updateVorblatt(DocumentDomain vorblatt, EgfaMapping egfaMapping,
		List<ServiceState> status, String module) {
		Optional<TraversableJSONDocument> document = getJSONDocument(vorblatt);
		if (document.isEmpty()) {
			status.add(ServiceState.VORBLATT_NOT_FOUND);
			return Optional.empty();
		}
		clearEgfaData(document.get().getDocumentObject(), module);
		EgfaMapping.vorblattSectionsInOrder.forEach(egfaId -> {
			EgfaDatenuebernahmeContentDTO contentDTO = egfaMapping.getContentDTO(egfaId);
			if (contentDTO != null) {
				insertContent(document.get(), egfaId, contentDTO, egfaMapping, status, module, true);
				if (contentDTO.getType()
					.equals(EgfaItemType.TABLE)) {
					EgfaDatenuebernahmeContentTextDTO placeholder =
						new EgfaDatenuebernahmeContentTextDTO("[Platzhalter]"); //NOSONAR
					insertContent(document.get(), egfaId, placeholder, egfaMapping, status, module, false);
				}
			}
		});

		return Optional.of(document.get()
			.getDocumentObject());

	}


	@SuppressWarnings("java:S1168")
	private Optional<JSONObject> updateBegruendung(DocumentDomain begruendung, EgfaMapping egfaMapping,
		List<ServiceState> status, String module) {
		Optional<TraversableJSONDocument> document = getJSONDocument(begruendung);
		if (document.isEmpty()) {
			status.add(ServiceState.BEGRUENDUNG_NOT_FOUND);
			return Optional.empty();
		}
		clearEgfaData(document.get().getDocumentObject(), module);
		EgfaMapping.begruendungSectionsInOrder.forEach(egfaId -> {
			EgfaDatenuebernahmeContentDTO contentDTO = egfaMapping.getContentDTO(egfaId);
			if (contentDTO != null) {
				boolean inserted = insertContent(document.get(), egfaId, contentDTO, egfaMapping, status, module, true);
				if (contentDTO.getType()
					.equals(EgfaItemType.TABLE) && inserted) {
					EgfaDatenuebernahmeContentTextDTO placeholder =
						new EgfaDatenuebernahmeContentTextDTO("[Platzhalter]"); //NOSONAR
					insertContent(document.get(), egfaId, placeholder, egfaMapping, status, module, false);
				}
			}
		});

		return Optional.of(document.get()
			.getDocumentObject());
	}

	private void clearEgfaData(JSONObject document, String module) {
		JSONArray children = JSONUtils.getChildren(document);
		if (children == null) {
			return;
		}
		Iterator i = children.iterator();
		while (i.hasNext()) {
			JSONObject jsonChild = (JSONObject) i.next();
			if (jsonChild.containsKey(LEA_PREFIX + EGFA_ATTR_EGFA_IMPORT + ":" + module)) {
				i.remove();
			} else {
				clearEgfaData(jsonChild, module);
			}
		}
	}


	private boolean insertContent(TraversableJSONDocument document, EgfaId egfaId,
		EgfaDatenuebernahmeContentDTO contentDTO, EgfaMapping egfaMapping, List<ServiceState> status, String module, boolean tagContent) {
		String refersToAttr = egfaId.getParentRef()
			.getLiteral();
		List<JSONObject> contentToInsert = makeNewObjects(contentDTO);

		if (contentToInsert == null) {
			log.error("no content to insert: {}", contentDTO);
			return false;
		}

		JSONObject parent = getParentToInsert(document, egfaId, egfaMapping, status, refersToAttr);

		if (parent == null) {
			return false;
		}

		for (JSONObject child : contentToInsert) {
			if (tagContent) {
				child.put(LEA_PREFIX + EGFA_ATTR_EGFA_IMPORT + ":" + module, true);
				tagEgfaData(child);
			}
			document.insertNewChild(parent, child);
		}

		return true;
	}

	private void tagEgfaData(JSONObject document) {
		if (document.containsKey(ATTR_TYPE)) {
			if ((document.get(ATTR_TYPE).equals(P_CONTAINER)) || document.get(ATTR_TYPE)
				.equals(JSON_KEY_AKN + ":" + ELEM_HEADING)) {
				document.put(LEA_PREFIX + EGFA_ATTR_FROM_EGFA, true);
			}
		}
		JSONArray children = JSONUtils.getChildren(document);
		if (children == null) {
			return;
		}
		Iterator i = children.iterator();
		while (i.hasNext()) {
			JSONObject jsonChild = (JSONObject) i.next();
			tagEgfaData(jsonChild);
		}
	}


	@SuppressWarnings("java:S1168")
	private List<JSONObject> makeNewObjects(EgfaDatenuebernahmeContentDTO contentDTO) {
		switch (contentDTO.getType()) {
			case TABLE:
				return makeNewTableObject((EgfaDatenuebernahmeContentTableDTO) contentDTO);
			case TEXT:
				return makeNewTextObjects((EgfaDatenuebernahmeContentTextDTO) contentDTO);
			case LIST:
				return makeNewTextObjects((EgfaDatenuebernahmeContentListDTO) contentDTO);
			case BLOCK:
				return makeNewBlockObjects((EgfaDatenuebernahmeContentBlockDTO) contentDTO);
			default:
				return null;
		}
	}


	@SuppressWarnings("unchecked")
	private List<JSONObject> makeNewBlockObjects(EgfaDatenuebernahmeContentBlockDTO contentBlockDTO) {
		JSONObject tblock = JSONUtils.makeNewDefaultJSONObject(ELEM_TBLOCK);
		JSONObject headingJson = JSONUtils.getHeading(contentBlockDTO.getLabel());
		headingJson.put(LEA_PREFIX + EGFA_ATTR_EDITABLE, true);
		JSONUtils.addChildren(tblock, headingJson);
		for (EgfaDatenuebernahmeContentDTO contentDTO : contentBlockDTO.getData()) {
			List<JSONObject> toAdd = makeNewObjects(contentDTO);
			if (toAdd != null) {
				for (JSONObject object : toAdd) {
					JSONUtils.addChildren(tblock, object);
				}
			}
		}
		return List.of(tblock);
	}


	private List<JSONObject> makeNewTextObjects(EgfaDatenuebernahmeContentTextDTO contentTextDTO) {
		if (contentTextDTO.getData() == null) {
			contentTextDTO.setData("");
		}

		String[] paras = contentTextDTO.getData().trim().replaceAll(" +", " ").split("\\r?\\n"); //NOSONAR
		return makeTextNodes(contentTextDTO.getLabel(), Arrays.asList(paras));
	}


	private List<JSONObject> makeNewTextObjects(EgfaDatenuebernahmeContentListDTO contentListDTO) {
		List<String> paras = new ArrayList<>();
		contentListDTO.getData()
			.forEach(s -> paras.addAll(Arrays.asList(s.split("\\r?\\n")))); //NOSONAR
		return makeTextNodes(contentListDTO.getLabel(), paras);
	}


	@SuppressWarnings("unchecked")
	private List<JSONObject> makeTextNodes(String heading, List<String> paras) {
		if (Utils.isMissing(heading)) {
			// only text paragraphs
			return JSONUtils.getTextParagraphs(paras);
		} else {
			// include new tblock (with num? and heading)
			return List.of(JSONUtils.makeTBlock(heading, paras));
		}
	}


	@SuppressWarnings("java:S1168")
	private List<JSONObject> makeNewTableObject(EgfaDatenuebernahmeContentTableDTO contentTableDTO) {
		//if the size of the data is 1, the table contains only headings in its row
		if (contentTableDTO.getData()
			.size() <= 1) {
			return null;
		}
		return List.of(
			JSONUtils.makeSimpleTableObject(contentTableDTO.getLabel(), contentTableDTO.getData())
		);
	}


	private void getDocuments(User user, CompoundDocumentDomain compoundDocument, List<DocumentDomain> vorblattList,
		List<DocumentDomain> begruendungList) {
		List<DocumentDomain> documents =
			documentRepository.findByCreatedByAndCompoundDocumentId(user, compoundDocument.getCompoundDocumentId());
		documents.forEach(d -> {
			if (d.getType()
				.getArt() == RechtsetzungsdokumentArt.VORBLATT) {
				vorblattList.add(d);
			} else if (d.getType()
				.getArt() == RechtsetzungsdokumentArt.BEGRUENDUNG) {
				begruendungList.add(d);
			}
		});
	}


	private Optional<CompoundDocumentDomain> getCompoundDocument(User user, RegelungsVorhabenId regelungsVorhabenId) {
		List<CompoundDocumentDomain> compoundDocuments =
			compoundDocumentRepository.findByCreatedByAndRegelungsVorhabenId(user, regelungsVorhabenId);
		compoundDocuments =
			compoundDocuments.stream()
				.filter(cd -> cd.getState() == DocumentState.DRAFT)
				.sorted(Comparator.comparing(CompoundDocumentDomain::getUpdatedAt)
					.reversed())
				.collect(Collectors.toList());
		return !compoundDocuments.isEmpty() ? Optional.ofNullable(compoundDocuments.get(0)) : Optional.empty();

	}


	private boolean isRequestValid(User user, String gid, UUID regelungsvorhabenId,
		EgfaDatenuebernahmeDTO egfaDatenuebernahmeDTO, List<ServiceState> status) {
		if (user == null || !user.getGid()
			.getId()
			.equals(gid)) {
			status.add(ServiceState.NO_PERMISSION);
		}

		List<PropositionDTO> userProposals = propositionRestPort.getAllPropositionDescriptionsByUser();
		if (!userProposals.stream()
			.map(PropositionDTO::getId)
			.collect(Collectors.toList())
			.contains(regelungsvorhabenId)) {
			status.add(ServiceState.NO_PERMISSION);
		}

		if (egfaDatenuebernahmeDTO == null) {
			status.add(ServiceState.INVALID_ARGUMENTS);
		}

		return status.isEmpty();
	}


	/**
	 * Checks whether the egfa module for this data is yet implemented.
	 *
	 * @param egfaDatenuebernahmeDTO the data
	 * @return true, if implemented, otherweise false
	 */
	protected boolean isValidEgfaModule(EgfaDatenuebernahmeDTO egfaDatenuebernahmeDTO) {
		List<EgfaModuleType> implementedModules = Arrays.stream(EgfaModuleType.values())
			.filter(x -> x.getEgfaId() != null)
			.collect(Collectors.toList());
		var moduleDataId = egfaDatenuebernahmeDTO.getId();
		return implementedModules.stream()
			.anyMatch(x -> x.getEgfaId()
				.getLiteral()
				.equals(moduleDataId));
	}


	private Optional<TraversableJSONDocument> getJSONDocument(@NotNull DocumentDomain document) {
		JSONObject jsonObject = JSONUtils.getDocumentObject(document.getContent());
		return jsonObject == null ? Optional.empty() : Optional.of(new TraversableJSONDocument(jsonObject));
	}

}
