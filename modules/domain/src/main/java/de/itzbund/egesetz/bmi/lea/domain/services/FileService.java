// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.core.Constants;
import de.itzbund.egesetz.bmi.lea.domain.dtos.media.MediaSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.MediaData;
import de.itzbund.egesetz.bmi.lea.domain.model.MediaMetadata;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.MediaMetadataPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.MediaPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.FileRestPort;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.sql.rowset.serial.SerialBlob;
import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

@Service
@Log4j2
@SuppressWarnings("unused")
public class FileService implements FileRestPort {

    public static final String FILE_DOES_NOT_EXISTS = "File does not exists";

    private final MediaPersistencePort mediaDataRepo;
    private final MediaMetadataPersistencePort mediaMetadataRepo;


    @Autowired
    public FileService(final MediaPersistencePort mediaDataRepo, final MediaMetadataPersistencePort mediaMetadataRepo) {
        this.mediaDataRepo = mediaDataRepo;
        this.mediaMetadataRepo = mediaMetadataRepo;
    }


    public UUID save(String fileName, MultipartFile file) {
        String fileNameType;
        if (fileName.contains(Constants.JPG)) {
            fileNameType = MediaType.IMAGE_JPEG.toString();
        } else {
            fileNameType = URLConnection.guessContentTypeFromName(fileName);
        }

        String encodedhash;
        try {
            encodedhash = DigestUtils.sha256Hex(file.getBytes());
        } catch (IOException e) {
            log.error("Hashcode could not be created", e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "sha256 could not be generated as id");
        }

        Blob data;
        try {
            data = new SerialBlob(file.getBytes());
        } catch (IOException | SQLException e) {
            log.error("Blob could not be created", e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "file data could not transferred to db");
        }
        MediaData dataEntity = MediaData.builder().id(encodedhash).data(data).build();
        MediaMetadata metaEntity = MediaMetadata.builder()
            .name(fileName).mediaType(fileNameType).mediaData(dataEntity).build();
        mediaDataRepo.saveAndFlush(dataEntity);
        mediaMetadataRepo.saveAndFlush(metaEntity);
        return metaEntity.getId();
    }


    public MediaSummaryDTO loadFromDB(UUID id) {
        Optional<MediaMetadata> metadataEntityOptional = mediaMetadataRepo.findById(id);
        if (metadataEntityOptional.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, FILE_DOES_NOT_EXISTS);
        }
        MediaMetadata metadataEntity = metadataEntityOptional.get();

        Optional<MediaData> dataEntityFromDB = mediaDataRepo.findById(metadataEntity.getMediaData().getId());
        if (dataEntityFromDB.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, FILE_DOES_NOT_EXISTS);
        }
        MediaData dataEntity = dataEntityFromDB.get();

        File file = createFile(dataEntity.getData(), metadataEntity.getName());

        return createDTO(file, metadataEntity.getId());
    }


    public void deleteFromDB(UUID id) {
        Optional<MediaMetadata> metadataEntityOptional = mediaMetadataRepo.findById(id);
        if (metadataEntityOptional.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, FILE_DOES_NOT_EXISTS);
        }
        mediaMetadataRepo.deleteById(id);
        MediaMetadata metadataEntity = metadataEntityOptional.get();

        if (!mediaMetadataRepo.checkForeignKeyHasRelation(metadataEntity.getMediaData())) {
            mediaDataRepo.deleteById(metadataEntity.getMediaData().getId());
        }
    }


    /**
     * Creates a {@link File} of data from repository encapsulated as a {@link Blob} with given Name
     *
     * @param blob     Data of file from repository
     * @param fileName Name given to the file
     * @return Data encapsulated as a File
     * @throws ResponseStatusException with {@link HttpStatus} if data as byte could not be written to file or blob can not be freed
     */
    public File createFile(Blob blob, String fileName) {
        File file = new File(fileName);

        try {
            FileUtils.writeByteArrayToFile(file, blob.getBytes(1, (int) blob.length()));
        } catch (IOException | SQLException e) {
            log.error("ByteArray could not be written to file", e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "could not write to file");
        }
        try {
            blob.free();
        } catch (SQLException e) {
            log.error("Blob could not be freed", e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "blob could not be freed");
        }

        return file;
    }


    @SneakyThrows
    private MediaSummaryDTO createDTO(File file, UUID id) {
        MediaType mediaType = MediaType.valueOf(URLConnection.guessContentTypeFromName(file.getName()));
        if (FilenameUtils.getName(file.getName()).contains(Constants.JPG)) {
            mediaType = MediaType.IMAGE_JPEG;
        }
        ByteArrayResource byteArrayResource = new ByteArrayResource(FileUtils.readFileToByteArray(file));
        MediaSummaryDTO mediaSummaryDTO = MediaSummaryDTO.builder()
            .id(id)
            .mediaType(mediaType)
            .name(file.getName())
            .byteArrayResource(byteArrayResource)
            .length(file.length())
            .build();
        Files.delete(Path.of(file.getPath()));
        return mediaSummaryDTO;
    }

}
