// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * indicates different types of Initiant
 */
@Schema(enumAsRef = true)
@AllArgsConstructor
@Getter
public enum RechtsetzungsdokumentInitiant {

    // Bundesregierung (Exekutive): Die Bundesregierung erarbeitet Gesetzesentwürfe und bringt sie in den Bundestag ein.
    // Die Bundesministerien sind für die Ausarbeitung von Gesetzesvorlagen in ihren jeweiligen Zuständigkeitsbereichen verantwortlich.
    BUNDESREGIERUNG("Bundesregierung", "bundesregierung"),

    // Bundestag (Legislative): Der Bundestag ist das maßgebliche Gesetzgebungsorgan in Deutschland.
    // Er debattiert und verabschiedet Gesetze, entscheidet über Haushaltsfragen und kontrolliert die Arbeit der Bundesregierung.
    BUNDESTAG("Bundestag", "bundestag"),

    // Bundesrat (Legislative): Der Bundesrat ist an der Gesetzgebung beteiligt, indem er Gesetzesvorlagen des Bundestages prüft und dazu Stellung nimmt.
    // In vielen Fällen ist seine Zustimmung erforderlich, damit ein Gesetz verabschiedet werden kann, insbesondere wenn es die Interessen der Länder betrifft.
    BUNDESRAT("Bundesrat", "bundesrat"),

    // Bundespräsident (Exekutive): Der Bundespräsident hat eine Rolle im Gesetzgebungsprozess, indem er Gesetze nach ihrer Verabschiedung durch den Bundestag
    // und den Bundesrat unterzeichnet und somit in Kraft setzt. Er kann jedoch Gesetze auch vor ihrer Unterzeichnung auf ihre Verfassungsmäßigkeit hin
    // überprüfen lassen.
    BUNDESPRAESIDENT("Bundespräsident", "bundespräsident"),

    BUNDESKANZLER("Bundeskanzler", "bundeskanzler"),
    
    NICHT_VORHANDEN("Nicht vorhanden", "nicht-vorhanden");

    private static final Map<String, RechtsetzungsdokumentInitiant> INITIANT_LITERALS = new HashMap<>();

    static {
        Arrays.stream(values()).forEach(form -> INITIANT_LITERALS.put(form.getLiteral(), form));
    }

    private final String label;
    private final String literal;

    public static RechtsetzungsdokumentInitiant fromLiteral(String literal) {
        if (Utils.isMissing(literal)) {
            return null;
        }

        return INITIANT_LITERALS.get(literal);
    }

}
