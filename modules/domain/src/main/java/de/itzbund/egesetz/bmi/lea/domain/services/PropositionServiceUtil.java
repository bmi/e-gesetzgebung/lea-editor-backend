// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.HomepageCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.HomepageRegulatoryProposalDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.HomepageSingleDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.PermissionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.RegelungsvorhabengTableDTOs;
import de.itzbund.egesetz.bmi.lea.domain.dtos.paging.FilterPropertyType;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RessortEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.RegelungsvorhabenTypType;
import de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentInitiant;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.Drucksache;
import de.itzbund.egesetz.bmi.lea.domain.model.DrucksacheDokument;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import lombok.experimental.UtilityClass;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState.FINAL;
import static de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType.BT_DRUCKSACHE;

@UtilityClass
public class PropositionServiceUtil {

    public RegelungsvorhabenEditorDTO dummyRegelungsvorhabenEditorDTO() {
        return RegelungsvorhabenEditorDTO.builder()
            .id(UUID.fromString("00000000-0000-0000-0000-000000000000")) //NOSONAR
            .bezeichnung("nicht verfügbar")
            .kurzbezeichnung("n.v.")
            .kurzbeschreibung("n.v.")
            .abkuerzung("n.v.")
            .status(VorhabenStatusType.ENTWURF)
            .technischesFfRessort(
                RessortEditorDTO.builder()
                    .id("00000000-0000-0000-0000-000000000000") //NOSONAR
                    .bezeichnungNominativ("n.v.")
                    .bezeichnungGenitiv("n.v.")
                    .aktiv(true)
                    .build()
            )
            .initiant(RechtsetzungsdokumentInitiant.BUNDESTAG)
            .vorhabenart(RegelungsvorhabenTypType.GESETZ)
            .bearbeitetAm(Instant.MIN)
            .build();
    }

    public HomepageRegulatoryProposalDTO createHomepageRegulatoryProposal(
        RegelungsvorhabenEditorDTO aktuellesRegelungsvorhaben, List<HomepageCompoundDocumentDTO> homepageCompoundDocumentDTOs) {
        return HomepageRegulatoryProposalDTO.builder()
            .id(aktuellesRegelungsvorhaben.getId())
            // Der Text, der für das Regelungsvorhaben angezeigt wird
            .abbreviation(aktuellesRegelungsvorhaben.getAbkuerzung())
            // Die Farbe des RV (in Plattform gesetzt)
            .farbe(aktuellesRegelungsvorhaben.getFarbe())
            // Dokumentenmappen
            .children(homepageCompoundDocumentDTOs)
            .status(aktuellesRegelungsvorhaben.getStatus())
            .build();
    }

    public HomepageCompoundDocumentDTO createHomepageCompoundDocument(CompoundDocument compoundDocumentOfPage,
        RegelungsvorhabenEditorDTO aktuellesRegelungsvorhaben, List<HomepageSingleDocumentDTO> homepageSingleDocumentDTOList, PermissionDTO permissionDTO,
        Set<UUID> pinnedDmIds) {
        HomepageCompoundDocumentDTO dto = HomepageCompoundDocumentDTO.builder()
            .id(compoundDocumentOfPage.getCompoundDocumentEntity().getCompoundDocumentId().getId())
            .regulatoryProposalId(aktuellesRegelungsvorhaben.getId())
            .regulatoryProposalState(aktuellesRegelungsvorhaben.getStatus())
            .title(compoundDocumentOfPage.getCompoundDocumentEntity().getTitle())
            .type(compoundDocumentOfPage.getCompoundDocumentEntity().getType())
            .state(compoundDocumentOfPage.getState())
            .isNewest(true)
            .isPinned(pinnedDmIds.contains(compoundDocumentOfPage.getCompoundDocumentId().getId()))
            .version(compoundDocumentOfPage.getCompoundDocumentEntity().getVersion())
            // Die Dokumente in der Dokumentenmappe
            .children(homepageSingleDocumentDTOList)
            .documentPermissions(permissionDTO)
            .build();

        if (compoundDocumentOfPage.getState().equals(DocumentState.ZUGELEITET_PKP)) {
            dto.setRegulatoryProposalState(VorhabenStatusType.BUNDESTAG);
        }

        return dto;
    }

    public HomepageSingleDocumentDTO getDocumentDTO(DocumentState state, Document document,
        UUID regulatoryProposalId, PermissionDTO documentPermissionDTO, PermissionDTO commentPermissionDTO) {
        return getDocumentDTO(state, document.getDocumentEntity(), regulatoryProposalId, documentPermissionDTO, commentPermissionDTO);
    }

    public HomepageSingleDocumentDTO getDocumentDTO(DocumentState state, DocumentDomain documentEntity,
        UUID regulatoryProposalId, PermissionDTO documentPermissionDTO, PermissionDTO commentPermissionDTO) {
        return HomepageSingleDocumentDTO.builder()
            .id(documentEntity.getDocumentId()
                .getId())
            .regulatoryProposalId(regulatoryProposalId)
            .title(documentEntity.getTitle())
            .type(documentEntity.getType())
            .state(state)
            .createdAt(documentEntity.getCreatedAt())
            .createdBy(documentEntity.getCreatedBy())
            .updatedAt(documentEntity.getUpdatedAt())
            .updatedBy(documentEntity.getUpdatedBy())
            .documentPermissions(documentPermissionDTO)
            .commentPermissions(commentPermissionDTO)
            .build();
    }

    public RegelungsvorhabengTableDTOs getRegelungsvorhabenTable(List<HomepageRegulatoryProposalDTO> meineDokumente, Pageable newPageable,
        int anzahlAllerElemente) {
        return RegelungsvorhabengTableDTOs.builder()
            .dtos(new PageImpl<>(meineDokumente, newPageable, anzahlAllerElemente))
            .vorhabenartFilter(Arrays.asList(RegelungsvorhabenTypType.values()))
            .filterNames(List.of(FilterPropertyType.REGELUNGSVORHABEN))
            .allContentEmpty(anzahlAllerElemente == 0)
            .build();
    }

    public HomepageSingleDocumentDTO getDrucksacheDTO(
        DrucksacheService drucksacheService, CompoundDocument compoundDocument, PropositionDTO propositionDTO) {
        CompoundDocumentId compoundDocumentId = compoundDocument.getCompoundDocumentId();
        UUID regelungsVorhabenId = propositionDTO.getId();
        DrucksacheDokument drucksacheDokument = drucksacheService.getDrucksacheDokument(compoundDocumentId);

        if (drucksacheDokument == null) {
            return null;
        } else {
            Drucksache drucksache = drucksacheService.getDrucksache(new RegelungsVorhabenId(regelungsVorhabenId));
            String title = String.format("Drucksache %s", drucksache == null ? "n/a" : drucksache.getDrucksachenNr());
            return HomepageSingleDocumentDTO.builder()
                .id(drucksacheDokument.getDrucksacheDokumentId().getId())
                .regulatoryProposalId(regelungsVorhabenId)
                .title(title)
                .type(BT_DRUCKSACHE)
                .state(FINAL)
                .createdAt(drucksacheDokument.getCreatedAt())
                .createdBy(drucksacheDokument.getCreatedBy())
                .updatedAt(drucksacheDokument.getUpdatedAt())
                .updatedBy(drucksacheDokument.getUpdatedBy())
                .documentPermissions(PermissionDTO.builder()
                    .hasRead(true)
                    .hasWrite(false)
                    .build())
                .commentPermissions(PermissionDTO.builder()
                    .hasRead(true)
                    .hasWrite(false)
                    .build())
                .build();
        }
    }

}
