// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;

/**
 * represents data for Ressort (department) for Editor
 */
@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@ToString
public class RessortEditorDTO {

    @NotNull
    private String id;
    @NotNull
    private String kurzbezeichnung;
    @NotNull
    private String bezeichnungNominativ;
    @NotNull
    private String bezeichnungGenitiv;
    private boolean aktiv;
}
