// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.task;

import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import org.xml.sax.InputSource;

/**
 * A class of {@link ValidationTask}s that validate XML instances.
 */
public interface XmlValidationTask {

    /**
     * Sets the XML input to validate encapsulated in an instance of {@link InputSource}.
     *
     * @param input the XML content to validate
     */
    void setInputSource(ReusableInputSource input);

}
