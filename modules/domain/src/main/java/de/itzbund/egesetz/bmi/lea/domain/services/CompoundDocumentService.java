// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.EditorRollenUndRechte;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentTitleDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CopyCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CreateCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.CreateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.PkpZuleitungDokumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.PermissionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtListDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtShortDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorShortDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserRightsDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.Freigabetyp;
import de.itzbund.egesetz.bmi.lea.domain.enums.Rights;
import de.itzbund.egesetz.bmi.lea.domain.enums.Roles;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.enums.VerfahrensType;
import de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType;
import de.itzbund.egesetz.bmi.lea.domain.mappers.CompoundDocumentMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.DocumentMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.AbstimmungId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CompoundDocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.services.exceptions.DokumentenmappeNotFoundException;
import de.itzbund.egesetz.bmi.lea.domain.services.exceptions.KeinRechteAnDerRessourceException;
import de.itzbund.egesetz.bmi.lea.domain.services.logging.DomainLoggingService;
import de.itzbund.egesetz.bmi.lea.domain.services.logging.LogActivity;
import de.itzbund.egesetz.bmi.lea.domain.services.meta.MetadatenUpdateService;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.ValidationResult;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.CompoundDocumentValidationResult;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationException;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.CompoundDocumentContentValidationTask;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.domain.validator.DocumentPermissionValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.RECHT_KOMMENTARE_LESEN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RECHT_KOMMENTIEREN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RECHT_LESEN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RECHT_NEUE_VERSION_ERSTELLEN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RECHT_SCHREIBEN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RESSORT_KURZBEZEICHNUNG_BT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RESSOURCENTYP_DOKUMENTENMAPPE;
import static de.itzbund.egesetz.bmi.lea.domain.services.CompoundDocumentUtil.addPermissionsForCompoundDocumentContainingDocuments;
import static de.itzbund.egesetz.bmi.lea.domain.services.CompoundDocumentUtil.getAllDocumentsFromOneCompoundDocument;

/**
 * A service class for compound documents It provides CRUD + Mapping functionalities
 */
@Service
@RequiredArgsConstructor
@Log4j2
@SuppressWarnings("unused")
public class CompoundDocumentService implements CompoundDocumentRestPort {

	private static final JSONParser JSON_PARSER = new JSONParser();
	private static final int MIN_LEVELS_REGULATORY_TEXT = 0;
	private static final int MAX_LEVELS_REGULATORY_TEXT = 3;
	public static final int MAX_POSSIBLE_LIST_ITEMS = 1;
	public static final int FIRST_LIST_ELEMENT = 0;

	// ----- Services ---------
	@Autowired
	private RegelungsvorhabenService regelungsvorhabenService;
	@Autowired
	private UserService userService;
	@Autowired
	private AuxiliaryService auxiliaryService;
	@Autowired
	private MetadatenUpdateService metadatenUpdateService;
	@Autowired
	private DomainLoggingService domainLoggingService;
	@Autowired
	private RBACPermissionService rbacPermissionService;

	// ----- Repositories -----
	@Autowired
	private CompoundDocumentPersistencePort compoundDocumentRepository;
	@Autowired
	private DokumentPersistencePort documentRepository;

	// ----- Mapper -----------
	@Autowired
	private CompoundDocumentMapper compoundDocumentMapper;
	@Autowired
	private DocumentMapper documentMapper;
	@Autowired
	private PropositionMapper propositionMapper;

	// ----- Other ------------
	@Autowired
	private LeageSession session;
	@Autowired
	private CompoundDocumentContentValidationTask compoundDocumentContentValidationTask;
	@Autowired
	private DocumentPermissionValidator documentPermissionValidator;
	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private EditorRollenUndRechte editorRollenUndRechte;

	// ========================= CREATE ==================================

	public static DocumentType getDocumentType(CompoundDocumentTypeVariant compoundDocumentType) {
		switch (compoundDocumentType) {
			case AENDERUNGSVERORDNUNG:
				return DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG;
			case MANTELGESETZ:
				return DocumentType.REGELUNGSTEXT_MANTELGESETZ;
			case STAMMVERORDNUNG:
				return DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG;
			default:
				return DocumentType.REGELUNGSTEXT_STAMMGESETZ;
		}
	}

	// ========================= READ ====================================

	/**
	 * Creates a new compound document and send it back to caller Used by: CompoundDocumentController::createCompoundDocument
	 *
	 * @param createCompoundDocumentDTO A transfer object for compound document creation
	 * @param status                    Is a call-by-reference type to transport status back to calling class
	 * @return A transfer object for created compound document
	 */
	@Override
	public CompoundDocumentDTO createCompoundDocument(CreateCompoundDocumentDTO createCompoundDocumentDTO, List<ServiceState> status) {
		try {

			log.debug("Map DTO '{}' to compound document", createCompoundDocumentDTO);
			CompoundDocument compoundDocumentCandidate = applicationContext.getBean(CompoundDocument.class);
			CompoundDocument compoundDocument = compoundDocumentMapper.merge(createCompoundDocumentDTO, compoundDocumentCandidate);

			log.debug("Save compound document '{}'.", compoundDocument);
			CompoundDocumentDTO newCompoundDocumentAsDTO = save(compoundDocument);
			log.debug("Returned '{}' from save.", newCompoundDocumentAsDTO);

			if (newCompoundDocumentAsDTO == null) {
				status.add(ServiceState.REGELUNGSVORHABEN_NOT_FOUND);
				return null;
			}

			// Rechtevergabe für diese Dokumentenmappe
			rbacPermissionService.addCreatorPermissionsForCompoundDocument(compoundDocument);
			// Erzeuge die default Dokumente

			// hänge die Defaultdokumente an das DTO an
			newCompoundDocumentAsDTO = addDefaultDocument(createCompoundDocumentDTO, newCompoundDocumentAsDTO, status);
			return newCompoundDocumentAsDTO;

		} catch (Exception e) {
			log.error(e);
			status.add(ServiceState.INVALID_ARGUMENTS);
			return null;
		}
	}

	/**
	 * Get all compound documents for a single regulatory proposal for the user. !!! Only called by controller, so refresh not needed !!! Used by:
	 * CompoundDocumentController::getByRegulatoryProposalId
	 *
	 * @return List of compound documents
	 */
	@Override
	public List<CompoundDocument> getAllCompoundDocumentsByAuthorAndRegulatoryProposal(UUID regulatoryProposalId) {
		CompoundDocument compoundDocument = applicationContext.getBean(CompoundDocument.class);

		List<UUID> regulatoryProposal = Collections.singletonList(regulatoryProposalId);

		User userEntity = session.getUser();

		return compoundDocument.getCompoundDocumentsOptionalFilteredByUserUndRegelungsvorhaben(userEntity, regulatoryProposal);
	}

	/**
	 * Get all compound documents for the user. !!! Only called by controller, so refresh not needed !!! Used by: CompoundDocumentController::getAll >>>>>>>
	 * origin/develop
	 *
	 * @return List of compound documents
	 */
	@Override
	public List<CompoundDocument> getAllCompoundDocumentsRestrictedByAccessRights() {
		User user = session.getUser();
		CompoundDocument compoundDocument = applicationContext.getBean(CompoundDocument.class);

		// Retrieve Regelungsvorhaben Ids from RBAC
		List<UUID> regelungsvorhabenMitBenutzerZugriff = Collections.emptyList();

		if (user != null) {
			regelungsvorhabenMitBenutzerZugriff = rbacPermissionService.getRegelungsvorhabenIdsMitNutzerZugriff(user);
		} else {
			log.error("User should not be null");
		}

		// No restriction by user
		return compoundDocument.getCompoundDocumentsOptionalFilteredByUserUndRegelungsvorhaben(null, regelungsvorhabenMitBenutzerZugriff);
	}

	/**
	 * Retrieves a compound document with special id. Called by: CompoundDocumentController::getByIdentifier
	 *
	 * @param compoundDocumentId The id of the searched compound document
	 * @param status             a back channel for controller
	 * @return A transfer object for the searched compound document
	 */
	@Override
	public CompoundDocument getCompoundDocumentById(CompoundDocumentId compoundDocumentId, List<ServiceState> status) {
		CompoundDocument compoundDocumentDomainEntity = getCompoundDocumentDomainEntity(compoundDocumentId);
		if (compoundDocumentDomainEntity != null) {
			status.add(ServiceState.OK);
			return compoundDocumentDomainEntity;
		}

		status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
		return null;
	}

	/**
	 * Get's the compound document with a list of containing document Used by: CompoundDocumentController::getDocumentsList
	 *
	 * @param compoundDocumentId Die Dokumenten Id
	 * @param status             Ein Statusfeld
	 * @return Das entsprechende DTO
	 */
	@Override
	public CompoundDocumentSummaryDTO getDocumentsList(CompoundDocumentId compoundDocumentId,
		List<ServiceState> status) {
		CompoundDocument compoundDocument = getCompoundDocumentById(compoundDocumentId, status);
		if (compoundDocument == null) {
			return null;
		}

		CompoundDocumentDomain compoundDocumentEntity = compoundDocument.getCompoundDocumentEntity();

		var dto = makeSummaryDTO(compoundDocumentEntity);
		status.clear();
		status.add(ServiceState.OK);
		return dto;
	}

	/**
	 * Liefert die Dokumentenmappen eines Benutzers für ein Regelungsvorhaben, die einen bestimmten Status innehaben.
	 *
	 * @param userEntity            Der Ersteller der Dokumentenmappe
	 * @param regelungsvorhabenId   Das Regelungsvorhaben
	 * @param compoundDocumentState Der Status der Dokumentenmappe
	 * @return Die Dokumentenmappen, die die o.g. Kriterien erfüllen
	 */
	@Override
	public List<CompoundDocument> getDokumentenmappenFuerEinRegelungsvorhabenMitStatus(
		User userEntity, RegelungsVorhabenId regelungsvorhabenId, DocumentState compoundDocumentState) {

		List<CompoundDocumentDomain> compoundDocumentDomain =
			compoundDocumentRepository.findByCreatedByAndRegelungsvorhabenIdAndState(
				userEntity, regelungsvorhabenId,
				compoundDocumentState);

		return compoundDocumentDomain.stream()
			.map(cd -> {
				CompoundDocument compoundDocument = applicationContext.getBean(CompoundDocument.class);
				compoundDocument.setCompoundDocumentEntity(cd);
				return compoundDocument;
			})
			.collect(Collectors.toList());
	}

	protected CompoundDocument getCompoundDocument(CompoundDocumentId compoundDocumentId, boolean ignoreRights) {
		User userEntity = null;
		if (!ignoreRights) {
			userEntity = session.getUser();
		}

		return CompoundDocumentUtil.getCompoundDocument(compoundDocumentId, userEntity, applicationContext);
	}

	/**
	 * Returns a list of compound documents for a given 'Regelungsvorhaben'
	 *
	 * @param rvId   Id of a 'Regelungsvorhaben'
	 * @param status A field for reflecting errors
	 * @return List of compound documents
	 */
	@Override
	public List<CompoundDocumentSummaryDTO> getCompoundDocumentsForRegelungsvorhaben(String gid, UUID rvId,
		List<ServiceState> status) {
		List<CompoundDocumentDomain> compoundDocuments =
			compoundDocumentRepository.findByCreatedByAndRegelungsVorhabenId(
				userService.getUserForGid(gid),
				new RegelungsVorhabenId(rvId));
		log.debug("List of compound documents for 'Regelungsvorhaben' with id {}: {}", rvId, compoundDocuments);

		List<CompoundDocumentSummaryDTO> compoundDocumentSummaryDTO =
			compoundDocumentMapper.maptoCompoundDocumentSummaryDTOList(
				compoundDocuments);
		status.add(ServiceState.OK);
		return compoundDocumentSummaryDTO;
	}

	/**
	 * Schicke alle Dokumentenmappen zu einem bestimmten Regelungsvorhaben, für das ich Teilnehmer bin - unabhängig vom Status der Dokumentenmappen
	 *
	 * @param gid                 Die ID des Teilnehmers
	 * @param regelungsVorhabenId Die ID des Regelungsvorhabens
	 * @param compoundDocumentId  -- nicht verwendet --
	 * @param abstimmungId        -- nicht verwendet --
	 * @param status              Status des Prozesses
	 * @return Liste der Dokumentenmappen für die o.g. zutrifft
	 */
	@Override
	public List<CompoundDocumentSummaryDTO> getCompoundDocumentsForAbstimmung(String gid,
		RegelungsVorhabenId regelungsVorhabenId, CompoundDocumentId compoundDocumentId, AbstimmungId abstimmungId,
		List<ServiceState> status) {
		// get user object for gid
		User userEntity = userService.getUserForGid(gid);

		CompoundDocument compoundDocument = applicationContext.getBean(CompoundDocument.class);
		List<CompoundDocument> compoundDocuments =
			compoundDocument.getCompoundDocumentsOptionalFilteredByUserUndRegelungsvorhaben(
				userEntity,
				Collections.singletonList(regelungsVorhabenId.getId()));

		if (compoundDocuments.isEmpty()) {
			status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
		} else {
			status.add(ServiceState.OK);
		}

		return compoundDocumentMapper.mapCompoundDocumentToCompoundDocumentSummaryDTOList(compoundDocuments);
	}

	// ========================= UPDATE ==================================

	/**
	 * Gibt die Dokumentenmappe eines Benutzers und eines Regelungsvorhabens zurück, die im Status 'Bereit für Kabinettverfahren' sind.
	 *
	 * @param gid                 Die Id des Benutzers
	 * @param regelungsVorhabenId Die Id des Regelungsvorhabens
	 * @param status              Der Status bei der Verarbeitung
	 * @return Eine Liste mit Dokumenten
	 */
	@Override
	public List<PkpZuleitungDokumentDTO> getCompoundDocumentsForKabinett(String gid,
		RegelungsVorhabenId regelungsVorhabenId, List<ServiceState> status) {
		// get user object for gid
		User userEntity = userService.getUserForGid(gid);

		CompoundDocument compoundDocument = applicationContext.getBean(CompoundDocument.class);
		List<CompoundDocument> dokumentMappen = getDokumentenmappenFuerEinRegelungsvorhabenMitStatus(
			userEntity, regelungsVorhabenId, DocumentState.BEREIT_FUER_KABINETT);

		// AK: Nach Konvention kann es nur EINE Dokumentenmappe geben, die im Status Bereit für Kabinettverfahren ist.
		if (!dokumentMappen.isEmpty()) {
			CompoundDocument compoundDocument1 = dokumentMappen.get(FIRST_LIST_ELEMENT);
			List<Document> dokumente = compoundDocument1.getDokumenteDieserDokumentenmappe();

			List<PkpZuleitungDokumentDTO> pkpZuleitungDokumentDTOList = dokumente.stream()
				.map(dok -> PkpZuleitungDokumentDTO.builder()
					.dokumentenMappeId(compoundDocument1.getCompoundDocumentId()
						.getId())
					.dokumentName(dok.getDocumentEntity()
						.getTitle())
					.pkpTyp(dok.getDocumentEntity()
						.getType())
					.dokumentInhalt(Utils.base64Encode(dok.getDocumentEntity()
						.getContent()))
					.build())
				.collect(Collectors.toList());

			if (pkpZuleitungDokumentDTOList.isEmpty()) {
				status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
			} else {
				status.add(ServiceState.OK);
			}

			return pkpZuleitungDokumentDTOList;
		}

		status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
		return Collections.emptyList();
	}

	private CompoundDocumentDTO addDefaultDocument(CreateCompoundDocumentDTO createCompoundDocumentDTO,
		CompoundDocumentDTO compoundDocumentDTO, List<ServiceState> status) {

		if (Utils.isMissing(createCompoundDocumentDTO.getTitleRegulatoryText())) {
			status.add(ServiceState.OK);
			return compoundDocumentDTO;
		}

		CompoundDocumentId compoundDocumentId = new CompoundDocumentId(compoundDocumentDTO.getId());
		DocumentType documentType = getDocumentType(createCompoundDocumentDTO.getType());

		CreateDocumentDTO createDocumentDTO = CreateDocumentDTO.builder()
			.title(createCompoundDocumentDTO.getTitleRegulatoryText())
			.regelungsvorhabenId(compoundDocumentDTO.getProposition()
				.getId())
			.type(documentType)
			.initialNumberOfLevels(
				Math.min(
					MAX_LEVELS_REGULATORY_TEXT,
					Math.max(
						MIN_LEVELS_REGULATORY_TEXT,
						createCompoundDocumentDTO.getInitialNumberOfLevels())))
			.build();

		Document document = auxiliaryService.makeDocument(compoundDocumentId, createDocumentDTO);

		return addDocument(
			compoundDocumentId,
			document,
			status);
	}

	// save part - 'protected' visibility only for testing
	protected CompoundDocumentDTO save(CompoundDocument compoundDocument) {

		CompoundDocument mergedCompoundDocument = compoundDocument.createOrUpdate(compoundDocument, session.getUser());

		log.debug("The full compound document is: {}", mergedCompoundDocument);
		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = regelungsvorhabenService.getRegelungsvorhabenEditorTriggeredByDocument(
			compoundDocument.getRegelungsVorhabenId());

		log.debug("Got its 'RegelungsvorhabenEditorDTO' {}", regelungsvorhabenEditorDTO);
		if (regelungsvorhabenEditorDTO != null) {
			return compoundDocumentMapper.mapFromCompoundDocumentAndProposition(mergedCompoundDocument, regelungsvorhabenEditorDTO);
		}

		return null;
	}

	@Override
	public CompoundDocument getCompoundDocumentDomainEntity(CompoundDocumentId compoundDocumentId) {
		CompoundDocument compoundDocument = applicationContext.getBean(CompoundDocument.class);
		return compoundDocument.getCompoundDocumentWithDocumentsForIdAndOptionalFiltered(compoundDocumentId, null);
	}

	/**
	 * Adds a document to the compound document for the current user
	 *
	 * @param compoundDocumentId The compound document where a document should be created
	 * @param document           The document that should be inserted
	 * @param status             An object for holding error or success information
	 * @return Compound document including new document and success or reason for failing
	 */
	@Override
	public CompoundDocumentDTO addDocument(CompoundDocumentId compoundDocumentId, Document document,
		List<ServiceState> status) {
		log.debug("Getting current user.");
		User userEntity = session.getUser();
		log.debug("User '{}' was found.", userEntity);

		return addDocumentForUser(compoundDocumentId, document, userEntity, status);
	}

	/**
	 * Adds a document to the compound document for the given user
	 *
	 * @param compoundDocumentId The compound document where a document should be created
	 * @param document           The document that should be inserted
	 * @param status             An object for holding error or success information
	 * @param userEntity         The user for which the object should be created
	 * @return Compound document including new document and success or reason for failing
	 */
	@Override
	public CompoundDocumentDTO addDocumentForUser(CompoundDocumentId compoundDocumentId, Document document,
		User userEntity, List<ServiceState> status) {

		log.debug("Get compound document");
		CompoundDocument compoundDocument1 = applicationContext.getBean(CompoundDocument.class);
		CompoundDocument compoundDocument2 = compoundDocument1.getCompoundDocumentWithDocumentsForIdAndOptionalFiltered(compoundDocumentId, null);
		log.debug("Compound document is {}", compoundDocument2);

		if (compoundDocument2 != null) {

			log.debug("Try to get Regelungsvorhaben.");
			RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO =
				regelungsvorhabenService.getRegelungsvorhabenEditorTriggeredByDocument(
					compoundDocument2.getRegelungsVorhabenId());
			log.debug("Found Regelungsvorhaben: {}", regelungsvorhabenEditorDTO);

			if (!validateDocumentInsertionPossibilities(compoundDocument2, document, status)) {
				document.setMetadatenUpdateService(metadatenUpdateService);
				boolean isDocumentAddedToCompoundDocument = compoundDocument2.addDocumentToCompoundDocument(document,
					compoundDocument2);

				if (isDocumentAddedToCompoundDocument) {
					status.add(ServiceState.OK);
					rbacPermissionService.addCreatorPermissionsForDocument(document);
				} else {
					status.add(ServiceState.COMPOUND_DOCUMENT_ID_MISSING);
					return null;
				}

				// Reload
				compoundDocument2 = getCompoundDocumentById(compoundDocument2.getCompoundDocumentId(),
					new ArrayList<>());

				return compoundDocumentMapper.mapFromCompoundDocumentAndProposition(compoundDocument2,
					regelungsvorhabenEditorDTO);
			} else {
				// log in methode
				return null;
			}
		} else {
			status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
			return null;
		}
	}

	private boolean validateDocumentInsertionPossibilities(CompoundDocument compoundDocument2,
		Document document, List<ServiceState> status) {
		if (document != null) {
			DocumentType newDocumentType = document.getDocumentEntity()
				.getType();
			compoundDocumentContentValidationTask.setInputSource(compoundDocument2, newDocumentType);
			try {
				ValidationResult run = compoundDocumentContentValidationTask.run(null);
				if (!CompoundDocumentValidationResult.VALIDATION_SUCCESS.equals(run)) {
					handleValidationFailure((CompoundDocumentValidationResult) run, newDocumentType, status);
					return true;
				}
			} catch (ValidationException e) {
				log.error(e);
				status.add(ServiceState.DOCUMENT_VALIDATION_ERROR);
				return true;
			}

		} else {
			log.error("Document from controller is null!");
			status.add(ServiceState.DOCUMENT_NOT_VALID);
			return true;
		}
		return false;
	}

	private void handleValidationFailure(CompoundDocumentValidationResult result, DocumentType newDocumentType,
		List<ServiceState> status) {
		if (result == CompoundDocumentValidationResult.COMPOUND_DOCUMENT_IS_PROTECTED) {
			log.debug("Compound document has invalid state. Adding new document is not allowed.");
			status.add(ServiceState.INVALID_STATE);
		} else if (result == CompoundDocumentValidationResult.TO_MUCH_DOCUMENTS_OF_GIVEN_TYPE) {
			log.debug("No more documents of type '{}' are allowed.", newDocumentType);
			status.add(ServiceState.DOCUMENT_DUPLICATION);
		} else {
			status.add(ServiceState.DOCUMENT_HAS_WRONG_TYPE);
		}
	}

	/**
	 * Erzeugt aus eine Liste von DokumentenmappeIDs eine Liste von Dokumentenmappen angereichert mit Dokumenten.
	 *
	 * @param gid                 UUID of user
	 * @param compoundDocumentIds A list of compound document ids
	 * @param status              A status list
	 * @return A list of compound docuents
	 */
	@Override
	public List<CompoundDocument> getAllDocumentsFromCompoundDocuments(String gid,
		List<String> compoundDocumentIds,
		List<ServiceState> status) {
		List<CompoundDocument> compoundDocumentList =
			compoundDocumentIds.stream()
				.map(
					compoundDocumentId -> getAllDocumentsFromOneCompoundDocument(userService.getUserForGid(gid),
						new CompoundDocumentId(UUID.fromString(compoundDocumentId)),
						applicationContext)
				)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());

		log.debug("Got compound documents {} from aggregate", compoundDocumentList);
		status.add(ServiceState.OK);
		return compoundDocumentList;
	}

	DocumentState getCompoundDocumentState(CompoundDocumentId compoundDocumentId) {
		CompoundDocument compoundDocument = getCompoundDocument(compoundDocumentId, true);
		if (compoundDocument != null) {
			return compoundDocument.getCompoundDocumentEntity()
				.getState();
		}
		return null;
	}

	/**
	 * Retrieves, if at least one compound document is in state "Bereit für Kabinettverfahren". These are all compound document that belong to the
	 * Regelungsvorhaben which belongs to the given compound document.
	 *
	 * @return True, if there is any Kabinett Verfahren in the Regelungsvorhaben. False, in other case.
	 */
	private boolean isAnyCompoundDocumentOfRegelungsvorhabenInKabinettVerfahren(CompoundDocumentDomain compoundDocument) {
		// Get Regelungsvorhaben for THIS compound document
		RegelungsVorhabenId regelungsVorhabenId = compoundDocument.getRegelungsVorhabenId();

		// Get all compound documents with SAME Regelungsvorhaben AND status "Bereit für Kabinettverfahren"
		List<CompoundDocumentDomain> listOfKabinettVerfahren =
			compoundDocumentRepository.findByRegelungsVorhabenIdAndCompoundDocumentState(
				regelungsVorhabenId, DocumentState.BEREIT_FUER_KABINETT);

		// True, if there is any
		return !listOfKabinettVerfahren.isEmpty();
	}

	/**
	 * This method is called by platform only. It changes the state of a compound document.
	 */
	@Override
	public ServiceState changeCompoundDocumentStateByPlatform(String gid, CompoundDocumentId compoundDocumentId,
		DocumentState possibleNewCompoundDocumentState) {
		// Test, if all values are set took place in PlategController
		List<Roles> rolesList = new ArrayList<>();
		rolesList.add(Roles.PLATFORM);

		User userEntity = userService.getUserForGid(gid);
		CompoundDocument compoundDocument = CompoundDocumentUtil.getCompoundDocument(compoundDocumentId, userEntity, applicationContext);

		if (compoundDocument != null) {
			CompoundDocumentDomain compoundDocumentEntity = compoundDocument.getCompoundDocumentEntity();

			List<ServiceState> serviceStates = new ArrayList<>();
			UUID rvReferenz = isStateChangeForCompoundDocumentPossible(compoundDocumentEntity, possibleNewCompoundDocumentState, rolesList, serviceStates);

			if (serviceStates.isEmpty()) {
				return ServiceState.INCONSISTENT_DATA;
			} else if (!serviceStates.get(0).equals(ServiceState.OK)) {
				return ServiceState.INVALID_STATE;
			}

			compoundDocumentEntity.setFixedRegelungsvorhabenReferenzId(rvReferenz);
			compoundDocumentEntity.setState(possibleNewCompoundDocumentState);
			rbacPermissionService.addCreatorPermissionsForCompoundDocument(gid, compoundDocumentId);

			// save changes
			compoundDocumentRepository.save(compoundDocumentEntity);
			return ServiceState.OK;
		}

		return ServiceState.COMPOUND_DOCUMENT_NOT_FOUND;
	}

	// ============================================================

	/**
	 * Possibility to give or revoke access rights for other users ONLY FOR READING in this version
	 *
	 * @param compoundDocumentId The id of a compound document
	 * @param userRightsDTO      A List of people and new rights
	 * @return A state
	 */
	@Transactional(value = "authTransactionManager")
	@Override
	public ServiceState changeAccessRightsForUsersOnCompoundDocuments(CompoundDocumentId compoundDocumentId,
		List<UserRightsDTO> userRightsDTO) {
		// Get the compound document
		CompoundDocument compoundDocument = getCompoundDocument(compoundDocumentId, true);
		if (compoundDocument == null) {
			return ServiceState.COMPOUND_DOCUMENT_NOT_FOUND;
		}

		List<Document> documents = compoundDocument.getDocuments();

		// Edge case - remove all
		if (userRightsDTO.isEmpty()) {
			// Remove all additional permissions (for LESER)
			rbacPermissionService.entferneAlleZusaetzlichenLeserechteDerDokumentenmappe(compoundDocumentId);
			documents.forEach(document -> rbacPermissionService.entferneAlleZusaetzlichenLeserechteDesDokuments(document.getDocumentId()));
			return ServiceState.REMOVED_ALL_PERMISSIONS;
		}

		// Get all user that have permission on this ressource
		List<String> alleBenutzerGids = rbacPermissionService.getBenutzerGidsMitDokumentenmappenZugriffAlsLeser(compoundDocument.getCompoundDocumentId());
		userRightsDTO.forEach(user ->
			addReadPermissionsForCompoundDocumentAndDocuments(compoundDocument, alleBenutzerGids, user));

		// remove
		alleBenutzerGids.forEach(user -> {
			rbacPermissionService.entferneAlleZusaetzlichenLeserechteDerDokumentenmappeEinesBenutzers(compoundDocumentId, user);
			domainLoggingService.logActivity(LogActivity.REVOKE_READ_ACCESS, userService.getUser().getGid().getId());

			documents.forEach(document -> rbacPermissionService.entferneAlleZusaetzlichenLeserechteDesDokumentsEinesBenutzers(document.getDocumentId(), user));
		});

		return ServiceState.OK;
	}

	/**
	 * Changes the write permission on a compound document. Only ONE write permission can be set (the creator has its own because of its role) Not implemented
	 * yet: Optionale Version eine DRAFT-Dokumentenmappe und zeitliche Schreibrechteweitergabe
	 *
	 * @param compoundDocumentId Id of compound document
	 * @param userRightsDTO      A list of what to do
	 * @return Status der Operation
	 */
	@Override
	public CompoundDocumentSummaryDTO changeWritePermissionsOnCompoundDocuments(CompoundDocumentId compoundDocumentId, UserRightsDTO userRightsDTO,
		List<ServiceState> status) {

		// Get the compound document
		CompoundDocument compoundDocument = getCompoundDocument(compoundDocumentId, true);
		if (compoundDocument == null) {
			status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
			return null;
		}

		// Löschen der vergebenen Schreibrechte
		if (userRightsDTO == null) {
			User ersteller = compoundDocument.getCreatedBy();

			userRightsDTO = UserRightsDTO.builder()
				.erstelleNeueVersionBeiDraft(false)
				.email(ersteller.getEmail())
				.name(ersteller.getName())
				.gid(ersteller.getGid().getId())
				.befristung(null)
				.anmerkungen(null)
				.freigabetyp(Freigabetyp.SCHREIBRECHTE)
				.build();
		}

		// Only write permission is possible
		if (userRightsDTO.getFreigabetyp() != Freigabetyp.SCHREIBRECHTE) {
			status.add(ServiceState.INCONSISTENT_DATA);
			return null;
		}

		return addWritePermissionsToUser(compoundDocument, userRightsDTO, status);
	}

	private void addReadPermissionsForCompoundDocumentAndDocuments(CompoundDocument compoundDocument, List<String> alleBenutzer,
		UserRightsDTO user) {

		if (!alleBenutzer.contains(user.getGid()) && (Freigabetyp.LESERECHTE.equals(user.getFreigabetyp()))) {

			CompoundDocumentId compoundDocumentId = compoundDocument.getCompoundDocumentId();
			List<String> benutzerGidsMitDokumentenmappenZugriffAlsSchreiber = rbacPermissionService.getBenutzerGidsMitDokumentenmappenZugriffAlsSchreiber(
				compoundDocumentId);

			if (!benutzerGidsMitDokumentenmappenZugriffAlsSchreiber.contains(user.getGid())) {
				CompoundDocumentSummaryDTO compoundDocumentDTO = CompoundDocumentSummaryDTO.builder()
					.id(compoundDocument.getCompoundDocumentId().getId())
					.title(compoundDocument.getCompoundDocumentEntity().getTitle()).build();

				rbacPermissionService.vergibLeserechteFuerEinenNutzerZuEinerDokumentenmappe(session.getUser(), user.getGid(), compoundDocumentDTO);
				domainLoggingService.logActivity(LogActivity.GRANT_READ_ACCESS, userService.getUser().getGid().getId());

				List<Document> documents = compoundDocument.getDocuments();
				documents.forEach(document -> rbacPermissionService.vergibLeserechteFuerEinenNutzerZuEinemDokument(user.getGid(), document.getDocumentId()));
			}
		}

		// remove calculated users from list
		alleBenutzer.remove(user.getGid());
	}

	// Gibt es den Teil des Erstellens einer Kopie nicht schon irgendwo im Code?
	private CompoundDocumentSummaryDTO addWritePermissionsToUser(CompoundDocument compoundDocument, UserRightsDTO userRights, List<ServiceState> status) {

		CompoundDocumentSummaryDTO compoundDocumentNewVersion;

		status.add(ServiceState.OK);
		CompoundDocumentId dokumentenMappeId = compoundDocument.getCompoundDocumentId();
		List<DocumentId> documentIds = compoundDocument.getDocuments().stream().map(Document::getDocumentId).collect(Collectors.toList());

		// In bestimmten Fällen eine neue Version erstellen (inkl. Dokumente)
		if ((compoundDocument.getState() != DocumentState.DRAFT)
			|| (compoundDocument.getState() == DocumentState.DRAFT && userRights.isErstelleNeueVersionBeiDraft())) {

			compoundDocumentNewVersion = copyCompoundDocument(dokumentenMappeId, compoundDocument.getCompoundDocumentEntity().getTitle(), documentIds,
				new ArrayList<>());
			documentIds = compoundDocumentNewVersion.getDocuments().stream().map(document -> new DocumentId(document.getId())).collect(Collectors.toList());

			dokumentenMappeId = new CompoundDocumentId(compoundDocumentNewVersion.getId());

			// Übernahme der bestehenden Leserechte
			List<String> alleBenutzerIds = rbacPermissionService.getBenutzerGidsMitDokumentenmappenZugriffAlsLeser(compoundDocument.getCompoundDocumentId());

			CompoundDocumentId finalMappeFueSchreibrechte = dokumentenMappeId; // Lambda
			List<DocumentId> finalDocumentIds = documentIds;  // Lambda
			alleBenutzerIds.forEach(
				benutzerId -> {

					CompoundDocumentSummaryDTO compoundDocumentDTO = CompoundDocumentSummaryDTO.builder()
						.id(finalMappeFueSchreibrechte.getId())
						.title(compoundDocumentNewVersion.getTitle()).build();

					rbacPermissionService.vergibLeserechteFuerEinenNutzerZuEinerDokumentenmappe(session.getUser(), benutzerId, compoundDocumentDTO);
					domainLoggingService.logActivity(LogActivity.GRANT_READ_ACCESS, userService.getUser().getGid().getId());
					finalDocumentIds.forEach(
						documentId -> rbacPermissionService.vergibLeserechteFuerEinenNutzerZuEinemDokument(benutzerId, documentId)
					);
				}
			);

		} else {
			// Ohne neue Dokumenterstellung
			compoundDocumentNewVersion = compoundDocumentMapper.map(compoundDocument.getCompoundDocumentEntity());
		}

		entferneLeserechteFuerNeuenNutzer(userRights, dokumentenMappeId, documentIds);
		// Den alten Nutzer mit Schreibrechten herausfinden
		List<String> benutzerGidsMitDokumentenmappenZugriffAlsSchreiber = Stream.concat(
				rbacPermissionService.getBenutzerGidsMitDokumentenmappenZugriffAlsErsteller(dokumentenMappeId).stream(),
				rbacPermissionService.getBenutzerGidsMitDokumentenmappenZugriffAlsSchreiber(dokumentenMappeId).stream())
			.collect(Collectors.toList());

		CompoundDocumentSummaryDTO compoundDocumentSummaryDTO = CompoundDocumentSummaryDTO.builder()
			.id(compoundDocument.getCompoundDocumentId().getId())
			.title(compoundDocument.getCompoundDocumentEntity().getTitle())
			.build();

		schreibRechteFuerNeuenNutzerAnlegen(userRights, compoundDocumentSummaryDTO, documentIds);
		leseRechteFuerAltenNutzerVergeben(benutzerGidsMitDokumentenmappenZugriffAlsSchreiber, compoundDocumentNewVersion, documentIds);

		return compoundDocumentNewVersion;
	}

	private void entferneLeserechteFuerNeuenNutzer(UserRightsDTO userRights, CompoundDocumentId dokumentenMappeId, List<DocumentId> documentIds) {
		// Vorhandene Leserechte entfernen, da sie durch Schreibrechte (mit Leserechten) ersetzt werden
		rbacPermissionService.entferneAlleZusaetzlichenLeserechteDerDokumentenmappeEinesBenutzers(dokumentenMappeId, userRights.getGid());
		domainLoggingService.logActivity(LogActivity.REVOKE_READ_ACCESS, userService.getUser().getGid().getId());
		documentIds.forEach(documentId -> rbacPermissionService.entferneAlleZusaetzlichenLeserechteDesDokumentsEinesBenutzers(documentId, userRights.getGid()));
	}

	private void leseRechteFuerAltenNutzerVergeben(List<String> benutzerGidsMitDokumentenmappenZugriffAlsSchreiber, CompoundDocumentSummaryDTO dokumentenMappe,
		List<DocumentId> documentIds) {
		if (!benutzerGidsMitDokumentenmappenZugriffAlsSchreiber.isEmpty()) {
			String benutzerGid = benutzerGidsMitDokumentenmappenZugriffAlsSchreiber.get(benutzerGidsMitDokumentenmappenZugriffAlsSchreiber.size() - 1);
			// Leserechte setzen (falls nicht schon vorhanden)
			// Hinweis: Doppeleintragungen --> Constraint violation --> wird abgefangen

			rbacPermissionService.vergibLeserechteFuerEinenNutzerZuEinerDokumentenmappe(session.getUser(), benutzerGid, dokumentenMappe);
			domainLoggingService.logActivity(LogActivity.GRANT_READ_ACCESS, userService.getUser().getGid().getId());
			domainLoggingService.logActivity(LogActivity.REVOKE_WRITE_ACCESS, userService.getUser().getGid().getId());
			documentIds.forEach(documentId -> rbacPermissionService.vergibLeserechteFuerEinenNutzerZuEinemDokument(benutzerGid, documentId));
		}
	}

	private void schreibRechteFuerNeuenNutzerAnlegen(UserRightsDTO userRights, CompoundDocumentSummaryDTO compoundDocumentSummaryDTO,
		List<DocumentId> documentIds) {
		// Der 'richtigen' Mappe die Schreibrechte zuordnen
		// Die Klasse regelt selber, dass kein Schreibrecht doppelt vorhanden sein darf.
		rbacPermissionService.vergibSchreibrechteFuerEinenNutzerZuEinerDokumentenmappeMitBefristung(session.getUser(), userRights.getGid(),
			compoundDocumentSummaryDTO,
			userRights.getBefristung());
		domainLoggingService.logActivity(LogActivity.GRANT_WRITE_ACCESS, userService.getUser().getGid().getId());

		documentIds.forEach(
			documentId -> rbacPermissionService.vergibSchreibrechteFuerEinenNutzerZuEinemDokumentMitBefristung(userRights.getGid(), documentId,
				userRights.getBefristung())
		);
	}

	// ============================================================

	/**
	 * Checks, if all conditions ar fullfilled to change state of given compound document
	 */
	protected UUID isStateChangeForCompoundDocumentPossible(CompoundDocumentDomain compoundDocumentDomain,
		DocumentState possibleNewCompoundDocumentState, List<Roles> rolesOfRequester, List<ServiceState> serviceStates) {
		DocumentState oldCompoundDocumentState = compoundDocumentDomain.getState();
		RegelungsVorhabenId regelungsVorhabenId = compoundDocumentDomain.getRegelungsVorhabenId();

		if (rolesOfRequester.contains(Roles.EDITOR) && (rolesOfRequester.contains(Roles.ERSTELLER_DOKUMENTENMAPPE))) {
			ServiceState kabinettVotingOngoing = creatorStateChanges(compoundDocumentDomain, possibleNewCompoundDocumentState, oldCompoundDocumentState,
				regelungsVorhabenId);
			if (kabinettVotingOngoing != null) {
				serviceStates.add(kabinettVotingOngoing);
				return null;
			}
		}

		if (rolesOfRequester.contains(Roles.PLATFORM)) {
			serviceStates.add(ServiceState.OK);
			return platformStateChanges(possibleNewCompoundDocumentState, oldCompoundDocumentState,
				regelungsVorhabenId, compoundDocumentDomain, serviceStates);
		}

		serviceStates.add(ServiceState.OK);
		return regelungsVorhabenId.getId();
	}

	@SuppressWarnings({"java:S1151", "java:S1541"})
	private ServiceState creatorStateChanges(CompoundDocumentDomain compoundDocumentDomain,
		DocumentState possibleNewCompoundDocumentState,
		DocumentState oldCompoundDocumentState, RegelungsVorhabenId regelungsVorhabenId) {

		// BEREIT_FUER_BUNDESTAG ist nur dann möglich, wenn das Regelungsvorhaben zur
		// Dokumentenmappe den Status BUNDESTAG hat
		if (possibleNewCompoundDocumentState.equals(DocumentState.BEREIT_FUER_BUNDESTAG)) {
			RegelungsvorhabenEditorDTO regulatoryProposalById = regelungsvorhabenService.getRegelungsvorhaben(
				compoundDocumentDomain.getRegelungsVorhabenId(), oldCompoundDocumentState);
			if (!regulatoryProposalById.getStatus().equals(VorhabenStatusType.BUNDESTAG)) {
				return ServiceState.FEHLENDER_STATUS_BUNDESRAT;
			}
		}

		switch (oldCompoundDocumentState) {
			case FINAL:
				if (possibleNewCompoundDocumentState != DocumentState.BEREIT_FUER_KABINETT) {
					return ServiceState.INVALID_STATE;
				} else {
					return ServiceState.OK;
				}
			case DRAFT:
				// Store static proposition data
				if (possibleNewCompoundDocumentState != DocumentState.DRAFT) {
					storePropositionData(regelungsVorhabenId, compoundDocumentDomain);
				}
				// fallthrough
			case FREEZE:
				return ServiceState.OK;
			case BEREIT_FUER_KABINETT:
				// Kein anderes darf den Status haben
				if (isAnyCompoundDocumentOfRegelungsvorhabenInKabinettVerfahren(compoundDocumentDomain)) {
					return ServiceState.ONLY_ONE_KABINETT_VOTING;
				}
				break;
			case ZUGELEITET_PKP:
				return ServiceState.OK;
			default:
				return ServiceState.INVALID_STATE;
		}
		return null;
	}


	@SuppressWarnings("java:S1151")
	private UUID platformStateChanges(DocumentState possibleNewCompoundDocumentState,
		DocumentState oldCompoundDocumentState, RegelungsVorhabenId regelungsVorhabenId,
		CompoundDocumentDomain compoundDocumentDomain, List<ServiceState> states) {
		switch (possibleNewCompoundDocumentState) {
			case DRAFT:
				states.add(ServiceState.OK);
				break;
			case FINAL:
			case FREEZE:
				return transitionsFromRegelungsentwurf(oldCompoundDocumentState, regelungsVorhabenId,
					compoundDocumentDomain, states);
			case ZUGELEITET_PKP:
				// Wenn vorher Bereit für Kabinettverfahren
				if (oldCompoundDocumentState == DocumentState.BEREIT_FUER_KABINETT) {
					states.add(ServiceState.OK);
					break;
				}
				states.add(ServiceState.INVALID_STATE);
				break;
			case BEREIT_FUER_KABINETT:
				// Darf Plattform nicht
			default:
				states.add(ServiceState.INVALID_STATE);
		}

		return null;
	}

	private UUID transitionsFromRegelungsentwurf(DocumentState oldCompoundDocumentState,
		RegelungsVorhabenId regelungsVorhabenId, CompoundDocumentDomain compoundDocumentDomain, List<ServiceState> states) {
		// Wenn vorher weder in Zuleitung noch in Bereit für Kabinettverfahren
		if ((oldCompoundDocumentState == DocumentState.BEREIT_FUER_KABINETT) || (oldCompoundDocumentState
			== DocumentState.ZUGELEITET_PKP)) {
			states.add(ServiceState.INVALID_STATE);
			return null;
		}

		// Store static proposition data
		if (oldCompoundDocumentState == DocumentState.DRAFT) {
			return storePropositionData(regelungsVorhabenId, compoundDocumentDomain);
		}

		states.add(ServiceState.OK);
		return null;
	}

	private UUID storePropositionData(RegelungsVorhabenId regelungsVorhabenId,
		CompoundDocumentDomain compoundDocumentDomain) {

		Proposition proposition = regelungsvorhabenService.sichereRegelungsvorhaben2(regelungsVorhabenId, true);

		updateProprietaryMetdata(compoundDocumentDomain, proposition);
		return proposition.getReferenzId();
	}

	@SuppressWarnings("unchecked")
	private void updateProprietaryMetdata(CompoundDocumentDomain compoundDocumentDomain, Proposition proposition) {
		Optional<List<DocumentDomain>> optionalList =
			documentRepository.findByCompoundDocumentId(compoundDocumentDomain.getCompoundDocumentId());
		compoundDocumentDomain.setFixedRegelungsvorhabenReferenzId(proposition.getReferenzId());

		List<DocumentDomain> documentList = optionalList.orElse(List.of());

		documentList.forEach(documentDomain -> {
			String content = documentDomain.getContent();
			try {
				JSONObject dokument = ((JSONObject) ((JSONArray) JSON_PARSER.parse(content)).get(FIRST_LIST_ELEMENT));
				dokument = metadatenUpdateService.update(dokument, proposition);
				JSONArray wrapper = new JSONArray();
				wrapper.add(dokument);
				documentDomain.setContent(wrapper.toJSONString());
				documentRepository.save(documentDomain);
			} catch (ParseException e) {
				log.error(e);
			}
		});
	}

	private CompoundDocumentSummaryDTO makeSummaryDTO(CompoundDocumentDomain compoundDocumentEntity) {
		Optional<List<DocumentDomain>> optionalDocumentEntities =
			documentRepository.findByCompoundDocumentId(compoundDocumentEntity.getCompoundDocumentId());
		List<DocumentDomain> documentEntities = optionalDocumentEntities.orElse(List.of());

		PermissionDTO documentPermissionDTO = getDocumentPermissionDTO(compoundDocumentEntity.getCompoundDocumentId());
		PermissionDTO commentPermissionDTO = getCommentPermissionDTO(compoundDocumentEntity.getCompoundDocumentId());

		CompoundDocumentSummaryDTO compoundDocumentSummaryDTO =
			compoundDocumentMapper.maptoCompoundDocumentSummaryDTOList(List.of(compoundDocumentEntity))
				.get(FIRST_LIST_ELEMENT);
		compoundDocumentSummaryDTO.setDocumentPermissions(documentPermissionDTO);
		compoundDocumentSummaryDTO.setCommentPermissions(commentPermissionDTO);
		compoundDocumentSummaryDTO.setDocuments(
			documentEntities.stream()
				.map(d -> {
					var dto = documentMapper.mapSummaryDomain(d);
					dto.setState(compoundDocumentEntity.getState());
					dto.setVersion(compoundDocumentEntity.getVersion());
					dto.setDocumentPermissions(documentPermissionDTO);
					dto.setCommentPermissions(commentPermissionDTO);
					return dto;
				})
				.collect(Collectors.toList())
		);
		return compoundDocumentSummaryDTO;
	}

	private PermissionDTO getDocumentPermissionDTO(CompoundDocumentId compoundDocumentId) {
		Map<String, Boolean> rechte =
			getAccessRightsForCompoundDocument(compoundDocumentId);

		PermissionDTO permissions = PermissionDTO.builder()
			.hasRead(false)
			.hasWrite(false)
			.build();

		if (rechte.containsKey(RECHT_LESEN)) {
			permissions.setHasRead(rechte.get(RECHT_LESEN));
		}

		if (rechte.containsKey(RECHT_SCHREIBEN)) {
			permissions.setHasWrite(rechte.get(RECHT_SCHREIBEN));
		}

		return permissions;
	}

	private PermissionDTO getCommentPermissionDTO(CompoundDocumentId compoundDocumentId) {
		Map<String, Boolean> rechte =
			getAccessRightsForCompoundDocument(compoundDocumentId);

		PermissionDTO permissions = PermissionDTO.builder()
			.hasRead(false)
			.hasWrite(false)
			.build();

		if (rechte.containsKey(RECHT_KOMMENTARE_LESEN)) {
			permissions.setHasRead(rechte.get(RECHT_KOMMENTARE_LESEN));
		}

		if (rechte.containsKey(RECHT_KOMMENTIEREN)) {
			permissions.setHasWrite(rechte.get(RECHT_KOMMENTIEREN));
		}

		return permissions;
	}

	/**
	 * Changes values of compound document. Called by 'Patch'.
	 */
	@Override
	public void update(CompoundDocumentId compoundDocumentId, String newTitle, VerfahrensType verfahrenstyp,
		DocumentState newCompoundDocumentState, List<ServiceState> status) {
		// Find compound document (by Identifier)
		Optional<CompoundDocumentDomain> optionalCompoundDocumentEntity =
			compoundDocumentRepository.findByCompoundDocumentId(compoundDocumentId);

		if (optionalCompoundDocumentEntity.isEmpty()) {
			status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
			return;
		}

		CompoundDocumentDomain compoundDocumentEntity = optionalCompoundDocumentEntity.get();

		// Proof my rights for this compound document
		if (!documentPermissionValidator.isUserOwnerOfCompoundDocumentEntity(compoundDocumentEntity)) {
			status.add(ServiceState.NO_PERMISSION);
			return;
		}

		// what I want to do?
		// change title
		if (newTitle != null) {
			if (DocumentState.DRAFT == compoundDocumentEntity.getState()) {
				compoundDocumentEntity.setTitle(newTitle);
			} else {
				status.add(ServiceState.NO_PERMISSION);
				return;
			}
		}

		// change Verfahrenstyp
		if (verfahrenstyp != null) {
			compoundDocumentEntity.setVerfahrensType(verfahrenstyp);
		}

		// change state of compound document
		if (newCompoundDocumentState != null) {
			CompoundDocumentDomain compoundDocumentDomain = changeDocumentStateByEditor(compoundDocumentEntity,
				newCompoundDocumentState,
				status);

			// Rechte für enthaltene Dokumente eintragen
			CompoundDocument compoundDocument = applicationContext.getBean(CompoundDocument.class);
			List<Document> inheritedDocuments = compoundDocument.getDocumentsFromCompoundDocument(
				compoundDocumentDomain.getCompoundDocumentId(),
				newCompoundDocumentState);

			addPermissionsForCompoundDocumentContainingDocuments(rbacPermissionService, inheritedDocuments);

			if (!status.isEmpty() && status.get(0) != ServiceState.OK) {
				// Something went wrong and we abort
				return;
			} else {
				status.clear();
			}
		}

		// save changes
		compoundDocumentRepository.save(compoundDocumentEntity);

		status.add(ServiceState.OK);
	}

	/**
	 * package-private for Testing
	 *
	 * @param compoundDocumentEntity   The CompoundDocumentDomain
	 * @param newCompoundDocumentState The new state from the compound document
	 * @param status                   A status field
	 * @return The CompoundDocumentDomain
	 */
	CompoundDocumentDomain changeDocumentStateByEditor(CompoundDocumentDomain compoundDocumentEntity,
		DocumentState newCompoundDocumentState, List<ServiceState> status) {
		List<Roles> rolesList = new ArrayList<>();
		rolesList.add(Roles.EDITOR);
		if (CompoundDocumentUtil.isUserCreaterOfCompoundDocument(compoundDocumentEntity, session.getUser())) {
			rolesList.add(Roles.ERSTELLER_DOKUMENTENMAPPE);
		}

		UUID rvReferenz = isStateChangeForCompoundDocumentPossible(compoundDocumentEntity, newCompoundDocumentState, rolesList,
			status);

		if (!status.get(0).equals(ServiceState.OK)) {
			return compoundDocumentEntity;
		}

		compoundDocumentEntity.setVerfahrensType(VerfahrensType.REGIERUNGSENTWURF);
		compoundDocumentEntity.setState(newCompoundDocumentState);
		return compoundDocumentEntity;
	}

	private List<DocumentId> getDocumentIds(List<Document> documentsForCompoundDocument) {
		return documentsForCompoundDocument.parallelStream().map(Document::getDocumentId).collect(Collectors.toList());
	}


	@Override
	public CompoundDocumentSummaryDTO copyCompoundDocument(CompoundDocumentId compoundDocumentId, String newTitle, List<DocumentId> elementIds,
		List<ServiceState> status) {

		// 1 - Find the compound document to be copied
		Optional<CompoundDocumentDomain> optionalSourceCompoundDocumentEntity = compoundDocumentRepository.findByCompoundDocumentId(compoundDocumentId);

		if (optionalSourceCompoundDocumentEntity.isEmpty()) {
			status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
			throw new DokumentenmappeNotFoundException("Dokumentenmappe with id '" + compoundDocumentId.getId() + "' not found");
		}

		CompoundDocumentDomain sourceCompoundDocumentEntity = optionalSourceCompoundDocumentEntity.get();

		String userId = session.getUser().getGid().getId();
		String ressort = editorRollenUndRechte.getRessortKurzbezeichnungForNutzer(userId);

		boolean isNotRessort = !Objects.equals(ressort, RESSORT_KURZBEZEICHNUNG_BT);
		boolean isUserAllowed = editorRollenUndRechte.isAllowed(userId, RECHT_NEUE_VERSION_ERSTELLEN, RESSOURCENTYP_DOKUMENTENMAPPE,
			sourceCompoundDocumentEntity.getCompoundDocumentId().getId().toString());
		boolean isDocumentCreatedByUser = sourceCompoundDocumentEntity.getCreatedBy().getGid().equals(session.getUser().getGid());

		if (isNotRessort && !(isUserAllowed || isDocumentCreatedByUser)) {
			status.add(ServiceState.NO_PERMISSION);
			throw new KeinRechteAnDerRessourceException(
				"Benutzer '" + userId + "' fehlen die Rechte an der Dokumentenmappe '" + compoundDocumentId.getId() + "'");
		}

		// I would prefer the JPA way (with entity manager detach and persist), but it doesn't work, so:
		// The version is incremented in makeCopyDocumentCopy

		// 2 - Create a new compound document, ALL documents are the same as from original
		CopyCompoundDocumentDTO copyCompoundDocumentDTO = makeModifiedCompoundDocumentCopy(sourceCompoundDocumentEntity, newTitle);
		// 3 - Copy selected documents
		filterAndCopyInheritedDocuments(copyCompoundDocumentDTO, elementIds);

		// 4 - Update some metadata
		CompoundDocumentDomain copy = compoundDocumentMapper.mapDtoToEntity(copyCompoundDocumentDTO);
		copy.setCreatedBy(session.getUser());
		copy.setUpdatedBy(session.getUser());
		copy.setCreatedAt(Instant.now());
		copy.setUpdatedAt(Instant.now());
		copy.setVerfahrensType(sourceCompoundDocumentEntity.getVerfahrensType());

		// Save compound document ...
		CompoundDocumentDomain save = compoundDocumentRepository.save(copy);

		// Copy Bestandsrecht
		copyAssignedBestandsrecht(sourceCompoundDocumentEntity.getCompoundDocumentId(), save.getCompoundDocumentId());

		// Rechte eintragen
		rbacPermissionService.addCreatorPermissionsForCompoundDocument(copyCompoundDocumentDTO);
		addPermissionsForCompoundDocumentContainingDocuments(rbacPermissionService, copyCompoundDocumentDTO.getDocuments());

		status.add(ServiceState.OK);
		return compoundDocumentMapper.map(copyCompoundDocumentDTO, save);
	}

	private void copyAssignedBestandsrecht(CompoundDocumentId source, CompoundDocumentId destination) {
		BestandsrechtService bestandsrechtService = applicationContext.getBean(BestandsrechtService.class);

		List<BestandsrechtListDTO> bestandsrechtForCompoundDocument = bestandsrechtService.getBestandsrechteFromRegelungsvorhabenEnhancedByCompoundDocumentAssignments(
			source,
			new ArrayList<>());

		List<BestandsrechtShortDTO> bestandsrechtShortDTOList = bestandsrechtForCompoundDocument.stream()
			.map(dto -> {
				// Ersetze alte DM-Verknüpfung mit Neuer
				if (dto.getVerknuepfteDokumentenMappeId() != null) {
					dto.setVerknuepfteDokumentenMappeId(destination.getId());
				}
				return (BestandsrechtShortDTO) dto;
			}).collect(Collectors.toList());

		bestandsrechtService.setBestandsrechteForCompoundDocumentByCompoundDocumentId(
			destination, bestandsrechtShortDTOList, new ArrayList<>());
	}

	/**
	 * Gets all compound documents on which the user has read or write permissions.
	 *
	 * @param status Success or failure description
	 * @return A list of found compound documents
	 */
	@Override
	public List<CompoundDocumentTitleDTO> getAllTitles(List<ServiceState> status) {

		final User user = session.getUser();

		// Retrieve Regelungsvorhaben Ids from RBAC

		TreeSet<UUID> rvIds = regelungsvorhabenService.getAllRegulatoryProposalsForUserHeIsAuthorOf()
			.stream()
			.map(RegelungsvorhabenEditorShortDTO::getId)
			.collect(Collectors.toCollection(TreeSet::new));

		List<CompoundDocumentDomain> compoundDocuments = compoundDocumentRepository.findByCreatedBy(user);
		final List<CompoundDocumentTitleDTO> results = new ArrayList<>();

		compoundDocuments.stream()
			.sorted(Comparator.comparing(CompoundDocumentDomain::getCreatedAt)
				.reversed())
			.forEach(cd -> {
				if (rvIds.contains(cd.getRegelungsVorhabenId()
					.getId())) {
					CompoundDocumentId compoundDocumentId = cd.getCompoundDocumentId();
					int numberOfAttachments =
						documentRepository.countByCompoundDocumentIdAndTypeIs(compoundDocumentId,
							DocumentType.ANLAGE);
					results.add(CompoundDocumentTitleDTO.builder()
						.id(compoundDocumentId.getId())
						.title(cd.getTitle())
						.type(cd.getType())
						.numberAttachments(numberOfAttachments)
						.build());
				}
			});

		status.add(ServiceState.OK);
		return results;
	}

	private CopyCompoundDocumentDTO makeModifiedCompoundDocumentCopy(CompoundDocumentDomain compoundDocumentEntity,
		String newTitle) {
		CopyCompoundDocumentDTO copyCompoundDocumentDTO = compoundDocumentMapper.mapEntityToDto(compoundDocumentEntity);

		// Store reference
		copyCompoundDocumentDTO.setInheritFromId(compoundDocumentEntity.getCompoundDocumentId());

		// Set a new document id
		copyCompoundDocumentDTO.setCompoundDocumentId(new CompoundDocumentId(UUID.randomUUID()));

		// Increase version
		try {
			setNewCompoundDocumentVersion(compoundDocumentEntity, copyCompoundDocumentDTO);
		} catch (NumberFormatException e) {
			log.error("Number from database could not be converted: ", e);
		}

		// Set to DRAFT
		copyCompoundDocumentDTO.setState(DocumentState.DRAFT);

		// Set new title
		String ressort = editorRollenUndRechte.getRessortKurzbezeichnungForNutzer(session.getUser().getGid().getId());
		if (ressort != null && ressort.equals(RESSORT_KURZBEZEICHNUNG_BT)) {
			copyCompoundDocumentDTO.setState(DocumentState.BEREIT_FUER_BUNDESTAG);
		}

		// Set new title
		copyCompoundDocumentDTO.setTitle(newTitle);

		// add documents
		CompoundDocument compoundDocument1 = applicationContext.getBean(CompoundDocument.class);
		List<Document> documentsFromCompoundDocument = compoundDocument1.getDocumentsFromCompoundDocument(compoundDocumentEntity.getCompoundDocumentId(), null);
		List<Document> documentList = documentsFromCompoundDocument.parallelStream()
			.peek(doc -> {
				DocumentDomain documentDomain = doc.getDocumentEntity();
				documentDomain.setCreatedAt(Instant.now());
				documentDomain.setUpdatedAt(Instant.now());
			}).collect(Collectors.toList());
		copyCompoundDocumentDTO.setDocuments(documentList);

		return copyCompoundDocumentDTO;
	}

	private void setNewCompoundDocumentVersion(CompoundDocumentDomain compoundDocumentEntity,
		CopyCompoundDocumentDTO copyCompoundDocumentDTO) throws NumberFormatException {

		String versionStringOfOldCompoundDocument = compoundDocumentEntity.getVersion();
		String[] splitedVersionString = versionStringOfOldCompoundDocument.split("\\.");

		// splitedVersionString.length == 1 --> we have version without a minor version
		int lastMajorVersion = Integer.decode(splitedVersionString[FIRST_LIST_ELEMENT]);
		int newMajorVersion = lastMajorVersion + MAX_POSSIBLE_LIST_ITEMS;
		int newMinorVersion = FIRST_LIST_ELEMENT;

		// Evtl. gibt es aber schon minor versionen der nächten major version.
		// Also suchen wir jetzt in der Datenbank
		List<CompoundDocumentDomain> versionList =
			compoundDocumentRepository.findByRegelungsVorhabenIdAndVersionLikeSorted(
				compoundDocumentEntity.getRegelungsVorhabenId(), (newMajorVersion + ".%"));

		// Tatsächlich gibt es (min. eine) Minor versionen, also müssen wir die Letzte herausfinden
		if (!versionList.isEmpty()) {
			// durch die Sortierung ist die oberste Version diejenige mit der höchsten minor version
			CompoundDocumentDomain latestCompoundDocument = versionList.get(FIRST_LIST_ELEMENT);
			String highestVersionOfSuccessor = latestCompoundDocument.getVersion();
			String[] splitedLastVersionString = highestVersionOfSuccessor.split("\\.");

			// splitedLastVersionString.length == 1 --> we have version without a minor version
			if (splitedLastVersionString.length > MAX_POSSIBLE_LIST_ITEMS) {
				Integer lastMinorVersion = Integer.decode(splitedLastVersionString[MAX_POSSIBLE_LIST_ITEMS]);
				newMinorVersion = lastMinorVersion + MAX_POSSIBLE_LIST_ITEMS;
			}

		}

		String versionOfCopy = newMajorVersion + "." + newMinorVersion;
		copyCompoundDocumentDTO.setVersion(versionOfCopy);
	}

	private void filterAndCopyInheritedDocuments(CopyCompoundDocumentDTO copyCompoundDocumentDTO,
		List<DocumentId> elementIds) {

		// filter documents to be copied - only these documents are valid for this compound document
		List<Document> documents = copyCompoundDocumentDTO.getDocuments();
		List<Document> documentsToBeCopied = documents.stream()
			.filter(document -> elementIds.contains(document.getDocumentEntity()
				.getDocumentId()))
			.collect(Collectors.toList());

		List<Document> copiedDocuments = Collections.emptyList();
		if (!documentsToBeCopied.isEmpty()) {
			// delegate the copy operation
			Document document = applicationContext.getBean(Document.class);
			CompoundDocumentId compoundDocumentId = copyCompoundDocumentDTO.getCompoundDocumentId();
			copiedDocuments = document.copyDocuments(compoundDocumentId, documentsToBeCopied,
				session.getUser());
		}

		copyCompoundDocumentDTO.setDocuments(copiedDocuments);
	}

	/**
	 * @param compoundDocumentId The UUID of the compound document
	 * @return A map
	 */
	public Map<String, Boolean> getAccessRightsForCompoundDocument(CompoundDocumentId compoundDocumentId) {
		Map<String, Boolean> accessRights = new HashMap<>();

		List<Rights> rechte = rbacPermissionService.getAccessRightsForCurrentUserOnGivenResource(RESSOURCENTYP_DOKUMENTENMAPPE,
			compoundDocumentId.getId());

		accessRights.put(RECHT_LESEN, rechte.contains(Rights.LESEN));
		accessRights.put(RECHT_SCHREIBEN, rechte.contains(Rights.SCHREIBEN));
		accessRights.put(RECHT_KOMMENTIEREN, rechte.contains(Rights.KOMMENTIEREN));
		accessRights.put(RECHT_KOMMENTARE_LESEN, rechte.contains(Rights.KOMMENTARE_LESEN));

		return accessRights;
	}

	public List<CompoundDocument> getNewestCompoundDocumentsForRegelungsvorhabenList(User user, List<RegelungsVorhabenId> regelungsVorhabenIds,
		int whichResult, AtomicInteger gesamtMenge) {

		List<CompoundDocumentDomain> compoundDocumentDomains = compoundDocumentRepository.findLatestCompoundDocumentForRegelungsvorhabenListAndCreatedBy(
			regelungsVorhabenIds, user);

		gesamtMenge.set(compoundDocumentDomains.size());

		compoundDocumentDomains.sort((dm2, dm1) ->
			dm1.getUpdatedAt().compareTo(dm2.getUpdatedAt())
		);

		if (!compoundDocumentDomains.isEmpty()) {

			CompoundDocumentDomain compoundDocumentDomain = compoundDocumentDomains.get(whichResult);
			CompoundDocument compoundDocumentBean = applicationContext.getBean(CompoundDocument.class);
			return List.of(compoundDocumentBean.getCompoundDocumentWithDocuments(compoundDocumentDomain));

		}

		return Collections.emptyList();
	}

	// --- Moved und must be redesigned

	public Page<CompoundDocumentDomain> findByRegelungsvorhabenIdInSortedByUpdatedAt(List<RegelungsVorhabenId> regelungsVorhabenIds, Pageable pageable) {
		return compoundDocumentRepository.findByRegelungsvorhabenIdInSortedByUpdatedAt(regelungsVorhabenIds, pageable);
	}

	public List<CompoundDocumentDomain> findByCompoundDocumentIdIn(List<CompoundDocumentId> compoundDocumentIds) {
		return compoundDocumentRepository.findByCompoundDocumentIdIn(compoundDocumentIds);
	}

	public List<CompoundDocumentDomain> findByCompoundDocumentIdInAndCompoundDocumentState(List<CompoundDocumentId> compoundDocumentIds,
		DocumentState filterDocumentState) {
		return compoundDocumentRepository.findByCompoundDocumentIdInAndCompoundDocumentState(compoundDocumentIds, filterDocumentState);
	}

	public List<CompoundDocumentDomain> findByCreatedByAndRegelungsVorhabenId(User currentUser, RegelungsVorhabenId regelungsVorhabenId) {
		return compoundDocumentRepository.findByCreatedByAndRegelungsVorhabenId(currentUser, regelungsVorhabenId);
	}

	public Optional<CompoundDocumentDomain> findByCompoundDocumentId(CompoundDocumentId id) {
		return compoundDocumentRepository.findByCompoundDocumentId(id);
	}

	public List<CompoundDocumentDomain> findByStateSortedByRegelungsVorhabenId(DocumentState state) {
		return compoundDocumentRepository.findByStateSortedByRegelungsVorhabenId(state);
	}
}
