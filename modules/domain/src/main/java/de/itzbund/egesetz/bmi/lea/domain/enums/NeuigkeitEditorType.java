// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(enumAsRef = true)
public enum NeuigkeitEditorType {
    DOKUMENTMAPPE_SCHREIBEN_ERTEILT,
    DOKUMENTMAPPE_LESEN_ERTEILT,
    DOKUMENTMAPPE_SCHREIBEN_ENTZOGEN,
    DOKUMENTMAPPE_LESEN_ENTZOGEN
}
