// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@Getter
public enum RechtsetzungsdokumentBearbeitendeInstitution {

    BUNDESREGIERUNG("Bundesregierung", "bundesregierung"),
    BUNDESTAG("Bundestag", "bundestag"),
    BUNDESRAT("Bundesrat", "bundesrat"),
    BUNDESPRAESIDENT("Bundespräsident", "bundespräsident"),
    BUNDESKANZLER("Bundeskanzler", "bundeskanzler"),
    NICHT_VORHANDEN("Nicht vorhanden", "nicht-vorhanden");

    private static final Map<String, RechtsetzungsdokumentBearbeitendeInstitution> LITERALS = new HashMap<>();

    static {
        Arrays.stream(values()).forEach(form -> LITERALS.put(form.getLiteral(), form));
    }

    private final String label;
    private final String literal;


    public static RechtsetzungsdokumentBearbeitendeInstitution fromLiteral(String literal) {
        if (Utils.isMissing(literal)) {
            return null;
        }

        return LITERALS.get(literal);
    }

}
