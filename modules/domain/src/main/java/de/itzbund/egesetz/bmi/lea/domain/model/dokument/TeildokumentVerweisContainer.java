// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model.dokument;

import de.itzbund.egesetz.bmi.lea.core.Constants;
import de.itzbund.egesetz.bmi.lea.domain.model.version.LegalDocMLDeVersion;
import de.itzbund.egesetz.bmi.lea.domain.ports.eid.EId;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.EIdImpl;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.List;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.core.Constants.DEFAULT_UUID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_CHILDREN;

/**
 * Container für den Verweis auf ein Teildokument eines Rechtsetzungsdokuments.
 */
@Getter
@EqualsAndHashCode(callSuper = true)
public class TeildokumentVerweisContainer extends ModellElement {

    private final UUID guid;
    @Setter
    private EId eId;
    @Setter
    private TeildokumentVerweis teildokumentVerweis;


    public TeildokumentVerweisContainer(UUID guid) {
        super(List.of(LegalDocMLDeVersion.LDML_DE_V1_04), ModellElementTyp.TEILDOKUMENT_VERWEIS_CONTAINER);
        this.guid = guid;
    }


    public static TeildokumentVerweisContainer createFromJSON(@NonNull JSONObject jsonObject) {
        return (new TeildokumentVerweisContainer(DEFAULT_UUID)).fromJSON(jsonObject);
    }


    @Override
    @SuppressWarnings("java:S1541")
    public TeildokumentVerweisContainer fromJSON(@NonNull JSONObject jsonObject) {
        if (jsonObject.isEmpty() || !typ.getXmlType()
            .equals(jsonObject.get(Constants.JSON_KEY_TYPE))) {
            return null;
        }

        String value = (String) jsonObject.get(Constants.JSON_KEY_ID_GUID);
        UUID guid1 = value == null || value.isBlank() ? null : UUID.fromString(value);
        if (guid1 == null) {
            return null;
        }

        TeildokumentVerweisContainer container = new TeildokumentVerweisContainer(guid1);

        value = (String) jsonObject.get(Constants.JSON_KEY_ID_EID);
        EId eId1 = value == null || value.isBlank() ? null : EIdImpl.fromString(value);
        if (eId1 != null) {
            container.setEId(eId1);
        }

        JSONArray children = (JSONArray) jsonObject.get(JSON_KEY_CHILDREN);
        if (children == null || children.isEmpty()) {
            // a container must include exactly one verweis
            return null;
        }

        JSONObject child = (JSONObject) children.get(0);
        TeildokumentVerweis verweis = TeildokumentVerweis.createFromJSON(child);
        if (verweis == null) {
            return null;
        } else {
            container.setTeildokumentVerweis(verweis);
        }

        return container;
    }


    @Override
    public JSONObject toJSONObject() {
        if (teildokumentVerweis != null) {
            JSONObject jsonObject = new JSONObject();
            addJSONAttribute(jsonObject, Constants.JSON_KEY_TYPE, typ.getXmlType());
            addJSONAttribute(jsonObject, Constants.JSON_KEY_ID_GUID, guid.toString());
            addJSONAttribute(jsonObject, Constants.JSON_KEY_ID_EID, eId == null ? null : eId.toString());

            addChild(jsonObject, teildokumentVerweis.toJSONObject());

            return jsonObject;
        }

        return new JSONObject();
    }

}
