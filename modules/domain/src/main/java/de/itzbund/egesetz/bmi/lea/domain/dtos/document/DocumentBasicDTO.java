// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.document;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.PermissionDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.Valid;
import java.util.UUID;

@Data
@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public abstract class DocumentBasicDTO extends DocumentAllDTO {

	@Valid
	@JsonProperty("id")
	@Schema(name = "id", example = "00000000-0000-0000-0003-000000000005", required = true)
	protected UUID id;

	@JsonProperty("state")
	@Schema(name = "state", example = "DRAFT", required = true)
	protected DocumentState state;

	@Schema(name = "version", example = "V1", required = true)
	protected String version;

	@Schema(name = "documentPermissions")
	@EqualsAndHashCode.Exclude
	protected PermissionDTO documentPermissions;

	@Schema(name = "commentPermissions")
	@EqualsAndHashCode.Exclude
	protected PermissionDTO commentPermissions;
}
