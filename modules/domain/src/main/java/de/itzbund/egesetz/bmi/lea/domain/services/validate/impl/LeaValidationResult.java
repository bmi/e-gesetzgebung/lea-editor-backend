// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.impl;

import de.itzbund.egesetz.bmi.lea.domain.services.validate.ValidationResult;

/**
 * Default values for validation outcomes. Can be extended by another implementation of the interface {@link ValidationResult}.
 */
public enum LeaValidationResult implements ValidationResult {

    /**
     * validation was successful
     */
    VALIDATION_SUCCESS,

    /**
     * validation was unsuccessful
     */
    VALIDATION_FAILURE,

    /**
     * validation produced a technical error which is not a validation error
     */
    VALIDATION_ERROR,

    /**
     * validation was not performed so far
     */
    VALIDATION_NOT_PERFORMED

}
