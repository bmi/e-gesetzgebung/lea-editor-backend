// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.filters;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.enums.ChangeTrackingType;
import lombok.NonNull;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.CHANGE_TRACK_ATTR_TYPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BLOCK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CAPTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DATE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DOCPROPONENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DOCSTAGE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DOCTITLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_HEADING;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_INLINE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LI;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LISTINTRODUCTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LISTWRAPUP;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LOCATION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_NUM;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_P;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_REF;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ROLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SHORTTITLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SPAN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TOCITEM;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_CHANGE_WRAPPER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_COMMENT_WRAPPER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_TEXT_WRAPPER;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getLocalName;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getStringAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getTextWrapper;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.replaceChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.toJSONArray;

@Component
public class ChangeAnnotationFilter implements XmlFilter {

    private static final Set<String> MIXED_CONTENT_ELEMENTS =
        Stream.of(
            ELEM_BLOCK,
            /*ELEM_SIGNATURE,*/
            /*ELEM_A,*/
            /*ELEM_ABBR,*/
            /*ELEM_MOD,*/
            /*ELEM_QUOTEDTEXT,*/
            ELEM_NUM,
            /*ELEM_B,*/
            /*ELEM_AFFECTEDDOCUMENT,*/
            /*ELEM_RELATEDDOCUMENT,*/
            ELEM_CAPTION,
            ELEM_DATE,
            ELEM_DOCSTAGE,
            ELEM_DOCTITLE,
            /*ELEM_DOCTYPE,*/
            /*ELEM_DOCNUMBER,*/
            ELEM_P,
            ELEM_ROLE,
            /*ELEM_I,*/
            ELEM_TOCITEM,
            ELEM_DOCPROPONENT,
            ELEM_INLINE,
            ELEM_SHORTTITLE,
            ELEM_LI,
            ELEM_LISTINTRODUCTION,
            ELEM_LISTWRAPUP,
            /*ELEM_ORGANIZATION,*/
            ELEM_LOCATION,
            /*ELEM_PERSON,*/
            ELEM_REF,
            /*ELEM_SESSION,*/
            ELEM_SPAN,
            /*ELEM_SUB,*/
            /*ELEM_SUP,*/
            /*ELEM_U,*/
            ELEM_HEADING
        ).collect(Collectors.toCollection(HashSet::new));

    private StringBuilder stringBuilder;

    @Override
    public void doFilter(JSONObject jsonObject, FilterChain chain) {
        clearAnnotations(jsonObject);
        chain.doFilter(jsonObject);
    }

    @SuppressWarnings("unchecked")
    private void clearAnnotations(@NonNull JSONObject jsonObject) {
        JSONArray children = getChildren(jsonObject);
        if (children == null) {
            return;
        }
        if (hasMixedContent(jsonObject)) {
            List<JSONObject> cleanedChildren = new ArrayList<>();
            stringBuilder = new StringBuilder();

            children.forEach(object -> {
                JSONObject child = (JSONObject) object;
                handleMixedContentChild(child, cleanedChildren);
            });

            if (stringBuilder.length() > 0) {
                cleanedChildren.add(getTextWrapper(stringBuilder.toString()));
            }

            if (cleanedChildren.isEmpty()) {
                cleanedChildren.add(getTextWrapper(""));
            }
            replaceChildren(jsonObject, toJSONArray(cleanedChildren));
        } else {
            children.forEach(object -> {
                JSONObject child = (JSONObject) object;
                clearAnnotations(child);
            });
        }
    }

    private void handleMixedContentChild(JSONObject jsonObject, List<JSONObject> cleanedChildren) {
        String type = getType(jsonObject);
        String changeType = getStringAttribute(jsonObject, CHANGE_TRACK_ATTR_TYPE);

        if (JSON_VAL_TEXT_WRAPPER.equals(type)) {
            stringBuilder.append(getText(jsonObject));
        } else if (JSON_VAL_CHANGE_WRAPPER.equals(type)) {
            if (!ChangeTrackingType.DELETED.getLiteral().equals(changeType)) {
                stringBuilder.append(getText(jsonObject));
            }
        } else if (JSON_VAL_COMMENT_WRAPPER.equals(type)) {
            // do nothing, filter out comment markers
        } else {
            // found inline element
            if (stringBuilder.length() > 0) {
                cleanedChildren.add(getTextWrapper(stringBuilder.toString()));
                stringBuilder.setLength(0);
            }
            clearAnnotations(jsonObject);
            cleanedChildren.add(jsonObject);
            stringBuilder.setLength(0);
        }
    }

    private boolean hasMixedContent(JSONObject jsonObject) {
        String type = getLocalName(getType(jsonObject));
        return !Utils.isMissing(type) && MIXED_CONTENT_ELEMENTS.contains(type);
    }

}
