// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.impl;

import de.itzbund.egesetz.bmi.lea.core.util.FlatGroupedList;
import de.itzbund.egesetz.bmi.lea.core.util.ListGroup;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.ValidationTask;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A {@link Validator} is used to validate a LegalDocML Document. An instance of this class is initialized by adding {@link ValidationTask}s to it. Then it can
 * start the validation. The validation results are stored in a {@link ValidationData} object.
 */

@Log4j2
@Component
@Scope("prototype")
@SuppressWarnings("unused")
public class Validator {

    @Getter
    private List<ValidationTask> workflow = new ArrayList<>();

    private Set<ValidationTask> tasks = new HashSet<>();

    @Getter
    @Autowired
    private ValidationData validationData;


    /**
     * Starts the validation by iterating over the {@link ValidationTask}s added to this validator. The series of validation tasks is calculated from the
     * logical dependency structure given by the predecessors of each task.
     */
    public void validate() {
        validationData.reset();
        assembleWorkflow();

        for (ValidationTask task : workflow) {
            if (task != null) {
                try {
                    task.run(validationData);
                } catch (Exception e) {
                    log.error(e);
                    String text = e.getCause() == null ? e.getMessage() : e.getCause().getMessage();
                    ValidationMessage msg = new ValidationMessage(task, SeverityLevel.ERROR, text);
                    validationData.addMessage(msg);
                }
            }
        }

        // log validation messages
        if (!isSuccess()) {
            log.error("Validation errors occurred.");

            for (ValidationTask task : workflow) {
                processTask(task);
            }
        }
    }


    private void processTask(ValidationTask task) {
        Map<SeverityLevel, Collection<ValidationMessage>> map = validationData
            .getMessages(task);

        if (map != null) {
            for (SeverityLevel level : SeverityLevel.values()) {
                Collection<ValidationMessage> messages = map.get(level);

                if (messages != null) {
                    processMessages(messages, level, task.getName());
                }
            }
        }
    }


    private void processMessages(Collection<ValidationMessage> messages, SeverityLevel level, String taskname) {
        for (ValidationMessage msg : messages) {
            String text = msg.getMessage();

            log.debug(String.format("Task '%s' [%s] :: %s", taskname, level, text));
        }
    }


    private void assembleWorkflow() {
        workflow = new ArrayList<>();
        FlatGroupedList<ValidationTask> groups = new FlatGroupedList<>();
        ListGroup<ValidationTask> startGroup = new ListGroup<>("G0");
        List<ValidationTask> tmp = new ArrayList<>();
        List<ValidationTask> del;

        // find all tasks that have no predecessors and use them as starting points
        for (ValidationTask task : tasks) {
            if (task.getPredecessors().isEmpty()) {
                startGroup.add(task);
            } else {
                tmp.add(task);
            }
        }

        // there must be some task that has no predecessors
        assert !startGroup.isEmpty();

        // starting tasks are all in the first group
        groups.addGroup(startGroup);

        int count = 1;
        while (!tmp.isEmpty()) {
            count++;
            ListGroup<ValidationTask> newGroup = new ListGroup<>("G" + count);
            del = new ArrayList<>();
            for (ValidationTask task : tmp) {
                if (groups.containsAll(task.getPredecessors())) {
                    newGroup.add(task);
                    del.add(task);
                }
            }
            groups.addGroup(newGroup);

            for (ValidationTask task : del) {
                tmp.remove(task);
            }
        }

        workflow = groups.getFlatList();
    }


    public void addValidationTask(ValidationTask task) {
        tasks.add(task);
    }


    public void clearTasks() {
        tasks = new HashSet<>();
    }


    public boolean isSuccess() {
        return validationData.getErrorCount() == 0;
    }

}
