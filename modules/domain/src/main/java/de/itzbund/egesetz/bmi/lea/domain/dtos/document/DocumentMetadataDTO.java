// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.document;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import java.util.UUID;

/**
 * Data transfer object comprising all metadata of a document but no content.
 */
@Data
@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DocumentMetadataDTO extends DocumentBasicDTO {

    @JsonProperty("compoundDocumentId")
    protected UUID compoundDocumentId;

    @JsonProperty("proposition")
    @Schema(name = "proposition")
    protected PropositionDTO proposition;

    @JsonProperty("createdBy")
    @Valid
    @Schema(name = "createdBy")
    protected UserDTO createdBy;

    @JsonProperty("updatedBy")
    @Valid
    @Schema(name = "updatedBy")
    protected UserDTO updatedBy;

    @Valid
    @JsonProperty("createdAt")
    @Schema(name = "createdAt", example = "19.02.2022", required = true)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    protected String createdAt;

    @Valid
    @JsonProperty("updatedAt")
    @Schema(name = "updatedAt", example = "19.02.2022", required = true)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    protected String updatedAt;

}
