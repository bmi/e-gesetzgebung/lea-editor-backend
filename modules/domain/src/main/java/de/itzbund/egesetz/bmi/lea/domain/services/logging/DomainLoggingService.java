// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.DomainLoggingRestPort;

@Service
public class DomainLoggingService implements DomainLoggingRestPort {

	private static final String LOGGER_NAME = "BusinessLogger";
	private static final Logger LOGGER = LoggerFactory.getLogger(LOGGER_NAME);

	public void logActivity(LogActivity activity, String userId) {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("[{}]: {}", userId, activity.getText());
		}
	}

}
