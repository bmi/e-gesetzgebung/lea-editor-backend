// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben;

import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentInitiant;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * represents Regelungsvorhaben with very basic information for Editor
 */
@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class RegelungsvorhabenEditorDTO extends RegelungsvorhabenEditorShortDTO {

    @NotBlank
    @Schema(example = "RV: Bezeichnung")
    private String bezeichnung;

    @NotNull
    private RessortEditorDTO technischesFfRessort;

    @NotNull
    private List<String> allFfRessorts;

    @NotNull
    @Schema(example = "BUNDESREGIERUNG")
    private RechtsetzungsdokumentInitiant initiant;

}
