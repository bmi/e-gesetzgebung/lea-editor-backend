// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde;

import de.itzbund.egesetz.bmi.lea.domain.model.ldmlde.LDMLDeModellElementBezeichnung;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum InhaltsabschnittRefersToLiteral implements RefersToLiteral {

    REGELUNGSFOLGEN_ABSCHNITT_RECHTS_UND_VERWALTUNGSVEREINFACHUNG(
        "regelungsfolgen-abschnitt-rechts-und-verwaltungsvereinfachung"),
    REGELUNGSFOLGEN_ABSCHNITT_NACHHALTIGKEITSASPEKTE("regelungsfolgen-abschnitt-nachhaltigkeitsaspekte"),
    REGELUNGSFOLGEN_ABSCHNITT_ERFUELLUNGSAUFWAND("regelungsfolgen-abschnitt-erfuellungsaufwand"),
    REGELUNGSFOLGEN_ABSCHNITT_WEITERE_KOSTEN("regelungsfolgen-abschnitt-weitere-kosten"),
    REGELUNGSFOLGEN_ABSCHNITT_GLEICHSTELLUNGSPOLITISCHE_RELEVANZPRUEFUNG(
        "regelungsfolgen-abschnitt-gleichstellungspolitische-relevanzpruefung"),
    REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND(
        "regelungsfolgen-abschnitt-haushaltsausgaben-ohne-erfuellungsaufwand"),
    REGELUNGSFOLGEN_ABSCHNITT_WEITERE_REGELUNGSFOLGEN("regelungsfolgen-abschnitt-weitere-regelungsfolgen"),
    BEGRUENDUNG_ERFUELLUNGSAUFWAND_FUER_BUERGERINNEN_UND_BUERGER(
        "begruendung-erfuellungsaufwand-fuer-buergerinnen-und-buerger"),
    BEGRUENDUNG_ERFUELLUNGSAUFWAND_FUER_DIE_WIRTSCHAFT("begruendung-erfuellungsaufwand-fuer-die-wirtschaft"),
    BEGRUENDUNG_ERFUELLUNGSAUFWAND_DER_VERWALTUNG("begruendung-erfuellungsaufwand-der-verwaltung"),
    ERFUELLUNGSAUFWAND_FUER_BUERGERINNEN_UND_BUERGER("erfuellungsaufwand-fuer-buergerinnen-und-buerger"),
    ERFUELLUNGSAUFWAND_FUER_DIE_WIRTSCHAFT("erfuellungsaufwand-fuer-die-wirtschaft"),
    DAVON_BUEROKRATIEKOSTEN_AUS_INFORMATIONSPFLICHTEN("davon-buerokratiekosten-aus-informationspflichten"),
    ERFUELLUNGSAUFWAND_DER_VERWALTUNG("erfuellungsaufwand-der-verwaltung");

    private static final Map<String, InhaltsabschnittRefersToLiteral> literals;

    static {
        literals = new HashMap<>();
        Arrays.stream(values()).forEach(v -> literals.put(v.getLiteral(), v));
    }

    private final String literal;


    @Override
    public LDMLDeModellElementBezeichnung getContext() {
        return LDMLDeModellElementBezeichnung.INHALTSABSCHNITT;
    }


    public static InhaltsabschnittRefersToLiteral fromLiteral(String literal) {
        return literals.get(literal);
    }

}
