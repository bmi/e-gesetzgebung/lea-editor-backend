// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;

@SuppressWarnings("unused")
public interface CompoundDocumentPersistencePort {


	List<CompoundDocumentDomain> findByCreatedBy(User userEntity);

	List<CompoundDocumentDomain> findByCreatedByAndRegelungsVorhabenId(User currentUser, RegelungsVorhabenId regelungsVorhabenId);

	List<CompoundDocumentDomain> findByRegelungsvorhabenIdInSortedByUpdatedAt(User userEntity, List<RegelungsVorhabenId> ids);

	Page<CompoundDocumentDomain> findByRegelungsvorhabenIdInSortedByUpdatedAt(List<RegelungsVorhabenId> ids, Pageable pageable);

	List<CompoundDocumentDomain> findByCreatedBySortedByUpdatedAtRegelungsVorhabenIdNotIn(User userEntity, List<RegelungsVorhabenId> ids);

	Page<CompoundDocumentDomain> findByCreatedBySortedByUpdatedAt(User userEntity, Pageable pageable);

	Optional<CompoundDocumentDomain> findByCreatedByAndCompoundDocumentId(User userEntity, CompoundDocumentId compoundDocumentId);

	List<CompoundDocumentDomain> findByCreatedByAndRegelungsvorhabenIdAndState(User entity, RegelungsVorhabenId regelungsVorhabenId,
		DocumentState state);

	// --------------------------------------------------

	Optional<CompoundDocumentDomain> findByCompoundDocumentId(CompoundDocumentId id);

	/**
	 * Return the newest compound document of a Regelungsvorhaben in the list and only one and only for the given user.
	 *
	 * @param regelungsVorhabenIds A list of Regelungsvorhaben
	 * @param user                 The user
	 * @return A list of compound documents
	 */
	List<CompoundDocumentDomain> findLatestCompoundDocumentForRegelungsvorhabenListAndCreatedBy(List<RegelungsVorhabenId> regelungsVorhabenIds, User user);

	List<CompoundDocumentDomain> findByCompoundDocumentIdIn(List<CompoundDocumentId> compoundDocumentIds);

	List<CompoundDocumentDomain> findByCompoundDocumentIdInAndCompoundDocumentState(
		List<CompoundDocumentId> compoundDocumentIds, DocumentState state);

	List<CompoundDocumentDomain> findByStateSortedByRegelungsVorhabenId(DocumentState state);

	List<CompoundDocumentDomain> findAll();

	CompoundDocumentDomain save(CompoundDocumentDomain compoundDocumentEntity);

	// --------------------------------------------------

	List<CompoundDocumentDomain> findByRegelungsVorhabenIdSortedByVersion(
		RegelungsVorhabenId regelungsVorhabenId);

	List<CompoundDocumentDomain> findByRegelungsVorhabenIdAndVersionLikeSorted(
		RegelungsVorhabenId regelungsVorhabenId, String version);

	List<CompoundDocumentDomain> findByRegelungsVorhabenIdAndCompoundDocumentState(
		RegelungsVorhabenId regelungsVorhabenId, DocumentState state);

	boolean existsCompoundDocumentsCreatedOrUpdatedByUser(UserId userId);
}
