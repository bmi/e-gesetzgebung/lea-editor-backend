// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.transform;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Set;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;
import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import lombok.extern.log4j.Log4j2;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_GUID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_TYPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ACT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_AKOMANTOSO;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BILL;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DOC;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_CHILDREN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_ID_GUID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_TEXT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_TYPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_XML_MODEL;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_XML_STYLESHEET;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_TEXT_WRAPPER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.LEA_PREFIX;
import static de.itzbund.egesetz.bmi.lea.core.Constants.NS_LEGALDOCML;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.withDefaultPrefix;

/**
 * A transformer that converts JSON documents from LEA frontend into LegalDocML.de compliant XML documents.
 */
@Log4j2
@Service
@SuppressWarnings("unchecked")
public class JsonToXmlTransformer {

	private static final String AKN_ATTR_TYPE = withDefaultPrefix(ATTR_TYPE);

	private final JSONParser jsonParser = new JSONParser();

	private final Set<String> noGUIDSet = Set.of(
		withDefaultPrefix(ELEM_AKOMANTOSO),
		withDefaultPrefix(ELEM_DOC),
		withDefaultPrefix(ELEM_BILL),
		withDefaultPrefix(ELEM_ACT)
	);

	private XMLStreamWriter writer = null;


	@SuppressWarnings("java:S1141")
	public ReusableInputSource transform(String jsonString) {
		try {
			JSONObject documentAsJson = parse(jsonString);
			if (documentAsJson == null) {
				return null;
			}

			try (StringWriter stringWriter = new StringWriter()) {
				traverse(documentAsJson, stringWriter);
				// convert content into result type
				return new ReusableInputSource(
					new ByteArrayInputStream(
						stringWriter.toString()
							.getBytes(StandardCharsets.UTF_8)));
			} catch (Exception e) {
				log.error(e);
			} finally {
				cleanUp();
			}
		} catch (NullPointerException e) {
			log.error(e);
		}
		return null;
	}


	private void traverse(JSONObject documentAsJson, StringWriter stringWriter) throws XMLStreamException {
		// prepare writer
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		writer = factory.createXMLStreamWriter(stringWriter);

		writer.writeStartDocument(StandardCharsets.UTF_8.name(), "1.0");

		if (documentAsJson.containsKey(JSON_KEY_XML_STYLESHEET)) {
			writer.writeProcessingInstruction(JSON_KEY_XML_STYLESHEET,
				(String) documentAsJson.get(JSON_KEY_XML_STYLESHEET));
		}
		if (documentAsJson.containsKey(JSON_KEY_XML_MODEL)) {
			writer.writeProcessingInstruction(JSON_KEY_XML_MODEL,
				(String) documentAsJson.get(JSON_KEY_XML_MODEL));
		}

		// write root start element
		writer.setDefaultNamespace(NS_LEGALDOCML);
		writer.writeStartElement((String) documentAsJson.get(JSON_KEY_TYPE));

		JSONArray children = (JSONArray) documentAsJson.get(JSON_KEY_CHILDREN);

		documentAsJson.remove(JSON_KEY_XML_MODEL);
		documentAsJson.remove(JSON_KEY_XML_STYLESHEET);
		documentAsJson.remove(JSON_KEY_TYPE);
		documentAsJson.remove(JSON_KEY_CHILDREN);
		documentAsJson.remove(ATTR_GUID);

		writeAttributes(documentAsJson);

		writeChildren(children);

		// close root element
		writer.writeEndElement();
		writer.writeEndDocument();
	}


	private JSONObject parse(String jsonString) {
		try {
			return (JSONObject) jsonParser.parse(jsonString);
		} catch (ParseException e) {
			log.error(e);
			return null; //NOSONAR
		}
	}


	private void cleanUp() {
		try {
			if (writer != null) {
				writer.flush();
				writer.close();
			}
		} catch (Exception ee) {
			log.error(ee);
		}
	}


	private void writeChildren(JSONArray children) {
		try {
			children.forEach(child -> {
				JSONObject json = (JSONObject) child;
				if (json.containsKey(JSON_KEY_TYPE) && JSON_VAL_TEXT_WRAPPER.equals(json.get(JSON_KEY_TYPE))) {
					handleText(json);
				} else if (((JSONObject) ((JSONArray) json.get(
					JSON_KEY_CHILDREN)).get(0)).size() == 1 && "".equals(((JSONObject) ((JSONArray) json.get(
					JSON_KEY_CHILDREN)).get(0)).get(JSON_KEY_TEXT))) {
					handleEmptyElement(json);
				} else {
					try {
						writeElement(json);
					} catch (XMLStreamException e) {
						log.error(e);
					}
				}

			});
		} catch (NullPointerException e) {
			log.error(e);
		}

	}


	private void handleText(JSONObject json) {
		JSONArray children = (JSONArray) json.get(JSON_KEY_CHILDREN);
		JSONObject textChild = (JSONObject) children.get(0);

		String text = (String) textChild.get(JSON_KEY_TEXT);
		try {
			writeText(text);
		} catch (XMLStreamException e) {
			log.error(e);
		}
	}


	private void handleEmptyElement(JSONObject json) {
		try {
			writeElement(json);
		} catch (XMLStreamException e) {
			log.error(e);
		}
	}


	private void writeElement(JSONObject element) throws XMLStreamException {
		try {
			writer.writeStartElement((String) element.get(JSON_KEY_TYPE));
			JSONArray children = (JSONArray) element.get(JSON_KEY_CHILDREN);

			if (noGUIDSet.contains((String) element.get(JSON_KEY_TYPE))) {
				element.remove(JSON_KEY_ID_GUID);
			}
			element.remove(JSON_KEY_TYPE);
			element.remove(JSON_KEY_CHILDREN);

			if (!element.isEmpty()) {
				writeAttributes(element);
			}
			writeChildren(children);
			writer.writeEndElement();
		} catch (NullPointerException e) {
			log.error(e);
		}
	}


	private void writeText(String text) throws XMLStreamException {
		try {
			writer.writeCharacters(text);
		} catch (NullPointerException e) {
			log.error(e);
		}
	}


	private void writeAttributes(JSONObject attributes) {
		try {
			attributes.forEach((key, value) -> {
				try {

					final String keyString = (String) key;
					if (!keyString.startsWith(LEA_PREFIX)) {
						final String stringValue = valueToString(value);
						writeAttr(keyString, stringValue);
					}

				} catch (XMLStreamException e) {
					log.error(e);
				}
			});
		} catch (NullPointerException e) {
			log.error(e);
		}
	}


	private String valueToString(final Object object) {
		try {

			return (String) object;

		} catch (ClassCastException e) {
			if (object instanceof Long) {
				return String.valueOf(object);
			} else {
				throw e;
			}
		}
	}


	private void writeAttr(String key, String val) throws XMLStreamException {
		if (AKN_ATTR_TYPE.equals(key)) {
			writer.writeAttribute(ATTR_TYPE, val);
		} else {
			writer.writeAttribute(key, val);
		}

	}

}
