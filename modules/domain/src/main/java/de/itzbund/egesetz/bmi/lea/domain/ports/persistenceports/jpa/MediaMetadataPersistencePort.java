// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa;

import de.itzbund.egesetz.bmi.lea.domain.model.MediaData;
import de.itzbund.egesetz.bmi.lea.domain.model.MediaMetadata;

import java.util.Optional;
import java.util.UUID;

public interface MediaMetadataPersistencePort {

    MediaMetadata saveAndFlush(MediaMetadata metaEntity);

    Optional<MediaMetadata> findById(UUID id);

    void deleteById(UUID id);

    boolean checkForeignKeyHasRelation(MediaData mediaData);
}
