// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.EditorRollenUndRechte;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.mappers.BaseMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CompoundDocumentPermissionRestPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Log4j2
@SuppressWarnings("unused")
public class CompoundDocumentPermissionsService implements CompoundDocumentPermissionRestPort {

    @Autowired
    private EditorRollenUndRechte editorRollenUndRechte;

    @Autowired
    private UserService userService;

    @Autowired
    private BaseMapper baseMapper;


    @Override
    public List<UserDTO> getPermissionsByRessourceIdAndAktion(UUID ressourcenId, String aktion,
        List<ServiceState> status) {
        try {
            Set<String> userIdsWithPermission =
                editorRollenUndRechte.getAlleBenutzerIdsByRessourceIdAndAktion(String.valueOf(ressourcenId),
                    aktion);
            status.add(ServiceState.OK);
            return userIdsWithPermission.stream()
                .map(x -> userToUserDTO(userService.getUserForGid(x)))
                .collect(Collectors.toList());
        } catch (IllegalArgumentException e) {
            status.add(ServiceState.INVALID_ARGUMENTS);
            log.error(e);
        }
        return Collections.emptyList();
    }


    protected UserDTO userToUserDTO(User user) {
        if (user == null) {
            return null;
        }

        UserDTO userDTO = new UserDTO();

        userDTO.setGid(baseMapper.mapFromUserId(user.getGid()));
        userDTO.setName(user.getName());
        userDTO.setEmail(user.getEmail());

        return userDTO;
    }
}
