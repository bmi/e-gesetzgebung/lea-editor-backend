// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum CompoundDocumentTypeVariant {

    AENDERUNGSVERORDNUNG("Änderungsverordnung"),
    MANTELGESETZ("Mantelgesetz"),
    STAMMGESETZ("Stammgesetz"),
    STAMMVERORDNUNG("Stammverordnung");

    private final String compoundDocumentVariant;

}
