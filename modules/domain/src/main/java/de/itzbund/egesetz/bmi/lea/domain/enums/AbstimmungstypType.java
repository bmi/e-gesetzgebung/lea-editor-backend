// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Typ der Abstimmung, bspw. Haus- oder Ressortabstimmung
 */
@SuppressWarnings("unused")
public enum AbstimmungstypType {
    RESSORTABSTIMMUNG("Ressortabstimmung"),
    HAUSABSTIMMUNG("Hausabstimmung"),
    UNTERABSTIMMUNG("Unterabstimmung"),
    SONSTIGE_ABSTIMMUNG("Sonstige Abstimmung");

    private final String value;


    AbstimmungstypType(String value) {
        this.value = value;
    }


    @JsonCreator
    public static AbstimmungstypType fromValue(String text) {
        for (AbstimmungstypType b : AbstimmungstypType.values()) {
            if (String.valueOf(b.value)
                .equals(text)) {
                return b;
            }
        }
        return null;
    }


    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }
}
