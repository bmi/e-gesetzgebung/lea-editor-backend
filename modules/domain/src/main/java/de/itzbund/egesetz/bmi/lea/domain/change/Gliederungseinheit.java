// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.change;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.change.linguistics.Genus;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static de.itzbund.egesetz.bmi.lea.domain.change.linguistics.Genus.FEMININUM;
import static de.itzbund.egesetz.bmi.lea.domain.change.linguistics.Genus.MASKULINUM;
import static de.itzbund.egesetz.bmi.lea.domain.change.linguistics.Genus.NEUTRUM;

@AllArgsConstructor
@Getter
@SuppressWarnings("java:S1192")
public enum Gliederungseinheit {

    GE_BUCH(NEUTRUM, "Buch", "Bücher"),
    GE_TEIL(MASKULINUM, "Teil", "Teile"),
    GE_KAPITEL(NEUTRUM, "Kapitel", "Kapitel"),
    GE_UNTERKAPITEL(NEUTRUM, "Unterkapitel", "Unterkapitel"),
    GE_ABSCHNITT(MASKULINUM, "Abschnitt", "Abschnitte"),
    GE_UNTERABSCHNITT(MASKULINUM, "Unterabschnitt", "Unterabschnitte"),
    GE_TITEL(MASKULINUM, "Titel", "Titel"),
    GE_UNTERTITEL(MASKULINUM, "Untertitel", "Untertitel"),
    GE_ARTIKEL(MASKULINUM, "Artikel", "Artikel"),
    GE_PARAGRAPH(MASKULINUM, "§", "§§"),
    GE_ABSATZ(MASKULINUM, "Absatz", "Absätze"),
    GE_NUMMER(FEMININUM, "Nummer", "Nummern"),
    GE_BUCHSTABE(MASKULINUM, "Buchstabe", "Buchstaben"),
    GE_DOPPELBUCHSTABE(MASKULINUM, "Doppelbuchstabe", "Doppelbuchstaben"),
    GE_DREIFACHBUCHSTABE(MASKULINUM, "Dreifachbuchstabe", "Dreifachbuchstaben"),
    GE_SATZ(MASKULINUM, "Satz", "Sätze");

    private static final Map<String, Gliederungseinheit> bezeichnungen;

    static {
        bezeichnungen = new HashMap<>();
        Arrays.stream(values()).forEach(
            gliederungseinheit -> bezeichnungen.put(gliederungseinheit.getBezeichnung(), gliederungseinheit));
    }

    private final Genus genus;
    private final String bezeichnung;
    private final String nominativPlural;

    /*
     * Referring to "Kapitel 1" returns Gliederungseinheit.GE_KAPITEL; referring to
     * "Abschnitt 2a" returns Gliederungseinheit.GE_ABSCHNITT.
     */
    public static Gliederungseinheit fromReference(String reference) {
        if (Utils.isMissing(reference)) {
            return null;
        }

        String[] parts = reference.split("\\s+");
        if (parts.length > 0) {
            return bezeichnungen.get(parts[0]);
        } else {
            return bezeichnungen.get(reference);
        }
    }

    public String getNominativSingular() {
        return bezeichnung;
    }

}
