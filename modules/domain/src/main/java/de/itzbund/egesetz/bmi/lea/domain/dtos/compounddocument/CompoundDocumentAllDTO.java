// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@SuperBuilder
@EqualsAndHashCode
@RequiredArgsConstructor
@ToString
public class CompoundDocumentAllDTO {

    // Title of compound document
    @NotNull
    @JsonProperty("title")
    @Schema(name = "title", example = "Title for compound document", required = true)
    protected String title;

    // Compound document should be assigned to a type
    @NotNull
    @JsonProperty("type")
    @Schema(name = "type", example = "STAMMGESETZ", required = true)
    protected CompoundDocumentTypeVariant type = CompoundDocumentTypeVariant.STAMMGESETZ;

}
