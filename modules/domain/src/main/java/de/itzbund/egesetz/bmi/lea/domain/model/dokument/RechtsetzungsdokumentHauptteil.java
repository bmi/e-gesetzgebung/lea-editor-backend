// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model.dokument;

import de.itzbund.egesetz.bmi.lea.core.Constants;
import de.itzbund.egesetz.bmi.lea.domain.model.version.LegalDocMLDeVersion;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_CHILDREN;

/**
 * Hauptteil des Rechtsetzungsdokuments.
 */
@Getter
@EqualsAndHashCode(callSuper = true)
@SuppressWarnings("unchecked")
public class RechtsetzungsdokumentHauptteil extends ModellElement {

    @NotNull
    private final List<TeildokumentVerweisContainer> verweise = new ArrayList<>();


    public RechtsetzungsdokumentHauptteil() {
        super(List.of(LegalDocMLDeVersion.LDML_DE_V1_04), ModellElementTyp.RECHTSETZUNGSDOKUMENT_HAUPTTEIL);
    }


    public static RechtsetzungsdokumentHauptteil createFromJSON(@NonNull JSONObject jsonObject) {
        return (new RechtsetzungsdokumentHauptteil()).fromJSON(jsonObject);
    }


    public void addVerweis(@NonNull TeildokumentVerweisContainer verweisContainer) {
        verweise.add(verweisContainer);
    }


    @Override
    public RechtsetzungsdokumentHauptteil fromJSON(@NonNull JSONObject jsonObject) {
        if (jsonObject.isEmpty() || !typ.getXmlType().equals(jsonObject.get(Constants.JSON_KEY_TYPE))) {
            return null;
        }

        JSONArray children = (JSONArray) jsonObject.get(JSON_KEY_CHILDREN);
        if (children == null || children.isEmpty()) {
            // a rechtsetzungsdokumentHauptteil must include at least one container
            return null;
        }

        RechtsetzungsdokumentHauptteil hauptteil = new RechtsetzungsdokumentHauptteil();

        children.forEach(obj -> {
            JSONObject child = (JSONObject) obj;
            TeildokumentVerweisContainer container = TeildokumentVerweisContainer.createFromJSON(child);
            if (container != null) {
                hauptteil.addVerweis(container);
            }
        });

        return hauptteil;
    }


    @Override
    public JSONObject toJSONObject() {
        JSONObject jsonObject = new JSONObject();
        addJSONAttribute(jsonObject, Constants.JSON_KEY_TYPE, typ.getXmlType());

        if (verweise.isEmpty()) {
            jsonObject.put(Constants.JSON_KEY_CHILDREN, new JSONArray());
        } else {
            verweise.forEach(verweis -> addChild(jsonObject, verweis.toJSONObject()));
        }

        return jsonObject;
    }

}
