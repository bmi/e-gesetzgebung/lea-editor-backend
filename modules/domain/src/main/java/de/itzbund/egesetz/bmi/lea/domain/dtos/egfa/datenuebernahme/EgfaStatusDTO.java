// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme;

import de.itzbund.egesetz.bmi.lea.domain.enums.egfa.datenuebernahme.EgfaModuleType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;
import java.time.Instant;

/**
 * represents root DTO for eGFA Datenübernahme to Editor
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class EgfaStatusDTO {

	@NotNull
	private EgfaModuleType egfaModuleId;
	@NotNull
	private Instant egfaDmUpdateDate;
	@NotNull
	private String egfaDmUpdateUser;
	@NotNull
	private EgfaStatus egfaDataStatus;
}
