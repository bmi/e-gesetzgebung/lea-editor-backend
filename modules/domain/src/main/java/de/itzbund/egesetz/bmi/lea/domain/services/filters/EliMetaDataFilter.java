// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.filters;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRAttribute;
import de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement;
import de.itzbund.egesetz.bmi.lea.domain.services.eli.Group1FRBREntity;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;

import javax.xml.namespace.QName;
import java.util.Map;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_VALUE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRALIAS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBREXPRESSION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRMANIFESTATION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRWORK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_IDENTIFICATION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_META;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PROPRIETARY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.META_BREG_TARGET_NS;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDescendant;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getLocalName;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getQName;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.isEffectivelyEmpty;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.setText;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.Group1FRBREntity.EXPRESSION;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.Group1FRBREntity.MANIFESTATION;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.Group1FRBREntity.WORK;

@Component
@Log4j2
@RequiredArgsConstructor
public class EliMetaDataFilter implements XmlFilter {

    private @Setter String dateValue;
    private @Setter String federfuehrer;
    private final Map<FRBRElement, Map<FRBRAttribute, String>> eliMetaData;


    @Override
    public void doFilter(JSONObject jsonObject, FilterChain chain) {
        log.info("EliMetaDataFilter called.");

        // Work level
        JSONObject frbrWorkObject = getDescendant(jsonObject, true, "*", ELEM_META, ELEM_IDENTIFICATION,
            ELEM_FRBRWORK);
        updateFRBRLevel(WORK, frbrWorkObject);

        // Expression level
        JSONObject frbrExprObject = getDescendant(jsonObject, true, "*", ELEM_META, ELEM_IDENTIFICATION,
            ELEM_FRBREXPRESSION);
        updateFRBRLevel(EXPRESSION, frbrExprObject);

        // Manifestation level
        JSONObject frbrManifestObject = getDescendant(jsonObject, true, "*", ELEM_META, ELEM_IDENTIFICATION,
            ELEM_FRBRMANIFESTATION);
        updateFRBRLevel(MANIFESTATION, frbrManifestObject);

        // Make meta:federfuehrung entry
        JSONObject proprietaryMetadataObject = getDescendant(jsonObject, true, "*", ELEM_META,
            ELEM_PROPRIETARY);
        updateProprietaryMetadata(proprietaryMetadataObject);

        // Fertig, weiter in der Filter Chain
        chain.doFilter(jsonObject);
    }


    @SuppressWarnings("unchecked")
    private void updateProprietaryMetadata(JSONObject wrapperObject) {
        JSONArray children = getChildren(wrapperObject);
        if (isEffectivelyEmpty(children) || (Utils.isMissing(dateValue) && Utils.isMissing(federfuehrer))) {
            return;
        }

        JSONObject metadataObject = null;
        for (Object object : children) {
            JSONObject child = (JSONObject) object;
            QName qName = getQName(child);
            if (qName != null
                && "legalDocML.de_metadaten".equals(qName.getLocalPart())
                && META_BREG_TARGET_NS.equals(qName.getNamespaceURI())) {
                metadataObject = child;
                break;
            }
        }

        if (metadataObject != null) {
            updateFederfuehrung(metadataObject);
        }
    }


    private void updateFederfuehrung(JSONObject metadataObject) {
        JSONObject federfuehrend = getDescendant(metadataObject, false, "meta:federfuehrung", "meta:federfuehrend");
        if (federfuehrend == null) {
            return;
        }

        if (!Utils.isMissing(dateValue)) {
            addAttribute(federfuehrend, Pair.of("ab", dateValue));
        }

        if (!Utils.isMissing(federfuehrer)) {
            setText(federfuehrend, federfuehrer);
        }
    }


    // assumed that structure in JSON is valid
    @SuppressWarnings("unchecked")
    private void updateFRBRLevel(Group1FRBREntity level, JSONObject frbrObject) {
        JSONArray jsonChildren = getChildren(frbrObject);
        jsonChildren.forEach(object -> {
            JSONObject child = (JSONObject) object;
            String type = getLocalName(getType(child));

            if (EXPRESSION.equals(level) && ELEM_FRBRALIAS.equals(type)) {
                addAttribute(child, Pair.of(ATTR_VALUE, UUID.randomUUID().toString()));
            } else {
                FRBRElement frbrElement = FRBRElement.getFRBRElement(level, type);
                frbrElement.getAttributes().forEach(frbrAttribute -> addAttribute(child, Pair.of(
                    frbrAttribute.getLdmlName(),
                    eliMetaData.get(frbrElement).get(frbrAttribute)
                )));
            }
        });
    }

}
