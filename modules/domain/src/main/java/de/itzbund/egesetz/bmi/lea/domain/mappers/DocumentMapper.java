// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.mappers;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentMetadataDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true),
    uses = {BaseMapper.class, PropositionMapper.class, UserMapper.class}
)
public interface DocumentMapper {

    // qualifiedByName defined in BaseMapper
    @Mapping(target = "id", source = "documentEntity.documentId", qualifiedByName = "documentIdStringMapping")
    @Mapping(target = "compoundDocumentId", source = "documentEntity.compoundDocumentId",
        qualifiedByName = "compoundDocumentIdUuidMapping")
    //    @Mapping(target = "state", source = "regelungsVorhabenDTO.status")
    @Mapping(target = "proposition", source = "regelungsVorhabenDTO")
    DocumentDTO mapToDocumentDTO(DocumentDomain documentEntity, RegelungsvorhabenEditorDTO regelungsVorhabenDTO);

    @Mapping(target = "id", source = "documentEntity.documentId", qualifiedByName = "documentIdStringMapping")
    @Mapping(target = "compoundDocumentId", source = "documentEntity.compoundDocumentId",
        qualifiedByName = "compoundDocumentIdUuidMapping")
    @Mapping(target = "title", source = "documentEntity.title")
    @Mapping(target = "type", source = "documentEntity.type")
    @Mapping(target = "state", source = "documentState")
    @Mapping(target = "createdBy", source = "documentEntity.createdBy")
    @Mapping(target = "updatedBy", source = "documentEntity.updatedBy")
    @Mapping(target = "createdAt", source = "documentEntity.createdAt")
    @Mapping(target = "updatedAt", source = "documentEntity.updatedAt")
    @Mapping(target = "content", source = "documentEntity.content")
    DocumentDTO mapToDto(Document document);

    @Mapping(target = "id", source = "documentEntity.documentId.id")
    @Mapping(target = "title", source = "documentEntity.title")
    @Mapping(target = "type", source = "documentEntity.type")
    @Mapping(target = "state", source = "documentState")
    //    @Mapping(target = "version", source = "???")
    @Mapping(target = "createdAt", source = "documentEntity.createdAt")
    @Mapping(target = "updatedAt", source = "documentEntity.updatedAt")
    DocumentSummaryDTO mapSummary(Document document);

    @Mapping(target = "id", source = "document.documentEntity.documentId", qualifiedByName = "documentIdStringMapping")
    @Mapping(target = "compoundDocumentId", source = "document.documentEntity.compoundDocumentId",
        qualifiedByName = "compoundDocumentIdUuidMapping")
    @Mapping(target = "title", source = "document.documentEntity.title")
    @Mapping(target = "type", source = "document.documentEntity.type")
    @Mapping(target = "state", source = "document.documentState")
    @Mapping(target = "createdBy", source = "document.documentEntity.createdBy")
    @Mapping(target = "updatedBy", source = "document.documentEntity.updatedBy")
    @Mapping(target = "createdAt", source = "document.documentEntity.createdAt")
    @Mapping(target = "updatedAt", source = "document.documentEntity.updatedAt")
    @Mapping(target = "content", source = "document.documentEntity.content")
    @Mapping(target = "proposition", source = "regelungsVorhabenDTO")
    DocumentDTO mapDokumentMitRegelungsvorhaben(Document document, RegelungsvorhabenEditorDTO regelungsVorhabenDTO);

    List<DocumentDTO> mapToDtoList(List<Document> documents);

    List<DocumentSummaryDTO> mapToSummatyDtoList(List<Document> documents);

    @Mapping(target = "id", source = "documentId.id")
    DocumentSummaryDTO mapSummaryDomain(DocumentDomain documentEntity);

    DocumentMetadataDTO mapMetaData(DocumentDTO documentDTO);

}
