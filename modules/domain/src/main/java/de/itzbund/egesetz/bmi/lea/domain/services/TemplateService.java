// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.model.Template;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.TemplatePersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.TemplateRestPort;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.setGUIDsInTemplate;

@Service
@Log4j2
@SuppressWarnings("unused")
public class TemplateService implements TemplateRestPort {

    private TemplatePersistencePort templateRepository;


    @Autowired
    public TemplateService(final TemplatePersistencePort templatePersistencePort) {
        this.templateRepository = templatePersistencePort;
    }


    @Override
    public String loadTemplate(DocumentType documentType, int numLevels) {
        String templateName = documentType.getTemplateID();
        if (documentType == DocumentType.REGELUNGSTEXT_STAMMGESETZ) {
            templateName = String.format("%s-%s", templateName, numLevels);
        } else if (documentType == DocumentType.REGELUNGSTEXT_MANTELGESETZ
            || documentType == DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG
            || documentType == DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG) {
            templateName = String.format("%s-0", templateName);
        }

        List<Template> templateVersions =
            templateRepository.findAllByTemplateNameOrderByVersionDescUpdatedAtDesc(templateName);
        Template template = templateVersions.isEmpty() ? null : templateVersions.get(0);

        if (template == null || template.getContent().isBlank()) {
            log.error("template not found: " + templateName);
            return null;
        }

        return setGUIDsInTemplate(template.getContent());
    }

}
