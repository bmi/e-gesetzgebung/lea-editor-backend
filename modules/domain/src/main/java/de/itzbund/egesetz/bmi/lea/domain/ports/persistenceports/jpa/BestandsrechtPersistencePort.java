// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa;

import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.DMBestandsrechtLinkDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtListDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.Bestandsrecht;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.BestandsrechtId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;

import java.util.List;
import java.util.Optional;

public interface BestandsrechtPersistencePort {

    Bestandsrecht save(Bestandsrecht bestandsrecht, RegelungsVorhabenId regelungsVorhabenId, List<ServiceState> status);

    DMBestandsrechtLinkDTO save(CompoundDocumentId compoundDocumentId, String dmTitel, BestandsrechtId bestandsrechtId, String brTitel);

    Optional<Bestandsrecht> getBestandsrechtByBestandsrechtId(BestandsrechtId bestandsrechtId);

    List<BestandsrechtListDTO> getBestandsrechtEntitiesById(List<BestandsrechtId> bestandsrechtIds);

    List<BestandsrechtId> getBestandsrechtAssociationsByRvId(RegelungsVorhabenId regelungsVorhabenId);

    List<DMBestandsrechtLinkDTO> getLinkedCompoundDocuments(List<BestandsrechtId> bestandsrechtIds);

    void deleteAssignmentForDokumentenMappeAndBestandsrecht(CompoundDocumentId verknuepfteDokumentenMappeId, BestandsrechtId bestandsrechtId);
}
