// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.document;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

/**
 * CreateDocumentDTO, e.g. used in /compounddocuments/{id}/documents
 */
@Data
@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class CreateDocumentDTO extends DocumentAllDTO {

    @Schema(name = "regelungsvorhabenId", example = "00000000-0000-0000-0000-000000000005", required = true)
    private UUID regelungsvorhabenId;

    @Schema(name = "initialNumberOfLevels", example = "1")
    private int initialNumberOfLevels;

}
