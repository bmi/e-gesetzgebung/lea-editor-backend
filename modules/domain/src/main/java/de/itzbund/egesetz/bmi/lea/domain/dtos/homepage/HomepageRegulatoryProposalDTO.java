// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.homepage;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.itzbund.egesetz.bmi.lea.domain.enums.EntityType;
import de.itzbund.egesetz.bmi.lea.domain.enums.FarbeType;
import de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
@SuppressWarnings("unused")
public class HomepageRegulatoryProposalDTO {

    private UUID id;

    @EqualsAndHashCode.Exclude
    private String abbreviation;

    @EqualsAndHashCode.Exclude
    private FarbeType farbe;

    @EqualsAndHashCode.Exclude
    private VorhabenStatusType status;

    // the name 'children' is required by frontend table component
    @EqualsAndHashCode.Exclude
    private List<HomepageCompoundDocumentDTO> children;


    @JsonProperty
    public EntityType getEntityType() {
        return EntityType.REGULATORY_PROPOSAL;
    }
}
