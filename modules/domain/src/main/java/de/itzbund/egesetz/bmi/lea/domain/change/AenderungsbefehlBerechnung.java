// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.change;

import de.itzbund.egesetz.bmi.lea.core.compare.JSONObjectListComparer;
import de.itzbund.egesetz.bmi.lea.core.compare.ObjectListDiff;
import de.itzbund.egesetz.bmi.lea.domain.compare.AbstractDocumentComparer;
import de.itzbund.egesetz.bmi.lea.domain.compare.RegulatoryTextComparer;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.EinzelvorschriftRefersToLiteral;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.EIdGeneratorRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.TemplateRestPort;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.IllegalDocumentType;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_EID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_REFERSTO;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ARTICLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BILL;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BOOK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CHAPTER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_HEADING;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LONGTITLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PARAGRAPH;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PART;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SECTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SUBCHAPTER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SUBSECTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SUBTITLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TITLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TOC;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.HIERARCHY_ELEMENTS;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDescendant;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDocumentObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getGuid;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getLocalName;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getStringAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.isEffectivelyEmpty;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewDefaultJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNumObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.replaceChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.setText;

@Component
@Log4j2
public class AenderungsbefehlBerechnung extends AbstractDocumentComparer {

    private TemplateRestPort templateService;

    private EIdGeneratorRestPort eIdGeneratorService;

    @Getter
    private JSONObject resultDocumentObject;

    private AenderungsfaehigeGliederungseinheiten gliederungseinheiten;

    private Set<String> alreadyInsertedLevels;

    private String predecessorEId;


    @Autowired
    public AenderungsbefehlBerechnung(final TemplateRestPort templateRestPort, final EIdGeneratorRestPort eIdGeneratorRestPort) {
        this.templateService = templateRestPort;
        this.eIdGeneratorService = eIdGeneratorRestPort;
    }


    @Override
    public void compare(@NonNull JSONObject baseDocumentObject, Map<String, Object> baseMetadata,
        @NonNull JSONObject versionDocumentObject, Map<String, Object> versionMetadata) {
        this.gliederungseinheiten = new AenderungsfaehigeGliederungseinheiten();
        this.alreadyInsertedLevels = new HashSet<>();

        try {
            baseDocumentObject = eIdGeneratorService.erzeugeEIdsFuerDokument(baseDocumentObject);
            versionDocumentObject = eIdGeneratorService.erzeugeEIdsFuerDokument(versionDocumentObject);
        } catch (IllegalDocumentType e) {
            log.error("EIdGeneratorService: illegal document type", e);
        }

        initialize(baseDocumentObject, versionDocumentObject);
        resultDocumentObject = getResultDocumentFromTemplate(baseMetadata, versionMetadata);
        compareHierarchyLevels();
        gliederungseinheiten.makeAenderungsbefehle(getArticlePara());

        try {
            eIdGeneratorService.erzeugeEIdsFuerDokument(resultDocumentObject);
        } catch (IllegalDocumentType e) {
            log.error(e);
        }
    }


    private void compareHierarchyLevels() {
        JSONObjectListComparer comparer = new JSONObjectListComparer(baseDocument, versionDocument);
        comparer.compare(baseDocument.getHierarchyLevels(), versionDocument.getHierarchyLevels());
        LinkedList<ObjectListDiff<UUID>> keyDiffs = comparer.getKeyDiffs();
        AtomicReference<JSONObject> tempObject = new AtomicReference<>();
        predecessorEId = null;

        keyDiffs.forEach(diff -> {
            DiffMatchPatch.Operation op = diff.getOperation();
            JSONObject baseObject = baseDocument.getObject(diff.getKey().toString());
            JSONObject versionObject = versionDocument.getObject(diff.getKey().toString());
            String type = baseObject != null
                ? getType(baseObject)
                : getType(versionObject);
            type = getLocalName(type);

            switch (type) {
                case ELEM_LONGTITLE:
                    handleLongTitle(baseObject, versionObject, op, tempObject);
                    break;
                case ELEM_TOC:
                    handleTOC(baseObject, versionObject, op, tempObject);
                    break;
                case ELEM_BOOK:
                case ELEM_PART:
                case ELEM_CHAPTER:
                case ELEM_SUBCHAPTER:
                case ELEM_SECTION:
                case ELEM_SUBSECTION:
                case ELEM_TITLE:
                case ELEM_SUBTITLE:
                    handleHierarchyLevel(baseObject, versionObject, op, predecessorEId);
                    break;
                case ELEM_ARTICLE:
                    handleArticle(baseObject, versionObject, op, predecessorEId);
                    break;
                default:
            }

            predecessorEId = baseObject == null ? getStringAttribute(versionObject, ATTR_EID) : getStringAttribute(baseObject, ATTR_EID);
        });
    }


    private void handleLongTitle(JSONObject baseObject, JSONObject versionObject, DiffMatchPatch.Operation op,
        AtomicReference<JSONObject> tempObject) {
        if (op == DiffMatchPatch.Operation.EQUAL) {
            gliederungseinheiten.addAenderungsfaehigeGliederungseinheit(
                new DokumentUeberschrift(baseObject, versionObject, op));
        } else {
            if (tempObject.get() == null) {
                tempObject.set(baseObject);
            } else {
                gliederungseinheiten.addAenderungsfaehigeGliederungseinheit(
                    new DokumentUeberschrift(tempObject.getAndSet(null), versionObject, op));
            }
        }
    }


    private void handleTOC(JSONObject baseObject, JSONObject versionObject, DiffMatchPatch.Operation op,
        AtomicReference<JSONObject> tempObject) {
        if (op == DiffMatchPatch.Operation.EQUAL) {
            gliederungseinheiten.addAenderungsfaehigeGliederungseinheit(
                new Inhaltsuebersicht(baseObject, versionObject, op));
        } else {
            if (tempObject.get() == null) {
                tempObject.set(baseObject);
            } else {
                gliederungseinheiten.addAenderungsfaehigeGliederungseinheit(
                    new Inhaltsuebersicht(tempObject.getAndSet(null), versionObject, op));
            }
        }
    }


    private void handleHierarchyLevel(JSONObject baseObject, JSONObject versionObject, DiffMatchPatch.Operation op, String predecessorEId) {
        if (!alreadyInsertedLevels.contains(getGuid(versionObject))) {
            gliederungseinheiten.addAenderungsfaehigeGliederungseinheit(
                new UebergeordneteGliederungseinheit(baseDocument, versionDocument, baseObject, versionObject, op, predecessorEId)
            );
        }

        if (op == DiffMatchPatch.Operation.INSERT) {
            addAlreadyInsertedLevels(versionObject);
        }
    }


    @SuppressWarnings("unchecked")
    private void addAlreadyInsertedLevels(JSONObject jsonObject) {
        alreadyInsertedLevels.add(getGuid(jsonObject));
        JSONArray children = getChildren(jsonObject);
        if (!isEffectivelyEmpty(children)) {
            children.forEach(object -> {
                JSONObject child = (JSONObject) object;
                String type = getLocalName(getType(child));
                if (HIERARCHY_ELEMENTS.contains(type)) {
                    addAlreadyInsertedLevels(child);
                }
            });
        }
    }


    private void handleArticle(JSONObject baseObject, JSONObject versionObject, DiffMatchPatch.Operation op, String predecessorEId) {
        if (!alreadyInsertedLevels.contains(getGuid(versionObject))) {
            gliederungseinheiten.addAenderungsfaehigeGliederungseinheit(
                new Einzelvorschrift(baseDocument, versionDocument, baseObject, versionObject, op, predecessorEId)
            );
        }
    }


    private JSONObject getArticlePara() {
        JSONObject body = getDescendant(resultDocumentObject, true, ELEM_BILL, ELEM_BODY);
        JSONObject article = makeNewDefaultJSONObject(ELEM_ARTICLE);
        addAttribute(article, Pair.of(ATTR_REFERSTO, EinzelvorschriftRefersToLiteral.EINZELVORSCHRIFT_MANTELFORM_HAUPTAENDERUNG.getLiteral()));

        JSONObject numObject = makeNumObject("1", "Artikel 1");

        JSONObject heading = makeNewDefaultJSONObject(ELEM_HEADING);
        setText(heading, "Änderung des XYZ-Gesetzes");

        JSONObject paraObject = makeNewDefaultJSONObject(ELEM_PARAGRAPH);
        JSONObject paraNumObject = makeNumObject("1", "");
        addChildren(paraObject, paraNumObject);

        addChildren(article, numObject, heading, paraObject);

        assert body != null;
        replaceChildren(body, article);
        return paraObject;
    }


    @SuppressWarnings("java:S1168")
    private JSONObject getResultDocumentFromTemplate(Map<String, Object> baseMetadata, Map<String, Object> versionMetadata) {
        RechtsetzungsdokumentTyp baseDocumentType =
            (RechtsetzungsdokumentTyp) baseMetadata.get(RegulatoryTextComparer.RegulatoryTextMetadata.TYPE.name());
        RechtsetzungsdokumentTyp versionDocumentType =
            (RechtsetzungsdokumentTyp) versionMetadata.get(RegulatoryTextComparer.RegulatoryTextMetadata.TYPE.name());

        if (baseDocumentType != versionDocumentType) {
            log.error("Same types are expected: {} vs. {}", baseDocumentType, versionDocumentType);
            return null;
        }

        DocumentType requiredDocumentType;
        switch (baseDocumentType) {
            case GESETZ:
                requiredDocumentType = DocumentType.REGELUNGSTEXT_MANTELGESETZ;
                break;
            case VERORDNUNG:
                requiredDocumentType = DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG;
                break;
            default:
                requiredDocumentType = null;
        }

        if (requiredDocumentType == null) {
            log.error("Unsupported document type: {}", baseDocumentType);
            return null;
        }

        String template = templateService.loadTemplate(requiredDocumentType, 0);
        return getDocumentObject(template);
    }

}
