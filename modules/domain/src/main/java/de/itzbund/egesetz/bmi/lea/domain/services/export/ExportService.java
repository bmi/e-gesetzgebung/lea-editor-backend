// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.export;

import de.itzbund.egesetz.bmi.lea.core.Constants;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentForm;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.ExportRestPort;
import de.itzbund.egesetz.bmi.lea.domain.services.CollectionDocumentBuilder;
import de.itzbund.egesetz.bmi.lea.domain.services.ConverterService;
import de.itzbund.egesetz.bmi.lea.domain.services.DownloadService;
import de.itzbund.egesetz.bmi.lea.domain.services.PlategRequestService;
import de.itzbund.egesetz.bmi.lea.domain.services.UserService;
import de.itzbund.egesetz.bmi.lea.domain.services.eli.EliMetadatenUtil;
import de.itzbund.egesetz.bmi.lea.domain.services.filters.ChangeAnnotationFilter;
import de.itzbund.egesetz.bmi.lea.domain.services.filters.EIdGeneratorFilter;
import de.itzbund.egesetz.bmi.lea.domain.services.filters.EliMetaDataFilter;
import de.itzbund.egesetz.bmi.lea.domain.services.filters.FilterChain;
import de.itzbund.egesetz.bmi.lea.domain.services.logging.DomainLoggingService;
import de.itzbund.egesetz.bmi.lea.domain.services.logging.LogActivity;
import de.itzbund.egesetz.bmi.lea.domain.services.meta.MetadatenUpdateService;
import lombok.extern.log4j.Log4j2;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDocumentObject;

@Service
@Log4j2
@SuppressWarnings("unused")
public class ExportService implements ExportRestPort {

    public static final Set<DocumentType> EXCLUDED_DOCTYPES = Stream.of(
            DocumentType.ANLAGE, DocumentType.SYNOPSE)
        .collect(Collectors.toCollection(HashSet::new));

    @Autowired
    private CompoundDocumentPersistencePort compoundDocumentRepository;
    @Autowired
    private DokumentPersistencePort documentRepository;
    @Autowired
    private PlategRequestService plategRequestService;
    @Autowired
    private DownloadService downloadService;
    @Autowired
    private MetadatenUpdateService metadatenUpdateService;
    @Autowired
    private ConverterService converterService;
    @Autowired
    private ZipExportFilenameBuilder filenameBuilder;
    @Autowired
    private CollectionDocumentBuilder collectionDocumentBuilder;
    @Autowired
    private PropositionMapper propositionMapper;
    @Autowired
    private ChangeAnnotationFilter changeAnnotationFilter;
    @Autowired
    private EIdGeneratorFilter eIdGeneratorFilter;
    @Autowired
    private DomainLoggingService domainLoggingService;
    @Autowired
    private UserService userService;

    @Override
    public void exportCompoundDocument(HttpServletResponse response, UUID compoundDocumentId,
        List<ServiceState> status) {
        CompoundDocumentDomain compoundDocumentDomain = compoundDocumentRepository.findByCompoundDocumentId(
                new CompoundDocumentId(compoundDocumentId))
            .orElse(null);
        if (compoundDocumentDomain == null) {
            status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
            return;
        }

        UUID rvId = compoundDocumentDomain.getRegelungsVorhabenId().getId();
        RegelungsvorhabenEditorDTO regelungsvorhaben = plategRequestService.getRegulatoryProposalById(rvId);
        Proposition proposition = propositionMapper.mapD(regelungsvorhaben);

        // date and time of export, should be same value for all documents
        long timestamp = System.currentTimeMillis();

        // get filename of the zip archive
        String zipFilename = filenameBuilder.getCompoundDocumentFilename(timestamp, compoundDocumentDomain,
            ZipExportFilenameBuilder.FileFormat.ARCHIVE);
        if (zipFilename == null || zipFilename.isBlank()) {
            zipFilename = Constants.DEFAULT_ZIP_NAME;
        }

        // get access to all single documents within the given compound document
        List<DocumentDomain> docs = documentRepository.findByCompoundDocumentId(
                compoundDocumentDomain.getCompoundDocumentId())
            .orElse(List.of());

        // store xml content and sort by filename
        SortedMap<String, String> documents = new TreeMap<>();
        SortedMap<String, DocumentType> docTypes = new TreeMap<>();

        for (DocumentDomain doc : docs) {
            String xmlContent = getXml(doc, compoundDocumentDomain, proposition);
            if (xmlContent == null) {
                log.error("XML content could not be retrieved: {}", doc.getDocumentId()
                    .getId());
                status.add(ServiceState.INVALID_ARGUMENTS);
                return;
            }

            String filename = filenameBuilder.getSingleDocumentFilename(timestamp, doc);
            documents.put(filename, xmlContent);
            docTypes.put(filename, doc.getType());
        }

        ExportContext exportContext = getExportContext(docTypes, proposition, compoundDocumentDomain);
        addXmlContent(response, documents, docTypes, timestamp, zipFilename, exportContext);
        status.add(ServiceState.OK);
        domainLoggingService.logActivity(LogActivity.EXPORT_LDML, userService.getUser().getGid().getId());
    }


    private ExportContext getExportContext(SortedMap<String, DocumentType> docTypes, Proposition proposition,
        CompoundDocumentDomain compoundDocumentDomain) {
        DocumentType collectionType = DocumentType.RECHTSETZUNGSDOKUMENT_STAMMGESETZ;
        for (DocumentType documentType : docTypes.values()) {
            if (!EXCLUDED_DOCTYPES.contains(documentType)) {
                RechtsetzungsdokumentForm form;
                RechtsetzungsdokumentTyp typ;
                collectionType = DocumentType.getFromTypology(
                    RechtsetzungsdokumentArt.RECHTSETZUNGSDOKUMENT,
                    documentType.getForm(),
                    documentType.getTyp()
                );
                break;
            }
        }

        return new ExportContext(
            documentRepository,
            proposition,
            compoundDocumentDomain,
            DocumentDomain.builder()
                .documentId(new DocumentId(UUID.randomUUID()))
                .type(collectionType)
                .createdAt(Instant.now())
                .build()
        );
    }


    @Override
    public String exportSingleDocument(String decodedJson, List<ServiceState> status) {
        JSONObject document = getDocumentObject(decodedJson);
        if (document == null) {
            status.add(ServiceState.INVALID_ARGUMENTS);
            log.error("no valid document JSON format");
            return null;
        }

        FilterChain filterChain = new FilterChain();
        filterChain.addFilter(changeAnnotationFilter);
        filterChain.addFilter(eIdGeneratorFilter);
        filterChain.doFilter(document);

        // Convert to XML
        String transformed = converterService.convertJsonToXml(document);
        if (transformed == null) {
            status.add(ServiceState.XML_TRANSFORM_ERROR);
            log.error("JSON document could not be transformed to XML");
            return null;
        }

        status.add(ServiceState.OK);
        domainLoggingService.logActivity(LogActivity.EXPORT_LDML, userService.getUser().getGid().getId());
        return transformed;
    }

    // add collection document to 'documents' with filename: extension=xml
    private void addXmlContent(
        HttpServletResponse response,
        SortedMap<String, String> documents,
        SortedMap<String, DocumentType> docTypes,
        long timestamp,
        String zipFilename,
        ExportContext exportContext
    ) {
        Proposition proposition = exportContext.getProposition();
        CompoundDocumentDomain compoundDocument = exportContext.getCompoundDocumentDomain();

        String xmlContent = collectionDocumentBuilder.getCollectionDocumentContent(docTypes, proposition,
            compoundDocument.getType(), exportContext);

        if (xmlContent != null && Utils.isXMLValid(xmlContent)) {
            PropositionDTO propositionDTO = propositionMapper.map(propositionMapper.mapD(proposition));
            String jsonContent = converterService.convertXmlToJson(xmlContent);
            JSONObject documentObject = getDocumentObject(jsonContent);
            DocumentDomain document = DocumentDomain.builder()
                .type(getDocumentType(compoundDocument))
                .createdAt(Instant.now())
                .content(jsonContent)
                .build();

            if (documentObject != null) {
                xmlContent = converterService.convertJsonToXml(documentObject);
            }
        }

        documents.put(
            filenameBuilder.getCompoundDocumentFilename(timestamp, compoundDocument,
                ZipExportFilenameBuilder.FileFormat.DOC_COLLECTION), xmlContent);

        downloadService.downloadCompoundDocumentAsZipFile(response, documents, zipFilename);
    }


    private DocumentType getDocumentType(CompoundDocumentDomain compoundDocument) {
        if (compoundDocument == null || compoundDocument.getType() == null) {
            return null;
        } else {
            CompoundDocumentTypeVariant type = compoundDocument.getType();
            switch (type) {
                case STAMMGESETZ:
                    return DocumentType.RECHTSETZUNGSDOKUMENT_STAMMGESETZ;
                case AENDERUNGSVERORDNUNG:
                    return DocumentType.RECHTSETZUNGSDOKUMENT_MANTELVERORDNUNG;
                case MANTELGESETZ:
                    return DocumentType.RECHTSETZUNGSDOKUMENT_MANTELGESETZ;
                case STAMMVERORDNUNG:
                    return DocumentType.RECHTSETZUNGSDOKUMENT_STAMMVERORDNUNG;
                default:
                    return null;
            }
        }
    }


    @SuppressWarnings("unchecked")
    private String getXml(DocumentDomain documentDomain, CompoundDocumentDomain compoundDocument, Proposition proposition) {
        String content = documentDomain.getContent();
        JSONObject documentObject = getDocumentObject(content);
        DocumentState documentState = compoundDocument.getState();

        if (documentObject != null) {
            FilterChain chain = new FilterChain();
            chain.addFilter(changeAnnotationFilter);
            chain.addFilter(eIdGeneratorFilter);
            chain.addFilter(new EliMetaDataFilter(EliMetadatenUtil.collectEliMetaDaten(
                new ExportContext(documentRepository, proposition, compoundDocument, documentDomain))));

            chain.doFilter(documentObject);

            // Update proprietary metadata
            if (documentState == DocumentState.DRAFT) {
                JSONObject updated = metadatenUpdateService.update(documentObject, proposition);
                JSONArray wrapper = new JSONArray();
                wrapper.add(updated);
                documentDomain.setContent(wrapper.toJSONString());
                documentRepository.save(documentDomain);
            }

            // Convert to XML
            return converterService.convertJsonToXml(documentObject);
        } else {
            log.error("could not parse string as JSON object");
            return null;
        }
    }

}
