// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.ProponentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RessortEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.PropositionPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.RegelungsvorhabenRestPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Log4j2
@SuppressWarnings("unused")
public class RegelungsvorhabenService implements RegelungsvorhabenRestPort {

	// Autowired by Constructor
	private final PlategRequestService plategRequestService;
	private final PropositionPersistencePort propositionRepository;
	private final PropositionMapper propositionMapper;

	// weiterleitung
	public RegelungsvorhabenEditorDTO getRegulatoryProposalUserCanAccess(UUID regelungsvorhabenId) {
		return plategRequestService.getRegulatoryProposalUserCanAccess(regelungsvorhabenId);
	}

	// weiterleitung
	public boolean isUserParticipantForCompoundDocument(CompoundDocumentId compoundDocumentI) {
		return plategRequestService.isUserParticipantForCompoundDocument(compoundDocumentI);
	}

	// weiterleitung
	public List<RegelungsvorhabenEditorDTO> getAllRegulatoryProposalsForUserHeIsAuthorOf() {
		return plategRequestService.getAllRegulatoryProposalsForUserHeIsAuthorOf();
	}

	// weiterleitung - PropositionService
	public List<RegelungsvorhabenEditorDTO> getRegulatoryProposalsByIdList(List<String> regelungsvorhabenIds) {
		return plategRequestService.getRegulatoryProposalsByIdList(regelungsvorhabenIds);
	}

	// weiterleitung - PropositionService
	public RegelungsvorhabenEditorDTO getRegulatoryProposalUserCanAccess(User user, UUID rvId) {
		return plategRequestService.getRegulatoryProposalUserCanAccess(user, rvId);
	}

	/**
	 * Holt das Regelungsvorhaben, die Quelle wird 'automatisch' gewählt.
	 *
	 * @param regelungsVorhabenId
	 * @param documentState
	 * @return
	 */
	public RegelungsvorhabenEditorDTO getRegelungsvorhaben(RegelungsVorhabenId regelungsVorhabenId, DocumentState documentState) {

		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = getRegelungsvorhabenAusDatenbank(regelungsVorhabenId, documentState);

		if (documentState.equals(DocumentState.DRAFT) && regelungsvorhabenEditorDTO != null) {
			return regelungsvorhabenEditorDTO;
		}

		List<RegelungsvorhabenEditorDTO> regelungsvorhabenListVonPlattform = getRegelungsvorhabenVonPlattform(List.of(regelungsVorhabenId), Optional.empty());
		RegelungsvorhabenEditorDTO regelungsvorhabenVonPlattform = null;
		if (!regelungsvorhabenListVonPlattform.isEmpty()) {
			regelungsvorhabenVonPlattform = regelungsvorhabenListVonPlattform.get(0);
		}

		if (regelungsvorhabenEditorDTO == null && regelungsvorhabenVonPlattform != null) {
			regelungsvorhabenEditorDTO = regelungsvorhabenVonPlattform;
			// Für das nächste Mal speichern
			uebernahmeRVvonPlattform(regelungsvorhabenEditorDTO);
		}

		if (!documentState.equals(DocumentState.DRAFT) && regelungsvorhabenVonPlattform != null) {
			regelungsvorhabenEditorDTO.setStatus(regelungsvorhabenVonPlattform.getStatus());
		}

		return regelungsvorhabenEditorDTO;

	}

	// ======================================================

	// ---- Von Plattform

	/**
	 * Die Regelungsvorhaben auf Plattform können dort gendert werden.
	 */
	private List<RegelungsvorhabenEditorDTO> getRegelungsvorhabenVonPlattform(List<RegelungsVorhabenId> regelungsVorhabenIds, Optional<UserId> userId) {

		List<String> regelungsVorhabenIdList = regelungsVorhabenIds.stream()
			.map(RegelungsVorhabenId::getId)
			.map(UUID::toString)
			.collect(Collectors.toList());

		return plategRequestService.getRegulatoryProposalsByIdListPlattform(regelungsVorhabenIdList, userId);
	}

	// ---- Aus Datenbank

	/**
	 * Abhängig von DocumentState werden die veränderbaren oder fixen Regelungsvorhaben aus der Datenbank geholt. Wir benutzen spezielle Queries, könnten aber
	 * auch die andere Methode verwenden und dann filtern.
	 */
	private RegelungsvorhabenEditorDTO getRegelungsvorhabenAusDatenbank(RegelungsVorhabenId regelungsVorhabenId, DocumentState documentState) {

		List<Proposition> propositions;
		if (DocumentState.DRAFT.equals(documentState)) {
			propositions = propositionRepository.findByUpdateablePropositionIds(List.of(regelungsVorhabenId));
		} else {
			propositions = propositionRepository.findByNotUpdateablePropositionId(regelungsVorhabenId);
		}

		if (!propositions.isEmpty()) {
			if (propositions.size() > 1) {
				log.error("Zu viele Ergebnisse zum Regelungsvorhaben '{}' aus der Datenbank.", regelungsVorhabenId.getId());
			}
			return propositionMapper.mapD(propositions.get(0));
		}

		log.debug("Keine Ergebnisse zum Regelungsvorhaben '{}' aus der Datenbank.", regelungsVorhabenId.getId());
		return null;
	}

	private List<RegelungsvorhabenEditorDTO> getMehrereRegelungsvorhabenAusDatenbank(List<RegelungsVorhabenId> regelungsVorhabenIds) {

		// Finde alle Einträge zu jeder RegelungsvorhabenId
		List<Proposition> propositions = propositionRepository.findByUpdateablePropositionIds(regelungsVorhabenIds);

		if (propositions.isEmpty()) {
			return Collections.emptyList();
		}

		return propositions.stream().map(p -> propositionMapper.mapD(p)).collect(Collectors.toList());
	}

	// ======================================================


	public List<RegelungsvorhabenEditorDTO> getRegelungsvorhabenEditorDTOs(List<RegelungsVorhabenId> regelungsVorhabenIds) {

		List<RegelungsvorhabenEditorDTO> result = new ArrayList<>();

		// Die Daten aus der Datenbank laden
		List<RegelungsvorhabenEditorDTO> regelungsvorhabenEditorDTOs = getMehrereRegelungsvorhabenAusDatenbank(regelungsVorhabenIds);

		// Wir streichen nun aus der Liste 'regelungsVorhabenIds' alle gefundenen RVs raus ...
		regelungsvorhabenEditorDTOs.forEach(rv -> regelungsVorhabenIds.remove(new RegelungsVorhabenId(rv.getId())));

		// ... was noch übrig ist, muss noch von der Plattform geholt und in Datenbank geschrieben werden
		if (!regelungsVorhabenIds.isEmpty()) {
			log.info("Es mussten immer noch RVs von Plattform geholt werden.");
			List<RegelungsvorhabenEditorDTO> regelungsvorhabenVonPlattform = getRegelungsvorhabenVonPlattform(regelungsVorhabenIds, Optional.empty());
			regelungsvorhabenVonPlattform.forEach(this::uebernahmeRVvonPlattform);
		}

		return regelungsvorhabenEditorDTOs;
	}

	/**
	 * Holt das Regelungsvorhaben für die Regelungsvorhaben-Id. - Aus der Datenbank - Fallback von Plattform (das ist aber nicht mehr erwünscht)
	 *
	 * @param regelungsVorhabenId Die Id des Regelungsvorhabens
	 * @return Das RegelungsvorhabenDTO
	 */
	public RegelungsvorhabenEditorDTO getRegelungsvorhabenEditorDTO(RegelungsVorhabenId regelungsVorhabenId) {
		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = getRegelungsvorhabenAusDatenbank(regelungsVorhabenId, DocumentState.DRAFT);

		// Ist nicht in der Datenbank, dann holen wir es eben von Plattform ...
		if (regelungsvorhabenEditorDTO == null) {

			log.info("Regelungsvorhaben fehlte in der Datenbank");
			List<RegelungsvorhabenEditorDTO> regelungsvorhabenVonPlattform = getRegelungsvorhabenVonPlattform(List.of(regelungsVorhabenId), Optional.empty());
			if (!regelungsvorhabenVonPlattform.isEmpty()) {
				regelungsvorhabenEditorDTO = regelungsvorhabenVonPlattform.get(0);
			}

			// Das ist eigentlich fachlich nicht möglich
			if ((regelungsvorhabenEditorDTO == null) || (PropositionServiceUtil.dummyRegelungsvorhabenEditorDTO().getId()
				.equals(regelungsvorhabenEditorDTO.getId()))) {
				log.error("Regelungsvorhaben '{}' wurde auch nicht bei Plattform gefunden", regelungsVorhabenId.getId());
				regelungsvorhabenEditorDTO = PropositionServiceUtil.dummyRegelungsvorhabenEditorDTO();
			} else {

				boolean isPlattformResponseValid = uebernahmeRVvonPlattform(regelungsvorhabenEditorDTO);
				if (!isPlattformResponseValid) {
					log.error("Plattformdaten inskonsistent");
					// Wir versuchen zu retten, was zu retten ist
					RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO2 = PropositionServiceUtil.dummyRegelungsvorhabenEditorDTO();
					regelungsvorhabenEditorDTO2.setId(regelungsvorhabenEditorDTO.getId());
					regelungsvorhabenEditorDTO = regelungsvorhabenEditorDTO2;
				}
			}

		}

		return regelungsvorhabenEditorDTO;
	}

	@Override
	public RegelungsvorhabenEditorDTO getRegelungsvorhabenEditorDTOForCompoundDocument(CompoundDocument compoundDocument) {
		// The regulary way ...
		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = getRegelungsvorhabenEditorDTO(compoundDocument.getRegelungsVorhabenId());
		// ..., but if the `Regelungsvorhaben` does not match the user, we use this hack (temporarily)
		if ((regelungsvorhabenEditorDTO == null) || (PropositionServiceUtil.dummyRegelungsvorhabenEditorDTO().getId()
			.equals(regelungsvorhabenEditorDTO.getId()))) {
			List<RegelungsvorhabenEditorDTO> regelungsvorhabenVonPlattform = getRegelungsvorhabenVonPlattform(
				List.of(compoundDocument.getRegelungsVorhabenId()), Optional.of(compoundDocument.getCompoundDocumentEntity().getCreatedBy().getGid()));
			if (!regelungsvorhabenVonPlattform.isEmpty()) {
				regelungsvorhabenEditorDTO = regelungsvorhabenVonPlattform.get(0);
			}
		}

		return regelungsvorhabenEditorDTO;
	}

	public static boolean allFieldsNotNull(Object obj) {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();

		Set<ConstraintViolation<Object>> violations = validator.validate(obj);
		return violations.isEmpty();
	}

	private boolean uebernahmeRVvonPlattform(RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO) {
		log.info("Regelungsvorhaben '{}' wird von Plattform übernommen.", regelungsvorhabenEditorDTO.getId());
		if (!allFieldsNotNull(regelungsvorhabenEditorDTO)) {
			log.error("Nicht alle zugesagten Felder sind gesetzt worden: {}", regelungsvorhabenEditorDTO);
			return false;
		}

		Proposition proposition = propositionMapper.mapD(regelungsvorhabenEditorDTO);
		propositionRepository.save(proposition);
		return true;
	}


	/**
	 * Retrieves Regelungsvorhaben from database or plattorm
	 *
	 * @deprecated Direkt getRegelungsvorhabenEditorDTO aufrufen
	 */
	@Override
	@Deprecated
	public RegelungsvorhabenEditorDTO getRegelungsvorhabenEditorDTONEW(RegelungsVorhabenId regelungsVorhabenId, boolean fromPlatform) {
		if (!fromPlatform) {
			return getRegelungsvorhabenAusDatenbank(regelungsVorhabenId, DocumentState.DRAFT);
		}

		List<RegelungsvorhabenEditorDTO> regelungsvorhabenVonPlattform = getRegelungsvorhabenVonPlattform(List.of(regelungsVorhabenId), Optional.empty());
		if (!regelungsvorhabenVonPlattform.isEmpty()) {
			return regelungsvorhabenVonPlattform.get(0);
		}

		log.error("Fachlich nicht möglich, dass kein Regelungsvorhaben vorhanden.");
		return PropositionServiceUtil.dummyRegelungsvorhabenEditorDTO();
	}

	/**
	 * Die ReferenzID ist eine UUID auf die sich das CompoundDocument beziehen kann.
	 *
	 * @param regelungsVorhabenId Die ID des Regelungsvorhabens
	 * @param isWriteProtected    Soll der Datenbankwert unveränderbar werden
	 * @return Die UUID als Referenz ID für die Dokumentenmappe
	 */
	public UUID sichereRegelungsvorhaben(RegelungsVorhabenId regelungsVorhabenId, boolean isWriteProtected) {

		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTONEW = getRegelungsvorhabenEditorDTO(regelungsVorhabenId);
		Proposition proposition = propositionMapper.mapD(regelungsvorhabenEditorDTONEW);

		boolean isDummyRv = PropositionServiceUtil.dummyRegelungsvorhabenEditorDTO().getId().equals(proposition.getPropositionId().getId());

		if ((isWriteProtected) && (!isDummyRv)) {

			// schaue nach, ob es das RV schon gibt
			List<Proposition> notUpdateablePropositions = propositionRepository.findByNotUpdateablePropositionId(regelungsVorhabenId);

			Proposition finalProposition = proposition;
			Optional<Proposition> first = notUpdateablePropositions.stream()
				.filter(e -> e.equals(finalProposition))
				.findFirst();

			if (first.isEmpty()) {
				proposition.setReferenzId(UUID.randomUUID());
				proposition = propositionRepository.copy(proposition);
				return proposition.getReferenzId();
			}

			return first.get().getReferenzId();

		}

		return proposition.getPropositionId().getId();

	}

	public Proposition sichereRegelungsvorhaben2(RegelungsVorhabenId regelungsVorhabenId, boolean isWriteProtected) {

		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTONEW = getRegelungsvorhabenEditorDTO(regelungsVorhabenId);
		Proposition proposition = propositionMapper.mapD(regelungsvorhabenEditorDTONEW);

		boolean isDummyRv = PropositionServiceUtil.dummyRegelungsvorhabenEditorDTO().getId().equals(proposition.getPropositionId().getId());

		if ((isWriteProtected) && (!isDummyRv)) {

			// schaue nach, ob es das RV schon gibt
			List<Proposition> notUpdateablePropositions = propositionRepository.findByNotUpdateablePropositionId(regelungsVorhabenId);

			Proposition finalProposition = proposition;
			Optional<Proposition> first = notUpdateablePropositions.stream()
				.filter(e -> e.equals(finalProposition))
				.findFirst();

			if (first.isEmpty()) {
				proposition.setReferenzId(UUID.randomUUID());
				return propositionRepository.copy(proposition);
			}

			return first.get();
		}

		return proposition;

	}

	private RessortEditorDTO getTechnischesRessort(RegelungsVorhabenId regelungsVorhabenId) {
		RegelungsvorhabenEditorDTO regulatoryProposal = plategRequestService.getRegulatoryProposalUserCanAccess(
			regelungsVorhabenId.getId());

		return regulatoryProposal.getTechnischesFfRessort();
	}

	@Override
	public ProponentDTO getProponent(RegelungsVorhabenId regelungsVorhabenId) {
		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = getRegelungsvorhabenEditorTriggeredByDocument(regelungsVorhabenId);

		RessortEditorDTO technischesFfRessort = regelungsvorhabenEditorDTO.getTechnischesFfRessort();

		return propositionMapper.map(technischesFfRessort);
	}

	public RegelungsvorhabenEditorDTO getRegelungsvorhabenEditorTriggeredByDocument(
		RegelungsVorhabenId regelungsVorhabenId) {
		// Use other endpoint
		// List to reuse method
		List<RegelungsvorhabenEditorDTO> regelungsvorhabens = Collections.singletonList(
			plategRequestService.getRegulatoryProposalUserCanAccess(regelungsVorhabenId.getId())
		);

		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = getRegelungsvorhabenEditorDTO(regelungsVorhabenId);
		if (regelungsvorhabenEditorDTO == null) {
			regelungsvorhabenEditorDTO = PropositionServiceUtil.dummyRegelungsvorhabenEditorDTO();
		}

		return regelungsvorhabenEditorDTO;
	}

	public RegelungsvorhabenEditorDTO getNoChangeableForGivenRegelungsvorhabenId(RegelungsVorhabenId regelungsVorhabenId,
		UUID referableId) {

		Proposition notUpdateableProposition = propositionRepository.findByNotUpdateablePropositionIdReferableId(regelungsVorhabenId, referableId);
		if (notUpdateableProposition != null) {
			return propositionMapper.mapD(notUpdateableProposition);
		}

		return null;
	}

}
