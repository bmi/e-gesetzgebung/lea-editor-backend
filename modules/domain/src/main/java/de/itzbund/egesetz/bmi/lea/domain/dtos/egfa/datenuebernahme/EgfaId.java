// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.BegruendungsteilAbschnittRefersToLiteral;
import de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.InhaltsabschnittRefersToLiteral;
import de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.RefersToLiteral;
import de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.VorblattabschnittRefersToLiteral;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@AllArgsConstructor
@SuppressWarnings("java:S1192")
public enum EgfaId {

	//beginn Vorblatt

	//Abschnitt D: Öffentliche Haushalte

	AUSWIRKUNGEN_EINNAHMEN_AUSGABEN_ERGEBNISDOKUMENTATION(
		"auswirkungenEinnahmenAusgabenErgebnisdokumentation",
		null,
		null,
		null,
		false),

	TEXT_VORBLATT_D(
		"textVorblattD",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		null,
		false),

	VORBLATT_AUSGABEN(
		"vorblattAusgaben",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		null,
		true),

	VORBLATT_AUSGABEN_BUND_CONTENT_00_REGELUNG_TABLE(
		"vorblattAusgabenBundContent00RegelungTable",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		null,
		false),

	VORBLATT_AUSGABEN_LAENDER_CONTENT_00_REGELUNG_TABLE(
		"vorblattAusgabenLaenderContent00RegelungTable",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		null,
		false),

	VORBLATT_AUSGABEN_GEMEINDEN_CONTENT_00_REGELUNG_TABLE(
		"vorblattAusgabenGemeindenContent00RegelungTable",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		null,
		false),

	VORBLATT_AUSGABEN_GESAMTSUMME_CONTENT_00_REGELUNG_TABLE(
		"vorblattAusgabenGesamtsummeContent00RegelungTable",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		null,
		false),

	VORBLATT_EINNAHMEN(
		"vorblattEinnahmen",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		null,
		true),

	VORBLATT_EINNAHMEN_BUND_CONTENT_00_REGELUNG_TABLE(
		"vorblattEinnahmenBundContent00RegelungTable",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		null,
		false),

	VORBLATT_EINNAHMEN_LAENDER_CONTENT_00_REGELUNG_TABLE(
		"vorblattEinnahmenLaenderContent00RegelungTable",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		null,
		false),

	VORBLATT_EINNAHMEN_GEMEINDEN_CONTENT_00_REGELUNG_TABLE(
		"vorblattEinnahmenGemeindenContent00RegelungTable",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		null,
		false),

	VORBLATT_EINNAHMEN_GESAMTSUMME_CONTENT_00_REGELUNG_TABLE(
		"vorblattEinnahmenGesamtsummeContent00RegelungTable",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		null,
		false),

	//Abschnitt E: Erfuellungsaufwand

	ERFUELLUNGSAUFWAND_ERGEBNISDOKUMENTATION(
		"erfuellungsaufwandErgebnisdokumentation",
		null,
		null,
		null,
		false),

	ALLGEMEINE_INFORMATIONEN(
		"allgemeineInformationen",
		null,
		null,
		null,
		false),

	ALLGEMEINE_INFORMATIONEN_ANFERTIGUNG(
		"allgemeineInformationenAnfertigung",
		null,
		null,
		null,
		false),

	ALLGEMEINE_INFORMATIONEN_UMGESETZTEN(
		"allgemeineInformationenUmgesetzten",
		null,
		null,
		null,
		false),

	TEXT_VORBLATT_E(
		"textVorblattE",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_ERFUELLUNGSAUFWAND,
		null,
		false),

	VORBLATT_BUERGER_TABLE_DATA(
		"vorblattBUERGERTableData",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.ERFUELLUNGSAUFWAND_FUER_BUERGERINNEN_UND_BUERGER,
		null,
		false),

	VORBLATT_WIRTSCHAFT_TABLE_DATA(
		"vorblattWIRTSCHAFTTableData",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.ERFUELLUNGSAUFWAND_FUER_DIE_WIRTSCHAFT,
		null,
		false),

	VORBLATT_VERWALTUNG_TABLE_DATA(
		"vorblattVERWALTUNGTableData",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.ERFUELLUNGSAUFWAND_DER_VERWALTUNG,
		null,
		false),

	//Abschnitt F: Weitere Kosten

	KMU_ERGEBNISDOKUMENTATION_VORBLATT(
		"kmuErgebnisdokumentation",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_WEITERE_KOSTEN,
		null,
		false),

	KMU_AFFECTED_TEXT_VORBLATT(
		"kmuAffectedText",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_WEITERE_KOSTEN,
		null,
		true),

	KMU_SUMMARY_VORBLATT(
		"kmuSummary",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_WEITERE_KOSTEN,
		null,
		true),

	PREIS_SUMMARY_VORBLATT(
		"preisSummary",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_WEITERE_KOSTEN,
		null,
		true),

	SONSTIGE_KOSTEN_SUMMARY_VORBLATT(
		"sonstigeKostenSummary",
		RechtsetzungsdokumentArt.VORBLATT,
		VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_WEITERE_KOSTEN,
		null,
		true),

	//beginn Begruendung

	//Abschnitt VI.2: Nachhaltigkeitsaspekte

	ENAP_ERGEBNISDOKUMENTATION(
		"enapErgebnisdokumentation",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		null,
		null,
		false),

	ENAP_SUMMARY(
		"enapSummary",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_NACHHALTIGKEITSASPEKTE,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	//Abschnitt VI.3: Öffentliche Haushalte

	//--> Zweckausgaben

	ZWECKAUSGABEN_PATTERN_SUMMARY_TEXT(
		"zweckausgabenPatternSummaryText",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	ZWECKAUSGABEN_PATTERN_SUMMARY_JAHRESWIRKUNG_TEXT(
		"zweckausgabenPatternSummaryJahreswirkungText",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	ZWECKAUSGABEN_PATTERN_SUMMARY_WIDGET_BUND_TABLE(
		"zweckausgabenPatternSummaryWidgetBundTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	ZWECKAUSGABEN_WIDGET_BUND_CONTENT_00_REGELUNG_TABLE(
		"zweckausgabenWidgetBundContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	ZWECKAUSGABEN_WIDGET_BUND_CONTENT_10_REGELUNG_TABLE(
		"zweckausgabenWidgetBundContent10RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	ZWECKAUSGABEN_WIDGET_BUND_CONTENT_20_REGELUNG_TABLE(
		"zweckausgabenWidgetBundContent20RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	ZWECKAUSGABEN_PATTERN_SUMMARY_WIDGET_LAENDER_TABLE(
		"zweckausgabenPatternSummaryWidgetLaenderTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	ZWECKAUSGABEN_WIDGET_LAENDER_CONTENT_00_REGELUNG_TABLE(
		"zweckausgabenWidgetLaenderContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	ZWECKAUSGABEN_WIDGET_LAENDER_CONTENT_10_REGELUNG_TABLE(
		"zweckausgabenWidgetLaenderContent10RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	ZWECKAUSGABEN_WIDGET_LAENDER_CONTENT_20_REGELUNG_TABLE(
		"zweckausgabenWidgetLaenderContent20RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	ZWECKAUSGABEN_PATTERN_SUMMARY_WIDGET_GEMEINDEN_TABLE(
		"zweckausgabenPatternSummaryWidgetGemeindenTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	ZWECKAUSGABEN_WIDGET_GEMEINDEN_CONTENT_00_REGELUNG_TABLE(
		"zweckausgabenWidgetGemeindenContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	ZWECKAUSGABEN_WIDGET_GEMEINDEN_CONTENT_10_REGELUNG_TABLE(
		"zweckausgabenWidgetGemeindenContent10RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	ZWECKAUSGABEN_PATTERN_SUMMARY_WIDGET_GESAMTSUMME_TABLE(
		"zweckausgabenPatternSummaryWidgetGesamtsummeTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	ZWECKAUSGABEN_WIDGET_GESAMTSUMME_CONTENT_00_REGELUNG_TABLE(
		"zweckausgabenWidgetGesamtSummeContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	//--> Vollzugsaufwand

	VOLLZUGSAUFWAND_PATTERN_SUMMARY_TEXT(
		"vollzugsaufwandPatternSummaryText",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VOLLZUGSAUFWAND_PATTERN_SUMMARY_WIDGET_BUND_TABLE(
		"vollzugsaufwandPatternSummaryWidgetBundTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VOLLZUGSAUFWAND_WIDGET_BUND_CONTENT_00_REGELUNG_TABLE(
		"vollzugsaufwandWidgetBundContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VOLLZUGSAUFWAND_WIDGET_BUND_CONTENT_10_REGELUNG_TABLE(
		"vollzugsaufwandWidgetBundContent10RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VOLLZUGSAUFWAND_PATTERN_SUMMARY_WIDGET_LAENDER_TABLE(
		"vollzugsaufwandPatternSummaryWidgetLaenderTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VOLLZUGSAUFWAND_WIDGET_LAENDER_CONTENT_00_REGELUNG_TABLE(
		"vollzugsaufwandWidgetLaenderContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VOLLZUGSAUFWAND_WIDGET_LAENDER_CONTENT_10_REGELUNG_TABLE(
		"vollzugsaufwandWidgetLaenderContent10RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VOLLZUGSAUFWAND_PATTERN_SUMMARY_WIDGET_GEMEINDEN_TABLE(
		"vollzugsaufwandPatternSummaryWidgetGemeindenTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VOLLZUGSAUFWAND_WIDGET_GEMEINDEN_CONTENT_00_REGELUNG_TABLE(
		"vollzugsaufwandWidgetGemeindenContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VOLLZUGSAUFWAND_WIDGET_GEMEINDEN_CONTENT_10_REGELUNG_TABLE(
		"vollzugsaufwandWidgetGemeindenContent10RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VOLLZUGSAUFWAND_WIDGET_GEMEINDEN_CONTENT_20_REGELUNG_TABLE(
		"vollzugsaufwandWidgetGemeindenContent20RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VOLLZUGSAUFWAND_PATTERN_SUMMARY_WIDGET_GESAMTSUMME_TABLE(
		"vollzugsaufwandPatternSummaryWidgetGesamtsummeTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VOLLZUGSAUFWAND_WIDGET_GESAMTSUMME_CONTENT_00_REGELUNG_TABLE(
		"vollzugsaufwandWidgetGesamtSummeContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VOLLZUGSAUFWAND_INFLUENCE_SUMMARY_TEXT(
		"vollzugsaufwandInfluenceSummaryText",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VOLLZUGSAUFWAND_PATTERN_SUMMARY_WIDGET_PLANSTELLEN_TABLE(
		"vollzugsaufwandPatternSummaryWidgetPlanstellenTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	//--> hier wäre es ein block mit einer Überschrift und mehreren Tabellen
	VOLLZUGSAUFWAND_WIDGET_PLANSTELLEN_CONTENT_00_REGELUNG_TABLE(
		"vollzugsaufwandWidgetPlanstellenContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VOLLZUGSAUFWAND_WIDGET_PLANSTELLEN_CONTENT_01_REGELUNG_TABLE(
		"vollzugsaufwandWidgetPlanstellenContent01RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		false),

	VOLLZUGSAUFWAND_WIDGET_PLANSTELLEN_CONTENT_10_REGELUNG_TABLE(
		"vollzugsaufwandWidgetPlanstellenContent10RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VOLLZUGSAUFWAND_PATTERN_SUMMARY_WIDGET_PLANSTELL_GESAMTEN_TABLE(
		"vollzugsaufwandPatternSummaryWidgetPlanstellGesamtenTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VOLLZUGSAUFWAND_WIDGET_PLANSTELL_GESAMT_CONTENT_00_REGELUNG_TABLE(
		"vollzugsaufwandWidgetPlanstellGesamtContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	//--> Normadressat

	NORMADRESSAT_SUMMARY_TEXT(
		"normadressatSummaryText",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	NORMADRESSAT_AUSGABEN_DURCH_TABLE(
		"normadressatAusgabenDurchTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	NORMADRESSAT_AUSGABEN_DURCH_CONTENT_00_REGELUNG_TABLE(
		"normadressatAusgabenDurchContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	NORMADRESSAT_AUSGABEN_DURCH_CONTENT_10_REGELUNG_TABLE(
		"normadressatAusgabenDurchContent10RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	NORMADRESSAT_AUSGABEN_GESAMT_TABLE(
		"normadressatAusgabenGesamtTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	NORMADRESSAT_AUSGABEN_GESAMT_CONTENT_00_REGELUNG_TABLE(
		"normadressatAusgabenGesamtContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	STELLENPLANEN_SUMMARY(
		"stellenplanenSummaryText",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		false),

	NORMADRESSAT_PLANSTELLEN_TABLE(
		"normadressatPlanstellenTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	NORMADRESSAT_PLANSTELLEN_CONTENT_00_REGELUNG_TABLE(
		"normadressatPlanstellenContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	NORMADRESSAT_PLANSTELLEN_CONTENT_01_REGELUNG_TABLE(
		"normadressatPlanstellenContent01RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		false),

	NORMADRESSAT_PLANSTELLEN_GESAMT_TABLE(
		"normadressatPlanstellenGesamtTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	NORMADRESSAT_PLANSTELLEN_GESAMT_CONTENT_00_REGELUNG_TABLE(
		"normadressatPlanstellenGesamtContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	//--> Verwaltungs und Sonstige

	VERWALTUNGS_UND_SONSTIGE_PATTERN_SUMMARY_TEXT(
		"verwaltungsUndSonstigePatternSummaryText",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VERWALTUNGS_UND_SONSTIGE_PATTERN_SUMMARY_JAHRESWIRKUNG_TEXT(
		"verwaltungsUndSonstigePatternSummaryJahreswirkungText",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VERWALTUNGS_UND_SONSTIGE_PATTERN_SUMMARY_WIDGET_BUND_TABLE(
		"verwaltungsUndSonstigePatternSummaryWidgetBundTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VERWALTUNGS_UND_SONSTIGE_WIDGET_BUND_CONTENT_00_REGELUNG_TABLE(
		"verwaltungsUndSonstigeWidgetBundContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VERWALTUNGS_UND_SONSTIGE_PATTERN_SUMMARY_WIDGET_LAENDER_TABLE(
		"verwaltungsUndSonstigePatternSummaryWidgetLaenderTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VERWALTUNGS_UND_SONSTIGE_WIDGET_LAENDER_CONTENT_00_REGELUNG_TABLE(
		"verwaltungsUndSonstigeWidgetLaenderContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VERWALTUNGS_UND_SONSTIGE_WIDGET_LAENDER_CONTENT_10_REGELUNG_TABLE(
		"verwaltungsUndSonstigeWidgetLaenderContent10RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VERWALTUNGS_UND_SONSTIGE_PATTERN_SUMMARY_WIDGET_GEMEINDEN_TABLE(
		"verwaltungsUndSonstigePatternSummaryWidgetGemeindenTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VERWALTUNGS_UND_SONSTIGE_WIDGET_GEMEINDEN_CONTENT_00_REGELUNG_TABLE(
		"verwaltungsUndSonstigeWidgetGemeindenContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VERWALTUNGS_UND_SONSTIGE_PATTERN_SUMMARY_WIDGET_GESAMTSUMME_TABLE(
		"verwaltungsUndSonstigePatternSummaryWidgetGesamtsummeTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VERWALTUNGS_UND_SONSTIGE_WIDGET_GESAMTSUMME_CONTENT_00_REGELUNG_TABLE(
		"verwaltungsUndSonstigeWidgetGesamtSummeContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	//--> Steuereinnahmen

	STEUEREINNAHMEN_PATTERN_SUMMARY_TEXT(
		"steuereinnahmenPatternSummaryText",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	STEUEREINNAHMEN_PATTERN_SUMMARY_JAHRESWIRKUNG_TEXT(
		"steuereinnahmenPatternSummaryJahreswirkungText",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	STEUEREINNAHMEN_PATTERN_SUMMARY_WIDGET_BUND_TABLE(
		"steuereinnahmenPatternSummaryWidgetBundTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	STEUEREINNAHMEN_WIDGET_BUND_CONTENT_00_REGELUNG_TABLE(
		"steuereinnahmenWidgetBundContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	STEUEREINNAHMEN_PATTERN_SUMMARY_WIDGET_LAENDER_TABLE(
		"steuereinnahmenPatternSummaryWidgetLaenderTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	STEUEREINNAHMEN_WIDGET_LAENDER_CONTENT_00_REGELUNG_TABLE(
		"steuereinnahmenWidgetLaenderContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	STEUEREINNAHMEN_PATTERN_SUMMARY_WIDGET_GEMEINDEN_TABLE(
		"steuereinnahmenPatternSummaryWidgetGemeindenTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	STEUEREINNAHMEN_WIDGET_GEMEINDEN_CONTENT_00_REGELUNG_TABLE(
		"steuereinnahmenWidgetGemeindenContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	STEUEREINNAHMEN_PATTERN_SUMMARY_WIDGET_GESAMTSUMME_TABLE(
		"steuereinnahmenPatternSummaryWidgetGesamtsummeTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	STEUEREINNAHMEN_WIDGET_GESAMTSUMME_CONTENT_00_REGELUNG_TABLE(
		"steuereinnahmenWidgetGesamtSummeContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	//--> Sonstige Bereiche

	SONSTIGE_BEREICHE_TEXT(
		"sonstigerBereicheText",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	SONSTIGE_BEREICHE_VOLLE_JAHRESWIRKUNG_SUMMARY_TEXT(
		"sonstigerBereicheVolleJahreswirkungSummaryText",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	SONSTIGE_BEREICHE_TABLE(
		"sonstigerBereicheTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	SONSTIGE_BEREICHE_CONTENT_00_REGELUNG_TABLE(
		"sonstigerBereicheContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	SONSTIGE_BEREICHE_GESAMT_TABLE(
		"sonstigerBereicheGesamtTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	SONSTIGE_BEREICHE_GESAMT_CONTENT_00_REGELUNG_TABLE(
		"sonstigerBereicheGesamtContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	SONSTIGE_BEREICHE_EINNAHMEN_TABLE(
		"sonstigerBereicheEinnahmenTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	SONSTIGE_BEREICHE_EINNAHMEN_CONTENT_00_REGELUNG_TABLE(
		"sonstigerBereicheEinnahmenContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	SONSTIGE_BEREICHE_EINNAHMEN_CONTENT_10_REGELUNG_TABLE(
		"sonstigerBereicheEinnahmenContent10RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	SONSTIGE_BEREICHE_EINNAHMEN_GESAMT_TABLE(
		"sonstigerBereicheEinnahmenGesamtTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	SONSTIGE_BEREICHE_EINNAHMEN_GESAMT_CONTENT_00_REGELUNG_TABLE(
		"sonstigerBereicheEinnahmenGesamtContent00RegelungTable",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	//--> Kompensation Kosten

	KOMPENSATION_KOSTEN_TEXT(
		"kompensationKostenText",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	//Abschnitt VI.4: Erfuellungsaufwand

	BEGRUENDUNG(
		"begruendung",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		null,
		null,
		false),

	TEXT_ZUSAMMENFASSUNG_BEGRUENDUNGSTEIL(
		"textZusammenfassungBegruendungsteil",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_ERFUELLUNGSAUFWAND,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		false),

	TEXT_ZUSAMMENFASSUNG_BEGRUENDUNG_BUERGER(
		"textZusammenfassungBegruendungBUERGER",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.BEGRUENDUNG_ERFUELLUNGSAUFWAND_FUER_BUERGERINNEN_UND_BUERGER,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_ERFUELLUNGSAUFWAND,
		false),

	ZUSAMMENFASSUNG_BEGRUENDUNG_BUERGER_GESAMTSUMMEN_TABLE_DATA(
		"zusammenfassungBegruendungBUERGERgesamtsummenTableData",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.BEGRUENDUNG_ERFUELLUNGSAUFWAND_FUER_BUERGERINNEN_UND_BUERGER,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_ERFUELLUNGSAUFWAND,
		true),

	ZUSAMMENFASSUNG_BEGRUENDUNG_BUERGER_JAEHRLICHEN_TABLE_DATA(
		"zusammenfassungBegruendungBUERGERjaehrlichenTableData",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.BEGRUENDUNG_ERFUELLUNGSAUFWAND_FUER_BUERGERINNEN_UND_BUERGER,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_ERFUELLUNGSAUFWAND,
		true),

	ZUSAMMENFASSUNG_BEGRUENDUNG_BUERGER_EINMALIGER_TABLE_DATA(
		"zusammenfassungBegruendungBUERGEReinmaligerTableData",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.BEGRUENDUNG_ERFUELLUNGSAUFWAND_FUER_BUERGERINNEN_UND_BUERGER,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_ERFUELLUNGSAUFWAND,
		true),

	TEXT_ZUSAMMENFASSUNG_BEGRUENDUNG_WIRTSCHAFT(
		"textZusammenfassungBegruendungWIRTSCHAFT",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.BEGRUENDUNG_ERFUELLUNGSAUFWAND_FUER_DIE_WIRTSCHAFT,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_ERFUELLUNGSAUFWAND,
		false),

	ZUSAMMENFASSUNG_BEGRUENDUNG_WIRTSCHAFT_GESAMTSUMMEN_TABLE_DATA(
		"zusammenfassungBegruendungWIRTSCHAFTgesamtsummenTableData",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.BEGRUENDUNG_ERFUELLUNGSAUFWAND_FUER_DIE_WIRTSCHAFT,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_ERFUELLUNGSAUFWAND,
		true),

	ZUSAMMENFASSUNG_BEGRUENDUNG_WIRTSCHAFT_JAEHRLICHEN_TABLE_DATA(
		"zusammenfassungBegruendungWIRTSCHAFTjaehrlichenTableData",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.BEGRUENDUNG_ERFUELLUNGSAUFWAND_FUER_DIE_WIRTSCHAFT,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_ERFUELLUNGSAUFWAND,
		true),

	ZUSAMMENFASSUNG_BEGRUENDUNG_WIRTSCHAFT_INFORMATIONSPFLICHTEN_TABLE_DATA(
		"zusammenfassungBegruendungWIRTSCHAFTinformationspflichtenTableData",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.BEGRUENDUNG_ERFUELLUNGSAUFWAND_FUER_DIE_WIRTSCHAFT,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_ERFUELLUNGSAUFWAND,
		true),

	ZUSAMMENFASSUNG_BEGRUENDUNG_WIRTSCHAFT_EINMALIGER_TABLE_DATA(
		"zusammenfassungBegruendungWIRTSCHAFTeinmaligerTableData",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.BEGRUENDUNG_ERFUELLUNGSAUFWAND_FUER_DIE_WIRTSCHAFT,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_ERFUELLUNGSAUFWAND,
		true),

	TEXT_ZUSAMMENFASSUNG_BEGRUENDUNG_VERWALTUNG(
		"textZusammenfassungBegruendungVERWALTUNG",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.BEGRUENDUNG_ERFUELLUNGSAUFWAND_DER_VERWALTUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_ERFUELLUNGSAUFWAND,
		false),

	ZUSAMMENFASSUNG_BEGRUENDUNG_VERWALTUNG_GESAMTSUMMEN_TABLE_DATA(
		"zusammenfassungBegruendungVERWALTUNGgesamtsummenTableData",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.BEGRUENDUNG_ERFUELLUNGSAUFWAND_DER_VERWALTUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_ERFUELLUNGSAUFWAND,
		true),

	ZUSAMMENFASSUNG_BEGRUENDUNG_VERWALTUNG_JAERLICHEN_TABLE_DATA(
		"zusammenfassungBegruendungVERWALTUNGjaehrlichenTableData",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.BEGRUENDUNG_ERFUELLUNGSAUFWAND_DER_VERWALTUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_ERFUELLUNGSAUFWAND,
		true),

	ZUSAMMENFASSUNG_BEGRUENDUNG_VERWALTUNG_EINMALIGER_TABLE_DATA(
		"zusammenfassungBegruendungVERWALTUNGeinmaligerTableData",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.BEGRUENDUNG_ERFUELLUNGSAUFWAND_DER_VERWALTUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_ERFUELLUNGSAUFWAND,
		true),

	//Abschnitt VI.5: Weitere Kosten

	KMU_ERGEBNISDOKUMENTATION_BEGRUENDUNG(
		"kmuErgebnisdokumentation",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_WEITERE_KOSTEN,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		false),

	KMU_AFFECTED_TEXT_BEGRUENDUNG(
		"kmuAffectedText",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_WEITERE_KOSTEN,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	KMU_SUMMARY_BEGRUENDUNG(
		"kmuSummary",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_WEITERE_KOSTEN,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	PREIS_ERGEBNISDOKUMENTATION_BEGRUENDUNG(
		"preisErgebnisdokumentation",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_WEITERE_KOSTEN,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		false),

	PREIS_SUMMARY_BEGRUENDUNG(
		"preisSummary",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_WEITERE_KOSTEN,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	SONSTIGE_KOSTEN_ERGEBNISDOKUMENTATION_BEGRUENDUNG(
		"sonstigeKostenErgebnisdokumentation",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_WEITERE_KOSTEN,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		false),

	SONSTIGE_KOSTEN_SUMMARY_BEGRUENDUNG(
		"sonstigeKostenSummary",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_WEITERE_KOSTEN,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	//Abschnitt VI.6: Gleichstellungspolitische Relevanzprüfung

	GLEICHSTELLUNGSORIENTIERTE_ERGEBNISDOKUMENTATION(
		"gleichstellungsorientierteErgebnisdokumentation",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_GLEICHSTELLUNGSPOLITISCHE_RELEVANZPRUEFUNG,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		false),

	GLEICHSTELLUNGSORIENTIERTE_DATA_SUMMARY(
		"gleichstellungsorientierteDataSummary",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_GLEICHSTELLUNGSPOLITISCHE_RELEVANZPRUEFUNG,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	DISABILITY_ERGEBNISDOKUMENTATION(
		"disabilityErgebnisdokumentation",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_GLEICHSTELLUNGSPOLITISCHE_RELEVANZPRUEFUNG,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		false),

	DISABILITY_DATA_SUMMARY(
		"disabilityDataSummary",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_GLEICHSTELLUNGSPOLITISCHE_RELEVANZPRUEFUNG,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	DEMOGRAFIE_CHECK_ERGEBNISDOKUMENTATION(
		"demografieCheckErgebnisdokumentation",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_GLEICHSTELLUNGSPOLITISCHE_RELEVANZPRUEFUNG,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		false),

	DEMOGRAFIE_CHECK_DATA_SUMMARY(
		"demografieCheckDataSummary",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_GLEICHSTELLUNGSPOLITISCHE_RELEVANZPRUEFUNG,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	GLEICHWERTIGKEITS_CHECK_ERGEBNISDOKUMENTATION(
		"gleichwertigkeitsCheckErgebnisdokumentation",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_GLEICHSTELLUNGSPOLITISCHE_RELEVANZPRUEFUNG,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		false),

	GLEICHWERTIGKEITS_CHECK_SUMMARY(
		"gleichwertigkeitsCheckSummary",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_GLEICHSTELLUNGSPOLITISCHE_RELEVANZPRUEFUNG,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	VERBRAUCHER_ERGEBNISDOKUMENTATION(
		"verbraucherErgebnisdokumentation",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_GLEICHSTELLUNGSPOLITISCHE_RELEVANZPRUEFUNG,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		false),

	VERBRAUCHER_SUMMARY(
		"verbraucherSummary",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_GLEICHSTELLUNGSPOLITISCHE_RELEVANZPRUEFUNG,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	WEITERE_ERGEBNISDOKUMENTATION(
		"weitereErgebnisdokumentation",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_GLEICHSTELLUNGSPOLITISCHE_RELEVANZPRUEFUNG,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		false),

	WEITERE_SUMMARY(
		"weitereSummary",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_GLEICHSTELLUNGSPOLITISCHE_RELEVANZPRUEFUNG,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN,
		true),

	EVALUIERUNG_ERGEBNISDUKUMENTATION(
		"evaluierungErgebnisdokumentation",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_BEFRISTUNG_EVALUIERUNG,
		null,
		false),

	EVALUIERUNG_DURCHFUEHRUNG_TEXT(
		"evaluierungDurchfuehrungText",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_BEFRISTUNG_EVALUIERUNG,
		null,
		true),

	NO_EVALUIERUNG(
		"noEvaluierung",
		RechtsetzungsdokumentArt.BEGRUENDUNG,
		BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_BEFRISTUNG_EVALUIERUNG,
		null,
		true);

	private static final Map<String, ArrayList<EgfaId>> LITERALS;

	static {
		LITERALS = new HashMap<>();
		Arrays.stream(values())
			.forEach(id -> LITERALS.put(id.getLiteral(), new ArrayList<>()));
		Arrays.stream(values())
			.forEach(id -> LITERALS.get(id.getLiteral())
				.add(id));
	}

	private final String literal;
	private final RechtsetzungsdokumentArt regardingDocType;
	private final RefersToLiteral parentRef;
	private final RefersToLiteral grandParentRef;
	private final boolean hasLabel;


	public static List<EgfaId> fromLiteral(String literal) {
		if (Utils.isMissing(literal)) {
			return List.of();
		} else {
			return LITERALS.get(literal);
		}
	}

}
