// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.compare;

import de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils;
import de.itzbund.egesetz.bmi.lea.core.compare.ElementChangeMarker;
import de.itzbund.egesetz.bmi.lea.core.compare.JSONObjectListComparer;
import de.itzbund.egesetz.bmi.lea.core.compare.ObjectListDiff;
import de.itzbund.egesetz.bmi.lea.core.compare.TextChangeMarker;
import de.itzbund.egesetz.bmi.lea.core.numbering.GenericNumberingScheme;
import de.itzbund.egesetz.bmi.lea.core.numbering.NumberingScheme;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.ports.eid.EId;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.BezeichnerImpl;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.EIdImpl;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_EID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.CHANGE_MARK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_AUTHORIALNOTE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BLOCK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BLOCKLIST;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CONTENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FOREIGN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_HEADING;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_INTRO;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LISTINTRODUCTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LISTWRAPUP;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LONGTITLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_NUM;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_OL;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_P;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TABLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TBLOCK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TOC;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_UL;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_WRAPUP;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_TEXT_WRAPPER;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.markChangesInText;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.markChangesInTextWrapper;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.markContent;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.markMatchingPair;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.markStructure;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.markText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.appendText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.fromJSONArray;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getEffectiveTextValue;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getFirstChild;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getGuid;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getHeadingOfElement;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getLocalName;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getNthChild;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getNumElement;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getParas;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getStringAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getTextWrapper;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.isLegalNorm;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewDefaultJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.mergeTextContent;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.prependText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.replaceChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.setGuid;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.setText;
import static de.itzbund.egesetz.bmi.lea.domain.assertion.DomainAssertions.assertComparableTypes;
import static de.itzbund.egesetz.bmi.lea.domain.assertion.DomainAssertions.assertConsistentCompareResult;
import static de.itzbund.egesetz.bmi.lea.domain.assertion.DomainAssertions.assertNotEmptyChildrenArray;
import static de.itzbund.egesetz.bmi.lea.domain.assertion.DomainAssertions.assertSizeIsAtLeast;
import static de.itzbund.egesetz.bmi.lea.domain.assertion.DomainAssertions.assertValidParaContent;
import static de.itzbund.egesetz.bmi.lea.domain.assertion.DomainAssertions.assertWrapUpHasExactlyOneP;
import static de.itzbund.egesetz.bmi.lea.domain.services.eid.Abkuerzungsliteral.UEBERSCHRIFT;

@Log4j2
@SuppressWarnings("unchecked")
public class RegulatoryTextComparer extends AbstractDocumentComparer {

    private static final ResourceBundle MESSAGES =
        ResourceBundle.getBundle("de.itzbund.egesetz.bmi.lea.domain.messages.DomainResourceBundle");

    private static final MessageFormat MSG_FMT_UNSUPPORTED_CONTENT = new MessageFormat(
        MESSAGES.getString("msg.unsupported.content"));

    public static final String STRING_FMT_SYNOPSE_HEADING = MESSAGES.getString("synopse.heading");

    private static final DateFormat SDF = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    private static final NumberingScheme LISTITEM_SYMBOLS =
        new GenericNumberingScheme("1.;a);(i);a.;1.", "•", false);

    private static final Set<String> UNSUPPORTED_CONTENT_TYPES = Stream.of(
            ELEM_FOREIGN, ELEM_TBLOCK, ELEM_TOC)
        .collect(Collectors.toCollection(HashSet::new));

    private static final HashSet<String> LIST_INTRO_OR_WRAPUP = Stream.of(ELEM_INTRO, ELEM_WRAPUP).collect(Collectors.toCollection(HashSet::new));

    private static final int JUDICIAL_PARA_MIN_SIZE = 2;

    @Getter
    private List<Pair<JSONObject, JSONObject>> table;

    public enum RegulatoryTextMetadata {
        LAST_MODIFICATION_DATE,
        TYPE
    }

    @Override
    public void compare(@NonNull JSONObject baseDocumentObject, Map<String, Object> baseMetadata,
        @NonNull JSONObject versionDocumentObject, Map<String, Object> versionMetadata) {
        initialize(baseDocumentObject, versionDocumentObject);
        table = new ArrayList<>();
        initializeTable(baseMetadata, versionMetadata);

        // longTitle
        JSONObject baseTitle = Utils.getFirstOrDefault(baseDocument.getElementsOfType(ELEM_LONGTITLE), null);
        JSONObject versionTitle = Utils.getFirstOrDefault(versionDocument.getElementsOfType(ELEM_LONGTITLE), null);

        if (baseTitle != null && versionTitle != null) {
            compareLongTitle(baseTitle, versionTitle);
        }

        // content
        compareHierarchyLevels();
    }

    private void initializeTable(Map<String, Object> baseMetadata, Map<String, Object> versionMetadata) {
        Instant baseDate = (Instant) baseMetadata.get(RegulatoryTextMetadata.LAST_MODIFICATION_DATE.name());
        Instant versionDate = (Instant) versionMetadata.get(RegulatoryTextMetadata.LAST_MODIFICATION_DATE.name());
        table.add(Pair.of(getDateBlock(baseDate), getDateBlock(versionDate)));
    }

    private JSONObject getDateBlock(Instant date) {
        JSONObject dateObject = makeNewDefaultJSONObject(ELEM_BLOCK);
        addAttribute(dateObject, Pair.of(CHANGE_MARK, ElementChangeMarker.MATCHED.getType()));
        addText(dateObject, String.format(STRING_FMT_SYNOPSE_HEADING, SDF.format(date.toEpochMilli())));
        markText(dateObject, TextChangeMarker.UNCHANGED);
        return dateObject;
    }


    // for the time being this is a crude solution expecting always the same structure of an akn:longTitle
    private void compareLongTitle(@NonNull JSONObject baseTitle, @NonNull JSONObject versionTitle) {
        JSONObject baseP = getFirstChild(baseTitle);
        JSONObject versionP = getFirstChild(versionTitle);
        List<Pair<JSONObject, JSONObject>> pairs = Utils.zipList(
            fromJSONArray(getChildren(baseP)),
            fromJSONArray(getChildren(versionP)),
            false, null, null
        );

        for (Pair<JSONObject, JSONObject> p : pairs) {
            markMatchingPair(p.getLeft(), p.getRight());
            makeTextCompare(p.getLeft(), p.getRight());
        }

        putLongTitleIntoTable(pairs);
    }

    private void putLongTitleIntoTable(List<Pair<JSONObject, JSONObject>> pairs) {
        int index = -1;

        if (pairs.size() == 1) {
            table.add(pairs.get(0));
        } else {
            // docStage
            index++;
            table.add(pairs.get(index));

            // docProponent
            index++;
            Pair<JSONObject, JSONObject> articles = pairs.get(index);
            index++;
            Pair<JSONObject, JSONObject> proponents = pairs.get(index);
            table.add(getDocProponents(articles, proponents));

            // docTitle
            index++;
            table.add(pairs.get(index));

            // shortTitle & abbreviation
            index++;
            index++;
            Pair<JSONObject, JSONObject> shortTitles = pairs.get(index);
            index++;
            index++;
            Pair<JSONObject, JSONObject> abbreviations = pairs.get(index);
            table.add(getShortTitles(shortTitles, abbreviations));
        }
    }

    private Pair<JSONObject, JSONObject> getDocProponents(Pair<JSONObject, JSONObject> articles, Pair<JSONObject, JSONObject> proponents) {
        JSONObject baseProponentObject = mergeTextContent(1, " ", "", articles.getLeft(), proponents.getLeft());
        JSONObject versionProponentObject = mergeTextContent(1, " ", "", articles.getRight(), proponents.getRight());
        return Pair.of(baseProponentObject, versionProponentObject);
    }

    private Pair<JSONObject, JSONObject> getShortTitles(Pair<JSONObject, JSONObject> shortTitles, Pair<JSONObject, JSONObject> abbreviations) {
        JSONObject baseShortTitleObject = mergeTextContent(0, " – ", "", shortTitles.getLeft(), abbreviations.getLeft());
        prependText(baseShortTitleObject, "(");
        appendText(baseShortTitleObject, ")");
        JSONObject versionShortTitleObject = mergeTextContent(0, " – ", "", shortTitles.getRight(), abbreviations.getRight());
        prependText(versionShortTitleObject, "(");
        appendText(versionShortTitleObject, ")");
        return Pair.of(baseShortTitleObject, versionShortTitleObject);
    }


    private void compareHierarchyLevels() {
        JSONObjectListComparer comparer = new JSONObjectListComparer(baseDocument, versionDocument);
        comparer.compare(baseDocument.getHierarchyLevels(), versionDocument.getHierarchyLevels());
        LinkedList<ObjectListDiff<UUID>> keyDiffs = comparer.getKeyDiffs();

        keyDiffs.forEach(diff -> {
            DiffMatchPatch.Operation op = diff.getOperation();
            JSONObject baseObject = baseDocument.getObject(diff.getKey().toString());
            JSONObject versionObject = versionDocument.getObject(diff.getKey().toString());
            assertConsistentCompareResult(baseObject, versionObject, op);

            // it's a hierarchy level, i.e. only num and heading elements need to be inserted into table
            switch (op) {
                case DELETE:
                    handleInsertedOrDeletedHierarchy(baseObject, CompareUtils.Origin.BASE);
                    break;
                case INSERT:
                    handleInsertedOrDeletedHierarchy(versionObject, CompareUtils.Origin.VERSION);
                    break;
                case EQUAL:
                    handleMatchingLevels(baseObject, versionObject);
            }
        });
    }

    private void handleInsertedOrDeletedHierarchy(JSONObject jsonObject, CompareUtils.Origin origin) {
        ElementChangeMarker marker = origin == CompareUtils.Origin.BASE ? ElementChangeMarker.DELETED : ElementChangeMarker.ADDED;
        JSONObject numObject = getNumElement(jsonObject);
        JSONObject headingObject = getHeadingOfElement(jsonObject);
        markContent(jsonObject, marker);
        markContent(numObject, marker);
        markContent(headingObject, marker);

        if (origin == CompareUtils.Origin.BASE) {
            table.add(Pair.of(numObject, null));
            table.add(Pair.of(headingObject, null));
        } else {
            table.add(Pair.of(null, numObject));
            table.add(Pair.of(null, headingObject));
        }

        if (isLegalNorm(jsonObject)) {
            compareInsertedOrDeletedNorm(jsonObject, origin);
        }
    }

    private void handleMatchingLevels(JSONObject baseObject, JSONObject versionObject) {
        JSONObject baseNumObject = getNumElement(baseObject);
        JSONObject baseHeading = getHeadingOfElement(baseObject);
        JSONObject versionNumObject = getNumElement(versionObject);
        JSONObject versionHeading = getHeadingOfElement(versionObject);

        ElementChangeMarker marker = ElementChangeMarker.MATCHED;
        markContent(baseObject, marker);
        markContent(baseNumObject, marker);
        baseHeading = markPossibleEmptyHeading(baseHeading, getStringAttribute(baseHeading, ATTR_EID), marker, CompareUtils.Origin.BASE);
        markContent(versionObject, marker);
        markContent(versionNumObject, marker);
        versionHeading = markPossibleEmptyHeading(versionHeading, getStringAttribute(versionHeading, ATTR_EID), marker, CompareUtils.Origin.VERSION);
        makeTextCompare(baseNumObject, versionNumObject);
        makeTextCompare(baseHeading, versionHeading);
        table.add(Pair.of(baseNumObject, versionNumObject));
        table.add(Pair.of(baseHeading, versionHeading));

        if (isLegalNorm(baseObject) || isLegalNorm(versionObject)) {
            compareMatchingNorms(baseObject, versionObject);
        }
    }

    private JSONObject markPossibleEmptyHeading(JSONObject headingObject, String parentEId, ElementChangeMarker marker, CompareUtils.Origin origin) {
        if (headingObject == null) {
            ElementChangeMarker otherMarker = origin == CompareUtils.Origin.BASE ? ElementChangeMarker.ADDED : ElementChangeMarker.DELETED;
            headingObject = makeNewDefaultJSONObject(ELEM_HEADING);
            if (!Utils.isMissing(parentEId)) {
                EId parentId = EIdImpl.fromString(parentEId);
                assert parentId != null;
                String eId = parentId.extendBy(BezeichnerImpl.newInstance(UEBERSCHRIFT.getAbkuerzung(), 1)).toString();
                addAttribute(headingObject, Pair.of(ATTR_EID, eId));
            }
            setText(headingObject, "");
            markContent(headingObject, otherMarker);
        } else {
            markContent(headingObject, marker);
        }
        return headingObject;
    }

    // num and heading have been already handled by previous method
    private void compareInsertedOrDeletedNorm(JSONObject jsonObject, CompareUtils.Origin origin) {
        ElementChangeMarker marker = origin == CompareUtils.Origin.BASE ? ElementChangeMarker.DELETED : ElementChangeMarker.ADDED;
        List<JSONObject> paras = getParas(jsonObject);
        paras.forEach(para -> {
            markContent(para, marker);
            JSONArray paraChildren = getChildren(para);
            assertNotEmptyChildrenArray(paraChildren);
            JSONObject numObject = (JSONObject) paraChildren.get(0);
            markContent(numObject, marker);

            for (int i = 1; i < paraChildren.size(); i++) {
                JSONObject child = (JSONObject) paraChildren.get(i);
                String type = getType(child);
                if (ELEM_CONTENT.equals(getLocalName(type))) {
                    handleParaContent(child, numObject, marker, origin);
                } else {
                    handleParaList(child, numObject, marker, origin);
                }
            }
        });
    }

    private void handleParaContent(JSONObject contentObject, JSONObject numObject, ElementChangeMarker marker, CompareUtils.Origin origin) {
        JSONArray contentChildren = getChildren(contentObject);
        assertNotEmptyChildrenArray(contentChildren);

        for (int i = 0; i < contentChildren.size(); i++) {
            JSONObject child = (JSONObject) contentChildren.get(i);
            List<JSONObject> numObjects = new ArrayList<>();
            if (i == 0) {
                numObjects.add(numObject);
            }
            handleParaContentElement(child, numObjects, marker, origin, 0);
        }
    }

    private void handleParaContentElement(JSONObject jsonObject, List<JSONObject> numObjects, ElementChangeMarker marker,
        CompareUtils.Origin origin, int depth) {
        if (isUnsupportedContent(jsonObject)) {
            handleUnsupportedContent(jsonObject, numObjects, origin);
        } else {
            String childType = getLocalName(getType(jsonObject));
            switch (childType) {
                case ELEM_BLOCK:
                case ELEM_P:
                    handleTextBlock(jsonObject, numObjects, marker, origin);
                    break;
                case ELEM_BLOCKLIST:
                    handleBlockList(jsonObject, numObjects, marker, origin);
                    break;
                case ELEM_OL:
                case ELEM_UL:
                    handleHTMLList(jsonObject, numObjects, marker, origin, childType.equals(ELEM_OL), depth);
                    break;
                case ELEM_TABLE:
                    handleTable(jsonObject, numObjects, origin);
                    break;
                default:
                    // no action taken
            }
        }
    }

    private void handleTextBlock(JSONObject jsonObject, List<JSONObject> numObjects, ElementChangeMarker marker, CompareUtils.Origin origin) {
        markContent(jsonObject, marker);
        JSONArray children = getChildren(jsonObject);
        assertNotEmptyChildrenArray(children);

        JSONArray extChildren = new JSONArray();
        extChildren.addAll(numObjects);
        children.forEach(obj -> {
            JSONObject child = (JSONObject) obj;
            String type = getLocalName(getType(child));
            if (!ELEM_AUTHORIALNOTE.equals(type)) {
                extChildren.add(child);
            }
        });

        replaceChildren(jsonObject, extChildren);

        if (origin == CompareUtils.Origin.BASE) {
            table.add(Pair.of(jsonObject, null));
        } else {
            table.add(Pair.of(null, jsonObject));
        }
    }

    private void handleBlockList(JSONObject listObject, List<JSONObject> numObjects, ElementChangeMarker marker, CompareUtils.Origin origin) {
        markContent(listObject, marker);
        JSONArray listChildren = getChildren(listObject);
        assertNotEmptyChildrenArray(listChildren);

        listChildren.forEach(obj -> {
            JSONObject child = (JSONObject) obj;
            String childType = getLocalName(getType(child));
            switch (childType) {
                case ELEM_LISTINTRODUCTION:
                case ELEM_LISTWRAPUP:
                    handleTextBlock(child, numObjects, marker, origin);
                    break;
                default:
                    handleBlockListItem(child, numObjects, marker, origin);
            }
        });
    }

    private void handleBlockListItem(JSONObject listItemObject, List<JSONObject> numObjects, ElementChangeMarker marker, CompareUtils.Origin origin) {
        markContent(listItemObject, marker);
        JSONArray itemChildren = getChildren(listItemObject);
        assertSizeIsAtLeast(itemChildren, 2); //NOSONAR
        JSONObject itemNumObject = (JSONObject) itemChildren.get(0);
        int startIndex = 1;
        List<JSONObject> secondNumList = new ArrayList<>();

        List<JSONObject> firstNumList = new ArrayList<>(numObjects);
        firstNumList.add(itemNumObject);
        JSONObject optionalHeading = (JSONObject) itemChildren.get(startIndex);
        if (ELEM_HEADING.equals(getLocalName(getType(optionalHeading)))) {
            firstNumList.add(optionalHeading);
            startIndex++;
        }

        for (int i = startIndex; i < itemChildren.size(); i++) {
            JSONObject child = (JSONObject) itemChildren.get(i);
            if (i == startIndex) {
                handleParaContentElement(child, firstNumList, marker, origin, 0);
            } else {
                handleParaContentElement(child, secondNumList, marker, origin, 0);
            }
        }
    }

    private void handleHTMLList(JSONObject listObject, List<JSONObject> numObjects, ElementChangeMarker marker, CompareUtils.Origin origin,
        boolean ordered, int depth) {
        markContent(listObject, marker);
        JSONArray listChildren = getChildren(listObject);
        assertNotEmptyChildrenArray(listChildren);
        List<JSONObject> symbolList = new ArrayList<>(numObjects);

        for (int i = 0; i < listChildren.size(); i++) {
            String symbol = LISTITEM_SYMBOLS.getSymbol(i + 1, depth, ordered);
            JSONObject symbolObject = makeNewDefaultJSONObject(ELEM_NUM);
            setText(symbolObject, symbol);
            markContent(symbolObject, marker);

            if (i > 0) {
                symbolList = new ArrayList<>();
            }
            symbolList.add(symbolObject);

            handleParaContentElement((JSONObject) listChildren.get(i), symbolList, marker, origin, depth + 1);
        }
    }

    private void handleTable(JSONObject tableObject, List<JSONObject> numObjects, CompareUtils.Origin origin) {
        // will be handled later
    }

    private void handleParaList(JSONObject listObject, JSONObject numObject, ElementChangeMarker marker, CompareUtils.Origin origin) {
        UUID guid = UUID.fromString(getGuid(listObject));
        JSONObject listIntro = getFirstChild(listObject);
        markContent(listIntro, marker);
        JSONArray listChildren = getChildren(listObject);
        List<JSONObject> listParts = getAllChildrenButFirst(listChildren);
        JSONObject blockObject = getContentBlockObject(guid, numObject, listIntro);
        table.add(origin == CompareUtils.Origin.BASE
            ? Pair.of(blockObject, null)
            : Pair.of(null, blockObject));
        putListPartsIntoTable(listParts, origin, marker);
    }

    private boolean isUnsupportedContent(JSONObject jsonObject) {
        return UNSUPPORTED_CONTENT_TYPES.contains(getLocalName(getType(jsonObject)));
    }

    private void handleUnsupportedContent(JSONObject jsonObject, List<JSONObject> numObjects, CompareUtils.Origin origin) {
        JSONObject baseObject = origin == CompareUtils.Origin.BASE ? jsonObject : null;
        JSONObject versionObject = baseObject == null ? jsonObject : null;
        List<JSONObject> baseNumObjects = baseObject == null ? List.of() : numObjects;
        List<JSONObject> versionNumObjects = baseObject == null ? numObjects : List.of();

        String baseText = baseObject != null ? MSG_FMT_UNSUPPORTED_CONTENT.format(getGuid(baseObject)) : "";
        String versionText = versionObject != null ? MSG_FMT_UNSUPPORTED_CONTENT.format(getGuid(versionObject)) : "";
        JSONObject baseBlock = getTextBlock(baseText, baseNumObjects);
        JSONObject versionBlock = getTextBlock(versionText, versionNumObjects);
        table.add(Pair.of(baseBlock, versionBlock));
    }

    private JSONObject getTextBlock(String text, List<JSONObject> numObjects) {
        JSONObject blockObject = makeNewDefaultJSONObject(ELEM_BLOCK);
        JSONObject textNode = getTextWrapper(text);
        JSONArray blockChildren = new JSONArray();
        blockChildren.addAll(numObjects);
        blockChildren.add(textNode);
        replaceChildren(blockObject, blockChildren);

        return blockObject;
    }


    // num and heading have already been handled
    private void compareMatchingNorms(JSONObject baseNorm, JSONObject versionNorm) {
        JSONObjectListComparer comparer = new JSONObjectListComparer(baseDocument, versionDocument);
        comparer.compare(getParas(baseNorm), getParas(versionNorm));
        LinkedList<ObjectListDiff<UUID>> keyDiffs = comparer.getKeyDiffs();

        keyDiffs.forEach(diff -> {
            DiffMatchPatch.Operation op = diff.getOperation();
            JSONObject baseObject = baseDocument.getObject(diff.getKey().toString());
            JSONObject versionObject = versionDocument.getObject(diff.getKey().toString());
            assertConsistentCompareResult(baseObject, versionObject, op);

            switch (op) {
                case DELETE:
                    handleInsertedOrDeletedJudicialPara(baseObject, CompareUtils.Origin.BASE);
                    break;
                case INSERT:
                    handleInsertedOrDeletedJudicialPara(versionObject, CompareUtils.Origin.VERSION);
                    break;
                case EQUAL:
                    handleMatchingParas(baseObject, versionObject);
            }
        });
    }

    private void handleInsertedOrDeletedJudicialPara(JSONObject paraObject, CompareUtils.Origin origin) {
        ElementChangeMarker marker = origin == CompareUtils.Origin.BASE ? ElementChangeMarker.DELETED : ElementChangeMarker.ADDED;
        JSONObject numObject = getNumElement(paraObject);
        markStructure(paraObject, marker);
        markStructure(numObject, marker);

        JSONArray paraChildren = getChildren(paraObject);
        for (int i = 1; i < paraChildren.size(); i++) {
            JSONObject child = (JSONObject) paraChildren.get(i);
            String type = getType(child);
            if (ELEM_CONTENT.equals(getLocalName(type))) {
                handleParaContent(child, numObject, marker, origin);
            } else {
                handleParaList(child, numObject, marker, origin);
            }
        }
    }

    private void handleMatchingParas(JSONObject basePara, JSONObject versionPara) {
        ElementChangeMarker marker = ElementChangeMarker.MATCHED;
        markContent(basePara, marker);
        markContent(versionPara, marker);

        JSONArray baseChildren = getChildren(basePara);
        assertSizeIsAtLeast(baseChildren, JUDICIAL_PARA_MIN_SIZE);
        JSONObject baseNumObject = (JSONObject) baseChildren.get(0);
        JSONObject baseAfterNumObject = (JSONObject) baseChildren.get(1);
        String baseType = getLocalName(getType(baseAfterNumObject));
        UUID baseParaId = UUID.fromString(getGuid(basePara));
        markContent(baseNumObject, marker);

        JSONArray versionChildren = getChildren(versionPara);
        assertSizeIsAtLeast(versionChildren, JUDICIAL_PARA_MIN_SIZE);
        JSONObject versionNumObject = (JSONObject) versionChildren.get(0);
        JSONObject versionAfterNumObject = (JSONObject) versionChildren.get(1);
        String versionType = getLocalName(getType(versionAfterNumObject));
        UUID versionParaId = UUID.fromString(getGuid(versionPara));
        markContent(versionNumObject, marker);

        assertValidParaContent(baseType, versionType);
        makeTextCompare(baseNumObject, versionNumObject);
        if (baseType.equals(ELEM_CONTENT)) {
            if (versionType.equals(ELEM_CONTENT)) {
                // there can only be one content element per para
                matchContentWithContent(baseParaId, baseNumObject, baseAfterNumObject, versionParaId, versionNumObject, versionAfterNumObject);
            } else {
                matchContentWithLists(baseParaId, baseNumObject, baseChildren, versionParaId, versionNumObject, versionChildren, CompareUtils.Origin.VERSION);
            }
        } else {
            if (versionType.equals(ELEM_CONTENT)) {
                matchContentWithLists(baseParaId, baseNumObject, baseChildren, versionParaId, versionNumObject, versionChildren, CompareUtils.Origin.BASE);
            } else {
                matchListsWithLists(baseParaId, baseNumObject, baseChildren, versionParaId, versionNumObject, versionChildren);
            }
        }
    }


    private void matchContentWithContent(UUID baseId, JSONObject baseNumObject, JSONObject baseContentObject,
        UUID versionId, JSONObject versionNumObject, JSONObject versionContentObject) {
        makeTextCompare(baseContentObject, versionContentObject);
        table.add(Pair.of(
            getContentBlockObject(baseId, baseNumObject, baseContentObject),
            getContentBlockObject(versionId, versionNumObject, versionContentObject)
        ));
    }


    private JSONObject getContentBlockObject(UUID guid, JSONObject numObject, JSONObject contentObject) {
        JSONObject blockObject = makeNewJSONObject(ELEM_BLOCK, true, false);
        setGuid(blockObject, guid);
        addChildren(blockObject, getChildren(numObject));
        addChildren(blockObject, getChildren(contentObject));
        return blockObject;
    }


    private void matchContentWithLists(UUID baseId, JSONObject baseNumObject, JSONArray baseChildren,
        UUID versionId, JSONObject versionNumObject, JSONArray versionChildren, CompareUtils.Origin sideOfLists) {
        List<JSONObject> listParts = new ArrayList<>();
        JSONArray listChildren = sideOfLists == CompareUtils.Origin.BASE ? baseChildren : versionChildren;
        List<JSONObject> lists = getAllChildrenButFirst(listChildren);

        lists.forEach(list -> listParts.addAll(fromJSONArray(getChildren(list))));
        JSONObject contentObject = (JSONObject) (sideOfLists == CompareUtils.Origin.BASE ? versionChildren.get(1) : baseChildren.get(1));
        JSONObject introObject = listParts.get(0);
        if (sideOfLists == CompareUtils.Origin.BASE) {
            makeTextCompare(introObject, contentObject);
            table.add(Pair.of(
                getContentBlockObject(baseId, baseNumObject, introObject),
                getContentBlockObject(versionId, versionNumObject, contentObject)
            ));
        } else {
            makeTextCompare(contentObject, introObject);
            table.add(Pair.of(
                getContentBlockObject(baseId, baseNumObject, contentObject),
                getContentBlockObject(versionId, versionNumObject, introObject)
            ));
        }

        ElementChangeMarker marker = sideOfLists == CompareUtils.Origin.BASE ? ElementChangeMarker.DELETED : ElementChangeMarker.ADDED;
        putListPartsIntoTable(listParts.subList(1, listParts.size()), sideOfLists, marker);
    }


    private void putListPartsIntoTable(List<JSONObject> listParts, CompareUtils.Origin sideOfLists, ElementChangeMarker marker) {
        listParts.forEach(part -> {
            String type = getLocalName(getType(part));
            if (LIST_INTRO_OR_WRAPUP.contains(type)) {
                markStructure(part, marker);
                getChildren(part).forEach(p -> markContent((JSONObject) p, marker));
                table.add(sideOfLists == CompareUtils.Origin.BASE
                    ? Pair.of(part, null)
                    : Pair.of(null, part));
            } else {
                // akn:point, always has akn:num as first child, followed by content or list
                JSONObject numObject = getFirstChild(part);
                markStructure(numObject, marker);
                JSONObject contentOrList = getNthChild(part, 1);
                markStructure(contentOrList, marker);
                UUID pointGuid = UUID.fromString(getGuid(part));
                String childType = getLocalName(getType(contentOrList));
                if (ELEM_CONTENT.equals(childType)) {
                    JSONObject blockObject = getContentBlockObject(pointGuid, numObject, contentOrList);
                    table.add(sideOfLists == CompareUtils.Origin.BASE
                        ? Pair.of(blockObject, null)
                        : Pair.of(null, blockObject));
                } else {
                    JSONObject subListIntro = getFirstChild(contentOrList);
                    JSONArray subListChildren = getChildren(contentOrList);
                    List<JSONObject> subListParts = getAllChildrenButFirst(subListChildren);
                    JSONObject blockObject = getContentBlockObject(pointGuid, numObject, subListIntro);
                    table.add(sideOfLists == CompareUtils.Origin.BASE
                        ? Pair.of(blockObject, null)
                        : Pair.of(null, blockObject));
                    putListPartsIntoTable(subListParts, sideOfLists, marker);
                }
            }
        });
    }


    private void matchListsWithLists(UUID baseId, JSONObject baseNumObject, JSONArray baseChildren,
        UUID versionId, JSONObject versionNumObject, JSONArray versionChildren) {
        JSONObjectListComparer comparer = new JSONObjectListComparer(baseDocument, versionDocument);
        List<JSONObject> baseLists = getAllChildrenButFirst(baseChildren);
        List<JSONObject> versionLists = getAllChildrenButFirst(versionChildren);
        comparer.compare(baseLists, versionLists);
        LinkedList<ObjectListDiff<UUID>> keyDiffs = comparer.getKeyDiffs();

        keyDiffs.forEach(diff -> {
            DiffMatchPatch.Operation op = diff.getOperation();
            JSONObject baseListObject = baseDocument.getObject(diff.getKey().toString());
            JSONObject versionListObject = versionDocument.getObject(diff.getKey().toString());
            assertConsistentCompareResult(baseListObject, versionListObject, op);

            switch (op) {
                case DELETE:
                    handleInsertedOrDeletedList(baseId, baseNumObject, baseListObject, CompareUtils.Origin.BASE);
                    break;
                case INSERT:
                    handleInsertedOrDeletedList(versionId, versionNumObject, versionListObject, CompareUtils.Origin.VERSION);
                    break;
                case EQUAL:
                    handleMatchingLists(baseNumObject, baseListObject, versionNumObject, versionListObject);
            }
        });
    }


    private void handleInsertedOrDeletedList(UUID paraId, JSONObject numObject, JSONObject listObject, CompareUtils.Origin origin) {
        ElementChangeMarker marker = origin == CompareUtils.Origin.BASE ? ElementChangeMarker.DELETED : ElementChangeMarker.ADDED;
        JSONObject introObject = getFirstChild(listObject);
        markContent(introObject, marker);
        JSONObject blockObject = getContentBlockObject(paraId, numObject, getFirstChild(listObject));
        table.add(origin == CompareUtils.Origin.BASE
            ? Pair.of(blockObject, null)
            : Pair.of(null, blockObject));

        List<JSONObject> listParts = getAllChildrenButFirst(getChildren(listObject));
        putListPartsIntoTable(listParts, origin, marker);
    }


    private void handleMatchingLists(JSONObject baseNumObject, JSONObject baseListObject, JSONObject versionNumObject, JSONObject versionListObject) {
        JSONArray baseListChildren = getChildren(baseListObject);
        JSONObject baseIntroObject = getFirstChild(baseListObject);
        int lastPointIndex = baseListChildren.size() - 1;
        JSONObject baseWrapUpObject = getNthChild(baseListObject, lastPointIndex);
        if (!ELEM_WRAPUP.equals(getLocalName(getType(baseWrapUpObject)))) {
            baseWrapUpObject = null;
        } else {
            lastPointIndex -= 1;
        }
        List<JSONObject> basePoints = fromJSONArray(baseListChildren).subList(1, lastPointIndex);
        UUID baseId = UUID.fromString(getGuid(baseListObject));

        JSONArray versionListChildren = getChildren(versionListObject);
        JSONObject versionIntroObject = getFirstChild(baseListObject);
        lastPointIndex = versionListChildren.size() - 1;
        JSONObject versionWrapUpObject = getNthChild(versionListObject, lastPointIndex);
        if (!ELEM_WRAPUP.equals(getLocalName(getType(versionWrapUpObject)))) {
            versionWrapUpObject = null;
        } else {
            lastPointIndex -= 1;
        }
        List<JSONObject> versionPoints = fromJSONArray(versionListChildren).subList(1, lastPointIndex);
        UUID versionId = UUID.fromString(getGuid(versionListObject));

        matchListIntros(baseId, baseNumObject, baseIntroObject, versionId, versionNumObject, versionIntroObject);
        matchListPoints(basePoints, versionPoints);
        matchWrapUps(baseWrapUpObject, versionWrapUpObject);
    }


    private void matchListIntros(UUID baseId, JSONObject baseNumObject, JSONObject baseIntroObject,
        UUID versionId, JSONObject versionNumObject, JSONObject versionIntroObject) {
        makeTextCompare(baseIntroObject, versionIntroObject);
        table.add(Pair.of(
            getContentBlockObject(baseId, baseNumObject, baseIntroObject),
            getContentBlockObject(versionId, versionNumObject, versionIntroObject)
        ));
    }


    private void matchListPoints(List<JSONObject> basePoints, List<JSONObject> versionPoints) {
        JSONObjectListComparer comparer = new JSONObjectListComparer(baseDocument, versionDocument);
        comparer.compare(basePoints, versionPoints);
        LinkedList<ObjectListDiff<UUID>> keyDiffs = comparer.getKeyDiffs();

        keyDiffs.forEach(diff -> {
            DiffMatchPatch.Operation op = diff.getOperation();
            JSONObject basePointObject = baseDocument.getObject(diff.getKey().toString());
            JSONObject versionPointObject = versionDocument.getObject(diff.getKey().toString());
            assertConsistentCompareResult(basePointObject, versionPointObject, op);

            switch (op) {
                case DELETE:
                    handleInsertedOrDeletedListPoint(basePointObject, CompareUtils.Origin.BASE);
                    break;
                case INSERT:
                    handleInsertedOrDeletedListPoint(versionPointObject, CompareUtils.Origin.VERSION);
                    break;
                case EQUAL:
                    handleMatchingListPoints(basePointObject, versionPointObject);
            }
        });
    }


    private void handleInsertedOrDeletedListPoint(JSONObject pointObject, CompareUtils.Origin origin) {
        handleInsertedOrDeletedJudicialPara(pointObject, origin);
    }


    private void handleMatchingListPoints(JSONObject basePointObject, JSONObject versionPointObject) {
        handleMatchingParas(basePointObject, versionPointObject);
    }


    private void matchWrapUps(JSONObject baseWrapUpObject, JSONObject versionWrapUpObject) {
        if (baseWrapUpObject == null) {
            if (versionWrapUpObject != null) {
                assertWrapUpHasExactlyOneP(versionWrapUpObject);
                JSONObject pObject = (JSONObject) getChildren(versionWrapUpObject).get(0);
                markContent(pObject, ElementChangeMarker.ADDED);
                table.add(Pair.of(null, pObject));
            }
        } else {
            if (versionWrapUpObject == null) {
                assertWrapUpHasExactlyOneP(baseWrapUpObject);
                JSONObject pObject = (JSONObject) getChildren(baseWrapUpObject).get(0);
                markContent(pObject, ElementChangeMarker.ADDED);
                table.add(Pair.of(pObject, null));
            } else {
                assertWrapUpHasExactlyOneP(baseWrapUpObject);
                JSONObject basePObject = (JSONObject) getChildren(baseWrapUpObject).get(0);
                assertWrapUpHasExactlyOneP(versionWrapUpObject);
                JSONObject versionPObject = (JSONObject) getChildren(versionWrapUpObject).get(0);
                makeTextCompare(basePObject, versionPObject);
                table.add(Pair.of(basePObject, versionPObject));
            }
        }
    }


    private void makeTextCompare(JSONObject baseObject, JSONObject versionObject) {
        String baseType = getType(baseObject);
        String versionType = getType(versionObject);
        assertComparableTypes(baseType, versionType);

        String baseText = getEffectiveTextValue(baseObject);
        String versionText = getEffectiveTextValue(versionObject);
        LinkedList<DiffMatchPatch.Diff> diffs = DMP.diffMain(baseText, versionText);
        DMP.diffCleanupSemantic(diffs);

        if (diffs.isEmpty()) {
            diffs.add(new DiffMatchPatch.Diff(DiffMatchPatch.Operation.EQUAL, baseText));
        }

        if (JSON_VAL_TEXT_WRAPPER.equals(baseType)) {
            markChangesInTextWrapper(baseObject, versionObject, diffs);
        } else {
            markChangesInText(baseObject, versionObject, diffs);
        }
    }

    private List<JSONObject> getAllChildrenButFirst(JSONArray jsonArray) {
        if (jsonArray == null || jsonArray.isEmpty()) {
            return new ArrayList<>();
        } else {
            return fromJSONArray(jsonArray).subList(1, jsonArray.size());
        }
    }

    @SuppressWarnings("unused")
    private String printTable() {
        return Utils.printTable(table);
    }

}
