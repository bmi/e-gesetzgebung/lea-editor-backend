// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.change.linguistics;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

import static de.itzbund.egesetz.bmi.lea.domain.change.linguistics.Definitheit.DEFINIT;
import static de.itzbund.egesetz.bmi.lea.domain.change.linguistics.Definitheit.INDEFINIT;
import static de.itzbund.egesetz.bmi.lea.domain.change.linguistics.Genus.FEMININUM;
import static de.itzbund.egesetz.bmi.lea.domain.change.linguistics.Numerus.PLURAL;

@AllArgsConstructor
@Getter
public enum Artikel {
    DER(DEFINIT, "der"),
    DIE(DEFINIT, "die"),
    DAS(DEFINIT, "das"),
    EIN(INDEFINIT, "ein"),
    EINE(INDEFINIT, "eine");

    private final Definitheit definitheit;
    private final String value;

    public static Artikel getFor(@NonNull Definitheit definitheit, @NonNull Genus genus, @NonNull Numerus numerus) {
        if (definitheit == DEFINIT) {
            if (numerus == PLURAL) {
                return DIE;
            } else {
                switch (genus) {
                    case MASKULINUM:
                        return DER;
                    case FEMININUM:
                        return DIE;
                    default:
                        return DAS;
                }
            }
        } else {
            if (numerus == PLURAL) {
                return null;
            } else {
                return genus == FEMININUM ? EINE : EIN;
            }
        }
    }
}
