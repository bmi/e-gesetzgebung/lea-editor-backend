// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.logging;

import java.util.ResourceBundle;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum LogActivity {

	EXPORT_LDML("dom.logging.export.ldml"),
	IMPORT_LDML("dom.logging.import.ldml"),
	EXPORT_PDF("dom.logging.export.pdf"),
	GRANT_WRITE_ACCESS("dom.logging.grant.write"),
	REVOKE_WRITE_ACCESS("dom.logging.revoke.write"),
	GRANT_READ_ACCESS("dom.logging.grant.read"),
	REVOKE_READ_ACCESS("dom.logging.revoke.read");

	private static final ResourceBundle MESSAGES =
		ResourceBundle.getBundle("de.itzbund.egesetz.bmi.lea.domain.messages.DomainResourceBundle");

	final String key;

	public String getText() {
		return MESSAGES.getString(key);
	}

}
