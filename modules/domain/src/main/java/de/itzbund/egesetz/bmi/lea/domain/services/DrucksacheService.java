// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.Drucksache;
import de.itzbund.egesetz.bmi.lea.domain.model.DrucksacheDokument;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DrucksacheDokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DrucksachenPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.DrucksacheRestPort;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Log4j2
@SuppressWarnings("unused")
public class DrucksacheService implements DrucksacheRestPort {

    // ----- Repositories -----
    @Autowired
    private DrucksachenPersistencePort drucksachenPersistencePort;
    @Autowired
    private DrucksacheDokumentPersistencePort drucksacheDokumentPersistencePort;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public List<ServiceState> drucksachenNummerAnlegen(RegelungsVorhabenId regelungsvorhabenId, String drucksachenNummer) {

        List<ServiceState> states = new ArrayList<>();

        if (isValidDrucksachennummer(drucksachenNummer)) {
            if (drucksachenPersistencePort.findByDrucksachenNr(drucksachenNummer) != null) {
                states.add(ServiceState.DRUCKSACHENNUMMER_BEREITS_VERGEBEN);
            } else if (drucksachenPersistencePort.findByRegelungsvorhabenId(regelungsvorhabenId) != null) {
                states.add(ServiceState.DRUCKSACHE_FUER_REGELUNGSVORHABEN_VORHANDEN);
            } else {

                if (isAnyDokumentenmappeAtBundesrat(regelungsvorhabenId)) {
                    drucksachenEintragVornehmen(regelungsvorhabenId, drucksachenNummer, states);
                } else {
                    states.add(ServiceState.FEHLENDER_STATUS_BUNDESRAT);
                }

            }
        } else {
            states.add(ServiceState.DRUCKSACHENNUMMER_UNGUELTIG);
        }

        return states;
    }

    @Override
    public Drucksache getDrucksache(RegelungsVorhabenId regelungsvorhabenId) {
        return drucksachenPersistencePort.findByRegelungsvorhabenId(regelungsvorhabenId);
    }

    private boolean isValidDrucksachennummer(String testString) {
        String regex = "^\\d+/\\d+$";
        Pattern pattern = Pattern.compile(regex);

        // Matcher-Objekt erstellen
        Matcher matcher = pattern.matcher(testString);

        // Überprüfen des Strings
        return matcher.matches();
    }

    private boolean isAnyDokumentenmappeAtBundesrat(RegelungsVorhabenId regelungsvorhabenId) {
        CompoundDocument compoundDocument = applicationContext.getBean(CompoundDocument.class);
        List<CompoundDocument> compoundDocuments = compoundDocument.getCompoundDocumentsOptionalFilteredByUserUndRegelungsvorhaben(
            null, List.of(regelungsvorhabenId.getId()));

        return compoundDocuments.stream().anyMatch(compoundDocument1 -> compoundDocument1.getState().equals(DocumentState.ZUGESTELLT_BUNDESRAT));
    }

    private void drucksachenEintragVornehmen(RegelungsVorhabenId regelungsvorhabenId, String drucksachenNummer, List<ServiceState> states) {
        Drucksache drucksache = Drucksache.builder()
            .drucksachenNr(drucksachenNummer)
            .regelungsVorhabenId(regelungsvorhabenId)
            .build();
        Drucksache saved = drucksachenPersistencePort.save(drucksache);
        if (saved != null) {
            states.add(ServiceState.OK);
        } else {
            states.add(ServiceState.UNKNOWN_ERROR);
        }
    }

    public DrucksacheDokument getDrucksacheDokument(CompoundDocumentId compoundDocumentId) {
        return drucksacheDokumentPersistencePort.getDrucksacheDokument(compoundDocumentId).orElse(null);
    }

}
