// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest;

import de.itzbund.egesetz.bmi.lea.domain.dtos.bestand.CreateBestandsrechtDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentImportDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentMetadataDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.UpdateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;

import java.util.List;
import java.util.UUID;

public interface DocumentRestPort {

	DocumentMetadataDTO importDocument(CompoundDocumentId compoundDocumentId, DocumentImportDTO decodedDTO,
		long contentLength, List<ServiceState> status);

	DocumentDTO getDocumentById(UUID documentId, boolean clearAnnotations);

	DocumentDTO updateDocument(UUID documentId,
		UpdateDocumentDTO decodedBody, List<ServiceState> states);

	DocumentDTO enrichDocumentDTOWithPermissions(DocumentDTO documentDTO);

    DocumentSummaryDTO createBestandsrecht(DocumentId documentId, CreateBestandsrechtDTO createBestandsrechtDTO, List<ServiceState> status);

}
