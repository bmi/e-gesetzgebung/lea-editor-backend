// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets DocumentState
 */
@SuppressWarnings("unused")
public enum DocumentState {
	DRAFT("DRAFT"),
	FINAL("FINAL"),
	FREEZE("FREEZE"),
	BEREIT_FUER_KABINETT("BEREIT_FUER_KABINETTVERFAHREN"),
	ZUGELEITET_PKP("ZUGELEITET_PKP"),
	ZUGESTELLT_BUNDESRAT("ZUGESTELLT_BUNDESRAT"),
	BEREIT_FUER_BUNDESTAG("BEREIT_FUER_BUNDESTAG"),
	BESTANDSRECHT("BESTANDSRECHT"),
	AENDERUNG_GEWUENSCHT_PKP("AENDERUNG_GEWUENSCHT_PKP"),
	AENDERUNG_GEWUENSCHT_FEDERFUEHRER("AENDERUNG_GEWUENSCHT_FEDERFUEHRER");

	private final String value;


	DocumentState(String value) {
		this.value = value;
	}


	@JsonCreator
	public static DocumentState fromValue(String text) {
		for (DocumentState b : DocumentState.values()) {
			if (String.valueOf(b.value).equals(text)) {
				return b;
			}
		}
		return null;
	}


	@Override
	@JsonValue
	public String toString() {
		return String.valueOf(value);
	}
}
