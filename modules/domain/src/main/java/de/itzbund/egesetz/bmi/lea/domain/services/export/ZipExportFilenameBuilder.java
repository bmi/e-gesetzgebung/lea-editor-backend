// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.export;

import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A class for building the names of exported files.
 */
@Component
public class ZipExportFilenameBuilder {

    private static final int SPACE = 32;
    private static final int HYPHEN = 45;

    // used for substituting illegal names
    private static final String SUBSTITUTE = "--ohne-Titel--";

    // those are reserved words that must not be used as file names
    // see https://docs.microsoft.com/de-de/windows/win32/fileio/naming-a-file?redirectedfrom=MSDN#namespaces
    private static final Set<Integer> RESERVED_NAMES = new HashSet<>(Stream.of(
        "CON", "PRN", "AUX", "NUL", "COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9", "LPT1",
        "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", "LPT9", ".", ".."
    ).map(String::hashCode).collect(Collectors.toSet()));

    // those are disallowed characters not allowed in file names
    // see https://support.tresorit.com/hc/en-us/articles/216114167-Fixing-invalid-characters-and-colliding-file-names
    private static final Set<Integer> DISALLOWED_CHARS = new HashSet<>(
        Arrays.stream(SpecialCharacters.values()).map(SpecialCharacters::getCodePoint)
            .collect(Collectors.toSet()));

    private static final String CLASSIFIER = "Export";
    private final DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmm");


    public String getSingleDocumentFilename(long timestamp, DocumentDomain documentDdocumentDomain) {
        return String.format("%s_%s_%s_%s.xml", dateFormat.format(timestamp), CLASSIFIER,
            documentDdocumentDomain.getType().getReadableName(), getValidTitle(documentDdocumentDomain.getTitle()));
    }


    public String getCompoundDocumentFilename(long timestamp, CompoundDocumentDomain compoundDocumentDomain,
        FileFormat fileFormat) {
        return String.format("%s_%s_%s.%s", dateFormat.format(timestamp), CLASSIFIER,
            getValidTitle(compoundDocumentDomain.getTitle()), fileFormat.getExtension());
    }


    // deletes or replaces illegal characters or replaces illegal words by SUBSTITUTE
    private String getValidTitle(String title) {
        if (title == null) {
            return SUBSTITUTE;
        }
        String transformed = title.codePoints()
            .filter(c -> (c >= SPACE))
            .map(c -> (DISALLOWED_CHARS.contains(c) ? HYPHEN : c))
            .collect(StringBuilder::new
                , StringBuilder::appendCodePoint
                , StringBuilder::append)
            .toString();
        transformed = transformed.replaceAll("^([ .]+)", "").replaceAll("([ .]+)$", ""); //NOSONAR

        if (RESERVED_NAMES.contains(transformed.hashCode())) {
            transformed = SUBSTITUTE;
        }

        return transformed;
    }


    @RequiredArgsConstructor
    @Getter
    public enum FileFormat {
        ARCHIVE("zip"),
        DOC_COLLECTION("xml");

        private final String extension;
    }

    @RequiredArgsConstructor
    @Getter
    private enum SpecialCharacters {
        QUOTATION_MARK(34),
        ASTERISK(42),
        SOLIDUS(47),
        COLON(58),
        LESS_THAN_SIGN(60),
        GREATER_THAN_SIGN(62),
        QUESTION_MARK(63),
        REVERSE_SOLIDUS(92),
        VERTICAL_LINE(124);

        final int codePoint;
    }

}
