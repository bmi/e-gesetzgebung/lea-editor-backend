// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.mappers;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CopyCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CreateCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import org.mapstruct.Builder;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;
import java.util.UUID;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true),
    uses = {BaseMapper.class, DocumentMapper.class, PropositionMapper.class}
)
public interface CompoundDocumentMapper {

    @Named("stringToUuid")
    default RegelungsVorhabenId stringToUuid(String uuidAsString) {
        return new RegelungsVorhabenId(UUID.fromString(uuidAsString));
    }

    @Mapping(target = "compoundDocumentEntity.regelungsVorhabenId", source = "regelungsVorhabenId", qualifiedByName = "stringToUuid")
    @Mapping(target = "compoundDocumentEntity.title", source = "title")
    @Mapping(target = "compoundDocumentEntity.type", source = "type")
    CompoundDocument map(CreateCompoundDocumentDTO createCompoundDocumentDTO);

    @Mapping(target = "id", source = "copyCompoundDocumentDTO.compoundDocumentId", qualifiedByName = "compoundDocumentIdUuidMapping")
    @Mapping(target = "title", source = "copyCompoundDocumentDTO.title")
    @Mapping(target = "type", source = "copyCompoundDocumentDTO.type")
    @Mapping(target = "version", source = "copyCompoundDocumentDTO.version")
    @Mapping(target = "createdAt", source = "copyCompoundDocumentDTO.createdAt")
    @Mapping(target = "updatedAt", source = "copyCompoundDocumentDTO.updatedAt")
    @Mapping(target = "documents", source = "copyCompoundDocumentDTO.documents")
    @Mapping(target = "state", source = "copyCompoundDocumentDTO.state")
    CompoundDocumentSummaryDTO map(CopyCompoundDocumentDTO copyCompoundDocumentDTO, CompoundDocumentDomain save);

    @Mapping(target = "compoundDocumentEntity.regelungsVorhabenId", source = "createCompoundDocumentDTO.regelungsVorhabenId", qualifiedByName = "stringToUuid")
    @Mapping(target = "compoundDocumentEntity.title", source = "createCompoundDocumentDTO.title")
    @Mapping(target = "compoundDocumentEntity.type", source = "createCompoundDocumentDTO.type")
    CompoundDocument merge(CreateCompoundDocumentDTO createCompoundDocumentDTO, CompoundDocument save);

    @Mapping(target = "proposition", source = "regelungsVorhabenDTO")
    @Mapping(target = "title", source = "compoundDocument.compoundDocumentEntity.title")
    @Mapping(target = "type", source = "compoundDocument.compoundDocumentEntity.type")
    @Mapping(target = "createdAt", source = "compoundDocument.compoundDocumentEntity.createdAt")
    @Mapping(target = "updatedAt", source = "compoundDocument.compoundDocumentEntity.updatedAt")
    @Mapping(target = "documents", source = "compoundDocument.documents")
    @Mapping(target = "id", source = "compoundDocument.compoundDocumentEntity.compoundDocumentId.id")
    @Mapping(target = "version", source = "compoundDocument.compoundDocumentEntity.version")
    CompoundDocumentDTO mapFromCompoundDocumentAndProposition(CompoundDocument compoundDocument,
        RegelungsvorhabenEditorDTO regelungsVorhabenDTO);

    @Mapping(target = "title", source = "compoundDocumentEntity.title")
    @Mapping(target = "type", source = "compoundDocumentEntity.type")
    @Mapping(target = "version", source = "compoundDocumentEntity.version")
    @Mapping(target = "createdAt", source = "compoundDocumentEntity.createdAt")
    @Mapping(target = "updatedAt", source = "compoundDocumentEntity.updatedAt")
    @Mapping(target = "documents", source = "documents")
    @Mapping(target = "id", source = "compoundDocumentEntity.compoundDocumentId.id")
    CompoundDocumentSummaryDTO mapCompoundDocument(CompoundDocument compoundDocuments);

    // -----------

    CompoundDocumentDomain mapDtoToEntity(CopyCompoundDocumentDTO copyCompoundDocumentDTO);

    @InheritInverseConfiguration
    CopyCompoundDocumentDTO mapEntityToDto(CompoundDocumentDomain compoundDocumentDomain);

    // -----------

    @Mapping(target = "id", source = "compoundDocumentId.id")
    CompoundDocumentSummaryDTO map(CompoundDocumentDomain compoundDocumentDomain);

    List<CompoundDocumentSummaryDTO> maptoCompoundDocumentSummaryDTOList(
        List<CompoundDocumentDomain> compoundDocuments);

    List<CompoundDocumentSummaryDTO> mapCompoundDocumentToCompoundDocumentSummaryDTOList(
        List<CompoundDocument> compoundDocuments);

    // -----------

}
