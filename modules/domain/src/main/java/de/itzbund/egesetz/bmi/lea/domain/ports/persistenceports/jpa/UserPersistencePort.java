// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa;

import java.util.List;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;

public interface UserPersistencePort {

	User findFirstByGid(UserId userId);

	List<UserId> findMarkedAsDeleted();

	void markUserAsNotReferenced(UserId userId);
}
