// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.synopse;

import de.itzbund.egesetz.bmi.lea.core.json.JSONUtils;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.change.AenderungsbefehlBerechnung;
import de.itzbund.egesetz.bmi.lea.domain.compare.DocumentComparer;
import de.itzbund.egesetz.bmi.lea.domain.compare.RegulatoryTextComparer;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.PermissionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.synopsis.SynopsisRequestDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.synopsis.SynopsisResponseDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.Roles;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import de.itzbund.egesetz.bmi.lea.domain.mappers.DocumentMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.UserMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.Bestandsrecht;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.BestandsrechtId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.BestandsrechtPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CompoundDocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.EIdGeneratorRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.SynopsisRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.TemplateRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.UserRestPort;
import de.itzbund.egesetz.bmi.lea.domain.services.DocumentService;
import de.itzbund.egesetz.bmi.lea.domain.services.RegelungsvorhabenService;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.IllegalDocumentType;
import de.itzbund.egesetz.bmi.lea.domain.services.filters.ChangeAnnotationFilter;
import de.itzbund.egesetz.bmi.lea.domain.services.filters.EIdGeneratorFilter;
import de.itzbund.egesetz.bmi.lea.domain.services.filters.FilterChain;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.core.Constants.DATE_TIME_FORMATTER;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDocumentObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.wrapUp;
import static de.itzbund.egesetz.bmi.lea.domain.change.AenderungsbefehlUtils.getDocumentType;

@Service
@Log4j2
@SuppressWarnings("unused")
public class SynopsisService implements SynopsisRestPort {

    private static final String DM_NAME = "Synopse";

    @Autowired
    private CompoundDocumentRestPort compoundDocumentRestPort;

    @Autowired
    private DokumentPersistencePort dokumentPersistencePort;

    @Autowired
    private EIdGeneratorRestPort eIdGeneratorRestPort;

    @Autowired
    private UserRestPort userService;

    @Autowired
    private LeageSession session;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private DocumentMapper documentMapper;

    @Autowired
    private PropositionMapper propositionMapper;

    @Autowired
    private DocumentService documentService;

    @Autowired
    private TemplateRestPort templateService;

    @Autowired
    private AenderungsbefehlBerechnung aenderungsbefehlBerechnung;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private BestandsrechtPersistencePort bestandsrechtRepository;

    @Autowired
    private RegelungsvorhabenService regelungsvorhabenService;


    private FilterChain filterChain;


    @Override
    public SynopsisResponseDTO getSynopse(SynopsisRequestDTO synopsisRequestDTO, List<ServiceState> status) {
        Pair<DocumentDomain, DocumentDomain> docs = getDocuments(synopsisRequestDTO, status);
        if (docs == null) {
            return null;
        }

        DocumentDomain baseDocument = docs.getLeft();
        DocumentDomain versionDocument = docs.getRight();
        User user = session.getUser();

        if (!isUserAllowed(baseDocument, versionDocument, user, status)) {
            return null;
        }

        DocumentType baseDocumentType = baseDocument.getType();
        DocumentType versionDocumentType = versionDocument.getType();
        if (baseDocumentType != versionDocumentType
            || baseDocumentType.getArt() != RechtsetzungsdokumentArt.REGELUNGSTEXT) {
            status.add(ServiceState.INVALID_ARGUMENTS);
            return null;
        }

        return getComparisonResult(baseDocument, versionDocument, user, status);
    }


    @Override
    public DocumentDTO erzeugeAenderungsbefehle(UUID regulatoryProposalId, UUID bestandsrechtId, UUID regelungstextId, List<ServiceState> status) {
        DocumentDomain regelungstext = documentService.getDocumentEntityById(new DocumentId(regelungstextId));
        if (regelungstext == null) {
            status.add(ServiceState.DOCUMENT_NOT_FOUND);
            return null;
        }

        Bestandsrecht bestandsrecht = bestandsrechtRepository.getBestandsrechtByBestandsrechtId(new BestandsrechtId(bestandsrechtId)).orElse(null);
        if (bestandsrecht == null) {
            status.add(ServiceState.DOCUMENT_NOT_FOUND);
            return null;
        }

        JSONObject bestandDocumentObject = getDocumentObject(bestandsrecht.getContentJson().replace("\\\\", "\\"));
        JSONObject neufassungDocumentObject = getDocumentObject(regelungstext.getContent().replace("\\\\", "\\"));
        if (bestandDocumentObject == null || neufassungDocumentObject == null) {
            status.add(ServiceState.INVALID_STATE);
            return null;
        }

        FilterChain localFilterChain = getFilterChain();
        localFilterChain.doFilter(bestandDocumentObject);
        localFilterChain.reset();
        localFilterChain.doFilter(neufassungDocumentObject);

        RechtsetzungsdokumentTyp metaTyp = getDocumentType(neufassungDocumentObject);
        aenderungsbefehlBerechnung.compare(
            bestandDocumentObject,
            Map.of(RegulatoryTextComparer.RegulatoryTextMetadata.TYPE.name(), metaTyp),
            neufassungDocumentObject,
            Map.of(RegulatoryTextComparer.RegulatoryTextMetadata.TYPE.name(), metaTyp));

        JSONObject resultDocumentObject = aenderungsbefehlBerechnung.getResultDocumentObject();
        if (resultDocumentObject == null) {
            status.add(ServiceState.INVALID_STATE);
            return null;
        }

        RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = regelungsvorhabenService.getRegulatoryProposalUserCanAccess(regulatoryProposalId);
        PropositionDTO propositionDTO = propositionMapper.map(regelungsvorhabenEditorDTO);

        status.add(ServiceState.OK);
        return buildNeufassung(propositionDTO, regelungstext, metaTyp, resultDocumentObject);
    }


    private DocumentDTO buildNeufassung(PropositionDTO propositionDTO, DocumentDomain regelungstext, RechtsetzungsdokumentTyp metaTyp,
        JSONObject resultDocumentObject) {
        UserDTO user = userMapper.map(userService.getUser());
        String timestamp = DATE_TIME_FORMATTER.format(Instant.now());
        DocumentType documentType = metaTyp == RechtsetzungsdokumentTyp.GESETZ
            ? DocumentType.REGELUNGSTEXT_MANTELGESETZ
            : DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG;
        return DocumentDTO.builder()
            .id(UUID.randomUUID())
            .state(DocumentState.FINAL)
            .version("1.0")
            .documentPermissions(PermissionDTO.builder()
                .hasRead(true)
                .hasWrite(false)
                .build())
            .commentPermissions(PermissionDTO.builder()
                .hasRead(true)
                .hasWrite(false)
                .build())
            .compoundDocumentId(regelungstext.getCompoundDocumentId().getId())
            .proposition(propositionDTO)
            .createdBy(user)
            .createdAt(timestamp)
            .updatedBy(user)
            .updatedAt(timestamp)
            .title("Änderungsbefehle")
            .type(documentType)
            .content(wrapUp(resultDocumentObject).toJSONString())
            .mappenName("Neufassung")
            .build();
    }


    // check fields and availability of documents
    private Pair<DocumentDomain, DocumentDomain> getDocuments(SynopsisRequestDTO synopsisRequestDTO,
        List<ServiceState> status) {
        List<UUID> versions = synopsisRequestDTO.getVersions();
        UUID base = synopsisRequestDTO.getBase();
        UUID version;

        if (versions.isEmpty() || (version = versions.get(0)) == null) {
            status.add(ServiceState.INVALID_ARGUMENTS);
            log.error("invalid SynopsisRequestDTO: {}", synopsisRequestDTO);
            return null;
        }

        Optional<DocumentDomain> optionalBase = dokumentPersistencePort.findByDocumentId(
            new DocumentId(base)
        );
        Optional<DocumentDomain> optionalVersion = dokumentPersistencePort.findByDocumentId(
            new DocumentId(version)
        );

        if (optionalBase.isEmpty() || optionalVersion.isEmpty()) {
            status.add(ServiceState.DOCUMENT_NOT_FOUND);
            log.error("specified documents not found: {}", synopsisRequestDTO);
            return null;
        }

        return Pair.of(optionalBase.get(), optionalVersion.get());
    }


    // check access rights
    private boolean isUserAllowed(DocumentDomain baseDocument, DocumentDomain versionDocument, User user,
        List<ServiceState> status) {
        CompoundDocument baseCompoundDocument = getCompoundDocumentOf(baseDocument);
        CompoundDocument versionCompoundDocument = getCompoundDocumentOf(versionDocument);

        if (baseCompoundDocument == null || versionCompoundDocument == null) {
            status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
            log.error("compound document not found");
            return false;
        }

        if (!isAllowedToRead(baseCompoundDocument, user) || !isAllowedToRead(versionCompoundDocument, user)) {
            status.add(ServiceState.NO_PERMISSION);
            log.error("user has no permission to read");
            return false;
        } else {
            return true;
        }
    }


    private SynopsisResponseDTO getComparisonResult(DocumentDomain baseDocument, DocumentDomain versionDocument,
        User user, List<ServiceState> status) {
        DocumentComparer comparer = new RegulatoryTextComparer();
        String baseJSONString = baseDocument.getContent();
        String versionJSONString = versionDocument.getContent();

        JSONObject baseDocumentObject = JSONUtils.getDocumentObject(baseJSONString);
        JSONObject versionDocumentObject = JSONUtils.getDocumentObject(versionJSONString);

        if (baseDocumentObject == null || versionDocumentObject == null) {
            status.add(ServiceState.DOCUMENT_NOT_VALID);
            log.error("non-parseable JSON document");
            return null;
        }

        FilterChain localFilterChain = getFilterChain();
        localFilterChain.doFilter(baseDocumentObject);
        localFilterChain.reset();
        localFilterChain.doFilter(versionDocumentObject);

        UserDTO userDTO = userMapper.map(user);

        Map<String, Object> baseMetadata = Map.of(
            RegulatoryTextComparer.RegulatoryTextMetadata.LAST_MODIFICATION_DATE.name(),
            baseDocument.getUpdatedAt());
        Map<String, Object> versionMetadata = Map.of(
            RegulatoryTextComparer.RegulatoryTextMetadata.LAST_MODIFICATION_DATE.name(),
            versionDocument.getUpdatedAt());
        comparer.compare(baseDocumentObject, baseMetadata, versionDocumentObject, versionMetadata);
        String template = templateService.loadTemplate(DocumentType.SYNOPSE, 0);

        try {
            DocumentDTO documentDTO = SynopsisUtil.getSynopseDocument(((RegulatoryTextComparer) comparer).getTable(), template, userDTO);
            return makeResponse(documentDTO, userDTO, status);
        } catch (ParseException | IllegalDocumentType e) {
            status.add(ServiceState.DOCUMENT_NOT_VALID);
            log.error(e);
            return null;
        }
    }


    private CompoundDocument getCompoundDocumentOf(DocumentDomain document) {
        List<ServiceState> status = new ArrayList<>();
        CompoundDocument compoundDocument =
            compoundDocumentRestPort.getCompoundDocumentById(document.getCompoundDocumentId(), status);
        ServiceState state = status.isEmpty() ? ServiceState.INVALID_STATE : status.get(0);
        return state == ServiceState.OK ? compoundDocument : null;
    }


    private boolean isAllowedToRead(CompoundDocument compoundDocument, User user) {
        List<Roles> roles = compoundDocument.whichRolesOwnsUser(user);
        return roles.contains(Roles.ERSTELLER_DOKUMENTENMAPPE) || roles.contains(Roles.TEILNEHMER_HRA);
    }


    private SynopsisResponseDTO makeResponse(DocumentDTO documentDTO, UserDTO userDTO, List<ServiceState> status) {
        documentDTO.setMappenName(DM_NAME);
        SynopsisResponseDTO response = new SynopsisResponseDTO()
            .type(SynopsisResponseDTO.TypeEnum.SYNOPSE)
            .ersteller(userDTO)
            .erstellt(OffsetDateTime.now())
            .document(documentDTO);

        if (response == null) {
            status.add(ServiceState.INVALID_STATE);
        } else {
            status.add(ServiceState.OK);
        }

        return response;
    }


    private FilterChain getFilterChain() {
        if (filterChain == null) {
            filterChain = new FilterChain();
            filterChain.addFilter(new ChangeAnnotationFilter());
            filterChain.addFilter(applicationContext.getBean(EIdGeneratorFilter.class));
        } else {
            filterChain.reset();
        }
        return filterChain;
    }

}
