// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;

/**
 * represents content text DTO for eGFA Datenübernahme to Editor
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Schema(allOf = {EgfaDatenuebernahmeContentDTO.class})
public class EgfaDatenuebernahmeContentTextDTO extends EgfaDatenuebernahmeContentDTO {

    @NotNull
    private String data;


    @Override
    @JsonIgnore
    public EgfaItemType getType() {
        return EgfaItemType.TEXT;
    }
}
