// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * indicates different types of Regelungsvorhaben
 */
@Schema(enumAsRef = true)
@AllArgsConstructor
@Getter
public enum RechtsetzungsdokumentFassung {
    ENTWURFSFASSUNG("Entwurfsfassung", "entwurfsfassung"),
    VERKUENDIGUNGSFASSUNG("Verkündigungsfassung", "verkuendungsfassung"),
    NEUFASSUNG("Neufassung", "neufassung");

    private static final Map<String, RechtsetzungsdokumentFassung> FASSUNG_LITERALS = new HashMap<>();

    static {
        Arrays.stream(values()).forEach(fassung -> FASSUNG_LITERALS.put(fassung.getLiteral(), fassung));
    }

    private final String label;
    private final String literal;


    public static RechtsetzungsdokumentFassung fromLiteral(String literal) {
        if (Utils.isMissing(literal)) {
            return null;
        }

        return FASSUNG_LITERALS.get(literal);
    }

}
