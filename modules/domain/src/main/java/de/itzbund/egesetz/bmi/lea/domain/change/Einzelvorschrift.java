// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.change;

import de.itzbund.egesetz.bmi.lea.core.compare.JSONObjectListComparer;
import de.itzbund.egesetz.bmi.lea.core.compare.ObjectListDiff;
import de.itzbund.egesetz.bmi.lea.core.json.TraversableJSONDocument;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_EID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_HREF;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CONTENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_P;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_REF;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getEffectiveTextValue;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getHeadingOfElement;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getNumElement;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getParas;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getStringAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getTextWrapper;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewDefaultJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewDefaultJSONObjectWithText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewJSONObjectWithText;
import static de.itzbund.egesetz.bmi.lea.domain.change.AenderungsbefehlUtils.getAenderungsbefehlSkeleton;
import static de.itzbund.egesetz.bmi.lea.domain.change.AenderungsbefehlUtils.getModIntroObject;
import static de.itzbund.egesetz.bmi.lea.domain.change.AenderungsbefehlUtils.getPrecedingParaNum;
import static de.itzbund.egesetz.bmi.lea.domain.change.AenderungsbefehlUtils.getQuotedStructure;

@Getter
@Log4j2
public class Einzelvorschrift implements AenderungsfaehigeGliederungseinheit {

	private static final int IDX_COUNT_PARAS_DELETED = 0;
	private static final int IDX_COUNT_PARAS_INSERTED = 1;
	private static final int IDX_COUNT_PARAS_EQUAL = 2;
	private static final int IDX_COUNT_PARAS_SUBST = 3;

	private static final double CHANGE_RATIO_LIMIT = 0.5;

	private static final DiffMatchPatch DMP = new DiffMatchPatch();

	private Aenderungsart aenderungsart;

	private JSONObject introObject;

	private final List<JSONObject> aenderungsbefehle;

	private String aenderungsstelle;

	private final TraversableJSONDocument baseDocument;

	private final TraversableJSONDocument versionDocument;

	public Einzelvorschrift(TraversableJSONDocument baseDocument, TraversableJSONDocument versionDocument,
		JSONObject baseNorm, JSONObject versionNorm, DiffMatchPatch.Operation op, String predecessorEId) {
		this.baseDocument = baseDocument;
		this.versionDocument = versionDocument;
		this.aenderungsbefehle = new ArrayList<>();

		switch (op) {
			case DELETE:
				streichen(baseNorm);
				break;
			case INSERT:
				einfuegen(versionNorm, predecessorEId);
				break;
			default:
				this.aenderungsart = compareNorms(baseNorm, versionNorm, predecessorEId);
		}
	}


	private void einfuegen(JSONObject normObject, String predecessorEId) {
		aenderungsart = Aenderungsart.EINFUEGEN;
		aenderungsstelle = AenderungsbefehlUtils.getAenderungsstelle(predecessorEId);

		JSONObject aenderungsbefehl = getAenderungsbefehlSkeleton(aenderungsart);
		JSONObject refObject = makeNewJSONObjectWithText(ELEM_REF, aenderungsstelle, true, false);
		addAttribute(refObject, Pair.of(ATTR_HREF, ""));

		addChildren(aenderungsbefehl,
			getTextWrapper("Nach "),
			refObject,
			getTextWrapper(String.format(" wird der folgende %s eingefügt:",
				AenderungsbefehlUtils.getAenderungsstelle(getStringAttribute(normObject, ATTR_EID)))),
			getQuotedStructure(normObject)
		);
		aenderungsbefehle.add(aenderungsbefehl);
	}


	private void streichen(JSONObject normObject) {
		aenderungsart = Aenderungsart.STREICHEN;
		aenderungsstelle = AenderungsbefehlUtils.getAenderungsstelle(getStringAttribute(normObject, ATTR_EID));

		JSONObject aenderungsbefehl = getAenderungsbefehlSkeleton(aenderungsart);
		JSONObject refObject = makeNewJSONObjectWithText(ELEM_REF, aenderungsstelle, true, false);
		addAttribute(refObject, Pair.of(ATTR_HREF, ""));

		addChildren(aenderungsbefehl,
			refObject,
			getTextWrapper(" wird gestrichen.")
		);
		aenderungsbefehle.add(aenderungsbefehl);
	}


	private Aenderungsart compareNorms(JSONObject baseNorm, JSONObject versionNorm, String predecessorEId) {

		// Überschrift

		String baseNumString = getEffectiveTextValue(getNumElement(baseNorm));
		String baseHeadingString = getEffectiveTextValue(getHeadingOfElement(baseNorm));
		String versionNumString = getEffectiveTextValue(getNumElement(versionNorm));
		String versionHeadingString = getEffectiveTextValue(getHeadingOfElement(versionNorm));
		boolean substituteHeading = !baseNumString.equals(versionNumString) || !baseHeadingString.equals(versionHeadingString);

		// Absätze

		JSONObjectListComparer comparer = new JSONObjectListComparer(baseDocument, versionDocument);
		comparer.compare(getParas(baseNorm), getParas(versionNorm));
		List<Integer> deletedParas = new ArrayList<>();
		List<Integer> insertedParas = new ArrayList<>();
		List<Integer> assignedParas = new ArrayList<>();
		List<Integer> replacedParas = new ArrayList<>();
		Pair<Double, Double> normChangeRatio = getChangeRatio(comparer.getKeyDiffs(), substituteHeading,
			deletedParas, insertedParas, assignedParas, replacedParas);
		double paraChangeRatio = (double) replacedParas.size() / assignedParas.size();

		if ((normChangeRatio.getLeft() >= CHANGE_RATIO_LIMIT && normChangeRatio.getRight() >= CHANGE_RATIO_LIMIT)
			|| (paraChangeRatio >= CHANGE_RATIO_LIMIT)) {
			return ersetzen(baseNorm, versionNorm);
		}

		return aendern(comparer.getKeyDiffs(), baseNorm, versionNorm, substituteHeading);
	}


	private Pair<Double, Double> getChangeRatio(LinkedList<ObjectListDiff<UUID>> keyDiffs, boolean substituteHeading,
		List<Integer> deletedParas, List<Integer> insertedParas, List<Integer> assignedParas, List<Integer> replacedParas) {
		AtomicInteger index = new AtomicInteger();
		AtomicInteger insertions = new AtomicInteger();
		AtomicInteger deletions = new AtomicInteger();
		AtomicInteger levenshtein = new AtomicInteger();
		// count #paras deleted, #paras inserted, #paras equal, #paras substituted
		int[] counters = new int[]{0, 0, 0, 0};

		keyDiffs.forEach(diff -> {
			DiffMatchPatch.Operation op = diff.getOperation();

			switch (op) {
				case DELETE:
					deletedParas.add(index.getAndIncrement());
					counters[IDX_COUNT_PARAS_DELETED]++;
					deletions.incrementAndGet();
					break;
				case INSERT:
					insertedParas.add(index.getAndIncrement());
					counters[IDX_COUNT_PARAS_INSERTED]++;
					insertions.incrementAndGet();
					break;
				default:
					assignedParas.add(index.getAndIncrement());
					counters[IDX_COUNT_PARAS_EQUAL]++;
					levenshtein.getAndAdd(Math.max(insertions.get(), deletions.get()));
					insertions.set(0);
					deletions.set(0);
			}
		});

		counters[IDX_COUNT_PARAS_SUBST] = getNumberOfReplacedParas(keyDiffs, assignedParas, replacedParas);
		int headingFlag = substituteHeading ? 1 : 0;

		double levenshteinRatio = (double) levenshtein.addAndGet(Math.max(insertions.get(), deletions.get()))
			/ Math.max(counters[IDX_COUNT_PARAS_DELETED] + counters[IDX_COUNT_PARAS_EQUAL],
			counters[IDX_COUNT_PARAS_INSERTED] + counters[IDX_COUNT_PARAS_EQUAL]);

		double changeRatio = (double) (Math.max(counters[IDX_COUNT_PARAS_DELETED],
			counters[IDX_COUNT_PARAS_INSERTED]) + headingFlag + counters[IDX_COUNT_PARAS_SUBST])
			/ (counters[IDX_COUNT_PARAS_DELETED] + counters[IDX_COUNT_PARAS_EQUAL]);

		return Pair.of(levenshteinRatio, changeRatio);
	}


	private int getNumberOfReplacedParas(LinkedList<ObjectListDiff<UUID>> keyDiffs, List<Integer> assignedParas, List<Integer> replacedParas) {
		assignedParas.forEach(i -> {
			ObjectListDiff<UUID> diff = keyDiffs.get(i);
			JSONObject basePara = baseDocument.getObject(diff.getKey().toString());
			String baseParaString = getEffectiveTextValue(basePara);
			JSONObject versionPara = versionDocument.getObject(diff.getKey().toString());
			String versionParaString = getEffectiveTextValue(versionPara);

			LinkedList<DiffMatchPatch.Diff> diffs = DMP.diffMain(baseParaString, versionParaString);
			DMP.diffCleanupSemantic(diffs);
			int levenshteinDistance = DMP.diffLevenshtein(diffs);
			double levenshteinRatio = (double) levenshteinDistance / Math.max(baseParaString.length(), versionParaString.length());

			if (levenshteinRatio >= CHANGE_RATIO_LIMIT) {
				replacedParas.add(i);
			}
		});

		return replacedParas.size();
	}


	private Aenderungsart ersetzen(JSONObject baseNorm, JSONObject versionNorm) {
		aenderungsart = Aenderungsart.ERSETZEN;
		aenderungsstelle = AenderungsbefehlUtils.getAenderungsstelle(getStringAttribute(baseNorm, ATTR_EID));

		JSONObject aenderungsbefehl = getAenderungsbefehlSkeleton(aenderungsart);
		JSONObject refObject = makeNewJSONObjectWithText(ELEM_REF, aenderungsstelle, true, false);
		addAttribute(refObject, Pair.of(ATTR_HREF, ""));

		addChildren(aenderungsbefehl,
			refObject,
			getTextWrapper(String.format(" wird durch den folgenden %s ersetzt:", aenderungsstelle)),
			getQuotedStructure(versionNorm)
		);
		aenderungsbefehle.add(aenderungsbefehl);
		return aenderungsart;
	}


	private Aenderungsart aendern(LinkedList<ObjectListDiff<UUID>> keyDiffs, JSONObject baseNorm, JSONObject versionNorm,
		boolean substituteHeading) {
		aenderungsart = Aenderungsart.AENDERN;
		aenderungsstelle = getEffectiveTextValue(getNumElement(baseNorm));
		introObject = getModIntroObject(aenderungsstelle);

		// Überschrift
		if (substituteHeading) {
			aenderungsbefehle.add(ueberschriftErsetzen(versionNorm));
		}

		// Absätze
		keyDiffs.forEach(diff -> {
			DiffMatchPatch.Operation op = diff.getOperation();
			JSONObject basePara = baseDocument.getObject(diff.getKey().toString());
			JSONObject versionPara = versionDocument.getObject(diff.getKey().toString());

			switch (op) {
				case DELETE:
					aenderungsbefehle.add(absatzStreichen(basePara));
					break;
				case INSERT:
					aenderungsbefehle.add(absatzEinfuegen(versionPara));
					break;
				default:
					compareParas(basePara, versionPara);
			}
		});

		return aenderungsart;
	}


	private JSONObject absatzStreichen(JSONObject paraObject) {
		JSONObject contentObject = makeNewDefaultJSONObject(ELEM_CONTENT);
		JSONObject pObject = makeNewDefaultJSONObject(ELEM_P);
		JSONObject aenderungsbefehl = getAenderungsbefehlSkeleton(Aenderungsart.STREICHEN);
		String bezeichnung = String.format("Absatz %s", getAenderungsstelle(paraObject));
		JSONObject refObject = makeNewJSONObjectWithText(ELEM_REF, bezeichnung, true, false);
		addAttribute(refObject, Pair.of(ATTR_HREF, ""));
		JSONObject textObject = getTextWrapper(" wird gestrichen.");
		addChildren(aenderungsbefehl, refObject, textObject);
		addChildren(pObject, aenderungsbefehl);
		addChildren(contentObject, pObject);
		return contentObject;
	}


	private JSONObject absatzEinfuegen(JSONObject paraObject) {
		JSONObject contentObject = makeNewDefaultJSONObject(ELEM_CONTENT);
		JSONObject pObject = makeNewDefaultJSONObject(ELEM_P);
		JSONObject aenderungsbefehl = getAenderungsbefehlSkeleton(Aenderungsart.EINFUEGEN);
		String paraNum = getEffectiveTextValue(getNumElement(paraObject));
		int precedingParaNum = getPrecedingParaNum(paraNum);
		if (precedingParaNum > 0) {
			String bezeichnung = String.format("Absatz %s", precedingParaNum); //NOSONAR
			JSONObject refObject = makeNewJSONObjectWithText(ELEM_REF, bezeichnung, true, false);
			addAttribute(refObject, Pair.of(ATTR_HREF, ""));
			addChildren(aenderungsbefehl,
				getTextWrapper("Nach "),
				refObject,
				getTextWrapper(String.format(" wird der folgende Absatz %s eingefügt:", paraNum.replaceAll("[()]", ""))),
				getQuotedStructure(paraObject)
			);
		} else {
			addChildren(aenderungsbefehl, getTextWrapper("!!! FEHLER !!!"));
		}
		addChildren(pObject, aenderungsbefehl);
		addChildren(contentObject, pObject);
		return contentObject;
	}


	private void compareParas(JSONObject basePara, JSONObject versionPara) {
		JSONArray baseChildren = getChildren(basePara);
		JSONArray versionChildren = getChildren(versionPara);
		try {
			if (getType((JSONObject) baseChildren.get(1)).equals(getType((JSONObject) versionChildren.get(1)))) {
				String baseString = getEffectiveTextValue(basePara);
				String versionString = getEffectiveTextValue(versionPara);
				if (versionString.equals(baseString)) {
					return;
				}
				//Hier die Fälle für gleichen Child type behandeln
			}
			aenderungsbefehle.add(absatzErsetzen(basePara, versionPara));
		} catch (Exception e) {
			log.error("Error during comparing paragraphs", e);
			JSONObject contentObject = makeNewDefaultJSONObject(ELEM_CONTENT);
			JSONObject pObject = makeNewDefaultJSONObjectWithText(ELEM_P, "Fehler im Vergleich der Absätze.");
			addChildren(contentObject, pObject);
			aenderungsbefehle.add(contentObject);
		}
	}

	private JSONObject absatzErsetzen(JSONObject basePara, JSONObject versionPara) {
		JSONObject contentObject = makeNewDefaultJSONObject(ELEM_CONTENT);
		JSONObject pObject = makeNewDefaultJSONObject(ELEM_P);
		JSONObject aenderungsbefehl = getAenderungsbefehlSkeleton(Aenderungsart.ERSETZEN);
		String bezeichnung = String.format("Absatz %s", getAenderungsstelle(basePara));
		JSONObject refObject = makeNewJSONObjectWithText(ELEM_REF, bezeichnung, true, false);
		addAttribute(refObject, Pair.of(ATTR_HREF, ""));
		addChildren(aenderungsbefehl,
			refObject,
			getTextWrapper(" wird durch den folgenden Absatz ersetzt:"),
			getQuotedStructure(versionPara)
		);
		addChildren(pObject, aenderungsbefehl);
		addChildren(contentObject, pObject);
		return contentObject;
	}

	private static String getAenderungsstelle(JSONObject para) {
		return getEffectiveTextValue(getNumElement(para)).replaceAll("[()]", "");
	}


	private JSONObject ueberschriftErsetzen(JSONObject versionNorm) {
		JSONObject contentObject = makeNewDefaultJSONObject(ELEM_CONTENT);
		JSONObject pObject = makeNewDefaultJSONObject(ELEM_P);
		JSONObject aenderungsbefehl = getAenderungsbefehlSkeleton(Aenderungsart.ERSETZEN);
		JSONObject befehlText = getTextWrapper("Die Überschrift wird durch die folgende Überschrift ersetzt:");
		JSONObject regelungssprachlicherTeil = getQuotedStructure(
			getNumElement(versionNorm),
			getHeadingOfElement(versionNorm)
		);
		addChildren(aenderungsbefehl, befehlText, regelungssprachlicherTeil);
		addChildren(pObject, aenderungsbefehl);
		addChildren(contentObject, pObject);
		return contentObject;
	}

}
