// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.ConverterRestPort;
import de.itzbund.egesetz.bmi.lea.domain.services.transform.JsonToXmlTransformer;
import de.itzbund.egesetz.bmi.lea.domain.services.transform.XmlToJsonTransformer;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.ValidationService;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ServiceUtils;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationData;
import lombok.extern.log4j.Log4j2;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;

import javax.xml.stream.XMLStreamException;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

@Log4j2
@Service
@SuppressWarnings({"unused", "java:S125", "java:S1488"})
public class ConverterService implements ConverterRestPort {

    @Autowired
    private JsonToXmlTransformer jsonToXmlTransformer;

    @Autowired
    private XmlToJsonTransformer xmlToJsonTransformer;

    @Autowired
    private ValidationService validationService;


    /**
     * converts the input string in xml structure to a json
     *
     * @param xml input string to be converted
     * @return converted json from input string
     */
    public String convertXmlToJson(String xml) {
        String transformedString = null;

        if (!Utils.isXMLValid(xml)) {
            log.error("xml is not well-formed");
            return null;
        }

        if (!validateXML(new InputSource((new ByteArrayInputStream(xml.getBytes(
            StandardCharsets.UTF_8)))))) {
            log.error("xml is not valid");
            return null;
        }

        try {
            transformedString = xmlToJsonTransformer
                .transform(xml);
        } catch (XMLStreamException e) {
            log.error("xml could not be converted to json", e);
        }
        return transformedString;
    }


    /**
     * converts the input string in json structure to a xml
     *
     * @param json input to be converted
     * @return converted xml from input JsonArray
     */
    @SuppressWarnings("java:S125")
    public String convertJsonToXml(JSONObject json) {
        String xml = ServiceUtils.jsonConvertToXml(json.toJSONString(), jsonToXmlTransformer);
        boolean isValid = false;

        try {
            isValid = validateXML(new InputSource((new ByteArrayInputStream(xml.getBytes(
                StandardCharsets.UTF_8)))));
        } catch (RuntimeException e) {
            log.error(e);
        }

        return isValid ? xml : null;
    }


    private boolean validateXML(InputSource inputSource) {
        ValidationData validationData = validationService.validateLDML(inputSource);
        if (validationData.getMessageCount() > 0) {
            log.error("Validation messages: \n{}", validationData);
        }
        return validationData.getErrorCount() == 0;
    }

}
