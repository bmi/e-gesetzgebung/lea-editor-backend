// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.impl;

import de.itzbund.egesetz.bmi.lea.domain.services.validate.ValidationResult;

/**
 * Default values for validation outcomes. Can be extended by another implementation of the interface {@link ValidationResult}.
 */
public enum CompoundDocumentValidationResult implements ValidationResult {

    /**
     * validation was successful
     */
    VALIDATION_SUCCESS,

    /**
     * Too much document (of a given type) should be inserted into compound document
     */
    TO_MUCH_DOCUMENTS_OF_GIVEN_TYPE,

    /**
     * validation was not performed so far
     */
    VALIDATION_NOT_PERFORMED,

    /**
     * parameters are null
     */
    PARAMETERS_NOT_CORRECTLY_INITIALISED,

    /**
     * Compound document is not in DRAFT mode
     */
    COMPOUND_DOCUMENT_IS_PROTECTED,

    /**
     * Type of document is not suitable for type of compound document
     */
    INVALID_DOCUMENT_TYPE

}
