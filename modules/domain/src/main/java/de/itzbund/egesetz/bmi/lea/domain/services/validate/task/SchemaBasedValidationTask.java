// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.task;

import javax.xml.validation.Schema;

/**
 * A class of {@link ValidationTask}s that are guided by a grammar represented as schema.
 */
public interface SchemaBasedValidationTask extends XmlValidationTask {

    /**
     * Sets the schema instance used for validation.
     *
     * @param schema the schema instance used for validation
     */
    void setSchema(Schema schema);

}
