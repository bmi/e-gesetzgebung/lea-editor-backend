// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.aggregate;

import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.Rights;
import de.itzbund.egesetz.bmi.lea.domain.enums.Roles;
import de.itzbund.egesetz.bmi.lea.domain.enums.VerfahrensType;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.services.CompoundDocumentUtil;
import de.itzbund.egesetz.bmi.lea.domain.services.RBACPermissionService;
import de.itzbund.egesetz.bmi.lea.domain.services.RegelungsvorhabenService;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@Getter
@Setter
@RequiredArgsConstructor
@ToString
@Log4j2
@Scope("prototype")
public class CompoundDocument {

	private static final String DBG_MSG = "Found compound documents {} for the user,";

	@Autowired
	private CompoundDocumentPersistencePort compoundDocumentRepository;
	@Autowired
	private RegelungsvorhabenService plategRequestService;
	@Autowired
	private Document document;
	private CompoundDocumentDomain compoundDocumentEntity;
	private List<Document> documents = new ArrayList<>();
	// Enthält die Zugriffsrechte
	private List<Rights> rechte = new ArrayList<>();

	@Autowired
	private ApplicationContext applicationContext;

	// ------------ Rollen und Rechte an der Dokumentenmappe --------------------

	public List<Roles> whichRolesOwnsUser(User user) {
		List<Roles> rolesList = new ArrayList<>();

		if (CompoundDocumentUtil.isUserCreaterOfCompoundDocument(this, user)) {
			// Auch Abstimmungsleiter der Hauptabstimmung?
			rolesList.add(Roles.ERSTELLER_DOKUMENTENMAPPE);
		} else if (plategRequestService.isUserParticipantForCompoundDocument(getCompoundDocumentId())) {
			rolesList.add(Roles.TEILNEHMER_HRA);
		}

		return rolesList;
	}

	// --------------------------------------------------------------------------


	private static List<CompoundDocumentDomain> filterContainingRegelungsvorhaben(List<UUID> allowedByRegelungsvorhabenId,
		List<CompoundDocumentDomain> compoundDocumentEntities) {

		// Filter only allowedByRegelungsvorhabenId
		List<CompoundDocumentDomain> allFiltersAppliedCompoundDocuments = compoundDocumentEntities.stream()
			.filter(cde -> allowedByRegelungsvorhabenId.contains(cde.getRegelungsVorhabenId()
				.getId()))
			.collect(Collectors.toList());

		log.debug(DBG_MSG, compoundDocumentEntities);
		return allFiltersAppliedCompoundDocuments;
	}


	/**
	 * @return The containing compound document id
	 */
	public synchronized CompoundDocumentId getCompoundDocumentId() {
		if (compoundDocumentEntity != null) {
			return compoundDocumentEntity.getCompoundDocumentId();
		}

		return null;
	}

	/**
	 * Set the compound document id, mainly used by tests
	 *
	 * @param compoundDocumentID the compound document id
	 */
	public synchronized void setCompoundDocumentId(CompoundDocumentId compoundDocumentID) {
		if (compoundDocumentEntity == null) {
			compoundDocumentEntity = CompoundDocumentDomain.builder()
				.build();
		}

		compoundDocumentEntity.setCompoundDocumentId(compoundDocumentID);
	}

	// +----------------------------------------------
	// |
	// v

	/**
	 * @return The containing compound document create date
	 */
	public synchronized Instant getCreatedAt() {
		if (compoundDocumentEntity != null) {
			return compoundDocumentEntity.getCreatedAt();
		}

		return null;
	}

	/**
	 * @return The containing compound document create user
	 */
	public synchronized User getCreatedBy() {
		if (compoundDocumentEntity != null) {
			return compoundDocumentEntity.getCreatedBy();
		}

		return null;
	}

	// ^
	// |
	// +---------------------------------------------------

	/**
	 * Stores an empty compound document entity in the database
	 *
	 * @return success or fail
	 */
	public CompoundDocument createOrUpdate(User user) {
		try {
			if (compoundDocumentEntity == null) {
				compoundDocumentEntity = CompoundDocumentDomain.builder().build();
			}

			// Ab Java 17 mit ??
			if (compoundDocumentEntity.getState() == null) {
				compoundDocumentEntity.setState(DocumentState.DRAFT);
				compoundDocumentEntity.setVerfahrensType(VerfahrensType.REFERENTENENTWURF);
				compoundDocumentEntity.setVersion("1.0");

				LeageSession leageSession = applicationContext.getBean(LeageSession.class);
				compoundDocumentEntity.setCreatedBy(leageSession.getUser());
				compoundDocumentEntity.setUpdatedBy(leageSession.getUser());
			}

			if (compoundDocumentEntity.getCompoundDocumentId() == null) {
				compoundDocumentEntity.setCompoundDocumentId(new CompoundDocumentId(UUID.randomUUID()));
			}

			compoundDocumentEntity = compoundDocumentRepository.save(compoundDocumentEntity);
		} catch (NullPointerException e) {
			log.error("Compound document creation not successfully", e);
		}

		return this;
	}

	public CompoundDocument createOrUpdate(CompoundDocument compoundDocument, User user) {
		compoundDocumentEntity = compoundDocument.getCompoundDocumentEntity();
		return createOrUpdate(user);
	}

	/**
	 * Adds document to compound document and stores the document in DB
	 *
	 * @param document The document (as aggregate)
	 * @return success or fail
	 */
	// DELEGATE
	public boolean addDocumentToCompoundDocument(Document document, CompoundDocument compoundDocument) {
		Document document1 = document.addDocumentToCompoundDocument(document, compoundDocument);
		return document1.getDocumentEntity()
			.getDocumentId() != null;
	}

	// DELEGATE
	public List<Document> getDocumentsFromCompoundDocument(CompoundDocumentId compoundDocumentId,
		DocumentState documentState) {
		log.debug("Delegate to Get documents from document");
		if (documentState == null) {
			documentState = getStateOfCompoundDocumentForInheritance(compoundDocumentId);
		}
		return document.getDocumentsForCompoundDocument(compoundDocumentId, documentState);
	}

	/**
	 * Get all compound documents, maybe restricted by user and/or RVs
	 *
	 * @param userEntity                   Null for all compound documents or a user for only his compound documents
	 * @param allowedByRegelungsvorhabenId A list of RVs for filtering out
	 * @return A list of compound documents maybe restricted by the above filters
	 */
	public List<CompoundDocument> getCompoundDocumentsOptionalFilteredByUserUndRegelungsvorhaben(User userEntity, List<UUID> allowedByRegelungsvorhabenId) {
		return filterForUserAndRegelungsvorhaben(userEntity, allowedByRegelungsvorhabenId);
	}

	/**
	 * Get all compound document, restricted by user and NOT owner of Regelungsvorhaben
	 *
	 * @param userEntity                   The user
	 * @param allowedByRegelungsvorhabenId The Regelungsvorhaben that should be filtered out
	 * @return A filtered list
	 */
	public List<CompoundDocument> getCompoundDocumentsFilteredByUserUndOhneAngegebenRegelungsvorhaben(User userEntity,
		List<UUID> allowedByRegelungsvorhabenId) {

		List<RegelungsVorhabenId> regelungsVorhabenIds = allowedByRegelungsvorhabenId.parallelStream()
			.map(RegelungsVorhabenId::new)
			.collect(Collectors.toList());
		List<CompoundDocumentDomain> compoundDocumentDomains =
			compoundDocumentRepository.findByCreatedBySortedByUpdatedAtRegelungsVorhabenIdNotIn(
				userEntity, regelungsVorhabenIds);

		return compoundDocumentDomains.stream()
			.parallel()
			.map(compoundDocumentDomain -> {
				CompoundDocument compoundDocument = applicationContext.getBean(CompoundDocument.class);
				compoundDocument.setCompoundDocumentEntity(compoundDocumentDomain);
				compoundDocument.setDocuments(getDocumentsFromCompoundDocument(
					compoundDocumentDomain.getCompoundDocumentId(), compoundDocumentDomain.getState()));
				return compoundDocument;
			})
			.collect(Collectors.toList());
	}

	private List<CompoundDocument> filterForUserAndRegelungsvorhaben(User userEntity, List<UUID> allowedByRegelungsvorhabenId) {

		// Alle Dokumentenmappe oder alle Dokumentenmappen eines Benutzers
		List<CompoundDocumentDomain> compoundDocumentEntities = getCompoundDocumentDomainsOptionalFiltered(userEntity);
		// Entfernen alle Dokumentenmappen deren Regelungsvorhaben nicht in der o.g. Liste enthalten sind
		List<CompoundDocumentDomain> allFiltersAppliedCompoundDocuments = filterContainingRegelungsvorhaben(allowedByRegelungsvorhabenId,
			compoundDocumentEntities);

		// Anreichern der Dokumentenmappen mit den zugehörigen Dokumenten
		return allFiltersAppliedCompoundDocuments.stream()
			.map(this::getCompoundDocumentWithDocuments)
			.collect(Collectors.toList());
	}

	private List<CompoundDocumentDomain> getCompoundDocumentDomainsOptionalFiltered(User userEntity) {
		List<CompoundDocumentDomain> compoundDocumentEntities;

		if (userEntity != null) {
			log.debug("Find compound documents for user {}", userEntity);
			compoundDocumentEntities =
				compoundDocumentRepository.findByCreatedBy(userEntity);
		} else {
			log.debug("Find unspecified compound documents");
			compoundDocumentEntities =
				compoundDocumentRepository.findAll();
		}
		return compoundDocumentEntities;
	}

	// @formatter:off
    /**
     * Creates the CompoundDocument with:
     * - its documents
     * - its access rights
     *
     * @param compoundDocumentEntity
     * @return The complete compound document
     */
    // @formatter:on
	public CompoundDocument getCompoundDocumentWithDocuments(CompoundDocumentDomain compoundDocumentEntity) {

		// Die entsprechenden Rechte herausfinden
		RBACPermissionService rbacPermissionService = applicationContext.getBean(RBACPermissionService.class);
		rechte = rbacPermissionService.getAccessRightsForCurrentUserOnGivenResource("DOKUMENTENMAPPE",
			compoundDocumentEntity.getCompoundDocumentId().getId());

		// Dokumentenmappe erstellen
		CompoundDocument compoundDocument = applicationContext.getBean(CompoundDocument.class);
		compoundDocument.setCompoundDocumentEntity(compoundDocumentEntity);
		compoundDocument.setRechte(rechte);

		// Die zugehörigen Dokumente anhängen
		List<Document> documentList = getDocumentsFromCompoundDocument(compoundDocumentEntity.getCompoundDocumentId(), compoundDocumentEntity.getState());
		documentList.forEach(document1 -> document1.setRechte(rechte));
		compoundDocument.setDocuments(documentList);

		return compoundDocument;
	}


	/**
	 * Gibt die Dokumente zurück, die zu dieser Dokumentenmappe gehören
	 *
	 * @return Die Dokumente dieser Dokumentenmappe
	 */
	public List<Document> getDokumenteDieserDokumentenmappe() {
		CompoundDocumentId compoundDocumentId = getCompoundDocumentId();
		if (compoundDocumentId == null) {
			return new ArrayList<>();
		}
		return getDocumentsFromCompoundDocument(compoundDocumentId, getState());
	}


	/**
	 * Retrieves a special compound documents for a specific user with all documents (including other ownership!)
	 *
	 * @param compoundDocumentId The id of the compound document
	 * @param userEntity         The user entity
	 * @return The requested compound document
	 */
	public CompoundDocument getCompoundDocumentWithDocumentsForIdAndOptionalFiltered(
		CompoundDocumentId compoundDocumentId,
		User userEntity) {
		Optional<CompoundDocumentDomain> myCompoundDocumentEntity = getCompoundDocumentEntityFiltered(
			compoundDocumentId, userEntity);

		// Create a Compound Document Aggregate from it
		if (myCompoundDocumentEntity.isPresent()) {
			CompoundDocument compoundDocument = applicationContext.getBean(CompoundDocument.class);
			compoundDocument.setCompoundDocumentEntity(myCompoundDocumentEntity.get());
			// and add the documents also, delegated through aggregate
			compoundDocument.setDocuments(
				getDocumentsFromCompoundDocument(compoundDocumentId, null)
			);

			return compoundDocument;
		}

		return null;
	}


	private Optional<CompoundDocumentDomain> getCompoundDocumentEntityFiltered(CompoundDocumentId compoundDocumentId,
		User userEntity) {

		Optional<CompoundDocumentDomain> myCompoundDocumentEntity;

		if (userEntity != null) {
			log.debug("Find compound documents with id '{}' for user '{}'.", compoundDocumentId, userEntity);
			myCompoundDocumentEntity =
				compoundDocumentRepository.findByCreatedByAndCompoundDocumentId(userEntity, compoundDocumentId);
		} else {
			log.debug("Find compound documents with id '{}'.", compoundDocumentId);
			myCompoundDocumentEntity = compoundDocumentRepository.findByCompoundDocumentId(
				compoundDocumentId);
		}

		log.debug("Found compound document '{}'", myCompoundDocumentEntity);
		return myCompoundDocumentEntity;
	}


	/**
	 * Tells, if compound documents are writeable.
	 *
	 * @return true or false
	 */
	public boolean isWriteProtected() {
		if (compoundDocumentEntity != null) {
			return (getStateOfCompoundDocumentForInheritance(compoundDocumentEntity.getCompoundDocumentId())
				!= DocumentState.DRAFT);
		}

		return false;
	}


	/**
	 * @return The id from the regelungsvorhaben
	 */
	public RegelungsVorhabenId getRegelungsVorhabenId() {
		if (compoundDocumentEntity != null) {
			return compoundDocumentEntity.getRegelungsVorhabenId();
		}

		return null;
	}


	/**
	 * Retrieves the compound document's state by its id from the database
	 *
	 * @param compoundDocumentId Id of compound document
	 * @return State of compound document
	 */
	public DocumentState getStateOfCompoundDocumentForInheritance(CompoundDocumentId compoundDocumentId) {
		Optional<CompoundDocumentDomain> compoundDocument = compoundDocumentRepository.findByCompoundDocumentId(
			compoundDocumentId);

		return compoundDocument.map(CompoundDocumentDomain::getState)
			.orElse(null);

	}


	public DocumentState getState() {
		if (compoundDocumentEntity != null) {
			return compoundDocumentEntity.getState();
		}

		return null;
	}


	public void updateState(DocumentState state) {
		if (compoundDocumentEntity != null) {
			compoundDocumentEntity.setState(state);
		}

	}

}
