// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.document;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Data
@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
public class DocumentAllDTO {

    @JsonProperty("title")
    @Schema(name = "title", example = "Energiesicherungsgesetz - EnSiG")
    protected String title;

    @JsonProperty("type")
    @Schema(name = "type", example = "VORBLATT_STAMMGESETZ")
    protected DocumentType type;

}
