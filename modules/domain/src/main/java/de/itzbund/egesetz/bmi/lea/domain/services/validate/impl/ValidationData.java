// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.impl;

import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.ValidationTask;
import lombok.Getter;
import lombok.ToString;
import org.json.simple.JSONValue;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_TYPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_VAL_ERR_COUNT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_VAL_MSG;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_VAL_MSG_LIST;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_VAL_SEVERITY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_VAL_WARN_COUNT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_VALIDATION_RESULT;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.asString;

/**
 * A data container to store validation messages for later use, e.g. by the GUI, separated by {@link ValidationTask} and {@link SeverityLevel}.
 */

@Component
@Scope("prototype")
@ToString
@SuppressWarnings("unused")
public class ValidationData {

    private static final Collection<SeverityLevel> DEFAULT_LEVELS = Arrays.asList(
        SeverityLevel.ERROR,
        SeverityLevel.FATAL,
        SeverityLevel.WARNING
    );

    private Map<ValidationTask, Map<SeverityLevel, Collection<ValidationMessage>>> messages =
        new HashMap<>();

    /**
     * -- GETTER -- Returns the number of validation messages.
     */
    @Getter
    private int messageCount = 0;


    /**
     * Adds a new {@link ValidationMessage}.
     *
     * @param message the message to add
     */
    public void addMessage(ValidationMessage message) {
        ValidationTask task = message.getTask();
        SeverityLevel level = message.getLevel();

        Map<SeverityLevel, Collection<ValidationMessage>> taskMessages = messages
            .computeIfAbsent(task, key -> new HashMap<>());

        Collection<ValidationMessage> validationMessages = taskMessages
            .computeIfAbsent(level, key -> new ArrayList<>());

        validationMessages.add(message);
        messageCount++;
    }


    /**
     * Resets variables
     */
    public void reset() {
        messageCount = 0;
        messages = new HashMap<>();
    }


    /**
     * Returns the number of error messages; summarizes errors and fatal errors.
     *
     * @return the number of validation errors
     */
    public int getErrorCount() {
        int count = 0;
        for (Map<SeverityLevel, Collection<ValidationMessage>> map : messages.values()) {
            Collection<ValidationMessage> coll = map.get(
                SeverityLevel.ERROR);
            count += coll == null ? 0 : coll.size();
            coll = map.get(SeverityLevel.FATAL);
            count += coll == null ? 0 : coll.size();
        }
        return count;
    }


    /**
     * Returns the number of validation warnings.
     *
     * @return the number of validation warnings
     */
    public int getWarningCount() {
        int count = 0;
        for (Map<SeverityLevel, Collection<ValidationMessage>> map : messages.values()) {
            Collection<ValidationMessage> coll = map.get(
                SeverityLevel.WARNING);
            count += coll == null ? 0 : coll.size();
        }
        return count;
    }


    /**
     * Returns the number of error messages related to the given {@link ValidationTask}; summarizes errors and fatal errors.
     *
     * @return the number of validation errors in the given task
     */
    public int getErrorCount(ValidationTask task) {
        if (task == null) {
            return 0;
        }
        Map<SeverityLevel, Collection<ValidationMessage>> map = messages.get(task);
        if (map == null) {
            return 0;
        }
        Collection<ValidationMessage> coll = map.get(SeverityLevel.ERROR);
        int count = coll == null ? 0 : coll.size();
        coll = map.get(SeverityLevel.FATAL);
        count += coll == null ? 0 : coll.size();
        return count;
    }


    /**
     * Returns the number of validation warnings related to the given {@link ValidationTask}.
     *
     * @return the number of validation warnings in the given task
     */
    @SuppressWarnings("unused")
    public int getWarningCount(ValidationTask task) {
        if (task == null) {
            return 0;
        }
        Map<SeverityLevel, Collection<ValidationMessage>> map = messages.get(task);
        if (map == null) {
            return 0;
        }
        Collection<ValidationMessage> coll = map.get(SeverityLevel.WARNING);
        return coll == null ? 0 : coll.size();
    }


    /**
     * Returns a {@link Map} of all validation messages related to the given {@link ValidationTask}, separated by {@link SeverityLevel}.
     *
     * @param task the validation task
     * @return all validation messages for this task
     */
    public Map<SeverityLevel, Collection<ValidationMessage>> getMessages(ValidationTask task) {
        Map<SeverityLevel, Collection<ValidationMessage>> map = messages.get(task);
        if (map == null) {
            return new EnumMap<>(SeverityLevel.class);
        }
        return map;
    }


    /**
     * Returns a collection of all validation messages with one of the given severity levels.
     *
     * @param severityLevels array of {@link SeverityLevel}s
     * @return a collection of all validation messages with one of the given severity levels
     */
    public Collection<ValidationMessage> getMessages(SeverityLevel... severityLevels) {
        Collection<ValidationMessage> messageCollection = new ArrayList<>();
        Set<SeverityLevel> levels = new HashSet<>();

        if (severityLevels.length == 0) {
            levels.addAll(DEFAULT_LEVELS);
        } else {
            levels.addAll(Arrays.asList(severityLevels.clone()));
        }

        for (Map<SeverityLevel, Collection<ValidationMessage>> map : messages.values()) {
            for (Map.Entry<SeverityLevel, Collection<ValidationMessage>> entry : map.entrySet()) {
                if (levels.contains(entry.getKey())) {
                    messageCollection.addAll(entry.getValue());
                }
            }
        }

        return messageCollection;
    }


    /**
     * Returns a JSON representation of the validation messages.
     *
     * @return a JSON representation of the validation messages
     */
    public String getJsonFormat() {
        StringBuilder sb = new StringBuilder();

        sb.append('{')
            .append(asString(JSON_KEY_TYPE)).append(':')
            .append(asString(JSON_VAL_VALIDATION_RESULT)).append(',')
            .append(asString(JSON_KEY_VAL_ERR_COUNT)).append(':').append(getErrorCount()).append(',')
            .append(asString(JSON_KEY_VAL_WARN_COUNT)).append(':').append(getWarningCount()).append(',')
            .append(asString(JSON_KEY_VAL_MSG_LIST)).append(":[");

        for (Iterator<ValidationMessage> it = getMessages().iterator(); it.hasNext(); ) {
            ValidationMessage msg = it.next();
            sb.append('{')
                .append(asString(JSON_KEY_VAL_SEVERITY)).append(':').append(asString(msg.getLevel().name()))
                .append(',').append(asString(JSON_KEY_VAL_MSG)).append(':')
                .append(asString(JSONValue.escape(msg.getMessage())))
                .append('}');

            if (it.hasNext()) {
                sb.append(',');
            }
        }

        sb.append("]}");

        return sb.toString();
    }

}
