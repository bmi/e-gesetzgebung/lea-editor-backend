// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.abstimmung;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.itzbund.egesetz.bmi.lea.domain.enums.AbstimmungstypType;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentAccessType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
public class AbstimmungEditorDTO {

    /**
     * ID der Abstimmung
     */
    @NotNull
    @JsonProperty("abstimmungId")
    @Valid
    @Schema(name = "abstimmungId", required = true)
    UUID abstimmungId;

    /**
     * Anlagedatum der Abstimmung
     */
    @Schema(name = "erstelltAm", example = "19.02.2022", required = true)
    Instant erstelltAm;

    /**
     * Letzte Änderungsdatum der Abstimmung
     */
    @Schema(name = "bearbeitetAm", example = "19.02.2022", required = true)
    Instant bearbeitetAm;

    /**
     * Typ der Abstimmung, bspw. Haus- oder Ressortabstimmung
     */
    @JsonProperty("type")
    @Schema(name = "type", required = true)
    @Valid
    AbstimmungstypType abstimmungsTyp;

    /**
     * Klassifizierung bzw. Art der Freigabe: Gibt an, ob der aktuelle User als Teilnehmer einer Abstimmung auf die Dokumentenmappe zugreifen darf oder als
     * Einleiter einer Abstimmung die Mappe als Antwort des Teilnehmers sehen darf
     */
    @JsonProperty("accessType")
    @Schema(name = "accessType", required = true)
    @Valid
    CompoundDocumentAccessType accessType;

}
