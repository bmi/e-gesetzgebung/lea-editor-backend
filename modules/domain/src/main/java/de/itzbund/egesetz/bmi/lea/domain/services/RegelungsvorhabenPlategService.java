// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.PropositionPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.RegelungsvorhabenPlategRestPort;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Log4j2
@SuppressWarnings("unused")
public class RegelungsvorhabenPlategService implements RegelungsvorhabenPlategRestPort {

    // ----- Repositories -----
    @Autowired
    private PropositionPersistencePort propositionPersistencePort;

    @Autowired
    private PropositionMapper propositionMapper;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public List<ServiceState> regelungsvorhabenCreateOrUpdate(List<RegelungsvorhabenEditorDTO> regelungsvorhabenDTO) {

        List<ServiceState> results = new ArrayList<>();

        regelungsvorhabenDTO.stream().forEach(
            regelungsvorhabenEditorDTO -> {

                Proposition byPropositionId = propositionPersistencePort.findByUpdateablePropositionId(
                    new RegelungsVorhabenId(regelungsvorhabenEditorDTO.getId()));
                Proposition proposition = propositionMapper.mapD(regelungsvorhabenEditorDTO);

                // Erstellen
                if (byPropositionId == null) {
                    propositionPersistencePort.save(proposition);
                    results.add(ServiceState.CREATED);
                } else {
                    if (!someCopoundDocumentIsNoMoreModifiable(regelungsvorhabenEditorDTO.getId())) {
                        // Update
                        Proposition update = propositionPersistencePort.update(proposition);
                        if (update != null) {
                            results.add(ServiceState.OK);
                        } else {
                            log.error("Nothing to update found!");
                        }
                    }
                }

                // Update nicht erlaubt
                results.add(ServiceState.NOT_MODIFIED);
            });

        return results;
    }

    // Obsolet
    private boolean someCopoundDocumentIsNoMoreModifiable(UUID regelungsvorhabenId) {

        CompoundDocument compoundDocument = applicationContext.getBean(CompoundDocument.class);
        List<CompoundDocument> compoundDocuments = compoundDocument.getCompoundDocumentsOptionalFilteredByUserUndRegelungsvorhaben(
            null, List.of(regelungsvorhabenId));

        Optional<CompoundDocument> first = compoundDocuments.stream()
            .filter(x -> x.getCompoundDocumentEntity().getState() != DocumentState.DRAFT).findFirst();

        return first.isPresent();

    }

}
