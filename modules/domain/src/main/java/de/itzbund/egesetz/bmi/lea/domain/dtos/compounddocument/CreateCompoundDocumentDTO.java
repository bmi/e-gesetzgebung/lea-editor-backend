// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.Valid;

@Data
@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class CreateCompoundDocumentDTO extends CompoundDocumentAllDTO {

    @Valid
    @JsonProperty("regelungsVorhabenId")
    @Schema(name = "regelungsVorhabenId", example = "00000000-0000-0000-0000-000000000005")
    private String regelungsVorhabenId;

    @JsonProperty("titleRegulatoryText")
    @Schema(name = "titleRegulatoryText", example = "Title for a regulatory text")
    private String titleRegulatoryText;

    @Builder.Default
    @JsonProperty("initialNumberOfLevels")
    @Schema(name = "initialNumberOfLevels", example = "1")
    private int initialNumberOfLevels = 0;

}
