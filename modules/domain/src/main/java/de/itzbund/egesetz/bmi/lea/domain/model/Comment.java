// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model;

import de.itzbund.egesetz.bmi.lea.domain.enums.CommentState;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.time.Instant;
import java.util.List;

@AllArgsConstructor
@Builder
@Data
public class Comment {

    private DocumentId documentId;

    private CommentId commentId;

    private String content;

    @Builder.Default
    private CommentState state = CommentState.OPEN;

    private User createdBy;

    private User updatedBy;

    private Instant createdAt;

    private Instant updatedAt;

    private Position anchor;

    private Position focus;

    @Builder.Default
    private boolean deleted = false;

    @Singular
    private List<Reply> replies;

}
