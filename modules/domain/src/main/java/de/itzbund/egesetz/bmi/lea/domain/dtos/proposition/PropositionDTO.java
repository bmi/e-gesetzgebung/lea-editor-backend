// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.proposition;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.itzbund.egesetz.bmi.lea.domain.enums.FarbeType;
import de.itzbund.egesetz.bmi.lea.domain.enums.PropositionState;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentInitiant;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

/**
 * Regelungsvorhaben
 */
@Data
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class PropositionDTO {

    @JsonProperty("id")
    @NotNull
    @Valid
    @Schema(name = "id", required = true)
    private UUID id;

    @NotNull
    @JsonProperty("title")
    @Schema(name = "title", example = "Beispiel Regelungsvorhaben", required = true)
    private String title;

    @JsonProperty("shortTitle")
    @Schema(name = "shortTitle", example = "Bsp. RegV")
    private String shortTitle;

    @JsonProperty("abbreviation")
    @Schema(name = "abbreviation", example = "BspRV")
    private String abbreviation;

    @JsonProperty("state")
    @Schema(name = "state", example = "IN_BEARBEITUNG")
    private PropositionState state;

    @JsonProperty("proponent")
    @Valid
    @Schema(name = "proponent")
    private ProponentDTO proponentDTO;

    @JsonProperty("initiant")
    @Valid
    @Schema(name = "initiant")
    private RechtsetzungsdokumentInitiant initiant;

    @JsonProperty("type")
    @Valid
    @Schema(name = "type")
    private RechtsetzungsdokumentTyp type;

    @JsonProperty("bearbeitetAm")
    @Schema(name = "bearbeitetAm", example = "2022-07-14T16:09:57.637388Z", required = true)
    private Instant bearbeitetAm;

    @JsonProperty("farbe")
    @Schema(name = "farbe", example = "BLAU")
    private FarbeType farbe;

}
