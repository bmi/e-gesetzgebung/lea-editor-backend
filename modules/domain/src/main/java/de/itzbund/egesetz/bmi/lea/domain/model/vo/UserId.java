// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model.vo;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * A domain related id for users.
 */
@Getter
@EqualsAndHashCode
public class UserId {

    @EqualsAndHashCode.Include
    private String id;


    public UserId(String id) {
        this.setId(id);
    }


    @Override
    public String toString() {
        return String.format("%s(id=%s)", UserId.class.getSimpleName(), this.getId());
    }


    private void setId(String id) {
        if (this.id != null) {
            throw new IllegalStateException("The id may not be changed.");
        }

        if (id == null) {
            throw new IllegalArgumentException("The id may not be set to null.");
        }

        this.id = id;
    }
}
