// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde;

import de.itzbund.egesetz.bmi.lea.domain.model.ldmlde.LDMLDeModellElementBezeichnung;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum VorblattabschnittRefersToLiteral implements RefersToLiteral {

    DAVON_BUEROKRATIEKOSTEN_AUS_INFORMATIONSPFLICHTEN("davon-buerokratiekosten-aus-informationspflichten"),
    ERFUELLUNGSAUFWAND_DER_VERWALTUNG("erfuellungsaufwand-der-verwaltung"),
    ERFUELLUNGSAUFWAND_FUER_BUERGERINNEN_UND_BUERGER("erfuellungsaufwand-fuer-buergerinnen-und-buerger"),
    ERFUELLUNGSAUFWAND_FUER_DIE_WIRTSCHAFT("erfuellungsaufwand-fuer-die-wirtschaft"),
    VORBLATTABSCHNITT_ALTERNATIVEN("vorblattabschnitt-alternativen"),
    VORBLATTABSCHNITT_ERFUELLUNGSAUFWAND("vorblattabschnitt-erfuellungsaufwand"),
    VORBLATTABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND(
        "vorblattabschnitt-haushaltsausgaben-ohne-erfuellungsaufwand"),
    VORBLATTABSCHNITT_LOESUNG("vorblattabschnitt-loesung"),
    VORBLATTABSCHNITT_PROBLEM_UND_ZIEL("vorblattabschnitt-problem-und-ziel"),
    VORBLATTABSCHNITT_WEITERE_KOSTEN("vorblattabschnitt-weitere-kosten");

    private static final Map<String, VorblattabschnittRefersToLiteral> literals;
    static {
        literals = new HashMap<>();
        Arrays.stream(values()).forEach(v -> literals.put(v.getLiteral(), v));
    }

    private final String literal;


    @Override
    public LDMLDeModellElementBezeichnung getContext() {
        return LDMLDeModellElementBezeichnung.VORBLATTABSCHNITT;
    }


    public static VorblattabschnittRefersToLiteral fromLiteral(String literal) {
        return literals.get(literal);
    }

}
