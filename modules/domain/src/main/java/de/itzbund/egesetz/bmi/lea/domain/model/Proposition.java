// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model;

import de.itzbund.egesetz.bmi.lea.domain.enums.FarbeType;
import de.itzbund.egesetz.bmi.lea.domain.enums.PropositionState;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentInitiant;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.Instant;
import java.util.UUID;

@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode
public class Proposition {

    private RegelungsVorhabenId propositionId;

    private String title;

    private String shortTitle;

    private String abbreviation;

    private PropositionState state;

    private String proponentId;

    private String proponentTitle;

    private String proponentDesignationGenitive;

    private String proponentDesignationNominative;

    private boolean proponentActive;

    private RechtsetzungsdokumentInitiant initiant;

    private RechtsetzungsdokumentTyp type;

    @EqualsAndHashCode.Exclude
    private Instant createdAt;

    private FarbeType farbe;

    @EqualsAndHashCode.Exclude
    private UUID referenzId;

}
