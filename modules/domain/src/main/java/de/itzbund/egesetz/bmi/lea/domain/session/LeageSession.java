// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.session;

import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentInitiant;
import de.itzbund.egesetz.bmi.lea.domain.mappers.UserMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserPersistencePort;
import de.itzbund.egesetz.bmi.user.entities.StellvertreterEntity;
import de.itzbund.egesetz.bmi.user.repositories.StellvertreterEntityRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Log4j2
@SuppressWarnings("unused")
public class LeageSession {

    private static final String ANON_USER = "anonymousUser";
    public static final String EMAIL = "EMAIL";
    public static final String NAME = "NAME";
    public static final String GID = "GID";

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserPersistencePort userRepository;

    @Autowired
    private StellvertreterEntityRepository stellvertreterEntityRepository;

    // the GID of the user who is being represented
    private User representedUser;

    public RechtsetzungsdokumentInitiant getEditorModus() {
        return RechtsetzungsdokumentInitiant.BUNDESREGIERUNG;
    }


    public User updateUser() {
        return getUser();
    }

    /**
     * delivers current user data
     *
     * @return User (from LEA) update from latest IAM token.
     */
    public User getUser() {
        User user;
        if (representedUser == null) {
            user = getUserFromAuthentication();
        } else {
            user = representedUser;
        }
        return user;
    }

    private User getUserFromAuthentication() {

        var userTokenDatails = getUserDatailsFromAuthentication();

        User storedUser = userRepository.findFirstByGid(new UserId(userTokenDatails.get(GID)));

        User user = User.builder()
            .gid(new UserId(userTokenDatails.get(GID)))
            .email(userTokenDatails.get(EMAIL))
            .name(userTokenDatails.get(NAME))
            .build();

        if (storedUser != null) {
            user.setPlattformUserId(storedUser.getPlattformUserId());
        }

        return user;
    }

    public static Map<String, String> getUserDatailsFromAuthentication() {

        Map<String, String> userDetails = new HashMap<>();

        Authentication authentication = getAuthentication();

        if (authentication instanceof OAuth2AuthenticationToken) {
            OAuth2AuthenticationToken oAuth2AuthenticationToken = (OAuth2AuthenticationToken) authentication;
            OAuth2User principal = oAuth2AuthenticationToken.getPrincipal();

            userDetails.put(EMAIL, principal.getAttribute("email"));
            userDetails.put(NAME, principal.getAttribute("name"));
            userDetails.put(GID, principal.getAttribute("gid"));

        } else if (authentication instanceof UsernamePasswordAuthenticationToken) {
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = (UsernamePasswordAuthenticationToken) authentication;
            OidcUser oidcUser = (OidcUser) usernamePasswordAuthenticationToken.getPrincipal();

            userDetails.put(EMAIL, oidcUser.getEmail());
            userDetails.put(NAME, (String) oidcUser.getClaims().get("name"));
            userDetails.put(GID, (String) oidcUser.getClaims().get("gid"));
        } else if (authentication instanceof AnonymousAuthenticationToken) {
            AnonymousAuthenticationToken anonymousAuthenticationToken = (AnonymousAuthenticationToken) authentication;
            Object principal = authentication.getPrincipal();

            if (principal instanceof User) {
                User user = (User) principal;

                userDetails.put(EMAIL, user.getEmail());
                userDetails.put(NAME, user.getName());
                userDetails.put(GID, user.getGid().getId());
            } else {
                return null;
            }
        }

        return userDetails;
    }

    private static Authentication getAuthentication() {
        return SecurityContextHolder.getContext()
            .getAuthentication();
    }

    public void setRepresentedUser(User representedUser) {
        if (representedUser != null) {
            StellvertreterEntity stellvertreterEntity = getStellvertretung(getUserFromAuthentication(), representedUser);
            if (stellvertreterEntity != null) {
                this.representedUser = representedUser;
            } else {
                this.representedUser = null;
            }
        } else {
            this.representedUser = null;
        }
    }

    private StellvertreterEntity getStellvertretung(User loggedInUser, User representedUser) {
        if (loggedInUser != null && representedUser != null) {
            return stellvertreterEntityRepository.findByNutzerIdAndStellvertreterId(
                    representedUser.getPlattformUserId(), loggedInUser.getPlattformUserId())
                .orElse(null);
        } else {
            return null;
        }
    }
}
