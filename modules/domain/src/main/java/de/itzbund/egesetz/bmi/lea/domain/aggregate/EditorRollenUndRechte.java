// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.aggregate;

import de.itzbund.egesetz.bmi.auth.api.rights.AbstractEGesetzRechte;
import de.itzbund.egesetz.bmi.auth.api.rights.EGesetzRechte;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * see: de.itzbund.egesetz.bmi.lea.controllers.security.CustomPermissionEvaluator
 */
@Component
@Log4j2
@SuppressWarnings("unused")
public class EditorRollenUndRechte extends AbstractEGesetzRechte implements EGesetzRechte {

	@Autowired
	private ApplicationContext applicationContext;


	@Override
	public String getStatusForResourceType(String ressourcenTyp, String ressourcenId) {
		switch (ressourcenTyp) {
			// Haus- u. Ressortabstimmungen, sowie Regelungsvorhaben sind unabhängig vom Status der Dokumentenmappen
			case "HRA":
			case "REGELUNGSVORHABEN":
				return "DONT CARE";

			// In der Dokumentenmappe steht direkt ihr Status
			case "DOKUMENTENMAPPE":
				return getStateOfCompoundDocument(new CompoundDocumentId(UUID.fromString(ressourcenId)));

			// Bei Dokumenten muss man erst ihre Mappe bestimmen. In der Dokumentenmappe steht dann der Status.
			case "DOKUMENTE":
				Document document = applicationContext.getBean(Document.class);
				CompoundDocumentId containingCompoundDocumentId = document.getContainingCompoundDocumentId(
					new DocumentId(UUID.fromString(ressourcenId)));
				return getStateOfCompoundDocument(containingCompoundDocumentId);

			default:
				return null;
		}

	}

	/**
	 * Helper to convert String-List into UUID-List
	 *
	 * @param benutzerId    The UUID of the user
	 * @param ressourcenTyp The name of ressource type
	 * @return
	 */
	public List<UUID> getAlleRessourcenFuerBenutzerUndRessourceTypAsUUID(String benutzerId, String ressourcenTyp) {
		List<String> allResourceIds = getAlleRessourcenFuerBenutzerUndRessourceTyp(benutzerId, ressourcenTyp);
		return allResourceIds.stream()
			.map(EditorRollenUndRechte::convertToUUID)
			.filter(uuid -> uuid != null)
			.collect(Collectors.toList());
	}

	private static UUID convertToUUID(String str) {
		try {
			return UUID.fromString(str);
		} catch (IllegalArgumentException e) {
			log.error("Invalid UUID: ", e);
			return null;
		}
	}

	private String getStateOfCompoundDocument(CompoundDocumentId containingCompoundDocumentId) {
		CompoundDocument compoundDocument = applicationContext.getBean(CompoundDocument.class);
		DocumentState compoundDocumentState = compoundDocument.getStateOfCompoundDocumentForInheritance(
			containingCompoundDocumentId);
		return compoundDocumentState.toString();
	}

}
