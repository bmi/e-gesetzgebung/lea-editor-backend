// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.dtos.paging;

public enum SortByType {

    CREATED_AT("createdAt"),
    ERSTELLT_AM("erstelltAm"),
    ZULETZT_BEARBEITET("bearbeitetAm"),
    VERSION("version");

    public final String label;

    SortByType(String label) {
        this.label = label;
    }

}