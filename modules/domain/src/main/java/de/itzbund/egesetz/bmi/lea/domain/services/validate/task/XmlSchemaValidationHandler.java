// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.task;

import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.SeverityLevel;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationData;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationMessage;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.xpath.XmlContentLocator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Application-specific implementation of a {@link org.xml.sax.ErrorHandler}.
 */
public class XmlSchemaValidationHandler extends DefaultHandler {

    private final ValidationData validationData;

    private final XmlSchemaValidationTask task;

    private final XmlContentLocator locator;


    public XmlSchemaValidationHandler(
        XmlSchemaValidationTask task, ValidationData data, XmlContentLocator locator) {
        this.task = task;
        validationData = data;
        this.locator = locator;
    }


    @Override
    public void warning(SAXParseException exception) throws SAXException {
        validationData.addMessage(
            new ValidationMessage(task, SeverityLevel.WARNING, getParseErrorInfo(exception)));
    }


    @Override
    public void error(SAXParseException exception) throws SAXException {
        validationData.addMessage(
            new ValidationMessage(task, SeverityLevel.ERROR, getParseErrorInfo(exception)));
    }


    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        validationData.addMessage(
            new ValidationMessage(task, SeverityLevel.FATAL, getParseErrorInfo(exception)));
    }


    private String getParseErrorInfo(SAXParseException spe) {
        String systemId = String.valueOf(spe.getSystemId());
        return String.format("URI=%s (path:%s): %s", systemId, locator.getXPath(spe.getLineNumber()),
            spe.getMessage());
    }

}
