// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.DownloadRestPort;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.SortedMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.MIME_TYPE_ZIP;
import static de.itzbund.egesetz.bmi.lea.core.Constants.getEmptyZipContent;

/**
 * A service for downloading content.
 */
@Service
@Log4j2
public class DownloadService implements DownloadRestPort {

    private static final String ERR_MSG = "Exception while reading and streaming data";

    /**
     * To create and write .zip file to the response's output stream.
     *
     * @param response  {@link HttpServletResponse} object that contains the ZIP archive
     * @param documents map of filenames to their XML documents belonging to the compound document
     */
    public void downloadCompoundDocumentAsZipFile(HttpServletResponse response, SortedMap<String, String> documents,
        String zipFilename) {
        response.setContentType(MIME_TYPE_ZIP);
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        response.setHeader("Content-Disposition", "attachment; filename=" +
            URLEncoder.encode(zipFilename, StandardCharsets.UTF_8));

        if (documents.isEmpty()) {
            try (BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream())) {
                byte[] emptyZip = getEmptyZipContent();
                bos.write(emptyZip, 0, emptyZip.length);
            } catch (IOException e) {
                log.error(ERR_MSG, e);
            }
        } else {
            try (ZipOutputStream zipOutputStream = new ZipOutputStream(response.getOutputStream())) {
                for (Map.Entry<String, String> entry : documents.entrySet()) {
                    String filename = entry.getKey();
                    String xmlContent = entry.getValue();
                    ZipEntry zipEntry = new ZipEntry(filename);
                    zipOutputStream.putNextEntry(zipEntry);

                    StreamUtils.copy(IOUtils.toInputStream(xmlContent, StandardCharsets.UTF_8), zipOutputStream);
                    zipOutputStream.closeEntry();
                }
                zipOutputStream.finish();
            } catch (IOException e) {
                log.error(ERR_MSG, e);
            }
        }
    }

}
