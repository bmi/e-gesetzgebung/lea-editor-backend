// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model.vo;

import java.util.UUID;

/**
 * A domain related id for comments.
 */
public class AbstimmungId extends AbstractId {

    public AbstimmungId(UUID id) {
        super(id);
    }

}
