// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.ReplyId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.time.Instant;

@AllArgsConstructor
@SuperBuilder
@Data
public class Reply {

    protected ReplyId replyId;

    protected CommentId commentId;

    protected DocumentId documentId;

    private String parentId;

    private String content;

    private User createdBy;

    private User updatedBy;

    private Instant createdAt;

    private Instant updatedAt;

    @Builder.Default
    private boolean deleted = false;

}
