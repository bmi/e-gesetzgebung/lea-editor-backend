// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.messages;

import java.util.ListResourceBundle;

public class DomainResourceBundle extends ListResourceBundle {

	@Override
	protected Object[][] getContents() {
		return new Object[][]{

			// Synopse
			{"msg.unsupported.content", "Unzulässiger Inhalt mit GUID {0} für juristische Absätze"},
			{"synopse.heading", "Fassung vom %s Uhr"},

			// Business logging
			{"dom.logging.export.ldml", "Export als LegalDocML.de"},
			{"dom.logging.import.ldml", "Import von LegalDocML.de"},
			{"dom.logging.export.pdf", "Export als PDF"},
			{"dom.logging.grant.write", "Übergabe von Schreibrechten auf Dokumentenmappe"},
			{"dom.logging.revoke.write", "Entzug von Schreibrechten auf Dokumentenmappe"},
			{"dom.logging.grant.read", "Übergabe von Leserechten auf Dokumentenmappe"},
			{"dom.logging.revoke.read", "Entzug von Leserechten auf Dokumentenmappe"},

		};
	}

}
