// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;
import javax.sql.rowset.serial.SerialBlob;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;
import de.itzbund.egesetz.bmi.lea.domain.dtos.media.MediaSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.MediaData;
import de.itzbund.egesetz.bmi.lea.domain.model.MediaMetadata;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.MediaMetadataPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.MediaPersistencePort;
import lombok.SneakyThrows;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class FileServiceTest {

    private final MediaMetadata mediaMetadataEntity = createMediaMetaData();
    @InjectMocks
    private FileService fileService;
    @Mock
    private MediaMetadataPersistencePort mediaMetadataRepository;
    @Mock
    private MediaPersistencePort mediadataRepository;


    @BeforeEach
    void setup() throws Exception {
        FieldUtils.writeField(fileService, "mediaMetadataRepo", mediaMetadataRepository, true);
        FieldUtils.writeField(fileService, "mediaDataRepo", mediadataRepository, true);
    }


    @Test
    @SneakyThrows
    void save_test_successful() {
        when(mediadataRepository.saveAndFlush(any(MediaData.class))).thenReturn(
            mediaMetadataEntity.getMediaData());
        when(mediaMetadataRepository.saveAndFlush(any())).thenReturn(
            mediaMetadataEntity);
        assertTrue(true);
    }


    @Test
    @SneakyThrows
    void loadFromDB_test_successful() {
        when(mediaMetadataRepository.findById(mediaMetadataEntity.getId())).thenReturn(
            Optional.of(mediaMetadataEntity));
        when(mediadataRepository.findById(mediaMetadataEntity.getMediaData().getId())).thenReturn(
            Optional.of(mediaMetadataEntity.getMediaData()));

        MediaSummaryDTO mediaSummaryDTO = fileService.loadFromDB(mediaMetadataEntity.getId());
        assertEquals(mediaMetadataEntity.getName(), mediaSummaryDTO.getName());
    }


    @Test
    void loadFromDB_test_unsuccessful() {
        UUID testId = UUID.randomUUID();

        try {
            fileService.loadFromDB(testId);
        } catch (ResponseStatusException ex) {
            assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
        }
        when(mediaMetadataRepository.findById(testId)).thenReturn(
            Optional.of(MediaMetadata.builder().mediaData(MediaData.builder().build()).build()));

        try {
            fileService.loadFromDB(testId);
        } catch (ResponseStatusException ex) {
            assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
        }
    }


    @Test
    void deleteFromDB_test() {
        UUID testId = UUID.randomUUID();

        try {
            fileService.deleteFromDB(testId);
        } catch (ResponseStatusException ex) {
            assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
        }
        doNothing().when(mediaMetadataRepository).deleteById(testId);

        when(mediaMetadataRepository.findById(mediaMetadataEntity.getId())).thenReturn(
            Optional.of(mediaMetadataEntity));

        when(mediaMetadataRepository.checkForeignKeyHasRelation(mediaMetadataEntity.getMediaData())).thenReturn(
            false);
        doNothing().when(mediadataRepository).deleteById(mediaMetadataEntity.getMediaData().getId());
        fileService.deleteFromDB(mediaMetadataEntity.getId());
        verify(mediadataRepository).deleteById(mediaMetadataEntity.getMediaData().getId());

    }


    @SneakyThrows
    private MediaMetadata createMediaMetaData() {
        MockMultipartFile mockMultipartFile = createMockFile();

        MediaData mediaData = MediaData.builder()
            .data(new SerialBlob(mockMultipartFile.getBytes()))
            .id(DigestUtils.sha256Hex(mockMultipartFile.getBytes()))
            .build();

        return MediaMetadata.builder()
            .mediaType(MediaType.IMAGE_JPEG.getType())
            .mediaData(mediaData)
            .id(UUID.randomUUID())
            .name(mockMultipartFile.getName())
            .createdDate(Instant.now())
            .build();
    }


    private MockMultipartFile createMockFile() {
        String path = "fileservice/logo.jpeg";
        ClassLoader classLoader = FileServiceTest.class.getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(path);
        assertNotNull(inputStream);

        MockMultipartFile mockMultipartFile = null;
        try {
            mockMultipartFile = new MockMultipartFile("testImage.jpg", inputStream);
        } catch (IOException e) {
            fail("test: could not create file");
        }
        return mockMultipartFile;
    }

}
