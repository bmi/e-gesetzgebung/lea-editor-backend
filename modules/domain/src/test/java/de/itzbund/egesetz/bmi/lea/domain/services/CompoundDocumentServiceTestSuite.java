// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;
import org.junit.platform.suite.api.SuiteDisplayName;

@Suite
@SuiteDisplayName("Dokumentenmappen Test Suite")
@SelectClasses({
    CompoundDocumentService1IntegrationTest.class,
    CompoundDocumentService2IntegrationTest.class,
    CompoundDocumentService3IntegrationTest.class,
    CompoundDocumentService4IntegrationTest.class})
public class CompoundDocumentServiceTestSuite {

}
