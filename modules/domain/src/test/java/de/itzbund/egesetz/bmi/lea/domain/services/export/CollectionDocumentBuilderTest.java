// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.export;

import de.itzbund.egesetz.bmi.lea.core.Constants;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentInitiant;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.model.dokument.RechtsetzungsdokumentHauptteil;
import de.itzbund.egesetz.bmi.lea.domain.model.dokument.TeildokumentVerweis;
import de.itzbund.egesetz.bmi.lea.domain.model.dokument.TeildokumentVerweisContainer;
import de.itzbund.egesetz.bmi.lea.domain.model.dokument.TeildokumentVerweisShowAsLiteral;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.TemplatePersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.services.CollectionDocumentBuilder;
import de.itzbund.egesetz.bmi.lea.domain.services.ConverterService;
import de.itzbund.egesetz.bmi.lea.domain.services.TemplateService;
import de.itzbund.egesetz.bmi.lea.domain.services.meta.MetadatenUpdateService;
import de.itzbund.egesetz.bmi.lea.domain.services.transform.JsonToXmlTransformer;
import de.itzbund.egesetz.bmi.lea.domain.services.transform.XmlToJsonTransformer;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.ValidationService;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.SchemaProvider;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationData;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationServiceImpl;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.Validator;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.SchematronValidationTask;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.XmlSchemaValidationTask;
import lombok.Builder;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.Instant;
import java.util.List;
import java.util.TreeMap;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_COLLECTIONBODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DOCUMENTCOLLECTION;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.clearChildrenArray;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDescendant;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDocumentObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.setGUIDsInTemplate;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.wrapUp;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link CollectionDocumentBuilder}.
 */
@SpringBootTest(classes = {
    // Services
    TemplateService.class, MetadatenUpdateService.class, JsonToXmlTransformer.class, ConverterService.class,
    ValidationServiceImpl.class,
    // Repositories
    TemplatePersistencePort.class,
    // Sonstige
    CollectionDocumentBuilder.class, XmlToJsonTransformer.class, SchemaProvider.class, Validator.class,
    XmlSchemaValidationTask.class, SchematronValidationTask.class, ValidationData.class
})
@Log4j2
@SuppressWarnings("unused")
class CollectionDocumentBuilderTest {

    private static final String PTN_EID_CONTAINER = "rdokhauptteil-1_tldokverweis-%s";
    private static final String PTN_EID_VERWEIS = PTN_EID_CONTAINER + "_verweis-1";

    private static String TPL_COLL_STAMMFORM;
    private static String TPL_COLL_MANTELFORM;

    private static Proposition testProposition;

    private TreeMap<String, DocumentType> docTypes;

    @Autowired
    private CollectionDocumentBuilder collectionDocumentBuilder;
    @Autowired
    private MetadatenUpdateService metadatenUpdateService;
    @Autowired
    private ConverterService converterService;
    @Autowired
    private XmlToJsonTransformer xmlToJsonTransformer;
    @Autowired
    private JsonToXmlTransformer jsonToXmlTransformer;
    @Autowired
    private ValidationService validationService;
    @Autowired
    private SchemaProvider schemaProvider;
    @Autowired
    private Validator validator;
    @Autowired
    private XmlSchemaValidationTask xmlSchemaValidationTask;
    @Autowired
    private SchematronValidationTask schematronValidationTask;
    @Autowired
    private ValidationData validationData;

    @MockBean
    private TemplateService templateService;


    @BeforeAll
    static void setUp() {
        TPL_COLL_MANTELFORM = getTestTemplate(
            "/services/templates/01-06_Gesetz_Mantelform_Entwurf/01-06_instanz_01/01-06_instanz_01.json"
        );
        TPL_COLL_STAMMFORM = getTestTemplate(
            "/services/templates/01-01_Gesetz_Stammform_Entwurf/01-01_instanz_01/01-01_instanz_01.json"
        );
        testProposition = Proposition.builder()
            .createdAt(Instant.now())
            .initiant(RechtsetzungsdokumentInitiant.BUNDESREGIERUNG)
            .proponentDesignationNominative("Bundesregierung")
            .build();
    }


    @BeforeEach
    void init() {
        docTypes = new TreeMap<>();
        assertFalse(Utils.isMissing(TPL_COLL_MANTELFORM));
        assertFalse(Utils.isMissing(TPL_COLL_STAMMFORM));
        when(templateService.loadTemplate(DocumentType.RECHTSETZUNGSDOKUMENT_STAMMGESETZ, 0)).
            thenReturn(TPL_COLL_STAMMFORM);
        when(templateService.loadTemplate(DocumentType.RECHTSETZUNGSDOKUMENT_MANTELGESETZ, 0)).
            thenReturn(TPL_COLL_MANTELFORM);
    }


    @Test
    void test_getCollectionDocumentContent_NoDocs() {
        String xmlContent = collectionDocumentBuilder.getCollectionDocumentContent(docTypes, testProposition,
            CompoundDocumentTypeVariant.STAMMGESETZ, null);
        assertNotNull(xmlContent);
        assertFalse(xmlContent.isBlank());

        RechtsetzungsdokumentHauptteil hauptteil = getRechtsetzungsdokumentHauptteil(xmlContent);
        checkRechtsetzungsdokumentHauptteil(hauptteil,
            VerweisMerkmal.builder().href("").showAsLiteral(TeildokumentVerweisShowAsLiteral.VORBLATT).build(),
            VerweisMerkmal.builder().href("").showAsLiteral(TeildokumentVerweisShowAsLiteral.REGELUNGSTEXT).build(),
            VerweisMerkmal.builder().href("").showAsLiteral(TeildokumentVerweisShowAsLiteral.BEGRUENDUNG).build()
        );
    }


    @Test
    void test_getCollectionDocumentContent_onlyVorblatt() {
        String filename = "vorblatt.xml";
        DocumentType documentType = DocumentType.VORBLATT_STAMMGESETZ;
        docTypes.put(filename, documentType);

        String xmlContent = collectionDocumentBuilder.getCollectionDocumentContent(docTypes, null,
            CompoundDocumentTypeVariant.STAMMGESETZ, null);
        assertNotNull(xmlContent);
        assertFalse(xmlContent.isBlank());

        RechtsetzungsdokumentHauptteil hauptteil = getRechtsetzungsdokumentHauptteil(xmlContent);
        checkRechtsetzungsdokumentHauptteil(hauptteil,
            VerweisMerkmal.builder().href(filename).showAsLiteral(TeildokumentVerweisShowAsLiteral.VORBLATT)
                .build(),
            VerweisMerkmal.builder().href("").showAsLiteral(TeildokumentVerweisShowAsLiteral.REGELUNGSTEXT).build(),
            VerweisMerkmal.builder().href("").showAsLiteral(TeildokumentVerweisShowAsLiteral.BEGRUENDUNG).build()
        );
    }


    @Test
    void test_getCollectionDocumentContent_OnlyRegelungstext() {
        String filename = "2022-05-05_RV_Regelungstext.xml";
        DocumentType documentType = DocumentType.REGELUNGSTEXT_STAMMGESETZ;
        docTypes.put(filename, documentType);

        String xmlContent = collectionDocumentBuilder.getCollectionDocumentContent(docTypes, null,
            CompoundDocumentTypeVariant.STAMMGESETZ, null);
        assertNotNull(xmlContent);
        assertFalse(xmlContent.isBlank());

        RechtsetzungsdokumentHauptteil hauptteil = getRechtsetzungsdokumentHauptteil(xmlContent);
        checkRechtsetzungsdokumentHauptteil(hauptteil,
            VerweisMerkmal.builder().href("").showAsLiteral(TeildokumentVerweisShowAsLiteral.VORBLATT).build(),
            VerweisMerkmal.builder().href(filename).showAsLiteral(TeildokumentVerweisShowAsLiteral.REGELUNGSTEXT)
                .build(),
            VerweisMerkmal.builder().href("").showAsLiteral(TeildokumentVerweisShowAsLiteral.BEGRUENDUNG).build()
        );
    }


    @Test
    void test_getCollectionDocumentContent_AllMandatory_AndAnschreiben() {
        String vorblatt = "dm1-vorblatt.xml";
        docTypes.put(vorblatt, DocumentType.VORBLATT_STAMMGESETZ);
        String regelungstext = "dm1-regelungstext.xml";
        docTypes.put(regelungstext, DocumentType.REGELUNGSTEXT_STAMMGESETZ);
        String begruendung = "dm1-begründung.xml";
        docTypes.put(begruendung, DocumentType.BEGRUENDUNG_STAMMGESETZ);
        String anschreiben = "dm1-anschreiben.xml";
        docTypes.put(anschreiben, DocumentType.ANSCHREIBEN_STAMMGESETZ);

        String xmlContent = collectionDocumentBuilder.getCollectionDocumentContent(docTypes, testProposition,
            CompoundDocumentTypeVariant.STAMMGESETZ, null);
        assertNotNull(xmlContent);
        assertFalse(xmlContent.isBlank());

        RechtsetzungsdokumentHauptteil hauptteil = getRechtsetzungsdokumentHauptteil(xmlContent);
        checkRechtsetzungsdokumentHauptteil(hauptteil,
            VerweisMerkmal.builder().href(vorblatt).showAsLiteral(TeildokumentVerweisShowAsLiteral.VORBLATT)
                .build(),
            VerweisMerkmal.builder().href(regelungstext)
                .showAsLiteral(TeildokumentVerweisShowAsLiteral.REGELUNGSTEXT).build(),
            VerweisMerkmal.builder().href(begruendung).showAsLiteral(TeildokumentVerweisShowAsLiteral.BEGRUENDUNG)
                .build(),
            VerweisMerkmal.builder().href(anschreiben).showAsLiteral(TeildokumentVerweisShowAsLiteral.ANSCHREIBEN)
                .build()
        );
    }


    @Test
    void test_getCollectionDocumentContent_NoRegelungstext_WithAnschreiben_AndTwoAnlage() {
        String vorblatt = "dm1-vorblatt.xml";
        docTypes.put(vorblatt, DocumentType.VORBLATT_STAMMGESETZ);
        String begruendung = "dm1-begründung.xml";
        docTypes.put(begruendung, DocumentType.BEGRUENDUNG_STAMMGESETZ);
        String anschreiben = "dm1-anschreiben.xml";
        docTypes.put(anschreiben, DocumentType.ANSCHREIBEN_STAMMGESETZ);
        String anlage1 = "dm1-stellungsnahme.xml";
        docTypes.put(anlage1, DocumentType.ANLAGE);
        String anlage2 = "anmerkungen.xml";
        docTypes.put(anlage2, DocumentType.ANLAGE);

        String xmlContent = collectionDocumentBuilder.getCollectionDocumentContent(docTypes, null,
            CompoundDocumentTypeVariant.STAMMGESETZ, null);
        assertNotNull(xmlContent);
        assertFalse(xmlContent.isBlank());

        RechtsetzungsdokumentHauptteil hauptteil = getRechtsetzungsdokumentHauptteil(xmlContent);
        // order of anlage1 and anlage2 depends on string compare of filenames
        checkRechtsetzungsdokumentHauptteil(hauptteil,
            VerweisMerkmal.builder().href(vorblatt).showAsLiteral(TeildokumentVerweisShowAsLiteral.VORBLATT)
                .build(),
            VerweisMerkmal.builder().href("").showAsLiteral(TeildokumentVerweisShowAsLiteral.REGELUNGSTEXT).build(),
            VerweisMerkmal.builder().href(begruendung).showAsLiteral(TeildokumentVerweisShowAsLiteral.BEGRUENDUNG)
                .build(),
            VerweisMerkmal.builder().href(anschreiben).showAsLiteral(TeildokumentVerweisShowAsLiteral.ANSCHREIBEN)
                .build(),
            VerweisMerkmal.builder().href(anlage2).showAsLiteral(TeildokumentVerweisShowAsLiteral.OFFENE_STRUKTUR)
                .build(),
            VerweisMerkmal.builder().href(anlage1).showAsLiteral(TeildokumentVerweisShowAsLiteral.OFFENE_STRUKTUR)
                .build()
        );
    }


    @SneakyThrows
    private RechtsetzungsdokumentHauptteil getRechtsetzungsdokumentHauptteil(@NonNull String xmlString) {
        String jsonString = xmlToJsonTransformer.transform(xmlString);
        assertNotNull(jsonString);
        assertFalse(jsonString.isBlank());

        JSONArray root = (JSONArray) new JSONParser().parse(jsonString);
        assertNotNull(root);

        JSONObject document = (JSONObject) root.get(0);
        assertNotNull(document);
        JSONArray children = (JSONArray) document.get(Constants.JSON_KEY_CHILDREN);
        if (children == null || children.isEmpty()) {
            return null;
        }

        JSONObject documentCollection = (JSONObject) children.get(0);
        assertEquals("akn:documentCollection", documentCollection.get(Constants.JSON_KEY_TYPE));
        children = (JSONArray) documentCollection.get(Constants.JSON_KEY_CHILDREN);
        if (children == null || children.isEmpty()) {
            return null;
        }
        assertEquals(2, children.size());

        JSONObject collectionBody = (JSONObject) children.get(1);
        assertEquals("akn:collectionBody", collectionBody.get(Constants.JSON_KEY_TYPE));

        return RechtsetzungsdokumentHauptteil.createFromJSON(collectionBody);
    }


    private void checkRechtsetzungsdokumentHauptteil(RechtsetzungsdokumentHauptteil hauptteil,
        VerweisMerkmal... merkmale) {
        assertNotNull(hauptteil);
        assertNotNull(hauptteil.getVerweise());
        List<TeildokumentVerweisContainer> verweise = hauptteil.getVerweise();

        assertEquals(merkmale.length, verweise.size());

        for (int i = 0; i < merkmale.length; i++) {
            int j = i + 1;
            VerweisMerkmal merkmal = merkmale[i];
            TeildokumentVerweisContainer container = verweise.get(i);
            assertNotNull(container);
            assertNotNull(container.getTeildokumentVerweis());
            assertEquals(String.format(PTN_EID_CONTAINER, j), container.getEId().toString());
            assertNotNull(container.getGuid());

            TeildokumentVerweis verweis = container.getTeildokumentVerweis();
            assertEquals(String.format(PTN_EID_VERWEIS, j), verweis.getEId().toString());
            assertNotNull(verweis.getGuid());
            assertEquals(merkmal.href, verweis.getHref());
            assertEquals(merkmal.showAsLiteral, verweis.getShowAsLiteral());
        }
    }


    private static String getTestTemplate(String resourcePath) {
        String jsonContent = Utils.getContentOfTextResource(resourcePath);
        jsonContent = setGUIDsInTemplate(jsonContent);
        JSONObject documentObject = getDocumentObject(jsonContent);
        JSONObject bodyObject = getDescendant(documentObject, true, ELEM_DOCUMENTCOLLECTION, ELEM_COLLECTIONBODY);
        assertNotNull(bodyObject);
        clearChildrenArray(bodyObject);
        return wrapUp(documentObject).toJSONString();
    }


    @Builder
    private static class VerweisMerkmal {

        final String href;
        final TeildokumentVerweisShowAsLiteral showAsLiteral;
    }

}
