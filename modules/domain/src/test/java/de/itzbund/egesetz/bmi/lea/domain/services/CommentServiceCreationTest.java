// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.core.json.JSONUtils;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CommentPositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CreateCommentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class CommentServiceCreationTest extends CommentServiceTest {


	@Test
	void testSaveComment_EverythingCorrect() {
		when(commentPersistencePort.save(any())).then(returnsFirstArg());
		when(dokumentPersistencePort.save(any())).then(returnsFirstArg());

		JSONObject baseDocument = getDocumentObject("/change/ozg-bestand.json");
		DocumentDomain randomDocument = TestObjectsUtil.getRandomDocumentEntity();
		String updatedContent = JSONUtils.wrapUp(baseDocument)
			.toJSONString();
		randomDocument.setContent(updatedContent);
		when(documentService.getDocumentEntityById(any())).thenReturn(randomDocument);
		when(documentService.getDocumentDTO(any(), any())).thenReturn(TestDTOUtil.getRandomDocumentDTO());
		when(documentService.buildDocumentDTO(any())).thenReturn(TestDTOUtil.getRandomDocumentDTO());
		when(documentService.enrichDocumentDTOWithPermissions(any())).thenReturn(TestDTOUtil.getRandomDocumentDTO());

		String content = "test";
		CreateCommentDTO commentDto = TestDTOUtil.getRandomCreateCommentDTO();
		commentDto.setContent(content);
		commentDto.setAnchor(CommentPositionDTO.builder().offset(5).elementGuid("e93b3ee5-7456-49d8-a3ec-b78291327eb1").build());
		commentDto.setFocus(CommentPositionDTO.builder().offset(5).elementGuid("e93b3ee5-7456-49d8-a3ec-b78291327eb1").build());

		List<ServiceState> status = new ArrayList<>();
		DocumentDTO response =
			commentService.createComment(new DocumentId(uuid), commentDto, status);

		assertThat(status.get(0)).isEqualTo(ServiceState.OK);
		assertNotNull(response);

	}

	private JSONObject getDocumentObject(String path) {
		String content = Utils.getContentOfTextResource(path);
		assertNotNull(content);
		assertFalse(content.isBlank());
		JSONObject resultObject = JSONUtils.getDocumentObject(content);
		assertNotNull(resultObject);
		return resultObject;
	}


	@Test
	void testSaveCommentForUnknownDocument_ThenDocumentNotFoundShouldBeReturned() {
		// Arrange
		when(documentService.isDocumentExisting(any(Document.class))).thenReturn(false);

		List<ServiceState> status = new ArrayList<>();
		CreateCommentDTO commentDto = TestDTOUtil.getRandomCreateCommentDTO();
		DocumentDTO document = commentService.createComment(new DocumentId(uuid), commentDto, status);

		assertThat(status.get(0)).isEqualTo(ServiceState.DOCUMENT_NOT_FOUND);
		assertThat(document).isNull();
	}

}
