// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.aggregate;

import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.services.CommentService;
import de.itzbund.egesetz.bmi.lea.domain.services.RBACPermissionService;
import de.itzbund.egesetz.bmi.lea.domain.services.RegelungsvorhabenService;
import de.itzbund.egesetz.bmi.lea.domain.services.meta.MetadatenUpdateService;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith({SpringExtension.class}) // JUnit 5
@SpringBootTest(classes = {CompoundDocument.class, Document.class})
@SuppressWarnings("unused")
abstract class CompoundDocumentIntegrationDomain {

	protected static final User USER = TestObjectsUtil.getUserEntity();
	protected CompoundDocumentDomain compoundDocumentEntity = TestObjectsUtil.getCompoundDocumentEntity();

	@Autowired
	protected CompoundDocument compoundDocument;
	@MockBean
	protected DokumentPersistencePort documentRepository;
	@MockBean
	protected CompoundDocumentPersistencePort compoundDocumentRepository;
	@MockBean
	private RegelungsvorhabenService regelungsvorhabenService;
	@MockBean
	private MetadatenUpdateService metadatenUpdateService;
	@MockBean
	private RBACPermissionService rbacPermissionService;
	@MockBean
	private CommentService commentService;


	@BeforeEach
	public void setUp() {
		compoundDocumentRepositoryMocks();
		documentRepositoryMocks();
	}


	private void compoundDocumentRepositoryMocks() {
		Mockito.when(compoundDocumentRepository.save(any(CompoundDocumentDomain.class)))
			.thenAnswer(invocation ->
				{
					CompoundDocumentDomain compoundDocumentEntity = invocation.getArgument(0);
					compoundDocumentEntity.setCreatedAt(Instant.now());
					compoundDocumentEntity.setUpdatedAt(Instant.now());
					return compoundDocumentEntity;
				}
			);

		Mockito.when(compoundDocumentRepository.findByCreatedBy(USER)).thenReturn(
			Arrays.asList(
				compoundDocumentEntity,
				TestObjectsUtil.getCompoundDocumentEntity()
			));

		Mockito.when(compoundDocumentRepository.findByCreatedByAndRegelungsvorhabenIdAndState(any(), any(), any()))
			.thenReturn(Collections.singletonList(compoundDocumentEntity));
	}


	private void documentRepositoryMocks() {
		Mockito.when(documentRepository.save(any(DocumentDomain.class)))
			.thenAnswer(invocation ->
				{
					DocumentDomain documentEntity = invocation.getArgument(0);
					documentEntity.setCreatedAt(Instant.now());
					documentEntity.setUpdatedAt(Instant.now());
					return documentEntity;
				}
			);

		Mockito.when(documentRepository.findByCompoundDocumentId(any(CompoundDocumentId.class))).thenReturn(
			Optional.of(
				Arrays.asList(
					TestObjectsUtil.getRandomDocumentEntity(),
					TestObjectsUtil.getRandomDocumentEntity()
				)));

	}


	protected boolean addDocumentToCompoundDocument(Document document) {
		if (compoundDocument.getCompoundDocumentEntity() == null) {
			compoundDocument.setCompoundDocumentEntity(TestObjectsUtil.getCompoundDocumentEntity());
		}

		CompoundDocumentId compoundDocumentId = compoundDocument.getCompoundDocumentId();

		if (compoundDocumentId != null) {
			return compoundDocument.addDocumentToCompoundDocument(document, compoundDocument);
		}
		return false;
	}

}
