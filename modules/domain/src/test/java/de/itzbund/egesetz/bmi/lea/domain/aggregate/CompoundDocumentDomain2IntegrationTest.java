// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.aggregate;

import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class CompoundDocumentDomain2IntegrationTest extends CompoundDocumentIntegrationDomain {

	private final Document document1 = TestObjectsUtil.getRandomDocument();
	private final Document document2 = TestObjectsUtil.getRandomDocument();


	@Test
	void whenCompoundDocumentShouldBeSaved_thenItShouldBePersisted() {
		// Arrange

		// Act
		CompoundDocument actual = compoundDocument.createOrUpdate(null);

		// Assert
		assertThat(actual).isSameAs(compoundDocument);
		assertThat(compoundDocument)
			.hasFieldOrProperty("compoundDocumentId").isNotNull()
			.hasFieldOrPropertyWithValue("documents", Collections.emptyList());
	}


	@Test
	void whenADocumentIsAddedToExistingCompoundDocument_thenItShouldBePersistedAndBeAvailableInAggregate() {
		// Arrange

		document1.setDocumentRepository(documentRepository);
		document2.setDocumentRepository(documentRepository);
		List<Document> documentList = List.of(document1, document2);

		CompoundDocument expectedCompoundDocument = TestObjectsUtil.getRandomCompoundDocument();
		expectedCompoundDocument.setCompoundDocumentRepository(compoundDocumentRepository);
		compoundDocument.setCompoundDocumentRepository(compoundDocumentRepository);

		expectedCompoundDocument.setDocuments(documentList);

		Mockito.when(documentRepository.save(Mockito.any(DocumentDomain.class)))
			.thenReturn(TestObjectsUtil.getDocumentEntity());

		// Act
		addDocumentToCompoundDocument(document1);
		boolean actual = addDocumentToCompoundDocument(document2);

		// Assert
		assertThat(actual).isTrue();
		assertThat(expectedCompoundDocument)
			.hasFieldOrProperty("documents").isNotNull();

		List<Document> documents = expectedCompoundDocument.getDocuments();
		assertThat(documents).isNotEmpty().hasSize(2);

		Document actualDocument = documents.get(0);
		assertThat(actualDocument).isEqualTo(document1);
	}


	@Test
	void whenAllCompoundDocumentForAValidUserIsRequested_thenTheyShouldBeProvided() {
		// Arrange

		// Act
		List<CompoundDocument> actual = compoundDocument.getCompoundDocumentsOptionalFilteredByUserUndRegelungsvorhaben(
			USER,
			List.of(TestObjectsUtil.REGELUNGSVORHABEN_ID));

		// Assert
		assertThat(actual).isNotEmpty().hasSize(2);
	}


	@Test
	void whenDocumentsForCompoundDocumentAreRequested_thenTheyShouldBeProvided() {
		// Arrange

		// Act
		List<Document> actual = compoundDocument.getDocumentsFromCompoundDocument(
			new CompoundDocumentId(UUID.randomUUID()), null);

		//Assert
		assertThat(actual).isNotEmpty().hasSize(2);
	}


}