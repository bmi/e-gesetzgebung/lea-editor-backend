// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.EditorRollenUndRechte;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.mappers.BaseMapper;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CompoundDocumentPermissionRestPort;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static de.itzbund.egesetz.bmi.lea.core.Constants.RECHT_LESEN;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(classes = {CompoundDocumentPermissionsService.class, EditorRollenUndRechte.class
})
@SuppressWarnings("unused")
class CompoundDocumentPermissionsServiceTest {

    @Autowired
    protected CompoundDocumentPermissionRestPort compoundDocumentPermissionsService;

    @MockBean
    protected EditorRollenUndRechte editorRollenUndRechte;

    @MockBean
    protected UserService userService;

    @MockBean
    protected BaseMapper baseMapper;


    @Test
    void testGetPermissionsByRessourceIdAndAktion() {
        List<ServiceState> status = new ArrayList<>();
        DocumentDTO documentDTO = TestDTOUtil.getRandomDocumentDTO();
        for (int i = 0; i < 7; i++) {
            UserDTO user = TestDTOUtil.getRandomUserDTO();
        }

        //Act
        List<UserDTO> users = compoundDocumentPermissionsService.getPermissionsByRessourceIdAndAktion(
            documentDTO.getId(), RECHT_LESEN, status);

        //Assert
        assertEquals(ServiceState.OK, status.get(0));
        assertNotNull(users);
    }
}