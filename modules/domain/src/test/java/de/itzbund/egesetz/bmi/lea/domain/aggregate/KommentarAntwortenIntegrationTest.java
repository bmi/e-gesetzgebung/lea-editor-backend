// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.aggregate;

import de.itzbund.egesetz.bmi.lea.domain.entities.Kommentar;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.ReplyId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.ReplyPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {KommentarAntworten.class})
class KommentarAntwortenIntegrationTest {

    private static final String NEW_REPLY_CONTENT = "New Reply Content";
    private static final DocumentId documentId = new DocumentId(UUID.randomUUID());
    private static final CommentId commentId = new CommentId(UUID.randomUUID());
    private static final ReplyId replyid = new ReplyId(UUID.randomUUID());
    private static final ReplyId parentReplyId = new ReplyId(UUID.randomUUID());
    private final Kommentar kommentar = Kommentar.builder()
        .commentId(commentId)
        .documentId(documentId)
        .replyId(replyid)
        .build();
    private final Kommentar vorgaengerKommentar = Kommentar.builder()
        .commentId(commentId)
        .documentId(documentId)
        .replyId(parentReplyId)
        .build();
    @Autowired
    protected KommentarAntworten kommentarAntworten;
    // ----- Repositories -----
    @MockBean
    protected ReplyPersistencePort replyPersistencePort;
    // ----- Session ----------
    @MockBean
    protected LeageSession session;
    List<Reply> replies = TestObjectsUtil.getAConnectedReplyListOfSize(4);

    // Beim Loeschen haben wir zwei Speicheroperationen
    List<Reply> storedReply = null;


    @BeforeEach
    public void setUp() {
        storedReply = new ArrayList<>();
        when(replyPersistencePort.findByReplyId(any(ReplyId.class)))
            .thenAnswer(invocation ->
                {
                    ReplyId replyId = invocation.getArgument(0);
                    Optional<Reply> answer = Optional.empty();

                    for (Reply reply : replies) {
                        if (reply.getReplyId()
                            .equals(replyId)) {
                            answer = Optional.of(reply);
                        }
                    }

                    return answer;
                }
            );

        when(replyPersistencePort.findByParentIdAndDeletedFalse(any(String.class)))
            .thenAnswer(invocation ->
                {
                    String replyId = invocation.getArgument(0);
                    Optional<Reply> answer = Optional.empty();

                    for (Reply reply : replies) {
                        if (reply.getParentId()
                            .equals(replyId)) {
                            answer = Optional.of(reply);
                        }
                    }

                    return answer;
                }
            );

        // replyPersistencePort.save -> reflects status
        when(replyPersistencePort.save(any(Reply.class))).thenAnswer(invocation -> {

            Reply changedReply = invocation.getArgument(0);

            for (int i = 0; i < replies.size(); i++) {
                if (replies.get(i)
                    .getReplyId()
                    .equals(changedReply.getReplyId())) {
                    replies.remove(i);
                    replies.add(i, changedReply);
                }
            }

            // To do assert
            storedReply.add(changedReply);
            return changedReply;
        });

        when(session.getUser()).thenReturn(TestObjectsUtil.getUserEntity());
    }


    @Test
    void testAntwortErstellenOhneVorgaenger() {
        // Act
        boolean actual = kommentarAntworten.antwortAufKommentarErstellen(kommentar, null, replies.get(0));

        // Assert
        assertThat(actual).isTrue();

        Reply actualR = storedReply.get(0);
        assertThat(actualR).hasNoNullFieldsOrProperties();
        assertThat(actualR).hasFieldOrPropertyWithValue("parentId", commentId.getId()
            .toString());
    }


    @Test
    void testAntwortErstellenMitVorgaenger() {
        // Act
        boolean actual = kommentarAntworten.antwortAufKommentarErstellen(kommentar, vorgaengerKommentar,
            replies.get(1));

        // Assert
        assertThat(actual).isTrue();

        Reply actualR = storedReply.get(0);
        assertThat(actualR).hasNoNullFieldsOrProperties();
        assertThat(actualR).hasFieldOrPropertyWithValue("parentId", parentReplyId.getId()
            .toString());
    }


    @Test
    void testKommentarAntwortAendern() {
        // Arrange
        Reply veraenderteAntwort = replies.get(1);
        veraenderteAntwort.setContent(NEW_REPLY_CONTENT);

        // Act
        boolean actual = kommentarAntworten.kommentarAntwortAendern(replies.get(0), veraenderteAntwort);

        // Assert
        assertThat(actual).isTrue();

        Reply actualR = storedReply.get(0);
        assertThat(actualR).hasNoNullFieldsOrProperties();
        assertThat(actualR).hasFieldOrPropertyWithValue("content", NEW_REPLY_CONTENT);

        int timeCompare = actualR.getCreatedAt()
            .compareTo(actualR.getUpdatedAt());
        assertThat(timeCompare).isNotPositive();
    }


    @Test
    void testKommentarAntwortLoeschen() {
        // Arrange
        Reply zuLoeschendeAntwort = replies.get(1);

        // Act
        boolean actual = kommentarAntworten.kommentarAntwortLoeschen(zuLoeschendeAntwort);

        // Assert
        assertThat(actual).isTrue();

        Reply actualR = storedReply.get(0);
        assertThat(actualR).hasFieldOrPropertyWithValue("deleted", true);
    }


    @Test
    void testReplyDeletionAdjustment() {
        // Act
        // - delete last -> save not called -> list empty
        kommentarAntworten.adjustPointers(replies.get(3));

        // - delete ONE somewhere in between -> comentId NOT equals parentId
        kommentarAntworten.adjustPointers(replies.get(1));

        // - delete a SECOND somewhere in between
        kommentarAntworten.adjustPointers(replies.get(2));

        // - delete first -> comentId equals parentId
        kommentarAntworten.adjustPointers(replies.get(0));
        Reply actual = replies.get(3);
        assertThat(actual.getParentId()).isEqualTo(actual.getCommentId()
            .getId()
            .toString());
    }

}
