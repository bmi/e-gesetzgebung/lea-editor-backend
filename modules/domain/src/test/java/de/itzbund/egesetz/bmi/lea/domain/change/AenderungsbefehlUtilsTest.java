// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.change;

import org.junit.jupiter.api.Test;

import static de.itzbund.egesetz.bmi.lea.domain.change.AenderungsbefehlUtils.getAenderungsstelle;
import static de.itzbund.egesetz.bmi.lea.domain.change.AenderungsbefehlUtils.getListPointNumValue;
import static org.junit.jupiter.api.Assertions.assertEquals;

class AenderungsbefehlUtilsTest {

    @Test
    void testGetListPointNumValue() {
        assertEquals("a", getListPointNumValue(1, AenderungsbefehlUtils.Gliederungsebene.BUCHSTABE));
        assertEquals("z", getListPointNumValue(26, AenderungsbefehlUtils.Gliederungsebene.BUCHSTABE));
        assertEquals("`", getListPointNumValue(0, AenderungsbefehlUtils.Gliederungsebene.BUCHSTABE));
        assertEquals("a", getListPointNumValue(27, AenderungsbefehlUtils.Gliederungsebene.BUCHSTABE));

        assertEquals("bb", getListPointNumValue(2, AenderungsbefehlUtils.Gliederungsebene.DOPPELBUCHSTABE));
        assertEquals("yy", getListPointNumValue(25, AenderungsbefehlUtils.Gliederungsebene.DOPPELBUCHSTABE));
        assertEquals("``", getListPointNumValue(0, AenderungsbefehlUtils.Gliederungsebene.DOPPELBUCHSTABE));
        assertEquals("bb", getListPointNumValue(28, AenderungsbefehlUtils.Gliederungsebene.DOPPELBUCHSTABE));

        assertEquals("ccc", getListPointNumValue(3, AenderungsbefehlUtils.Gliederungsebene.DREIFACHBUCHSTABE));
        assertEquals("xxx", getListPointNumValue(24, AenderungsbefehlUtils.Gliederungsebene.DREIFACHBUCHSTABE));
        assertEquals("```", getListPointNumValue(0, AenderungsbefehlUtils.Gliederungsebene.DREIFACHBUCHSTABE));
        assertEquals("ccc", getListPointNumValue(29, AenderungsbefehlUtils.Gliederungsebene.DREIFACHBUCHSTABE));
    }


    @Test
    void testGetAenderungsstelle() {
        assertEquals("", getAenderungsstelle(null));
        assertEquals("", getAenderungsstelle(""));
        assertEquals("", getAenderungsstelle(" "));
        assertEquals("", getAenderungsstelle(" \t"));

        assertEquals("Abschnitt 1", getAenderungsstelle("hauptteil-1_abschnitt-1"));
        assertEquals("§ 3", getAenderungsstelle("hauptteil-1_abschnitt-2_para-3"));
        assertEquals("Absatz 2", getAenderungsstelle("hauptteil-1_abschnitt-2_para-7_abs-2"));
        assertEquals("Nummer 1", getAenderungsstelle("hauptteil-1_abschnitt-2_para-7_abs-1_untergl-2_listenelem-1"));
        assertEquals("Absatz 3", getAenderungsstelle("hauptteil-1_abschnitt-2_para-8_abs-3"));
        assertEquals("Buchstabe b", getAenderungsstelle("hauptteil-1_abschnitt-2_para-4_abs-2_untergl-1_listenelem-1_untergl-1_listenelem-b"));
        assertEquals("Artikel 1", getAenderungsstelle("hauptteil-1_art-1"));
        assertEquals("§ 1", getAenderungsstelle("hauptteil-1_abschnitt-1_uabschnitt-1_para-1"));
        assertEquals("Absatz 1", getAenderungsstelle("hauptteil-1_abschnitt-1_uabschnitt-1_para-2_abs-1"));
        assertEquals("Buchstabe a", getAenderungsstelle("hauptteil-1_abschnitt-1_uabschnitt-1_para-2_abs-1_untergl-1_listenelem-1_untergl-1_listenelem-a"));
        assertEquals("Nummer 1a", getAenderungsstelle("hauptteil-1_abschnitt-1_uabschnitt-1_para-2_abs-1_untergl-1_listenelem-1a"));
        assertEquals("Buchstabe b", getAenderungsstelle("hauptteil-1_abschnitt-1_uabschnitt-1_para-2_abs-1_untergl-1_listenelem-1a_untergl-1_listenelem-b"));
        assertEquals("Teil 1", getAenderungsstelle("hauptteil-1_teil-1"));
        assertEquals("Kapitel 1b", getAenderungsstelle("hauptteil-1_teil-2_kapitel-1b"));
        assertEquals("§ 1", getAenderungsstelle("hauptteil-1_teil-1_kapitel-1_para-1"));
        assertEquals("Nummer 2", getAenderungsstelle("hauptteil-1_teil-2_kapitel-1_abschnitt-1_para-23_abs-1_untergl-1_listenelem-2"));
        assertEquals("§ 52", getAenderungsstelle("hauptteil-1_teil-3_kapitel-2_para-52"));
        assertEquals("Nummer 5", getAenderungsstelle("hauptteil-1_teil-3_kapitel-4_para-70_abs-1_untergl-1_listenelem-5"));
        assertEquals("§ 17", getAenderungsstelle("hauptteil-1_teil-1_kapitel-5_para-17"));
        assertEquals("Kapitel 4a", getAenderungsstelle("hauptteil-1_teil-1_kapitel-4a"));
        assertEquals("Absatz 4", getAenderungsstelle("hauptteil-1_teil-1_kapitel-4_para-16_abs-4"));
        assertEquals("Nummer 7", getAenderungsstelle("hauptteil-1_teil-1_kapitel-4_para-14_abs-1_untergl-1_listenelem-7"));
    }

}
