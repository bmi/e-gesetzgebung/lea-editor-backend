// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.model.dokument;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.model.version.LegalDocMLDeVersion;
import de.itzbund.egesetz.bmi.lea.domain.ports.eid.EId;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.BezeichnerImpl;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.EIdImpl;
import lombok.SneakyThrows;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class RechtsetzungsdokumentHauptteilTest {

    @Test
    @SneakyThrows
    void testSerializeTeildokumentVerweis() {
        UUID guid = UUID.randomUUID();
        TeildokumentVerweis teildokumentVerweis = new TeildokumentVerweis(guid);
        JSONAssert.assertEquals(String.format(
                "{"
                    + "    \"type\": \"akn:documentRef\","
                    + "    \"GUID\": \"%s\","
                    + "    \"href\": \"\""
                    + "}", guid),
            teildokumentVerweis.toJSONObject().toJSONString(),
            true);

        EId eId = EIdImpl.fromString("tldokverweis-1_verweis-1");
        assertNotNull(eId);
        teildokumentVerweis.setEId(eId);

        String href = "regelungstext.xml";
        teildokumentVerweis.setHref(href);

        TeildokumentVerweisShowAsLiteral literal = TeildokumentVerweisShowAsLiteral.REGELUNGSTEXT;
        teildokumentVerweis.setShowAsLiteral(literal);

        JSONAssert.assertEquals(String.format(
                "{"
                    + "    \"type\": \"akn:documentRef\","
                    + "    \"GUID\": \"%s\","
                    + "    \"eId\": \"%s\","
                    + "    \"href\": \"%s\","
                    + "    \"showAs\": \"%s\""
                    + "}", guid, eId, href, literal.getLiteral()),
            teildokumentVerweis.toJSONObject().toJSONString(),
            true);
    }


    @Test
    @SneakyThrows
    void testDeserializeTeildokumentVerweis() {
        UUID guid = UUID.randomUUID();
        EId eId = EIdImpl.fromString("tldokverweis-3_verweis-1");
        assertNotNull(eId);
        String href = "";
        TeildokumentVerweisShowAsLiteral literal = TeildokumentVerweisShowAsLiteral.ANSCHREIBEN;

        JSONObject jsonObject = (JSONObject) new JSONParser().parse(String.format(
            "{"
                + "    \"type\": \"akn:documentRef\","
                + "    \"GUID\": \"%s\","
                + "    \"eId\": \"%s\","
                + "    \"href\": \"%s\","
                + "    \"showAs\": \"%s\""
                + "}", guid, eId, href, literal.getLiteral()));

        TeildokumentVerweis teildokumentVerweis = TeildokumentVerweis.createFromJSON(jsonObject);
        assertNotNull(teildokumentVerweis);

        assertEquals(ModellElementTyp.TEILDOKUMENT_VERWEIS, teildokumentVerweis.getTyp());
        assertEquals(LegalDocMLDeVersion.LDML_DE_V1_04, teildokumentVerweis.supportedVersions.get(0));
        assertEquals(guid, teildokumentVerweis.getGuid());
        assertEquals(eId, teildokumentVerweis.getEId());
        assertEquals(href, teildokumentVerweis.getHref());
        assertEquals(literal, teildokumentVerweis.getShowAsLiteral());
    }


    @Test
    @SneakyThrows
    void testDeserializeTeildokumentVerweisContainer() {
        UUID guidContainer = UUID.randomUUID();
        UUID guidVerweis = UUID.randomUUID();
        EId eIdContainer = EIdImpl.fromString("tldokverweis-2");
        assertNotNull(eIdContainer);
        EId eIdVerweis = eIdContainer.extendBy(BezeichnerImpl.fromString("verweis-1"));
        assertNotNull(eIdVerweis);
        String href = Utils.getRandomString();
        TeildokumentVerweisShowAsLiteral literal = TeildokumentVerweisShowAsLiteral.VORBLATT;

        JSONObject jsonObject = (JSONObject) new JSONParser().parse(String.format(
            "{"
                + "    \"type\": \"akn:component\","
                + "    \"GUID\": \"%s\","
                + "    \"eId\": \"%s\","
                + "    \"children\": ["
                + "        {"
                + "            \"type\": \"akn:documentRef\","
                + "            \"GUID\": \"%s\","
                + "            \"eId\": \"%s\","
                + "            \"href\": \"%s\","
                + "            \"showAs\": \"%s\""
                + "        }"
                + "    ]"
                + "}", guidContainer, eIdContainer, guidVerweis, eIdVerweis, href, literal.getLiteral()));

        TeildokumentVerweisContainer container = TeildokumentVerweisContainer.createFromJSON(jsonObject);
        assertNotNull(container);

        assertEquals(ModellElementTyp.TEILDOKUMENT_VERWEIS_CONTAINER, container.getTyp());
        assertEquals(LegalDocMLDeVersion.LDML_DE_V1_04, container.supportedVersions.get(0));
        assertEquals(guidContainer, container.getGuid());
        assertEquals(eIdContainer, container.getEId());

        TeildokumentVerweis teildokumentVerweis = container.getTeildokumentVerweis();
        assertNotNull(teildokumentVerweis);
        assertEquals(ModellElementTyp.TEILDOKUMENT_VERWEIS, teildokumentVerweis.getTyp());
        assertEquals(LegalDocMLDeVersion.LDML_DE_V1_04, teildokumentVerweis.supportedVersions.get(0));
        assertEquals(guidVerweis, teildokumentVerweis.getGuid());
        assertEquals(eIdVerweis, teildokumentVerweis.getEId());
        assertEquals(href, teildokumentVerweis.getHref());
        assertEquals(literal, teildokumentVerweis.getShowAsLiteral());
    }


    @Test
    @SneakyThrows
    void testDeserializeRechtsetzungsdokumentHauptteil() {
        UUID guidContainer1 = UUID.randomUUID();
        UUID guidContainer2 = UUID.randomUUID();
        UUID guidVerweis1 = UUID.randomUUID();
        UUID guidVerweis2 = UUID.randomUUID();
        EId eIdContainer1 = EIdImpl.fromString("tldokverweis-1");
        assertNotNull(eIdContainer1);
        EId eIdContainer2 = EIdImpl.fromString("tldokverweis-2");
        assertNotNull(eIdContainer2);
        EId eIdVerweis1 = eIdContainer1.extendBy(BezeichnerImpl.fromString("verweis-1"));
        assertNotNull(eIdVerweis1);
        EId eIdVerweis2 = eIdContainer2.extendBy(BezeichnerImpl.fromString("verweis-1"));
        assertNotNull(eIdVerweis2);
        String href1 = Utils.getRandomString();
        String href2 = Utils.getRandomString();
        TeildokumentVerweisShowAsLiteral literal1 = TeildokumentVerweisShowAsLiteral.REGELUNGSTEXT;
        TeildokumentVerweisShowAsLiteral literal2 = TeildokumentVerweisShowAsLiteral.BEGRUENDUNG;

        JSONObject jsonObject = (JSONObject) new JSONParser().parse(String.format(
            "{"
                + "    \"type\": \"akn:collectionBody\","
                + "    \"children\": ["
                + "        {"
                + "            \"type\": \"akn:component\","
                + "            \"GUID\": \"%s\","
                + "            \"eId\": \"%s\","
                + "            \"children\": ["
                + "                {"
                + "                    \"type\": \"akn:documentRef\","
                + "                    \"GUID\": \"%s\","
                + "                    \"eId\": \"%s\","
                + "                    \"href\": \"%s\","
                + "                    \"showAs\": \"%s\""
                + "                }"
                + "            ]"
                + "        },"
                + "        {"
                + "            \"type\": \"akn:component\","
                + "            \"GUID\": \"%s\","
                + "            \"eId\": \"%s\","
                + "            \"children\": ["
                + "                {"
                + "                    \"type\": \"akn:documentRef\","
                + "                    \"GUID\": \"%s\","
                + "                    \"eId\": \"%s\","
                + "                    \"href\": \"%s\","
                + "                    \"showAs\": \"%s\""
                + "                }"
                + "            ]"
                + "        }"
                + "    ]"
                + "}",
            guidContainer1, eIdContainer1, guidVerweis1, eIdVerweis1, href1, literal1.getLiteral(),
            guidContainer2, eIdContainer2, guidVerweis2, eIdVerweis2, href2, literal2.getLiteral()));

        RechtsetzungsdokumentHauptteil hauptteil = RechtsetzungsdokumentHauptteil.createFromJSON(jsonObject);
        assertNotNull(hauptteil);

        assertEquals(ModellElementTyp.RECHTSETZUNGSDOKUMENT_HAUPTTEIL, hauptteil.getTyp());
        assertEquals(LegalDocMLDeVersion.LDML_DE_V1_04, hauptteil.supportedVersions.get(0));
        assertNotNull(hauptteil.getVerweise());
        assertEquals(2, hauptteil.getVerweise().size());
    }

}
