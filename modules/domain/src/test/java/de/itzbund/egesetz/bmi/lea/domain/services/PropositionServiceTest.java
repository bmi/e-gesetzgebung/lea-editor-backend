// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.EditorRollenUndRechte;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.DokumentenmappeTableDTOs;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.HomepageRegulatoryProposalDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.RegelungsvorhabengTableDTOs;
import de.itzbund.egesetz.bmi.lea.domain.dtos.paging.PagingUtil;
import de.itzbund.egesetz.bmi.lea.domain.dtos.paging.PaginierungDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RessortEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.PropositionState;
import de.itzbund.egesetz.bmi.lea.domain.enums.RegelungsvorhabenTypType;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import de.itzbund.egesetz.bmi.lea.domain.mappers.BaseMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.BestandsrechtPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DrucksacheDokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.PropositionPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {
	PropositionService.class, PropositionMapperImpl.class, BestandsrechtService.class
})
@SuppressWarnings("unused")
class PropositionServiceTest {

	@Autowired
	private PropositionService propositionService;
	@Autowired
	private PropositionMapper propositionMapper;
	@Autowired
	private BestandsrechtService bestandsrechtService;

	// Fuer den PropositionService
	@MockBean
	private CompoundDocumentService compoundDocumentService;
	@MockBean
	private DocumentService documentService;
	@MockBean
	private DrucksacheService drucksacheService;
	@MockBean
	private RBACPermissionService rbacPermissionService;
	@MockBean
	private RegelungsvorhabenService regelungsvorhabenService;
	@MockBean
	protected UserService userService;
	@MockBean
	private LeageSession session;

	// Fuer den BestandsrechtService
	@MockBean
	private BestandsrechtPersistencePort bestandsrechtPersistencePort;

	// Fuer den PropositionMapper
	@MockBean
	protected BaseMapper baseMapper;

	// Andere Abhaengigkeiten
	@MockBean
	private CompoundDocumentPersistencePort compoundDocumentPersistencePort;
	@MockBean
	private PropositionPersistencePort propositionPersistencePort;
	@MockBean
	private EditorRollenUndRechte editorRollenUndRechte;
	@MockBean
	private ConverterService converterService;
	@MockBean
	private DrucksacheDokumentPersistencePort drucksacheDokumentPersistencePort;

	private static final User user1 = User.builder()
		.gid(new UserId("user1"))
		.name(Utils.getRandomString())
		.build();
	private static final User user2 = User.builder()
		.gid(new UserId("user2"))
		.name(Utils.getRandomString())
		.build();
	private static final User user3 = User.builder()
		.gid(new UserId("user3"))
		.name(Utils.getRandomString())
		.build();

	private static final Proposition regelungsvorhaben = Proposition.builder()
		.propositionId(new RegelungsVorhabenId(UUID.randomUUID()))
		.title(Utils.getRandomString())
		.type(RechtsetzungsdokumentTyp.GESETZ)
		.state(PropositionState.IN_BEARBEITUNG)
		.proponentId(UUID.randomUUID().toString())
		.proponentTitle(Utils.getRandomString())
		.proponentDesignationNominative(Utils.getRandomString())
		.proponentDesignationGenitive(Utils.getRandomString())
		.proponentActive(true)
		.build();

	private CompoundDocumentDomain dokumentenMappe1;
	private CompoundDocumentDomain dokumentenMappe2;
	private CompoundDocumentDomain dokumentenMappe3;
	private CompoundDocumentDomain dokumentenMappe4;
	private CompoundDocumentDomain dokumentenMappe5;

	private static Stream<Arguments> provideTestInput() {
		return Stream.of(
			Arguments.of(user1, List.of(), 0, 2),
			Arguments.of(user2, List.of(RegelungsvorhabenEditorDTO.builder()
				.id(regelungsvorhaben.getPropositionId()
					.getId())
				.bezeichnung(regelungsvorhaben.getTitle())
				.vorhabenart(RegelungsvorhabenTypType.GESETZ)
				.status(VorhabenStatusType.IN_BEARBEITUNG)
				.technischesFfRessort(RessortEditorDTO.builder()
					.id(regelungsvorhaben.getProponentId())
					.kurzbezeichnung(regelungsvorhaben.getProponentTitle())
					.bezeichnungNominativ(regelungsvorhaben.getProponentDesignationNominative())
					.bezeichnungGenitiv(regelungsvorhaben.getProponentDesignationGenitive())
					.aktiv(regelungsvorhaben.isProponentActive())
					.build())
				.build()), 1, 0),
			Arguments.of(user3, List.of(), 0, 0),
			Arguments.of(user3, List.of(RegelungsvorhabenEditorDTO.builder()
				.id(regelungsvorhaben.getPropositionId()
					.getId())
				.bezeichnung(regelungsvorhaben.getTitle())
				.vorhabenart(RegelungsvorhabenTypType.GESETZ)
				.status(VorhabenStatusType.IN_BEARBEITUNG)
				.technischesFfRessort(RessortEditorDTO.builder()
					.id(regelungsvorhaben.getProponentId())
					.kurzbezeichnung(regelungsvorhaben.getProponentTitle())
					.bezeichnungNominativ(regelungsvorhaben.getProponentDesignationNominative())
					.bezeichnungGenitiv(regelungsvorhaben.getProponentDesignationGenitive())
					.aktiv(regelungsvorhaben.isProponentActive())
					.build())
				.build()), 1, 0)
		);
	}

	@ParameterizedTest(name = "#{index} - Test with {0}")
	@MethodSource("provideTestInput")
	void testGetStartseiteMeineDokumente_Benutzer(User user, List<RegelungsvorhabenEditorDTO> eigeneRegelungsvorhaben,
		int anzahlEigeneMappen, int anzahlMappenAusAbstimmungen) {
		when(session.getUser()).thenReturn(user);
		if (user.equals(user2)) {
			when(regelungsvorhabenService.getAllRegulatoryProposalsForUserHeIsAuthorOf()).thenReturn(
				eigeneRegelungsvorhaben);
		} else {
			when(regelungsvorhabenService.getAllRegulatoryProposalsForUserHeIsAuthorOf()).thenReturn(List.of());
		}

		List<CompoundDocumentDomain> meineDokumente = new ArrayList<>();
		when(compoundDocumentPersistencePort.findByCreatedBySortedByUpdatedAt(
			any(User.class), any(Pageable.class))).thenReturn(
			new PageImpl<>(Collections.emptyList(), PagingUtil.createDefaultPagingForMeineDokumente(), 0));
		when(rbacPermissionService.getDokumentenmappenIdsFuerNutzerAlsSchreiber(
			any())).thenReturn(eigeneRegelungsvorhaben.stream().map(x -> x.getId().toString()).collect(Collectors.toList()));
		when(editorRollenUndRechte.getRessortKurzbezeichnungForNutzer(any())).thenReturn("");

		CompoundDocument randomCompoundDocument = TestObjectsUtil.getRandomCompoundDocument();
		randomCompoundDocument.getCompoundDocumentEntity().setRegelungsVorhabenId(regelungsvorhaben.getPropositionId());

		List<ServiceState> status = new ArrayList<>();
		Optional<PaginierungDTO> paginierungDTO = Optional.empty();

		RegelungsvorhabengTableDTOs x = propositionService.getStartseiteMeineDokumente(status, paginierungDTO);

		List<HomepageRegulatoryProposalDTO> dokumente = x.getDtos().toList();
		assertEquals(ServiceState.OK, status.get(0));

		if (!dokumente.isEmpty()) {
			assertEquals(anzahlEigeneMappen, dokumente.get(0)
				.getChildren()
				.size());
		}
	}

	@ParameterizedTest(name = "#{index} - Test with {0}")
	@MethodSource("provideTestInput")
	void testGetStartseiteMeineDokumente_BT_Benutzer(User user, List<RegelungsvorhabenEditorDTO> eigeneRegelungsvorhaben,
		int anzahlEigeneMappen, int anzahlMappenAusAbstimmungen) {

		when(session.getUser()).thenReturn(user);
		when(regelungsvorhabenService.getRegulatoryProposalsByIdList(any())).thenReturn(
			eigeneRegelungsvorhaben);
		when(regelungsvorhabenService.getRegelungsvorhabenEditorDTOs(any())).thenReturn(
			eigeneRegelungsvorhaben);

		List<CompoundDocumentDomain> meineDokumente = new ArrayList<>();
		meineDokumente.add(dokumentenMappe1);
		when(compoundDocumentPersistencePort.findByStateSortedByRegelungsVorhabenId(
			any())).thenReturn(
			Collections.emptyList());
		when(rbacPermissionService.getDokumentenmappenIdsFuerNutzerAlsSchreiber(
			any())).thenReturn(eigeneRegelungsvorhaben.stream().map(x -> x.getId().toString()).collect(Collectors.toList()));
		when(editorRollenUndRechte.getRessortKurzbezeichnungForNutzer(any())).thenReturn("BT");
		when(compoundDocumentService.findByStateSortedByRegelungsVorhabenId(any())).thenReturn(meineDokumente);
		when(drucksacheDokumentPersistencePort.getDrucksacheDokument(any())).thenReturn(Optional.ofNullable(TestObjectsUtil.getDrucksacheDokument()));

		CompoundDocument randomCompoundDocument = TestObjectsUtil.getRandomCompoundDocument();
		randomCompoundDocument.getCompoundDocumentEntity().setRegelungsVorhabenId(regelungsvorhaben.getPropositionId());

		List<ServiceState> status = new ArrayList<>();
		Optional<PaginierungDTO> paginierungDTO = Optional.empty();

		RegelungsvorhabengTableDTOs x = propositionService.getStartseiteMeineDokumente(status, paginierungDTO);

		if (eigeneRegelungsvorhaben.isEmpty()) {
			assertEquals(ServiceState.REGELUNGSVORHABEN_NOT_FOUND, status.get(0));
		} else {
			assertEquals(ServiceState.OK, status.get(0));
		}


	}


	@ParameterizedTest(name = "#{index} - Test with {0}")
	@MethodSource("provideTestInput")
	void testGetStartseiteDokumenteAusAbstimmungen_Benutzer(User user,
		List<RegelungsvorhabenEditorDTO> eigeneRegelungsvorhaben,
		int anzahlEigeneMappen, int anzahlMappenAusAbstimmungen) {
		when(session.getUser()).thenReturn(user);
		if (user.equals(user2)) {
			when(regelungsvorhabenService.getAllRegulatoryProposalsForUserHeIsAuthorOf()).thenReturn(
				eigeneRegelungsvorhaben);
		} else {
			when(regelungsvorhabenService.getAllRegulatoryProposalsForUserHeIsAuthorOf()).thenReturn(List.of());
		}

		List<ServiceState> status = new ArrayList<>();
		List<HomepageRegulatoryProposalDTO> dokumente = propositionService.getStartseiteDokumenteAusAbstimmungen(
			status);
		assertEquals(ServiceState.OK, status.get(0));

		if (!dokumente.isEmpty()) {
			assertEquals(anzahlMappenAusAbstimmungen, dokumente.get(0)
				.getChildren()
				.size());
		}
	}


	@ParameterizedTest(name = "#{index} - Test with {0}")
	@MethodSource("provideTestInput")
	void testGetVersionshistorie_Benutzer(User user, List<RegelungsvorhabenEditorDTO> eigeneRegelungsvorhaben,
		int anzahlEigeneMappen, int anzahlMappenAusAbstimmungen) {
		when(session.getUser()).thenReturn(user);

		if (user.equals(user2)) {
			when(regelungsvorhabenService.getRegulatoryProposalsByIdList(any())).thenReturn(
				List.of(eigeneRegelungsvorhaben.get(0)));
		} else {
			when(regelungsvorhabenService.getRegulatoryProposalsByIdList(any())).thenReturn(Collections.emptyList());
		}
		when(compoundDocumentService.findByRegelungsvorhabenIdInSortedByUpdatedAt(any(), any(Pageable.class))).thenReturn(
			new PageImpl<>(List.of(dokumentenMappe1), PagingUtil.createDefaultPagingForMeineDokumente(), 0));

		List<ServiceState> status = new ArrayList<>();
		HomepageRegulatoryProposalDTO dokumente = null;

		DokumentenmappeTableDTOs pagedObject = propositionService.getVersionshistorie(status, UUID.randomUUID(), Optional.empty());
		if (pagedObject != null) {
			dokumente = pagedObject.getDtos().toList().get(0);
		}

		if (dokumente != null) {
			assertEquals(ServiceState.OK, status.get(0));
			assertEquals(anzahlEigeneMappen, dokumente.getChildren().size());
		} else if (!user.equals(user2)) {
			assertEquals(ServiceState.REGELUNGSVORHABEN_NOT_FOUND, status.get(0));
		}

	}

	@BeforeEach
	void init() {
		MockitoAnnotations.initMocks(this);

		RegelungsVorhabenId regelungsVorhabenId = regelungsvorhaben.getPropositionId();
		when(propositionPersistencePort.findByUpdateablePropositionId(regelungsvorhaben.getPropositionId())).
			thenReturn(regelungsvorhaben);

		Instant now = Instant.now();
		dokumentenMappe1 = erzeugeMappe(regelungsVorhabenId, user2, DocumentState.DRAFT,
			now.minus(5, ChronoUnit.DAYS), null);
		dokumentenMappe2 = erzeugeMappe(regelungsVorhabenId, user2, DocumentState.FREEZE,
			now.minus(4, ChronoUnit.DAYS), dokumentenMappe1.getCompoundDocumentId());
		dokumentenMappe3 = erzeugeMappe(regelungsVorhabenId, user1, DocumentState.DRAFT,
			now.minus(3, ChronoUnit.DAYS), dokumentenMappe2.getCompoundDocumentId());
		dokumentenMappe4 = erzeugeMappe(regelungsVorhabenId, user1, DocumentState.FINAL,
			now.minus(2, ChronoUnit.DAYS), dokumentenMappe3.getCompoundDocumentId());
		dokumentenMappe5 = erzeugeMappe(regelungsVorhabenId, user2, DocumentState.DRAFT,
			now.minus(1, ChronoUnit.DAYS), dokumentenMappe4.getCompoundDocumentId());

		List<CompoundDocumentDomain> alleMappen = List.of(
			dokumentenMappe1,
			dokumentenMappe2,
			dokumentenMappe3,
			dokumentenMappe4,
			dokumentenMappe5
		);

		initializeMappenRepository(user1, alleMappen);
		assertEquals(2, compoundDocumentPersistencePort.findByCreatedBy(user1)
			.size());
		initializeMappenRepository(user2, alleMappen);
		assertEquals(3, compoundDocumentPersistencePort.findByCreatedBy(user2)
			.size());
		initializeMappenRepository(user3, alleMappen);
		assertEquals(0, compoundDocumentPersistencePort.findByCreatedBy(user3)
			.size());
	}


	private CompoundDocumentDomain erzeugeMappe(RegelungsVorhabenId regelungsVorhabenId, User creator,
		DocumentState state, Instant createdAt, CompoundDocumentId vorgaengerId) {
		return CompoundDocumentDomain.builder()
			.compoundDocumentId(new CompoundDocumentId(UUID.randomUUID()))
			.regelungsVorhabenId(regelungsVorhabenId)
			.inheritFromId(vorgaengerId)
			.state(state)
			.createdBy(creator)
			.createdAt(createdAt)
			.updatedAt(createdAt)
			.build();
	}


	private void initializeMappenRepository(User user, List<CompoundDocumentDomain> mappen) {
		doReturn(mappen.stream()
			.filter(mappe -> user.equals(mappe.getCreatedBy()))
			.collect(Collectors.toList()))
			.when(compoundDocumentPersistencePort)
			.findByCreatedBy(user);
	}

	@Test
	void testBestandsrechtDatenSpeicherung() {

		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = TestDTOUtil.getRandomRegelungsvorhabenEditorDTO();

		when(converterService.convertXmlToJson(any())).thenReturn(
			"{}");
		when(session.getUser()).thenReturn(
			user1);
		when(regelungsvorhabenService.getAllRegulatoryProposalsForUserHeIsAuthorOf()).thenReturn(List.of(
			regelungsvorhabenEditorDTO
		));

		doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			((ArrayList) args[2]).add(ServiceState.OK);
			return null;
		}).when(bestandsrechtPersistencePort)
			.save(any(), any(), any());

		BestandsrechtDTO bestandsrechtDTO = BestandsrechtDTO.builder().content("{}").eli("testEli123").titelKurz("Test Titel kurz")
			.titelLang("Test Titel Lang").build();

		List<ServiceState> status = new ArrayList<>();

		propositionService.bestandsrechtDatenSpeicherung(user1.getGid().getId(), regelungsvorhabenEditorDTO.getId(), bestandsrechtDTO, status);

		assertFalse(status.isEmpty());
		assertEquals(ServiceState.OK, status.get(0));
	}

	@Test
	void testBestandsrechtDatenSpeicherungError() {

		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = TestDTOUtil.getRandomRegelungsvorhabenEditorDTO();

		when(converterService.convertXmlToJson(any())).thenReturn(
			"error");
		when(session.getUser()).thenReturn(
			user1);
		when(regelungsvorhabenService.getAllRegulatoryProposalsForUserHeIsAuthorOf()).thenReturn(List.of(
			regelungsvorhabenEditorDTO
		));

		doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			((ArrayList) args[2]).add(ServiceState.OK);
			return null;
		}).when(bestandsrechtPersistencePort)
			.save(any(), any(), any());

		BestandsrechtDTO bestandsrechtDTO = BestandsrechtDTO.builder().content("notValid").eli("testEli123").titelKurz("Test Titel kurz")
			.titelLang("Test Titel Lang").build();

		List<ServiceState> status = new ArrayList<>();

		propositionService.bestandsrechtDatenSpeicherung(user1.getGid().getId(), regelungsvorhabenEditorDTO.getId(), bestandsrechtDTO, status);

		assertFalse(status.isEmpty());
		assertEquals(ServiceState.DOCUMENT_NOT_VALID, status.get(0));
	}


	@Test
	void testBestandsrechtDatenSpeicherungNoPermissionError() {

		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = TestDTOUtil.getRandomRegelungsvorhabenEditorDTO();

		when(converterService.convertXmlToJson(any())).thenReturn(
			"error");
		when(session.getUser()).thenReturn(
			user1);

		BestandsrechtDTO bestandsrechtDTO = BestandsrechtDTO.builder().content("notValid").eli("testEli123").titelKurz("Test Titel kurz")
			.titelLang("Test Titel Lang").build();

		List<ServiceState> status = new ArrayList<>();

		propositionService.bestandsrechtDatenSpeicherung(user2.getGid().getId(), regelungsvorhabenEditorDTO.getId(), bestandsrechtDTO, status);

		assertFalse(status.isEmpty());
		assertEquals(ServiceState.NO_PERMISSION, status.get(0));
	}

}
