// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.validate.impl;

import de.itzbund.egesetz.bmi.lea.domain.services.transform.JsonToXmlTransformer;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.Test;
import org.springframework.web.server.ResponseStatusException;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import static de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ServiceUtils.convertToString;
import static de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ServiceUtils.jsonConvertToXml;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test suite for {@link de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ServiceUtils}.
 */
@Log4j2
class ServiceUtilsTest {

    @Test
    @SneakyThrows
    void testConvertToString() {
        String expected = "TEST";
        InputStream inputStream = fromString(expected);
        assertEquals(expected, convertToString(inputStream));

        expected = "Line 1\n Line 2 \n\n ... \n\nLine n";
        assertTrue(expected.lines().count() > 1);
        inputStream = fromString(expected);
        assertEquals(expected, convertToString(inputStream));

        BufferedReader reader = new BufferedReader(new InputStreamReader(
            Objects.requireNonNull(ServiceUtilsTest.class.getResourceAsStream("/utils/sample-data.txt"))));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            if (sb.length() > 0) {
                sb.append('\n');
            }
            sb.append(line);
        }
        expected = sb.toString();

        inputStream = new DataInputStream(
            Objects.requireNonNull(ServiceUtilsTest.class.getResourceAsStream("/utils/sample-data.txt")));
        String actual = convertToString(inputStream);
        assertEquals(expected, actual);
    }


    @Test
    void testJsonConvertToXml() {
        JsonToXmlTransformer transformer = new JsonToXmlTransformer();

        // positive case: valid JSON as input => texts are equal
        String json = convertToString(Objects.requireNonNull(
            ServiceUtilsTest.class.getResourceAsStream("/transform/stammgesetz-vorblatt.json")
        ));
        JSONParser jsonParser = new JSONParser();
        JSONArray jsonArray = null;
        try {
            jsonArray = (JSONArray) jsonParser.parse(json);
        } catch (ParseException e) {
            log.error(e);
        }
        assert jsonArray != null;
        JSONObject jsonObject = (JSONObject) jsonArray.get(0);
        String xmlContent = jsonConvertToXml(jsonObject.toJSONString(), transformer);
        assertNotNull(xmlContent);
        assertFalse(xmlContent.isBlank());

        // negative case: invalid JSON as input => exception expected
        String noJson = "foo";
        assertThrows(ResponseStatusException.class, () -> jsonConvertToXml(noJson, transformer));
    }


    private InputStream fromString(String text) {
        return new ByteArrayInputStream(text.getBytes(StandardCharsets.UTF_8));
    }

}
