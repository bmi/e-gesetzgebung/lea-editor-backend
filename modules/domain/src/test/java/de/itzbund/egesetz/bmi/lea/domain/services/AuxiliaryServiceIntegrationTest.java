// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.core.json.JSONUtils;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.CreateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.ProponentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RessortEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.model.Template;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.TemplatePersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.services.meta.MetadatenUpdateService;
import de.itzbund.egesetz.bmi.lea.domain.services.transform.JsonToXmlTransformer;
import de.itzbund.egesetz.bmi.lea.domain.services.transform.XmlToJsonTransformer;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.SchemaProvider;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationData;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationServiceImpl;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.Validator;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.SchematronValidationTask;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.XmlSchemaValidationTask;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_AUTHORIALNOTE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_HEADING;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_P;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_TYPE;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDocumentObject;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {
    // Services
    TemplateService.class,
    AuxiliaryService.class,
    PlategRequestService.class,
    ConverterService.class,
    ValidationServiceImpl.class,
    // Repositories
    DokumentPersistencePort.class, TemplatePersistencePort.class, CompoundDocumentPersistencePort.class,
    // Rest
    LeageSession.class,
    Document.class,
    MetadatenUpdateService.class,
    PropositionMapper.class,
    JsonToXmlTransformer.class,
    XmlToJsonTransformer.class,
    SchemaProvider.class,
    Validator.class,
    XmlSchemaValidationTask.class,
    SchematronValidationTask.class,
    ValidationData.class
})
@Log4j2
@SuppressWarnings("unused")
class AuxiliaryServiceIntegrationTest {

    private static final UUID REGELUNGS_VORHABEN_ID =
        UUID.fromString("00000000-0000-0000-0000-000000000005");

    private static final Pattern PTN_GUID = Pattern.compile(
        "\"GUID\":\\s?\"([\\dabcdef]{8}\\-[\\dabcdef]{4}\\-[\\dabcdef]{4}\\-[\\dabcdef]{4}\\-[\\dabcdef]{12})\"");

    private static final Pattern PTN_TEMPLATE_INSERT_SQL = Pattern.compile("\\s+\\('([-\\w]+)'(, '.+?'){2}, \\d+, '(.+)'\\)[,;]");

    private static final Set<String> EXCLUDED_TEMPLATES = Stream.of(
            "anlage", "synopse",
            "mantelgesetz-rechtsetzungsdokument", "mantelverordnung-rechtsetzungsdokument",
            "stammgesetz-rechtsetzungsdokument", "stammverordnung-rechtsetzungsdokument",
            "mantelgesetz-regelungstext-0", "mantelverordnung-regelungstext-0")
        .collect(Collectors.toCollection(HashSet::new));

    private static User userEntity;

    @Autowired
    private AuxiliaryService auxiliaryService;
    @Autowired
    private TemplateService templateService;
    @MockBean
    private CommentService commentService;
    @MockBean
    private LeageSession session;
    @MockBean
    private DokumentPersistencePort documentRepository;
    @MockBean
    private TemplatePersistencePort templateRepository;
    @MockBean
    private CompoundDocumentPersistencePort compoundDocumentRepository;
    @MockBean
    private PlategRequestService plategRequestService;
    @MockBean
    private PropositionMapper propositionMapper;

    @Autowired
    private SchemaProvider schemaProvider;
    @Autowired
    private Validator validator;
    @Autowired
    private XmlSchemaValidationTask xmlSchemaValidationTask;
    @Autowired
    private SchematronValidationTask schematronValidationTask;
    @Autowired
    private ValidationData validationData;
    @Autowired
    private JsonToXmlTransformer jsonToXmlTransformer;
    @Autowired
    private XmlToJsonTransformer xmlToJsonTransformer;
    @Autowired
    private ValidationServiceImpl validationService;
    @Autowired
    private ConverterService converterService;


    private String propositionId;


    @BeforeAll
    static void setUp() {
        Utils.UserData userData = Utils.getRandomUserData();
        userEntity = User.builder()
            .gid(new UserId(userData.getGid()))
            .email(userData.getEmail())
            .name(userData.getName())
            .build();
    }


    @BeforeEach
    void init() {
        Proposition randomProposition = TestObjectsUtil.getRandomProposition();
        propositionId = randomProposition.getPropositionId().getId().toString();
        when(propositionMapper.mapDto(any(PropositionDTO.class))).thenReturn(randomProposition);
    }

    @Disabled("not running in maven because of file paths")
    @ParameterizedTest(name = "{index}: Load template with GUIDs - {0}")
    @MethodSource("provideTemplateData")
    @SneakyThrows
    void testMakeDocument(String templateName, String templateContent) {
        // Arrange
        assertNotNull(templateContent);
        assertFalse(templateContent.isBlank());
        UUID rvId = UUID.fromString("10000000-0000-0000-0000-000000000000");

        CreateDocumentDTO createDocumentDTO = CreateDocumentDTO.builder()
            .title(Utils.getRandomString())
            .regelungsvorhabenId(REGELUNGS_VORHABEN_ID)
            .type(DocumentType.getFromTemplateID(templateName))
            .initialNumberOfLevels(0)
            .build();

        when(session.getUser()).thenReturn(userEntity);

        when(templateRepository.findAllByTemplateNameOrderByVersionDescUpdatedAtDesc(any())).thenReturn(
            List.of(Template.builder()
                .templateName(templateName)
                .content(templateContent)
                .updatedAt(Instant.now())
                .build())
        );

        when(compoundDocumentRepository.findByCompoundDocumentId(any())).thenReturn(Optional.of(TestObjectsUtil.getCompoundDocumentEntity()));

        when(plategRequestService.getRegulatoryProposalById(any())).thenReturn(
            RegelungsvorhabenEditorDTO.builder()
                .id(rvId)
                .bearbeitetAm(Instant.now())
                .technischesFfRessort(
                    RessortEditorDTO.builder()
                        .bezeichnungNominativ("Bundesregierung")
                        .build()
                )
                .build());

        when(propositionMapper.map(any(RegelungsvorhabenEditorDTO.class))).thenReturn(
            PropositionDTO.builder()
                .id(rvId)
                .bearbeitetAm(Instant.now())
                .proponentDTO(
                    ProponentDTO.builder()
                        .designationNominative("Bundesregierung")
                        .build()
                )
                .build());

        // Act
        Document document = auxiliaryService.makeDocument(new CompoundDocumentId(UUID.randomUUID()), createDocumentDTO);

        // Assert
        assertNotNull(document);
        assertNotNull(document.getDocumentEntity());
        assertNotNull(document.getDocumentEntity().getContent());

        String content = document.getDocumentEntity().getContent();
        assertFalse(content.isBlank());
        List<String> guids = Utils.findAll(content, PTN_GUID, 1);
        assertNotNull(guids);
        assertFalse(guids.isEmpty());

        guids.forEach(guid -> assertTrue(Utils.isUUID(guid)));

        // assert that: all UUIDs are different, i.e. no two GUIDs have the same value
        Set<String> setOfGuids = new HashSet<>(guids);
        assertEquals(guids.size(), setOfGuids.size());

        // assert that exported XML is valid
        JSONObject jsonObject = getDocumentObject(content);
        assertNotNull(jsonObject);
        String xmlContent = converterService.convertJsonToXml(jsonObject);
        assertNotNull(xmlContent);
    }

    @Test
    void testAppendEmptyTextNodes() {
        // Load test content
        String jsonContent = Utils.getContentOfTextResource(
            "/services/templates/template-stammgesetz-begruendung.LDML.de-1.6.json");
        JSONObject document = JSONUtils.getDocumentObject(jsonContent);
        assertNotNull(document);

        // Append a footnote at the end
        JSONObject heading = JSONUtils.getDescendant(document, false,
            "*", "akn:mainBody", "akn:hcontainer", "akn:heading");
        assertNotNull(heading);
        assertEquals(ELEM_HEADING, JSONUtils.getLocalName(JSONUtils.getType(heading)));
        assertEquals("Allgemeiner Teil", JSONUtils.getText(heading));
        assertEquals("Allgemeiner Teil", JSONUtils.getEffectiveTextValue(heading));
        assertEquals(1, JSONUtils.getChildren(heading)
            .size());

        JSONUtils.addChildren(heading, getFootnote());
        assertEquals("Allgemeiner Teil", JSONUtils.getEffectiveTextValue(heading));
        assertEquals(2, JSONUtils.getChildren(heading)
            .size());

        // Append an empty text node to the end
        String changedContent = document.toJSONString();
        String appendedContent = auxiliaryService.appendEmptyTextNodes(changedContent);
        document = JSONUtils.getDocumentObject(appendedContent);
        assertNotNull(document);

        // Check heading
        heading = JSONUtils.getDescendant(document, false,
            "*", "akn:mainBody", "akn:hcontainer", "akn:heading");
        assertNotNull(heading);
        assertEquals("Allgemeiner Teil", JSONUtils.getEffectiveTextValue(heading));
        assertEquals(3, JSONUtils.getChildren(heading)
            .size());
    }

    @SuppressWarnings("unchecked")
    private JSONObject getFootnote() {
        JSONObject p = new JSONObject();
        p.put(JSON_KEY_TYPE, "akn:" + ELEM_P);
        JSONUtils.addText(p, "fussnote");

        JSONObject footnote = new JSONObject();
        footnote.put(JSON_KEY_TYPE, "akn:" + ELEM_AUTHORIALNOTE);
        JSONUtils.addChildren(footnote, p);

        return footnote;
    }


    @SneakyThrows
    private static Stream<Arguments> provideTemplateData() {
        String path = "../persistence/src/main/resources/db/migration/h2/V1_0_4__template.sql";
        File file = new File(path);
        assertTrue(file.canRead());

        List<Pair<String, String>> templates = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                Matcher matcher = PTN_TEMPLATE_INSERT_SQL.matcher(line);
                if (matcher.matches()) {
                    String templateName = matcher.group(1);
                    String templateContent = matcher.group(3);
                    if (!EXCLUDED_TEMPLATES.contains(templateName)) {
                        templates.add(Pair.of(templateName, templateContent));
                    }
                }
            }
        } catch (IOException e) {
            log.error(e);
        }

        assertEquals(25 - EXCLUDED_TEMPLATES.size(), templates.size());

        return templates.stream()
            .map(p -> arguments(p.getLeft(), p.getRight()));
    }


    private static String convertStreamToString(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
        }
        reader.close();
        return stringBuilder.toString();
    }


    public static String formatInstantToString(Instant instant) {
        // Konvertiere Instant in ein ZonedDateTime-Objekt mit einer Zeitzone deiner Wahl
        ZoneId zoneId = ZoneId.of("CET"); // Hier kannst du die gewünschte Zeitzone angeben
        ZonedDateTime zonedDateTime = instant.atZone(zoneId);

        // Erstelle einen DateTimeFormatter für das gewünschte Format "Jahr-Monat-Tag"
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd");

        // Formatiere das ZonedDateTime-Objekt in den gewünschten String
        String formattedString = zonedDateTime.format(formatter);

        return formattedString;
    }


    @Test
    void testmakeDocumentForUserFilter() throws IOException {
        // Arrange
        // - Pfad zur Ressource angeben (ohne das "resources" Verzeichnis)
        String resourcePath = "eli-metadata/sample-regelungstext.json";
        Instant now = Instant.now();

        // Ressource laden
        ClassPathResource classPathResource = new ClassPathResource(resourcePath);
        InputStream inputStream = classPathResource.getInputStream();
        String template = convertStreamToString(inputStream);

        when(templateRepository.findAllByTemplateNameOrderByVersionDescUpdatedAtDesc(any())).thenReturn(
            List.of(Template.builder()
                .templateName("templateName")
                .content(template)
                .updatedAt(now)
                .build())
        );

        CreateDocumentDTO createDocumentDTO = TestDTOUtil.getRandomCreateDocumentDTO();
        PropositionDTO propositionDTO = TestDTOUtil.getRandomPropositionDTO();

        // Act
        Document document = auxiliaryService.makeDocumentForUser(TestObjectsUtil.getCompoundDocumentEntity(), createDocumentDTO, userEntity, propositionDTO);

        // Assert
        assertThat(document.getDocumentEntity().getContent()).contains(propositionId);
    }

}
