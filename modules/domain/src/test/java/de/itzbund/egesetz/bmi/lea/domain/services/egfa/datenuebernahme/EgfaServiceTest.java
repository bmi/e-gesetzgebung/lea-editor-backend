// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.egfa.datenuebernahme;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.ImportEgfaDataDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.CreateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeParentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaStatus;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaStatusDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.UserMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.EgfaData;
import de.itzbund.egesetz.bmi.lea.domain.model.EgfaMetadata;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.EgfaDataId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.EgfaDataPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.EgfaMetadataPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.AuxiliaryRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CompoundDocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.PropositionRestPort;
import de.itzbund.egesetz.bmi.lea.domain.services.RegelungsvorhabenService;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import lombok.SneakyThrows;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {EgfaService.class})
@SuppressWarnings("unused")
class EgfaServiceTest {

	public static final List<EgfaServiceTestInput> testInputs = List.of(
		EgfaServiceTestInput.OEFFENTLICHE_HAUSHALTE,
		EgfaServiceTestInput.ERFUELLUNGSAUFWAND,
		EgfaServiceTestInput.ERFUELLUNGSAUFWAND2,
		EgfaServiceTestInput.ERFUELLUNGSAUFWAND_LEER,
		EgfaServiceTestInput.KMU_TEST,
		EgfaServiceTestInput.AUSWIRKUNGEN_EINZELPREISE_PREISNIVEAU,
		EgfaServiceTestInput.SONSTIGE_KOSTEN,
		EgfaServiceTestInput.ENAP,
		EgfaServiceTestInput.GLEICHSTELLUNGSORIENTIERTE_GFA,
		EgfaServiceTestInput.DISABILITY_MAINSTREAMING,
		EgfaServiceTestInput.DEMOFGRAFIE_CHECK,
		EgfaServiceTestInput.GLEICHWERTIGKEITS_CHECK,
		EgfaServiceTestInput.AUSWIRKUNGEN_VERBRAUCHERINNEN,
		EgfaServiceTestInput.WEITERE_WESENTLICHE_AUSWIRKUNGEN,
		EgfaServiceTestInput.EVALUIERUNG
	);
	private static final Pattern PTN_DEFAULT_GUID =
		Pattern.compile("\"GUID\":\\s?\"00000000-0000-0000-0000-000000000001\"");
	private static ObjectMapper objectMapper;
	private static String begruendungContent;
	private static String vorblattContent;
	@Autowired
	private EgfaService egfaService;
	@MockBean
	private LeageSession session;
	@MockBean
	private PropositionRestPort propositionRestPort;
	@MockBean
	private CompoundDocumentPersistencePort compoundDocumentRepository;
	@MockBean
	private DokumentPersistencePort documentRepository;
	@MockBean
	private AuxiliaryRestPort auxiliaryService;
	@MockBean
	private CompoundDocumentRestPort compoundDocumentService;
	@MockBean
	private RegelungsvorhabenService regelungsvorhabenService;
	@MockBean
	private PropositionMapper propositionMapper;
	@MockBean
	private UserMapper userMapper;
	@MockBean
	private EgfaDataPersistencePort egfaDataRepository;
	@MockBean
	private EgfaMetadataPersistencePort egfaMetadataRepository;

	private User testUser;


	@BeforeAll
	@SneakyThrows
	static void setUp() {
		objectMapper = new ObjectMapper();
		objectMapper.registerModule(new Jdk8Module());
		objectMapper.registerModule(new JavaTimeModule());
		objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

		String egfaContent;

		for (EgfaServiceTestInput input : testInputs) {
			egfaContent = Utils.getContentOfTextResource(input.exportDataPath);
			EgfaDatenuebernahmeDTO datenuebernahmeDTO =
				objectMapper.readValue(egfaContent, EgfaDatenuebernahmeDTO.class);
			assertNotNull(datenuebernahmeDTO);
		}

		begruendungContent = changeGUIDs(Utils.getContentOfTextResource(
			"/services/egfa/stammgesetz-begruendung.json"));
		vorblattContent = changeGUIDs(Utils.getContentOfTextResource(
			"/services/egfa/stammgesetz-vorblatt.json"));
	}


	private static Stream<Arguments> provideEGFAModuleData() {
		return Stream.of(
			Arguments.of(EgfaServiceTestInput.OEFFENTLICHE_HAUSHALTE.getEgfaDatenuebernahmeDTO()),
			Arguments.of(EgfaServiceTestInput.ERFUELLUNGSAUFWAND.getEgfaDatenuebernahmeDTO()),
			Arguments.of(EgfaServiceTestInput.ERFUELLUNGSAUFWAND2.getEgfaDatenuebernahmeDTO()),
			Arguments.of(EgfaServiceTestInput.ERFUELLUNGSAUFWAND_LEER.getEgfaDatenuebernahmeDTO()),
			Arguments.of(EgfaServiceTestInput.KMU_TEST.getEgfaDatenuebernahmeDTO()),
			Arguments.of(EgfaServiceTestInput.AUSWIRKUNGEN_EINZELPREISE_PREISNIVEAU.getEgfaDatenuebernahmeDTO()),
			Arguments.of(EgfaServiceTestInput.SONSTIGE_KOSTEN.getEgfaDatenuebernahmeDTO()),
			Arguments.of(EgfaServiceTestInput.ENAP.getEgfaDatenuebernahmeDTO()),
			Arguments.of(EgfaServiceTestInput.GLEICHSTELLUNGSORIENTIERTE_GFA.getEgfaDatenuebernahmeDTO()),
			Arguments.of(EgfaServiceTestInput.DISABILITY_MAINSTREAMING.getEgfaDatenuebernahmeDTO()),
			Arguments.of(EgfaServiceTestInput.DEMOFGRAFIE_CHECK.getEgfaDatenuebernahmeDTO()),
			Arguments.of(EgfaServiceTestInput.GLEICHWERTIGKEITS_CHECK.getEgfaDatenuebernahmeDTO()),
			Arguments.of(EgfaServiceTestInput.AUSWIRKUNGEN_VERBRAUCHERINNEN.getEgfaDatenuebernahmeDTO()),
			Arguments.of(EgfaServiceTestInput.WEITERE_WESENTLICHE_AUSWIRKUNGEN.getEgfaDatenuebernahmeDTO()),
			Arguments.of(EgfaServiceTestInput.EVALUIERUNG.getEgfaDatenuebernahmeDTO())
		);
	}


	private static String changeGUIDs(String origContent) {
		return Utils.replaceTokens(origContent, PTN_DEFAULT_GUID,
			p -> String.format("\"GUID\":\"%s\"", UUID.randomUUID()));
	}


	@AfterAll
	static void tearDown() {
		objectMapper = null;
	}


	@BeforeEach
	void init() {
		testUser = User.builder()
			.gid(new UserId(UUID.randomUUID()
				.toString()))
			.name(Utils.getRandomString())
			.email(Utils.getRandomString())
			.build();
	}


	@Test
	void when_UserInSessionIsDifferentFromGIDParam_then_Error_NoPermission() {
		// Arrange
		String gid = Utils.getRandomString();
		UUID regelungsvorhabenId = UUID.randomUUID();
		List<ServiceState> status = new ArrayList<>();

		when(session.getUser()).thenReturn(testUser);

		// Act
		egfaService.egfaDatenuebernahme(gid, regelungsvorhabenId, null, status);

		//Assert
		assertFalse(status.isEmpty());
		assertEquals(ServiceState.NO_PERMISSION, status.get(0));
	}


	@Test
	void when_RegelungsvorhabenIdNotValidForUser_then_Error_NoPermission() {
		// Arrange
		UUID regelungsvorhabenId = UUID.randomUUID();
		List<ServiceState> status = new ArrayList<>();

		when(session.getUser()).thenReturn(testUser);
		when(propositionRestPort.getAllPropositionDescriptionsByUser()).thenReturn(List.of());

		// Act
		egfaService.egfaDatenuebernahme(testUser.getGid()
			.getId(), regelungsvorhabenId, null, status);

		//Assert
		assertFalse(status.isEmpty());
		assertEquals(ServiceState.NO_PERMISSION, status.get(0));
	}


	@Test
	void when_DTOIsNull_then_Error_InvalidArguments() {
		// Arrange
		UUID regelungsvorhabenId = UUID.randomUUID();
		List<ServiceState> status = new ArrayList<>();

		when(session.getUser()).thenReturn(testUser);
		when(propositionRestPort.getAllPropositionDescriptionsByUser()).thenReturn(List.of(
			PropositionDTO.builder()
				.id(regelungsvorhabenId)
				.build()
		));

		// Act
		egfaService.egfaDatenuebernahme(testUser.getGid()
			.getId(), regelungsvorhabenId, null, status);

		//Assert
		assertFalse(status.isEmpty());
		assertEquals(ServiceState.INVALID_ARGUMENTS, status.get(0));
	}


	@Test
	@SneakyThrows
	void when_EgfaDataIsSupplied_then_MappingIsPossible() {
		EgfaMapping egfaMapping;

		for (EgfaServiceTestInput input : testInputs) {
			egfaMapping = new EgfaMapping(input.getEgfaDatenuebernahmeDTO());
			assertNotNull(egfaMapping);
			assertEquals(input.concerningVorblatt, egfaMapping.isConcerningVorblatt());
			assertEquals(input.concerningBegruendung, egfaMapping.isConcerningBegruendung());
		}
	}


	@ParameterizedTest
	@MethodSource("provideEGFAModuleData")
	@SneakyThrows
	void when_DTOIsValid_then_Success(EgfaDatenuebernahmeDTO egfaDatenuebernahmeDTO) {
		// Arrange
		UUID regelungsvorhabenId = UUID.randomUUID();
		List<ServiceState> status = new ArrayList<>();

		CompoundDocumentDomain compoundDocument = CompoundDocumentDomain.builder()
			.regelungsVorhabenId(new RegelungsVorhabenId(regelungsvorhabenId))
			.type(CompoundDocumentTypeVariant.STAMMGESETZ)
			.state(DocumentState.DRAFT)
			.updatedAt(Instant.now())
			.build();

		List<DocumentDomain> documents = List.of(
			DocumentDomain.builder()
				.type(DocumentType.BEGRUENDUNG_STAMMGESETZ)
				.content(begruendungContent)
				.build(),
			DocumentDomain.builder()
				.type(DocumentType.VORBLATT_STAMMGESETZ)
				.content(vorblattContent)
				.build()
		);

		when(session.getUser()).thenReturn(testUser);
		when(propositionRestPort.getAllPropositionDescriptionsByUser()).thenReturn(List.of(
			PropositionDTO.builder()
				.id(regelungsvorhabenId)
				.build()
		));
		when(compoundDocumentRepository.findByCreatedByAndRegelungsVorhabenId(any(), any())).thenReturn(
			List.of(compoundDocument));
		when(documentRepository.findByCreatedByAndCompoundDocumentId(any(), any())).thenReturn(documents);

		// Act
		egfaService.egfaDatenuebernahme(testUser.getGid()
			.getId(), regelungsvorhabenId, egfaDatenuebernahmeDTO, status);

		//Assert
		assertFalse(status.isEmpty());
		assertEquals(ServiceState.OK, status.get(0));
	}

	@ParameterizedTest
	@MethodSource("provideEGFAModuleData")
	@SneakyThrows
	void when_DTOIsImportedTwice_then_Success(EgfaDatenuebernahmeDTO egfaDatenuebernahmeDTO) {
		// Arrange
		UUID regelungsvorhabenId = UUID.randomUUID();
		List<ServiceState> status = new ArrayList<>();

		CompoundDocumentDomain compoundDocument = CompoundDocumentDomain.builder()
			.regelungsVorhabenId(new RegelungsVorhabenId(regelungsvorhabenId))
			.type(CompoundDocumentTypeVariant.STAMMGESETZ)
			.state(DocumentState.DRAFT)
			.updatedAt(Instant.now())
			.build();

		List<DocumentDomain> documents = List.of(
			DocumentDomain.builder()
				.type(DocumentType.BEGRUENDUNG_STAMMGESETZ)
				.content(begruendungContent)
				.build(),
			DocumentDomain.builder()
				.type(DocumentType.VORBLATT_STAMMGESETZ)
				.content(vorblattContent)
				.build()
		);

		when(session.getUser()).thenReturn(testUser);
		when(propositionRestPort.getAllPropositionDescriptionsByUser()).thenReturn(List.of(
			PropositionDTO.builder()
				.id(regelungsvorhabenId)
				.build()
		));
		when(compoundDocumentRepository.findByCreatedByAndRegelungsVorhabenId(any(), any())).thenReturn(
			List.of(compoundDocument));
		when(documentRepository.findByCreatedByAndCompoundDocumentId(any(), any())).thenReturn(documents);

		// Act
		egfaService.egfaDatenuebernahme(testUser.getGid()
			.getId(), regelungsvorhabenId, egfaDatenuebernahmeDTO, status);

		egfaService.egfaDatenuebernahme(testUser.getGid()
			.getId(), regelungsvorhabenId, egfaDatenuebernahmeDTO, status);

		//Assert
		assertFalse(status.isEmpty());
		assertEquals(ServiceState.OK, status.get(0));
	}


	@ParameterizedTest
	@MethodSource("provideEGFAModuleData")
	@SneakyThrows
	void when_VorblattorBegruendung_get_created_then_Success(EgfaDatenuebernahmeDTO egfaDatenuebernahmeDTO) {
		// Arrange
		UUID regelungsvorhabenId = UUID.randomUUID();
		List<ServiceState> status = new ArrayList<>();

		CreateDocumentDTO vorblattDocumentDto = TestDTOUtil.getRandomCreateDocumentDTO();
		vorblattDocumentDto.setType(DocumentType.getFromTemplateID("stammgesetz-vorblatt"));
		vorblattDocumentDto.setTitle("Vorblatt");

		CreateDocumentDTO begruendungDocumentDto = TestDTOUtil.getRandomCreateDocumentDTO();
		begruendungDocumentDto.setType(DocumentType.getFromTemplateID("stammgesetz-begruendung"));
		begruendungDocumentDto.setTitle("Begründung");

		DocumentId vorblattDocumentId = new DocumentId(UUID.randomUUID());
		DocumentId begruendungDocumentId = new DocumentId(UUID.randomUUID());

		DocumentDomain vorblattDocumentDomain = DocumentDomain.builder()
			.documentId(vorblattDocumentId)
			.title("Vorblatt")
			.type(DocumentType.VORBLATT_STAMMGESETZ)
			.content(vorblattContent)
			.build();

		DocumentDomain begruendungDocumentDomain = DocumentDomain.builder()
			.documentId(begruendungDocumentId)
			.title("Begründung")
			.type(DocumentType.BEGRUENDUNG_STAMMGESETZ)
			.content(begruendungContent)
			.build();

		when(auxiliaryService.makeDocumentForUser(any(), any(), any(), any())).thenAnswer(
			invocation -> {
				Object argument = invocation.getArguments()[1];
				if (((CreateDocumentDTO) argument).getType() == DocumentType.getFromTemplateID(
					"stammgesetz-vorblatt")) {
					return new Document(vorblattDocumentDomain);
				} else if (((CreateDocumentDTO) argument).getType() == DocumentType.getFromTemplateID(
					"stammgesetz-begruendung")) {
					return new Document(begruendungDocumentDomain);
				} else {
					return null;

				}
			}
		);

		when(compoundDocumentService.addDocumentForUser(any(), any(), any(), any())).thenReturn(
			new CompoundDocumentDTO());

		when(documentRepository.findByDocumentId(any())).thenAnswer(
			invocation -> {
				Object argument = invocation.getArguments()[0];
				if (argument.equals(vorblattDocumentId)) {
					return Optional.of(vorblattDocumentDomain);
				} else if (argument.equals(begruendungDocumentId)) {
					return Optional.of(begruendungDocumentDomain);
				} else {
					return null;
				}
			}

		);

		CompoundDocumentDomain compoundDocument = CompoundDocumentDomain.builder()
			.regelungsVorhabenId(new RegelungsVorhabenId(regelungsvorhabenId))
			.type(CompoundDocumentTypeVariant.STAMMGESETZ)
			.state(DocumentState.DRAFT)
			.updatedAt(Instant.now())
			.build();

		when(session.getUser()).thenReturn(testUser);
		when(propositionRestPort.getAllPropositionDescriptionsByUser()).thenReturn(List.of(
			PropositionDTO.builder()
				.id(regelungsvorhabenId)
				.build()
		));
		when(compoundDocumentRepository.findByCreatedByAndRegelungsVorhabenId(any(), any())).thenReturn(
			List.of(compoundDocument));
		when(documentRepository.findByCreatedByAndCompoundDocumentId(any(), any())).thenReturn(Collections.emptyList());

		// Act
		egfaService.egfaDatenuebernahme(testUser.getGid()
			.getId(), regelungsvorhabenId, egfaDatenuebernahmeDTO, status);

		//Assert
		assertFalse(status.isEmpty());
		assertEquals(ServiceState.OK, status.get(0));
	}


	@Test
	void when_DTOIsCorrect_then_save_is_possible() {
		// Arrange
		UUID regelungsvorhabenId = UUID.randomUUID();
		List<ServiceState> status = new ArrayList<>();

		when(session.getUser()).thenReturn(testUser);
		when(propositionRestPort.getAllPropositionDescriptionsByUser()).thenReturn(List.of(
			PropositionDTO.builder()
				.id(regelungsvorhabenId)
				.build()
		));

		doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			((ArrayList) args[1]).add(ServiceState.OK);
			return null;
		}).when(egfaDataRepository)
			.save(any(), any());

		EgfaDatenuebernahmeParentDTO egfaDatenuebernahmeParentDTO = new EgfaDatenuebernahmeParentDTO();
		egfaDatenuebernahmeParentDTO.setModuleName("ERFUELLUNGSAUFWAND");
		egfaDatenuebernahmeParentDTO.setEgfaContent(
			EgfaServiceTestInput.ERFUELLUNGSAUFWAND.getEgfaDatenuebernahmeDTO());
		egfaDatenuebernahmeParentDTO.setCompletedAt(Instant.now());

		// Act
		egfaService.egfaDatenSpeicherung(testUser.getGid()
			.getId(), regelungsvorhabenId, egfaDatenuebernahmeParentDTO, status);

		//Assert
		assertFalse(status.isEmpty());
		assertEquals(ServiceState.OK, status.get(0));
	}

	@Test
	void when_DTOIsNotCorrect_then_save_is_not_possible() {
		// Arrange
		UUID regelungsvorhabenId = UUID.randomUUID();
		List<ServiceState> status = new ArrayList<>();

		when(session.getUser()).thenReturn(testUser);
		when(propositionRestPort.getAllPropositionDescriptionsByUser()).thenReturn(List.of(
			PropositionDTO.builder()
				.id(regelungsvorhabenId)
				.build()
		));

		doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			((ArrayList) args[1]).add(ServiceState.OK);
			return null;
		}).when(egfaDataRepository)
			.save(any(), any());

		EgfaDatenuebernahmeParentDTO egfaDatenuebernahmeParentDTO = new EgfaDatenuebernahmeParentDTO();
		egfaDatenuebernahmeParentDTO.setModuleName("INVALID");
		egfaDatenuebernahmeParentDTO.setEgfaContent(
			EgfaServiceTestInput.ERFUELLUNGSAUFWAND.getEgfaDatenuebernahmeDTO());
		egfaDatenuebernahmeParentDTO.setCompletedAt(Instant.now());

		// Act
		egfaService.egfaDatenSpeicherung(testUser.getGid()
			.getId(), regelungsvorhabenId, egfaDatenuebernahmeParentDTO, status);

		//Assert
		assertFalse(status.isEmpty());
		assertEquals(ServiceState.INVALID_ARGUMENTS, status.get(0));
	}

	@Test
	void egfa_datenuebernahme_test() {
		// Arrange
		RegelungsVorhabenId regelungsvorhabenId = new RegelungsVorhabenId(UUID.randomUUID());
		CompoundDocumentId compoundDocumentId = new CompoundDocumentId(UUID.randomUUID());
		List<ServiceState> status = new ArrayList<>();
		EgfaDataId egfaDataId = new EgfaDataId(UUID.randomUUID());

		when(session.getUser()).thenReturn(testUser);
		when(compoundDocumentRepository.findByCompoundDocumentId(any())).thenReturn(
			Optional.of(
				CompoundDocumentDomain.builder().compoundDocumentId(compoundDocumentId).regelungsVorhabenId(regelungsvorhabenId)
					.build()
			));

		EgfaDatenuebernahmeDTO contentDTO = EgfaServiceTestInput.SONSTIGE_KOSTEN.getEgfaDatenuebernahmeDTO();
		ObjectMapper mapper = new ObjectMapper();
		String contentDTOString = "";
		try {
			contentDTOString = mapper.writeValueAsString(contentDTO);
		} catch (JsonProcessingException e) {
			fail(e);
		}
		when(egfaDataRepository.findNewestByRegelungsVorhabenIdAndModuleName(any(), any())).thenReturn(
			EgfaData.builder().egfaDataId(egfaDataId).content(contentDTOString).build()
		);

		when(propositionRestPort.getAllPropositionDescriptionsByUser()).thenReturn(List.of(
			PropositionDTO.builder()
				.id(regelungsvorhabenId.getId())
				.build()
		));

		List<DocumentDomain> documents = List.of(
			DocumentDomain.builder()
				.type(DocumentType.BEGRUENDUNG_STAMMGESETZ)
				.content(begruendungContent)
				.build(),
			DocumentDomain.builder()
				.type(DocumentType.VORBLATT_STAMMGESETZ)
				.content(vorblattContent)
				.build()
		);
		when(documentRepository.findByCreatedByAndCompoundDocumentId(any(), any())).thenReturn(documents);

		ImportEgfaDataDTO importEgfaDataDTO = new ImportEgfaDataDTO();
		importEgfaDataDTO.setModuleNames(List.of("SONSTIGE_KOSTEN"));
		// Act
		egfaService.egfaUebernahmeInDokumentenmappe(compoundDocumentId.getId(), importEgfaDataDTO, status);

		//Assert
		assertFalse(status.isEmpty());
		assertEquals(ServiceState.OK, status.get(0));
	}

	private static Stream<Arguments> provideEGFAStatusData() {
		EgfaDataId egfaDataId = new EgfaDataId(UUID.randomUUID());
		Instant completedAt = Instant.now();
		Instant completedAt2 = Instant.now().plusSeconds(TimeUnit.MINUTES.toSeconds(5));
		User testUser1 = User.builder()
			.gid(new UserId(UUID.randomUUID()
				.toString()))
			.name(Utils.getRandomString())
			.email(Utils.getRandomString())
			.build();

		return Stream.of(
			Arguments.of(Arrays.asList(EgfaStatus.MODUL_NO_DATA, null, null)),
			Arguments.of(Arrays.asList(EgfaStatus.MODUL_FINISHED_NOT_USED, EgfaData.builder().egfaDataId(egfaDataId).completedAt(completedAt).build(),
				null
			)),
			Arguments.of(Arrays.asList(EgfaStatus.MODUL_NO_NEWER_DATA, EgfaData.builder().egfaDataId(egfaDataId).completedAt(completedAt).build(),
				EgfaMetadata.builder().moduleCompletedAt(completedAt).createdByUserId(testUser1).createdAt(completedAt).build()
			)),
			Arguments.of(Arrays.asList(EgfaStatus.MODUL_DATA_NEW_UPDATE, EgfaData.builder().egfaDataId(egfaDataId).completedAt(completedAt2).build(),
				EgfaMetadata.builder().moduleCompletedAt(completedAt).createdByUserId(testUser1).createdAt(completedAt).build()
			))
		);
	}

	@ParameterizedTest
	@MethodSource("provideEGFAStatusData")
	@SneakyThrows
	void get_egfa_status_test(List<Object> inputData) {
		// Arrange
		RegelungsVorhabenId regelungsvorhabenId = new RegelungsVorhabenId(UUID.randomUUID());
		CompoundDocumentId compoundDocumentId = new CompoundDocumentId(UUID.randomUUID());
		List<ServiceState> status = new ArrayList<>();
		EgfaDataId egfaDataId = new EgfaDataId(UUID.randomUUID());
		Instant completedAt = Instant.now();

		when(session.getUser()).thenReturn(testUser);
		when(compoundDocumentRepository.findByCompoundDocumentId(any())).thenReturn(
			Optional.of(
				CompoundDocumentDomain.builder().compoundDocumentId(compoundDocumentId).regelungsVorhabenId(regelungsvorhabenId)
					.build()
			));

		when(egfaDataRepository.findNewestByRegelungsVorhabenIdAndModuleName(any(), any())).thenReturn(
			(EgfaData) inputData.get(1)
		);

		when(egfaMetadataRepository.findNewestByCompoundDocumentIdAndModuleName(any(), any())).thenReturn(
			Optional.ofNullable((EgfaMetadata) inputData.get(2))
		);

		// Act
		List<EgfaStatusDTO> egfaStatusList = egfaService.getEgfaStatusByCompoundDocumentId(status, compoundDocumentId.getId());

		//Assert
		assertFalse(status.isEmpty());
		assertEquals(ServiceState.OK, status.get(0));
		assertEquals(egfaStatusList.get(0).getEgfaDataStatus(), inputData.get(0));
	}


}
