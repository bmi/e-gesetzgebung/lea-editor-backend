// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.validate;

import de.itzbund.egesetz.bmi.lea.domain.services.validate.SchemaInstanceName;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.SchemaProvider;
import org.junit.jupiter.api.Test;

import javax.xml.XMLConstants;
import javax.xml.validation.Schema;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Testsuite for {@link SchemaProvider}.
 */
class SchemaProviderTest {

    @Test
    void testSchemaProvider() {
        SchemaProvider schemaProvider = new SchemaProvider();
        Schema testSchema = schemaProvider.getSchema(
            SchemaProvider.SchemaInstance.LEGAL_DOC_ML_DE_REGELUNGSTEXT_ENTWURF);
        assertNotNull(testSchema);

        SchemaInstanceName name1 = () -> "TestSchemaA";
        schemaProvider.registerSchema(name1, testSchema);
        assertNotNull(schemaProvider.getSchema(name1));

        SchemaInstanceName name2 = () -> "TestSchemaB";
        schemaProvider.registerSchema(name2, XMLConstants.W3C_XML_SCHEMA_NS_URI,
            "/schemas/akn4de/legalDocML.de-1.6/legalDocML.de-anschreiben.xsd");
        assertNotNull(schemaProvider.getSchema(name2));
    }

}
