// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.change;

import de.itzbund.egesetz.bmi.lea.core.json.JSONUtils;
import de.itzbund.egesetz.bmi.lea.core.json.TraversableJSONDocument;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.compare.RegulatoryTextComparer;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import de.itzbund.egesetz.bmi.lea.domain.services.TemplateService;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.EIdGeneratorService;
import lombok.SneakyThrows;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Map;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CHAPTER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_P;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PART;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SECTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_TEXT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_TYPE;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getLocalName;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewDefaultJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewDefaultJSONObjectWithText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNumObject;
import static de.itzbund.egesetz.bmi.lea.domain.change.AenderungsbefehlUtils.getAenderungsstelleUebergeordneteGliederungseinheit;
import static de.itzbund.egesetz.bmi.lea.domain.change.AenderungsbefehlUtils.getDocumentType;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class, SpringExtension.class})
@SuppressWarnings("unused")
class AenderungsbefehlBerechnungTest {

	@InjectMocks
	private AenderungsbefehlBerechnung aenderungsbefehlBerechnung;

	@MockBean
	private TemplateService templateService;

	@MockBean
	private EIdGeneratorService eIdGeneratorService;

	@Test
	@SneakyThrows
	void test_CreateChangeCommands_OZG() {
		// Arrange
		AenderungsbefehlBerechnung creator = aenderungsbefehlBerechnung;
		assertNotNull(creator);
		JSONObject baseDocument = getDocumentObject("/change/ozg-bestand.json");
		RechtsetzungsdokumentTyp baseDocumentType = getDocumentType(baseDocument);
		JSONObject versionDocument = getDocumentObject("/change/ozg-neufassung.json");
		RechtsetzungsdokumentTyp versionDocumentType = getDocumentType(versionDocument);

		when(templateService.loadTemplate(DocumentType.REGELUNGSTEXT_MANTELGESETZ, 0))
			.thenReturn(Utils.getContentOfTextResource("/change/mantelgesetz-regelungstext-0.json"));
		when(templateService.loadTemplate(DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG, 0))
			.thenReturn(Utils.getContentOfTextResource("/change/mantelverordnung-regelungstext-0.json"));
		when(eIdGeneratorService.erzeugeEIdsFuerDokument(any(JSONObject.class))).thenAnswer(i -> i.getArguments()[0]);

		// Act
		creator.compare(
			baseDocument,
			Map.of(RegulatoryTextComparer.RegulatoryTextMetadata.TYPE.name(), baseDocumentType),
			versionDocument,
			Map.of(RegulatoryTextComparer.RegulatoryTextMetadata.TYPE.name(), versionDocumentType));
		JSONObject resultDocumentObject = creator.getResultDocumentObject();

		// Assert
		assertNotNull(resultDocumentObject);
		testForValidSlateValue(resultDocumentObject);
	}

	@Test
	@SneakyThrows
	void test_CreateChangeCommands_BDSG() {
		// Arrange
		AenderungsbefehlBerechnung creator = aenderungsbefehlBerechnung;
		assertNotNull(creator);
		JSONObject baseDocument = getDocumentObject("/change/bdsg-bestand.json");
		RechtsetzungsdokumentTyp baseDocumentType = getDocumentType(baseDocument);
		JSONObject versionDocument = getDocumentObject("/change/bdsg-neufassung.json");
		RechtsetzungsdokumentTyp versionDocumentType = getDocumentType(versionDocument);

		when(templateService.loadTemplate(DocumentType.REGELUNGSTEXT_MANTELGESETZ, 0))
			.thenReturn(Utils.getContentOfTextResource("/change/mantelgesetz-regelungstext-0.json"));
		when(templateService.loadTemplate(DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG, 0))
			.thenReturn(Utils.getContentOfTextResource("/change/mantelverordnung-regelungstext-0.json"));
		when(eIdGeneratorService.erzeugeEIdsFuerDokument(any(JSONObject.class))).thenAnswer(i -> i.getArguments()[0]);

		// Act
		creator.compare(
			baseDocument,
			Map.of(RegulatoryTextComparer.RegulatoryTextMetadata.TYPE.name(), baseDocumentType),
			versionDocument,
			Map.of(RegulatoryTextComparer.RegulatoryTextMetadata.TYPE.name(), versionDocumentType));
		JSONObject resultDocumentObject = creator.getResultDocumentObject();

		// Assert
		assertNotNull(resultDocumentObject);
		testForValidSlateValue(resultDocumentObject);
	}


	@Test
	void testGetAenderungsstelleUebergeordneteGliederungseinheit() {
		JSONObject sectionObject = makeHierarchyLevel(ELEM_SECTION, "3a", "Abschnitt 3a");
		addChildren(sectionObject, makeNewDefaultJSONObjectWithText(ELEM_P, "Test"));

		JSONObject chapterObject = makeHierarchyLevel(ELEM_CHAPTER, "2", "Kapitel 2");
		addChildren(chapterObject, sectionObject);

		JSONObject partObject = makeHierarchyLevel(ELEM_PART, "1c", "Teil 1c");
		addChildren(partObject, chapterObject);

		JSONObject bodyObject = makeHierarchyLevel(ELEM_BODY, "", "");
		addChildren(bodyObject, partObject);

		TraversableJSONDocument document = Mockito.mock(TraversableJSONDocument.class);
		when(document.getParent(any())).thenAnswer(invocation -> {
			JSONObject levelObject = invocation.getArgument(0);
			String type = getLocalName(getType(levelObject));

			switch (type) {
				case ELEM_SECTION:
					return chapterObject;
				case ELEM_CHAPTER:
					return partObject;
				default:
					return bodyObject;
			}
		});

		assertEquals("Teil 1c", getAenderungsstelleUebergeordneteGliederungseinheit(document, partObject));
		assertEquals("Teil 1c Kapitel 2", getAenderungsstelleUebergeordneteGliederungseinheit(document, chapterObject));
		assertEquals("Teil 1c Kapitel 2 Abschnitt 3a", getAenderungsstelleUebergeordneteGliederungseinheit(document, sectionObject));
	}


	private JSONObject makeHierarchyLevel(String type, String marker, String name) {
		JSONObject levelObject = makeNewDefaultJSONObject(type);
		JSONObject numObject = makeNumObject(marker, name);
		addChildren(levelObject, numObject);
		return levelObject;
	}


	@SuppressWarnings("unchecked")
	private void testForValidSlateValue(JSONObject jsonObject) {
		JSONArray children = getChildren(jsonObject);
		assertNotNull(children);
		assertFalse(children.isEmpty());

		if (children.size() == 1) {
			JSONObject child = (JSONObject) children.get(0);
			assertTrue(Utils.xor(child.containsKey(JSON_KEY_TEXT), child.containsKey(JSON_KEY_TYPE)));
			if (child.containsKey(JSON_KEY_TYPE)) {
				testForValidSlateValue(child);
			}
		} else {
			children.forEach(obj -> {
				assertInstanceOf(JSONObject.class, obj);
				JSONObject child = (JSONObject) obj;
				testForValidSlateValue(child);
			});
		}
	}


	private JSONObject getDocumentObject(String path) {
		String content = Utils.getContentOfTextResource(path);
		assertNotNull(content);
		assertFalse(content.isBlank());
		JSONObject resultObject = JSONUtils.getDocumentObject(content);
		assertNotNull(resultObject);
		return resultObject;
	}

}
