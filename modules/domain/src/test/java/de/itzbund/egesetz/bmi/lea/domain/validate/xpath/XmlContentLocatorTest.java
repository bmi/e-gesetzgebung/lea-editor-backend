// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.validate.xpath;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.xpath.XmlContentLocator;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Testsuite for {@link XmlContentLocator}.
 */
@Disabled("needs fix for inputs with nested inline elements per text line")
class XmlContentLocatorTest {

    @Test
    void testXmlContentLocatorInitialize() {
        InputStream inputStream = XmlContentLocatorTest.class.getResourceAsStream("/xpath/sample.xml");
        assertNotNull(inputStream);
        ReusableInputSource ris = new ReusableInputSource(inputStream);
        XmlContentLocator locator = new XmlContentLocator();
        locator.initialize(ris);

        assertEquals("/test", locator.getXPath(3));
        assertEquals("/test/ex:elem1[1]", locator.getXPath(5));
        assertEquals("/test/ex:elem1[1]/child1[1]", locator.getXPath(6));
        assertEquals("/test/ex:elem1[1]/ex:child1[1]", locator.getXPath(7));
        assertEquals("/test/ex:elem1[1]/ex:child1[2]", locator.getXPath(10));
        assertEquals("/test/elem2[1]", locator.getXPath(12));
        assertEquals("/test/elem3[1]", locator.getXPath(16));
    }


    @Test
    void testXmlContentLocatorComplex() {
        InputStream inputStream = XmlContentLocatorTest.class.getResourceAsStream(
            "/validationexamples/regelungsentwurf_03_regelungstext.xml");
        assertNotNull(inputStream);
        String xml = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))
            .lines().collect(Collectors.joining("\n"));
        assertNotNull(xml);
        assertFalse(xml.isBlank());
        StringReader input = new StringReader(xml);
        String prettyXml = Utils.getPrettyPrintedXml(input);
        assertNotNull(prettyXml);
        assertFalse(prettyXml.isBlank());

        ReusableInputSource ris = new ReusableInputSource(new ByteArrayInputStream(prettyXml.getBytes()));
        XmlContentLocator locator = new XmlContentLocator();
        locator.initialize(ris);

        assertEquals("/akn:akomaNtoso/akn:bill[1]", locator.getXPath(26));
        assertEquals("/akn:akomaNtoso/akn:bill[1]/akn:preface[1]/akn:longTitle[1]/akn:p[1]/akn:shortTitle[1]",
            locator.getXPath(74));
        //Ab hier wird /bill nicht erkannt
        assertEquals("/akn:akomaNtoso/akn:preamble[1]/akn:tblock[1]/akn:toc[1]/akn:tocItem[4]",
            locator.getXPath(103));
        assertEquals("/akn:akomaNtoso/akn:body[1]/akn:section[2]/akn:article[4]/akn:paragraph[1]/akn:list"
            + "[1]/akn:point[5]/akn:content[1]/akn:p[1]", locator.getXPath(557));
        assertEquals("/akn:akomaNtoso/akn:body[1]/akn:section[2]/akn:article[5]/akn:paragraph[4]/akn:num"
            + "[1]", locator.getXPath(709));
    }

}
