// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.util;

import java.nio.charset.StandardCharsets;
import java.util.Properties;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.SneakyThrows;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Class with static helper methods for integration tests.
 */
@SuppressWarnings("unused")
public class TestUtil {

    public static User getUserEntity(UserPersistencePort userRepository, String[] userData) {
        String guid = userData[UserFields.GUID.ordinal()];
        User userEntity = userRepository.findFirstByGid(new UserId(guid));
        if (userEntity == null) {
            userEntity = User.builder()
                .email(userData[UserFields.EMAIL.ordinal()])
                .gid(new UserId(guid))
                .name(userData[UserFields.NAME.ordinal()])
                .build();
        }
        return userEntity;
    }


    @SneakyThrows
    public static String createNewDocument(LeageSession session, MockMvc mockMvc,
        UserPersistencePort userRepository,
        Properties properties, String[] userData, String propName, String[]... replacements) {
        // check request JSON
        String jsonRequest = getRequestJson(properties, propName);
        if (replacements != null && replacements.length > 0) {
            jsonRequest = replaceBy(jsonRequest, replacements);
        }

        // send request to endpoint /document
        ResultActions result = mockMvc.perform(
            MockMvcRequestBuilders.post("/documents")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonRequest)
                .characterEncoding(StandardCharsets.UTF_8.name()));

        result.andExpect(status().is2xxSuccessful());
        String jsonResponse = result.andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);
        assertNotNull(jsonResponse);

        return jsonResponse;
    }


    public static String getRequestJson(Properties properties, String propName) {
        String jsonRequest = properties.getProperty(propName);
        assertNotNull(jsonRequest);
        assertFalse(jsonRequest.isBlank());
        return jsonRequest;
    }


    public static String replaceBy(String original, String[]... pairs) {
        if (pairs == null || pairs.length == 0) {
            return original;
        } else {
            String tmp = original;
            for (String[] pair : pairs) {
                if (pair.length > 1 && !pair[0].isBlank()) {
                    tmp = tmp.replace(pair[0], pair[1]);
                }
            }
            return tmp;
        }
    }


    public static User getNonexistingUser() {
        return User.builder()
            .email("Test.Nonexisting1@email.de")
            .gid(new UserId("someRandomId"))
            .name("TestVorname Testnachmane")
            .build();
    }


    private enum UserFields {
        GUID, NAME, EMAIL
    }
}
