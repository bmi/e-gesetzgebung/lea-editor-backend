// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.model.Template;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.TemplatePersistencePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static de.itzbund.egesetz.bmi.lea.core.Constants.TPL_NAME_BEGRUENDUNG;
import static de.itzbund.egesetz.bmi.lea.core.Constants.TPL_NAME_REGTEXT_MANTELFORM_0;
import static de.itzbund.egesetz.bmi.lea.core.Constants.TPL_NAME_REGTEXT_MANTELFORM_1;
import static de.itzbund.egesetz.bmi.lea.core.Constants.TPL_NAME_REGTEXT_MANTELFORM_2;
import static de.itzbund.egesetz.bmi.lea.core.Constants.TPL_NAME_REGTEXT_MANTELFORM_3;
import static de.itzbund.egesetz.bmi.lea.core.Constants.TPL_NAME_REGTEXT_STAMMFORM_0;
import static de.itzbund.egesetz.bmi.lea.core.Constants.TPL_NAME_REGTEXT_STAMMFORM_1;
import static de.itzbund.egesetz.bmi.lea.core.Constants.TPL_NAME_REGTEXT_STAMMFORM_2;
import static de.itzbund.egesetz.bmi.lea.core.Constants.TPL_NAME_REGTEXT_STAMMFORM_3;
import static de.itzbund.egesetz.bmi.lea.core.Constants.TPL_NAME_VORBLATT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class, SpringExtension.class})
@SuppressWarnings("unused")
class TemplateServiceTest {

    private static final String CNT_TPL_BEGRUENDUNG = TPL_NAME_BEGRUENDUNG;
    private static final String CNT_TPL_VORBLATT = TPL_NAME_VORBLATT;
    private static final String CNT_TPL_REGTEXT_STAMMFORM_0 = TPL_NAME_REGTEXT_STAMMFORM_0;
    private static final String CNT_TPL_REGTEXT_STAMMFORM_1 = TPL_NAME_REGTEXT_STAMMFORM_1;
    private static final String CNT_TPL_REGTEXT_STAMMFORM_2 = TPL_NAME_REGTEXT_STAMMFORM_2;
    private static final String CNT_TPL_REGTEXT_STAMMFORM_3 = TPL_NAME_REGTEXT_STAMMFORM_3;
    private static final String CNT_TPL_REGTEXT_MANTELFORM_0 = TPL_NAME_REGTEXT_MANTELFORM_0;
    private static final String CNT_TPL_REGTEXT_MANTELFORM_1 = TPL_NAME_REGTEXT_MANTELFORM_1;
    private static final String CNT_TPL_REGTEXT_MANTELFORM_2 = TPL_NAME_REGTEXT_MANTELFORM_2;
    private static final String CNT_TPL_REGTEXT_MANTELFORM_3 = TPL_NAME_REGTEXT_MANTELFORM_3;
    private static final List<String> tplNames = List.of(
        TPL_NAME_BEGRUENDUNG,
        TPL_NAME_VORBLATT,
        TPL_NAME_REGTEXT_STAMMFORM_0,
        TPL_NAME_REGTEXT_STAMMFORM_1,
        TPL_NAME_REGTEXT_STAMMFORM_2,
        TPL_NAME_REGTEXT_STAMMFORM_3,
        TPL_NAME_REGTEXT_MANTELFORM_0,
        TPL_NAME_REGTEXT_MANTELFORM_1,
        TPL_NAME_REGTEXT_MANTELFORM_2,
        TPL_NAME_REGTEXT_MANTELFORM_3
    );
    // ----- Services ---------
    // Also Mock this class, but also put the other Mocks in it
    @InjectMocks
    private TemplateService templateService;
    // ----- Repositories -----
    @MockBean
    private TemplatePersistencePort templateRepository;

    @BeforeEach
    void init() {
        for (String tplName : tplNames) {
            when(templateRepository.findAllByTemplateNameOrderByVersionDescUpdatedAtDesc(tplName)).thenReturn(
                List.of(
                    Template.builder()
                        .content(tplName)
                        .build()
                ));
        }
    }


    @Test
    void testLoadTemplate_whenVorblattRequested_thenReturnTemplateOfVorblatt() {
        String content = templateService.loadTemplate(DocumentType.VORBLATT_STAMMGESETZ, 0);
        assertEquals(TPL_NAME_VORBLATT, content);
    }


    @Test
    void testLoadTemplate_whenBegruendungRequested_thenReturnTemplateOfBegruendung() {
        String content = templateService.loadTemplate(DocumentType.BEGRUENDUNG_STAMMGESETZ, 0);
        assertEquals(TPL_NAME_BEGRUENDUNG, content);
    }


    @Test
    void testLoadTemplate_whenRegTextStammformRequestedwithLevel1_thenReturnTemplateOfRegTextStammform1() {
        String content = templateService.loadTemplate(DocumentType.REGELUNGSTEXT_STAMMGESETZ, 1);
        assertEquals(TPL_NAME_REGTEXT_STAMMFORM_1, content);
    }


    @Test
    void testLoadTemplate_whenRegTextMantelformRequestedwithLevel3_thenReturnTemplateOfRegTextMantelform0() {
        String content = templateService.loadTemplate(DocumentType.REGELUNGSTEXT_MANTELGESETZ, 3);
        assertEquals(TPL_NAME_REGTEXT_MANTELFORM_3, content);
    }

}
