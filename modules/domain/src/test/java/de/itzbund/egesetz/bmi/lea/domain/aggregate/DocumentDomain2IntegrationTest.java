// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.aggregate;

import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.services.CommentService;
import de.itzbund.egesetz.bmi.lea.domain.services.meta.MetadatenUpdateService;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import lombok.extern.log4j.Log4j2;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@SpringBootTest(classes = {Document.class})
@Log4j2
@SuppressWarnings("unused")
class DocumentDomain2IntegrationTest {

	private final static UUID SECOND_UUID = UUID.randomUUID();
	@Autowired
	private Document document;
	@MockBean
	private DokumentPersistencePort documentRepository;
	@MockBean
	private MetadatenUpdateService metadatenUpdateService;
	@MockBean
	private CommentService commentService;

	@BeforeEach
	public void setUp() {
		List<DocumentDomain> documentEntityList = new ArrayList<>();
		documentEntityList.add(TestObjectsUtil.getDocumentEntity());

		DocumentDomain documentEntity = TestObjectsUtil.getDocumentEntity();
		documentEntity.setDocumentId(new DocumentId(SECOND_UUID));
		documentEntityList.add(documentEntity);

		Mockito.when(documentRepository.findByCompoundDocumentId(Mockito.any(CompoundDocumentId.class)))
			.thenReturn(Optional.of(documentEntityList));
	}


	@Test
	void whenACompoundDocumentIsGiven_thenItsDocumentsShouldBeReturned() {
		List<Document> actual = document.getDocumentsForCompoundDocument(new CompoundDocumentId(UUID.randomUUID()),
			DocumentState.DRAFT);
		Assertions.assertThat(actual).asList().isNotEmpty().hasSize(2);
	}

}
