// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CreateCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.PkpZuleitungDokumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.enums.VerfahrensType;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.services.exceptions.DokumentenmappeNotFoundException;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class CompoundDocumentService1IntegrationTest extends AbstractCompoundDocumentServiceTest {

	@Test
	void whenValidCompoundDocument_thenSaveShouldBeSuccessful() {
		CompoundDocument compoundDocument = TestObjectsUtil.getRandomCompoundDocument();
		CompoundDocumentDTO actual = compoundDocumentService.save(compoundDocument);

		assertThat(actual)
			.hasFieldOrProperty("id")
			.isNotNull()
			.hasFieldOrPropertyWithValue("title", TestObjectsUtil.COMPOUND_DOCUMENT_TITLE)
			.hasFieldOrProperty("proposition")
			.hasFieldOrPropertyWithValue("type", CompoundDocumentTypeVariant.STAMMGESETZ)
			.hasFieldOrProperty("documents");
	}

	@Test
	void whenInValidCreateCompoundDocumentDTO_thenShowProblem() {
		List<ServiceState> status = new ArrayList<>();
		CreateCompoundDocumentDTO compoundDocument = CreateCompoundDocumentDTO.builder()
			.regelungsVorhabenId("IN_VALID_VALUE")
			.title(TITLE)
			.type(CompoundDocumentTypeVariant.STAMMGESETZ)
			.build();

		CompoundDocumentDTO actual = compoundDocumentService.createCompoundDocument(compoundDocument, status);

		assertThat(actual).isNull();
		assertThat(status.get(0)).isEqualByComparingTo(ServiceState.INVALID_ARGUMENTS);
	}

	@Test
	void whenAllCompoundDocumentAreRequestedAndUserIsValid_thenTheyShouldBeProvided() {
		// Arrange - Two compound documents with two documents each
		UUID secondDocumentId = UUID.randomUUID();
		CompoundDocument compoundDocument = TestObjectsUtil.getRandomCompoundDocument();
		CompoundDocument compoundDocument2 = TestObjectsUtil.getCompoundDocumentWithSpecificId(secondDocumentId);

		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = TestDTOUtil.getRegelungsvorhabenEditorDTOWithSpecificId(
			sampleUUID);

		// called by CompoundDocument::getCompoundDocumentsForUser
		when(compoundDocumentRepository.findByCreatedBy(TestObjectsUtil.getUserEntity()))
			.thenReturn(
				Arrays.asList(
					compoundDocument.getCompoundDocumentEntity(),
					compoundDocument2.getCompoundDocumentEntity()
				));

		// called by Document::getDocumentsForCompoundDocument
		when(documentRepository.findByCompoundDocumentId(any(CompoundDocumentId.class)))
			.thenAnswer(invocation ->
				{
					CompoundDocumentId compoundDocumentId = invocation.getArgument(0);

					List<Document> documents = compoundDocument.getDocuments();
					if (compoundDocumentId.equals(compoundDocument2.getCompoundDocumentId())) {
						documents = compoundDocument2.getDocuments();
					}

					List<DocumentDomain> documentEntities = documents.stream()
						.map(Document::getDocumentEntity)
						.collect(Collectors.toList());

					return Optional.of(documentEntities);
				}
			);

		// called by CompoundDocumentService::getRegelungsvorhabenEditorDTO
		when(plategRequestService.getRegelungsvorhabenEditorDTO(any(UUID.class)))
			.thenReturn(regelungsvorhabenEditorDTO);

		// Act
		List<CompoundDocument> actual = compoundDocumentService.getAllCompoundDocumentsRestrictedByAccessRights();

		// Assert
		assertThat(actual).asList().hasSize(0);
	}

	@Test
	void whenAllCompoundDocumentAreRequestedAndUserIsInValid_thenNothingShouldBeProvided() {
		when(session.getUser()).thenReturn(null);

		List<CompoundDocument> actual = compoundDocumentService.getAllCompoundDocumentsRestrictedByAccessRights();
		assertThat(actual).isEqualTo(Collections.emptyList());
	}

	@Test
	void whenASpecialCompoundDocumentIsRequestedAndUserIsValid_thenItShouldBeProvided() {
		// Arrange
		CompoundDocument expected = new CompoundDocument();
		expected.setCompoundDocumentEntity(compoundDocumentEntity);

		// aka proposition
		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO =
			TestDTOUtil.getRandomRegelungsvorhabenEditorDTO();

		when(compoundDocumentRepository.findByCreatedByAndCompoundDocumentId(any(User.class),
			any(CompoundDocumentId.class)))
			.thenReturn(Optional.of(compoundDocumentEntity));

		when(plategRequestService.getRegelungsvorhabenEditorDTO(any(UUID.class)))
			.thenReturn(
				regelungsvorhabenEditorDTO
			);

		// Act
		CompoundDocument actual = compoundDocumentService.getCompoundDocumentById(
			new CompoundDocumentId(sampleUUID), new ArrayList<>());

		// Assert
		assertThat(actual).isNotNull();
	}

	@Test
	void whenASpecialCompoundDocumentIsRequestedButNotAvailable_thenNothingShouldBeProvided() {
		// Arrange - override standard behaviour
		when(compoundDocumentRepository.findByCompoundDocumentId(any(CompoundDocumentId.class)))
			.thenReturn(Optional.empty());

		CompoundDocument actual = compoundDocumentService.getCompoundDocumentById(
			new CompoundDocumentId(UUID.randomUUID()), new ArrayList<>());
		assertThat(actual).isNull();
	}

	@Test
	void whenAValidCompoundDocumentIsGiven_aDocumentsShouldBeCreatedInCompoundDocument() {
		List<ServiceState> status = new ArrayList<>();

		Document randomDocument = TestObjectsUtil.getRandomDocumentOfType(DocumentType.ANLAGE);
		randomDocument.setDocumentRepository(documentRepository);
		CompoundDocumentDTO actual = compoundDocumentService.addDocument(new CompoundDocumentId(sampleUUID),
			randomDocument,
			status);
		assertThat(status.get(0)).isEqualByComparingTo(ServiceState.OK);
		assertThat(actual).isNotNull();
	}

	@Test
	void whenAnInValidCompoundDocumentIsGiven_ItShouldDetected() {
		// Arrange -- Override standard behaviour
		when(compoundDocumentRepository.findByCreatedByAndCompoundDocumentId(any(User.class),
			any(CompoundDocumentId.class)))
			.thenReturn(Optional.empty());

		List<ServiceState> status = new ArrayList<>();

		// Act
		CompoundDocumentDTO actual = compoundDocumentService.addDocument(
			new CompoundDocumentId(java.util.UUID.randomUUID()),
			TestObjectsUtil.getRandomDocument(), status);

		// Assert
		assertThat(status.get(0)).isEqualByComparingTo(ServiceState.DOCUMENT_DUPLICATION);
		assertThat(actual).isNull();
	}

	/**
	 * We test only one type as Validator is tested itself
	 */
	@Test
	void whenASecondDocumentShouldBeAdded_thenCompoundDocumentShouldGiveAnError() {
		// Arrange - create a compound document with a document
		List<ServiceState> status = new ArrayList<>();

		when(compoundDocumentRepository.findByCreatedByAndCompoundDocumentId(
			any(User.class),
			any(CompoundDocumentId.class)))
			.thenAnswer(invocation ->
				Optional.of(TestObjectsUtil.getRandomCompoundDocumentEntity())
			);

		when(documentRepository.findByCompoundDocumentId(
			any(CompoundDocumentId.class))).thenAnswer(
			invocation ->
			{
				List<DocumentDomain> documentEntityList = new ArrayList<>();
				documentEntityList.add(TestObjectsUtil.getDocumentEntity());
				return Optional.of(documentEntityList);
			}
		);

		when(documentRepository.findByCreatedByAndCompoundDocumentId(
			TestObjectsUtil.getUserEntity(), null))
			.thenAnswer(invocation ->
				{
					List<DocumentDomain> documentEntityList = new ArrayList<>();
					documentEntityList.add(TestObjectsUtil.getDocumentEntity());
					return documentEntityList;
				}
			);

		// Arrange - add another document of same type
		Document document = new Document(TestObjectsUtil.getDocumentEntity());
		document.setDocumentRepository(documentRepository);

		// Act
		compoundDocumentService.addDocument(new CompoundDocumentId(UUID.randomUUID()), document,
			status);

		// Assert
		assertThat(status.get(0)).isEqualTo(ServiceState.DOCUMENT_DUPLICATION);
	}

	@Test
	void whenAValidRvIdIsGiven_AListOfCompoundDocumentsShouldBeReturned() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();
		CompoundDocumentDomain testCompoundEntity = TestObjectsUtil.getCompoundDocumentEntity();

		List<CompoundDocumentDomain> compoundDocumentEntities = new ArrayList<>();
		compoundDocumentEntities.add(testCompoundEntity);

		CompoundDocumentSummaryDTO expected = compoundDocumentMapper.map(testCompoundEntity);

		when(compoundDocumentRepository.findByCreatedByAndRegelungsVorhabenId(
			any(),
			any()
		)).thenReturn(compoundDocumentEntities);

		// Act
		List<CompoundDocumentSummaryDTO> actual = compoundDocumentService.getCompoundDocumentsForRegelungsvorhaben(
			"someRandomId",
			sampleUUID,
			status);

		// Assert
		assertThat(actual).asList()
			.isNotNull()
			.hasSize(1)
			.contains(expected);
		assertThat(status).asList()
			.isNotNull()
			.hasSize(1)
			.contains(ServiceState.OK);
	}

	@Test
	void whenAValidIdIsGiven_ASingletonListWithCompoundDocumentIsReturned() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();
		CompoundDocumentDomain testCompoundEntity = TestObjectsUtil.getCompoundDocumentEntity();

		when(compoundDocumentRepository.findByCreatedByAndCompoundDocumentId(
			any(),
			any()
		)).thenReturn(Optional.of(testCompoundEntity));

		when(documentRepository.findByCompoundDocumentId(any()))
			.thenReturn(Optional.of(List.of(DocumentDomain.builder()
				.build())));

		//Act
		List<CompoundDocument> compoundDocuments = compoundDocumentService.getAllDocumentsFromCompoundDocuments("",
			List.of(testCompoundEntity.getCompoundDocumentId()
				.getId()
				.toString()), status);

		// Assert
		assertThat(compoundDocuments).asList()
			.isNotNull()
			.hasSize(1);
	}

	@Test
	void whenAnInValidRvIdIsGiven_AnEmptyListOfCompoundDocumentsShouldBeReturned() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();
		List<CompoundDocumentDomain> compoundDocuments = new ArrayList<>();

		when(compoundDocumentRepository
			.findByCreatedByAndRegelungsVorhabenId(any(User.class),
				any(RegelungsVorhabenId.class)))
			.thenReturn(compoundDocuments);

		// Act
		List<CompoundDocumentSummaryDTO> actual = compoundDocumentService.getCompoundDocumentsForRegelungsvorhaben(
			"123",
			sampleUUID,
			status);

		// Assert
		assertThat(actual).asList()
			.isNotNull()
			.isEmpty();
		assertThat(status).asList()
			.isNotNull()
			.hasSize(1)
			.contains(ServiceState.OK);
	}

	@Test
	void whenInvalidCompoundDocumentsAreGiven_AnEmptyListShouldBeProvided() {
		String gid = "";
		List<String> compoundDocumentIds = new ArrayList<>();
		List<ServiceState> status = new ArrayList<>();

		List<CompoundDocument> actual = compoundDocumentService.getAllDocumentsFromCompoundDocuments(gid,
			compoundDocumentIds, status);

		assertThat(actual).isNotNull()
			.asList()
			.isEmpty();
	}

	@Test
	void when_UserIsParticipant_then_ReturnListOfDocuments() {
		// Arrange
		User creator = User.builder()
			.gid(new UserId("creator"))
			.build();

		User participant = User.builder()
			.gid(new UserId("participant"))
			.build();

		assertNotEquals(creator, participant);

		UUID dmId = UUID.randomUUID();
		UUID docId1 = UUID.randomUUID();
		UUID docId2 = UUID.randomUUID();
		UUID docId3 = UUID.randomUUID();
		String version = Utils.getRandomVersion();
		CompoundDocumentId compoundDocumentId = new CompoundDocumentId(dmId);

		when(session.getUser()).thenReturn(participant);
		when(compoundDocumentRepository.findByCompoundDocumentId(compoundDocumentId))
			.thenReturn(Optional.of(CompoundDocumentDomain.builder()
				.compoundDocumentId(compoundDocumentId)
				.createdBy(creator)
				.updatedBy(creator)
				.state(DocumentState.FREEZE)
				.version(version)
				.build()));
		when(plategRequestService.isUserParticipantForCompoundDocument(compoundDocumentId))
			.thenReturn(true);
		when(documentRepository.findByCompoundDocumentId(compoundDocumentId))
			.thenReturn(Optional.of(List.of(
				DocumentDomain.builder()
					.documentId(new DocumentId(docId1))
					.build(),
				DocumentDomain.builder()
					.documentId(new DocumentId(docId2))
					.build(),
				DocumentDomain.builder()
					.documentId(new DocumentId(docId3))
					.build()
			)));

		List<ServiceState> status = new ArrayList<>();

		// Act
		CompoundDocumentSummaryDTO compoundDocumentSummaryDTO =
			compoundDocumentService.getDocumentsList(compoundDocumentId, status);

		// Assert
		assertNotNull(compoundDocumentSummaryDTO);
		assertEquals(1, status.size());
		assertEquals(ServiceState.OK, status.get(0));
		assertEquals(version, compoundDocumentSummaryDTO.getVersion());

		assertNotNull(compoundDocumentSummaryDTO.getDocuments());
		List<DocumentSummaryDTO> docs = compoundDocumentSummaryDTO.getDocuments();
		assertEquals(3, docs.size());

		checkId(docId1, docs.get(0));
		checkId(docId2, docs.get(1));
		checkId(docId3, docs.get(2));
		docs.forEach(d -> {
			assertEquals(DocumentState.FREEZE, d.getState());
			assertEquals(version, d.getVersion());
		});
	}

	private void checkId(UUID expectedId, DocumentSummaryDTO doc) {
		assertEquals(expectedId, doc.getId());
	}

	@Test
	void testRename_whenCompoundDocumentNotFound_thenFailure() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();
		CompoundDocumentId compoundDocumentId = new CompoundDocumentId(UUID.randomUUID());

		when(compoundDocumentRepository.findByCompoundDocumentId(any())).thenReturn(Optional.empty());

		// Act
		compoundDocumentService.update(compoundDocumentId, Utils.getRandomString(), VerfahrensType.REFERENTENENTWURF,
			DocumentState.DRAFT, status);

		// Assert
		assertEquals(1, status.size());
		assertEquals(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND, status.get(0));
	}

	@Test
	void testRename_whenStatusNotDraft_thenFailure_NoPermission() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();
		CompoundDocumentId compoundDocumentId = new CompoundDocumentId(UUID.randomUUID());

		when(compoundDocumentRepository.findByCompoundDocumentId(any())).thenReturn(Optional.of(
			CompoundDocumentDomain.builder()
				.state(DocumentState.FREEZE)
				.build()
		));

		// Act
		compoundDocumentService.update(compoundDocumentId, Utils.getRandomString(), VerfahrensType.REFERENTENENTWURF,
			DocumentState.FREEZE, status);

		// Assert
		assertEquals(1, status.size());
		assertEquals(ServiceState.NO_PERMISSION, status.get(0));
	}

	@Test
	void testRename_whenUserNotOwner_thenFailure_NoPermission() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();
		CompoundDocumentId compoundDocumentId = new CompoundDocumentId(UUID.randomUUID());

		when(compoundDocumentRepository.findByCompoundDocumentId(any())).thenReturn(Optional.of(
			CompoundDocumentDomain.builder()
				.state(DocumentState.DRAFT)
				.createdBy(getRandomUser())
				.build()
		));
		when(session.getUser()).thenReturn(getRandomUser());
		when(documentPermissionValidator.isUserOwnerOfCompoundDocumentEntity(any())).thenReturn(false);

		// Act
		compoundDocumentService.update(compoundDocumentId, Utils.getRandomString(), VerfahrensType.REFERENTENENTWURF,
			DocumentState.DRAFT, status);

		// Assert
		assertEquals(1, status.size());
		assertEquals(ServiceState.NO_PERMISSION, status.get(0));
	}

	@Test
	void testRename_whenCompoundDocumentIsRenamed_thenSuccess() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();
		CompoundDocumentId compoundDocumentId = new CompoundDocumentId(UUID.randomUUID());
		String newTitle = Utils.getRandomString();

		User userEntity = getRandomUser();

		CompoundDocumentDomain compoundDocumentEntity = CompoundDocumentDomain.builder()
			.state(DocumentState.DRAFT)
			.createdBy(userEntity)
			.build();

		when(compoundDocumentRepository.findByCompoundDocumentId(any())).thenReturn(Optional.of(
			compoundDocumentEntity
		));
		when(session.getUser()).thenReturn(userEntity);
		when(documentPermissionValidator.isUserOwnerOfCompoundDocumentEntity(any())).thenReturn(true);

		// Act
		compoundDocumentService.update(compoundDocumentId, newTitle, VerfahrensType.REFERENTENENTWURF,
			DocumentState.DRAFT, status);

		// Assert
		assertEquals(1, status.size());
		assertEquals(ServiceState.OK, status.get(0));
		assertEquals(newTitle, compoundDocumentEntity.getTitle());
	}

	private User getRandomUser() {
		return User.builder()
			.gid(new UserId(UUID.randomUUID()
				.toString()))
			.build();
	}

	@Test
	void whenCompoundDocumentWithInvalidIdShouldBeCopied_AnErrorShouldOccure() {
		// No compound document found - override standard
		when(compoundDocumentRepository.findByCompoundDocumentId(
			any(CompoundDocumentId.class)))
			.thenReturn(Optional.empty());

		List<ServiceState> status = new ArrayList<>();

		assertThrows(DokumentenmappeNotFoundException.class, () -> compoundDocumentService.copyCompoundDocument(new CompoundDocumentId(UUID.randomUUID()),
			TITLE, Collections.emptyList(), status));

		assertThat(status.get(0)).isEqualTo(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
	}

	@Test
	void whenCompoundDocumentIncreaseVersionWithDocumentCopy_ItShouldBePossible() {

		// Arrange
		Document randomDocument1 = TestObjectsUtil.getRandomDocument();
		Document randomDocument2 = TestObjectsUtil.getRandomDocument();
		Document randomDocument3 = TestObjectsUtil.getRandomDocument();

		when(documentRepository.findByDocumentId(any(DocumentId.class)))
			.thenReturn(Optional.of(TestObjectsUtil.getDocumentEntity()));

		when(documentRepository.findByCompoundDocumentId(any(CompoundDocumentId.class)))
			.thenReturn(Optional.of(List.of(
				randomDocument1.getDocumentEntity(),
				randomDocument2.getDocumentEntity(),
				randomDocument3.getDocumentEntity()
			)));

		when(documentRepository.findByDocumentId(any(DocumentId.class)))
			.thenAnswer(invocation ->
				{
					DocumentId docid = invocation.getArgument(0);
					DocumentDomain entity = randomDocument3.getDocumentEntity();

					if (docid.equals(randomDocument1.getDocumentEntity()
						.getDocumentId())) {
						entity = randomDocument1.getDocumentEntity();
					} else if (docid.equals(randomDocument2.getDocumentEntity()
						.getDocumentId())) {
						entity = randomDocument2.getDocumentEntity();
					}

					return Optional.of(entity);
				}
			);

		CompoundDocumentId compoundDocumentId = new CompoundDocumentId(UUID.randomUUID());
		String newTitle = "X";
		List<DocumentId> documentIds = Arrays.asList(
			randomDocument1.getDocumentEntity()
				.getDocumentId(),
			randomDocument2.getDocumentEntity()
				.getDocumentId(),
			randomDocument3.getDocumentEntity()
				.getDocumentId()
		);
		List<ServiceState> status = new ArrayList<>();

		// Act
		CompoundDocumentSummaryDTO compoundDocumentSummaryDTO = compoundDocumentService.copyCompoundDocument(
			compoundDocumentId,
			newTitle,
			documentIds, status);

		assertThat(status.get(0)).isEqualTo(ServiceState.OK);
		assertThat(compoundDocumentSummaryDTO)
			.hasFieldOrProperty("documents")
			.hasFieldOrPropertyWithValue("version", "13.0")
			.hasFieldOrPropertyWithValue("title", newTitle)
			.hasNoNullFieldsOrPropertiesExcept("id", "documentPermissions", "commentPermissions");

		assertThat(compoundDocumentSummaryDTO.getDocuments()).asList()
			.hasSize(3);
		verify(commentService, atLeastOnce()).deleteAllCommentWrappers(any());
		verify(commentService, atLeastOnce()).deleteAllCommentAttributes(any());
	}

	@Test
	void whenCompoundDocumentStatusIsUpdatedByEditor_OnlyOneKabinettverfahrenShouldBePossible() {
		// Arrange

		// Mocks:
		// - CompoundDocumentRepository::findByCreatedByAndCompoundDocumentId(User, CompoundDocumentId)
		// - CompoundDocumentRepository::findByRegelungsVorhabenIdAndCompoundDocumentState(RegelungsVorhabenId,
		// DocumentState.BEREIT_FUER_KABINETT)

		List<ServiceState> status = new ArrayList<>();

		// Act
		CompoundDocumentDomain actual = compoundDocumentService.changeDocumentStateByEditor(compoundDocumentEntity,
			DocumentState.BEREIT_FUER_KABINETT, status);

		// Assert
		assertThat(status.get(0)).isEqualTo(ServiceState.ONLY_ONE_KABINETT_VOTING);
		assertNotNull(actual);
		assertNotNull(actual.getState());
		assertEquals(DocumentState.BEREIT_FUER_KABINETT, actual.getState());
	}

	@Test
	void whenCompoundDocumentForAnAbstimmungIsRequested_TheyShouldBeProvided() {
		// Arrange

		// Mocks:
		// - CompoundDocumentRepository::findByCreatedBy(User)

		List<ServiceState> status = new ArrayList<>();
		User user = compoundDocumentEntity.getCreatedBy();
		RegelungsVorhabenId regelungsVorhabenId = compoundDocumentEntity.getRegelungsVorhabenId();

		List<CompoundDocumentSummaryDTO> actual = compoundDocumentService.getCompoundDocumentsForAbstimmung(
			user.getGid()
				.getId(), regelungsVorhabenId, null, null, status);

		assertThat(actual).hasSize(2);
		assertThat(actual.get(0)).hasNoNullFieldsOrPropertiesExcept("documentPermissions", "commentPermissions");
		assertThat(status.get(0)).isEqualTo(ServiceState.OK);
	}

	@Test
	void whenCompoundDocumentForKabinettverfahrenIsRequested_ItShouldBeProvided() {
		// Arrange

		// Mocks:
		// - CompoundDocumentRepository::findByCreatedByAndRegelungsvorhabenIdAndState(User, RegelungsVorhabenId,
		// DocumentState)
		// - DocumentRepository::findByCompoundDocumentId(CompoundDocumentId)
		compoundDocumentEntity.setState(DocumentState.BEREIT_FUER_KABINETT);

		List<ServiceState> status = new ArrayList<>();
		User user = compoundDocumentEntity.getCreatedBy();
		RegelungsVorhabenId regelungsVorhabenId = compoundDocumentEntity.getRegelungsVorhabenId();

		List<PkpZuleitungDokumentDTO> actual = compoundDocumentService.getCompoundDocumentsForKabinett(
			user.getGid()
				.getId(), regelungsVorhabenId, status);

		assertThat(actual).hasSize(1);
		assertThat(actual.get(0)).hasNoNullFieldsOrProperties();
		assertThat(status.get(0)).isEqualTo(ServiceState.OK);
	}

	@Test
	void testGetDokumentenmappenFuerEinRegelungsvorhabenMitStatus() {
		// Arrange
		// Mocks:
		// - CompoundDocumentRepository::findByCreatedByAndRegelungsvorhabenIdAndState(User, RegelungsVorhabenId, DocumentState)
		compoundDocumentEntity.setState(DocumentState.BEREIT_FUER_KABINETT);

		// Act
		List<CompoundDocument> actual = compoundDocumentService.getDokumentenmappenFuerEinRegelungsvorhabenMitStatus(
			TestObjectsUtil.getUserEntity(),
			compoundDocumentEntity.getRegelungsVorhabenId(),
			DocumentState.BEREIT_FUER_KABINETT);

		//Assert
		assertThat(actual).isNotEmpty().hasSize(1);
	}

}
