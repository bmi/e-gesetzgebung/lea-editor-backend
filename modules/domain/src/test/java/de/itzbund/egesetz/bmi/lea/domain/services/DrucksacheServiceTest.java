// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.Drucksache;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DrucksacheDokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DrucksachenPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {DrucksacheService.class})
public class DrucksacheServiceTest {

    // ----- Services ---------
    @Autowired
    protected DrucksacheService drucksacheService;

    @MockBean
    protected CompoundDocument compoundDocument;

    // ----- Repositories -----
    @MockBean
    protected DrucksachenPersistencePort drucksachenPersistencePort;
    @MockBean
    protected DrucksacheDokumentPersistencePort drucksacheDokumentPersistencePort;

    private final static String DRUCKSACHEN_NR = "123/2004";
    private final static RegelungsVorhabenId REGELUNGSVORHABEN_ID = new RegelungsVorhabenId(UUID.randomUUID());

    private final Drucksache drucksache = Drucksache.builder()
        .drucksachenNr(DRUCKSACHEN_NR)
        .regelungsVorhabenId(REGELUNGSVORHABEN_ID)
        .build();

    @BeforeEach
    void init() {
        MockitoAnnotations.openMocks(this);

        when(drucksachenPersistencePort.save(drucksache)).thenReturn(drucksache);
    }

    @Test
    void testDrucksacheEintragenAlsNeuerWert() {
        // Arrange
        CompoundDocument randomCompoundDocument = TestObjectsUtil.getRandomCompoundDocument();
        randomCompoundDocument.getCompoundDocumentEntity().setState(DocumentState.ZUGESTELLT_BUNDESRAT);
        when(compoundDocument.getCompoundDocumentsOptionalFilteredByUserUndRegelungsvorhaben(isNull(), anyList()))
            .thenReturn(List.of(randomCompoundDocument));

        // Act
        List<ServiceState> actual = drucksacheService.drucksachenNummerAnlegen(REGELUNGSVORHABEN_ID, DRUCKSACHEN_NR);

        // Assert
        assertThat(actual.get(0)).isEqualByComparingTo(ServiceState.OK);
    }

    @Test
    void testDrucksacheEintragenOhneRichtigenDMStatus() {
        // Arrange
        CompoundDocument randomCompoundDocument = TestObjectsUtil.getRandomCompoundDocument();
        when(compoundDocument.getCompoundDocumentsOptionalFilteredByUserUndRegelungsvorhaben(isNull(), anyList()))
            .thenReturn(List.of(randomCompoundDocument));

        // Act
        List<ServiceState> actual = drucksacheService.drucksachenNummerAnlegen(REGELUNGSVORHABEN_ID, DRUCKSACHEN_NR);

        // Assert
        assertThat(actual.get(0)).isEqualByComparingTo(ServiceState.FEHLENDER_STATUS_BUNDESRAT);
    }

    @Test
    void testDrucksacheEintragenFuerRegelungsvorhabenGibtEsSchonEineDrucksache() {
        // Arrange
        when(drucksachenPersistencePort.findByRegelungsvorhabenId(any(RegelungsVorhabenId.class))).thenReturn(Drucksache.builder().build());

        // Act
        List<ServiceState> actual = drucksacheService.drucksachenNummerAnlegen(REGELUNGSVORHABEN_ID, DRUCKSACHEN_NR);

        // Assert
        assertThat(actual.get(0)).isEqualByComparingTo(ServiceState.DRUCKSACHE_FUER_REGELUNGSVORHABEN_VORHANDEN);
    }

    @Test
    void testDrucksachenNummerExistiertBereits() {
        // Arrange
        when(drucksachenPersistencePort.findByDrucksachenNr(anyString())).thenReturn(Drucksache.builder().build());

        // Act
        List<ServiceState> actual = drucksacheService.drucksachenNummerAnlegen(REGELUNGSVORHABEN_ID, DRUCKSACHEN_NR);

        // Assert
        assertThat(actual.get(0)).isEqualByComparingTo(ServiceState.DRUCKSACHENNUMMER_BEREITS_VERGEBEN);
    }


}
