// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.egfa.datenuebernahme;

import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class EgfaServiceUnitTest {

	private EgfaService egfaService;


	@BeforeEach
	void setUp() {
		egfaService = new EgfaService(null, null, null, null, null, null, null, null);
	}


	@Test
	void isImplementedEgfaModule() {
		var data = EgfaDatenuebernahmeDTO.builder()
			.id(EgfaId.ERFUELLUNGSAUFWAND_ERGEBNISDOKUMENTATION.getLiteral())
			.build();
		assertTrue(egfaService.isValidEgfaModule(data));

		data = EgfaDatenuebernahmeDTO.builder()
			.id(EgfaId.ENAP_ERGEBNISDOKUMENTATION.getLiteral())
			.build();
		assertTrue(egfaService.isValidEgfaModule(data));
	}


	@ParameterizedTest
	@NullAndEmptySource
	@ValueSource(strings = {"xyz", "", "   "})
	void isNotImplementedEgfaModule(String id) {
		var data = EgfaDatenuebernahmeDTO.builder()
			.id(id)
			.build();
		assertFalse(egfaService.isValidEgfaModule(data));
	}

}