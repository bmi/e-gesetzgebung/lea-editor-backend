// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.change.AenderungsbefehlBerechnung;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.PermissionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.synopsis.SynopsisRequestDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.synopsis.SynopsisResponseDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.mappers.DocumentMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.UserMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.UserMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.BestandsrechtPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.EIdGeneratorRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.TemplateRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.UserRestPort;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.EIdGeneratorService;
import de.itzbund.egesetz.bmi.lea.domain.services.filters.EIdGeneratorFilter;
import de.itzbund.egesetz.bmi.lea.domain.services.synopse.SynopsisService;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.core.util.Utils.setReflectiveAttribute;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {SynopsisService.class, UserMapperImpl.class, EIdGeneratorService.class, TemplateService.class})
@SuppressWarnings("unused")
class SynopsisServiceTest {

    @Autowired
    private SynopsisService synopsisService;
    @Autowired
    private EIdGeneratorRestPort eIdGeneratorRestPort;
    @Autowired
    private UserMapper userMapper;

    @MockBean
    private DocumentService documentService;
    @MockBean
    private CompoundDocumentService compoundDocumentService;
    @MockBean
    private RegelungsvorhabenService regelungsvorhabenService;
    @MockBean
    private DokumentPersistencePort dokumentPersistencePort;
    @MockBean
    private UserPersistencePort userPersistencePort;
    @MockBean
    private TemplateRestPort templateRestPort;
    @MockBean
    private DocumentMapper documentMapper;
    @MockBean
    private PropositionMapper propositionMapper;
    @MockBean
    private LeageSession session;
    @MockBean
    private EIdGeneratorFilter eIdGeneratorFilter;
    @MockBean
    private UserRestPort userService;
    @MockBean
    private AenderungsbefehlBerechnung aenderungsbefehlBerechnung;
    @MockBean
    private BestandsrechtPersistencePort bestandsrechtRepository;


    private User testUser;


    @BeforeEach
    void init() {
        testUser = User.builder()
            .gid(new UserId(UUID.randomUUID()
                .toString()))
            .name(Utils.getRandomString())
            .email(Utils.getRandomString())
            .build();
    }


    @Test
    void whenBaseIsMissing_thenValidationError() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        SynopsisRequestDTO synopsisRequestDTO = new SynopsisRequestDTO();

        // Act
        SynopsisResponseDTO synopsisResponseDTO = synopsisService.getSynopse(synopsisRequestDTO, status);

        // Assert
        assertNull(synopsisResponseDTO);
        assertEquals(1, status.size());
        assertEquals(ServiceState.INVALID_ARGUMENTS, status.get(0));
    }


    @Test
    void whenVersionIsMissing_thenValidationError() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        SynopsisRequestDTO synopsisRequestDTO = new SynopsisRequestDTO()
            .base(UUID.randomUUID());

        // Act
        SynopsisResponseDTO synopsisResponseDTO = synopsisService.getSynopse(synopsisRequestDTO, status);

        // Assert
        assertNull(synopsisResponseDTO);
        assertEquals(1, status.size());
        assertEquals(ServiceState.INVALID_ARGUMENTS, status.get(0));
    }


    @Test
    void whenBaseDocumentNotFound_thenValidationError() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        SynopsisRequestDTO synopsisRequestDTO = makeSynopsisRequestDTO();

        when(dokumentPersistencePort.findByDocumentId(any())).thenAnswer(docId -> Optional.empty());

        // Act
        SynopsisResponseDTO synopsisResponseDTO = synopsisService.getSynopse(synopsisRequestDTO, status);

        // Assert
        assertNull(synopsisResponseDTO);
        assertEquals(1, status.size());
        assertEquals(ServiceState.DOCUMENT_NOT_FOUND, status.get(0));
    }


    @Test
    void whenVersionDocumentNotFound_thenValidationError() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        UUID baseId = UUID.randomUUID();
        UUID versionId = UUID.randomUUID();

        SynopsisRequestDTO synopsisRequestDTO = new SynopsisRequestDTO()
            .base(baseId)
            .addVersionsItem(versionId)
            .addVersionsItem(UUID.randomUUID());

        when(dokumentPersistencePort.findByDocumentId(any())).thenAnswer(invocation -> {
            DocumentId documentId = invocation.getArgument(0);
            if (baseId.equals(documentId.getId())) {
                return Optional.of(DocumentDomain.builder()
                    .build());
            } else {
                return Optional.empty();
            }
        });

        // Act
        SynopsisResponseDTO synopsisResponseDTO = synopsisService.getSynopse(synopsisRequestDTO, status);

        // Assert
        assertNull(synopsisResponseDTO);
        assertEquals(1, status.size());
        assertEquals(ServiceState.DOCUMENT_NOT_FOUND, status.get(0));
    }


    @Test
    void whenCompoundDocumentNotFound_thenValidationError() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        SynopsisRequestDTO synopsisRequestDTO = makeSynopsisRequestDTO();

        when(dokumentPersistencePort.findByDocumentId(any())).thenAnswer(
            invocation -> Optional.of(DocumentDomain.builder()
                .build()));

        when(compoundDocumentService.getCompoundDocumentById(any(), any())).thenReturn(null);

        // Act
        SynopsisResponseDTO synopsisResponseDTO = synopsisService.getSynopse(synopsisRequestDTO, status);

        // Assert
        assertNull(synopsisResponseDTO);
        assertEquals(1, status.size());
        assertEquals(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND, status.get(0));
    }


    @Test
    void whenUserHasNoPermission_thenAccessDenied() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        SynopsisRequestDTO synopsisRequestDTO = makeSynopsisRequestDTO();
        CompoundDocument compoundDocument = makeCompoundDocument(User.builder()
            .gid(new UserId(UUID.randomUUID()
                .toString()))
            .build());

        when(session.getUser()).thenReturn(testUser);

        when(dokumentPersistencePort.findByDocumentId(any())).thenAnswer(
            invocation -> Optional.of(DocumentDomain.builder()
                .build()));

        when(compoundDocumentService.getCompoundDocumentById(any(), any())).thenAnswer(invocation -> {
            List<ServiceState> states = invocation.getArgument(1);
            states.add(ServiceState.OK);
            return compoundDocument;
        });

        when(regelungsvorhabenService.isUserParticipantForCompoundDocument(any())).thenReturn(false);

        // Act
        SynopsisResponseDTO synopsisResponseDTO = synopsisService.getSynopse(synopsisRequestDTO, status);

        // Assert
        assertNull(synopsisResponseDTO);
        assertEquals(1, status.size());
        assertEquals(ServiceState.NO_PERMISSION, status.get(0));
    }


    @Test
    void whenRequestIsValid_and_UserHasPermission_thenSuccess() {
        // Arrange
        UUID baseId = UUID.fromString("01000000-0000-0000-0000-000000000000");
        UUID versionId = UUID.fromString("02000000-0000-0000-0000-000000000000");
        List<ServiceState> status = new ArrayList<>();
        SynopsisRequestDTO synopsisRequestDTO = new SynopsisRequestDTO()
            .base(baseId)
            .addVersionsItem(versionId);
        CompoundDocument compoundDocument = makeCompoundDocument(testUser);

        when(session.getUser()).thenReturn(testUser);

        when(dokumentPersistencePort.findByDocumentId(any())).thenAnswer(
            invocation -> {
                DocumentId documentId = invocation.getArgument(0);
                String content;
                Instant timestamp = Instant.now();

                if (baseId.toString().equals(documentId.getId().toString())) {
                    content = Utils.getContentOfTextResource("/compare/Regelungstext-V1.json");
                    timestamp = timestamp.minus(Duration.ofDays(2));
                } else {
                    content = Utils.getContentOfTextResource("/compare/Regelungstext-V2.json");
                }

                return Optional.of(DocumentDomain.builder()
                    .content(content)
                    .type(DocumentType.REGELUNGSTEXT_STAMMGESETZ)
                    .updatedAt(timestamp)
                    .build());
            });

        when(compoundDocumentService.getCompoundDocumentById(any(), any())).thenAnswer(invocation -> {
            List<ServiceState> states = invocation.getArgument(1);
            states.add(ServiceState.OK);
            return compoundDocument;
        });

        when(regelungsvorhabenService.isUserParticipantForCompoundDocument(any())).thenReturn(true);

        when(documentMapper.mapToDocumentDTO(any(), any())).thenReturn(DocumentDTO.builder()
            .build());

        when(documentService.enrichDocumentDTOWithPermissions(any())).thenAnswer(invocation -> {
            DocumentDTO documentDTO = invocation.getArgument(0);
            documentDTO.setDocumentPermissions(PermissionDTO.builder()
                .hasRead(true)
                .hasWrite(false)
                .build());
            documentDTO.setCommentPermissions(PermissionDTO.builder()
                .hasRead(true)
                .hasWrite(false)
                .build());
            return documentDTO;
        });

        when(templateRestPort.loadTemplate(DocumentType.SYNOPSE, 0)).thenReturn(
            Utils.getContentOfTextResource("/compare/synopse-template.json"));

        setReflectiveAttribute(eIdGeneratorFilter, "eIdGeneratorService", eIdGeneratorRestPort);

        // Act
        SynopsisResponseDTO synopsisResponseDTO = synopsisService.getSynopse(synopsisRequestDTO, status);

        // Assert
        assertNotNull(synopsisResponseDTO);
        assertEquals(1, status.size());
        assertEquals(ServiceState.OK, status.get(0));

        assertEquals("SYNOPSE", synopsisResponseDTO.getType()
            .name());
        assertEquals(userMapper.map(testUser), synopsisResponseDTO.getErsteller());
    }


    @Test
    void whenInputNoRegulatoryText_thenFailure_InvalidArguments() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        SynopsisRequestDTO synopsisRequestDTO = makeSynopsisRequestDTO();
        CompoundDocument compoundDocument = makeCompoundDocument(testUser);

        when(session.getUser()).thenReturn(testUser);

        when(dokumentPersistencePort.findByDocumentId(any())).thenAnswer(
            invocation -> Optional.of(DocumentDomain.builder()
                .content(Utils.getContentOfTextResource(
                    "/compare/20230420_1234_Export_Anschreiben-Stammgesetz_AN-SG.json"
                ))
                .type(DocumentType.ANSCHREIBEN_STAMMGESETZ)
                .build()));

        when(compoundDocumentService.getCompoundDocumentById(any(), any())).thenAnswer(invocation -> {
            List<ServiceState> states = invocation.getArgument(1);
            states.add(ServiceState.OK);
            return compoundDocument;
        });

        when(regelungsvorhabenService.isUserParticipantForCompoundDocument(any())).thenReturn(true);

        when(documentMapper.mapToDocumentDTO(any(), any())).thenReturn(DocumentDTO.builder()
            .build());

        // Act
        SynopsisResponseDTO synopsisResponseDTO = synopsisService.getSynopse(synopsisRequestDTO, status);

        // Assert
        assertNull(synopsisResponseDTO);
        assertEquals(1, status.size());
        assertEquals(ServiceState.INVALID_ARGUMENTS, status.get(0));
    }

    @Test
    void whenDocsAreEqual_thenSuccess() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        UUID baseId = UUID.randomUUID();
        UUID versionId = UUID.randomUUID();
        String content = Utils.getContentOfTextResource("/compare/Regelungstext-V1.json");
        assertNotNull(content);

        SynopsisRequestDTO synopsisRequestDTO = new SynopsisRequestDTO()
            .base(baseId)
            .addVersionsItem(versionId);

        when(session.getUser()).thenReturn(testUser);

        when(dokumentPersistencePort.findByDocumentId(any())).thenAnswer(invocation ->
            Optional.of(DocumentDomain.builder()
                .type(DocumentType.REGELUNGSTEXT_STAMMGESETZ)
                .content(content)
                .updatedAt(Instant.now())
                .build()));

        when(compoundDocumentService.getCompoundDocumentById(any(), any())).thenAnswer(invocation -> {
            List<ServiceState> states = invocation.getArgument(1);
            states.add(ServiceState.OK);
            return makeCompoundDocument(testUser);
        });

        when(documentService.enrichDocumentDTOWithPermissions(any())).thenAnswer(invocation -> invocation.getArgument(0));

        when(regelungsvorhabenService.isUserParticipantForCompoundDocument(any())).thenReturn(true);

        when(documentMapper.mapToDocumentDTO(any(), any())).thenReturn(DocumentDTO.builder()
            .build());

        when(templateRestPort.loadTemplate(DocumentType.SYNOPSE, 0)).thenReturn(
            Utils.getContentOfTextResource("/compare/synopse-template.json"));

        setReflectiveAttribute(eIdGeneratorFilter, "eIdGeneratorService", eIdGeneratorRestPort);

        // Act
        SynopsisResponseDTO synopsisResponseDTO = synopsisService.getSynopse(synopsisRequestDTO, status);

        // Assert
        assertNotNull(synopsisResponseDTO);
        assertEquals(1, status.size());
        assertEquals(ServiceState.OK, status.get(0));
    }


    private SynopsisRequestDTO makeSynopsisRequestDTO() {
        return new SynopsisRequestDTO()
            .base(UUID.randomUUID())
            .addVersionsItem(UUID.randomUUID())
            .addVersionsItem(UUID.randomUUID());
    }


    private CompoundDocument makeCompoundDocument(User user) {
        CompoundDocument compoundDocument = new CompoundDocument();
        compoundDocument.setCompoundDocumentEntity(CompoundDocumentDomain.builder()
            .createdBy(user)
            .build());
        compoundDocument.setPlategRequestService(regelungsvorhabenService);
        return compoundDocument;
    }

}
