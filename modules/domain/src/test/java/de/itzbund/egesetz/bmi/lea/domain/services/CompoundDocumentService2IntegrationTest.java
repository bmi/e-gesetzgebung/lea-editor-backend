// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CreateCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
class CompoundDocumentService2IntegrationTest extends AbstractCompoundDocumentServiceTest {

    private static final String RV_ID = "00000000-0000-0000-0000-000000000005";

    private static Stream<Arguments> testParams() {
        return Stream.of(
            Arguments.of(makeCreateCompoundDocumentDTO(CompoundDocumentTypeVariant.AENDERUNGSVERORDNUNG, ""),
                DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG),
            Arguments.of(makeCreateCompoundDocumentDTO(CompoundDocumentTypeVariant.AENDERUNGSVERORDNUNG,
                Utils.getRandomString()), DocumentType.REGELUNGSTEXT_MANTELVERORDNUNG),
            Arguments.of(makeCreateCompoundDocumentDTO(CompoundDocumentTypeVariant.MANTELGESETZ, ""),
                DocumentType.REGELUNGSTEXT_MANTELGESETZ),
            Arguments.of(makeCreateCompoundDocumentDTO(CompoundDocumentTypeVariant.STAMMGESETZ, ""),
                DocumentType.REGELUNGSTEXT_STAMMGESETZ),
            Arguments.of(makeCreateCompoundDocumentDTO(CompoundDocumentTypeVariant.MANTELGESETZ,
                Utils.getRandomString()), DocumentType.REGELUNGSTEXT_MANTELGESETZ),
            Arguments.of(makeCreateCompoundDocumentDTO(CompoundDocumentTypeVariant.STAMMVERORDNUNG, ""),
                DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG),
            Arguments.of(makeCreateCompoundDocumentDTO(CompoundDocumentTypeVariant.STAMMVERORDNUNG,
                Utils.getRandomString()), DocumentType.REGELUNGSTEXT_STAMMVERORDNUNG)
        );
    }

    private static CreateCompoundDocumentDTO makeCreateCompoundDocumentDTO(
        CompoundDocumentTypeVariant compoundDocumentType,
        String titleRegulatoryText) {
        return CreateCompoundDocumentDTO.builder()
            .regelungsVorhabenId(RV_ID)
            .title(Utils.getRandomString())
            .type(compoundDocumentType)
            .titleRegulatoryText(titleRegulatoryText)
            .initialNumberOfLevels(0)
            .build();
    }

    private static CompoundDocumentDTO makeCompoundDocumentDTO(CreateCompoundDocumentDTO createCompoundDocumentDTO) {
        PropositionDTO proposition = PropositionDTO.builder()
            .id(UUID.fromString(RV_ID))
            .build();
        return CompoundDocumentDTO.builder()
            .id(UUID.randomUUID())
            .title(createCompoundDocumentDTO.getTitle())
            .state(DocumentState.DRAFT)
            .type(createCompoundDocumentDTO.getType())
            .createdAt(Instant.now())
            .updatedAt(Instant.now())
            .documents(new ArrayList<>())
            .proposition(proposition)
            .build();
    }

    private static CompoundDocument makeCompoundDocument(CreateCompoundDocumentDTO createCompoundDocumentDTO) {
        UUID rvId = UUID.fromString(RV_ID);
        Proposition proposition = Proposition.builder()
            .propositionId(new RegelungsVorhabenId(rvId))
            .build();
        CompoundDocument compoundDocument = new CompoundDocument();
        CompoundDocumentDomain entity = CompoundDocumentDomain.builder()
            .compoundDocumentId(new CompoundDocumentId(UUID.randomUUID()))
            .regelungsVorhabenId(new RegelungsVorhabenId(rvId))
            .title(createCompoundDocumentDTO.getTitle())
            .state(DocumentState.DRAFT)
            .type(createCompoundDocumentDTO.getType())
            .createdAt(Instant.now())
            .updatedAt(Instant.now())
            .build();
        compoundDocument.setCompoundDocumentEntity(entity);
        return compoundDocument;
    }

    @ParameterizedTest
    @MethodSource("testParams")
    void test_whenCreateCompoundDocument_thenSuccess(CreateCompoundDocumentDTO createCompoundDocumentDTO,
        DocumentType documentType) {
        // Arrange
        CompoundDocument compoundDocument = makeCompoundDocument(createCompoundDocumentDTO);

        when(documentPermissionValidator.isUserOwnerOfRegulatoryProposal(any()))
            .thenReturn(true);
        when(templateService.loadTemplate(any(), anyInt())).thenReturn(Utils.getRandomString());
        when(plategRequestService.getRegelungsvorhabenEditorDTO(any(UUID.class)))
            .thenAnswer(invocation ->
                TestDTOUtil.getRegelungsvorhabenEditorDTOWithSpecificId(UUID.fromString(RV_ID))
            );
        when(regelungsvorhabenService.getRegelungsvorhabenEditorTriggeredByDocument(any(RegelungsVorhabenId.class)))
            .thenReturn(TestDTOUtil.getRandomRegelungsvorhabenEditorDTO());
        doReturn(Optional.of(compoundDocument.getCompoundDocumentEntity()))
            .when(compoundDocumentRepository)
            .findByCreatedByAndCompoundDocumentId(any(), any());
        doReturn(Optional.of(compoundDocument.getCompoundDocumentEntity()))
            .when(compoundDocumentRepository)
            .findByCompoundDocumentId(any());
        doReturn(DocumentDomain.builder()
            .compoundDocumentId(compoundDocument.getCompoundDocumentId())
            .documentId(new DocumentId(UUID.randomUUID()))
            .title(createCompoundDocumentDTO.getTitleRegulatoryText())
            .type(documentType)
            .build())
            .when(documentRepository)
            .save(any());

        List<ServiceState> status = new ArrayList<>();
        assertNotNull(createCompoundDocumentDTO);

        // Act
        CompoundDocumentDTO actual = compoundDocumentService.createCompoundDocument(createCompoundDocumentDTO, status);

        // Assert
        assertThat(status.get(0)).isEqualByComparingTo(ServiceState.OK);
        assertEquals(createCompoundDocumentDTO.getType(), actual.getType());
    }

}
