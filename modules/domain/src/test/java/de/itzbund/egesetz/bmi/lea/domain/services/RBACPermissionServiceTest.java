// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.EditorRollenUndRechte;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CopyCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.transaction.TransactionSystemException;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.core.Constants.RESSOURCENTYP_DOKUMENTENMAPPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ROLLE_HRA_TEILNEHMER;
import static de.itzbund.egesetz.bmi.lea.domain.services.RBACPermissionService.LESERECHTE_HINZU;
import static de.itzbund.egesetz.bmi.lea.domain.services.RBACPermissionService.SCHREIBRECHTE_ENTFERNT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {RBACPermissionService.class})
@SuppressWarnings("unused")
class RBACPermissionServiceTest {

    private final CopyCompoundDocumentDTO copyCompoundDocumentDTO = TestDTOUtil.getRandomCopyCompoundDocumentDTO();
    private final CompoundDocument compoundDocument = TestObjectsUtil.getRandomCompoundDocument();
    private final CompoundDocumentId compoundDocumentId = new CompoundDocumentId(UUID.randomUUID());
    private final Document document = TestObjectsUtil.getRandomDocument();
    private final UserId userId = new UserId(UUID.randomUUID().toString());

    @Autowired
    private RBACPermissionService rbacPermissionService;
    @MockBean
    private EditorRollenUndRechte editorRollenUndRechte;

    @MockBean
    private LeageSession session;

    @Mock
    private TransactionSystemException transactionSystemException;

    @MockBean
    private BenachrichtigungService benachrichtigungService;

    @BeforeEach
    void init() {
        when(session.getUser()).thenReturn(TestObjectsUtil.getUserEntity());
        when(editorRollenUndRechte.getAlleRessourcenFuerBenutzerUndRolleUndRessourceTyp(
            anyString(), anyString(), anyString())).thenAnswer(i -> {

            String argument = i.getArgument(2);

            if (argument.equals(RESSOURCENTYP_DOKUMENTENMAPPE)) {
                return List.of(copyCompoundDocumentDTO.getCompoundDocumentId()
                        .getId()
                        .toString(),
                    compoundDocument.getCompoundDocumentEntity()
                        .getCompoundDocumentId()
                        .getId()
                        .toString(),
                    compoundDocumentId.getId()
                        .toString());
            } else {
                return List.of(document.getDocumentId()
                    .getId()
                    .toString());
            }

        });
    }

    @Test
    void testGetAlleResourcenForUserRessourceTypAndAktionen() {
        Set<String> result = rbacPermissionService.getAlleResourcenForUserRessourceTypAndAktionen(session.getUser(), "", Collections.emptyList(),
            Collections.emptyList());

        assertThat(result).isEmpty();
    }

    @Test
    void testIstOderWarDieDokumentenmappeTeilEinerHRA() {
        // Positiv
        when(editorRollenUndRechte.getAlleBenutzerIdsByRessourceIdAndRolle(compoundDocumentId.getId().toString(), ROLLE_HRA_TEILNEHMER))
            .thenReturn(List.of("anyThing"));
        boolean actual = rbacPermissionService.istOderWarDieDokumentenmappeTeilEinerHRA(compoundDocumentId);
        assertThat(actual).isTrue();

        // Negativ
        actual = rbacPermissionService.istOderWarDieDokumentenmappeTeilEinerHRA(new CompoundDocumentId(UUID.randomUUID()));
        assertThat(actual).isFalse();
    }


    @Test
    void testAddCreatorPermissionsForCompoundDocument1Existing() {
        // Gibt es schon und wird gefunden
        boolean actual = rbacPermissionService.addCreatorPermissionsForCompoundDocument(copyCompoundDocumentDTO);
        assertThat(actual).isTrue();
    }


    @Test
    void testAddCreatorPermissionsForCompoundDocument1New() {
        copyCompoundDocumentDTO.setCompoundDocumentId(new CompoundDocumentId(UUID.randomUUID()));
        // Ist neu und wird eingetragen
        boolean actual = rbacPermissionService.addCreatorPermissionsForCompoundDocument(copyCompoundDocumentDTO);
        assertThat(actual).isTrue();
    }


    @Test
    void testAddCreatorPermissionsForCompoundDocument1InsertionException() {
        when(editorRollenUndRechte.addRessourcePermission(anyString(), anyString(), anyString(),
            anyString())).thenThrow(transactionSystemException);

        CopyCompoundDocumentDTO copyCompoundDocumentDTO1 = TestDTOUtil.getRandomCopyCompoundDocumentDTO();
        copyCompoundDocumentDTO1.setCompoundDocumentId(new CompoundDocumentId(UUID.randomUUID()));

        // Ist neu und wird eingetragen
        boolean actual = rbacPermissionService.addCreatorPermissionsForCompoundDocument(copyCompoundDocumentDTO1);
        assertThat(actual).isFalse();
    }


    @Test
    void testAddCreatorPermissionsForCompoundDocument2() {
        // Ist neu und wird eingetragen
        boolean actual = rbacPermissionService.addCreatorPermissionsForCompoundDocument(compoundDocument);
        assertThat(actual).isTrue();
    }


    @Test
    void testAddCreatorPermissionsForCompoundDocument3() {
        boolean actual = rbacPermissionService.addCreatorPermissionsForCompoundDocument(UUID.randomUUID()
                .toString(),
            compoundDocumentId);
        assertThat(actual).isTrue();
    }


    @Test
    void testAddCreatorPermissionsForDocumentExisting() {
        // Gibt es schon und wird gefunden
        boolean actual = rbacPermissionService.addCreatorPermissionsForDocument(document);
        assertThat(actual).isTrue();
    }


    @Test
    void testAddCreatorPermissionsForDocumentNew() {
        Document document1 = TestObjectsUtil.getRandomDocument();
        // Ist neu und wird eingetragen
        boolean actual = rbacPermissionService.addCreatorPermissionsForDocument(document1);
        assertThat(actual).isTrue();
    }


    @Test
    void testAddCreatorPermissionsForDocumentInsertionException() {
        when(editorRollenUndRechte.addRessourcePermission(anyString(), anyString(), anyString(),
            anyString())).thenThrow(transactionSystemException);

        Document document1 = TestObjectsUtil.getRandomDocument();

        // Beim Eintrag gibt es eine Exception
        boolean actual = rbacPermissionService.addCreatorPermissionsForDocument(document1);
        assertThat(actual).isFalse();
    }

    @Test
    void testVergibSchreibrechteFuerEinenNutzerZuEinerDokumentenmappeMitBefristung() {
        when(editorRollenUndRechte.addRessourcePermissionWithDetails(anyString(), anyString(), anyString(),
            anyString(), any(), any()))
            .thenReturn(Map.of(
                    LESERECHTE_HINZU, List.of(LESERECHTE_HINZU),
                    SCHREIBRECHTE_ENTFERNT, List.of(SCHREIBRECHTE_ENTFERNT)
                )
            );

        // Act
        rbacPermissionService.vergibSchreibrechteFuerEinenNutzerZuEinerDokumentenmappeMitBefristung(TestObjectsUtil.getUserEntity(), userId.getId(),
            TestDTOUtil.getRandomCompoundDocumentSummaryDTO(), null);
    }
}
