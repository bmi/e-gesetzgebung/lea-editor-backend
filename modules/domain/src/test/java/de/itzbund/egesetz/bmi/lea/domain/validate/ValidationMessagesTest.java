// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.validate;

import de.itzbund.egesetz.bmi.lea.core.util.FlatGroupedList;
import de.itzbund.egesetz.bmi.lea.core.util.TestResourceProvider;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.ValidationService;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.SchemaProvider;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationData;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationServiceImpl;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.Validator;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.SchematronValidationTask;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.XmlSchemaValidationTask;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.xml.sax.InputSource;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Testsuite for {@link de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationData}.
 */
@Log4j2
@SpringBootTest(classes = {ValidationServiceImpl.class, Validator.class, SchemaProvider.class,
    TestValidationTask.class, ValidationData.class, XmlSchemaValidationTask.class, FlatGroupedList.class,
    SchematronValidationTask.class})
@ExtendWith(SpringExtension.class)
@Disabled("needs fix for inputs with nested inline elements per text line")
class ValidationMessagesTest {

    private static final TestResourceProvider TEST_RESOURCE_PROVIDER = TestResourceProvider.getInstance();

    private static ValidationService validationService;


    @SuppressWarnings("unused")
    private static Stream<ReusableInputSource> provideInputDocuments() {
        Iterator<ReusableInputSource> docs = TEST_RESOURCE_PROVIDER.getInputSources("akn4de");
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(docs, Spliterator.ORDERED), false);
    }


    @Autowired
    @SuppressWarnings("unused")
    void setService(ValidationServiceImpl service) {
        validationService = service;
    }


    @ParameterizedTest
    @MethodSource("provideInputDocuments")
    void testValidationMessages(ReusableInputSource ris) {
        assertNotNull(ris);
        String docId = ris.getSystemId();
        assertNotNull(docId);
        log.info("validating " + docId);

        String content = Utils.getPrettyPrintedXml(Utils.xmlLinearize(Utils.getContent(ris)));
        assertFalse(Utils.isMissing(content));

        ValidationData messages = validationService.validateLDML(new InputSource(new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8))));
        assertNotNull(messages);
        if (messages.getErrorCount() > 0) {
            log.info("FAILURE");
        } else {
            log.info("SUCCESS");
        }
    }

}
