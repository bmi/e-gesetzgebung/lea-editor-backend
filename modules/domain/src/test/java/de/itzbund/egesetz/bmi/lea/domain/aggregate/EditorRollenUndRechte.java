/*
 * SPDX-License-Identifier: MPL-2.0
 *
 * Copyright (C) 2021-2023 Bundesministerium des Innern und für Heimat, \
 * Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
 */
package de.itzbund.egesetz.bmi.lea.domain.aggregate;

import de.itzbund.egesetz.bmi.auth.api.dto.PermissionDTO;
import de.itzbund.egesetz.bmi.auth.api.enums.GetPermissionOptionType;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Component
public class EditorRollenUndRechte {

	public List<String> getAlleRessourcenFuerBenutzerUndRolleUndRessourceTyp(String benutzerId, String rolle,
		String ressourcenTyp) {
		return Collections.emptyList();
	}

	public List<String> getAlleRessourcenFuerBenutzerUndRolleUndRessourceTyp(String benutzerId, String rolle,
		String ressourcenTyp, boolean mitDeaktivierten) {
		return Collections.emptyList();
	}

	public boolean addRessourcePermission(String benutzerId, String ressourcenTyp, String ressourceId, String rolle) {
		return false;
	}

	public boolean addRessourcePermission(String benutzerId, String ressourcenTyp, String ressourceId, String rolle, Instant befristung) {
		return false;
	}

	public boolean addRessourcePermission(String benutzerId, String ressourcenTyp, String ressourceId, String rolle, Instant befristung, String anmerkungen) {
		return false;
	}

	public Map<String, List<String>> addRessourcePermissionWithDetails(String benutzerId, String ressourcenTyp, String ressourceId, String rolle,
		Instant befristung, String anmerkungen) {
		return Map.of();
	}

	public Set<String> getAlleBenutzerIdsByRessourceIdAndAktion(String ressourcenId, String aktion) {
		return Collections.emptySet();
	}


	public boolean isAllowed(String benutzerId, String aktion, String ressourcenTyp, String ressourcenId) {
		return false;
	}


	public List<String> getAlleBenutzerIdsByRessourceIdAndRolle(String ressourcenId, String rolle) {
		return Collections.emptyList();
	}


	public boolean removeRessourcePermission(String benutzerId, String ressourcenTyp, String ressourceId, String rolle) {
		return false;
	}


	public boolean removeRessourcePermission(String ressourcenTyp, String ressourceId, String rolle) {
		return false;
	}

	public List<UUID> getAlleRessourcenFuerBenutzerUndRessourceTypAsUUID(String ressourcenTyp, String ressourceId) {
		return Collections.emptyList();
	}

	public List<String> getAlleRessourcenFuerBenutzerUndRessourceTyp(String benutzerId, String ressourcenTyp) {
		return Collections.emptyList();
	}

	public List<String> getAlleRessourceTypenIds(String benutzerId, String ressourcenTyp, String rolle) {
		return Collections.emptyList();
	}

	public List<PermissionDTO> getPermissions(Optional<Collection<String>> benutzerIds, Optional<Collection<String>> aktionen,
		Optional<Collection<String>> ressourceTypen, Optional<Collection<String>> ressourcenIds, Optional<Collection<String>> rollen,
		GetPermissionOptionType optionType) {
		return Collections.emptyList();
	}

	public String getRessortKurzbezeichnungForNutzer(String benutzerId) {
		return "";
	}
}
