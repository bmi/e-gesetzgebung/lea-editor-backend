// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.mappers;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentMetadataDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * Tests for {@link DocumentMapper}
 */
class DocumentMapperTest {

	private DocumentMapper documentMapper;


	@BeforeEach
	void init() {
		documentMapper = new DocumentMapperImpl(new BaseMapperImpl(), new PropositionMapperImpl(new BaseMapperImpl()),
			new UserMapperImpl()
		);
	}


	@Test
	void testMapToDocumentDTO() {
		DocumentDomain documentEntity = TestObjectsUtil.getDocumentEntity();
		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = TestDTOUtil.getRandomRegelungsvorhabenEditorDTO();

		DocumentDTO actual = documentMapper.mapToDocumentDTO(documentEntity, regelungsvorhabenEditorDTO);

		assertNotNull(actual);
		assertThat(actual).hasNoNullFieldsOrPropertiesExcept("state", "documentPermissions", "commentPermissions", "version", "mappenName");

		PropositionDTO proposition = actual.getProposition();
		assertThat(proposition).isNotNull();

		assertEquals(documentEntity.getDocumentId()
			.getId(), actual.getId());
		assertEquals(documentEntity.getTitle(), actual.getTitle());
		assertEquals(documentEntity.getContent(), actual.getContent());
		assertEquals(documentEntity.getType(), actual.getType());
	}


	@Test
	void testMapToDto() {
		Document document = TestObjectsUtil.getRandomDocument();

		DocumentDTO actual = documentMapper.mapToDto(document);

		assertNotNull(actual);
		assertThat(actual).hasNoNullFieldsOrPropertiesExcept("proposition", "documentPermissions", "commentPermissions", "version", "mappenName");
	}


	@Test
	void testMapToDocumentDto() {
		// Arrange
		Document document = TestObjectsUtil.getRandomDocument();
		RegelungsvorhabenEditorDTO regelungsVorhabenDTO = TestDTOUtil.getRandomRegelungsvorhabenEditorDTO();

		// Act
		DocumentDTO actual = documentMapper.mapDokumentMitRegelungsvorhaben(document, regelungsVorhabenDTO);

		assertNotNull(actual);
		assertThat(actual).hasNoNullFieldsOrPropertiesExcept("documentPermissions", "commentPermissions", "version", "mappenName");
	}


	@Test
	void testMapToDtoList() {
		List<Document> documents = TestObjectsUtil.getDocumentListOfSize(3);
		List<DocumentDTO> actual = documentMapper.mapToDtoList(documents);

		assertNotNull(actual);
		assertThat(actual).hasSize(documents.size());
		// Test list
		assertThat(actual.get(0)).hasNoNullFieldsOrPropertiesExcept("proposition", "documentPermissions", "commentPermissions", "version", "mappenName");
	}


	@Test
	void testMapToSummatyDtoList() {
		// Arrange
		List<Document> documents = TestObjectsUtil.getDocumentListOfSize(3);

		// Act
		List<DocumentSummaryDTO> actual = documentMapper.mapToSummatyDtoList(documents);

		// Assert
		assertNotNull(actual);
		assertThat(actual).hasSize(documents.size());
		// Test list
		assertThat(actual.get(0)).hasNoNullFieldsOrPropertiesExcept("state", "version", "documentPermissions", "commentPermissions");
	}


	@Test
	void testMapSummaryDomain() {
		DocumentDomain documentEntity = TestObjectsUtil.getDocumentEntity();
		DocumentSummaryDTO actual = documentMapper.mapSummaryDomain(documentEntity);
		assertNotNull(actual);
		assertThat(actual).hasNoNullFieldsOrPropertiesExcept("state", "version", "documentPermissions", "commentPermissions");
	}


	@Test
	void testMapSummary() {
		Document document = TestObjectsUtil.getRandomDocument();
		DocumentSummaryDTO actual = documentMapper.mapSummary(document);
		assertNotNull(actual);
		assertThat(actual).hasNoNullFieldsOrPropertiesExcept("version", "documentPermissions", "commentPermissions");
	}


	@Test
	void testMapMetaData() {
		DocumentDTO documentDTO = TestDTOUtil.getRandomDocumentDTO();
		DocumentMetadataDTO actual = documentMapper.mapMetaData(documentDTO);
		assertNotNull(actual);
		assertThat(actual).hasNoNullFieldsOrPropertiesExcept("documentPermissions", "commentPermissions");
	}
}
