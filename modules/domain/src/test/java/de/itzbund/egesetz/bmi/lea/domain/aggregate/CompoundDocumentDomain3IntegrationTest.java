// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.aggregate;

import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("unused")
class CompoundDocumentDomain3IntegrationTest extends CompoundDocumentIntegrationDomain {

    private static final User user2 = User.builder()
        .gid(new UserId("Other USER ID"))
        .name("Username")
        .email("meine2@email.de")
        .build();
    private static final String TITLE = "My document title";

    private final static UUID uuid1 = UUID.randomUUID();
    private final static UUID uuid2 = UUID.randomUUID();


    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();

        Mockito.when(compoundDocumentRepository.findByCreatedBy(USER))
            .thenReturn(
                Arrays.asList(
                    createSampleCompoundDocumentEntity(uuid1),
                    createSampleCompoundDocumentEntity(uuid2)
                ));

        Mockito.when(compoundDocumentRepository.findByCreatedByAndCompoundDocumentId(USER,
                new CompoundDocumentId(uuid1)))
            .thenReturn(
                Optional.of(createSampleCompoundDocumentEntity(uuid1))
            );

        List<DocumentDomain> documentEntities = Collections.singletonList(
            DocumentDomain.builder()
                .title(TITLE)
                .type(DocumentType.REGELUNGSTEXT_STAMMGESETZ)
                .content(
                    "[{\"xml-model\":\"href=\\\"02_schema\\/legalDocML.de\\/legalDocML.de_schematron"
                        + ".sch\\\" schematypens=\\\"http:\\/\\/purl.oclc"
                        + ".org\\/dsdl\\/schematron\\\"\","
                        + "\"children\":[{\"children\":[{\"children\":[{\"children\":[{\"children"
                        + "\":[{\"children\":[{\"text\":\"\"}],\"type\":\"akn:FRBRthis\"},"
                        + "{\"children\":[{\"text\":\"\"}],\"type\":\"akn:FRBRuri\"},"
                        + "{\"children\":[{\"text\":\"\"}],\"type\":\"akn:FRBRdate\"},"
                        + "{\"children\":[{\"text\":\"\"}],\"type\":\"akn:FRBRauthor\"},"
                        + "{\"children\":[{\"text\":\"\"}],\"type\":\"akn:FRBRcountry\"},"
                        + "{\"children\":[{\"text\":\"\"}],\"type\":\"akn:FRBRnumber\"}],"
                        + "\"type\":\"akn:FRBRWork\"},{\"children\":[{\"children\":[{\"text\":\"\"}],"
                        + "\"type\":\"akn:FRBRthis\"},{\"children\":[{\"text\":\"\"}],"
                        + "\"type\":\"akn:FRBRuri\"},{\"children\":[{\"text\":\"\"}],"
                        + "\"type\":\"akn:FRBRdate\"},{\"children\":[{\"text\":\"\"}],"
                        + "\"type\":\"akn:FRBRauthor\"},{\"children\":[{\"text\":\"\"}],"
                        + "\"type\":\"akn:FRBRversionNumber\"},{\"children\":[{\"text\":\"\"}],"
                        + "\"type\":\"akn:FRBRlanguage\"}],\"type\":\"akn:FRBRExpression\"},"
                        + "{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"akn:FRBRthis\"},"
                        + "{\"children\":[{\"text\":\"\"}],\"type\":\"akn:FRBRuri\"},"
                        + "{\"children\":[{\"text\":\"\"}],\"type\":\"akn:FRBRdate\"},"
                        + "{\"children\":[{\"text\":\"\"}],\"type\":\"akn:FRBRauthor\"}],"
                        + "\"type\":\"akn:FRBRManifestation\"}],\"type\":\"akn:identification\"},"
                        + "{\"children\":[{\"children\":[{\"children\":[{\"text\":\"gesetz\"}],"
                        + "\"type\":\"meta:typ\"},{\"children\":[{\"text\":\"stammform\"}],"
                        + "\"type\":\"meta:form\"},{\"children\":[{\"text\":\"bundesregierung\"}],"
                        + "\"type\":\"meta:initiant\"},"
                        + "{\"children\":[{\"text\":\"bundesregierung\"}],"
                        + "\"type\":\"meta:bearbeitendeInstitution\"}],\"type\":\"meta:legalDocML"
                        + ".de_metadaten\",\"xmlns:meta\":\"http:\\/\\/Metadaten.LegalDocML.de\"}],"
                        + "\"type\":\"akn:proprietary\"}],\"type\":\"akn:meta\"},"
                        + "{\"children\":[{\"children\":[{\"children\":[{\"children\":[{\"text"
                        + "\":\"Referentenentwurf\"}],\"type\":\"akn:docStage\"},"
                        + "{\"children\":[{\"text\":\"[Platzhalter]\"}],"
                        + "\"type\":\"akn:docProponent\"},"
                        + "{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"akn:docTitle\"},"
                        + "{\"children\":[{\"text\":\"([Platzhalter] -\"},"
                        + "{\"refersTo\":\"amtliche-abkuerzung\","
                        + "\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"akn:inline\"},"
                        + "{\"text\":\")\"}],\"type\":\"akn:shortTitle\"}],\"type\":\"akn:p\"}],"
                        + "\"type\":\"akn:longTitle\"},"
                        + "{\"children\":[{\"refersTo\":\"ausfertigung-datum\","
                        + "\"children\":[{\"text\":\"Vom [Platzhalter]\"}],\"type\":\"akn:date\"}],"
                        + "\"type\":\"akn:block\"}],\"type\":\"akn:preface\"},"
                        + "{\"children\":[{\"children\":[{\"children\":[{\"text\":\"Der Bundestag hat"
                        + " das folgende Gesetz beschlossen:\"}],\"type\":\"akn:p\"}],"
                        + "\"type\":\"akn:formula\"}],\"type\":\"akn:preamble\"},"
                        + "{\"refersTo\":\"hauptteil-stammform\","
                        + "\"children\":[{\"children\":[{\"children\":[{\"text\":\"Abschnitt 1\"}],"
                        + "\"type\":\"akn:num\"},{\"children\":[{\"text\":\"[Platzhalter]\"}],"
                        + "\"type\":\"akn:heading\"},{\"refersTo\":\"einzelvorschrift-stammform\","
                        + "\"children\":[{\"children\":[{\"text\":\"§ 1\"}],\"type\":\"akn:num\"},"
                        + "{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"akn:heading\"},"
                        + "{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"akn:num\"},"
                        + "{\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],"
                        + "\"type\":\"akn:p\"}],\"type\":\"akn:content\"}],"
                        + "\"type\":\"akn:paragraph\"}],\"type\":\"akn:article\"},"
                        + "{\"refersTo\":\"einzelvorschrift-stammform\","
                        + "\"children\":[{\"children\":[{\"text\":\"§ 2\"}],\"type\":\"akn:num\"},"
                        + "{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"akn:heading\"},"
                        + "{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"akn:num\"},"
                        + "{\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],"
                        + "\"type\":\"akn:p\"}],\"type\":\"akn:content\"}],"
                        + "\"type\":\"akn:paragraph\"}],\"type\":\"akn:article\"}],"
                        + "\"type\":\"akn:section\"},"
                        + "{\"children\":[{\"children\":[{\"text\":\"Abschnitt 2\"}],"
                        + "\"type\":\"akn:num\"},{\"children\":[{\"text\":\"[Platzhalter]\"}],"
                        + "\"type\":\"akn:heading\"},{\"refersTo\":\"einzelvorschrift-stammform\","
                        + "\"children\":[{\"children\":[{\"text\":\"§ 3\"}],\"type\":\"akn:num\"},"
                        + "{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"akn:heading\"},"
                        + "{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"akn:num\"},"
                        + "{\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],"
                        + "\"type\":\"akn:p\"}],\"type\":\"akn:content\"}],"
                        + "\"type\":\"akn:paragraph\"}],\"type\":\"akn:article\"},"
                        + "{\"refersTo\":\"einzelvorschrift-stammform|geltungszeitregel\","
                        + "\"children\":[{\"children\":[{\"text\":\"§ 4\"}],\"type\":\"akn:num\"},"
                        + "{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"akn:heading\"},"
                        + "{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"akn:num\"},"
                        + "{\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],"
                        + "\"type\":\"akn:p\"}],\"type\":\"akn:content\"}],"
                        + "\"type\":\"akn:paragraph\"}],\"type\":\"akn:article\"}],"
                        + "\"type\":\"akn:section\"}],\"type\":\"akn:body\"},"
                        + "{\"children\":[{\"text\":\"\"}],\"type\":\"akn:conclusions\"}],"
                        + "\"name\":\"regelungstext\",\"type\":\"akn:bill\"}],"
                        + "\"xml-stylesheet\":\"href=\\\"stylesheets\\/css\\/main.min.css\\\"\","
                        + "\"xsi:schemaLocation\":\"http:\\/\\/docs.oasis-open"
                        + ".org\\/legaldocml\\/ns\\/akn\\/3.0 schema\\/legalDocML.de\\/legalDocML"
                        + ".de-regelungstextentwurfsfassung.xsd http:\\/\\/Metadaten.LegalDocML.de "
                        + "schema\\/metadaten\\/legalDocML.de-metadatenSchema.xsd\","
                        + "\"xmlns:xsi\":\"http:\\/\\/www.w3.org\\/2001\\/XMLSchema-instance\","
                        + "\"type\":\"akn:akomaNtoso\",\"xmlns:akn\":\"http:\\/\\/docs.oasis-open"
                        + ".org\\/legaldocml\\/ns\\/akn\\/3.0\"}]")
                .createdAt(Instant.now())
                .updatedAt(Instant.now())
                .build()
        );

        Mockito.when(documentRepository.findByCreatedByAndCompoundDocumentId(
                USER, null))
            .thenAnswer(invocation ->
                documentEntities
            );
    }


    private Document createSampleDocument() {
        DocumentId id = new DocumentId(UUID.randomUUID());

        DocumentDomain documentEntity = DocumentDomain.builder()
            .documentId(id)
            .type(DocumentType.REGELUNGSTEXT_STAMMGESETZ)
            .title(TITLE)
            .createdBy(USER)
            .createdAt(Instant.now())
            .updatedAt(Instant.now())
            .build();

        return new Document(documentEntity);
    }


    private CompoundDocumentDomain createSampleCompoundDocumentEntity(UUID externalUUID) {
        CompoundDocumentDomain compoundDocumentEntity = CompoundDocumentDomain.builder()
            .build();
        compoundDocumentEntity.setCompoundDocumentId(new CompoundDocumentId(externalUUID));
        compoundDocumentEntity.setRegelungsVorhabenId(new RegelungsVorhabenId(externalUUID));
        compoundDocumentEntity.setTitle(TITLE);
        compoundDocumentEntity.setType(CompoundDocumentTypeVariant.STAMMGESETZ);
        compoundDocumentEntity.setCreatedBy(USER);
        compoundDocumentEntity.setCreatedAt(Instant.now());
        compoundDocumentEntity.setUpdatedAt(Instant.now());

        return compoundDocumentEntity;
    }


    @Test
    void whenCompoundDocumentShouldBeSavedAndRepositoryIsAvailable_thenItShouldBePersisted() {
        compoundDocument = compoundDocument.createOrUpdate(null);
        assertThat(compoundDocument)
            .hasFieldOrProperty("compoundDocumentId")
            .isNotNull();
    }


    @Test
    void whenAllCompoundDocumentForAValidUserIsRequested_thenTheyShouldBeProvided() {
        // Repository is mocked, so result is always 2
        List<CompoundDocument> actualCompoundDocuments =
            compoundDocument.getCompoundDocumentsOptionalFilteredByUserUndRegelungsvorhaben(
                USER,
                Arrays.asList(uuid1, uuid2));
        assertThat(actualCompoundDocuments).isNotEmpty()
            .hasSize(2);
    }


    @Test
    void whenAllCompoundDocumentForAValidUserIsRequestedButThereAreNothing_thenAnEmptyResultShouldBeProvided() {
        Mockito.when(compoundDocumentRepository.findByCreatedBy(USER))
            .thenReturn(
                Collections.emptyList()
            );
        List<CompoundDocument> actualCompoundDocuments =
            compoundDocument.getCompoundDocumentsOptionalFilteredByUserUndRegelungsvorhaben(
                USER,
                Collections.emptyList());
        assertThat(actualCompoundDocuments).isEmpty();
    }


    @Test
    void whenAllCompoundDocumentForAnInvalidUserIsRequested_thenAnEmptyResultShouldBeProvided() {
        Mockito.when(compoundDocumentRepository.findByCreatedBy(user2))
            .thenReturn(
                Collections.emptyList()
            );
        List<CompoundDocument> actualCompoundDocuments =
            compoundDocument.getCompoundDocumentsOptionalFilteredByUserUndRegelungsvorhaben(
                user2,
                Collections.emptyList());
        assertThat(actualCompoundDocuments).isEmpty();
    }


    @Test
    void whenASpecialCompoundDocumentForAValidUserIsRequested_thenAValidResultShouldBeProvided() {
        CompoundDocument actualCompoundDocuments =
            compoundDocument.getCompoundDocumentWithDocumentsForIdAndOptionalFiltered(
                new CompoundDocumentId(uuid1), USER);
        assertThat(actualCompoundDocuments)
            .hasFieldOrProperty("compoundDocumentEntity")
            .isNotNull()
            .hasFieldOrProperty("documents")
            .isNotNull();
        assertThat(actualCompoundDocuments.getDocuments())
            .asList()
            .isNotEmpty();
    }


    @Test
    void testInitialize() {
        assertThat(compoundDocument)
            .hasFieldOrProperty("compoundDocumentRepository")
            .isNotNull();
    }

}