// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.eId;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentForm;
import de.itzbund.egesetz.bmi.lea.domain.ports.eid.Bezeichner;
import de.itzbund.egesetz.bmi.lea.domain.ports.eid.EId;
import de.itzbund.egesetz.bmi.lea.domain.ports.eid.JSONContext;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.BezeichnerImpl;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.EIdGeneratorService;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.EIdImpl;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.JSONContextImpl;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_CHILDREN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_EID;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDocumentObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getGuid;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.getContentOfTextResource;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.xor;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Named.named;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class EIdGeneratorServiceTest {


    @ParameterizedTest
    @MethodSource("sampleDocs")
    @SneakyThrows
    void test_SampleDocs_from_Standard(JSONObject expectedDoc, JSONObject testDoc) {
        ObjectMapper om = new ObjectMapper();
        assertNotEquals(om.readTree(expectedDoc.toJSONString()), om.readTree(testDoc.toJSONString()));
        EIdGeneratorService eIdGeneratorService = new EIdGeneratorService();
        JSONObject actualDoc = eIdGeneratorService.erzeugeEIdsFuerDokument(testDoc);
        assertEquals(om.readTree(expectedDoc.toJSONString()), om.readTree(actualDoc.toJSONString()));
        checkEIdAndGUIDs(actualDoc);
    }


    private void checkEIdAndGUIDs(JSONObject jsonObject) {
        if (jsonObject != null && !Utils.isMissing(getType(jsonObject))) {
            String eId = getEId(jsonObject);
            String guid = getGuid(jsonObject);
            assertTrue(xor(eId != null && guid != null, eId == null && guid == null));
        }
    }


    private static Stream<Arguments> sampleDocs() {
        List<String> files = List.of(
            "/services/eid/01-01_instanz_01",
            "/services/eid/01-01_instanz_01_begruendung",
            "/services/eid/01-01_instanz_01_regelungstext",
            "/services/eid/01-01_instanz_01_vorblatt"
        );

        return files.stream().map(file -> {
            JSONObject expectedDoc = getDocumentObject(getContentOfTextResource(file + "-expected.json"));
            assertNotNull(expectedDoc);
            JSONObject testDoc = getDocumentObject(getContentOfTextResource(file + "-ohneEId.json"));
            assertNotNull(testDoc);
            return arguments(named(file, expectedDoc), testDoc);
        });
    }


    @Test
    void testBezeichner_Create() {
        assertThrows(NullPointerException.class, () -> BezeichnerImpl.newInstance(null, 0));
        assertThrows(NullPointerException.class, () -> BezeichnerImpl.newInstance("text", null));

        Bezeichner b = BezeichnerImpl.newInstance("abc", 0);
        assertNotNull(b);
        assertEquals(BezeichnerImpl.UNGUELTIGER_BEZEICHNER, b);
        assertFalse(((BezeichnerImpl) b).isValid());

        b = BezeichnerImpl.newInstance("abc", 1);
        assertNotNull(b);
        assertEquals(BezeichnerImpl.UNGUELTIGER_BEZEICHNER, b);

        b = BezeichnerImpl.newInstance("text", 7);
        assertTrue(((BezeichnerImpl) b).isValid());
        assertEquals("text", b.getElementTypKuerzel());
        assertEquals("7", b.getPosition());
    }


    @Test
    void testBezeichner_fromString() {
        assertThrows(NullPointerException.class, () -> BezeichnerImpl.fromString(null));
        assertThrows(IndexOutOfBoundsException.class, () -> BezeichnerImpl.fromString(""));

        assertNotNull(BezeichnerImpl.fromString("art-2"));
        assertNotNull(BezeichnerImpl.fromString("container-a"));
        assertNotNull(BezeichnerImpl.fromString("tblue-1"));
    }


    @Test
    void testBezeichner_toString() {
        assertEquals("", BezeichnerImpl.newInstance("xyz", 0).toString());
        assertEquals("text-13", BezeichnerImpl.newInstance("text", 13).toString());
        assertEquals("", BezeichnerImpl.newInstance("tabelle", 0).toString());
        assertEquals("tabelle-1", BezeichnerImpl.newInstance("tabelle", 1).toString());
        assertEquals("para-5b", BezeichnerImpl.newInstance("para", "5b").toString());
    }


    @Test
    void testEId_Create() {
        assertNull(EIdImpl.newInstance(BezeichnerImpl.UNGUELTIGER_BEZEICHNER));
        assertNotNull(EIdImpl.newInstance(BezeichnerImpl.newInstance("art", 2)));
        assertNotNull(EIdImpl.newInstance(BezeichnerImpl.newInstance("para", "5b")));
        assertNotNull(Objects.requireNonNull(EIdImpl.newInstance(BezeichnerImpl.newInstance("buch", 1)))
            .extendBy(BezeichnerImpl.newInstance("kapitel", 2)));
    }


    @Test
    void testEId_fromString() {
        assertThrows(NullPointerException.class, () -> EIdImpl.fromString(null));
        assertThrows(IndexOutOfBoundsException.class, () -> EIdImpl.fromString(""));

        assertNotNull(EIdImpl.fromString("para-107"));
        assertNotNull(EIdImpl.fromString("buch-1_kapitel-2"));
        assertNotNull(EIdImpl.fromString("art-3b"));
        assertNotNull(EIdImpl.fromString("para-3_bezeichnung-1"));
        assertNotNull(EIdImpl.fromString("para-2_abs-2_inhalt-1"));
        assertNotNull(EIdImpl.fromString("para-2_abs-1_untergl-1_listenelem-b_inhalt-1_text-1"));
        assertNotNull(EIdImpl.fromString("container-a_container-vi_überschrift-1"));
    }


    @Test
    void testEId_toString() {
        assertEquals("art-2", Objects.requireNonNull(
            EIdImpl.newInstance(BezeichnerImpl.newInstance("art", 2))).toString());
        assertEquals("para-5b", Objects.requireNonNull(
            EIdImpl.newInstance(BezeichnerImpl.newInstance("para", "5b"))).toString());
        assertEquals("buch-1_kapitel-2", Objects.requireNonNull(
            Objects.requireNonNull(EIdImpl.newInstance(BezeichnerImpl.newInstance("buch",
                    1)))
                .extendBy(BezeichnerImpl.newInstance("kapitel", 2))).toString());

        String testEId = "para-2_abs-1_untergl-1_listenelem-b_inhalt-1_text-1";
        assertEquals(testEId, Objects.requireNonNull(EIdImpl.fromString(testEId)).toString());
    }


    @Test
    void testEId_getPraefix() {
        assertNull(Objects.requireNonNull(EIdImpl.fromString("container-a")).getPraefix());

        String testEId = "container-e_inhalt-1_inhaltabschnitt-e.2_inhaltabschnitt-1_überschrift-1";
        EId eId = EIdImpl.fromString(testEId);
        assertNotNull(eId);
        assertEquals(testEId, eId.toString());

        EId praefix = eId.getPraefix();
        assertNotNull(praefix);
        assertTrue(testEId.startsWith(praefix.toString()));
    }


    @Test
    void testJSONContext_incrementCounts_AndReadBack() {
        JSONContext jsonContext = new JSONContextImpl();
        String testPraefix = Utils.getRandomString();
        String typeA = "typeA";
        String typeB = "typeB";
        String typeC = "typeC";

        jsonContext.setPraefix(testPraefix);
        assertEquals(testPraefix, jsonContext.getPraefix());

        jsonContext.addTypeOccurrence(typeA);
        assertEquals(1, jsonContext.getCurrentTypeCount(typeA));
        assertEquals(0, jsonContext.getCurrentTypeCount(typeB));
        assertEquals(0, jsonContext.getCurrentTypeCount(typeC));

        jsonContext.addTypeOccurrence(typeB);
        jsonContext.addTypeOccurrence(typeA);
        jsonContext.addTypeOccurrence(typeC);
        jsonContext.addTypeOccurrence(typeB);
        jsonContext.addTypeOccurrence(typeA);
        assertEquals(3, jsonContext.getCurrentTypeCount(typeA));
        assertEquals(2, jsonContext.getCurrentTypeCount(typeB));
        assertEquals(1, jsonContext.getCurrentTypeCount(typeC));
    }


    @RepeatedTest(13)
    void testJSONContext_CreateFrom_EmptyMap() {
        Map<String, Integer> counts = Map.of();
        JSONContext jsonContext = JSONContextImpl.createFrom(counts);
        assertNotNull(jsonContext);
        assertEquals("", jsonContext.getPraefix());
        assertEquals(0, jsonContext.getCurrentTypeCount(Utils.getRandomString()));
    }


    @Test
    void testJSONContext_CreateFrom() {
        String[] strings = new String[]{
            Utils.getRandomString(),
            Utils.getRandomString(),
            Utils.getRandomString()
        };
        Map<String, Integer> counts = Map.of(
            strings[0], 0,
            strings[1], 7,
            strings[2], 1
        );
        JSONContext jsonContext = JSONContextImpl.createFrom(counts);
        assertNotNull(jsonContext);
        assertEquals("", jsonContext.getPraefix());
        assertEquals(0, jsonContext.getCurrentTypeCount(strings[0]));
        assertEquals(7, jsonContext.getCurrentTypeCount(strings[1]));
        assertEquals(1, jsonContext.getCurrentTypeCount(strings[2]));
    }


    @Test
    void testJSONContext_CopyContext_EmptyContext() {
        String testPraefix = Utils.getRandomString();
        JSONContext origContext = JSONContextImpl.createFrom(Map.of());
        origContext.setPraefix(testPraefix);

        JSONContext copy1 = origContext.copyWithIncrement(null);
        assertNotNull(copy1);
        assertEquals(testPraefix, copy1.getPraefix());
        assertEquals(0, copy1.getCurrentTypeCount(null));
        assertNotEquals(origContext.hashCode(), copy1.hashCode());

        String testType = Utils.getRandomString();
        JSONContext copy2 = origContext.copyWithIncrement(testType);
        assertNotNull(copy2);
        assertEquals(testPraefix, copy2.getPraefix());
        assertEquals(1, copy2.getCurrentTypeCount(testType));
        assertNotEquals(origContext.hashCode(), copy2.hashCode());
        assertNotEquals(copy1.hashCode(), copy2.hashCode());
    }


    @Test
    void testJSONContext_CopyContext_NonEmptyContext_WithoutNullKey() {
        String testPraefix = Utils.getRandomString();
        Map<String, Integer> map = Map.of(
            "str1", 1, "str2", 2, "str3", 3, "str4", 5, "str5", 8
        );
        JSONContext origContext = JSONContextImpl.createFrom(map);
        origContext.setPraefix(testPraefix);

        JSONContext copy = origContext.copyWithIncrement(null);
        assertNotNull(copy);
        assertNotEquals(origContext.hashCode(), copy.hashCode());
        assertEquals(testPraefix, copy.getPraefix());
        checkEquals(map, origContext, copy);
    }


    @Test
    void testJSONContext_CopyContext_NonEmptyContext_WithNullKey() {
        String testPraefix = Utils.getRandomString();
        Map<String, Integer> map = Map.of(
            "str1", 1, "str2", 2, "str3", 3, "str4", 5, "str5", 8
        );
        Map<String, Integer> map0 = new HashMap<>(map);
        map0.put(null, 13);
        JSONContext origContext = JSONContextImpl.createFrom(map0);
        origContext.setPraefix(testPraefix);
        assertEquals(13, origContext.getCurrentTypeCount(null));

        JSONContext copy1 = origContext.copyWithIncrement(null);
        assertNotNull(copy1);
        assertNotEquals(origContext.hashCode(), copy1.hashCode());
        assertEquals(testPraefix, copy1.getPraefix());
        assertEquals(14, copy1.getCurrentTypeCount(null));

        JSONContext copy2 = origContext.copyWithIncrement("str3");
        assertNotNull(copy2);
        assertEquals(4, copy2.getCurrentTypeCount("str3"));

        JSONContext copy3 = origContext.copyWithIncrement("n/a");
        assertNotNull(copy3);
        assertEquals(1, copy3.getCurrentTypeCount("n/a"));
    }


    private void checkEquals(@NonNull Map<String, Integer> map, JSONContext ctx1, JSONContext ctx2) {
        map.keySet().forEach(s -> assertEquals(ctx1.getCurrentTypeCount(s), ctx2.getCurrentTypeCount(s)));
    }


    @Test
    @SneakyThrows
    void testErzeugeEIdsFuerTeilbaum_EmptyObject() {
        JSONObject jsonObject = (JSONObject) new JSONParser().parse("{}");
        EIdGeneratorService eIdGeneratorService = new EIdGeneratorService();
        JSONContext jsonContext = new JSONContextImpl();

        JSONObject amendedObject = eIdGeneratorService.erzeugeEIdsFuerTeilbaum(jsonObject, jsonContext,
            RechtsetzungsdokumentForm.STAMMFORM);
        assertTrue(collectEIds(amendedObject).isEmpty());
    }


    @Test
    @SneakyThrows
    void testErzeugeEIdsFuerTeilbaum_SimpleObject_1() {
        JSONObject jsonObject = (JSONObject) new JSONParser().parse("{\"type\":\"text\"}");
        EIdGeneratorService eIdGeneratorService = new EIdGeneratorService();
        JSONContext jsonContext = new JSONContextImpl();

        JSONObject amendedObject = eIdGeneratorService.erzeugeEIdsFuerTeilbaum(jsonObject, jsonContext,
            RechtsetzungsdokumentForm.STAMMFORM);
        assertTrue(collectEIds(amendedObject).isEmpty());
    }


    @Test
    @SneakyThrows
    void testErzeugeEIdsFuerTeilbaum_SimpleObject_2() {
        JSONObject jsonObject = (JSONObject) new JSONParser().parse("{"
            + "    \"type\": \"doc\","
            + "    \"children\": ["
            + "        {"
            + "            \"type\": \"p\""
            + "        }"
            + "    ]"
            + "}");
        EIdGeneratorService eIdGeneratorService = new EIdGeneratorService();
        JSONContext jsonContext = new JSONContextImpl();

        JSONObject amendedObject = eIdGeneratorService.erzeugeEIdsFuerTeilbaum(jsonObject, jsonContext,
            RechtsetzungsdokumentForm.STAMMFORM);

        List<String> eIds = collectEIds(amendedObject);
        assertEquals(1, eIds.size());
        assertEquals("text-1", eIds.get(0));
    }


    @Test
    @SneakyThrows
    void testErzeugeEIdsFuerTeilbaum_SimpleObject_3() {
        JSONObject jsonObject = (JSONObject) new JSONParser().parse("{"
            + "    \"type\": \"doc\","
            + "    \"children\": ["
            + "        {"
            + "            \"type\": \"p\""
            + "        },"
            + "        {"
            + "            \"type\": \"p\""
            + "        }"
            + "    ]"
            + "}");
        EIdGeneratorService eIdGeneratorService = new EIdGeneratorService();
        JSONContext jsonContext = new JSONContextImpl();

        JSONObject amendedObject = eIdGeneratorService.erzeugeEIdsFuerTeilbaum(jsonObject, jsonContext,
            RechtsetzungsdokumentForm.STAMMFORM);

        List<String> eIds = collectEIds(amendedObject);
        assertEquals(2, eIds.size());
        assertEquals(List.of("text-1", "text-2"), eIds);
    }


    @Test
    @SneakyThrows
    void testErzeugeEIdsFuerTeilbaum_SimpleObject_4() {
        JSONObject jsonObject = (JSONObject) new JSONParser().parse("{"
            + "    \"type\": \"doc\","
            + "    \"children\": ["
            + "        {"
            + "            \"type\": \"p\""
            + "        },"
            + "        {"
            + "            \"type\": \"block\""
            + "        }"
            + "    ]"
            + "}");
        EIdGeneratorService eIdGeneratorService = new EIdGeneratorService();
        JSONContext jsonContext = new JSONContextImpl();

        JSONObject amendedObject = eIdGeneratorService.erzeugeEIdsFuerTeilbaum(jsonObject, jsonContext,
            RechtsetzungsdokumentForm.STAMMFORM);

        List<String> eIds = collectEIds(amendedObject);
        assertEquals(2, eIds.size());
        assertEquals(List.of("text-1", "block-1"), eIds);
    }


    @Test
    @SneakyThrows
    void testErzeugeEIdsFuerTeilbaum_SimpleObject_5() {
        JSONObject jsonObject = (JSONObject) new JSONParser().parse("{"
            + "    \"type\": \"doc\","
            + "    \"children\": ["
            + "        {"
            + "            \"type\": \"akn:p\""
            + "        },"
            + "        {"
            + "            \"type\": \"akn:block\""
            + "        },"
            + "        {"
            + "            \"type\": \"akn:p\""
            + "        }"
            + "    ]"
            + "}");
        EIdGeneratorService eIdGeneratorService = new EIdGeneratorService();
        JSONContext jsonContext = new JSONContextImpl();

        JSONObject amendedObject = eIdGeneratorService.erzeugeEIdsFuerTeilbaum(jsonObject, jsonContext,
            RechtsetzungsdokumentForm.STAMMFORM);

        List<String> eIds = collectEIds(amendedObject);
        assertEquals(List.of("text-1", "block-1", "text-2"), eIds);
    }


    @Test
    @SneakyThrows
    void testErzeugeEIdsFuerTeilbaum_SimpleObject_6() {
        JSONObject jsonObject = (JSONObject) new JSONParser().parse("{"
            + "    \"type\": \"akn:p\","
            + "    \"children\": []"
            + "}");

        String testPraefix = "container-e_inhalt-1_inhaltabschnitt-e.2";
        EIdGeneratorService eIdGeneratorService = new EIdGeneratorService();
        JSONContext jsonContext = JSONContextImpl.createFrom(Map.of("akn:p", 3));
        jsonContext.setPraefix(testPraefix);

        JSONObject amendedObject = eIdGeneratorService.erzeugeEIdsFuerTeilbaum(jsonObject, jsonContext,
            RechtsetzungsdokumentForm.STAMMFORM);

        List<String> eIds = collectEIds(amendedObject);
        assertEquals(List.of(testPraefix + "_text-3"), eIds);
    }


    @Test
    @SneakyThrows
    void testErzeugeEIdsFuerTeilbaum_Einzelvorschrift() {
        JSONObject jsonObject = (JSONObject) new JSONParser().parse("{"
            + "    \"type\": \"akn:article\","
            + "    \"children\": []"
            + "}");

        EIdGeneratorService eIdGeneratorService = new EIdGeneratorService();

        // Variant 1: no context; Stammform
        JSONContext jsonContext = JSONContextImpl.createFrom(Map.of("akn:article", 1));
        JSONObject amendedObject = eIdGeneratorService.erzeugeEIdsFuerTeilbaum(jsonObject, jsonContext,
            RechtsetzungsdokumentForm.STAMMFORM);
        List<String> eIds = collectEIds(amendedObject);
        assertEquals(List.of("para-1"), eIds);

        // Variant 2: with context; Mantelform
        jsonContext = JSONContextImpl.createFrom(Map.of("akn:article", 17));
        jsonContext.setPraefix("abschnitt-1_bezeichnung-1");
        amendedObject = eIdGeneratorService.erzeugeEIdsFuerTeilbaum(jsonObject, jsonContext,
            RechtsetzungsdokumentForm.MANTELFORM);
        eIds = collectEIds(amendedObject);
        assertEquals(List.of("abschnitt-1_bezeichnung-1_art-17"), eIds);
    }


    @Test
    @SneakyThrows
    void testErzeugeEIdsFuerTeilbaum_Einzelvorschrift_2() {
        JSONObject jsonObject = (JSONObject) new JSONParser().parse(
            "{\"type\": \"akn:article\", \"children\": ["
                + "        {\"type\": \"akn:num\", \"children\": ["
                + "                {\"type\": \"akn:marker\"}]"
                + "        },"
                + "        {\"type\": \"akn:heading\""
                + "        },"
                + "        {\"type\": \"akn:paragraph\", \"children\": ["
                + "                {\"type\": \"akn:num\", \"children\": ["
                + "                        {\"type\": \"akn:marker\"}]"
                + "                },"
                + "                {\"type\": \"akn:content\", \"children\": ["
                + "                        {\"type\": \"akn:p\"}]"
                + "                }]"
                + "        }]}");

        assertNotNull(jsonObject);
        EIdGeneratorService eIdGeneratorService = new EIdGeneratorService();

        JSONContext jsonContext = JSONContextImpl.createFrom(Map.of("akn:article", 3));
        jsonContext.setPraefix("abschnitt-4");
        JSONObject amendedObject = eIdGeneratorService.erzeugeEIdsFuerTeilbaum(jsonObject, jsonContext,
            RechtsetzungsdokumentForm.STAMMFORM);
        List<String> eIds = collectEIds(amendedObject);
        assertEquals(List.of("abschnitt-4_para-3", "abschnitt-4_para-3_bezeichnung-1",
            "abschnitt-4_para-3_bezeichnung-1_zaehlbez-1", "abschnitt-4_para-3_überschrift-1",
            "abschnitt-4_para-3_abs-1", "abschnitt-4_para-3_abs-1_bezeichnung-1",
            "abschnitt-4_para-3_abs-1_bezeichnung-1_zaehlbez-1", "abschnitt-4_para-3_abs-1_inhalt-1",
            "abschnitt-4_para-3_abs-1_inhalt-1_text-1"), eIds);
    }


    @Test
    @SneakyThrows
    void testErzeugeEIdsFuerDokument_AnschreibenVorlage() {
        String jsonString =
            getContentOfTextResource("/services/eid/template-stammgesetz-anschreiben.LDML.de-1.2.json");
        JSONArray jsonArray = (JSONArray) new JSONParser().parse(jsonString);
        JSONObject jsonObject = (JSONObject) jsonArray.get(0);
        assertNotNull(jsonObject);

        List<String> expectedEIds = List.of(
            "meta-1", "meta-1_ident-1", "meta-1_ident-1_frbrwork-1", "meta-1_ident-1_frbrwork-1_frbrthis-1",
            "meta-1_ident-1_frbrwork-1_frbruri-1", "meta-1_ident-1_frbrwork-1_frbrdate-1", "meta-1_ident-1_frbrwork-1_frbrauthor-1",
            "meta-1_ident-1_frbrwork-1_frbrcountry-1", "meta-1_ident-1_frbrwork-1_frbrnumber-1", "meta-1_ident-1_frbrexpression-1",
            "meta-1_ident-1_frbrexpression-1_frbrthis-1", "meta-1_ident-1_frbrexpression-1_frbruri-1",
            "meta-1_ident-1_frbrexpression-1_frbrdate-1", "meta-1_ident-1_frbrexpression-1_frbrauthor-1",
            "meta-1_ident-1_frbrexpression-1_frbrersionnumber-1", "meta-1_ident-1_frbrexpression-1_frbrlanguage-1",
            "meta-1_ident-1_frbrmanifestation-1", "meta-1_ident-1_frbrmanifestation-1_frbrthis-1",
            "meta-1_ident-1_frbrmanifestation-1_frbruri-1", "meta-1_ident-1_frbrmanifestation-1_frbrdate-1",
            "meta-1_ident-1_frbrmanifestation-1_frbrauthor-1", "meta-1_proprietary-1", "einleitung-1", "einleitung-1_text-1",
            "einleitung-1_text-1_docauth-1",
            "einleitung-1_text-1_drucksachennr-1", "einleitung-1_text-1_docdatum-1", "einleitung-1_text-1_inline-1",
            "einleitung-1_doktitel-1", "einleitung-1_doktitel-1_text-1", "einleitung-1_doktitel-1_text-1_docstadium-1",
            "einleitung-1_doktitel-1_text-1_docproponent-1", "einleitung-1_doktitel-1_text-1_doctitel-1",
            "einleitung-1_doktitel-1_text-1_kurztitel-1", "einleitung-1_doktitel-1_text-1_kurztitel-1_inline-1",
            "einleitung-1_inhaltabschnitt-1", "einleitung-1_inhaltabschnitt-1_text-1",
            "einleitung-1_inhaltabschnitt-1_text-1_ort-1", "einleitung-1_inhaltabschnitt-1_text-1_fktbez-1",
            "einleitung-1_inhaltabschnitt-1_text-1_datum-1", "einleitung-1_inhaltabschnitt-2",
            "einleitung-1_inhaltabschnitt-2_text-1", "einleitung-1_inhaltabschnitt-2_text-1_fktbez-1",
            "einleitung-1_inhaltabschnitt-2_text-1_fktbez-2", "einleitung-1_inhaltabschnitt-2_text-1_person-1", "hauptteil-1",
            "hauptteil-1_text-1", "hauptteil-1_text-2", "hauptteil-1_text-2_ref-1", "hauptteil-1_text-2_org-1",
            "hauptteil-1_text-3", "hauptteil-1_text-3_doctitel-1", "hauptteil-1_text-4", "hauptteil-1_text-5",
            "hauptteil-1_text-6", "hauptteil-1_text-7"
        );
        EIdGeneratorService eIdGeneratorService = new EIdGeneratorService();
        JSONObject amendedObject = eIdGeneratorService.erzeugeEIdsFuerDokument(jsonObject);

        List<String> actualEIds = collectEIds(amendedObject);
        assertEquals(expectedEIds, actualEIds);
    }


    @Test
    @SneakyThrows
    void testErzeugeEIdsFuerTeilbaum_Ausnahme_OrderedList() {
        JSONObject jsonObject = (JSONObject) new JSONParser().parse(
            "{"
                + "    \"type\": \"akn:authorialNote\","
                + "    \"children\": ["
                + "        {\"type\": \"akn:p\"},"
                + "        {\"type\": \"akn:ol\","
                + "            \"children\": ["
                + "                {\"type\": \"akn:li\", \"value\": \"1.\", \"children\": [{\"type\": \"akn:p\"}]},"
                + "                {\"type\": \"akn:li\", \"value\": \"a.\", \"children\": [{\"type\": \"akn:p\"}]},"
                + "                {\"type\": \"akn:li\", \"value\": \"(3)\", \"children\": [{\"type\": \"akn:p\"}]},"
                + "                {\"type\": \"akn:li\", \"value\": \"(iv)\", \"children\": [{\"type\": \"akn:p\"}]},"
                + "                {\"type\": \"akn:li\", \"value\": \"V\", \"children\": [{\"type\": \"akn:p\"}]}"
                + "            ]"
                + "        }"
                + "    ]"
                + "}");
        assertNotNull(jsonObject);
        EIdGeneratorService eIdGeneratorService = new EIdGeneratorService();

        JSONContext jsonContext = JSONContextImpl.createFrom(Map.of("akn:authorialNote", 1));
        jsonContext.setPraefix("para-22_überschrift-1");
        JSONObject amendedObject = eIdGeneratorService.erzeugeEIdsFuerTeilbaum(jsonObject, jsonContext,
            RechtsetzungsdokumentForm.STAMMFORM);

        List<String> eIds = collectEIds(amendedObject);
        assertEquals(List.of(
                "para-22_überschrift-1_fnote-1",
                "para-22_überschrift-1_fnote-1_text-1",
                "para-22_überschrift-1_fnote-1_listegeor-1",
                "para-22_überschrift-1_fnote-1_listegeor-1_listenelem-1",
                "para-22_überschrift-1_fnote-1_listegeor-1_listenelem-1_text-1",
                "para-22_überschrift-1_fnote-1_listegeor-1_listenelem-a",
                "para-22_überschrift-1_fnote-1_listegeor-1_listenelem-a_text-1",
                "para-22_überschrift-1_fnote-1_listegeor-1_listenelem-3",
                "para-22_überschrift-1_fnote-1_listegeor-1_listenelem-3_text-1",
                "para-22_überschrift-1_fnote-1_listegeor-1_listenelem-iv",
                "para-22_überschrift-1_fnote-1_listegeor-1_listenelem-iv_text-1",
                "para-22_überschrift-1_fnote-1_listegeor-1_listenelem-v",
                "para-22_überschrift-1_fnote-1_listegeor-1_listenelem-v_text-1"),
            eIds);
    }


    @Test
    @SneakyThrows
    void testErzeugeEIdsFuerTeilbaum_Ausnahme_Gliederungselemente_1() {
        String jsonString = getContentOfTextResource("/services/eid/testdaten-einzelvorschrift-1.json");
        JSONObject jsonObject = (JSONObject) new JSONParser().parse(jsonString);
        assertNotNull(jsonObject);
        EIdGeneratorService eIdGeneratorService = new EIdGeneratorService();

        JSONContext jsonContext = JSONContextImpl.createFrom(Map.of("akn:section", 5));
        JSONObject amendedObject = eIdGeneratorService.erzeugeEIdsFuerTeilbaum(jsonObject, jsonContext,
            RechtsetzungsdokumentForm.STAMMFORM);

        List<String> eIds = collectEIds(amendedObject);
        assertEquals(List.of(
            "abschnitt-2b", "abschnitt-2b_bezeichnung-1", "abschnitt-2b_bezeichnung-1_zaehlbez-1",
            "abschnitt-2b_para-11", "abschnitt-2b_para-11_bezeichnung-1", "abschnitt-2b_para-11_bezeichnung-1_zaehlbez-1",
            "abschnitt-2b_para-11a", "abschnitt-2b_para-11a_bezeichnung-1", "abschnitt-2b_para-11a_bezeichnung-1_zaehlbez-1",
            "abschnitt-2b_para-12", "abschnitt-2b_para-12_bezeichnung-1", "abschnitt-2b_para-12_bezeichnung-1_zaehlbez-1",
            "abschnitt-2b_para-22j", "abschnitt-2b_para-22j_bezeichnung-1", "abschnitt-2b_para-22j_bezeichnung-1_zaehlbez-1"
        ), eIds);

    }


    private List<String> collectEIds(JSONObject jsonObject) {
        List<String> eIds = new ArrayList<>();
        traverseTree(jsonObject, eIds);
        return eIds;
    }


    private void traverseTree(JSONObject jsonObject, List<String> eIds) {
        String eId = getEId(jsonObject);
        if (eId != null) {
            eIds.add(eId);
        }

        JSONArray children = (JSONArray) jsonObject.get(JSON_KEY_CHILDREN);
        if (children != null) {
            iterateArray(children, eIds);
        }
    }


    @SuppressWarnings("unchecked")
    private void iterateArray(JSONArray jsonArray, List<String> eIds) {
        jsonArray.forEach(obj -> {
            JSONObject jsonObject = (JSONObject) obj;
            traverseTree(jsonObject, eIds);
        });
    }


    private String getEId(JSONObject jsonObject) {
        return (String) jsonObject.get(JSON_KEY_EID);
    }

}
