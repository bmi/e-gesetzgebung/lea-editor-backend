// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.model.MediaData;
import de.itzbund.egesetz.bmi.lea.domain.model.MediaMetadata;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.Test;

import javax.sql.rowset.serial.SerialBlob;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

@Log4j2
class MediaTest {

    @Test
    void testCreateMediaMetadata() {
        String uuidRandom = UUID.randomUUID().toString();
        String testData = "Lorem ipsum dolor sit amet, "
            + "consetetur sadipscing elitr, "
            + "sed diam nonumy eirmod tempor invidunt ut "
            + "labore et dolore magna aliquyam erat, sed diam voluptua. "
            + "At vero eos et accusam et justo duo dolores et ea rebum. "
            + "Stet clita kasd gubergren, "
            + "no sea takimata sanctus est Lorem ipsum dolor sit amet.";

        MediaData mediaDataEntity = null;
        try {
            mediaDataEntity = MediaData.builder()
                .id(DigestUtils.sha256Hex(testData))
                .data(new SerialBlob(testData.getBytes(StandardCharsets.UTF_8)))
                .build();
        } catch (SQLException e) {
            log.error("test: could not create SerialBlob " + e);
            fail("test: could not create SerialBlob");
        }

        assertEquals(DigestUtils.sha256Hex(testData), mediaDataEntity.getId());
        assertNotNull(mediaDataEntity.getData());

        MediaMetadata metadataEntity = MediaMetadata.builder()
            .mediaType("application/test")
            .name("test")
            .id(UUID.fromString(uuidRandom))
            .mediaData(mediaDataEntity)
            .build();

        assertEquals(uuidRandom, metadataEntity.getId().toString());
        assertEquals("test", metadataEntity.getName());
        assertEquals(mediaDataEntity, metadataEntity.getMediaData());
        assertEquals("application/test", metadataEntity.getMediaType());
    }

}
