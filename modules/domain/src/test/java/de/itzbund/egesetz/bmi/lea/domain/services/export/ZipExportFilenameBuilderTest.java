// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.export;

import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests for {@link ZipExportFilenameBuilder}.
 */
@Log4j2
class ZipExportFilenameBuilderTest {

    ZipExportFilenameBuilder zipName;


    @BeforeEach
    void init() {
        zipName = new ZipExportFilenameBuilder();
    }


    @Test
    void testGetSingleDocumentFilename() {
        Calendar time1 = new GregorianCalendar(2022, Calendar.JANUARY, 1, 10, 30, 5);
        DocumentDomain doc1 = DocumentDomain.builder().title("test").type(DocumentType.BEGRUENDUNG_STAMMGESETZ).build();
        String filename = zipName.getSingleDocumentFilename(time1.getTimeInMillis(), doc1);
        assertEquals("20220101_1030_Export_Stammgesetz-Begründung_test.xml", filename);
        System.out.println(getLogMsgSin(time1, doc1, filename));

        doc1.setTitle(" Schöne neue Welt .");
        doc1.setType(DocumentType.REGELUNGSTEXT_STAMMGESETZ);
        filename = zipName.getSingleDocumentFilename(time1.getTimeInMillis(), doc1);
        assertEquals("20220101_1030_Export_Stammgesetz-Regelungstext_Schöne neue Welt.xml", filename);
        System.out.println(getLogMsgSin(time1, doc1, filename));

        doc1.setTitle(".  :TEST:x...");
        doc1.setType(DocumentType.VORBLATT_STAMMGESETZ);
        filename = zipName.getSingleDocumentFilename(time1.getTimeInMillis(), doc1);
        assertEquals("20220101_1030_Export_Stammgesetz-Vorblatt_-TEST-x.xml", filename);
        System.out.println(getLogMsgSin(time1, doc1, filename));

        // 👻 ::: 😀@😜 /COM4\ 😶 🙄 😩 *🤡? ßßßö
        doc1.setTitle(".\"\uD83D\uDC7B ::: \uD83D\uDE00@\uD83D\uDE1C /COM4\\ \uD83D\uDE36 \t \uD83D\uDE44 "
            + "\uD83D\uDE29 "
            + "*\uD83E\uDD21?\" ßßßö");
        doc1.setType(DocumentType.ANLAGE);
        filename = zipName.getSingleDocumentFilename(time1.getTimeInMillis(), doc1);
        assertEquals("20220101_1030_Export_Anlage_-\uD83D\uDC7B --- \uD83D\uDE00@\uD83D\uDE1C -COM4- "
            + "\uD83D\uDE36  \uD83D\uDE44 \uD83D\uDE29 -\uD83E\uDD21-- ßßßö.xml", filename);
        System.out.println(getLogMsgSin(time1, doc1, filename));

        Calendar time2 = new GregorianCalendar(1979, Calendar.NOVEMBER, 11, 11, 11, 25);
        DocumentDomain doc2 =
            DocumentDomain.builder().title("EUArchRV2 - Begründung ").type(DocumentType.BEGRUENDUNG_STAMMGESETZ)
                .build();
        filename = zipName.getSingleDocumentFilename(time2.getTimeInMillis(), doc2);
        assertEquals("19791111_1111_Export_Stammgesetz-Begründung_EUArchRV2 - Begründung.xml", filename);
        System.out.println(getLogMsgSin(time2, doc2, filename));

        doc2.setType(DocumentType.REGELUNGSTEXT_STAMMGESETZ);
        doc2.setTitle("Die zweite Europäische Regelwerksverordnung zum Aufstocken der RV-Kennzahlen");
        filename = zipName.getSingleDocumentFilename(time2.getTimeInMillis(), doc2);
        assertEquals("19791111_1111_Export_Stammgesetz-Regelungstext_Die zweite Europäische Regelwerksverordnung"
            + " zum Aufstocken der RV-Kennzahlen.xml", filename);
        System.out.println(getLogMsgSin(time2, doc2, filename));

        doc2.setType(DocumentType.VORBLATT_STAMMGESETZ);
        doc2.setTitle("COM1");
        filename = zipName.getSingleDocumentFilename(time2.getTimeInMillis(), doc2);
        assertEquals("19791111_1111_Export_Stammgesetz-Vorblatt_--ohne-Titel--.xml", filename);
        System.out.println(getLogMsgSin(time2, doc2, filename));
    }


    @Test
    void testGetCompoundDocumentFilename() {
        Calendar time = new GregorianCalendar(2000, Calendar.JANUARY, 30, 1, 0, 0);
        CompoundDocumentDomain doc = CompoundDocumentDomain.builder().build();
        String filename = zipName.getCompoundDocumentFilename(time.getTimeInMillis(), doc,
            ZipExportFilenameBuilder.FileFormat.ARCHIVE);
        assertEquals("20000130_0100_Export_--ohne-Titel--.zip", filename);
        System.out.println(getLogMsgCom(time, doc, filename));

        time = new GregorianCalendar(2000, Calendar.FEBRUARY, 29, 2, 0, 0);
        doc.setTitle("Regelungsentwurf 345/00");
        filename = zipName.getCompoundDocumentFilename(time.getTimeInMillis(), doc,
            ZipExportFilenameBuilder.FileFormat.ARCHIVE);
        assertEquals("20000229_0200_Export_Regelungsentwurf 345-00.zip", filename);
        System.out.println(getLogMsgCom(time, doc, filename));

        time = new GregorianCalendar(2000, Calendar.MARCH, 28, 3, 0, 0);
        doc.setTitle("Neues Gesetz: ABCG");
        filename = zipName.getCompoundDocumentFilename(time.getTimeInMillis(), doc,
            ZipExportFilenameBuilder.FileFormat.ARCHIVE);
        assertEquals("20000328_0300_Export_Neues Gesetz- ABCG.zip", filename);
        System.out.println(getLogMsgCom(time, doc, filename));
    }


    private String getLogMsgSin(Calendar time, DocumentDomain doc, String filename) {
        return String.format("Time: %td.%tm.%tY    DocType: %s\nText: %s\nFile: %s", time, time, time,
            doc.getType().getReadableName(), doc.getTitle(), filename);
    }


    private String getLogMsgCom(Calendar time, CompoundDocumentDomain doc, String filename) {
        return String.format("Time: %td.%tm.%tY    DocType: Dokumentenmappe\nText: %s\nFile: %s", time, time, time,
            doc.getTitle(), filename);
    }

}
