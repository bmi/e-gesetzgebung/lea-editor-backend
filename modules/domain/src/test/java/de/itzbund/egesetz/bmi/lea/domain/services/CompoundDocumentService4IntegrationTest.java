// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtListDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.Rights;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
class CompoundDocumentService4IntegrationTest extends AbstractCompoundDocumentServiceTest {

    @MockBean
    private CompoundDocument compoundDocument;

    @Test
    void whenInUserHasNoAktion_thenReadAndWriteShouldBeFalse() {
        when(rbacPermissionService.getAccessRightsForCurrentUserOnGivenResource(anyString(), any()))
            .thenReturn(Collections.emptyList());

        Map<String, Boolean> actual = compoundDocumentService.getAccessRightsForCompoundDocument(new CompoundDocumentId(UUID.randomUUID()));

        assertThat(actual).isEqualTo(new HashMap<>(Map.of("KOMMENTARE_LESEN", false, "KOMMENTIEREN", false, "LESEN", false, "SCHREIBEN", false)));
    }

    @Test
    void whenInUserHasAktion_thenReadAndWriteShouldBeTrue() {
        when(rbacPermissionService.getAccessRightsForCurrentUserOnGivenResource(anyString(), any()))
            .thenReturn(List.of(Rights.LESEN, Rights.SCHREIBEN));

        Map<String, Boolean> actual = compoundDocumentService.getAccessRightsForCompoundDocument(new CompoundDocumentId(UUID.randomUUID()));

        assertThat(actual).isEqualTo(new HashMap<>(Map.of("KOMMENTARE_LESEN", false, "KOMMENTIEREN", false, "LESEN", true, "SCHREIBEN", true)));
    }

    @Test
    void testChangeCompoundDocumentStateByPlatform() {
        User userEntity = TestObjectsUtil.getUserEntity();

        when(compoundDocument.getCompoundDocumentWithDocumentsForIdAndOptionalFiltered(any(CompoundDocumentId.class), any())).thenReturn(compoundDocument);
        when(compoundDocument.getCompoundDocumentEntity()).thenReturn(compoundDocumentEntity);

        ServiceState actual = compoundDocumentService.changeCompoundDocumentStateByPlatform(userEntity.getGid().getId(),
            new CompoundDocumentId(UUID.randomUUID()), DocumentState.FINAL);

        assertThat(actual).isEqualTo(ServiceState.OK);
    }

    @Test
    void testChangeCompoundDocumentStateByPlatformCompoundDocumentNotFound() {
        User userEntity = TestObjectsUtil.getUserEntity();
        when(compoundDocument.getCompoundDocumentWithDocumentsForIdAndOptionalFiltered(any(CompoundDocumentId.class), any())).thenReturn(null);

        ServiceState actual = compoundDocumentService.changeCompoundDocumentStateByPlatform(userEntity.getGid().getId(),
            new CompoundDocumentId(UUID.randomUUID()), DocumentState.FINAL);

        assertThat(actual).isEqualTo(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
    }


}
