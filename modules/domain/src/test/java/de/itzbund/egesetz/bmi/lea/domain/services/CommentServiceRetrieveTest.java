// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.enums.CommentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class CommentServiceRetrieveTest extends CommentServiceTest {

    @Test
    void testLoadComments_whenNoComments_thenReturnEmptyList() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        DocumentId documentId = new DocumentId(UUID.randomUUID());

        when(documentService.isDocumentExisting(any())).thenReturn(true);
        when(documentService.getDocumentByIdNEW(any())).thenReturn(new Document(DocumentDomain.builder().build()));
        when(documentPermissionValidator.isUserOwnerOfDocument(any(), any())).thenReturn(true);
        when(commentPersistencePort.findByDocumentId(any())).thenReturn(List.of());

        // Act
        List<Comment> commentEntities = commentService.loadAllCommentsOfUserOfDocument(documentId, status);

        // Assert
        assertFalse(status.isEmpty());
        assertEquals(ServiceState.THERE_ARE_NO_COMMENTS, status.get(0));
        assertNotNull(commentEntities);
        assertTrue(commentEntities.isEmpty());
    }


    @Test
    void testSetStatus_whenDocumentNotExists_thenFail_NotFound() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        when(documentService.getDocumentEntityById(any())).thenReturn(null);

        // Act
        commentService.setCommentState(
            new DocumentId(UUID.randomUUID()),
            new CommentId(UUID.randomUUID()),
            CommentState.CLOSED,
            status
        );

        // Assert
        assertEquals(1, status.size());
        assertEquals(ServiceState.DOCUMENT_NOT_FOUND, status.get(0));
    }


    @Test
    void testSetStatus_whenCommentNotExists_thenFail_NotFound() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        when(documentService.getDocumentEntityById(any())).thenReturn(DocumentDomain.builder().build());
        when(commentPersistencePort.findByCommentId(any())).thenReturn(null);

        // Act
        commentService.setCommentState(
            new DocumentId(UUID.randomUUID()),
            new CommentId(UUID.randomUUID()),
            CommentState.CLOSED,
            status
        );

        // Assert
        assertEquals(1, status.size());
        assertEquals(ServiceState.COMMENT_NOT_FOUND, status.get(0));
    }


    @Test
    void testSetStatus_whenUserNotDocOwner_thenFail_NoPermission() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        when(documentService.getDocumentEntityById(any())).thenReturn(DocumentDomain.builder().build());
        when(commentPersistencePort.findByCommentId(any())).thenReturn(Comment.builder().build());
        when(documentPermissionValidator.isUserOwnerOfDocumentEntity(any())).thenReturn(false);

        // Act
        commentService.setCommentState(
            new DocumentId(UUID.randomUUID()),
            new CommentId(UUID.randomUUID()),
            CommentState.CLOSED,
            status
        );

        // Assert
        assertEquals(1, status.size());
        assertEquals(ServiceState.NO_PERMISSION, status.get(0));
    }


    @Test
    void testSetStatus_whenStatusUpdateRequested_thenSuccess() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        when(documentService.getDocumentEntityById(any())).thenReturn(DocumentDomain.builder().build());
        when(commentPersistencePort.findByCommentId(any())).thenReturn(Comment.builder().build());
        when(documentPermissionValidator.isUserOwnerOfDocumentEntity(any())).thenReturn(true);
        when(commentPersistencePort.save(any())).thenReturn(Comment.builder().build());

        // Act
        commentService.setCommentState(
            new DocumentId(UUID.randomUUID()),
            new CommentId(UUID.randomUUID()),
            CommentState.CLOSED,
            status
        );

        // Assert
        assertEquals(1, status.size());
        assertEquals(ServiceState.OK, status.get(0));
    }

}
