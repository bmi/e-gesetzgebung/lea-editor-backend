// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.aggregate;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;
import org.junit.platform.suite.api.SuiteDisplayName;

@Suite
@SuiteDisplayName("Dokumentenmappen Test Suite")
@SelectClasses({CompoundDocumentDomain1IntegrationTest.class, CompoundDocumentDomain2IntegrationTest.class,
    CompoundDocumentDomain3IntegrationTest.class})
public class CompoundDocumentDomainTestSuite {

}
