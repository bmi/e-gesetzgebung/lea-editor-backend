// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.ProponentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RessortEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.RegelungsvorhabenTypType;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.EIdGeneratorService;
import de.itzbund.egesetz.bmi.lea.domain.services.export.ExportService;
import de.itzbund.egesetz.bmi.lea.domain.services.export.ZipExportFilenameBuilder;
import de.itzbund.egesetz.bmi.lea.domain.services.filters.ChangeAnnotationFilter;
import de.itzbund.egesetz.bmi.lea.domain.services.filters.EIdGeneratorFilter;
import de.itzbund.egesetz.bmi.lea.domain.services.logging.DomainLoggingService;
import de.itzbund.egesetz.bmi.lea.domain.services.meta.MetadatenUpdateService;
import de.itzbund.egesetz.bmi.lea.domain.util.TestUtil;
import lombok.SneakyThrows;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.core.util.Utils.setReflectiveAttribute;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class, SpringExtension.class})
@SuppressWarnings("unused")
public class ExportServiceTest {

    private static final String jsonContent = Utils.getContentOfTextResource(
        "/compare/Regelungstext-V1.json");

    @InjectMocks
    private ExportService exportService;

    @MockBean
    private CompoundDocumentPersistencePort compoundDocumentRepository;
    @MockBean
    private DokumentPersistencePort documentRepository;
    @MockBean
    private RegelungsvorhabenService regelungsvorhabenService;
    @MockBean
    private DownloadService downloadService;
    @MockBean
    private MetadatenUpdateService metadatenUpdateService;
    @MockBean
    private EIdGeneratorService eIdGeneratorService;
    @MockBean
    private ConverterService converterService;
    @MockBean
    private ZipExportFilenameBuilder filenameBuilder;
    @MockBean
    private CollectionDocumentBuilder collectionDocumentBuilder;
    @MockBean
    private PropositionMapper propositionMapper;
    @MockBean
    private EIdGeneratorFilter eIdGeneratorFilter;
    @MockBean
    private ChangeAnnotationFilter changeAnnotationFilter;
    @MockBean
    private PlategRequestService plategRequestService;
    @MockBean
    private UserService userService;
    @MockBean
    private DomainLoggingService domainLoggingService;

    @Test
    void testExportSingleDocument_whenJSONInvalid_thenFailure() {
        // Arrange
        String jsonString = "";
        List<ServiceState> status = new ArrayList<>();

        // Act
        String returnValue = exportService.exportSingleDocument(jsonString, status);

        // Assert
        assertNull(returnValue);
        assertEquals(1, status.size());
        assertEquals(ServiceState.INVALID_ARGUMENTS, status.get(0));
    }

    @Test
    @SneakyThrows
    void testExportSingleDocument_whenXMLInvalid_thenFailure() {
        // Arrange
        String jsonString = "{}";
        List<ServiceState> status = new ArrayList<>();

        setReflectiveAttribute(eIdGeneratorFilter, "eIdGeneratorService", eIdGeneratorService);

        when(eIdGeneratorService.erzeugeEIdsFuerDokument(any()))
            .thenAnswer(invocation -> {
                return invocation.getArgument(0);
            });

        // Act
        String returnValue = exportService.exportSingleDocument(jsonString, status);

        // Assert
        assertNull(returnValue);
        assertEquals(1, status.size());
        assertEquals(ServiceState.XML_TRANSFORM_ERROR, status.get(0));
    }

    @Test
    @SneakyThrows
    void testExportSingleDocument_whenXMLValid_thenSuccess() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();

        setReflectiveAttribute(eIdGeneratorFilter, "eIdGeneratorService", eIdGeneratorService);

        when(eIdGeneratorService.erzeugeEIdsFuerDokument(any()))
            .thenAnswer(invocation -> {
                return invocation.getArgument(0);
            });

        when(converterService.convertJsonToXml(any())).thenReturn("test");

        when(userService.getUser()).thenReturn(TestUtil.getNonexistingUser());

        // Act
        String returnValue = exportService.exportSingleDocument(jsonContent, status);

        // Assert
        assertNotNull(returnValue);
        assertEquals(1, status.size());
        assertEquals(ServiceState.OK, status.get(0));
    }

    @Test
    void testExportCompoundDocument_whenDMNotFound_thenFailure() {
        // Arrange
        MockHttpServletResponse response = new MockHttpServletResponse();
        List<ServiceState> status = new ArrayList<>();

        when(compoundDocumentRepository.findByCompoundDocumentId(any())).thenReturn(Optional.empty());

        when(userService.getUser()).thenReturn(TestUtil.getNonexistingUser());

        // Act
        exportService.exportCompoundDocument(response, UUID.randomUUID(), status);

        // Assert
        assertNotNull(response);
        assertEquals(1, status.size());
        assertEquals(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND, status.get(0));
    }

    @Test
    void testExportCompoundDocument_whenXMLInvalid_thenFailure() {
        // Arrange
        MockHttpServletResponse response = new MockHttpServletResponse();
        List<ServiceState> status = new ArrayList<>();

        when(compoundDocumentRepository.findByCompoundDocumentId(any())).thenReturn(Optional.of(
            CompoundDocumentDomain.builder()
                .regelungsVorhabenId(new RegelungsVorhabenId(UUID.randomUUID()))
                .state(DocumentState.DRAFT).build()));

        when(regelungsvorhabenService.getRegelungsvorhabenEditorDTONEW(any(), anyBoolean())).thenReturn(
            RegelungsvorhabenEditorDTO.builder().build());

        when(propositionMapper.mapD(Mockito.any(RegelungsvorhabenEditorDTO.class))).thenReturn(
            Proposition.builder().build());

        when(filenameBuilder.getCompoundDocumentFilename(anyLong(), any(), any())).thenReturn(Utils.getRandomString());

        when(documentRepository.findByCompoundDocumentId(any())).thenReturn(Optional.of(
            List.of(DocumentDomain.builder()
                .documentId(new DocumentId(UUID.randomUUID()))
                .content(Utils.getRandomString())
                .build())));

        // Act
        exportService.exportCompoundDocument(response, UUID.randomUUID(), status);

        // Assert
        assertNotNull(response);
        assertEquals(1, status.size());
        assertEquals(ServiceState.INVALID_ARGUMENTS, status.get(0));
    }

    @Test
    void testExportCompoundDocument_Success() {
        // Arrange
        MockHttpServletResponse response = new MockHttpServletResponse();
        List<ServiceState> status = new ArrayList<>();

        DocumentDomain document = DocumentDomain.builder()
            .documentId(new DocumentId(UUID.randomUUID()))
            .type(DocumentType.VORBLATT_STAMMGESETZ)
            .content(jsonContent)
            .createdAt(Instant.now())
            .build();

        UUID proponentId = UUID.randomUUID();

        when(compoundDocumentRepository.findByCompoundDocumentId(any())).thenReturn(Optional.of(
            CompoundDocumentDomain.builder()
                .regelungsVorhabenId(new RegelungsVorhabenId(UUID.randomUUID()))
                .state(DocumentState.DRAFT)
                .createdAt(Instant.now())
                .build()));

        when(plategRequestService.getRegulatoryProposalById(any())).thenReturn(
            RegelungsvorhabenEditorDTO.builder()
                .vorhabenart(RegelungsvorhabenTypType.GESETZ)
                .bearbeitetAm(Instant.now())
                .technischesFfRessort(RessortEditorDTO.builder()
                    .id(proponentId.toString())
                    .aktiv(true)
                    .build())
                .build());

        when(propositionMapper.mapD(Mockito.any(RegelungsvorhabenEditorDTO.class))).thenReturn(
            Proposition.builder()
                .type(RechtsetzungsdokumentTyp.GESETZ)
                .createdAt(Instant.now())
                .proponentActive(true)
                .proponentId(proponentId.toString())
                .build());

        when(propositionMapper.mapD(Mockito.any(Proposition.class))).thenReturn(
            RegelungsvorhabenEditorDTO.builder()
                .bearbeitetAm(Instant.now())
                .vorhabenart(RegelungsvorhabenTypType.GESETZ)
                .technischesFfRessort(RessortEditorDTO.builder()
                    .id(proponentId.toString())
                    .aktiv(true)
                    .build())
                .build());

        when(propositionMapper.map(Mockito.any(RegelungsvorhabenEditorDTO.class))).thenReturn(
            PropositionDTO.builder()
                .bearbeitetAm(Instant.now())
                .type(RechtsetzungsdokumentTyp.GESETZ)
                .proponentDTO(ProponentDTO.builder()
                    .id(proponentId)
                    .active(true)
                    .build())
                .build());

        when(filenameBuilder.getCompoundDocumentFilename(anyLong(), any(), any())).thenReturn("");

        when(filenameBuilder.getSingleDocumentFilename(anyLong(), any())).thenReturn(Utils.getRandomString());

        when(documentRepository.findByCompoundDocumentId(any())).thenReturn(Optional.of(
            List.of(document)));

        when(metadatenUpdateService.update(any(), Mockito.any(Proposition.class))).thenReturn(new JSONObject());

        when(documentRepository.save(any())).thenReturn(document);

        when(converterService.convertJsonToXml(any())).thenReturn(Utils.getRandomString());

        when(collectionDocumentBuilder.getCollectionDocumentContent(any(), any(), any(), any())).thenReturn(Utils.getRandomString());

        when(userService.getUser()).thenReturn(TestUtil.getNonexistingUser());

        doNothing().when(downloadService).downloadCompoundDocumentAsZipFile(any(), any(), anyString());

        setReflectiveAttribute(eIdGeneratorFilter, "eIdGeneratorService", eIdGeneratorService);

        // Act
        exportService.exportCompoundDocument(response, UUID.randomUUID(), status);

        // Assert
        assertNotNull(response);
        assertEquals(1, status.size());
        assertEquals(ServiceState.OK, status.get(0));
    }

}
