// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.mappers;

import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorShortDTO;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class RegelungsvorhabenMapperTest {

    private RegelungsvorhabenMapper regelungsvorhabenMapper;


    @BeforeEach
    void init() {
        regelungsvorhabenMapper = new RegelungsvorhabenMapperImpl();
    }


    @Test
    void testMapToRegelungsvorhabenEditorDTO() {
        RegelungsvorhabenEditorShortDTO reply = TestDTOUtil.getRandomRegelungsvorhabenEditorShortDTO();
        RegelungsvorhabenEditorDTO actual = regelungsvorhabenMapper.map(reply);
        assertThat(actual).hasNoNullFieldsOrPropertiesExcept(
            "bezeichnung",
            "technischesFfRessort",
            "allFfRessorts",
            "initiant",
            "farbe");
    }


    @Test
    void testMapToRegelungsvorhabenEditorShortDTO() {
        RegelungsvorhabenEditorDTO randomRegelungsvorhabenEditorDTO = TestDTOUtil.getRandomRegelungsvorhabenEditorDTO();
        RegelungsvorhabenEditorShortDTO actual = regelungsvorhabenMapper.map(randomRegelungsvorhabenEditorDTO);
        assertThat(actual).hasNoNullFieldsOrPropertiesExcept("farbe");
    }

}
