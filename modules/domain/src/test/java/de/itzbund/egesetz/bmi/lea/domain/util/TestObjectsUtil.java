// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.util;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.DeleteCommentsDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.UpdateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.FarbeType;
import de.itzbund.egesetz.bmi.lea.domain.enums.PropositionState;
import de.itzbund.egesetz.bmi.lea.domain.enums.RegelungsvorhabenTypType;
import de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentInitiant;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DrucksacheDokument;
import de.itzbund.egesetz.bmi.lea.domain.model.Position;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DruckDokumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DrucksacheDokumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.ReplyId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import lombok.Getter;
import lombok.experimental.UtilityClass;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@UtilityClass
public class TestObjectsUtil {

	public static final String CONTENT = "some CONTENT";

	public static final String DOCUMENT_TITLE = "DOCUMENT Title";
	public static final String DOCUMENT_VERSION = "Version 1";
	public static final UUID DOCUMENT_ID = UUID.randomUUID();
	public static final DocumentType DOCUMENT_TYPE = DocumentType.REGELUNGSTEXT_STAMMGESETZ;

	public static final UUID REGELUNGSVORHABEN_ID = UUID.randomUUID();
	public static final String REGELUNGSVORHABEN_ABKUERZUNG = "Abk.";
	public static final String REGELUNGSVORHABEN_BEZEICHNUNG = "Bezeichnung";
	public static final String REGELUNGSVORHABEN_KURZBESCHREIBUNG = "Kurzbeschreibung";
	public static final String REGELUNGSVORHABEN_KURZBEZEICHNUNG = "Titel, aka: Kurz Bezeichnung";
	public static final RegelungsvorhabenTypType REGELUNGSVORHABEN_ART = RegelungsvorhabenTypType.GESETZ;
	public static final VorhabenStatusType REGELUNGSVORHABEN_STATUS = VorhabenStatusType.IN_BEARBEITUNG;

	public static final UUID COMPOUND_DOCUMENT_ID = UUID.randomUUID();
	public static final String COMPOUND_DOCUMENT_TITLE = "COMPOUND DOCUMENT Title";
	public static final String COMPOUND_DOCUMENT_VERSION = "12.0";

	public static final String USER_ID = UUID.randomUUID().toString();
	public static final String USER_NAME = "TestVorname Testnachmane";
	public static final String USER_EMAIL = "Test.Nonexisting1@email.de";

	public static final Instant TIME_STAMP = Instant.now();
	public static final long ANCHOR_OFFSET = 1L;
	public static final List<Long> ANCHOR_PATH_LIST = List.of(1L, 2L, 3L);
	public static final long FOCUS_OFFSET = 2L;
	public static final List<Long> FOCUS_PATH_LIST = List.of(4L, 5L, 6L, 7L);


	// -------- User -------------
	public User getUserEntity() {
		return User.builder()
			.gid(new UserId(USER_ID))
			.plattformUserId(UUID.fromString(USER_ID))
			.name(USER_NAME)
			.email(USER_EMAIL)
			.build();
	}

	// -------- Drucksache ---------

	public DrucksacheDokument getDrucksacheDokument() {
		return DrucksacheDokument.builder()
			.drucksacheDokumentId(new DrucksacheDokumentId(UUID.randomUUID()))
			.compoundDocumentId(new CompoundDocumentId(UUID.randomUUID()))
			.druckDokumentId(new DruckDokumentId(UUID.randomUUID()))
			.createdBy(getUserEntity())
			.updatedBy(getUserEntity())
			.createdAt(TIME_STAMP)
			.updatedAt(TIME_STAMP)
			.build();
	}

	// -------- Document ---------


	public DocumentDomain getDocumentEntity() {
		DocumentId documentId = new DocumentId(DOCUMENT_ID);

		return DocumentDomain.builder()
			.documentId(documentId)
			.content(CONTENT)
			.title(DOCUMENT_TITLE)
			.type(DOCUMENT_TYPE)
			.createdAt(TIME_STAMP)
			.updatedAt(TIME_STAMP)
			.createdBy(getUserEntity())
			.updatedBy(getUserEntity())
			.compoundDocumentId(new CompoundDocumentId(COMPOUND_DOCUMENT_ID))
			.build();
	}


	public DocumentDomain getRandomDocumentEntity() {
		DocumentDomain documentEntity = getDocumentEntity();
		documentEntity.setDocumentId(new DocumentId(UUID.randomUUID()));
		documentEntity.setCompoundDocumentId(new CompoundDocumentId(COMPOUND_DOCUMENT_ID));
		documentEntity.setInheritFromId(new DocumentId(UUID.randomUUID()));
		return documentEntity;
	}


	public Document getRandomDocument() {
		return getRandomDocumentOfType(null);
	}

	public UpdateDocumentDTO getUpdateDocumentDTO() {
		UpdateDocumentDTO updateDocumentDTO = new UpdateDocumentDTO();
		updateDocumentDTO.setContent(CONTENT);
		updateDocumentDTO.setTitle(DOCUMENT_TITLE);
		updateDocumentDTO.setHdr(false);
		return updateDocumentDTO;
	}


	public DeleteCommentsDTO getDeleteCommentsDTO() {
		DeleteCommentsDTO deleteCommentsDTO = new DeleteCommentsDTO();
		deleteCommentsDTO.setContent(CONTENT);
		deleteCommentsDTO.setTitle(DOCUMENT_TITLE);
		deleteCommentsDTO.setHdr(false);
		deleteCommentsDTO.setCommentIds(List.of(UUID.randomUUID().toString()));
		return deleteCommentsDTO;
	}


	public Document getRandomDocumentOfType(DocumentType documentType) {
		Document document = new Document(null, null, null);
		document.setDocumentState(DocumentState.DRAFT);

		DocumentDomain randomDocumentEntity = getRandomDocumentEntity();

		if (documentType != null) {
			randomDocumentEntity.setType(documentType);
		}
		document.setDocumentEntity(randomDocumentEntity);

		return document;
	}


	public List<Document> getDocumentListOfSize(int size) {
		List<Document> documents = new ArrayList<>();

		for (int i = 0; i < size; i++) {
			documents.add(getRandomDocument());
		}

		return documents;
	}

	// -------- Compound document entity ----


	public CompoundDocumentDomain getCompoundDocumentEntity() {
		CompoundDocumentId compoundDocumentId = new CompoundDocumentId(COMPOUND_DOCUMENT_ID);

		return CompoundDocumentDomain.builder()
			.compoundDocumentId(compoundDocumentId)
			.title(COMPOUND_DOCUMENT_TITLE)
			.regelungsVorhabenId(new RegelungsVorhabenId(REGELUNGSVORHABEN_ID))
			.type(CompoundDocumentTypeVariant.STAMMGESETZ)
			.version(COMPOUND_DOCUMENT_VERSION)
			.state(DocumentState.DRAFT)
			.createdBy(getUserEntity())
			.updatedBy(getUserEntity())
			.createdAt(TIME_STAMP)
			.updatedAt(TIME_STAMP)
			.build();
	}


	public CompoundDocumentDomain getCompoundDocumentEntityWithSpecificId(UUID id) {
		CompoundDocumentDomain compoundDocumentEntity = getCompoundDocumentEntity();
		compoundDocumentEntity.setCompoundDocumentId(new CompoundDocumentId(id));

		return compoundDocumentEntity;
	}


	public CompoundDocumentDomain getRandomCompoundDocumentEntity() {
		return getCompoundDocumentEntityWithSpecificId(UUID.randomUUID());
	}

	// -------- Compound document -----------


	public static List<CompoundDocument> getCompoundDocumentListOfSize(int size) {
		List<CompoundDocument> compoundDocuments = new ArrayList<>();

		for (int i = 0; i < size; i++) {
			compoundDocuments.add(getRandomCompoundDocument());
		}

		return compoundDocuments;

	}


	public CompoundDocument getRandomCompoundDocument() {
		List<Document> documents = getDocumentListOfSize(4);
		return getRandomCompoundDocumentWithDocuments(documents);
	}


	public CompoundDocument getRandomCompoundDocumentWithDocuments(List<Document> documents) {
		CompoundDocument compoundDocument = new CompoundDocument();
		compoundDocument.setCompoundDocumentEntity(getCompoundDocumentEntity());

		if (documents == null) {
			documents = getDocumentListOfSize(2);
		}
		compoundDocument.setDocuments(documents);

		return compoundDocument;
	}


	public CompoundDocument getCompoundDocumentWithSpecificId(UUID uuid) {
		CompoundDocument compoundDocument = new CompoundDocument();
		compoundDocument.setCompoundDocumentEntity(getCompoundDocumentEntity());
		compoundDocument.setDocuments(getDocumentListOfSize(2));
		compoundDocument.setCompoundDocumentId(new CompoundDocumentId(uuid));

		return compoundDocument;
	}

	// -------- Comment -----------


	public Comment getCommentWithId(CommentId commentId) {
		Comment comment = getRandomComment();
		comment.setCommentId(commentId);
		return comment;
	}


	public Comment getRandomComment() {
		Document randomDocument = getRandomDocument();

		return Comment.builder()
			.documentId(randomDocument.getDocumentEntity().getDocumentId())
			.commentId(new CommentId(UUID.randomUUID()))
			.content(Utils.getRandomString())
			.anchor(Position.builder()
				.offset(ANCHOR_OFFSET)
				.path(ANCHOR_PATH_LIST)
				.build())
			.focus(Position.builder()
				.offset(FOCUS_OFFSET)
				.path(FOCUS_PATH_LIST)
				.build())
			.updatedBy(getUserEntity())
			.createdBy(getUserEntity())
			.createdAt(Instant.now())
			.updatedAt(Instant.now())
			.replies(getReplyListOfSize(3))
			.build();

	}


	public List<Reply> getReplyListOfSize(int size) {
		List<Reply> replies = new ArrayList<>();

		for (int i = 0; i < size; i++) {
			replies.add(getRandomReplyEntity());
		}

		return replies;
	}


	public Reply getRandomReplyEntity() {
		UUID uuid = UUID.randomUUID();
		return Reply.builder()
			.replyId(new ReplyId(UUID.randomUUID()))
			.commentId(new CommentId(uuid))
			.parentId(uuid.toString())
			.documentId(new DocumentId(UUID.randomUUID()))
			.content(Utils.getRandomString())
			.updatedBy(getUserEntity())
			.createdBy(getUserEntity())
			.createdAt(Instant.now())
			.updatedAt(Instant.now())
			.build();
	}


	public List<Reply> getAConnectedReplyListOfSize(int size) {
		DocumentId documentId = new DocumentId(UUID.randomUUID());
		CommentId commentId = new CommentId(UUID.randomUUID());
		List<Reply> replyList = getReplyListOfSize(size);

		for (int i = replyList.size() - 1; i > 0; i--) {
			replyList.get(i).setParentId(replyList.get(i - 1).getReplyId().getId().toString());
			replyList.get(i).setDocumentId(documentId);
			replyList.get(i).setCommentId(commentId);
		}

		replyList.get(0).setParentId(commentId.getId().toString());
		replyList.get(0).setDocumentId(documentId);
		replyList.get(0).setCommentId(commentId);

		return replyList;
	}


	public static Position getRandomPosition() {
		return Position.builder()
			.offset(123L)
			.path(List.of(1L, 2L, 3L))
			.build();
	}


	public static List<CompoundDocumentDomain> getCompoundDocumentDomainListOfSize(int size) {
		return getCompoundDocumentDomainListOfSizeForRegelungsvorhabenId(size, new RegelungsVorhabenId(REGELUNGSVORHABEN_ID));
	}

	public static List<CompoundDocumentDomain> getCompoundDocumentDomainListOfSizeForRegelungsvorhabenId(int size, RegelungsVorhabenId rvId) {
		List<CompoundDocumentDomain> compoundDocumentDomains = new ArrayList<>();

		for (int i = 0; i < size; i++) {
			CompoundDocumentDomain randomCompoundDocumentEntity = getRandomCompoundDocumentEntity();
			randomCompoundDocumentEntity.setRegelungsVorhabenId(rvId);
			randomCompoundDocumentEntity.setVersion(Integer.toString(i));
			compoundDocumentDomains.add(randomCompoundDocumentEntity);
		}

		return compoundDocumentDomains;
	}

	public static List<DocumentDomain> getDocumentEntityListOfSize(int size) {

		List<DocumentDomain> documentDomains = new ArrayList<>();

		for (int i = 0; i < size; i++) {
			documentDomains.add(getRandomDocumentEntity());
		}

		return documentDomains;
	}


	public static Proposition getRandomProposition() {
		return Proposition.builder()
			.propositionId(new RegelungsVorhabenId(REGELUNGSVORHABEN_ID))
			.proponentId(REGELUNGSVORHABEN_ID.toString())
			.abbreviation(REGELUNGSVORHABEN_ABKUERZUNG)
			.title(REGELUNGSVORHABEN_KURZBEZEICHNUNG)
			.shortTitle(REGELUNGSVORHABEN_KURZBESCHREIBUNG)
			.state(PropositionState.IN_BEARBEITUNG)
			.proponentTitle("Proponent Title")
			.proponentActive(true)
			.proponentDesignationNominative("Nominativ")
			.proponentDesignationGenitive("Genitiv")
			.type(RechtsetzungsdokumentTyp.GESETZ)
			.initiant(RechtsetzungsdokumentInitiant.BUNDESREGIERUNG)
			.createdAt(Instant.now())
			.farbe(FarbeType.BLAU)
			// explizit keine Referenz
			.build();
	}
}
