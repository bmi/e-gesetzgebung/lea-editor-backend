// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.validate.xpath;

import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.xpath.XPath;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.xpath.XPathComponent;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * Testsuite for {@link XPath}.
 */
class XPathTest {

    @Test
    void testXPath() {
        String testXPath = "/akn:akomaNtoso/akn:bill[1]/akn:body[1]/akn:article[4]/akn:paragraph[3]/akn:list[1]/"
            + "akn:point[2]/akn:list[1]/akn:point[1]/akn:content[1]/akn:p[1]/akn:mod[1]/akn:quotedText[4]";

        XPath xpath1 = XPath.parse(testXPath);
        assertNotNull(xpath1);
        assertEquals(testXPath, xpath1.toString());

        XPath xpath2 = new XPath();
        xpath2.addComponent(new XPathComponent("akn:akomaNtoso", 1));
        xpath2.addComponent(new XPathComponent("akn:bill", 1));
        xpath2.addComponent(new XPathComponent("akn:body", 1));
        xpath2.addComponent(new XPathComponent("akn:article", 4));
        xpath2.addComponent(new XPathComponent("akn:paragraph", 3));
        xpath2.addComponent(new XPathComponent("akn:list", 1));
        xpath2.addComponent(new XPathComponent("akn:point", 2));
        xpath2.addComponent(new XPathComponent("akn:list", 1));
        xpath2.addComponent(new XPathComponent("akn:point", 1));
        xpath2.addComponent(new XPathComponent("akn:content", 1));
        xpath2.addComponent(new XPathComponent("akn:p", 1));
        xpath2.addComponent(new XPathComponent("akn:mod", 1));
        xpath2.addComponent(new XPathComponent("akn:quotedText", 4));

        assertEquals(testXPath, xpath2.toString());
        assertEquals(xpath1, xpath2);
    }


    @Test
    void testErroneousPath() {
        String testXPath = "/X-0815";
        XPath xpath = XPath.parse(testXPath);
        assertNotNull(xpath);
        assertEquals(1, xpath.getPath().size());
        assertNull(xpath.getPath().get(0));

        testXPath = "/X_0815";
        xpath = XPath.parse(testXPath);
        assertNotNull(xpath);
        assertEquals(1, xpath.getPath().size());
        assertNotNull(xpath.getPath().get(0));
        assertEquals(testXPath, xpath.toString());
    }


    @Test
    void testEmptyPath() {
        XPath xpath = new XPath();
        assertEquals("", xpath.toString());
        assertEquals("", XPath.parse("").toString());
    }


    @Test
    void testGetParentPath() {
        String lastCmp = "/akn:collectionBody[1]";
        String testPath = "/akn:akomaNtoso/akn:documentCollection[1]";
        XPath xpath = XPath.parse(testPath + lastCmp);
        assertEquals(XPath.parse(testPath), xpath.getParent());
        assertEquals(testPath, XPath.getParentString(xpath.toString()));
    }

}
