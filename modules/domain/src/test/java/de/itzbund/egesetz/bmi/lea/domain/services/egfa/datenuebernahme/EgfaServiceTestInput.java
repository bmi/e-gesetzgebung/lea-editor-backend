// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.egfa.datenuebernahme;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.SneakyThrows;

@Getter
@AllArgsConstructor
public enum EgfaServiceTestInput {
    OEFFENTLICHE_HAUSHALTE(
        "/services/egfa/egfaExports/OeffentlicheHaushalte.json",
        true,
        true),

    ERFUELLUNGSAUFWAND(
        "/services/egfa/egfaExports/ErfuellungsaufwandExport.json",
        true,
        true),

    ERFUELLUNGSAUFWAND2(
        "/services/egfa/egfaExports/ErfuellungsaufwandExport2.json",
        true,
        true),

    ERFUELLUNGSAUFWAND_LEER(
        "/services/egfa/egfaExports/ErfuellungsaufwandExport_Leer.json",
        true,
        true),

    KMU_TEST(
        "/services/egfa/egfaExports/zuBegruendungAbschnitt5/kmu.json",
        true,
        true),

    AUSWIRKUNGEN_EINZELPREISE_PREISNIVEAU(
        "/services/egfa/egfaExports/zuBegruendungAbschnitt5/preis.json",
        true,
        true),

    SONSTIGE_KOSTEN(
        "/services/egfa/egfaExports/zuBegruendungAbschnitt5/sonstigeKosten.json",
        true,
        true),

    ENAP(
        "/services/egfa/egfaExports/beispiel-enap.json",
        false,
        true),

    GLEICHSTELLUNGSORIENTIERTE_GFA(
        "/services/egfa/egfaExports/zuBegruendungAbschnitt6/gleichstellungsorientierte.json",
        false,
        true),

    DISABILITY_MAINSTREAMING(
        "/services/egfa/egfaExports/zuBegruendungAbschnitt6/disability.json",
        false,
        true),

    DEMOFGRAFIE_CHECK(
        "/services/egfa/egfaExports/zuBegruendungAbschnitt6/demografieCheck.json",
        false,
        true),

    GLEICHWERTIGKEITS_CHECK(
        "/services/egfa/egfaExports/zuBegruendungAbschnitt6/gleichwertigkeitsCheck.json",
        false,
        true),

    AUSWIRKUNGEN_VERBRAUCHERINNEN(
        "/services/egfa/egfaExports/zuBegruendungAbschnitt6/verbraucher.json",
        false,
        true),

    WEITERE_WESENTLICHE_AUSWIRKUNGEN(
        "/services/egfa/egfaExports/zuBegruendungAbschnitt6/weitere.json",
        false,
        true),

    EVALUIERUNG(
        "/services/egfa/egfaExports/zuBegruendungAbschnitt7/evaluierung.json",
        false,
        true);

    private static final ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new Jdk8Module());
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    public final String exportDataPath;
    public final boolean concerningVorblatt;
    public final boolean concerningBegruendung;


    @SneakyThrows
    public EgfaDatenuebernahmeDTO getEgfaDatenuebernahmeDTO() {
        String egfaContent = Utils.getContentOfTextResource(exportDataPath);
        return objectMapper.readValue(egfaContent, EgfaDatenuebernahmeDTO.class);
    }

}
