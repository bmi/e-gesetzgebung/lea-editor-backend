// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.validate;

import de.itzbund.egesetz.bmi.lea.domain.services.validate.ValidationResult;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationData;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.AbstractValidationTask;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.UUID;

/**
 * A special {@link de.itzbund.egesetz.bmi.lea.domain.services.validate.task.ValidationTask} that is used in the validation test cases.
 */
@SuppressWarnings("unused")
@Component()
@Qualifier("test")
@Scope("prototype")
public class TestValidationTask extends AbstractValidationTask {

    private String label;


    @PostConstruct
    private void setLabel() {
        label = UUID.randomUUID().toString().replaceAll("-", "");
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return "validation task just for testing";
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationResult run(ValidationData data) {
        return null;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationResult getResult() {
        return null;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return label;
    }

}
