// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.ReplyId;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.when;

class CommentServiceRepliesTest extends CommentServiceTest {

    @Test
    void whenRepliesExist_thenReturnThemInTheirComment() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        CommentId commentId1 = new CommentId(UUID.randomUUID());
        CommentId commentId2 = new CommentId(UUID.randomUUID());

        when(commentPersistencePort.findByDocumentIdAndCreatedByAndDeleted(any(), any(), anyBoolean())).thenReturn(
            List.of(
                TestObjectsUtil.getCommentWithId(commentId1),
                TestObjectsUtil.getCommentWithId(commentId2)
            ));

        when(commentPersistencePort.findByDocumentIdAndDeleted(any(), anyBoolean())).thenReturn(
            List.of(
                TestObjectsUtil.getCommentWithId(commentId1),
                TestObjectsUtil.getCommentWithId(commentId2)
            ));

        when(replyPersistencePort.findByCommentIdAndDeleted(commentId1, false)).thenReturn(List.of(
            TestObjectsUtil.getRandomReplyEntity(),
            TestObjectsUtil.getRandomReplyEntity()
        ));
        when(replyPersistencePort.findByCommentIdAndDeleted(commentId2, false)).thenReturn(List.of());

        // Act
        List<Comment> comments = commentService.loadAllCommentsOfUserOfDocument(new DocumentId(uuid), status);

        // Assert
        assertEquals(ServiceState.OK, status.get(0));
        assertNotNull(comments);
        assertEquals(2, comments.size());

        comments.forEach(ce -> {
            CommentId commentId = ce.getCommentId();
            assertNotNull(ce.getReplies());

            if (commentId1.equals(commentId)) { // expect 2 replies
                assertEquals(2, ce.getReplies().size());
                Reply reply1 = ce.getReplies().get(0);
                Reply reply2 = ce.getReplies().get(1);

                assertNotNull(reply1);
                assertNotNull(reply1.getCreatedAt());
                assertNotNull(reply2);
                assertNotNull(reply2.getCreatedAt());
            } else { // expect 0 replies
                assertEquals(0, ce.getReplies().size());
            }
        });
    }


    @Test
    void whenReplyIsCreatedForValidDocumentAndValidComment_thenSuccess() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        Document randomDocument = TestObjectsUtil.getRandomDocument();
        Comment randomComment = TestObjectsUtil.getRandomComment();

        DocumentId documentId = randomDocument.getDocumentEntity().getDocumentId();
        CommentId commentId = randomComment.getCommentId();

        when(documentService.getDocumentByIdNEW(any())).thenReturn(randomDocument);
        when(commentPersistencePort.existsCommentByCommentId(any())).thenReturn(true);
        when(commentPersistencePort.findByCommentId(any())).thenReturn(randomComment);

        // Act
        commentService.createReply(
            documentId,
            commentId,
            TestObjectsUtil.getRandomReplyEntity(),
            status
        );

        // Assert
        assertEquals(1, status.size());
        assertEquals(ServiceState.OK, status.get(0));
    }


    @Test
    void whenReplyIsCreatedForInValidComment_thenNotFoundError() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();

        // Act
        commentService.createReply(
            new DocumentId(UUID.randomUUID()),
            new CommentId(UUID.randomUUID()),
            Reply.builder().build(),
            status
        );

        // Assert
        assertEquals(1, status.size());
        assertEquals(ServiceState.COMMENT_NOT_FOUND, status.get(0));
    }


    @Test
    void whenReplyIsCreatedForValidDocumentAndInValidComment_thenNotFoundError() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();

        when(commentPersistencePort.existsCommentByCommentId(any())).thenReturn(false);

        // Act
        commentService.createReply(
            new DocumentId(UUID.randomUUID()),
            new CommentId(UUID.randomUUID()),
            Reply.builder().build(),
            status
        );

        // Assert
        assertEquals(1, status.size());
        assertEquals(ServiceState.COMMENT_NOT_FOUND, status.get(0));
    }


    @Test
    void whenReplyIsUpdatedByValidUser_updateContent_thenSuccess() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        ReplyId replyId = new ReplyId(UUID.randomUUID());
        String origContent = Utils.getRandomString();
        String newContent = Utils.getRandomString();
        assertNotEquals(origContent, newContent);

        Reply replyEntity = Reply.builder()
            .replyId(replyId)
            .content(origContent)
            .build();

        Reply replyDTO = Reply.builder().content(newContent).build();

        when(commentPersistencePort.existsCommentByCommentId(any())).thenReturn(true);
        when(replyPersistencePort.findByReplyIdAndDeletedFalse(replyId)).thenReturn(Optional.of(replyEntity));

        // Act
        commentService.updateReply(
            new DocumentId(UUID.randomUUID()),
            new CommentId(UUID.randomUUID()),
            replyId,
            replyDTO,
            status
        );

        // Assert
        assertEquals(1, status.size());
        assertEquals(ServiceState.OK, status.get(0));
        assertEquals(newContent, replyEntity.getContent());
    }


    @Test
    void whenReplyIsUpdatedForInValidUser_thenNoPermissionError() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();

        when(commentPersistencePort.existsCommentByCommentId(any())).thenReturn(true);

        // Act
        commentService.updateReply(
            new DocumentId(UUID.randomUUID()),
            new CommentId(UUID.randomUUID()),
            new ReplyId(UUID.randomUUID()),
            Reply.builder().build(),
            status
        );

        // Assert
        assertEquals(1, status.size());
        assertEquals(ServiceState.NO_PERMISSION, status.get(0));
    }


    @Test
    void whenReplyIsUpdatedForInValidReply_thenNotFoundError() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();

        // override behaviour
        when(replyPersistencePort.existsReplyByReplyId(any())).thenReturn(false);

        // Act
        commentService.updateReply(
            new DocumentId(UUID.randomUUID()),
            new CommentId(UUID.randomUUID()),
            new ReplyId(UUID.randomUUID()),
            Reply.builder().build(),
            status
        );

        // Assert
        assertEquals(1, status.size());
        assertEquals(ServiceState.REPLY_NOT_FOUND, status.get(0));
    }


    @Test
    void whenReplyShouldBeDeleted_thenSuccess() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();

        // Act
        commentService.deleteCommentReply(
            new DocumentId(UUID.randomUUID()),
            new CommentId(UUID.randomUUID()),
            replyId,
            status
        );

        // Assert
        assertEquals(1, status.size());
        assertEquals(ServiceState.OK, status.get(0));
    }

}
