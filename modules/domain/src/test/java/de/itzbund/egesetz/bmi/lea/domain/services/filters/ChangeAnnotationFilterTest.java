// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.filters;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import lombok.SneakyThrows;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_P;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.cloneJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.findByEId;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDocumentObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getEffectiveTextValue;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getGuid;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewChangeWrapper;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewDefaultJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.setText;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ChangeAnnotationFilterTest {

    private static final JSONParser JSON_PARSER = new JSONParser();

    private static final Pattern PTN_CHANGE_MARKER = Pattern.compile("\"inserted\"|\"deleted\"");

    private static final Pattern PTN_COMMENT_MARKER = Pattern.compile("lea:commentMarkerWrapper|lea:commentMarker");

    private FilterChain filterChain;


    @BeforeEach
    void init() {
        filterChain = new FilterChain();
        filterChain.addFilter(new ChangeAnnotationFilter());
    }


    @Test
    void testDoFilter_whenNoChangeAnnotations_thenReturnUnchanged() {
        // Arrange
        JSONObject testObject = makeNewDefaultJSONObject(ELEM_P);
        setText(testObject, Utils.getRandomString());

        JSONObject testClone = cloneJSONObject(testObject);

        // Act
        filterChain.doFilter(testObject);

        // Assert
        assertEquals(testObject, testClone);
    }


    @Test
    void testDoFilter_whenOnlyInsertion_thenReturnCollapsedSameContent() {
        // Arrange
        String text1 = Utils.getRandomString();
        String text2 = Utils.getRandomString();
        JSONObject testObject = makeNewDefaultJSONObject(ELEM_P);
        setText(testObject, text1);
        addChildren(testObject, makeNewChangeWrapper(text2, "ct_inserted"));

        JSONObject testClone = cloneJSONObject(testObject);

        // Act
        filterChain.doFilter(testObject);

        // Assert
        assertNotEquals(testObject, testClone);
        assertEquals(getType(testObject), getType(testClone));
        assertEquals(getGuid(testObject), getGuid(testClone));

        JSONArray testObjectChildren = getChildren(testObject);
        JSONArray cloneChildren = getChildren(testClone);

        assertEquals(2, cloneChildren.size());
        assertEquals(1, testObjectChildren.size());
        assertEquals(text1 + text2, getText(testObject));
    }


    @Test
    @SneakyThrows
    void testDoFilter_LongTitle_NoChangesExpected() {
        String jsonString = "{\"eId\":\"doktitel-1\",\"children\":[{\"eId\":\"doktitel-1_text-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"Referentenentwurf\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:docStage\"},{\"refersTo\":\"artikel\",\"children\":[{\"children\":[{\"text\":\" des \"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:span\"},{\"children\":[{\"children\":[{\"text\":\"Bundesministeriums der Justiz\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:docProponent\"},{\"children\":[{\"children\":[{\"text\":\"RV - Test LEAGE-3556 (DÄNV vs StatÄV)\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:docTitle\"},{\"children\":[{\"text\":\"(\"}],\"type\":\"lea:textWrapper\"},{\"eId\":\"doktitel-1_text-1_kurztitel-1\",\"children\":[{\"children\":[{\"text\":\"RV - DÄNV vs StatÄV\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"71cc0898-e0f1-4c48-81e3-593b3f59f3d0\",\"type\":\"akn:shortTitle\"},{\"children\":[{\"text\":\" \\u2013 \"}],\"type\":\"lea:textWrapper\"},{\"refersTo\":\"amtliche-abkuerzung\",\"children\":[{\"children\":[{\"text\":\"Test LEAGE-3556\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:inline\"},{\"children\":[{\"text\":\")\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"4e39052d-8b6b-4f88-9c51-bfccdd2ee895\",\"type\":\"akn:p\"}],\"GUID\":\"5be0721b-7c8e-43c1-a482-12e253b96740\",\"type\":\"akn:longTitle\"}";
        JSONObject testObject = (JSONObject) JSON_PARSER.parse(jsonString);
        assertNotNull(testObject);

        JSONObject clone = cloneJSONObject(testObject);
        filterChain.doFilter(testObject);
        assertEquals(clone, testObject);
    }


    @Test
    @SneakyThrows
    void testDoFilter_RegulatoryTextWithDeletions_OnlyInsertionsRemain() {
        String jsonString1 = Utils.getContentOfTextResource("/filter/rt-change-tracking-only-insertions.json");
        assertEquals(3, getCountOfSubString(jsonString1));
        JSONObject documentObject1 = getDocumentObject(jsonString1);
        assertNotNull(documentObject1);
        String jsonString2 = Utils.getContentOfTextResource("/filter/rt-change-tracking-with-deletions.json");
        assertEquals(6, getCountOfSubString(jsonString2));
        JSONObject documentObject2 = getDocumentObject(jsonString2);
        assertNotNull(documentObject2);
        assertNotEquals(documentObject1, documentObject2);

        filterChain.doFilter(documentObject2);
        assertNotNull(documentObject2);
        assertEquals(0, getCountOfSubString(documentObject2.toJSONString()));
    }


    @Test
    void testDoFilter_RegulatoryTextWithCommentMarkers_ShouldFilterThemOut() {
        String jsonString = Utils.getContentOfTextResource("/filter/rt-comment-markers.json");
        JSONObject documentObject = getDocumentObject(jsonString);
        assertNotNull(documentObject);
        filterChain.doFilter(documentObject);

        Matcher matcher = PTN_COMMENT_MARKER.matcher(documentObject.toJSONString());
        assertFalse(matcher.matches());

        List<JSONObject> objects = findByEId(documentObject, "para-1_überschrift-1");
        assertEquals(1, objects.size());
        assertEquals("Portalverbund für digitale Verwaltungsleistungen", getEffectiveTextValue(objects.get(0)));
    }

    private int getCountOfSubString(String text) {
        Matcher matcher = PTN_CHANGE_MARKER.matcher(text);
        int found = 0;
        while (matcher.find()) {
            found++;
        }
        return found;
    }

}
