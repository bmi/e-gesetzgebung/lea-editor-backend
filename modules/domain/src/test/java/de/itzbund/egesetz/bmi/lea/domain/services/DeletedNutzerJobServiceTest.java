// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CommentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.EgfaDataPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.EgfaMetadataPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.ReplyPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserSettingsPersistencePort;

import static java.util.Collections.singletonList;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DeletedNutzerJobServiceTest {

	@Mock
	private UserPersistencePort userPersistencePort;

	@Mock
	private CommentPersistencePort commentPersistencePort;

	@Mock
	private ReplyPersistencePort replyPersistencePort;

	@Mock
	private CompoundDocumentPersistencePort compoundDocumentPersistencePort;

	@Mock
	private DokumentPersistencePort dokumentPersistencePort;

	@Mock
	private EgfaDataPersistencePort egfaDataPersistencePort;

	@Mock
	private EgfaMetadataPersistencePort egfaMetadataPersistencePort;

	@Mock
	private UserSettingsPersistencePort userSettingsPersistencePort;

	@InjectMocks
	private DeletedNutzerJobService deletedNutzerJobService;

	@Test
	@DisplayName("Mark deleted user that is not used as not referenced")
	void testMarkNotReferencedUsers() {
		// given
		final UserId deletedUserId = new UserId(Utils.getRandomString());
		when(userPersistencePort.findMarkedAsDeleted()).thenReturn(singletonList(deletedUserId));

		// when
		deletedNutzerJobService.markNotReferencedUsers();

		// then
		verify(userPersistencePort, times(1)).markUserAsNotReferenced(deletedUserId);
	}

	@Test
	@DisplayName("Do not mark deleted user as not referenced if it is still used")
	void testMarkNotReferencedUsers1() {
		// given
		final UserId deletedUserId = new UserId(Utils.getRandomString());
		when(userPersistencePort.findMarkedAsDeleted()).thenReturn(singletonList(deletedUserId));
		when(userSettingsPersistencePort.existsUserSettingsByUser(deletedUserId)).thenReturn(true);

		// when
		deletedNutzerJobService.markNotReferencedUsers();

		// then
		verify(userPersistencePort, never()).markUserAsNotReferenced(deletedUserId);
	}
}
