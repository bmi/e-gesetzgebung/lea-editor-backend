// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.aggregate;

import org.springframework.boot.test.context.TestConfiguration;

@TestConfiguration
public class TestConfig
{

    //    @Bean
    //    public EditorRollenUndRechte abstractService()
    //    {
    //        return new EditorRollenUndRechte()
    //        {
    //            @Override
    //            public String getStatusForResourceType(String ressourcenTyp, String ressourcenId)
    //            {
    //                return null;
    //            }
    //
    //        };
    //    }
}