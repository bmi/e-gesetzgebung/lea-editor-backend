// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.EditorRollenUndRechte;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.HomepageCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.HomepageRegulatoryProposalDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.RegelungsvorhabengTableDTOs;
import de.itzbund.egesetz.bmi.lea.domain.dtos.paging.PagingUtil;
import de.itzbund.egesetz.bmi.lea.domain.dtos.paging.PaginierungDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.paging.SortByType;
import de.itzbund.egesetz.bmi.lea.domain.dtos.paging.SortDirectionType;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserSettingsDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.mappers.BaseMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.BestandsrechtPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DrucksacheDokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {
	PropositionService.class, PropositionMapperImpl.class
})
@SuppressWarnings("unused")
class PropositionService2IntegrationTest {

	public static final int MAX_ANZAHL_VERSIONEN = 1;

	@Autowired
	private PropositionService propositionService;
	@Autowired
	private PropositionMapper propositionMapper;

	// Fuer den PropositionService
	@MockBean
	private BestandsrechtService bestandsrechtService;
	@MockBean
	private CompoundDocumentService compoundDocumentService;
	@MockBean
	private DocumentService documentService;
	@MockBean
	private DrucksacheService drucksacheService;
	@MockBean
	private RBACPermissionService rbacPermissionService;
	@MockBean
	private RegelungsvorhabenService regelungsvorhabenService;
	@MockBean
	protected UserService userService;
	@MockBean
	private LeageSession session;
	@MockBean
	private EditorRollenUndRechte editorRollenUndRechte;
	@MockBean
	private DrucksacheDokumentPersistencePort drucksacheDokumentPersistencePort;

	// Fuer den BestandsrechtService
	@MockBean
	private BestandsrechtPersistencePort bestandsrechtPersistencePort;

	// Fuer den PropositionMapper
	@MockBean
	protected BaseMapper baseMapper;

	// Andere Abhaengigkeiten
	@MockBean
	private CompoundDocumentPersistencePort compoundDocumentRepository;

	private List<CompoundDocumentDomain> compoundDocumentDomainListRV1;
	private List<CompoundDocumentDomain> compoundDocumentDomainListRV2;
	private List<RegelungsvorhabenEditorDTO> regelungsvorhaben;

	@BeforeEach
	void init() {
		// User
		when(session.getUser()).thenReturn(TestObjectsUtil.getUserEntity());

		// Regelungsvorhaben
		regelungsvorhaben = TestDTOUtil.getRegelungsvorhabenEditorDTOListOfSize(3);
		when(regelungsvorhabenService.getAllRegulatoryProposalsForUserHeIsAuthorOf()).thenReturn(regelungsvorhaben);

		// Dokumentenmappen
		compoundDocumentDomainListRV1 = TestObjectsUtil.getCompoundDocumentDomainListOfSizeForRegelungsvorhabenId(10,
			new RegelungsVorhabenId(regelungsvorhaben.get(0).getId()));

		compoundDocumentDomainListRV2 = TestObjectsUtil.getCompoundDocumentDomainListOfSizeForRegelungsvorhabenId(3,
			new RegelungsVorhabenId(regelungsvorhaben.get(1).getId()));

		List<CompoundDocumentDomain> mergedList = new ArrayList<>();
		mergedList.addAll(compoundDocumentDomainListRV1.subList(0, MAX_ANZAHL_VERSIONEN));
		mergedList.addAll(compoundDocumentDomainListRV2);

		when(compoundDocumentRepository.findByRegelungsvorhabenIdInSortedByUpdatedAt(anyList(), any(Pageable.class))).thenAnswer(
			invocation -> {
				Pageable pageable = invocation.getArgument(2);
				return mergedList;
			}
		);

		when(compoundDocumentRepository.findByRegelungsvorhabenIdInSortedByUpdatedAt(any(User.class), anyList())).thenAnswer(
			invocation -> {
				return mergedList;
			}
		);

		Map<UUID, RegelungsvorhabenEditorDTO> rvMap =
			regelungsvorhaben.stream()
				.collect(Collectors.toMap(RegelungsvorhabenEditorDTO::getId, RegelungsvorhabenEditorDTO -> RegelungsvorhabenEditorDTO));

		Set<String> uuids = rvMap.keySet().stream().map(k -> k.toString()).collect(Collectors.toSet());
		when(rbacPermissionService.getAlleRegelungsvorhabenFuerBenutzer(any())).thenReturn(uuids);

		when(compoundDocumentRepository.findByCreatedBySortedByUpdatedAt(
			any(User.class), any(Pageable.class))).thenReturn(
			new PageImpl<>(Collections.emptyList(),
				PagingUtil.createDefaultPagingForMeineDokumente(), 0));

	}

	@Test
	void testUebertrageneLeserechte() {
		// Arrange
		// we return 3 'übertragene Leserechte'
		List<CompoundDocument> compoundDocuments = TestObjectsUtil.getCompoundDocumentListOfSize(3);

		User userEntity = TestObjectsUtil.getUserEntity();
		userEntity.setGid(new UserId(UUID.randomUUID().toString()));
		compoundDocuments.get(1).getCompoundDocumentEntity().setCreatedBy(userEntity);
		when(rbacPermissionService.getDokumentenmappenIdsFuerNutzerAlsLeser(any())).thenReturn(
			compoundDocuments.stream().map(x -> x.getCompoundDocumentId().getId().toString()).collect(Collectors.toList()));
		when(compoundDocumentService.findByCompoundDocumentIdIn(any())).thenAnswer(
			invocation -> {
				List<CompoundDocumentId> compoundDocumentIds = invocation.getArgument(0);
				if (compoundDocumentIds.isEmpty()) {
					return Collections.emptyList();
				}
				return compoundDocuments.stream().map(CompoundDocument::getCompoundDocumentEntity).collect(Collectors.toList());
			}
		);
		List<DocumentDomain> documents = new ArrayList<>();
		compoundDocuments.forEach(x -> {
			List<DocumentDomain> documentList = x.getDocuments().stream().map(Document::getDocumentEntity).collect(Collectors.toList());
			documents.addAll(documentList);
		});
		when(documentService.findByCompoundDocumentIdIn(any())).thenAnswer(
			invocation -> {
				List<CompoundDocumentId> compoundDocumentIds = invocation.getArgument(0);
				if (compoundDocumentIds.isEmpty()) {
					return Collections.emptyList();
				}
				return documents;
			}
		);

		// we return a regelungsvorhaben
		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = TestDTOUtil.getRandomRegelungsvorhabenEditorDTO();
		when(regelungsvorhabenService.getRegulatoryProposalUserCanAccess(any(), any())).thenReturn(regelungsvorhabenEditorDTO);

		// Act
		List<HomepageRegulatoryProposalDTO> actual = propositionService.getStartseiteDokumenteAusAbstimmungen(
			new ArrayList<>());

		// Assert
		// 1 Regelungsvorhaben
		assertThat(actual).asList().hasSize(1);
		// 3 Dokumentenmappen
		List<HomepageCompoundDocumentDTO> hpCompoundDocuments = actual.get(0).getChildren();
		assertThat(hpCompoundDocuments).asList().hasSize(1);
		// 4 Dokumente in einer Mappe
		HomepageCompoundDocumentDTO homepageCompoundDocumentDTO = hpCompoundDocuments.get(0);
		assertThat(homepageCompoundDocumentDTO.getChildren()).asList().hasSize(12);
	}

	@Test
	void testPaginierungVersionenDokumentenmappen() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();
		Optional<PaginierungDTO> paginierungDTO = Optional.of(PaginierungDTO.builder()
			.sortDirection(SortDirectionType.DESC)
			.sortBy(SortByType.CREATED_AT)
			.pageNumber(0)
			.pageSize(MAX_ANZAHL_VERSIONEN)
			.build());

		UUID rvId = regelungsvorhaben.get(1).getId();
		List<CompoundDocument> compoundDocuments = TestObjectsUtil.getCompoundDocumentListOfSize(1);
		compoundDocuments.get(0).getCompoundDocumentEntity().setRegelungsVorhabenId(new RegelungsVorhabenId(rvId));

		UserSettingsDTO userSettingsDTO = TestDTOUtil.getUserSettingsDTO();
		userSettingsDTO.getPinnedDokumentenmappen().get(0).setRvId(rvId);

		when(userService.getUserSettings()).thenReturn(userSettingsDTO);
		when(compoundDocumentService.getCompoundDocumentById(any(), anyList())).thenReturn(TestObjectsUtil.getRandomCompoundDocument());
		when(compoundDocumentService.getNewestCompoundDocumentsForRegelungsvorhabenList(any(), anyList(), anyInt(), any())).thenReturn(compoundDocuments);
		when(editorRollenUndRechte.getRessortKurzbezeichnungForNutzer(any())).thenReturn("");

		// Act

		RegelungsvorhabengTableDTOs pagedObject = propositionService.getStartseiteMeineDokumente(status, paginierungDTO);
		List<HomepageRegulatoryProposalDTO> actual = pagedObject.getDtos().toList();

		// Assert
		// 1 - Nur RV2 darf zurückkommen, weil die anderen RVs keine Dokumentenmappe haben
		assertThat(actual).asList().hasSize(1);
		// 2 - RV2 darf nur noch eine DM haben
		assertThat(actual.get(0).getChildren()).asList().hasSize(MAX_ANZAHL_VERSIONEN);
	}

}
