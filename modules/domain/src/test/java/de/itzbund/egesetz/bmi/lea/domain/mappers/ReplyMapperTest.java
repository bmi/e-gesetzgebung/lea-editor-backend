// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.mappers;

import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.ReplyDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ReplyMapperTest {

    private ReplyMapper replyMapper;


    @BeforeEach
    void init() {
        replyMapper = new ReplyMapperImpl(
            new BaseMapperImpl()
        );
    }


    @Test
    void testMapToReplyId() {
        Reply reply = TestObjectsUtil.getRandomReplyEntity();
        ReplyDTO actual = replyMapper.map(reply);
        assertThat(actual).hasNoNullFieldsOrProperties();
    }


    @Test
    void testMap() {
        ReplyDTO replyDTO = TestDTOUtil.getRandomReplyDTO();
        Reply actual = replyMapper.map(replyDTO);
        assertThat(actual).hasNoNullFieldsOrPropertiesExcept("commentId", "documentId");
    }

}
