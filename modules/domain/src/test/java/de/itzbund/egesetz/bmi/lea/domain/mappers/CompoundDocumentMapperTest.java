// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.mappers;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CopyCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CreateCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class CompoundDocumentMapperTest {

	private CompoundDocumentMapper compoundDocumentMapper;


	@BeforeEach
	void init() {
		BaseMapper baseMapper = new BaseMapperImpl();
		PropositionMapper propositionMapper = new PropositionMapperImpl(baseMapper);
		UserMapper userMapper = new UserMapperImpl();

		DocumentMapper documentMapper = new DocumentMapperImpl(baseMapper, propositionMapper, userMapper);
		compoundDocumentMapper = new CompoundDocumentMapperImpl(baseMapper, documentMapper, propositionMapper);
	}


	@Test
	void testMapping_CreateCompoundDocumentDTO_to_CompoundDocument() {
		CreateCompoundDocumentDTO expected = TestDTOUtil.getRandomCreateCompoundDocumentDTO();
		CompoundDocument actual = compoundDocumentMapper.map(expected);

		assertNotNull(actual);
		assertThat(actual.getCompoundDocumentEntity()).isNotNull();

		// The main properties are found here
		CompoundDocumentDomain compoundDocumentEntity = actual.getCompoundDocumentEntity();
		assertThat(compoundDocumentEntity).hasNoNullFieldsOrPropertiesExcept(
			"compoundDocumentId",
			"version",
			"inheritFromId",
			"state",
			"createdBy",
			"updatedBy",
			"createdAt",
			"updatedAt",
			"verfahrensType",
			"fixedRegelungsvorhabenReferenzId");
	}


	@Test
	void testMapping_CopyCompoundDocument_to_CompoundDocumentSummaryDTO() {
		CopyCompoundDocumentDTO copyCompoundDocumentDTO = TestDTOUtil.getRandomCopyCompoundDocumentDTO();
		CompoundDocumentDomain save = TestObjectsUtil.getCompoundDocumentEntity();

		CompoundDocumentSummaryDTO actual = compoundDocumentMapper.map(copyCompoundDocumentDTO, save);

		assertNotNull(actual);
		assertThat(actual).hasNoNullFieldsOrPropertiesExcept("documentPermissions", "commentPermissions");
	}


	@Test
	void testMapping_CompoundDocument_to_CompoundDocumentDTO() {
		CompoundDocument compoundDocument = TestObjectsUtil.getRandomCompoundDocument();
		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = TestDTOUtil.getRandomRegelungsvorhabenEditorDTO();

		CompoundDocumentDTO actual = compoundDocumentMapper.mapFromCompoundDocumentAndProposition(compoundDocument,
			regelungsvorhabenEditorDTO);

		assertNotNull(actual);
		assertThat(actual).hasNoNullFieldsOrPropertiesExcept("documentPermissions", "commentPermissions");

		// Test Proposition
		PropositionDTO propositionDTO = actual.getProposition();
		assertThat(propositionDTO).hasNoNullFieldsOrPropertiesExcept("title", "farbe");

		// Test Dokuments
		assertThat(actual.getDocuments()
			.get(0)).hasNoNullFieldsOrPropertiesExcept("proposition", "documentPermissions", "commentPermissions", "version", "mappenName");
	}


	@Test
	void testMapping_CompoundDocument_to_CompoundDocumentSummaryDTO() {
		CompoundDocument compoundDocument = TestObjectsUtil.getRandomCompoundDocument();
		CompoundDocumentSummaryDTO actual = compoundDocumentMapper.mapCompoundDocument(compoundDocument);

		assertNotNull(actual);
		assertThat(actual).hasNoNullFieldsOrPropertiesExcept("documentPermissions", "commentPermissions");

		// Test Dokuments
		assertThat(actual.getDocuments()
			.get(0)).hasNoNullFieldsOrPropertiesExcept("proposition", "version", "documentPermissions", "commentPermissions");
	}

	// -----------


	@Test
	void testMapping_CopyCompoundDocumentDTO_to_CompoundDocumentDomain() {
		CopyCompoundDocumentDTO copyCompoundDocumentDTO = TestDTOUtil.getRandomCopyCompoundDocumentDTO();
		CompoundDocumentDomain actual = compoundDocumentMapper.mapDtoToEntity(copyCompoundDocumentDTO);

		assertNotNull(actual);
		assertThat(actual).hasNoNullFieldsOrPropertiesExcept("createdBy", "updatedBy", "verfahrensType", "fixedRegelungsvorhabenReferenzId");
	}


	@Test
	void testMapping_CompoundDocumentDomain_to_CopyCompoundDocumentDTO() {
		CompoundDocumentDomain compoundDocumentDomain = TestObjectsUtil.getCompoundDocumentEntity();
		CopyCompoundDocumentDTO actual = compoundDocumentMapper.mapEntityToDto(compoundDocumentDomain);

		assertNotNull(actual);
		assertThat(actual).hasNoNullFieldsOrPropertiesExcept("inheritFromId", "documents", "documentPermissions", "commentPermissions");
	}

	// -----------


	@Test
	void testMapping_CompoundDocumentEntity_to_CompoundDocumentSummaryDTO() {
		// Arrange
		CompoundDocumentDomain compoundDocumentEntity = TestObjectsUtil.getCompoundDocumentEntity();

		// Act
		CompoundDocumentSummaryDTO actual = compoundDocumentMapper.map(compoundDocumentEntity);

		// Assert
		assertNotNull(actual);
		assertThat(actual).hasNoNullFieldsOrPropertiesExcept("documents", "documentPermissions", "commentPermissions");
	}


	@Test
	void testMapping_CompoundDocumentDomainList_to_CompoundDocumentSummaryDTOList() {
		List<CompoundDocumentDomain> compoundDocuments = TestObjectsUtil.getCompoundDocumentDomainListOfSize(5);
		List<CompoundDocumentSummaryDTO> actual = compoundDocumentMapper.maptoCompoundDocumentSummaryDTOList(
			compoundDocuments);

		assertNotNull(actual);
		assertThat(actual).hasSize(compoundDocuments.size());

		// Test Liste
		assertThat(actual.get(0)).hasNoNullFieldsOrPropertiesExcept("documents", "documentPermissions", "commentPermissions");

	}


	@Test
	void testMapping_CompoundDocumentList_to_CompoundDocumentSummaryDTOList() {
		List<CompoundDocument> compoundDocuments = TestObjectsUtil.getCompoundDocumentListOfSize(5);
		List<CompoundDocumentSummaryDTO> actual =
			compoundDocumentMapper.mapCompoundDocumentToCompoundDocumentSummaryDTOList(
				compoundDocuments);

		assertNotNull(actual);
		assertThat(actual).hasSize(compoundDocuments.size());

		// Test Liste
		assertThat(actual.get(0)).hasNoNullFieldsOrPropertiesExcept("documentPermissions", "commentPermissions");

	}

}
