// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validate.task;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.ValidationResult;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.CompoundDocumentValidationResult;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationData;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {CompoundDocumentPersistencePort.class})
class CompoundDocumentContentValidationTaskTest {

    @MockBean
    CompoundDocumentPersistencePort compoundDocumentRepository;


    public void setupCompoundDocumentRepositoryMockups(CompoundDocumentPersistencePort compoundDocumentRepository) {
        when(compoundDocumentRepository.findByCompoundDocumentId(
            Mockito.any(CompoundDocumentId.class))).thenReturn(
            Optional.of(TestObjectsUtil.getCompoundDocumentEntity()));
    }


    @BeforeEach
    void init() {
        setupCompoundDocumentRepositoryMockups(compoundDocumentRepository);
    }


    @Test
    void whenDocumentTypeNotPartOfDocumentList_thenValidationShouldBeOK() {
        // Arrange
        List<Document> documents = new ArrayList<>();
        CompoundDocument randomCompoundDocument = TestObjectsUtil.getRandomCompoundDocumentWithDocuments(documents);
        randomCompoundDocument.setCompoundDocumentRepository(compoundDocumentRepository);

        // Act

        // Assert
        documentTypeNotPartOfDocumentList(randomCompoundDocument, DocumentType.ANLAGE,
            CompoundDocumentValidationResult.VALIDATION_SUCCESS);
        documentTypeNotPartOfDocumentList(randomCompoundDocument, DocumentType.VORBLATT_STAMMGESETZ,
            CompoundDocumentValidationResult.VALIDATION_SUCCESS);

        documentTypeNotPartOfDocumentList(randomCompoundDocument, DocumentType.REGELUNGSTEXT_STAMMGESETZ,
            CompoundDocumentValidationResult.VALIDATION_SUCCESS);
        documentTypeNotPartOfDocumentList(randomCompoundDocument, DocumentType.ANSCHREIBEN_STAMMGESETZ,
            CompoundDocumentValidationResult.VALIDATION_SUCCESS);
        documentTypeNotPartOfDocumentList(randomCompoundDocument, DocumentType.BEGRUENDUNG_STAMMGESETZ,
            CompoundDocumentValidationResult.VALIDATION_SUCCESS);
    }


    @Test
    void whenDocumentTypeIsPartOfDocumentList_thenValidationShouldFail() {
        // Arrange
        List<Document> documents =
            List.of(new Document[]{
                TestObjectsUtil.getRandomDocumentOfType(DocumentType.REGELUNGSTEXT_STAMMGESETZ),
                TestObjectsUtil.getRandomDocumentOfType(DocumentType.ANSCHREIBEN_STAMMGESETZ),
                TestObjectsUtil.getRandomDocumentOfType(DocumentType.BEGRUENDUNG_STAMMGESETZ),
                TestObjectsUtil.getRandomDocumentOfType(DocumentType.VORBLATT_STAMMGESETZ)
            });

        CompoundDocument randomCompoundDocument = TestObjectsUtil.getRandomCompoundDocumentWithDocuments(documents);
        randomCompoundDocument.setCompoundDocumentRepository(compoundDocumentRepository);

        // Assert
        documentTypeNotPartOfDocumentList(randomCompoundDocument, DocumentType.REGELUNGSTEXT_STAMMGESETZ,
            CompoundDocumentValidationResult.TO_MUCH_DOCUMENTS_OF_GIVEN_TYPE);
        documentTypeNotPartOfDocumentList(randomCompoundDocument, DocumentType.ANSCHREIBEN_STAMMGESETZ,
            CompoundDocumentValidationResult.TO_MUCH_DOCUMENTS_OF_GIVEN_TYPE);
        documentTypeNotPartOfDocumentList(randomCompoundDocument, DocumentType.BEGRUENDUNG_STAMMGESETZ,
            CompoundDocumentValidationResult.TO_MUCH_DOCUMENTS_OF_GIVEN_TYPE);
        documentTypeNotPartOfDocumentList(randomCompoundDocument, DocumentType.VORBLATT_STAMMGESETZ,
            CompoundDocumentValidationResult.TO_MUCH_DOCUMENTS_OF_GIVEN_TYPE);
    }


    @Test
    void whenDocumentCouldOccurSeveralTimes_thenValidationShouldNotFail() {

        List<Document> documents =
            List.of(new Document[]{
                TestObjectsUtil.getRandomDocumentOfType(DocumentType.ANLAGE)
            });

        CompoundDocument randomCompoundDocument = TestObjectsUtil.getRandomCompoundDocumentWithDocuments(documents);
        randomCompoundDocument.setCompoundDocumentRepository(compoundDocumentRepository);

        documentTypeNotPartOfDocumentList(randomCompoundDocument, DocumentType.ANLAGE,
            CompoundDocumentValidationResult.VALIDATION_SUCCESS);
    }


    @Test
    void whenCompoundDocumentIsNotWriteable_thenValidationShouldDetectWriteProtection() {
        // Arrange
        DocumentState actualState = DocumentState.FREEZE;

        // - Override standard mockup behaviour
        CompoundDocumentDomain compoundDocumentEntity = TestObjectsUtil.getCompoundDocumentEntity();
        compoundDocumentEntity.setState(actualState);
        when(compoundDocumentRepository.findByCompoundDocumentId(
            Mockito.any(CompoundDocumentId.class))).thenReturn(
            Optional.of(compoundDocumentEntity));

        // - Testdocument
        List<Document> documents = new ArrayList<>();
        CompoundDocument randomCompoundDocument = TestObjectsUtil.getRandomCompoundDocumentWithDocuments(documents);
        randomCompoundDocument.setCompoundDocumentRepository(compoundDocumentRepository);
        randomCompoundDocument.updateState(actualState);

        // Act & Assert
        documentTypeNotPartOfDocumentList(randomCompoundDocument, DocumentType.ANLAGE,
            CompoundDocumentValidationResult.COMPOUND_DOCUMENT_IS_PROTECTED);
        documentTypeNotPartOfDocumentList(randomCompoundDocument, DocumentType.VORBLATT_STAMMGESETZ,
            CompoundDocumentValidationResult.COMPOUND_DOCUMENT_IS_PROTECTED);

        documentTypeNotPartOfDocumentList(randomCompoundDocument, DocumentType.REGELUNGSTEXT_STAMMGESETZ,
            CompoundDocumentValidationResult.COMPOUND_DOCUMENT_IS_PROTECTED);
        documentTypeNotPartOfDocumentList(randomCompoundDocument, DocumentType.ANSCHREIBEN_STAMMGESETZ,
            CompoundDocumentValidationResult.COMPOUND_DOCUMENT_IS_PROTECTED);
        documentTypeNotPartOfDocumentList(randomCompoundDocument, DocumentType.BEGRUENDUNG_STAMMGESETZ,
            CompoundDocumentValidationResult.COMPOUND_DOCUMENT_IS_PROTECTED);
    }


    @SneakyThrows
    private void documentTypeNotPartOfDocumentList(CompoundDocument compoundDocument, DocumentType documentType,
        ValidationResult expected) {
        // Arrange
        ValidationData validationData = new ValidationData();

        CompoundDocumentContentValidationTask compoundDocumentContentValidationTask =
            new CompoundDocumentContentValidationTask();
        compoundDocumentContentValidationTask.setInputSource(compoundDocument, documentType);

        // Act
        ValidationResult actual = compoundDocumentContentValidationTask.run(validationData);
        ValidationResult actual2 = compoundDocumentContentValidationTask.getResult();

        // Assert
        assertThat(actual).isEqualTo(actual2);
        assertThat(actual).isEqualTo(expected);
    }

}
