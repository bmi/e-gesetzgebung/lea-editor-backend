// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.util;

import de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRAttribute;
import de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRAttribute.FRBR_DATE;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRAttribute.FRBR_HREF;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRAttribute.FRBR_LANG;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRAttribute.FRBR_NAME;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRAttribute.FRBR_VALUE;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.EXPRESSION_FRBR_AUTHOR;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.EXPRESSION_FRBR_DATE;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.EXPRESSION_FRBR_LANGUAGE;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.EXPRESSION_FRBR_THIS;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.EXPRESSION_FRBR_URI;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.EXPRESSION_FRBR_VERSION_NUMBER;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.MANIFESTATION_FRBR_AUTHOR;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.MANIFESTATION_FRBR_DATE;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.MANIFESTATION_FRBR_FORMAT;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.MANIFESTATION_FRBR_THIS;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.MANIFESTATION_FRBR_URI;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.WORK_FRBR_ALIAS;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.WORK_FRBR_AUTHOR;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.WORK_FRBR_COUNTRY;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.WORK_FRBR_DATE;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.WORK_FRBR_NAME;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.WORK_FRBR_NUMBER;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.WORK_FRBR_SUBTYPE;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.WORK_FRBR_THIS;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement.WORK_FRBR_URI;

@UtilityClass
public class TestEliMetadataUtil {

    // list of elements and attribute-value-pairs
    private static final List<Pair<FRBRElement, Set<Pair<FRBRAttribute, String>>>> TEST_DATA =
        List.of(
            // Work ---------------------------------------------------------------------

            Pair.of(WORK_FRBR_THIS, Set.of(Pair.of(FRBR_VALUE, "eli/dl/2020/BUND/0815/RV/DOKUMENT-0"))),
            Pair.of(WORK_FRBR_URI, Set.of(Pair.of(FRBR_VALUE, "eli/dl/2020/BUND/0815/RV"))),
            Pair.of(WORK_FRBR_ALIAS, Set.of(
                Pair.of(FRBR_VALUE, "10000000-0000-0000-0000-000000000000"),
                Pair.of(FRBR_NAME, "RV-ID")
            )),
            Pair.of(WORK_FRBR_DATE, Set.of(
                Pair.of(FRBR_DATE, "2020-02-02"),
                Pair.of(FRBR_NAME, "ENTWURF")
            )),
            Pair.of(WORK_FRBR_AUTHOR, Set.of(Pair.of(FRBR_HREF, "/BUND"))),
            Pair.of(WORK_FRBR_COUNTRY, Set.of(Pair.of(FRBR_VALUE, "XX"))),
            Pair.of(WORK_FRBR_NUMBER, Set.of(Pair.of(FRBR_VALUE, "0815"))),
            Pair.of(WORK_FRBR_NAME, Set.of(Pair.of(FRBR_VALUE, "RV"))),
            Pair.of(WORK_FRBR_SUBTYPE, Set.of(Pair.of(FRBR_VALUE, "DOKUMENT-0"))),

            // Expression ---------------------------------------------------------------

            Pair.of(EXPRESSION_FRBR_THIS, Set.of(Pair.of(FRBR_VALUE, "eli/dl/2020/BUND/0815/RV/LAND/2023-03-03/4711/GER/DOKUMENT-0"))),
            Pair.of(EXPRESSION_FRBR_URI, Set.of(Pair.of(FRBR_VALUE, "eli/dl/2020/BUND/0815/RV/LAND/2023-03-03/4711/GER"))),
            Pair.of(EXPRESSION_FRBR_AUTHOR, Set.of(Pair.of(FRBR_HREF, "/LAND"))),
            Pair.of(EXPRESSION_FRBR_DATE, Set.of(
                Pair.of(FRBR_DATE, "2023-03-03"),
                Pair.of(FRBR_NAME, "VERSION")
            )),
            Pair.of(EXPRESSION_FRBR_LANGUAGE, Set.of(Pair.of(FRBR_LANG, "GER"))),
            Pair.of(EXPRESSION_FRBR_VERSION_NUMBER, Set.of(Pair.of(FRBR_VALUE, "4711"))),

            // Manifestation ------------------------------------------------------------

            Pair.of(MANIFESTATION_FRBR_THIS, Set.of(Pair.of(FRBR_VALUE, "eli/dl/2020/BUND/0815/RV/LAND/2023-03-03/4711/GER/DOKUMENT-0.PDF"))),
            Pair.of(MANIFESTATION_FRBR_URI, Set.of(Pair.of(FRBR_VALUE, "eli/dl/2020/BUND/0815/RV/LAND/2023-03-03/4711/GER/DOKUMENT-0.PDF"))),
            Pair.of(MANIFESTATION_FRBR_DATE, Set.of(
                Pair.of(FRBR_DATE, "2024-04-04"),
                Pair.of(FRBR_NAME, "ERSTELLUNG")
            )),
            Pair.of(MANIFESTATION_FRBR_AUTHOR, Set.of(Pair.of(FRBR_HREF, "/STADT"))),
            Pair.of(MANIFESTATION_FRBR_FORMAT, Set.of(Pair.of(FRBR_VALUE, "PDF")))
        );

    public Map<FRBRElement, Map<FRBRAttribute, String>> getTestEliMetaData() {
        Map<FRBRElement, Map<FRBRAttribute, String>> eliMetaData = new EnumMap<>(FRBRElement.class);
        Arrays.stream(FRBRElement.values()).forEach(frbrElement -> eliMetaData.put(frbrElement, new EnumMap<>(FRBRAttribute.class)));

        TEST_DATA.forEach(elemPair -> {
            elemPair.getRight().forEach(attrPair -> {
                eliMetaData.get(elemPair.getLeft()).put(attrPair.getKey(), attrPair.getValue());
            });
        });

        return eliMetaData;
    }

}
