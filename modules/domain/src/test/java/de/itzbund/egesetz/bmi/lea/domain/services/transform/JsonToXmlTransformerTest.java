// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.transform;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.util.Utils.getDocumentBuilderFactory;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

@Log4j2
class JsonToXmlTransformerTest {

    private static JsonToXmlTransformer transformer;


    @SuppressWarnings("unused")
    static Stream<Arguments> testResources() {
        return Stream.of(
            arguments("/transform/stammgesetz-begruendung"),
            arguments("/transform/stammgesetz-regelungstext-0"),
            arguments("/transform/stammgesetz-regelungstext-1"),
            arguments("/transform/stammgesetz-regelungstext-2"),
            arguments("/transform/stammgesetz-regelungstext-3"),
            arguments("/transform/stammgesetz-regelungstext-4"),
            arguments("/transform/stammgesetz-vorblatt")
        );
    }


    @BeforeAll
    static void setUp() {
        transformer = new JsonToXmlTransformer();
    }


    @AfterAll
    static void tearDown() {
        transformer = null;
    }


    @ParameterizedTest(name = "#{index} - Test with {0}")
    @MethodSource("testResources")
    @SneakyThrows
    void testTransformDocument(String filePath) {
        String jsonInputFile = filePath + ".json";
        String xmlOutputFile = String.format("target%s.xml",
            filePath.substring(filePath.lastIndexOf('/')));

        // step 1: read JSON input
        URL url = JsonToXmlTransformerTest.class.getResource(jsonInputFile);
        if (url == null) {
            fail("could not find resource " + jsonInputFile);
        }
        FileReader reader = new FileReader(url.getFile());
        StringWriter writer = new StringWriter();
        reader.transferTo(writer);
        reader.close();

        JSONParser jsonParser = new JSONParser();

        JSONArray jsonArray = (JSONArray) jsonParser.parse(writer.toString());
        JSONObject jsonObject = (JSONObject) jsonArray.get(0);
        assertNotNull(jsonObject);
        // step 2: transform to XML
        String jsonString = jsonObject.toJSONString();
        ReusableInputSource ris = transformer.transform(jsonString);
        assertNotNull(ris);

        // step 3: write XML content to file
        String xmlContent = convertToString(ris.getByteStream());
        StringReader input = new StringReader(xmlContent);
        String prettyXml = Utils.getPrettyPrintedXml(input);
        assertNotNull(prettyXml);
        log.trace(prettyXml);
        writeToFile(prettyXml, xmlOutputFile);
        assertTrue((new File(xmlOutputFile).canRead()));

        Document expectedDoc;
        Document actualDoc = parseXMLContent(prettyXml);

        String expectedFilePath = filePath + ".xml";
        URL urlExpected = JsonToXmlTransformerTest.class.getResource(expectedFilePath);
        if (urlExpected == null) {
            fail("could not find resource " + expectedFilePath);
        }

        FileReader readerCompare = new FileReader(urlExpected.getFile());
        StringWriter writerCompare = new StringWriter();
        readerCompare.transferTo(writerCompare);
        readerCompare.close();
        expectedDoc = parseXMLContent(writerCompare.toString());

        NodeList expectedNodeList = expectedDoc.getElementsByTagName("*");
        NodeList actualNodeList = actualDoc.getElementsByTagName("*");

        assertEquals(expectedNodeList.getLength(), actualNodeList.getLength());

        for (int i = 0; i <= expectedNodeList.getLength() - 1; i++) {
            assertEquals(expectedNodeList.item(i).getNodeName(), actualNodeList.item(i).getNodeName());
        }
    }


    private Document parseXMLContent(String xmlContent) {
        DocumentBuilder documentBuilder;
        Document document = null;

        try {
            documentBuilder = getDocumentBuilderFactory().newDocumentBuilder();
            document = documentBuilder.parse(new ByteArrayInputStream(xmlContent.getBytes(StandardCharsets.UTF_8)));
        } catch (Exception e) {
            log.error(e);
        }

        return document;
    }


    private String convertToString(InputStream inputStream) {
        return new BufferedReader(
            new InputStreamReader(inputStream, StandardCharsets.UTF_8))
            .lines()
            .collect(Collectors.joining("\n"));
    }


    @SneakyThrows
    private void writeToFile(String xmlContent, String filepath) {
        FileWriter writer = new FileWriter(filepath);
        writer.write(xmlContent);
        writer.flush();
        writer.close();
    }

}
