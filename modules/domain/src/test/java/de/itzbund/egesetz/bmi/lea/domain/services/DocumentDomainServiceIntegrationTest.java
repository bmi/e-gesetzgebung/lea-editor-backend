// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.core.util.TestResourceProvider;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.EditorRollenUndRechte;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.CreateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.UpdateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RessortEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType;
import de.itzbund.egesetz.bmi.lea.domain.mappers.BaseMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.mappers.CompoundDocumentMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.mappers.DocumentMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.DocumentMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.mappers.UserMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.model.Template;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.meta.MetadatenUpdatePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.BestandsrechtPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DrucksacheDokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.PropositionPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.TemplatePersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserSettingsPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.EIdGeneratorRestPort;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.EIdGeneratorService;
import de.itzbund.egesetz.bmi.lea.domain.services.export.ZipExportFilenameBuilder;
import de.itzbund.egesetz.bmi.lea.domain.services.logging.DomainLoggingService;
import de.itzbund.egesetz.bmi.lea.domain.services.meta.MetadatenUpdateService;
import de.itzbund.egesetz.bmi.lea.domain.services.transform.JsonToXmlTransformer;
import de.itzbund.egesetz.bmi.lea.domain.services.transform.XmlToJsonTransformer;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.ValidationService;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.SchemaProvider;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationData;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.Validator;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.CompoundDocumentContentValidationTask;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.SchematronValidationTask;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.XmlSchemaValidationTask;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import de.itzbund.egesetz.bmi.lea.domain.validator.DocumentPermissionValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith({SpringExtension.class}) // JUnit 5
@SpringBootTest(classes = {
	// Mapper
	BaseMapperImpl.class, CompoundDocumentMapperImpl.class, DocumentMapperImpl.class, PropositionMapperImpl.class,
	UserMapperImpl.class,
	// Services
	CompoundDocumentService.class, ConverterService.class, DocumentService.class, DownloadService.class,
	PropositionService.class, TemplateService.class, UserService.class, ValidationService.class,
	AuxiliaryService.class, EIdGeneratorService.class, MetadatenUpdateService.class, DomainLoggingService.class,
	// Validation
	CompoundDocumentContentValidationTask.class, Document.class, SchematronValidationTask.class,
	ValidationData.class,
	Validator.class, XmlSchemaValidationTask.class,
	// Rest
	CompoundDocument.class, JsonToXmlTransformer.class, PropositionPersistencePort.class, SchemaProvider.class,
	XmlToJsonTransformer.class, ZipExportFilenameBuilder.class,
	CollectionDocumentBuilder.class,

	EditorRollenUndRechte.class,
	RBACPermissionService.class,
	BenachrichtigungService.class
})
@SuppressWarnings("unused")
class DocumentDomainServiceIntegrationTest {

	private static final User user = User.builder()
		.gid(new UserId("User ID"))
		.name("Username")
		.email("meine@email.de")
		.build();

	private final static UUID sampleUUID = UUID.randomUUID();
	private final static UUID sampleUUID2 = UUID.randomUUID();
	private static final String BEZEICHNUNG = "Term";
	private static final String KURZBEZEICHNUNG = "Short term";
	private static final String ABKUERZUNG = "abk";
	// Wird im Testprojekt überschrieben und liefert immer erfolgreich gespeichert
	@MockBean
	protected EditorRollenUndRechte editorRollenUndRechte;
	@MockBean
	private TemplatePersistencePort templateRepository;
	@MockBean
	private DokumentPersistencePort documentRepository;
	@MockBean
	private CompoundDocumentPersistencePort compoundDocumentRepository;
	@MockBean
	private PropositionPersistencePort propositionRepository;
	@MockBean
	private DocumentPermissionValidator documentPermissionValidator;
	@MockBean
	private PlategRequestService plategRequestService;
	@MockBean
	private LeageSession session;
	@MockBean
	private ValidationService validationService;
	@MockBean
	private PropositionService propositionService;
	@MockBean
	private CommentService commentService;
	@MockBean
	private UserPersistencePort userRepository;
	@MockBean
	private UserSettingsPersistencePort userSettingsRepository;
	@MockBean
	private BestandsrechtPersistencePort bestandsrechtRepository;
	@MockBean
	private DrucksacheDokumentPersistencePort drucksacheDokumentRepository;
	@MockBean
	private RegelungsvorhabenService regelungsvorhabenService;
	@MockBean
	private EIdGeneratorRestPort eIdGeneratorRestPort;
	@MockBean
	private UserService userService;
	@MockBean
	private DomainLoggingService domainLoggingService;
	@InjectMocks
	private CompoundDocument compoundDocument;
	@Autowired
	private ConverterService converterService;
	@Autowired
	private SchemaProvider schemaProvider;
	@Autowired
	private Validator validator;
	@Autowired
	private XmlSchemaValidationTask xmlSchemaValidationTask;
	@Autowired
	private SchematronValidationTask schematronValidationTask;
	@Autowired
	private ValidationData validationData;
	@Autowired
	private JsonToXmlTransformer jsonToXmlTransformer;
	@Autowired
	private XmlToJsonTransformer xmlToJsonTransformer;
	@Autowired
	private DocumentService documentService;
	@Autowired
	private TemplateService templateService;
	@Autowired
	private CompoundDocumentService compoundDocumentService;
	@Autowired
	private CollectionDocumentBuilder collectionDocumentBuilder;
	@Autowired
	private AuxiliaryService auxiliaryService;
	@Autowired
	private MetadatenUpdatePort metadatenUpdatePort;
	@Autowired
	private DocumentMapper documentMapper;
	private DocumentDomain sampleDocument;
	private DocumentDomain sampleDocument2;
	private List<ServiceState> serviceStates;

	private CompoundDocument randomCompoundDocument;

	@BeforeEach
	public void setUp() {
		serviceStates = new ArrayList<>();

		sampleDocument = createSampleDocument(sampleUUID);
		sampleDocument2 = createSampleDocument(sampleUUID2);

		when(session.getUser()).thenReturn(user);

		prepareDocumentRepositoryMock();
		prepareCompoundDocumentRepositoryMock();
		preparePropositionRepositoryMock();
		preparePropositionRepositoryMock();
		preparePropositionServiceMock();

		// For test of whenDocumentIdIsValid_thenDocumentStateShouldBeFreeze()
		when(documentPermissionValidator.isUserOwnerOfDocumentEntity(sampleDocument)).thenReturn(true);
		when(documentPermissionValidator.isUserCollaborating(Mockito.any(DocumentDomain.class)))
			.thenReturn(true);

		preparePlateqRequestServiceMock();
		prepareTemplateRepositoryMock();
		prepareRegelungsvorhabenMock();
		prepareCompoundDocumentMock();
	}


	private void prepareCompoundDocumentMock() {
		// Arrange
		randomCompoundDocument = TestObjectsUtil.getRandomCompoundDocument();
		randomCompoundDocument.getCompoundDocumentEntity()
			.setCreatedBy(user);
		randomCompoundDocument.setCompoundDocumentRepository(compoundDocumentRepository);
	}


	private void prepareRegelungsvorhabenMock() {
		when(regelungsvorhabenService.getRegelungsvorhabenEditorTriggeredByDocument(Mockito.any(RegelungsVorhabenId.class)))
			.thenReturn(TestDTOUtil.getRandomRegelungsvorhabenEditorDTO());
	}


	// --- Only instantiated Mocks -------------
	private void prepareDocumentRepositoryMock() {

		when(documentRepository.findByDocumentId(sampleDocument.getDocumentId()))
			.thenReturn(Optional.of(sampleDocument));

		// For test of whenAuthorHasDocuments_getAllDocumentsForAuthor()
		when(documentRepository.findByCreatedBy(user)).thenReturn(
			Arrays.asList(
				sampleDocument,
				sampleDocument2
			));

		when(documentRepository.save(Mockito.any(DocumentDomain.class)))
			.thenAnswer(invocation ->
				{
					DocumentDomain documentEntity = invocation.getArgument(0);
					documentEntity.setCreatedAt(Instant.now());
					documentEntity.setUpdatedAt(Instant.now());
					return documentEntity;
				}
			);

	}


	private void prepareCompoundDocumentRepositoryMock() {
		when(compoundDocumentRepository.findByCreatedByAndCompoundDocumentId(Mockito.any(User.class),
			Mockito.any(CompoundDocumentId.class)))
			.thenReturn(Optional.of(TestObjectsUtil.getRandomCompoundDocumentEntity()));
		when(compoundDocumentRepository.findByCompoundDocumentId(
			Mockito.any(CompoundDocumentId.class)))
			.thenReturn(Optional.of(TestObjectsUtil.getRandomCompoundDocumentEntity()));
	}


	private void preparePropositionRepositoryMock() {
		Proposition propositionEntity = Proposition.builder()
			.propositionId(new RegelungsVorhabenId(sampleUUID))
			.proponentId(sampleUUID.toString())
			.build();

		when(propositionRepository.findByUpdateablePropositionId(new RegelungsVorhabenId(sampleUUID)))
			.thenReturn(propositionEntity);
	}


	private void prepareTemplateRepositoryMock() {
		// For test whenCreateDocumentDTOValid_thenDocumentShouldBeProvided()
		prepareTemplateRepositoryMock(DocumentType.REGELUNGSTEXT_STAMMGESETZ.getTemplateID() + "-0");

		// For test whenDokumentTypeVorblatt_thenTemplateShouldBeProvided()
		prepareTemplateRepositoryMock(DocumentType.VORBLATT_STAMMGESETZ.getTemplateID());

		// For test whenDokumentTypeBegruendung_thenTemplateShouldBeProvided()
		prepareTemplateRepositoryMock(DocumentType.BEGRUENDUNG_STAMMGESETZ.getTemplateID());

		// For test whenDokumentTypeAnschreiben_thenTemplateShouldBeProvided()
		prepareTemplateRepositoryMock(DocumentType.ANSCHREIBEN_STAMMGESETZ.getTemplateID());

		// For test whenDokumentTypeAnlage_thenTemplateShouldBeProvided()
		prepareTemplateRepositoryMock(DocumentType.ANLAGE.getTemplateID());
	}


	private void prepareTemplateRepositoryMock(String templateName) {
		when(templateRepository.findAllByTemplateNameOrderByVersionDescUpdatedAtDesc(templateName))
			.thenReturn(List.of(Template.builder()
				.content("[{}]")
				.build()));
	}


	private void preparePropositionServiceMock() {
		when(propositionService.getPropositionUuid(sampleDocument))
			.thenReturn(sampleUUID);
		when(propositionService.getPropositionUuid(Mockito.any(DocumentDomain.class)))
			.thenReturn(sampleUUID);
	}


	private void preparePlateqRequestServiceMock() {
		List<RegelungsvorhabenEditorDTO> regelungsvorhaben = createRegelungsvorhaben();
		when(plategRequestService.getAllRegulatoryProposalsForUserHeIsAuthorOf()).thenReturn(regelungsvorhaben);

		when(plategRequestService.getRegulatoryProposalUserCanAccess(
			sampleDocument.getDocumentId()
				.getId()))
			.thenReturn(createRegelungsvorhaben2());

		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = RegelungsvorhabenEditorDTO.builder()
			.id(sampleUUID)
			.bezeichnung(BEZEICHNUNG)
			.kurzbezeichnung(KURZBEZEICHNUNG)
			.abkuerzung(ABKUERZUNG)
			.technischesFfRessort(RessortEditorDTO.builder()
				.id(UUID.randomUUID()
					.toString())
				.build())
			.build();

		when(plategRequestService.getRegelungsvorhabenEditorDTO(sampleUUID))
			.thenReturn(regelungsvorhabenEditorDTO);

		when(plategRequestService.isUserParticipantForCompoundDocument(any())).thenReturn(true);
	}
	// -----------------------------


	private List<RegelungsvorhabenEditorDTO> createRegelungsvorhaben() {
		RessortEditorDTO ressortEditor = RessortEditorDTO.builder()
			.id(UUID.randomUUID()
				.toString())
			.kurzbezeichnung("don")
			.bezeichnungNominativ("der")
			.bezeichnungGenitiv("des")
			.aktiv(true)
			.build();

		RegelungsvorhabenEditorDTO vorhaben1 = RegelungsvorhabenEditorDTO.builder()
			.id(UUID.randomUUID())
			.bezeichnung(BEZEICHNUNG)
			.kurzbezeichnung(KURZBEZEICHNUNG)
			.status(VorhabenStatusType.ENTWURF)
			.technischesFfRessort(ressortEditor)
			.build();

		RegelungsvorhabenEditorDTO vorhabenForSampleDocument = RegelungsvorhabenEditorDTO.builder()
			.id(sampleDocument.getDocumentId()
				.getId())
			.bezeichnung(BEZEICHNUNG)
			.kurzbezeichnung(KURZBEZEICHNUNG)
			.status(VorhabenStatusType.IN_BEARBEITUNG)
			.technischesFfRessort(ressortEditor)
			.build();

		RegelungsvorhabenEditorDTO vorhaben3 = RegelungsvorhabenEditorDTO.builder()
			.id(sampleDocument2.getDocumentId()
				.getId())
			.bezeichnung(BEZEICHNUNG)
			.kurzbezeichnung(KURZBEZEICHNUNG)
			.status(VorhabenStatusType.ARCHIVIERT)
			.technischesFfRessort(ressortEditor)
			.build();

		return Arrays.asList(vorhaben1, vorhabenForSampleDocument, vorhaben3);
	}


	private RegelungsvorhabenEditorDTO createRegelungsvorhaben2() {
		RessortEditorDTO ressortEditor = RessortEditorDTO.builder()
			.id(UUID.randomUUID()
				.toString())
			.kurzbezeichnung("don")
			.bezeichnungNominativ("der")
			.bezeichnungGenitiv("des")
			.aktiv(true)
			.build();

		return RegelungsvorhabenEditorDTO.builder()
			.id(sampleDocument2.getDocumentId()
				.getId())
			.bezeichnung(BEZEICHNUNG)
			.kurzbezeichnung(KURZBEZEICHNUNG)
			.status(VorhabenStatusType.ARCHIVIERT)
			.technischesFfRessort(ressortEditor)
			.build();
	}


	private DocumentDomain createSampleDocument(UUID uuid) {
		DocumentId id = new DocumentId(uuid);
		DocumentDomain documentEntity = TestObjectsUtil.getDocumentEntity();
		documentEntity.setDocumentId(id);

		return documentEntity;
	}


	@Test
	void whenValidDokumentId_thenDokumentEntityShouldBeFound() {
		DocumentDomain found = documentService.getDocumentEntityById(sampleDocument.getDocumentId());
		assertThat(found).isEqualTo(sampleDocument);
	}


	@Test
	void whenInvalidDokumentId_thenDokumentEntityShouldBeNull() {
		// Invalid UUID as it generated now
		DocumentDomain found = documentService.getDocumentEntityById(new DocumentId(UUID.randomUUID()));
		assertThat(found).isNull();
	}


	@Test
	void whenDokumentTypeAnlage_thenTemplateShouldBeProvided() {
		String actual = templateService.loadTemplate(DocumentType.ANLAGE, 0);
		assertThat(actual).isNotNull()
			.isNotEmpty();
	}


	@Test
	void whenDokumentTypeAnschreiben_thenTemplateShouldBeProvided() {
		String actual = templateService.loadTemplate(DocumentType.ANSCHREIBEN_STAMMGESETZ, 0);
		assertThat(actual).isNotNull()
			.isNotEmpty();
	}


	@Test
	void whenDokumentTypeBegruendung_thenTemplateShouldBeProvided() {
		String actual = templateService.loadTemplate(DocumentType.BEGRUENDUNG_STAMMGESETZ, 0);
		assertThat(actual).isNotNull()
			.isNotEmpty();
	}


	@Test
	void whenDokumentTypeRegelungstextStammform_thenTemplateShouldBeProvided() {
		String actual = templateService.loadTemplate(DocumentType.REGELUNGSTEXT_STAMMGESETZ, 0);
		assertThat(actual).isNotNull()
			.isNotEmpty();
	}


	@Test
	void whenDokumentTypeVorblatt_thenTemplateShouldBeProvided() {
		String actual = templateService.loadTemplate(DocumentType.VORBLATT_STAMMGESETZ, 0);
		assertThat(actual).isNotNull()
			.isNotEmpty();
	}


	private CreateDocumentDTO getCreateDocumentDTO(UUID uuid) {
		CreateDocumentDTO.CreateDocumentDTOBuilder<?, ?> artefact = CreateDocumentDTO.builder()
			.title("A title")
			.initialNumberOfLevels(0)
			.type(DocumentType.REGELUNGSTEXT_STAMMGESETZ);

		return artefact.build();
	}


	@Test
	void whenAtCreateDocumentDTOUserIsInvalid_thenDocumentShouldBeNull() {
		// Override expected behaviour
		when(session.getUser()).thenReturn(null);

		CreateDocumentDTO createDocumentDTO = getCreateDocumentDTO(null);
		Document actual = auxiliaryService.makeDocument(
			new CompoundDocumentId(UUID.fromString("10000000-0000-0000-0000-000000000000")),
			createDocumentDTO);
		assertThat(actual).isNull();
	}


	@Test
	void whenCreateDocumentDTOValid_thenDocumentShouldBeProvided() {
		// UUID ist falsch
		CreateDocumentDTO createDocumentDTO = getCreateDocumentDTO(UUID.randomUUID());

		Document actual = auxiliaryService.makeDocument(
			new CompoundDocumentId(UUID.fromString("10000000-0000-0000-0000-000000000000")),
			createDocumentDTO);

		// check most common fields
		assertThat(actual).hasFieldOrProperty("documentEntity")
			.hasFieldOrPropertyWithValue("documentEntity.title", createDocumentDTO.getTitle())
			.hasFieldOrPropertyWithValue("documentEntity.type", createDocumentDTO.getType())
			.hasFieldOrPropertyWithValue("documentEntity.createdBy", user);

		// check especially content which is the template
		String content = actual.getDocumentEntity()
			.getContent();
		assertThat(content).isNotNull()
			.isNotEmpty();
	}


	@Test
	void whenAuthorHasDocumentsAndRegelungsvorhaben_getAllDocumentsForAuthorIncludingRegelungsvorhaben() {
		List<DocumentDTO> actual = getAllDocumentsByAuthor(user);
		assertThat(actual).asList()
			.hasSize(2);
		actual.forEach(listElement -> assertThat(listElement).isNotNull()
			.hasFieldOrPropertyWithValue("title", TestObjectsUtil.DOCUMENT_TITLE)
		);
	}

	@Test
	void whenAuthorHasDocumentsAndRegelungsvorhabenIsUnchangeable_getAllDocumentsForAuthorIncludingRegelungsvorhaben() {
		CompoundDocumentDomain randomCompoundDocumentEntity = TestObjectsUtil.getRandomCompoundDocumentEntity();
		randomCompoundDocumentEntity.setState(DocumentState.FREEZE);
		randomCompoundDocumentEntity.setFixedRegelungsvorhabenReferenzId(UUID.randomUUID());

		when(propositionRepository.findByNotUpdateablePropositionIdReferableId(any(RegelungsVorhabenId.class), any(UUID.class))).thenReturn(
			TestObjectsUtil.getRandomProposition());

		when(compoundDocumentRepository.findByCompoundDocumentId(Mockito.any(CompoundDocumentId.class))).thenReturn(Optional.of(randomCompoundDocumentEntity));

		when(compoundDocumentRepository.findByCreatedByAndCompoundDocumentId(Mockito.any(User.class),
			Mockito.any(CompoundDocumentId.class))).thenReturn(Optional.of(randomCompoundDocumentEntity));

		when(regelungsvorhabenService.getNoChangeableForGivenRegelungsvorhabenId(any(RegelungsVorhabenId.class), any(UUID.class))).thenReturn(
			TestDTOUtil.getRandomRegelungsvorhabenEditorDTO());

		List<DocumentDTO> actual = getAllDocumentsByAuthor(user);
		assertThat(actual).asList().hasSize(2);

		actual.forEach(listElement -> assertThat(listElement).isNotNull()
			.hasFieldOrPropertyWithValue("title", TestObjectsUtil.DOCUMENT_TITLE)
		);
	}

	@Test
	void whenAuthorHasDocumentsAndNoRegelungsvorhaben_getAllDocumentsForAuthor() {

		// Arrange - override 'normal' behaviour
		DocumentDomain dangledDocument = TestObjectsUtil.getRandomDocumentEntity();

		when(documentRepository.findByCreatedBy(user)).thenReturn(List.of(dangledDocument));
		when(propositionService.getPropositionUuid(sampleDocument))
			.thenReturn(null);

		// Act
		List<DocumentDTO> actual = getAllDocumentsByAuthor(user);

		// Assert
		assertThat(actual).asList()
			.hasSize(1);
	}

	@Test
	void whenAuthorHasNoDocuments_getNoDocumentsForAuthor() {
		when(documentRepository.findByCreatedBy(user)).thenReturn(
			Collections.emptyList());
		List<DocumentDTO> actual = getAllDocumentsByAuthor(user);
		assertThat(actual).asList()
			.isEmpty();
	}

	@Test
	void whenAuthorIsInvalid_EmptyListIsReturned() {
		User userNew = User.builder()
			.gid(new UserId("AnotherUser"))
			.name("Username2")
			.email("Meine.Second@email.de")
			.build();

		when(session.getUser()).thenReturn(userNew);
		List<DocumentDTO> actual = getAllDocumentsByAuthor(userNew);
		assertThat(actual).asList()
			.isEmpty();
	}

	@Test
	void whenAuthorIsValid_DocumentShouldBeUpdated() {
		String newContent = "Doesn't care, but changed";
		UUID documentUUID = sampleDocument.getDocumentId()
			.getId();
		UpdateDocumentDTO updateDocumentDTO = UpdateDocumentDTO.builder()
			.content(newContent)
			.build();

		assertThat(sampleDocument).hasFieldOrPropertyWithValue("content", TestObjectsUtil.CONTENT);

		documentService.updateDocument(documentUUID, updateDocumentDTO, serviceStates);

		assertThat(serviceStates.get(0)).isEqualTo(ServiceState.OK);
		assertThat(sampleDocument).hasFieldOrPropertyWithValue("content", newContent);
	}

	@Test
	void whenAutorIsValidAndUUIDIsInvalid_DocumentShouldNotBeUpdated() {
		String newContent = "Doesn't care, but changed";
		// In this case UUID is always invalid
		UUID documentUUID = UUID.randomUUID();
		UpdateDocumentDTO updateDocumentDTO = UpdateDocumentDTO.builder()
			.content(newContent)
			.build();

		assertThat(sampleDocument).hasFieldOrPropertyWithValue("content", TestObjectsUtil.CONTENT);

		documentService.updateDocument(documentUUID, updateDocumentDTO, serviceStates);

		assertThat(serviceStates.get(0)).isEqualTo(ServiceState.DOCUMENT_NOT_FOUND);
		assertThat(sampleDocument).hasFieldOrPropertyWithValue("content", TestObjectsUtil.CONTENT);
	}

	@Test
	void whenDocumentIsValid_CreateANewValidDocument() {
		CreateDocumentDTO createDocumentDTO = TestDTOUtil.getRandomCreateDocumentDTO();

		List<ServiceState> status = new ArrayList<>();
		DocumentDTO actual = createAndSaveNewDocument(createDocumentDTO, status);

		assertThat(status.get(0)).isEqualTo(ServiceState.OK);
		assertThat(actual).hasFieldOrPropertyWithValue("title", TestDTOUtil.DOCUMENT_TITLE)
			.hasFieldOrPropertyWithValue("type", TestDTOUtil.DOCUMENT_TYPE)
			.hasFieldOrPropertyWithValue("state", DocumentState.DRAFT)
			.hasFieldOrProperty("content")
			.isNotNull()
			.hasFieldOrProperty("id")
			.isNotNull()
			.hasFieldOrProperty("createdBy")
			.isNotNull()
			.hasFieldOrProperty("createdAt")
			.isNotNull();
	}

	@Test
	void whenDocumentIdAndUserIsValid_GetDocument() {
		// Arrange

		// Act
		DocumentDTO actual = documentService.getDocumentById(sampleDocument.getDocumentId()
			.getId(), false);

		// Assert
		assertThat(actual).isNotNull()
			.hasFieldOrPropertyWithValue("title", TestObjectsUtil.DOCUMENT_TITLE);
	}

	@Test
	void whenDocumentIdAndUserIsValid_GetDocumentWithAnnotations() {
		// Arrange
		String begruendungContent = changeGUIDs(Utils.getContentOfTextResource(
			"/services/egfa/stammgesetz-begruendung.json"));
		when(plategRequestService.getRegulatoryProposalUserCanAccess(any(), any())).thenReturn(
			TestDTOUtil.getRandomRegelungsvorhabenEditorDTO()
		);

		sampleDocument.setContent(begruendungContent);

		// Act
		DocumentDTO actual = documentService.getDocumentById(sampleDocument.getDocumentId()
			.getId(), true);

		// Assert
		assertThat(actual).isNotNull()
			.hasFieldOrPropertyWithValue("title", TestObjectsUtil.DOCUMENT_TITLE);
	}

	private static final Pattern PTN_DEFAULT_GUID =
		Pattern.compile("\"GUID\":\\s?\"00000000-0000-0000-0000-000000000001\"");

	public static String changeGUIDs(String origContent) {
		return Utils.replaceTokens(origContent, PTN_DEFAULT_GUID,
			p -> String.format("\"GUID\":\"%s\"", UUID.randomUUID()));
	}

	@Test
	void whenDocumentIdIsInvalid_GetNoDocument() {
		DocumentDTO actual = documentService.getDocumentById(UUID.randomUUID(), false);
		assertThat(actual).isNull();
	}

	@Test
	void testRename_whenDocumentNotFound_thenFailure() {
		// Arrange
		User user = User.builder()
			.name("other")
			.gid(new UserId(UUID.randomUUID()
				.toString()))
			.email("")
			.build();
		randomCompoundDocument.getCompoundDocumentEntity()
			.setCreatedBy(user);

		List<ServiceState> status = new ArrayList<>();
		DocumentId documentId = new DocumentId(UUID.randomUUID());

		when(documentRepository.findByDocumentId(any())).thenReturn(Optional.empty());

		// Act
		documentService.updateDocument(
			documentId.getId(),
			UpdateDocumentDTO.builder()
				.title(Utils.getRandomString())
				.build(),
			status);

		// Assert
		assertEquals(1, status.size());
		assertEquals(ServiceState.DOCUMENT_NOT_FOUND, status.get(0));
	}

	@Test
	void testRename_whenStatusNotDraft_thenFailure_NoPermission() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();
		CompoundDocumentId compoundDocumentId = new CompoundDocumentId(UUID.randomUUID());
		DocumentId documentId = new DocumentId(UUID.randomUUID());

		when(documentRepository.findByDocumentId(any())).thenReturn(Optional.of(
			TestObjectsUtil.getRandomDocumentEntity()
		));

		when(compoundDocumentRepository.findByCompoundDocumentId(any())).thenReturn(Optional.of(
			CompoundDocumentDomain.builder()
				.compoundDocumentId(compoundDocumentId)
				.state(DocumentState.FINAL)
				.build()
		));

		// Act
		documentService.updateDocument(
			documentId.getId(),
			UpdateDocumentDTO.builder()
				.title(Utils.getRandomString())
				.build(),
			status);

		// Assert
		assertEquals(1, status.size());
		assertEquals(ServiceState.DOCUMENT_IS_NOT_IN_DRAFT, status.get(0));
	}

	@Test
	void testRename_whenDocumentIsRenamed_thenSuccess() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();
		CompoundDocumentId compoundDocumentId = new CompoundDocumentId(UUID.randomUUID());
		DocumentId documentId = new DocumentId(UUID.randomUUID());
		String newTitle = Utils.getRandomString();

		DocumentDomain documentEntity = DocumentDomain.builder()
			.compoundDocumentId(compoundDocumentId)
			.documentId(documentId)
			.content(Utils.getContent(TestResourceProvider.getInstance().getSingleInputSource(
				"/services/templates/template-mantelgesetz-begruendung.LDML.de-1.6.json"
			)))
			.build();

		when(documentRepository.findByDocumentId(any())).thenReturn(Optional.of(
			documentEntity
		));
		when(compoundDocumentRepository.findByCompoundDocumentId(any())).thenReturn(Optional.of(
			CompoundDocumentDomain.builder()
				.compoundDocumentId(compoundDocumentId)
				.state(DocumentState.DRAFT)
				.build()
		));
		when(documentPermissionValidator.isUserOwnerOfDocumentEntity(any())).thenReturn(true);
		when(plategRequestService.getRegulatoryProposalUserCanAccess(any(), any())).thenReturn(
			TestDTOUtil.getRandomRegelungsvorhabenEditorDTO()
		);

		// Act
		documentService.updateDocument(
			documentId.getId(),
			UpdateDocumentDTO.builder()
				.title(newTitle)
				.build(),
			status);

		// Assert
		assertEquals(1, status.size());
		assertEquals(ServiceState.OK, status.get(0));
		assertEquals(newTitle, documentEntity.getTitle());
	}

	/**
	 * Returns a list of all documents (and their metadata) of the current user.
	 *
	 * @return list of {@link DocumentDTO} comprising metadata and the document content
	 */
	public List<DocumentDTO> getAllDocumentsByAuthor(User user) {
		List<DocumentDomain> documentEntities = documentRepository.findByCreatedBy(user);

		return documentEntities.stream()
			.map(documentEntity -> documentService.getDocumentDTO(documentEntity,
				propositionService.getPropositionUuid(documentEntity)))
			.collect(Collectors.toList());
	}

	/**
	 * Creates a new document based on a template and immediately saves it in the database so that the document can be referred to by an ID.
	 *
	 * @param createDocumentDTO data from document creation wizard to define kind of document to create
	 * @return instance of {@link DocumentDTO} comprising metadata and a template document
	 */
	public DocumentDTO createAndSaveNewDocument(CreateDocumentDTO createDocumentDTO, List<ServiceState> status) {
		Document document = auxiliaryService.makeDocument(
			new CompoundDocumentId(UUID.fromString("10000000-0000-0000-0000-000000000000")),
			createDocumentDTO);
		if (document == null) {
			status.add(ServiceState.TEMPLATE_NOT_FOUND);
			return null;
		}

		DocumentDomain documentEntity = document.getDocumentEntity();
		documentEntity = documentRepository.save(documentEntity);
		status.add(ServiceState.OK);

		UUID propositionId = propositionService.getPropositionUuid(documentEntity);

		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO =
			regelungsvorhabenService.getRegelungsvorhabenEditorDTONEW(
				new RegelungsVorhabenId(propositionId), true);

		DocumentDTO documentDTO = documentMapper.mapToDocumentDTO(documentEntity, regelungsvorhabenEditorDTO);
		// if we create a document it gets the state from its compound document,
		// but if we can create one, it MUST be DRAFT
		documentDTO.setState(DocumentState.DRAFT);

		return documentDTO;
	}

}
