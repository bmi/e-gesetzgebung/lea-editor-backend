// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.compare;

import de.itzbund.egesetz.bmi.lea.core.json.JSONUtils;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserDTO;
import de.itzbund.egesetz.bmi.lea.domain.services.synopse.SynopsisUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import lombok.SneakyThrows;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.reflect.Method;
import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Named.named;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class RegulatoryTextComparerTest {

    private static String SYNOPSE_TEMPLATE;

    @BeforeAll
    public static void setUp() {
        SYNOPSE_TEMPLATE = Utils.getContentOfTextResource("/compare/synopse-template.json");
        assertNotNull(SYNOPSE_TEMPLATE);
        assertFalse(SYNOPSE_TEMPLATE.isBlank());
    }

    @ParameterizedTest
    @MethodSource("provideTestData")
    @SneakyThrows
    void testCompareTwoDocuments_Regelungstext_withHierarchy(JSONObject baseDocument, JSONObject versionDocument) {
        // Arrange
        DocumentComparer comparer = new RegulatoryTextComparer();
        assertNotNull(comparer);
        Instant now = Instant.now();
        UserDTO userDTO = TestDTOUtil.getRandomUserDTO();

        // Act
        comparer.compare(
            baseDocument,
            Map.of(RegulatoryTextComparer.RegulatoryTextMetadata.LAST_MODIFICATION_DATE.name(), now.minus(Duration.ofDays(2))),
            versionDocument,
            Map.of(RegulatoryTextComparer.RegulatoryTextMetadata.LAST_MODIFICATION_DATE.name(), now));
        DocumentDTO documentDTO = SynopsisUtil.getSynopseDocument(
            ((RegulatoryTextComparer) comparer).getTable(), SYNOPSE_TEMPLATE, userDTO);

        // Assert
        assertNotNull(documentDTO);
        assertNotNull(documentDTO.getContent());
        JSONObject synopse = JSONUtils.getDocumentObject(documentDTO.getContent());
        assertNotNull(synopse);

        Method printTableMethod = RegulatoryTextComparer.class.getDeclaredMethod("printTable");
        printTableMethod.setAccessible(true);
        String table = (String) printTableMethod.invoke(comparer);
        assertNotNull(table);
    }

    private static Stream<Arguments> provideTestData() {
        return Stream.of(
            arguments(named("simple case - no hierarchy",
                    getDocumentObject("/compare/Regelungstext-Stammgesetz_BASE.json")),
                getDocumentObject("/compare/Regelungstext-Stammgesetz_VERSION.json")),
            arguments(named("documents with hierarchy",
                    getDocumentObject("/compare/BASE_421362d6-e642-4e2a-ac69-738693087203.json")),
                getDocumentObject("/compare/VERSION_1e9f766a-b6ed-4b7e-9030-1d3cb4d39e9a.json")),
            arguments(named("complex case",
                    getDocumentObject("/compare/Regelungstext-V1.json")),
                getDocumentObject("/compare/Regelungstext-V2.json"))
        );
    }

    private static JSONObject getDocumentObject(String path) {
        String content = Utils.getContentOfTextResource(path);
        assertNotNull(content);
        assertFalse(content.isBlank());
        JSONObject resultObject = JSONUtils.getDocumentObject(content);
        assertNotNull(resultObject);
        return resultObject;
    }

}
