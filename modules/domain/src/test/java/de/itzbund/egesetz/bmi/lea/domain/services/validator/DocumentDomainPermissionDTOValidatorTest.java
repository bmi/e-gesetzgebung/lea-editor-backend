// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.validator;

import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.services.RegelungsvorhabenService;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.domain.validator.DocumentPermissionValidator;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atMostOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {DocumentPermissionValidator.class})
@Log4j2
@SuppressWarnings("unused")
class DocumentDomainPermissionDTOValidatorTest {

    @Autowired
    DocumentPermissionValidator documentPermissionValidator;

    @MockBean
    private LeageSession session;
    @MockBean
    private RegelungsvorhabenService plategRequestService;

    private User owner;
    private User user;


    @BeforeEach
    void init() {
        owner = User.builder()
            .gid(new UserId("someRandomGid"))
            .build();
        user = User.builder()
            .gid(new UserId("someRandomGid1"))
            .build();
    }


    @Test
    void testIsUserOwner_Successful_AsOwner() {
        DocumentDomain document = DocumentDomain.builder()
            .createdBy(user)
            .build();

        when(session.getUser()).thenReturn(user);
        boolean response = documentPermissionValidator.isUserOwnerOfDocumentEntity(document);

        assertTrue(response);
    }


    @Test
    void testIsUserOwner_Unsuccessful_AsNoOwner() {
        DocumentDomain document = DocumentDomain.builder()
            .createdBy(owner)
            .build();

        when(session.getUser()).thenReturn(user);
        boolean response = documentPermissionValidator.isUserOwnerOfDocumentEntity(document);

        assertFalse(response);
    }


    @Test
    void testCheckIsUserOwner_Unsuccessful_AsNoOwner() {
        DocumentDomain document = DocumentDomain.builder()
            .createdBy(owner)
            .build();

        when(session.getUser()).thenReturn(user);
        assertFalse(documentPermissionValidator.isUserOwnerOfDocumentEntity(document));
    }


    @Test
    void testCheckIsUserOwner_successful_AsOwner() {
        DocumentDomain document = DocumentDomain.builder()
            .createdBy(user)
            .build();

        when(session.getUser()).thenReturn(user);
        documentPermissionValidator.isUserOwnerOfDocumentEntity(document);

        verify(session, atMostOnce()).getUser();
    }


    @Test
    void testCheckIsAllowedToView_AsNonOwnerButParticipant_DocumentInFreeze() {
        UUID documentId = UUID.randomUUID();
        DocumentDomain document = DocumentDomain.builder()
            .documentId(new DocumentId(documentId))
            .createdBy(owner)
            .build();

        when(session.getUser()).thenReturn(user);
        when(plategRequestService.getRegelungsvorhaben(any(), any())).thenReturn(
            new RegelungsvorhabenEditorDTO());
        documentPermissionValidator.isUserCollaborating(document);

        verify(session, atMostOnce()).getUser();
    }


    @Test
    void testCheckIsAllowedToView_AsNonOwnerAndNoParticipant_DocumentInFreeze() {
        UUID documentId = UUID.randomUUID();
        DocumentDomain document = DocumentDomain.builder()
            .documentId(new DocumentId(documentId))
            .createdBy(owner)
            .build();

        when(session.getUser()).thenReturn(user);
        when(plategRequestService.getRegelungsvorhaben(any(), any())).thenReturn(
            new RegelungsvorhabenEditorDTO());
        assertFalse(documentPermissionValidator.isUserCollaborating(document));
    }


    @Test
    void testCheckIsAllowedToView_AsNonOwnerButParticipant_DocumentNotInFreeze() {
        UUID documentId = UUID.randomUUID();
        DocumentDomain document = DocumentDomain.builder()
            .documentId(new DocumentId(documentId))
            .createdBy(owner)
            .build();

        when(session.getUser()).thenReturn(user);
        when(plategRequestService.getRegelungsvorhaben(any(), any())).thenReturn(
            new RegelungsvorhabenEditorDTO());
        assertFalse(documentPermissionValidator.isUserCollaborating(document));
    }


    @Test
    void testCheckIsAllowedToView_successful_AsOwner() {
        DocumentDomain document = DocumentDomain.builder()
            .createdBy(user)
            .build();

        when(session.getUser()).thenReturn(user);
        documentPermissionValidator.isUserCollaborating(document);

        verify(session, atMostOnce()).getUser();
    }


    @Test
    void testIsUserOwner_CompoundDocument_Success() {
        CompoundDocumentDomain compoundDocument = CompoundDocumentDomain.builder()
            .createdBy(owner)
            .build();

        when(session.getUser()).thenReturn(owner);
        assertTrue(documentPermissionValidator.isUserOwnerOfCompoundDocumentEntity(compoundDocument));
    }


    @Test
    void testIsUserOwner_CompoundDocument_Failure() {
        CompoundDocumentDomain compoundDocument = CompoundDocumentDomain.builder()
            .createdBy(owner)
            .build();

        when(session.getUser()).thenReturn(user);
        assertFalse(documentPermissionValidator.isUserOwnerOfCompoundDocumentEntity(compoundDocument));
    }


    @Test
    void testIsUserOwner_Comment_Success() {
        Comment comment = Comment.builder()
            .createdBy(owner)
            .build();

        when(session.getUser()).thenReturn(owner);
        assertTrue(documentPermissionValidator.isUserOwnerOfCommentEntity(comment));
    }


    @Test
    void testIsUserOwner_Comment_Failure() {
        Comment comment = Comment.builder()
            .createdBy(owner)
            .build();

        when(session.getUser()).thenReturn(user);
        assertFalse(documentPermissionValidator.isUserOwnerOfCommentEntity(comment));
    }


    @Test
    void testUserOwnerOfRegulatoryProposal_NoRegulatoryProposalOfOwner_Failure() {
        when(plategRequestService.getAllRegulatoryProposalsForUserHeIsAuthorOf())
            .thenReturn(List.of());
        assertFalse(documentPermissionValidator.isUserOwnerOfRegulatoryProposal(UUID.randomUUID()));
    }


    @Test
    void testUserOwnerOfRegulatoryProposal_OneRegulatoryProposalOfOwner_Success() {
        UUID regulatoryProposalId = UUID.randomUUID();
        when(plategRequestService.getAllRegulatoryProposalsForUserHeIsAuthorOf())
            .thenReturn(List.of(
                RegelungsvorhabenEditorDTO.builder()
                    .id(regulatoryProposalId)
                    .build()
            ));
        assertTrue(documentPermissionValidator.isUserOwnerOfRegulatoryProposal(regulatoryProposalId));
    }


    @Test
    void testUserOwnerOfRegulatoryProposal_OneRegulatoryProposalOfOwner_Failure() {
        when(plategRequestService.getAllRegulatoryProposalsForUserHeIsAuthorOf())
            .thenReturn(List.of(
                RegelungsvorhabenEditorDTO.builder()
                    .id(UUID.randomUUID())
                    .build()
            ));
        assertFalse(documentPermissionValidator.isUserOwnerOfRegulatoryProposal(UUID.randomUUID()));
    }


    @Test
    void whenDocumentEntityIsEmpty_CollaberationShouldBeFalse() {
        assertFalse(documentPermissionValidator.isUserCollaborating(null));
    }
}
