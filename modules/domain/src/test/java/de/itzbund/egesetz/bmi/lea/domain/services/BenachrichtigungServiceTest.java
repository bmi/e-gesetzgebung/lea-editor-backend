// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {BenachrichtigungService.class})
class BenachrichtigungServiceTest {

    private final static User user = TestObjectsUtil.getUserEntity();

    @Autowired
    private BenachrichtigungService benachrichtigungService;

    @MockBean
    private PlategRequestService plategRequestService;

    @MockBean
    private UserService userService;

    @MockBean
    private CompoundDocument compoundDocument;

    @BeforeEach
    void init() throws Exception {
        when(userService.getUserForGid(any())).thenReturn(user);
        when(plategRequestService.setMessageInPlatform(any())).thenReturn(true);
    }

    @Test
    public void testLeserechteErteilen() {
        boolean actual = benachrichtigungService.leserechteErteilt(user, user, TestDTOUtil.getRandomCompoundDocumentSummaryDTO());
        assertThat(actual).isTrue(); // Modify this based on your actual logic
    }

    @Test
    public void testLeserechteEnziehen() {
        boolean actual = benachrichtigungService.leserechteEntzogen(user, user, TestDTOUtil.getRandomCompoundDocumentSummaryDTO());
        assertThat(actual).isTrue(); // Modify this based on your actual logic
    }

    @Test
    public void testSchreibrechteErteilen() {
        boolean actual = benachrichtigungService.schreibrechteErteilt(user, user, TestDTOUtil.getRandomCompoundDocumentSummaryDTO());
        assertThat(actual).isTrue(); // Modify this based on your actual logic
    }

}
