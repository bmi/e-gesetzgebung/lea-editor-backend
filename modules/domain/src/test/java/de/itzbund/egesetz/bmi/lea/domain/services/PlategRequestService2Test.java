// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.mappers.RegelungsvorhabenMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.model.PlatformMessage;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.client.MockMvcClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {PlategRequestService.class, RegelungsvorhabenMapperImpl.class, RestTemplate.class
})
class PlategRequestService2Test {

    @Autowired
    private PlategRequestService plategRequestService;

    @MockBean
    private RestTemplate restTemplate;

    @MockBean
    private LeageSession leageSession;

    @MockBean
    private RBACPermissionService rbacPermissionService;

    @MockBean
    private RegelungsvorhabenService regelungsvorhabenService;

    @Test
    public void testGetAllRegulatoryProposalsForUserHeIsAuthorOf() {
        when(leageSession.getUser()).thenReturn(TestObjectsUtil.getUserEntity());
        when(rbacPermissionService.getRegelungsvorhabenIdsMitNutzerZugriff(any(User.class))).thenReturn(List.of(UUID.randomUUID()));

        when(regelungsvorhabenService.getRegelungsvorhabenEditorDTO(any(RegelungsVorhabenId.class))).thenReturn(
            TestDTOUtil.getRandomRegelungsvorhabenEditorDTO());

        // Invoke the method to be tested
        List<RegelungsvorhabenEditorDTO> result = plategRequestService.getAllRegulatoryProposalsForUserHeIsAuthorOf();

        // Assert the result based on the mocked response
        assertThat(result).isNotEmpty(); // Modify this based on your actual logic
    }


    @Test
    public void testAllRegulatoryProposalsFromIdList() {
        when(leageSession.getUser()).thenReturn(TestObjectsUtil.getUserEntity());

        // Mocking restTemplate.exchange to return a specific response
        when(restTemplate.exchange(anyString(), any(), any(),
            any(ParameterizedTypeReference.class)))
            .thenReturn(new ResponseEntity<>(Collections.singletonList(
                TestDTOUtil.getRandomRegelungsvorhabenEditorDTO()),
                HttpStatus.OK));

        // Invoke the method to be tested
        List<RegelungsvorhabenEditorDTO> result = plategRequestService.getRegulatoryProposalsByIdList(List.of("00000000-0000-0000-0000-000000000005"));

        // Assert the result based on the mocked response
        assertThat(result).isNotEmpty(); // Modify this based on your actual logic

        MockMvcClientHttpRequestFactory x;

    }

    @Test
    public void testSetMessageInPlatform() {
        when(leageSession.getUser()).thenReturn(TestObjectsUtil.getUserEntity());

        // Mocking restTemplate.exchange to return a specific response
        when(restTemplate.exchange(
            any(String.class),
            eq(HttpMethod.POST),
            any(HttpEntity.class),
            eq(Void.class)))
            .thenReturn(ResponseEntity.ok().build());

        // Invoke the method to be tested
        boolean result = plategRequestService.setMessageInPlatform(PlatformMessage.builder().build());

        // Assert the result based on the mocked response
        assertThat(result).isTrue();
    }

    @Test
    public void testSetMessageInPlatformClientException() {
        when(leageSession.getUser()).thenReturn(TestObjectsUtil.getUserEntity());

        // Mocking restTemplate.exchange to return a specific response
        when(restTemplate.exchange(
            any(String.class),
            eq(HttpMethod.POST),
            any(HttpEntity.class),
            eq(Void.class)))
            .thenReturn(ResponseEntity.badRequest().build());

        // Invoke the method to be tested
        boolean result = plategRequestService.setMessageInPlatform(PlatformMessage.builder().build());

        // Assert the result based on the mocked response
        assertThat(result).isFalse();
    }

}
