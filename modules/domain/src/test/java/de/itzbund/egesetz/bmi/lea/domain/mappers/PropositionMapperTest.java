// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.mappers;

import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.ProponentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.ProposalSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RessortEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests for {@link PropositionMapper}
 */
class PropositionMapperTest {

    private PropositionMapper propositionMapper;


    @BeforeEach
    void init() {
        propositionMapper = new PropositionMapperImpl(new BaseMapperImpl());
    }


    @Test
    void testMapping_RegelungsvorhabenEditorShortDTO_to_PropositionDTO() {
        RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = TestDTOUtil.getRandomRegelungsvorhabenEditorDTO();

        PropositionDTO actual = propositionMapper.map(regelungsvorhabenEditorDTO);
        assertThat(actual).hasNoNullFieldsOrPropertiesExcept("farbe");

        // Proponenttest
        assertThat(actual.getProponentDTO()).hasNoNullFieldsOrProperties();

        assertEquals(regelungsvorhabenEditorDTO.getId(), actual.getId());
        assertEquals(regelungsvorhabenEditorDTO.getAbkuerzung(), actual.getAbbreviation());
        assertEquals(regelungsvorhabenEditorDTO.getStatus().name(), actual.getState().name());
        assertEquals(regelungsvorhabenEditorDTO.getVorhabenart().name(), actual.getType().name());
    }


    @Test
    void testMapping_RegelungsvorhabenEditorShortDTO_to_Proposition() {
        RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = TestDTOUtil.getRandomRegelungsvorhabenEditorDTO();

        Proposition actual = propositionMapper.mapD(regelungsvorhabenEditorDTO);
        assertThat(actual).hasNoNullFieldsOrPropertiesExcept("farbe", "referenzId");
    }


    @Test
    void testMapping_Proposition_to_RegelungsvorhabenEditortDTO() {
        Proposition proposition = TestObjectsUtil.getRandomProposition();

        RegelungsvorhabenEditorDTO actual = propositionMapper.mapD(proposition);
        assertThat(actual).hasNoNullFieldsOrPropertiesExcept("allFfRessorts", "kurzbeschreibung", "farbe");
    }


    @Test
    void testMapping_RessortEditorDTO_to_ProponentDTO() {
        RessortEditorDTO ressortEditorDTO = TestDTOUtil.getRandomRessortEditorDTO();

        ProponentDTO actual = propositionMapper.map(ressortEditorDTO);
        assertThat(actual).hasNoNullFieldsOrProperties();
    }

    @Test
    void testMapping_DocumentDomain_to_ProposalSummaryDTO() {
        DocumentDomain document = TestObjectsUtil.getDocumentEntity();
        ProposalSummaryDTO actual = propositionMapper.map(document);
        assertThat(actual).hasNoNullFieldsOrPropertiesExcept("proposalID", "version", "propositionID");
    }

}
