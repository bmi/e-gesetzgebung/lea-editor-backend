// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.validate;

import com.helger.schematron.xslt.SchematronResourceXSLT;
import de.itzbund.egesetz.bmi.lea.core.util.FlatGroupedList;
import de.itzbund.egesetz.bmi.lea.core.util.TestResourceProvider;
import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.ValidationService;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.LeaValidationResult;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.SchemaProvider;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationData;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationException;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationServiceImpl;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.Validator;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.xpath.XmlContentLocator;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.SchematronValidationTask;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.ValidationTask;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.XmlSchemaValidationHandler;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.XmlSchemaValidationTask;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.validation.Schema;
import java.io.ByteArrayInputStream;
import java.lang.reflect.Field;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

@Log4j2
@SpringBootTest(classes = {ValidationServiceImpl.class, Validator.class, SchemaProvider.class,
    TestValidationTask.class, ValidationData.class, XmlSchemaValidationTask.class, FlatGroupedList.class,
    SchematronValidationTask.class})
@ExtendWith(SpringExtension.class)
@SuppressWarnings("unused")
class ValidationTest {

    private static final String TEST_TASK_NAME = "TEST_TASK";
    @Autowired
    private ValidationService validationService;
    @Autowired
    private SchemaProvider schemaProvider;
    @Autowired
    private XmlSchemaValidationTask task;
    @Autowired
    private FlatGroupedList<ValidationTask> fgl;
    @Autowired
    private Validator validator;
    private ValidationTask t1;
    private ValidationTask t2;
    private ValidationTask t3;
    private ValidationTask t4;
    private ValidationTask t5;
    private ValidationTask t6;
    private ValidationTask t7;
    private ValidationTask t8;


    static Stream<Arguments> testResources() {
        return Stream.of(
            arguments(
                "/ldml/legalDocML.de-1.6/01-01_Gesetz_Stammform_Entwurf/01-01_instanz_01/01-01_instanz_01.xml"),
            arguments(
                "/ldml/legalDocML.de-1.6/01-01_Gesetz_Stammform_Entwurf/01-01_instanz_01/01-01_instanz_01_begruendung.xml"),
            arguments(
                "/ldml/legalDocML.de-1.6/01-01_Gesetz_Stammform_Entwurf/01-01_instanz_01/01-01_instanz_01_regelungstext.xml"),
            arguments(
                "/ldml/legalDocML.de-1.6/01-01_Gesetz_Stammform_Entwurf/01-01_instanz_01/01-01_instanz_01_vorblatt.xml")
        );
    }


    public void prepareTestRun() {
        t3.addPredecessor(t1);
        t3.addPredecessor(t2);

        t5.addPredecessor(t3);
        t5.addPredecessor(t4);

        t6.addPredecessor(t5);

        t7.addPredecessor(t5);

        t8.addPredecessor(t4);
    }


    @Autowired
    void setTasks(TestValidationTask t1, TestValidationTask t2, TestValidationTask t3, TestValidationTask t4,
        TestValidationTask t5, TestValidationTask t6, TestValidationTask t7, TestValidationTask t8) {
        this.t1 = t1;
        this.t2 = t2;
        this.t3 = t3;
        this.t4 = t4;
        this.t5 = t5;
        this.t6 = t6;
        this.t7 = t7;
        this.t8 = t8;
    }


    @Test
    void testTaskAssembler() {

        prepareTestRun();
        ValidationTask[] tasks = new ValidationTask[]{t1, t2, t3, t4, t5, t6, t7, t8};

        int[][] permutations = new int[][]{
            {0, 1, 2, 3, 4, 5, 6, 7},
            {3, 5, 7, 0, 2, 4, 6, 1},
            {7, 6, 5, 4, 3, 2, 1, 0}
        };

        fgl.makeGroups("G0", "G1", "G2", "G3");
        fgl.addElements("G0", false, t1, t2, t4);
        fgl.addElements("G1", false, t3, t8);
        fgl.addElements("G2", false, t5);
        fgl.addElements("G3", false, t6, t7);
        validator.clearTasks();
        for (int[] indexes : permutations) {

            for (int i : indexes) {
                validator.addValidationTask(tasks[i]);
            }
            validator.validate();
            assertTrue(fgl.conformsTo(validator.getWorkflow()));
        }
    }


    @Test()
    void testSchemaValidation() {
        log.info("no schema, no input");
        validator.clearTasks();
        validator.addValidationTask(task);
        validator.validate();
        assertFalse(validator.isSuccess());

        // AKN test case

        log.info("schema set, but no input");
        task.setSchema(schemaProvider.getSchema(SchemaProvider.SchemaInstance.OASIS_LEGAL_DOC_ML));
        validator.validate();
        assertFalse(validator.isSuccess());

        // LegalDocML.de test case

        log.info("LDML.de compatible test case");
        String filepath = "/ldml/legalDocML.de-1.6/01-01_Gesetz_Stammform_Entwurf/01-01_instanz_01/01-01_instanz_01_regelungstext.xml";
        ReusableInputSource ris = TestResourceProvider.getInstance().getSingleInputSource(filepath);
        InputSource xmlContent = new InputSource(ris.getByteStream());
        ValidationData validationData = validationService.validateLDML(xmlContent);
        assertEquals(0, validationData.getErrorCount());

        log.info("xml = null test");
        Optional<ValidationData> nullValidationData = Optional.ofNullable(validationService.validateLDML(null));
        assertFalse(nullValidationData.isPresent());
    }


    @Test
    @SneakyThrows
    void testSchematronValidation() {

        // successful case
        InputSource input = new InputSource(TestResourceProvider.getInstance().getSingleInputSource(
            "/ldml/legalDocML.de-1.6/01-01_Gesetz_Stammform_Entwurf/01-01_instanz_01/01-01_instanz_01_regelungstext.xml"
        ).getByteStream());
        ValidationData data = validationService.validateLDML(input);
        assertEquals(0, data.getErrorCount());

        // unsuccessful case
        input = new InputSource(
            ValidationTest.class.getResourceAsStream(
                "/schematron/regelungsentwurf_03_regelungstext-fehler.xml"));
        data = validationService.validateLDML(input);
        assertEquals(2, data.getErrorCount());

        // for code coverage
        Field validatorField = validationService.getClass().getDeclaredField("validator");
        validatorField.setAccessible(true);
        Validator validator = (Validator) validatorField.get(validationService);
        for (ValidationTask task : validator.getWorkflow()) {
            assertNotNull(task.getResult());
        }
    }


    @Test
    void testXmlSchemaValidationTask() {
        XmlSchemaValidationTask task = new XmlSchemaValidationTask();
        assertNotNull(task.getDescription());

        Schema schema = schemaProvider.getSchema(SchemaProvider.SchemaInstance.OASIS_LEGAL_DOC_ML);
        assertNotNull(schema);
        task.setSchema(schema);

        String testpath = "/validate/cl_Sesion56_2.xml";
        ReusableInputSource ris = TestResourceProvider.getInstance().getSingleInputSource(testpath);
        task.setInputSource(ris);

        ValidationData messages = new ValidationData();
        try { // validation successful
            task.run(messages);
        } catch (ValidationException | SAXNotSupportedException | SAXNotRecognizedException |
                 ParserConfigurationException e) {
            log.error(e);
        }
        assertNotNull(task.getResult());
        assertEquals(LeaValidationResult.VALIDATION_SUCCESS, task.getResult());
        assertEquals(0, messages.getErrorCount());

        schema = schemaProvider.getSchema(SchemaProvider.SchemaInstance.LEGAL_DOC_ML_DE_ANSCHREIBEN);
        assertNotNull(schema);
        task.setSchema(schema);
        task.setInputSource(ris);
        try { // validation fails
            task.run(messages);
        } catch (ValidationException | SAXNotSupportedException | SAXNotRecognizedException |
                 ParserConfigurationException e) {
            log.error(e);
        }
        assertNotNull(task.getResult());
        assertEquals(LeaValidationResult.VALIDATION_FAILURE, task.getResult());
        assertTrue(messages.getErrorCount() > 0);

        // validation results in technical error
        // running same task again tries to read an already read InputSource, which throws an exception
        ValidationData finalMessages = messages;
        assertThrows(ValidationException.class, () -> task.run(finalMessages));
        assertNotNull(task.getResult());
        assertEquals(LeaValidationResult.VALIDATION_ERROR, task.getResult());

        // call warning method of default handler
        messages = new ValidationData();
        XmlContentLocator locator = new XmlContentLocator();
        locator.initialize(ris);
        XmlSchemaValidationHandler handler = new XmlSchemaValidationHandler(task, messages, locator);
        SAXParseException exc = new SAXParseException("test", null, null, 0, 0);
        try {
            handler.warning(exc);
        } catch (SAXException e) {
            log.error(e);
        }
        assertEquals(1, messages.getMessageCount());
        assertEquals(1, messages.getWarningCount());
    }


    @Test
    @SneakyThrows
    void testValidationTaskToIncreaseCoverage() {
        ValidationTask task = new XmlSchemaValidationTask();
        assertNotNull(task.getId());

        task.setName(TEST_TASK_NAME);
        assertTrue(task.toString().contains(TEST_TASK_NAME));

        SchematronValidationTask schematronValidationTask = new SchematronValidationTask();
        assertNotNull(schematronValidationTask.getDescription());

        // ruleset is null
        schematronValidationTask.setRuleset("");
        SchematronValidationTask finalSchematronValidationTask1 = schematronValidationTask;
        Exception exc = assertThrows(ValidationException.class,
            () -> finalSchematronValidationTask1.run(new ValidationData()));
        assertEquals("no ruleset defined", exc.getMessage());

        // ruleset is invalid
        schematronValidationTask = new SchematronValidationTask();
        SchematronResourceXSLT ruleset = SchematronResourceXSLT.fromInputStream("invalid-schematron.xsl",
            Objects.requireNonNull(ValidationTest.class.getResourceAsStream("/validate/invalid-schematron.xsl")));
        schematronValidationTask.setRuleset(ruleset);
        SchematronValidationTask finalSchematronValidationTask = schematronValidationTask;
        assertThrows(ValidationException.class,
            () -> finalSchematronValidationTask.run(new ValidationData()));

        // no input provided
        schematronValidationTask = new SchematronValidationTask();
        ruleset = SchematronResourceXSLT.fromInputStream("valid-schematron.xsl",
            Objects.requireNonNull(ValidationTest.class.getResourceAsStream("/validate/valid-schematron.xsl")));
        schematronValidationTask.setRuleset(ruleset);
        SchematronValidationTask finalSchematronValidationTask2 = schematronValidationTask;
        assertThrows(ValidationException.class,
            () -> finalSchematronValidationTask2.run(new ValidationData()));

        // no SVRL report data available
        schematronValidationTask = new SchematronValidationTask();
        schematronValidationTask.setRuleset(ruleset);
        schematronValidationTask.setInputSource(
            new ReusableInputSource(
                ValidationTest.class.getResourceAsStream(
                    "/schematron/regelungsentwurf_03_regelungstext-fehler.xml"))
        );
        SchematronValidationTask finalSchematronValidationTask3 = schematronValidationTask;
        assertNotNull(finalSchematronValidationTask3.getResult());
    }


    @ParameterizedTest(name = "#{index} - Test with {0}")
    @MethodSource("testResources")
    void validate(String path) {
        String xml = "";
        log.debug("testing: " + path);
        try {
            xml = new String(Objects.requireNonNull(ValidationTest.class
                    .getResourceAsStream(path))
                .readAllBytes());
        } catch (Exception e) {
            log.error(e);
            fail("could not read data");
        }
        if (!xml.isBlank()) {
            ValidationData validationData = validationService
                .validateLDML(new InputSource((new ByteArrayInputStream(xml.getBytes()))));

            if (validationData.getErrorCount() > 0) {
                fail("validation failed");
            }
        } else {
            log.error("xml content could not be loaded: " + path);
        }
    }

}
