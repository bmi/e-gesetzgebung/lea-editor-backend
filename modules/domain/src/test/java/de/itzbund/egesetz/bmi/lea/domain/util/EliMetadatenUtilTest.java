/*
 * SPDX-License-Identifier: MPL-2.0
 *
 * Copyright (C) 2021-2023 Bundesministerium des Innern und für Heimat, \
 * Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
 */
package de.itzbund.egesetz.bmi.lea.domain.util;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.ProponentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.entities.DocumentCount;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.PropositionState;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentInitiant;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import de.itzbund.egesetz.bmi.lea.domain.mappers.BaseMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.services.eli.EliMetadatenUtil;
import de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRAttribute;
import de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement;
import de.itzbund.egesetz.bmi.lea.domain.services.export.ExportContext;
import lombok.Data;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.core.util.Utils.getRandomString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EliMetadatenUtilTest {

    @Mock
    private DokumentPersistencePort documentRepository;

    @Test
    void testCollectEliMetaDaten_Regelungstext() {
        // Arrange
        UUID rvId = UUID.fromString("10000000-0000-0000-0000-000000000000");
        String rvDateString = "2024-01-01";
        Instant rvDate = LocalDate.parse(rvDateString).atStartOfDay().toInstant(ZoneOffset.UTC);
        RechtsetzungsdokumentInitiant initiant = RechtsetzungsdokumentInitiant.BUNDESREGIERUNG;
        PropositionDTO testProposition = getProposition(rvId, rvDate, initiant);

        UUID dmId = UUID.fromString("10000000-1000-0000-0000-000000000000");
        String dmDateString = "2024-02-01";
        Instant dmDate = LocalDate.parse(dmDateString).atStartOfDay().toInstant(ZoneOffset.UTC);
        CompoundDocumentDomain testCompoundDocument = getCompoundDocument(dmId, rvId, dmDate);

        UUID docId = UUID.fromString("10000000-1000-1000-0000-000000000000");
        String docDateString = "2024-03-01";
        Instant docDate = LocalDate.parse(docDateString).atStartOfDay().toInstant(ZoneOffset.UTC);
        DocumentType docType = DocumentType.REGELUNGSTEXT_STAMMGESETZ;
        DocumentDomain testDocument = getDocument(docId, dmId, docDate, docType);

        when(documentRepository.getDocumentCount(any())).thenReturn(
            getDocumentCounts(
                new DocCountParas(new DocumentId(docId), docType, 1),
                new DocCountParas(new DocumentId(UUID.randomUUID()), DocumentType.VORBLATT_STAMMGESETZ, 1)));

        PropositionMapper propositionMapper = new PropositionMapperImpl(new BaseMapperImpl());
        Proposition proposition = propositionMapper.mapDto(testProposition);

        // Act
        Map<FRBRElement, Map<FRBRAttribute, String>> eliMetaData = EliMetadatenUtil.collectEliMetaDaten(
            new ExportContext(documentRepository, proposition, testCompoundDocument, testDocument));

        // Assert
        assertNotNull(eliMetaData);

        String expWorkFRBRthis = "eli/dl/2024/bundesregierung/1/regelungsentwurf/regelungstext-1";
        assertEquals(expWorkFRBRthis, eliMetaData.get(FRBRElement.WORK_FRBR_THIS).get(FRBRAttribute.FRBR_VALUE));
        String expWorkFRBRuri = "eli/dl/2024/bundesregierung/1/regelungsentwurf";
        assertEquals(expWorkFRBRuri, eliMetaData.get(FRBRElement.WORK_FRBR_URI).get(FRBRAttribute.FRBR_VALUE));
        String expExprFRBRthis = "eli/dl/2024/bundesregierung/1/regelungsentwurf/bundesregierung/2024-02-01/1/deu/regelungstext-1";
        assertEquals(expExprFRBRthis, eliMetaData.get(FRBRElement.EXPRESSION_FRBR_THIS).get(FRBRAttribute.FRBR_VALUE));
        String expExprFRBRuri = "eli/dl/2024/bundesregierung/1/regelungsentwurf/bundesregierung/2024-02-01/1/deu";
        assertEquals(expExprFRBRuri, eliMetaData.get(FRBRElement.EXPRESSION_FRBR_URI).get(FRBRAttribute.FRBR_VALUE));
        String expManiFRBRthis = "eli/dl/2024/bundesregierung/1/regelungsentwurf/bundesregierung/2024-02-01/1/deu/regelungstext-1.xml";
        assertEquals(expManiFRBRthis, eliMetaData.get(FRBRElement.MANIFESTATION_FRBR_THIS).get(FRBRAttribute.FRBR_VALUE));
        String expManiFRBRuri = "eli/dl/2024/bundesregierung/1/regelungsentwurf/bundesregierung/2024-02-01/1/deu/regelungstext-1.xml";
        assertEquals(expManiFRBRuri, eliMetaData.get(FRBRElement.MANIFESTATION_FRBR_URI).get(FRBRAttribute.FRBR_VALUE));
    }


    private PropositionDTO getProposition(UUID id, Instant modificationDate, RechtsetzungsdokumentInitiant initiant) {
        return PropositionDTO.builder()
            .id(id)
            .title(getRandomString())
            .shortTitle(getRandomString())
            .abbreviation(getRandomString())
            .state(PropositionState.IN_BEARBEITUNG)
            .proponentDTO(ProponentDTO.builder()
                .id(UUID.randomUUID())
                .designationNominative(Utils.getRandomString())
                .build())
            .initiant(initiant)
            .type(RechtsetzungsdokumentTyp.GESETZ)
            .bearbeitetAm(modificationDate)
            .build();
    }


    private CompoundDocumentDomain getCompoundDocument(UUID dmId, UUID rvId, Instant modificationDate) {
        User user = TestObjectsUtil.getUserEntity();
        return CompoundDocumentDomain.builder()
            .compoundDocumentId(new CompoundDocumentId(dmId))
            .title(getRandomString())
            .regelungsVorhabenId(new RegelungsVorhabenId(rvId))
            .type(CompoundDocumentTypeVariant.STAMMGESETZ)
            .createdAt(modificationDate)
            .createdBy(user)
            .updatedAt(modificationDate)
            .updatedBy(user)
            .build();
    }


    private DocumentDomain getDocument(UUID docId, UUID dmId, Instant modificationDate, DocumentType documentType) {
        User user = TestObjectsUtil.getUserEntity();
        return DocumentDomain.builder()
            .documentId(new DocumentId(docId))
            .title(getRandomString())
            .content(Utils.getContentOfTextResource("/eli-metadata/sample-regelungstext.json"))
            .type(documentType)
            .compoundDocumentId(new CompoundDocumentId(dmId))
            .createdAt(modificationDate)
            .createdBy(user)
            .updatedAt(modificationDate)
            .updatedBy(user)
            .build();
    }


    private List<DocumentCount> getDocumentCounts(DocCountParas... parameters) {
        List<DocumentCount> list = new ArrayList<>();
        Arrays.stream(parameters)
            .forEach(p -> list.add(DocumentCount.builder()
                .documentId(p.getDocumentId().getId().toString())
                .type(p.getDocumentType().name())
                .count(BigInteger.valueOf(p.getCount()))
                .build()));
        return list;
    }


    @Data
    static class DocCountParas {

        final DocumentId documentId;
        final DocumentType documentType;
        final int count;
    }

}
