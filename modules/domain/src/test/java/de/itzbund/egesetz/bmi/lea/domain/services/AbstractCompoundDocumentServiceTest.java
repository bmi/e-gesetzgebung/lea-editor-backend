// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.EditorRollenUndRechte;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.mappers.BaseMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.mappers.CompoundDocumentMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.CompoundDocumentMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.mappers.DocumentMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.mappers.UserMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.Template;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.BestandsrechtPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DrucksacheDokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.PropositionPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.TemplatePersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.EIdGeneratorRestPort;
import de.itzbund.egesetz.bmi.lea.domain.services.export.ZipExportFilenameBuilder;
import de.itzbund.egesetz.bmi.lea.domain.services.logging.DomainLoggingService;
import de.itzbund.egesetz.bmi.lea.domain.services.meta.MetadatenUpdateService;
import de.itzbund.egesetz.bmi.lea.domain.services.transform.JsonToXmlTransformer;
import de.itzbund.egesetz.bmi.lea.domain.services.transform.XmlToJsonTransformer;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.SchemaProvider;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationData;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationServiceImpl;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.Validator;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.CompoundDocumentContentValidationTask;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.SchematronValidationTask;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.task.XmlSchemaValidationTask;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import de.itzbund.egesetz.bmi.lea.domain.validator.DocumentPermissionValidator;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {
    // Mapper
    BaseMapperImpl.class, CompoundDocumentMapperImpl.class, DocumentMapperImpl.class,
    PropositionMapperImpl.class, UserMapperImpl.class,
    // Services
    ConverterService.class, CompoundDocumentService.class, DocumentService.class, DownloadService.class,
    PropositionService.class, TemplateService.class, ValidationServiceImpl.class, AuxiliaryService.class,
    UserService.class, DomainLoggingService.class,
    // Validation
    CompoundDocumentContentValidationTask.class, SchematronValidationTask.class, XmlSchemaValidationTask.class,
    Validator.class, ValidationData.class,
    // Repositories
    PropositionPersistencePort.class,
    // Rest
    CollectionDocumentBuilder.class, CompoundDocument.class, Document.class, JsonToXmlTransformer.class,
    SchemaProvider.class, XmlToJsonTransformer.class, ZipExportFilenameBuilder.class,

    //        EditorRollenUndRechte.class
})
@SuppressWarnings("unused")
abstract class AbstractCompoundDocumentServiceTest {

    protected final static String TITLE = "title";
    protected final static UUID sampleUUID = UUID.fromString(
        "00000000-0000-0000-0000-000000000005");
    protected final static CompoundDocumentDomain compoundDocumentEntity =
        TestObjectsUtil.getCompoundDocumentEntityWithSpecificId(
            sampleUUID);
    protected final static CompoundDocumentDomain compoundDocumentEntity2 = TestObjectsUtil.getCompoundDocumentEntity();

    @Autowired
    protected CompoundDocumentService compoundDocumentService;
    @Autowired
    protected CompoundDocumentMapper compoundDocumentMapper;
    @Autowired
    protected AuxiliaryService auxiliaryService;

    @MockBean
    protected DocumentPermissionValidator documentPermissionValidator;
    @MockBean
    protected CompoundDocumentPersistencePort compoundDocumentRepository;
    @MockBean
    protected DrucksacheDokumentPersistencePort drucksacheDokumentRepository;
    @MockBean
    protected DokumentPersistencePort documentRepository;
    @MockBean
    protected TemplatePersistencePort templateRepository;
    @MockBean
    protected UserPersistencePort userRepository;
    @MockBean
    protected PropositionPersistencePort propositionRepository;
    @MockBean
    protected BestandsrechtPersistencePort bestandsrechtRepository;
    @MockBean
    protected LeageSession session;
    @MockBean
    protected PlategRequestService plategRequestService;
    @MockBean
    protected PropositionService propositionService;
    @MockBean
    protected UserService userService;
    @MockBean
    protected RegelungsvorhabenService regelungsvorhabenService;
    @MockBean
    protected TemplateService templateService;
    @MockBean
    protected EIdGeneratorRestPort eIdGeneratorRestPort;
    @MockBean
    protected MetadatenUpdateService metadatenUpdateService;
    @MockBean
    protected CommentService commentService;
    @MockBean
    protected BestandsrechtService bestandsrechtService;

    // Wird im Testprojekt überschrieben und liefert immer erfolgreich gespeichert
    @MockBean
    protected EditorRollenUndRechte editorRollenUndRechte;
    @MockBean
    protected RBACPermissionService rbacPermissionService;
    @MockBean
    protected DomainLoggingService domainLoggingService;


    @BeforeEach
    void init() {
        when(session.getUser()).thenReturn(TestObjectsUtil.getUserEntity());
        when(userService.getUserForGid(any(String.class))).thenReturn(TestObjectsUtil.getUserEntity());
        when(userService.getUser()).thenReturn(TestObjectsUtil.getUserEntity());

        setupCompoundDocumentRepositoryMocks(compoundDocumentRepository);
        setupDocumentRepositoryMocks(documentRepository);
        setupTemplateRepositoryMock(DocumentType.REGELUNGSTEXT_STAMMGESETZ.getTemplateID() + "-1");
        setupRegelungsvorhabenServiceMock(regelungsvorhabenService);

        when(plategRequestService.getRegelungsvorhabenEditorDTO(any(UUID.class)))
            .thenAnswer(invocation ->
                TestDTOUtil.getRegelungsvorhabenEditorDTOWithSpecificId(sampleUUID)
            );
    }


    protected void setupRegelungsvorhabenServiceMock(RegelungsvorhabenService regelungsvorhabenService) {
        when(regelungsvorhabenService.getRegelungsvorhabenEditorTriggeredByDocument(any(RegelungsVorhabenId.class)))
            .thenReturn(TestDTOUtil.getRandomRegelungsvorhabenEditorDTO());
    }


    protected void setupDocumentRepositoryMocks(DokumentPersistencePort documentRepository) {
        when(documentRepository.save(any(DocumentDomain.class)))
            .thenAnswer(invocation ->
                {
                    DocumentDomain documentEntity = invocation.getArgument(0);
                    documentEntity.setCreatedAt(Instant.now());
                    documentEntity.setUpdatedAt(Instant.now());
                    return documentEntity;
                }
            );

        when(documentRepository.findByCompoundDocumentId(any(CompoundDocumentId.class)))
            .thenReturn(Optional.of(TestObjectsUtil.getDocumentEntityListOfSize(1)));
    }


    protected void setupCompoundDocumentRepositoryMocks(CompoundDocumentPersistencePort compoundDocumentRepository) {
        // save returns the elements that is saved
        when(compoundDocumentRepository.save(any(CompoundDocumentDomain.class)))
            .thenAnswer(invocation ->
                {
                    CompoundDocumentDomain compoundDocumentEntity = invocation.getArgument(0);
                    compoundDocumentEntity.setCreatedBy(TestObjectsUtil.getUserEntity());
                    compoundDocumentEntity.setUpdatedBy(TestObjectsUtil.getUserEntity());
                    compoundDocumentEntity.setCreatedAt(Instant.now());
                    compoundDocumentEntity.setUpdatedAt(Instant.now());
                    return compoundDocumentEntity;
                }
            );

        when(compoundDocumentRepository.findByCreatedBy(TestObjectsUtil.getUserEntity())).thenReturn(
            Arrays.asList(
                compoundDocumentEntity,
                compoundDocumentEntity2
            ));

        when(compoundDocumentRepository.findByCreatedByAndCompoundDocumentId(any(User.class),
            any(CompoundDocumentId.class)))
            .thenReturn(Optional.of(compoundDocumentEntity));

        when(compoundDocumentRepository.findByCompoundDocumentId(
            any(CompoundDocumentId.class)))
            .thenReturn(Optional.of(TestObjectsUtil.getRandomCompoundDocumentEntity()));

        compoundDocumentEntity.setState(DocumentState.BEREIT_FUER_KABINETT);
        when(compoundDocumentRepository.findByRegelungsVorhabenIdAndCompoundDocumentState(
            any(RegelungsVorhabenId.class), any()))
            .thenReturn(List.of(compoundDocumentEntity));

        Mockito.when(compoundDocumentRepository.findByCreatedByAndRegelungsvorhabenIdAndState(any(), any(), any()))
            .thenReturn(List.of(compoundDocumentEntity));
    }


    protected void setupTemplateRepositoryMock(String templateName) {
        when(templateRepository.findAllByTemplateNameOrderByVersionDescUpdatedAtDesc(templateName))
            .thenReturn(List.of(Template.builder()
                .content("[{}]")
                .build()));
    }

}
