// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.PinnedDokumentenmappenDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserSettingsDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.UserSettings;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserSettingsPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.rbacmock.NutzerService;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {UserService.class})
class UserServiceTest {

    protected final static UUID uuid = UUID.randomUUID();

    // ----- Services ---------
    @Autowired
    private UserService userService;
    @MockBean
    private UserSettingsPersistencePort userSettingsRepository;
    @MockBean
    private LeageSession session;
    @MockBean
    private UserSettings userSettings;
    @MockBean
    private UserPersistencePort userPersistencePort;
    @MockBean
    private PlategRequestService plategRequestService;
    @MockBean
    private NutzerService nutzerService;

    @BeforeEach
    void init() throws JsonProcessingException {
        MockitoAnnotations.openMocks(this);
        setupSession();
        setUpUserSettings();
    }

    protected void setupSession() {
        when(session.getUser()).thenReturn(TestObjectsUtil.getUserEntity());
    }

    protected void setUpUserSettings() throws JsonProcessingException {
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            ((ArrayList) args[1]).add(ServiceState.OK);
            return null;
        }).when(userSettingsRepository)
            .save(any(), any());

        UserSettingsDTO userSettingsDTO = UserSettingsDTO.builder().build();
        ObjectMapper objectMapper = new ObjectMapper();
        String newCofiguration = objectMapper.writeValueAsString(userSettingsDTO);

        UserSettings userSettings = UserSettings.builder().configuration(newCofiguration).build();
        when(userSettingsRepository.get(any())).thenReturn(Optional.of(userSettings));
    }

    @Test
    void whenUpdateUserSettingsThenSuccess() {

        List<ServiceState> status = new ArrayList<>();
        PinnedDokumentenmappenDTO pinnedDokumentenmappenDTO = PinnedDokumentenmappenDTO.builder().rvId(UUID.randomUUID()).dmIds(List.of(UUID.randomUUID()))
            .build();
        UserSettingsDTO userSettingsDTO = UserSettingsDTO.builder().pinnedDokumentenmappen(List.of(pinnedDokumentenmappenDTO)).build();

        userService.updateUserSettings(userSettingsDTO, status);
        assertFalse(status.isEmpty());
        assertEquals(ServiceState.OK, status.get(0));
    }

    @Test
    void whenGetUserSettingsThenSuccess() {
        UserSettingsDTO userSettingsDTO = userService.getUserSettings();
        assertNotEquals(null, userSettingsDTO);
    }


}
