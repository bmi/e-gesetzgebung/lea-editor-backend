// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.enums;

import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentForm;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.Abkuerzungsliteral;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EnumTest {

    @Test
    void test_vorhabenStatus_test() {
        VorhabenStatusType inBearbeitung = VorhabenStatusType.IN_BEARBEITUNG;
        assertEquals("IN_BEARBEITUNG", inBearbeitung.toString());

        VorhabenStatusType abgeschlossen = VorhabenStatusType.ARCHIVIERT;
        assertEquals("ARCHIVIERT", abgeschlossen.toString());

        VorhabenStatusType abgebrochen = VorhabenStatusType.BUNDESTAG;
        assertEquals("BUNDESTAG", abgebrochen.toString());

        VorhabenStatusType entwurf = VorhabenStatusType.ENTWURF;
        assertEquals("ENTWURF", entwurf.toString());

    }


    @Test
    void testDocumentType() {
        DocumentType type = DocumentType.VORBLATT_STAMMGESETZ;
        assertEquals("stammgesetz-vorblatt", type.getTemplateID());
        assertEquals("vorblatt", type.getShowAsLiteral());
        assertEquals(type, DocumentType.getFromTemplateID("stammgesetz-vorblatt"));
    }


    @Test
    void testAbkuerzungsliteral_FindeAbkuerzung() {
        assertEquals("text", Abkuerzungsliteral.getLiteral("akn:p", RechtsetzungsdokumentForm.STAMMFORM));
        assertEquals("", Abkuerzungsliteral.getLiteral("akn:p", RechtsetzungsdokumentForm.MANTELFORM));

        assertEquals("listegeor", Abkuerzungsliteral.getLiteral("akn:ol", RechtsetzungsdokumentForm.STAMMFORM));
        assertEquals("", Abkuerzungsliteral.getLiteral("akn:ol", RechtsetzungsdokumentForm.MANTELFORM));

        assertEquals("para", Abkuerzungsliteral.getLiteral("akn:article", RechtsetzungsdokumentForm.STAMMFORM));
        assertEquals("art", Abkuerzungsliteral.getLiteral("akn:article", RechtsetzungsdokumentForm.MANTELFORM));
    }


    @Test
    void testDocumentType_TemplateIDs() {
        assertEquals(new TreeSet<>(List.of(
                "anlage",
                "synopse",
                "mantelgesetz-anschreiben",
                "mantelgesetz-begruendung",
                "mantelgesetz-rechtsetzungsdokument",
                "mantelgesetz-regelungstext",
                "mantelgesetz-vorblatt",
                "stammgesetz-anschreiben",
                "stammgesetz-begruendung",
                "stammgesetz-rechtsetzungsdokument",
                "stammgesetz-regelungstext",
                "stammgesetz-vorblatt",
                "stammverordnung-anschreiben",
                "stammverordnung-begruendung",
                "stammverordnung-rechtsetzungsdokument",
                "stammverordnung-regelungstext",
                "stammverordnung-vorblatt",
                "mantelverordnung-anschreiben",
                "mantelverordnung-begruendung",
                "mantelverordnung-rechtsetzungsdokument",
                "mantelverordnung-regelungstext",
                "mantelverordnung-vorblatt",
                "nicht-vorhandenexternes-dokument-drucksache"
            )),
            Arrays.stream(DocumentType.values())
                .map(DocumentType::getTemplateID)
                .collect(Collectors.toCollection(TreeSet::new))
        );
    }

}
