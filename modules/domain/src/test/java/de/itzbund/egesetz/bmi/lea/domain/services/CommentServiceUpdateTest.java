// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.core.json.JSONUtils;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CommentPositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.DeleteCommentsDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.PatchCommentDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.mappers.BaseMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.mappers.CommentMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.CommentMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.mappers.ReplyMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.Position;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class CommentServiceUpdateTest extends CommentServiceTest {

	@Test
	void whenUpdateCommentForInValidDocument_thenNotFoundError() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();

		when(documentService.isDocumentExisting(any())).thenReturn(false);

		// Act
		commentService.updateComment(
			new DocumentId(UUID.randomUUID()),
			new CommentId(UUID.randomUUID()),
			PatchCommentDTO.builder().build(),
			status
		);

		// Assert
		assertEquals(1, status.size());
		assertEquals(ServiceState.DOCUMENT_NOT_FOUND, status.get(0));
	}


	@Test
	void whenUpdateCommentForInValidCommentId_thenNotFoundError() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();

		when(commentPersistencePort.findByCommentId(any())).thenReturn(null);

		// Act
		commentService.updateComment(
			new DocumentId(UUID.randomUUID()),
			new CommentId(UUID.randomUUID()),
			PatchCommentDTO.builder().build(),
			status
		);

		// Assert
		assertEquals(1, status.size());
		assertEquals(ServiceState.COMMENT_NOT_FOUND, status.get(0));
	}


	@Test
	void whenUpdateCommentIsAllowed_updateContent_thenSuccess() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();
		String origContent = Utils.getRandomString();
		String newContent = Utils.getRandomString();
		assertNotEquals(origContent, newContent);

		User userEntity = User.builder().gid(new UserId(Utils.getRandomString())).build();
		Comment commentEntity = Comment.builder().content(origContent).createdBy(userEntity).build();
		PatchCommentDTO patchCommentDTO = PatchCommentDTO.builder().content(newContent).build();

		when(documentService.isDocumentExisting(any())).thenReturn(true);
		when(documentPermissionValidator.isUserOwnerOfCommentEntity(any())).thenReturn(true);
		when(commentPersistencePort.findByCommentId(any())).thenReturn(commentEntity);

		// Act
		commentService.updateComment(
			new DocumentId(UUID.randomUUID()),
			new CommentId(UUID.randomUUID()),
			patchCommentDTO,
			status
		);

		// Assert
		assertEquals(1, status.size());
		assertEquals(ServiceState.OK, status.get(0));
		assertEquals(newContent, commentEntity.getContent());
	}

	@Test
	void testDeleteComment_Success() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();

		when(documentService.isDocumentExisting(any())).thenReturn(true);

		String content = Utils.getContentOfTextResource("/change/comment-testdocument.json");
		DocumentDomain returnDocument = TestObjectsUtil.getRandomDocumentEntity();
		returnDocument.setContent(content);
		when(documentService.getDocumentEntityById(any())).thenReturn(returnDocument);
		when(documentPermissionValidator.isUserOwnerOfCommentEntity(any())).thenReturn(true);

		DeleteCommentsDTO deleteCommentsDTO = TestObjectsUtil.getDeleteCommentsDTO();
		deleteCommentsDTO.setContent(content);
		deleteCommentsDTO.setCommentIds(List.of("0f522cdc-eeef-435f-89cc-94a78e139618"));
		// Act
		commentService.deleteComments(
			new DocumentId(UUID.randomUUID()), deleteCommentsDTO, status
		);

		// Assert
		assertEquals(1, status.size());
		assertEquals(ServiceState.OK, status.get(0));
	}


	@Test
	void testDeleteComment_NoDocumentFound() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();

		when(documentService.isDocumentExisting(any())).thenReturn(false);
		DeleteCommentsDTO deleteCommentsDTO = TestObjectsUtil.getDeleteCommentsDTO();

		// Act
		commentService.deleteComments(
			new DocumentId(UUID.randomUUID()),
			deleteCommentsDTO,
			status
		);

		// Assert
		assertEquals(1, status.size());
		assertEquals(ServiceState.DOCUMENT_NOT_FOUND, status.get(0));
	}


	@Test
	void whenUpdateCommentIsAllowed_updateEmptyContent_thenNoChange() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();
		String origContent = Utils.getRandomString();

		User userEntity = User.builder().gid(new UserId(Utils.getRandomString())).build();
		Comment commentEntity = Comment.builder().content(origContent).createdBy(userEntity).build();
		PatchCommentDTO patchCommentDTO = PatchCommentDTO.builder().content(null).build();

		when(documentService.isDocumentExisting(any())).thenReturn(true);
		when(documentPermissionValidator.isUserOwnerOfCommentEntity(any())).thenReturn(true);
		when(commentPersistencePort.findByCommentId(any())).thenReturn(commentEntity);

		// Act
		commentService.updateComment(
			new DocumentId(UUID.randomUUID()),
			new CommentId(UUID.randomUUID()),
			patchCommentDTO,
			status
		);

		// Assert
		assertEquals(1, status.size());
		assertEquals(ServiceState.OK, status.get(0));
		assertEquals(origContent, commentEntity.getContent());
	}


	@Test
	void whenUpdateCommentIsAllowed_updateSelection_thenSuccess() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();
		CommentMapper cm = new CommentMapperImpl(new BaseMapperImpl(), new ReplyMapperImpl(new BaseMapperImpl()));

		User userEntity = User.builder().gid(new UserId(Utils.getRandomString())).build();

		Position origAnchor = Position.builder()
			.offset(1)
			.path(Utils.getRandomListOfLong(3))
			.build();
		Position origFocus = Position.builder()
			.offset(2)
			.path(Utils.getRandomListOfLong(3))
			.build();
		CommentPositionDTO newAnchor = CommentPositionDTO.builder()
			.offset(3)
			.path(Utils.getRandomListOfLong(5))
			.build();
		CommentPositionDTO newFocus = CommentPositionDTO.builder()
			.offset(4)
			.path(Utils.getRandomListOfLong(7))
			.build();

		Comment commentEntity = Comment.builder()
			.anchor(origAnchor)
			.focus(origFocus)
			.createdBy(userEntity)
			.build();
		PatchCommentDTO patchCommentDTO = PatchCommentDTO.builder()
			.anchor(newAnchor)
			.focus(newFocus)
			.build();

		when(documentPermissionValidator.isUserOwnerOfCommentEntity(any())).thenReturn(true);
		when(commentPersistencePort.findByCommentId(any())).thenReturn(commentEntity);
		when(commentMapper.map(newAnchor)).thenReturn(cm.map(newAnchor));
		when(commentMapper.map(newFocus)).thenReturn(cm.map(newFocus));

		// Act
		commentService.updateComment(
			new DocumentId(UUID.randomUUID()),
			new CommentId(UUID.randomUUID()),
			patchCommentDTO,
			status
		);

		// Assert
		assertEquals(1, status.size());
		assertEquals(ServiceState.OK, status.get(0));
		assertEquals(commentMapper.map(newAnchor), commentEntity.getAnchor());
		assertEquals(commentMapper.map(newFocus), commentEntity.getFocus());
	}


	@Test
	void testDeleteAllCommentAnnotations() {
		// Arrange
		JSONObject documentWithComments = getDocumentObject("/change/comment-testdocument.json");

		// Act

		commentService.deleteAllCommentAttributes(documentWithComments);
		commentService.deleteAllCommentWrappers(documentWithComments);

		// Assert
		assertThat(documentWithComments.toJSONString())
			.doesNotContain("lea:commentMarkerWrapper")
			.doesNotContain("lea:commentMarker")
			.doesNotContain("lea:commentColor")
			.doesNotContain("lea:commentsOnElement");
	}

	private JSONObject getDocumentObject(String path) {
		String content = Utils.getContentOfTextResource(path);
		assertNotNull(content);
		assertFalse(content.isBlank());
		JSONObject resultObject = JSONUtils.getDocumentObject(content);
		assertNotNull(resultObject);
		return resultObject;
	}


}
