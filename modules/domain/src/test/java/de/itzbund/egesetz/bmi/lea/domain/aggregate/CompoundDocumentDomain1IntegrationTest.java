// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.aggregate;

import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.services.CommentService;
import de.itzbund.egesetz.bmi.lea.domain.services.RegelungsvorhabenService;
import de.itzbund.egesetz.bmi.lea.domain.services.meta.MetadatenUpdateService;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class) // JUnit 5
@SpringBootTest(classes = {CompoundDocument.class, Document.class})
@SuppressWarnings("unused")
class CompoundDocumentDomain1IntegrationTest {

	private final static UUID COMPOUND_DOCUMENT_UUID = UUID.randomUUID();
	private final static CompoundDocumentId COMPOUND_DOCUMENT_ID = new CompoundDocumentId(
		COMPOUND_DOCUMENT_UUID);
	@Autowired
	private CompoundDocument compoundDocument;
	@MockBean
	private CompoundDocumentPersistencePort compoundDocumentRepository;
	@MockBean
	private DokumentPersistencePort documentRepository;
	@MockBean
	private RegelungsvorhabenService regelungsvorhabenService;
	@MockBean
	private MetadatenUpdateService metadatenUpdateService;
	@MockBean
	private CommentService commentService;


	@Test
	void whenCompoundDocumentIsSet_ItMustAlsoBeSetInEntity() {
		// Arrange

		// Act
		compoundDocument.setCompoundDocumentId(COMPOUND_DOCUMENT_ID);
		CompoundDocumentDomain actual = compoundDocument.getCompoundDocumentEntity();

		// Assert
		assertThat(actual).isNotNull();
		assertThat(actual.getCompoundDocumentId()).isEqualTo(COMPOUND_DOCUMENT_ID);
	}


	@Test
	void whenCompoundDocumentIsChanged_ItMustAlsoBeChangedInEntity() {
		// Arrange
		compoundDocument.setCompoundDocumentId(COMPOUND_DOCUMENT_ID);
		compoundDocument.setCompoundDocumentEntity(
			TestObjectsUtil.getCompoundDocumentEntityWithSpecificId(COMPOUND_DOCUMENT_UUID));

		// Act
		CompoundDocumentId newId = new CompoundDocumentId(UUID.randomUUID());
		compoundDocument.setCompoundDocumentId(newId);
		CompoundDocumentDomain actual = compoundDocument.getCompoundDocumentEntity();

		// Assert
		assertThat(actual).isNotNull();
		assertThat(actual.getCompoundDocumentId()).isEqualTo(compoundDocument.getCompoundDocumentId());
	}


	@Test
	void whenUpdateWithGivenCompoundDocument_ItMustBeUsed() {
		// Arrange
		Mockito.when(compoundDocumentRepository.save(Mockito.any(CompoundDocumentDomain.class)))
			.thenAnswer(invocation ->
				{
					CompoundDocumentDomain compoundDocumentEntity = invocation.getArgument(0);
					compoundDocumentEntity.setCreatedAt(Instant.now());
					compoundDocumentEntity.setUpdatedAt(Instant.now());
					return compoundDocumentEntity;
				}
			);

		CompoundDocument expected = TestObjectsUtil.getRandomCompoundDocument();

		// Act
		CompoundDocument actual = compoundDocument.createOrUpdate(expected, null);

		// Assert
		assertThat(actual).hasFieldOrPropertyWithValue("compoundDocumentEntity", expected.getCompoundDocumentEntity())
			.hasFieldOrPropertyWithValue("compoundDocumentId", expected.getCompoundDocumentId());
	}
}
