// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.transform;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.skyscreamer.jsonassert.JSONAssert;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.StringWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.util.Utils.getDocumentBuilderFactory;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

/**
 * Testsuite for class {@link XmlToJsonTransformer}.
 */

@Log4j2
class XmlToJsonTransformerTest {

    private static final JSONParser jsonParser = new JSONParser();

    private static XmlToJsonTransformer transformer;


    @SuppressWarnings("unused")
    static Stream<Arguments> testResources() {
        return Stream.of(
            arguments("/transform/stammgesetz-begruendung"),
            arguments("/transform/stammgesetz-regelungstext-0"),
            arguments("/transform/stammgesetz-regelungstext-1"),
            arguments("/transform/stammgesetz-regelungstext-2"),
            arguments("/transform/stammgesetz-regelungstext-3"),
            arguments("/transform/stammgesetz-vorblatt")
        );
    }


    @BeforeAll
    static void setUp() {
        transformer = new XmlToJsonTransformer();
    }


    @AfterAll
    static void tearDown() {
        transformer = null;
    }


    @ParameterizedTest(name = "#{index} - Test with {0}")
    @MethodSource("testResources")
    @SneakyThrows
    void testTransformDocument(String filePath) {
        String xmlInputFile = filePath + ".xml";
        String jsonExpectedFile = filePath + ".json";

        URL url = XmlToJsonTransformerTest.class.getResource(xmlInputFile);
        if (url == null) {
            fail("could not find xml resource: " + xmlInputFile);
        }

        FileReader reader = new FileReader(url.getFile(), StandardCharsets.UTF_8);
        StringWriter writer = new StringWriter();
        reader.transferTo(writer);
        reader.close();

        String xmlString = writer.toString();

        String jsonString = transformer.transform(xmlString);
        assertNotNull(jsonString);
        assertFalse(jsonString.isBlank());
        assertTrue(Utils.isJSONValid(jsonString));

        DocumentBuilder documentBuilder = getDocumentBuilderFactory().newDocumentBuilder();
        Document xmlContent = documentBuilder.parse(
            new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8)));
        assertNotNull(xmlContent);

        JSONArray jsonArray = (JSONArray) jsonParser.parse(jsonString);
        JSONObject jsonContent = (JSONObject) jsonArray.get(0);
        assertNotNull(jsonContent);

        Path jsonFile = Paths.get(
            Objects.requireNonNull(XmlToJsonTransformerTest.class.getResource(jsonExpectedFile)).toURI());
        log.debug("JSON file: " + jsonFile);
        assertTrue(jsonFile.toFile().canRead());
        String expectedJsonString = Files.readString(jsonFile);

        JSONAssert.assertEquals(expectedJsonString, jsonString, true);
    }

}
