// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.DMBestandsrechtLinkDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtListDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.Rights;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.BestandsrechtId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.BestandsrechtPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.services.exceptions.DokumentenmappeNotFoundException;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {BestandsrechtService.class})
@SuppressWarnings("unused")
class BestandsrechtServiceIntegrationTest {

    private final static UUID sampleUUID = UUID.fromString(
        "00000000-0000-0000-0000-000000000005");
    private final static CompoundDocumentDomain compoundDocumentEntity =
        TestObjectsUtil.getCompoundDocumentEntityWithSpecificId(
            sampleUUID);


    @Autowired
    private BestandsrechtService bestandsrechtService;

    @MockBean
    private CompoundDocument compoundDocument;
    @MockBean
    private CompoundDocumentService compoundDocumentService;
    @MockBean
    private ConverterService converterService;
    @MockBean
    private UserService userService;
    @MockBean
    private RBACPermissionService rbacPermissionService;
    @MockBean
    private BestandsrechtPersistencePort bestandsrechtRepository;
    @MockBean
    private CompoundDocumentPersistencePort compoundDocumentRepository;


    @Test
    void testGetBestandsrechtForCompoundDocumentId() {

        BestandsrechtListDTO bestandsrechtListDTO = new BestandsrechtListDTO();

        // Arrange
        when(compoundDocument.getCompoundDocumentWithDocumentsForIdAndOptionalFiltered(any(CompoundDocumentId.class), any()))
            .thenReturn(compoundDocument);
        when(compoundDocument.getRegelungsVorhabenId()).thenReturn(new RegelungsVorhabenId(sampleUUID));
        when(bestandsrechtRepository.getBestandsrechtEntitiesById(any()))
            .thenReturn(List.of(bestandsrechtListDTO));
        when(compoundDocumentService.getCompoundDocumentDomainEntity(any()))
            .thenReturn(compoundDocument);
        when(rbacPermissionService.getAccessRightsForCurrentUserOnGivenResource(any(), any()))
            .thenReturn(List.of(Rights.SCHREIBEN));

        // Act
        UUID compoundDocumentId = UUID.randomUUID();
        List<ServiceState> status = new ArrayList<>();

        List<BestandsrechtListDTO> actual = bestandsrechtService.getBestandsrechtForCompoundDocumentId(compoundDocumentId, status);

        // Assert
        assertThat(actual).asList().hasSize(1);
    }


    @Test
    void testErrorGetBestandsrechtForCompoundDocumentId() {

        BestandsrechtListDTO bestandsrechtListDTO = new BestandsrechtListDTO();

        // Arrange
        when(compoundDocument.getCompoundDocumentWithDocumentsForIdAndOptionalFiltered(any(CompoundDocumentId.class), any()))
            .thenReturn(null);
        when(bestandsrechtRepository.getBestandsrechtEntitiesById(any()))
            .thenReturn(List.of(bestandsrechtListDTO));

        // Act - Assert
        UUID compoundDocumentId = UUID.randomUUID();
        List<ServiceState> status = new ArrayList<>();

        assertThrows(DokumentenmappeNotFoundException.class, () -> bestandsrechtService.getBestandsrechtForCompoundDocumentId(compoundDocumentId, status));

    }

    @Test
    void whenMappeNotFound_thenFailure() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        when(compoundDocumentRepository.findByCreatedByAndCompoundDocumentId(any(), any()))
            .thenReturn(Optional.empty());
        when(compoundDocumentRepository.findByCompoundDocumentId(any()))
            .thenReturn(Optional.empty());

        // Act - Assert
        assertThrows(DokumentenmappeNotFoundException.class, () -> bestandsrechtService.getBestandsrechtLinks(sampleUUID, status));
    }

    @Test
    void whenNoBestandsrechtAssigned_thenReturnEmptyList_andStatusIsOK() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        when(compoundDocumentRepository.findByCreatedByAndCompoundDocumentId(any(), any()))
            .thenReturn(Optional.of(compoundDocumentEntity));
        when(compoundDocumentRepository.findByCompoundDocumentId(any()))
            .thenReturn(Optional.of(compoundDocumentEntity));
        when(bestandsrechtRepository.getBestandsrechtAssociationsByRvId(any()))
            .thenReturn(Collections.emptyList());
        when(bestandsrechtRepository.getBestandsrechtEntitiesById(any()))
            .thenReturn(Collections.emptyList());
        when(compoundDocumentService.getCompoundDocumentDomainEntity(any()))
            .thenReturn(compoundDocument);
        when(compoundDocument.getRegelungsVorhabenId()).thenReturn(new RegelungsVorhabenId(sampleUUID));
        when(rbacPermissionService.getAccessRightsForCurrentUserOnGivenResource(any(), any()))
            .thenReturn(List.of(Rights.SCHREIBEN));

        // Act
        List<DMBestandsrechtLinkDTO> links = bestandsrechtService.getBestandsrechtLinks(sampleUUID, status);

        // Assert
        assertFalse(status.isEmpty());
        assertEquals(ServiceState.OK, status.get(0));
        assertNotNull(links);
        assertTrue(links.isEmpty());
    }


    @Test
    void whenBestandsrechtAssigned_thenReturnListOfLinkedDM_andStatusIsOK() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        when(compoundDocumentRepository.findByCreatedByAndCompoundDocumentId(any(), any()))
            .thenReturn(Optional.of(compoundDocumentEntity));
        when(compoundDocumentRepository.findByCompoundDocumentId(any()))
            .thenReturn(Optional.of(compoundDocumentEntity));
        when(bestandsrechtRepository.getBestandsrechtAssociationsByRvId(any()))
            .thenReturn(List.of(new BestandsrechtId(sampleUUID)));
        when(bestandsrechtRepository.getBestandsrechtEntitiesById(any()))
            .thenReturn(List.of(BestandsrechtListDTO.builder().id(sampleUUID).build()));
        when(bestandsrechtRepository.getLinkedCompoundDocuments(any()))
            .thenReturn(List.of(DMBestandsrechtLinkDTO.builder().bestandsrechtId(new BestandsrechtId(sampleUUID)).build()));
        when(compoundDocumentService.getCompoundDocumentDomainEntity(any()))
            .thenReturn(compoundDocument);
        when(compoundDocument.getRegelungsVorhabenId()).thenReturn(new RegelungsVorhabenId(sampleUUID));
        when(rbacPermissionService.getAccessRightsForCurrentUserOnGivenResource(any(), any()))
            .thenReturn(List.of(Rights.SCHREIBEN));

        // Act
        List<DMBestandsrechtLinkDTO> links = bestandsrechtService.getBestandsrechtLinks(sampleUUID, status);

        // Assert
        assertFalse(status.isEmpty());
        assertEquals(ServiceState.OK, status.get(0));
        assertNotNull(links);
        assertFalse(links.isEmpty());
    }

}
