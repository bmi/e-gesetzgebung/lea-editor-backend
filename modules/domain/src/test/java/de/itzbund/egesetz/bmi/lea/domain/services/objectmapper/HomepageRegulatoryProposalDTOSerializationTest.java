// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.objectmapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.HomepageCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.HomepageRegulatoryProposalDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.HomepageSingleDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.PermissionDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.FarbeType;
import de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType.BUNDESTAG;
import static de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType.ENTWURF;
import static de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType.IN_BEARBEITUNG;

class HomepageRegulatoryProposalDTOSerializationTest {

	private static ObjectMapper objectMapper;


	/**
	 * see {link JacksonConfiguration}
	 */
	@BeforeAll
	static void setUp() {
		objectMapper = new ObjectMapper();
		objectMapper.registerModule(new Jdk8Module());
		objectMapper.registerModule(new JavaTimeModule());
		objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
	}


	@Test
	@SneakyThrows
	void test_SerializeEmptyList() {
		List<HomepageRegulatoryProposalDTO> regulatoryProposalDTOS = List.of();
		JSONAssert.assertEquals("[]",
			objectMapper.writeValueAsString(regulatoryProposalDTOS),
			JSONCompareMode.STRICT);
	}


	@Test
	@SneakyThrows
	void test_SerializeList_OneRV_NoDM() {
		UUID rvId = UUID.randomUUID();
		String abbrev = Utils.getRandomString();
		VorhabenStatusType rvStatus = IN_BEARBEITUNG;
		List<HomepageRegulatoryProposalDTO> regulatoryProposalDTOS = List.of(
			HomepageRegulatoryProposalDTO.builder()
				.id(rvId)
				.abbreviation(abbrev)
				.status(rvStatus)
				.children(List.of())
				.build()
		);

		String expected = "[\n"
			+ "    {\n"
			+ "        \"entityType\": \"REGULATORY_PROPOSAL\",\n"
			+ "        \"id\": \"" + rvId + "\",\n"
			+ "        \"abbreviation\": \"" + abbrev + "\",\n"
			+ "        \"farbe\": null,\n"
			+ "        \"status\": \"" + rvStatus.name() + "\",\n"
			+ "        \"children\": []\n"
			+ "    }\n"
			+ "]";
		String actual = objectMapper.writeValueAsString(regulatoryProposalDTOS);

		JSONAssert.assertEquals(expected, actual, JSONCompareMode.STRICT);
	}


	@Test
	@SneakyThrows
	void test_SerializeList_OneRV_OneDM_NoDoc() {
		UUID rvId = UUID.randomUUID();
		UUID dmId = UUID.randomUUID();
		String abbrev = Utils.getRandomString();
		String dmTitle = Utils.getRandomString();
		String dmVersion = "1";
		DocumentState dmState = DocumentState.DRAFT;
		VorhabenStatusType rvStatus = ENTWURF;
		List<HomepageRegulatoryProposalDTO> regulatoryProposalDTOS = List.of(
			HomepageRegulatoryProposalDTO.builder()
				.id(rvId)
				.abbreviation(abbrev)
				.farbe(FarbeType.LILA)
				.status(rvStatus)
				.children(List.of(
					HomepageCompoundDocumentDTO.builder()
						.id(dmId)
						.regulatoryProposalId(rvId)
						.regulatoryProposalState(rvStatus)
						.title(dmTitle)
						.version(dmVersion)
						.type(CompoundDocumentTypeVariant.STAMMGESETZ)
						.state(dmState)
						.isNewest(false)
						.isPinned(false)
						.children(List.of())
						.build()
				))
				.build()
		);

		String expected = "[\n"
			+ "    {\n"
			+ "        \"entityType\": \"REGULATORY_PROPOSAL\",\n"
			+ "        \"id\": \"" + rvId + "\",\n"
			+ "        \"abbreviation\": \"" + abbrev + "\",\n"
			+ "        \"farbe\": \"LILA\",\n"
			+ "        \"status\": \"" + rvStatus.name() + "\",\n"
			+ "        \"children\": [\n"
			+ "            {\n"
			+ "                \"entityType\": \"COMPOUND_DOCUMENT\",\n"
			+ "                \"id\": \"" + dmId + "\",\n"
			+ "                \"regulatoryProposalId\": \"" + rvId + "\",\n"
			+ "                \"regulatoryProposalState\": \"" + rvStatus.name() + "\",\n"
			+ "                \"title\": \"" + dmTitle + "\",\n"
			+ "                \"type\": \"STAMMGESETZ\",\n"
			+ "                \"state\": \"" + dmState.name() + "\",\n"
			+ "                \"isNewest\": false,\n"
			+ "                \"isPinned\": false,\n"
			+ "                \"version\": \"" + dmVersion + "\",\n"
			+ "                \"children\": [],\n"
			+ "                \"documentPermissions\": null,\n"
			+ "                \"commentPermissions\": null\n"
			+ "            }\n"
			+ "        ]\n"
			+ "    }\n"
			+ "]";
		String actual = objectMapper.writeValueAsString(regulatoryProposalDTOS);

		JSONAssert.assertEquals(expected, actual, JSONCompareMode.STRICT);
	}


	@Test
	@SneakyThrows
	void test_SerializeList_OneRV_OneDM_OneDoc() {
		UUID rvId = UUID.randomUUID();
		UUID dmId = UUID.randomUUID();
		UUID docId = UUID.randomUUID();
		String abbrev = Utils.getRandomString();
		String dmTitle = Utils.getRandomString();
		String docTitle = Utils.getRandomString();
		String dmVersion = "99";
		DocumentType docType = DocumentType.REGELUNGSTEXT_STAMMGESETZ;
		DocumentState dmState = DocumentState.DRAFT;
		VorhabenStatusType rvStatus = BUNDESTAG;
		Instant docUpdatedAt = Instant.now();
		Utils.UserData docUpdatedBy = Utils.getRandomUserData();
		User user = User.builder()
			.gid(new UserId(docUpdatedBy.getGid()))
			.name(docUpdatedBy.getName())
			.email(docUpdatedBy.getEmail())
			.build();
		List<HomepageRegulatoryProposalDTO> regulatoryProposalDTOS = List.of(
			HomepageRegulatoryProposalDTO.builder()
				.id(rvId)
				.abbreviation(abbrev)
				.farbe(FarbeType.BLAU)
				.status(rvStatus)
				.children(List.of(
					HomepageCompoundDocumentDTO.builder()
						.id(dmId)
						.regulatoryProposalId(rvId)
						.regulatoryProposalState(rvStatus)
						.title(dmTitle)
						.version(dmVersion)
						.type(CompoundDocumentTypeVariant.STAMMGESETZ)
						.state(dmState)
						.isNewest(false)
						.isPinned(false)
						.children(List.of(
							HomepageSingleDocumentDTO.builder()
								.id(docId)
								.regulatoryProposalId(rvId)
								.title(docTitle)
								.type(docType)
								.state(dmState)
								.createdAt(docUpdatedAt)
								.createdBy(user)
								.updatedAt(docUpdatedAt)
								.updatedBy(user)
								.documentPermissions(PermissionDTO.builder()
									.hasRead(true)
									.hasWrite(false)
									.build())
								.commentPermissions(PermissionDTO.builder()
									.hasRead(true)
									.hasWrite(false)
									.build())
								.build()
						))
						.documentPermissions(PermissionDTO.builder()
							.hasRead(true)
							.hasWrite(true)
							.build())
						.commentPermissions(PermissionDTO.builder()
							.hasRead(true)
							.hasWrite(true)
							.build())
						.build()
				))
				.build()
		);

		String expected = "[\n"
			+ "  {\n"
			+ "    \"id\": \"" + rvId + "\",\n"
			+ "    \"abbreviation\": \"" + abbrev + "\",\n"
			+ "    \"farbe\": \"BLAU\",\n"
			+ "    \"status\": \"" + rvStatus.name() + "\",\n"
			+ "    \"children\": [\n"
			+ "      {\n"
			+ "        \"id\": \"" + dmId + "\",\n"
			+ "        \"regulatoryProposalId\": \"" + rvId + "\",\n"
			+ "        \"regulatoryProposalState\": \"" + rvStatus.name() + "\",\n"
			+ "        \"title\": \"" + dmTitle + "\",\n"
			+ "        \"type\": \"STAMMGESETZ\",\n"
			+ "        \"state\": \"" + dmState + "\",\n"
			+ "        \"isNewest\": false,\n"
			+ "        \"isPinned\": false,\n"
			+ "        \"version\": \"" + dmVersion + "\",\n"
			+ "        \"children\": [\n"
			+ "          {\n"
			+ "            \"id\": \"" + docId + "\",\n"
			+ "            \"regulatoryProposalId\": \"" + rvId + "\",\n"
			+ "            \"title\": \"" + docTitle + "\",\n"
			+ "            \"type\": \"" + docType + "\",\n"
			+ "            \"state\": \"" + dmState + "\",\n"
			+ "            \"createdAt\": \"" + docUpdatedAt + "\",\n"
			+ "            \"createdBy\": {\n"
			+ "              \"plattformUserId\": null,\n"
			+ "              \"gid\": { \"id\": \"" + docUpdatedBy.getGid() + "\" },\n"
			+ "              \"name\": \"" + docUpdatedBy.getName() + "\",\n"
			+ "              \"email\": \"" + docUpdatedBy.getEmail() + "\"\n"
			+ "            },\n"
			+ "            \"updatedAt\": \"" + docUpdatedAt + "\",\n"
			+ "            \"updatedBy\": {\n"
			+ "              \"plattformUserId\": null,\n"
			+ "              \"gid\": { \"id\": \"" + docUpdatedBy.getGid() + "\" },\n"
			+ "              \"name\": \"" + docUpdatedBy.getName() + "\",\n"
			+ "              \"email\": \"" + docUpdatedBy.getEmail() + "\"\n"
			+ "            },\n"
			+ "            \"documentPermissions\": { \"hasRead\": true, \"hasWrite\": false },\n"
			+ "            \"commentPermissions\": { \"hasRead\": true, \"hasWrite\": false },\n"
			+ "            \"entityType\": \"SINGLE_DOCUMENT\"\n"
			+ "          }\n"
			+ "        ],\n"
			+ "        \"documentPermissions\": { \"hasRead\": true, \"hasWrite\": true },\n"
			+ "        \"commentPermissions\": { \"hasRead\": true, \"hasWrite\": true },\n"
			+ "        \"entityType\": \"COMPOUND_DOCUMENT\"\n"
			+ "      }\n"
			+ "    ],\n"
			+ "    \"entityType\": \"REGULATORY_PROPOSAL\"\n"
			+ "  }\n"
			+ "]\n";
		String actual = objectMapper.writeValueAsString(regulatoryProposalDTOS);

		JSONAssert.assertEquals(expected, actual, JSONCompareMode.NON_EXTENSIBLE);
	}

}
