// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.filters;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRAttribute;
import de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRElement;
import de.itzbund.egesetz.bmi.lea.domain.util.TestEliMetadataUtil;
import lombok.extern.log4j.Log4j2;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.Instant;
import java.util.Map;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBREXPRESSION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRMANIFESTATION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRTHIS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRURI;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_FRBRWORK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_IDENTIFICATION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_META;
import static de.itzbund.egesetz.bmi.lea.core.Constants.INSTANT_TO_DATE_FORMATTER;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDescendant;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDocumentObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getStringAttribute;
import static de.itzbund.egesetz.bmi.lea.domain.services.eli.FRBRAttribute.FRBR_VALUE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@Log4j2
class EliMetaDataFilterTest {

    private static final String TESTFILE_PATH = "/eli-metadata/sample-regelungstext.json";

    // - Mocking a FilterChain
    private final FilterChain mockChain = mock(FilterChain.class);
    private final Map<FRBRElement, Map<FRBRAttribute, String>> eliMetaData = TestEliMetadataUtil.getTestEliMetaData();
    private JSONObject documentObject;

    @BeforeEach
    void setUp() throws IOException {
        String jsonString = Utils.getContentOfTextResource(TESTFILE_PATH);
        documentObject = getDocumentObject(jsonString);
    }


    @Test
    void testEliMetaDataFilter() {
        // Creating an instance of the filter
        EliMetaDataFilter filter = new EliMetaDataFilter(eliMetaData);
        filter.setDateValue(INSTANT_TO_DATE_FORMATTER.format(Instant.now()));
        filter.setFederfuehrer("Bundesregierung");

        // Act
        filter.doFilter(documentObject, mockChain);
        String actual = documentObject.toJSONString();

        // Assert
        String workUri = getStringAttribute(getDescendant(documentObject, true, "*", ELEM_META, ELEM_IDENTIFICATION,
            ELEM_FRBRWORK, ELEM_FRBRTHIS), FRBR_VALUE.getLdmlName());
        assertEquals("eli/dl/2020/BUND/0815/RV/DOKUMENT-0", workUri);

        String exprUri = getStringAttribute(getDescendant(documentObject, true, "*", ELEM_META, ELEM_IDENTIFICATION,
            ELEM_FRBREXPRESSION, ELEM_FRBRTHIS), FRBR_VALUE.getLdmlName());
        assertEquals("eli/dl/2020/BUND/0815/RV/LAND/2023-03-03/4711/GER/DOKUMENT-0", exprUri);

        String manifestUri = getStringAttribute(getDescendant(documentObject, true, "*", ELEM_META, ELEM_IDENTIFICATION,
            ELEM_FRBRMANIFESTATION, ELEM_FRBRURI), FRBR_VALUE.getLdmlName());
        assertEquals("eli/dl/2020/BUND/0815/RV/LAND/2023-03-03/4711/GER/DOKUMENT-0.PDF", manifestUri);

        // - Verify that the next filter in the chain was called
        verify(mockChain).doFilter(documentObject);
    }

}
