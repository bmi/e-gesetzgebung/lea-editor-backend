// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType;
import de.itzbund.egesetz.bmi.lea.domain.mappers.BaseMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.PropositionPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

@ExtendWith({SpringExtension.class}) // JUnit 5
@SpringBootTest(classes = {BaseMapperImpl.class, PropositionMapperImpl.class,
    RegelungsvorhabenService.class
})
class RegelungsvorhabenServiceIntegrationTest {

    private final List<RegelungsvorhabenEditorDTO> regelungsvorhabenList = Arrays.asList(
        TestDTOUtil.getRandomRegelungsvorhabenEditorDTO(),
        TestDTOUtil.getRandomRegelungsvorhabenEditorDTO(),
        TestDTOUtil.getRandomRegelungsvorhabenEditorDTO(),
        TestDTOUtil.getRandomRegelungsvorhabenEditorDTO());
    @Autowired
    private RegelungsvorhabenService regelungsvorhabenService;

    @Autowired
    private PropositionMapper propositionMapper;

    @MockBean
    private PlategRequestService plategRequestService;
    @MockBean
    private PropositionPersistencePort propositionRepository;

    private List<Proposition> saveActions;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        saveActions = new ArrayList<>();

        when(plategRequestService.getRegelungsvorhabenEditorDTO(any(UUID.class)))
            .thenThrow(new HttpClientErrorException(HttpStatus.BAD_GATEWAY));

        regelungsvorhabenList.get(0).setStatus(VorhabenStatusType.ENTWURF);
        regelungsvorhabenList.get(1).setStatus(VorhabenStatusType.IN_BEARBEITUNG);
        regelungsvorhabenList.get(2).setStatus(VorhabenStatusType.ARCHIVIERT);
        regelungsvorhabenList.get(3).setStatus(VorhabenStatusType.BUNDESTAG);

        when(plategRequestService.getAllRegulatoryProposalsForUserHeIsAuthorOf())
            .thenReturn(regelungsvorhabenList);

        when(propositionRepository.save(any(Proposition.class))).thenAnswer(i -> {
            Proposition proposition = i.getArgument(0);
            saveActions.add(proposition);
            return proposition;
        });

        when(propositionRepository.copy(any(Proposition.class))).thenAnswer(i -> {
            Proposition proposition = i.getArgument(0);
            proposition.setReferenzId(UUID.randomUUID());
            saveActions.add(proposition);
            return proposition;
        });
    }


    @Test
    void whenRegelungsvorhabenIsNotValid_ADummyMustBeProvided() {
        // Arrange
        RegelungsvorhabenEditorDTO expected = PropositionServiceUtil.dummyRegelungsvorhabenEditorDTO();

        // Act
        RegelungsvorhabenEditorDTO actual = regelungsvorhabenService.getRegelungsvorhabenEditorDTO(
            new RegelungsVorhabenId(UUID.randomUUID())
        );

        // Assert
        assertThat(actual)
            .hasFieldOrPropertyWithValue("id", expected.getId())
            .hasFieldOrPropertyWithValue("bezeichnung", expected.getBezeichnung());

        assertThat(actual.getTechnischesFfRessort())
            .hasFieldOrPropertyWithValue("id", expected.getTechnischesFfRessort().getId())
            .hasFieldOrPropertyWithValue("bezeichnungNominativ",
                expected.getTechnischesFfRessort().getBezeichnungNominativ())
            .hasFieldOrPropertyWithValue("bezeichnungGenitiv",
                expected.getTechnischesFfRessort().getBezeichnungGenitiv());
    }

    @Test
    void whenRegelungsvorhabenIsNotValid_ADummyMustBeProvided2() {
        // Arrange
        RegelungsvorhabenEditorDTO expected = PropositionServiceUtil.dummyRegelungsvorhabenEditorDTO();

        // Act
        RegelungsvorhabenEditorDTO actual = regelungsvorhabenService.getRegelungsvorhabenEditorDTOForCompoundDocument(
            TestObjectsUtil.getRandomCompoundDocument());

        // Assert
        assertThat(actual)
            .hasFieldOrPropertyWithValue("id", expected.getId())
            .hasFieldOrPropertyWithValue("bezeichnung", expected.getBezeichnung());

        assertThat(actual.getTechnischesFfRessort())
            .hasFieldOrPropertyWithValue("id", expected.getTechnischesFfRessort().getId())
            .hasFieldOrPropertyWithValue("bezeichnungNominativ",
                expected.getTechnischesFfRessort().getBezeichnungNominativ())
            .hasFieldOrPropertyWithValue("bezeichnungGenitiv",
                expected.getTechnischesFfRessort().getBezeichnungGenitiv());
    }

    // - Es gibt noch kein Regelungsvorhaben in der Datenbank (muss von Plattform geholt werden)
    // - Dieses wird angelegt und ist veränderbar
    @Test
    void legeDasErsteRegelungsvorhabenAn() {
        RegelungsvorhabenEditorDTO randomRegelungsvorhabenEditorDTO = TestDTOUtil.getRandomRegelungsvorhabenEditorDTO();
        Proposition randomProposition = propositionMapper.mapD(randomRegelungsvorhabenEditorDTO);

        // Datenbank hat kein Regelungsvorhaben
        when(propositionRepository.findByUpdateablePropositionId(any(RegelungsVorhabenId.class))).thenReturn(null);
        // Plattform liefert das Regelungsvorhaben
        when(plategRequestService.getRegulatoryProposalsByIdListPlattform(any(), any())).thenReturn(List.of(randomRegelungsvorhabenEditorDTO));
        // Die Datenbank liefert kein 'unveränderbares' Regelungsvorhaben
        when(propositionRepository.findByNotUpdateablePropositionId(any(RegelungsVorhabenId.class))).thenReturn(Collections.emptyList());

        UUID actual = regelungsvorhabenService.sichereRegelungsvorhaben(randomProposition.getPropositionId(), false);

        assertThat(actual).isNotNull();
        assertThat(saveActions).hasSize(1);
        assertThat(saveActions.get(0)).hasFieldOrPropertyWithValue("referenzId", null);
    }

    // - Es gibt ein Regelungsvorhaben, dass verändert werden kann
    // - Jetzt wird ein Regelungsvorhaben angelegt, das unveränderbar ist.
    // - Das veränderbare Regelungsvorhaben bleibt bestehen
    @Test
    void legeUnveraenderbaresRegelungsvorhabenAn() {
        Proposition randomProposition = TestObjectsUtil.getRandomProposition();
        // Die Datenbank findet das veränderbare Regelungsvorhaben
        when(propositionRepository.findByUpdateablePropositionIds(anyList())).thenReturn(List.of(randomProposition));
        // Die Datenbank liefert kein 'unveränderbares' Regelungsvorhaben
        when(propositionRepository.findByNotUpdateablePropositionId(any(RegelungsVorhabenId.class))).thenReturn(Collections.emptyList());

        UUID actual = regelungsvorhabenService.sichereRegelungsvorhaben(randomProposition.getPropositionId(), true);

        assertThat(actual).isNotNull();
        assertThat(saveActions).hasSize(1);
        assertThat(saveActions.get(0)).hasNoNullFieldsOrProperties();
        assertThat(saveActions.get(0)).hasFieldOrPropertyWithValue("referenzId", actual);
    }

    // - Es gibt ein Regelungsvorhaben, dass verändert werden kann und eins, welches unveränderbar ist.
    // - Es wird ein weiteres Regelungsvorhaben angelegt, das unveränderbar ist.
    // - Das veränderbare Regelungsvorhaben bleibt bestehen
    @Test
    void legeWeiteresUnveraenderbaresRegelungsvorhabenAn() {
        Proposition randomProposition = TestObjectsUtil.getRandomProposition();
        randomProposition.setProponentTitle("Changed title");

        Proposition randomPropositionFixed = TestObjectsUtil.getRandomProposition();
        randomPropositionFixed.setReferenzId(UUID.randomUUID());

        // Die Datenbank findet das veränderbare Regelungsvorhaben
        when(propositionRepository.findByUpdateablePropositionIds(anyList())).thenReturn(List.of(randomProposition));

        // Die Datenbank liefert das 'unveränderbares' Regelungsvorhaben
        when(propositionRepository.findByNotUpdateablePropositionId(any(RegelungsVorhabenId.class))).thenReturn(List.of(randomPropositionFixed));

        UUID actual = regelungsvorhabenService.sichereRegelungsvorhaben(randomProposition.getPropositionId(), true);

        assertThat(actual).isNotNull();
        assertThat(saveActions).hasSize(1);
        assertThat(saveActions.get(0)).hasNoNullFieldsOrProperties();
        assertThat(saveActions.get(0).getReferenzId()).isNotSameAs(randomPropositionFixed.getReferenzId());
    }

    // - Es gibt ein Regelungsvorhaben, dass verändert werden kann und eins, welches unveränderbar ist.
    // - Jetzt wird ein weiteres Regelungsvorhaben angelegt, das unveränderbar ist. Das ist aber dasselbe, wie es schon gibt.
    // - Ein Duplikat soll verhindert werden.
    // - Das veränderbare Regelungsvorhaben bleibt bestehen.
    @Test
    void legeWeiteresAberGleichesUnveraenderbaresRegelungsvorhabenAn() {
        Proposition randomProposition = TestObjectsUtil.getRandomProposition();

        Proposition randomPropositionFixed = TestObjectsUtil.getRandomProposition();
        randomPropositionFixed.setReferenzId(UUID.randomUUID());

        // Die Datenbank findet das veränderbare Regelungsvorhaben
        when(propositionRepository.findByUpdateablePropositionIds(anyList())).thenReturn(List.of(randomProposition));

        // Die Datenbank liefert das 'unveränderbares' Regelungsvorhaben
        when(propositionRepository.findByNotUpdateablePropositionId(any(RegelungsVorhabenId.class))).thenReturn(List.of(randomPropositionFixed));

        UUID actual = regelungsvorhabenService.sichereRegelungsvorhaben(randomProposition.getPropositionId(), true);

        assertThat(actual).isNotNull();
        assertThat(saveActions).isEmpty();
        assertThat(actual).isSameAs(randomPropositionFixed.getReferenzId());
    }

    // fachlich unmöglich: Wir bekommen kein bestehendes Regelungsvorhaben
    @Test
    void verhaltenBeiNichtVorhandenemRegelungsvorhaben() {
        // Die 'Plattform' liefert auch nichts
        when(plategRequestService.getAllRegulatoryProposalsForUserHeIsAuthorOf()).thenReturn(Collections.emptyList());

        UUID actual = regelungsvorhabenService.sichereRegelungsvorhaben(new RegelungsVorhabenId(UUID.randomUUID()), true);
        assertThat(actual).isEqualTo(PropositionServiceUtil.dummyRegelungsvorhabenEditorDTO().getId());
    }

    @Test
    void testGetNoChangeableForGivenRegelungsvorhabenId() {
        RegelungsvorhabenEditorDTO randomRegelungsvorhabenEditorDTO = TestDTOUtil.getRandomRegelungsvorhabenEditorDTO();
        Proposition proposition = propositionMapper.mapD(randomRegelungsvorhabenEditorDTO);
        when(propositionRepository.findByNotUpdateablePropositionIdReferableId(any(RegelungsVorhabenId.class), any(UUID.class))).thenReturn(proposition);

        RegelungsvorhabenEditorDTO actual = regelungsvorhabenService.getNoChangeableForGivenRegelungsvorhabenId(
            new RegelungsVorhabenId(UUID.randomUUID()), UUID.randomUUID());

        assertThat(actual).hasNoNullFieldsOrPropertiesExcept("kurzbeschreibung", "farbe", "allFfRessorts");
    }

    @Test
    void testWeiterleitung1() {
        when(plategRequestService.getRegulatoryProposalUserCanAccess(any())).thenReturn(TestDTOUtil.getRandomRegelungsvorhabenEditorDTO());
        RegelungsvorhabenEditorDTO actual = regelungsvorhabenService.getRegulatoryProposalUserCanAccess(UUID.randomUUID());
        assertThat(actual).isNotNull();
    }

    @Test
    void testWeiterleitung2() {
        when(plategRequestService.isUserParticipantForCompoundDocument(any())).thenReturn(true);
        boolean actual = regelungsvorhabenService.isUserParticipantForCompoundDocument(new CompoundDocumentId(UUID.randomUUID()));
        assertThat(actual).isTrue();
    }

    @Test
    void testWeiterleitung3() {
        when(plategRequestService.getAllRegulatoryProposalsForUserHeIsAuthorOf()).thenReturn(Collections.emptyList());
        List<RegelungsvorhabenEditorDTO> actual = regelungsvorhabenService.getAllRegulatoryProposalsForUserHeIsAuthorOf();
        assertThat(actual).hasSize(0);
    }

}
