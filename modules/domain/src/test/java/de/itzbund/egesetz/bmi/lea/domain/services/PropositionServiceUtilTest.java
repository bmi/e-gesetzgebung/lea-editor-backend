// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.HomepageCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.HomepageRegulatoryProposalDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.HomepageSingleDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.PermissionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.RegelungsvorhabengTableDTOs;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Pageable;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

class PropositionServiceUtilTest {

	private RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO;
	private HomepageCompoundDocumentDTO homepageCompoundDocumentDTO;
	private List<HomepageSingleDocumentDTO> homepageSingleDocumentDTOList;
	private PermissionDTO permissionDTO;

	@BeforeEach
	void init() {
		regelungsvorhabenEditorDTO = TestDTOUtil.getRandomRegelungsvorhabenEditorDTO();
		homepageCompoundDocumentDTO = TestDTOUtil.getRandomHomepageCompoundDocumentDTO();
		homepageSingleDocumentDTOList = List.of(TestDTOUtil.getRandomHomepageSingleDocumentDTO());
		permissionDTO = TestDTOUtil.getRandomPermissionDTO();
	}

	@Test
	void createHomepageRegulatoryProposal_ShouldCreateCorrectDTO() {
		// Arrange

		// Act
		HomepageRegulatoryProposalDTO result = PropositionServiceUtil.createHomepageRegulatoryProposal(
			regelungsvorhabenEditorDTO, List.of(homepageCompoundDocumentDTO));

		// Assert
		assertNotNull(result);
	}

	@Test
	void createHomepageCompoundDocument_ShouldCreateCorrectDTO() {
		// Arrange
		CompoundDocument compoundDocument = TestObjectsUtil.getRandomCompoundDocument();

		// Act
		HomepageCompoundDocumentDTO result = PropositionServiceUtil.createHomepageCompoundDocument(
			compoundDocument, regelungsvorhabenEditorDTO, homepageSingleDocumentDTOList, permissionDTO, Collections.emptySet());

		// Assert
		assertNotNull(result);
	}

	@Test
	void getDocumentDTO_ShouldCreateCorrectDTO() {
		// Arrange
		DocumentState state = DocumentState.DRAFT;
		DocumentDomain documentEntity = TestObjectsUtil.getDocumentEntity();
		UUID regulatoryProposalId = UUID.randomUUID();

		// Act
		HomepageSingleDocumentDTO result = PropositionServiceUtil.getDocumentDTO(state, documentEntity, regulatoryProposalId, permissionDTO, permissionDTO);

		// Assert
		assertNotNull(result);
	}

	@Test
	void getRegelungsvorhabenTable_ShouldCreateCorrectDTO() {
		// Arrange
		List<HomepageRegulatoryProposalDTO> meineDokumente = Collections.singletonList(mock(HomepageRegulatoryProposalDTO.class));
		Pageable newPageable = mock(Pageable.class);
		int anzahlAllerElemente = 1;

		// Act
		RegelungsvorhabengTableDTOs result = PropositionServiceUtil.getRegelungsvorhabenTable(meineDokumente, newPageable, anzahlAllerElemente);

		// Assert
		assertNotNull(result);
	}

}
