// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.EditorRollenUndRechte;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.KommentarAntworten;
import de.itzbund.egesetz.bmi.lea.domain.mappers.CommentMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.ReplyId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CommentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.ReplyPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import de.itzbund.egesetz.bmi.lea.domain.validator.DocumentPermissionValidator;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {CommentService.class, KommentarAntworten.class})
abstract class CommentServiceTest {

	protected final static UUID uuid = UUID.randomUUID();

	// ----- Services ---------
	@Autowired
	protected CommentService commentService;
	@MockBean
	protected DocumentService documentService;
	@MockBean
	protected PropositionService propositionService;
	// ----- Repositories -----
	@MockBean
	protected CommentPersistencePort commentPersistencePort;
	// ----- Mapper -----------
	@MockBean
	protected CommentMapper commentMapper;
	// ----- Other ------------
	@MockBean
	protected DocumentPermissionValidator documentPermissionValidator;
	@MockBean
	protected DokumentPersistencePort dokumentPersistencePort;
	@MockBean
	protected ReplyPersistencePort replyPersistencePort;
	@MockBean
	protected LeageSession session;
	@MockBean
	protected Document document;
	@MockBean
	private EditorRollenUndRechte editorRollenUndRechte;

	// Some variables

	ReplyId replyId = new ReplyId(UUID.randomUUID());


	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
		setupDocumentServiceMockups(documentService);
		setupDocumentRepositoryMockups(dokumentPersistencePort);
		setupCommentPersistencePortMockups(commentPersistencePort);
		setupReplyPersistencePortMockups(replyPersistencePort);
		setupSession();
		setupPermissionValidation();
	}


	protected void setupPermissionValidation() {
		when(documentPermissionValidator.isUserOwnerOfDocument(any(), any())).thenReturn(true);
		when(documentPermissionValidator.isUserOwnerOfReplyEntity(any())).thenReturn(true);
	}


	protected void setupSession() {
		when(session.getUser()).thenReturn(TestObjectsUtil.getUserEntity());
	}


	protected void setupDocumentServiceMockups(DocumentService documentService) {
		// checks
		when(documentService.isDocumentExisting(any(Document.class))).thenReturn(true);
		when(documentService.getDocumentByIdNEW(any(DocumentId.class))).thenReturn(TestObjectsUtil.getRandomDocument());
	}


	protected void setupDocumentRepositoryMockups(DokumentPersistencePort documentRepository) {
		DocumentDomain document = TestObjectsUtil.getDocumentEntity();
		document.setDocumentId(new DocumentId(uuid));
		when(documentRepository.findByDocumentId(document.getDocumentId())).thenReturn(Optional.of(document));
	}


	protected void setupCommentPersistencePortMockups(CommentPersistencePort commentPersistencePort) {
		when(commentPersistencePort.findByCommentId(any(CommentId.class))).thenReturn(
			TestObjectsUtil.getRandomComment());
	}


	protected void setupReplyPersistencePortMockups(ReplyPersistencePort replyPersistencePort) {
		// check
		when(replyPersistencePort.existsReplyByReplyId(any())).thenReturn(true);

		when(replyPersistencePort.save(any())).thenAnswer(invocation ->
			invocation.getArgument(0)
		);

		when(replyPersistencePort.findByReplyIdAndDeletedFalse(replyId)).thenReturn(
			Optional.of(TestObjectsUtil.getRandomReplyEntity()));
	}

}
