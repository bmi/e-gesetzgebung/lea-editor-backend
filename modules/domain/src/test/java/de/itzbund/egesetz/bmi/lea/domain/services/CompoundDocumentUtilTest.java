// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CompoundDocumentUtilTest {

    @Test
    void testUserIsCreatorOfCompoundDocument() {
        CompoundDocument compoundDocument = TestObjectsUtil.getRandomCompoundDocument();
        User user = compoundDocument.getCompoundDocumentEntity()
            .getCreatedBy();

        boolean actual = CompoundDocumentUtil.isUserCreaterOfCompoundDocument(compoundDocument,
            user);

        assertThat(actual).isTrue();
    }


    @Test
    void testUserIsCreatorOfCompoundDocument2() {
        CompoundDocumentDomain compoundDocument = TestObjectsUtil.getRandomCompoundDocumentEntity();
        User user = compoundDocument.getCreatedBy();

        boolean actual = CompoundDocumentUtil.isUserCreaterOfCompoundDocument(compoundDocument,
            user);

        assertThat(actual).isTrue();
    }
}
