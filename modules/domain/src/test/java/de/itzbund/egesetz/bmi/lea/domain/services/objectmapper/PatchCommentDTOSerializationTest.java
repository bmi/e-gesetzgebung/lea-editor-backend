// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.objectmapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CommentPositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.PatchCommentDTO;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PatchCommentDTOSerializationTest {

    private static ObjectMapper objectMapper;


    @BeforeAll
    static void setUp() {
        objectMapper = new ObjectMapper();
    }


    @Test
    @SneakyThrows
    void test_Deserialize_OnlyContentUpdate() {
        String updateContent = Utils.getRandomString();
        assertEquals(
            PatchCommentDTO.builder().content(updateContent).build(),
            objectMapper.readValue(
                "{\"content\":\"" + updateContent + "\"}",
                PatchCommentDTO.class
            )
        );
    }


    @Test
    @SneakyThrows
    void test_Deserialize_OnlyAnchorAndFocusUpdate() {
        long updateAnchorOffset = Utils.getRandomLong();
        List<Long> updateAnchorPath = Utils.getRandomListOfLong(5);
        CommentPositionDTO updateAnchor = CommentPositionDTO.builder()
            .offset(updateAnchorOffset)
            .path(updateAnchorPath)
            .build();

        long updateFocusOffset = Utils.getRandomLong();
        List<Long> updateFocusPath = Utils.getRandomListOfLong(5);
        CommentPositionDTO updateFocus = CommentPositionDTO.builder()
            .offset(updateFocusOffset)
            .path(updateFocusPath)
            .build();

        assertEquals(
            PatchCommentDTO.builder()
                .focus(updateFocus)
                .anchor(updateAnchor)
                .build(),
            objectMapper.readValue(
                "{\"anchor\":{\"offset\":" + updateAnchorOffset +
                    ",\"path\":" + updateAnchorPath +
                    "},\"focus\":{\"offset\":" + updateFocusOffset +
                    ",\"path\":" + updateFocusPath + "}}",
                PatchCommentDTO.class
            )
        );
    }

}
