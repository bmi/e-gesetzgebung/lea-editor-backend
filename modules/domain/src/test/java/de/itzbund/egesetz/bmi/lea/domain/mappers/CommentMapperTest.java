// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.mappers;

import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CommentPositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CommentResponseDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.ReplyDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import de.itzbund.egesetz.bmi.lea.domain.model.Position;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class CommentMapperTest {

	private CommentMapper commentMapper;
	private ReplyMapper replyMapper;


	@BeforeEach
	void init() {
		commentMapper = new CommentMapperImpl(
			new BaseMapperImpl(), new ReplyMapperImpl(new BaseMapperImpl())
		);

		replyMapper = new ReplyMapperImpl(new BaseMapperImpl());
	}


	@Test
	void testMapComment() {
		Comment comment = TestObjectsUtil.getRandomComment();
		List<Reply> replies = comment.getReplies();

		CommentResponseDTO actual = commentMapper.map(comment);

		// Test elements (without list) are mapped correctly
		assertThat(actual).hasNoNullFieldsOrProperties();

		// Test list ist correctly mapped
		assertThat(actual.getReplies()).hasSize(replies.size());

		ReplyDTO replyDTO = actual.getReplies().get(0);
		ReplyDTO expected = replyMapper.map(replies.get(0));

		assertThat(expected).hasNoNullFieldsOrProperties();
		assertThat(replyDTO).isEqualTo(expected);
	}


	@Test
	void testMapCommentPositionDTO() {
		CommentPositionDTO commentPositionDTO = TestDTOUtil.getRandomCommentPositionDTO();
		Position actual = commentMapper.map(commentPositionDTO);
		assertThat(actual).hasNoNullFieldsOrProperties();
		assertThat(actual.getPath()).hasSize(commentPositionDTO.getPath().size());
	}


	@Test
	void testMapPosition() {
		Position position = TestObjectsUtil.getRandomPosition();
		CommentPositionDTO actual = commentMapper.map(position);
		assertThat(actual.getPath()).hasSize(position.getPath().size());
	}

}
