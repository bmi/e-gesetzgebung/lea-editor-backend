// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserRightsDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.Freigabetyp;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestObjectsUtil;
import de.itzbund.egesetz.bmi.lea.domain.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

class CompoundDocumentService3IntegrationTest extends AbstractCompoundDocumentServiceTest {

    @BeforeEach
    void init() {
        super.init();
        User user = TestObjectsUtil.getUserEntity();
        when(session.getUser()).thenReturn(user);
        when(editorRollenUndRechte.isAllowed(eq(user.getGid()
            .toString()), eq("LESEN"), any(), any())).thenReturn(true);
    }

    // ---------- Leserechte -----------------

    @Test
    void whenInValidCompoundDocumentId_thenNotFoundError() {
        when(compoundDocumentRepository.findByCompoundDocumentId(any(CompoundDocumentId.class)))
            .thenReturn(Optional.empty());

        ServiceState actual = compoundDocumentService.changeAccessRightsForUsersOnCompoundDocuments(
            new CompoundDocumentId(UUID.randomUUID()), TestDTOUtil.getRandomUserRightsDTOList(3));

        assertThat(actual)
            .isEqualTo(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
    }


    @Test
    void whenNewRightsEmpty_thenOnlyCreatorShouldOwnRights() {
        CompoundDocumentDomain compoundDocumentEntity1 = TestObjectsUtil.getCompoundDocumentEntity();
        when(compoundDocumentRepository.findByCompoundDocumentId(any(CompoundDocumentId.class)))
            .thenReturn(Optional.of(compoundDocumentEntity1));

        ServiceState actual = compoundDocumentService.changeAccessRightsForUsersOnCompoundDocuments(
            new CompoundDocumentId(UUID.randomUUID()), Collections.emptyList());

        assertThat(actual)
            .isEqualTo(ServiceState.REMOVED_ALL_PERMISSIONS);
    }


    @Test
    void whenNewRightsContainsDuplicates_thenCreatinShouldBePossible() {
        CompoundDocumentDomain compoundDocumentEntity1 = TestObjectsUtil.getCompoundDocumentEntity();
        when(compoundDocumentRepository.findByCompoundDocumentId(any(CompoundDocumentId.class)))
            .thenReturn(Optional.of(compoundDocumentEntity1));

        List<String> benutzerList = new ArrayList<>();
        benutzerList.add("123");

        // Note: Mocks the EditorRollenUndRechte-TEST-Class
        when(editorRollenUndRechte.getAlleBenutzerIdsByRessourceIdAndRolle(anyString(), anyString())).thenReturn(
            benutzerList);

        List<UserRightsDTO> userRightsDTOList = new ArrayList<>();
        UserRightsDTO randomUserRightsDTO = TestDTOUtil.getRandomUserRightsDTO();
        userRightsDTOList.add(randomUserRightsDTO);
        userRightsDTOList.add(randomUserRightsDTO);
        userRightsDTOList.add(randomUserRightsDTO);

        ServiceState actual = compoundDocumentService.changeAccessRightsForUsersOnCompoundDocuments(
            new CompoundDocumentId(UUID.randomUUID()), userRightsDTOList);

        assertThat(actual)
            .isEqualTo(ServiceState.OK);
    }


    @Test
    void whenNewRightsCOntainsParts_thenCreationShouldOnlyHaveTheseRights() {
        CompoundDocumentDomain compoundDocumentEntity1 = TestObjectsUtil.getCompoundDocumentEntity();
        when(compoundDocumentRepository.findByCompoundDocumentId(any(CompoundDocumentId.class)))
            .thenReturn(Optional.of(compoundDocumentEntity1));

        List<String> benutzerList = new ArrayList<>();
        benutzerList.add("123");

        // Note: Mocks the EditorRollenUndRechte-TEST-Class
        when(editorRollenUndRechte.getAlleBenutzerIdsByRessourceIdAndRolle(anyString(), anyString())).thenReturn(
            benutzerList);

        ServiceState actual = compoundDocumentService.changeAccessRightsForUsersOnCompoundDocuments(
            new CompoundDocumentId(UUID.randomUUID()), TestDTOUtil.getRandomUserRightsDTOList(3));

        assertThat(actual)
            .isEqualTo(ServiceState.OK);
    }

    // ---------- Schreibrechte -----------------

    @Test
    void onlyOneWritePermissionShouldBeSetable() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        when(compoundDocumentRepository.findByCompoundDocumentId(any(CompoundDocumentId.class)))
            .thenReturn(Optional.of(TestObjectsUtil.getRandomCompoundDocumentEntity()));

        // Act
        CompoundDocumentSummaryDTO actual = compoundDocumentService.changeWritePermissionsOnCompoundDocuments(
            new CompoundDocumentId(UUID.randomUUID()), TestDTOUtil.getRandomUserRightsDTO(), status);

        // Assert
        assertThat(status).asList().hasSize(1).contains(ServiceState.INCONSISTENT_DATA);
        assertThat(actual).isNull();
    }

    @Test
    void missingDocumentAtWrtitePermissionShouldBeDetected() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        // - kein Dokumentenmappe
        when(compoundDocumentRepository.findByCompoundDocumentId(any(CompoundDocumentId.class)))
            .thenReturn(Optional.empty());

        // Act
        CompoundDocumentSummaryDTO actual = compoundDocumentService.changeWritePermissionsOnCompoundDocuments(
            new CompoundDocumentId(UUID.randomUUID()), TestDTOUtil.getRandomUserRightsDTO(), status);

        // Assert
        assertThat(status).asList().hasSize(1).contains(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
        assertThat(actual).isNull();
    }

    @Test
    void whenNewWritePermissinIsGiven_thenWeShouldHaveTheseRights() {

        // Arrange
        // - Die Dokumentenmappe
        CompoundDocumentDomain compoundDocumentEntity1 = TestObjectsUtil.getCompoundDocumentEntity();
        when(compoundDocumentRepository.findByCompoundDocumentId(any(CompoundDocumentId.class)))
            .thenReturn(Optional.of(compoundDocumentEntity1));
        when(documentRepository.findByDocumentId(any())).thenReturn(Optional.of(TestObjectsUtil.getRandomDocumentEntity()));
        when(rbacPermissionService.getBenutzerGidsMitDokumentenmappenZugriffAlsLeser(any())).thenReturn(List.of("something"));

        // - Die Dokumente in der Mappe
        DocumentDomain documentEntity = TestObjectsUtil.getDocumentEntity();
        when(documentRepository.findByCompoundDocumentId(any(CompoundDocumentId.class))).thenReturn(Optional.of(List.of(documentEntity)));

        List<String> benutzerList = new ArrayList<>();
        benutzerList.add("123");

        List<ServiceState> status = new ArrayList<>();

        // - Note: Mocks the EditorRollenUndRechte-TEST-Class
        when(editorRollenUndRechte.getAlleBenutzerIdsByRessourceIdAndRolle(anyString(), anyString())).thenReturn(
            benutzerList);

        when(userService.getUser()).thenReturn(TestUtil.getNonexistingUser());

        UserRightsDTO randomUserRightsDTO = TestDTOUtil.getRandomUserRightsDTO();
        randomUserRightsDTO.setFreigabetyp(Freigabetyp.SCHREIBRECHTE);

        // Act
        CompoundDocumentSummaryDTO actual = compoundDocumentService.changeWritePermissionsOnCompoundDocuments(
            new CompoundDocumentId(UUID.randomUUID()), randomUserRightsDTO, status);

        // Assert
        assertThat(actual.getId()).isNotEqualTo(compoundDocumentEntity1.getCompoundDocumentId().getId());
    }

}
