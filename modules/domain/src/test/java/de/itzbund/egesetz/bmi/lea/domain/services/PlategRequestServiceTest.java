// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services;

import de.itzbund.egesetz.bmi.lea.domain.dtos.abstimmung.CompoundDocumentEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.mappers.RegelungsvorhabenMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.RegelungsvorhabenMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.domain.util.TestDTOUtil;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static de.itzbund.egesetz.bmi.lea.core.Constants.API_KEY_NAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith({SpringExtension.class})
@SuppressWarnings("unused")
class PlategRequestServiceTest {

    private final User user = User.builder()
        .name("Tester")
        .gid(new UserId("testGid"))
        .email("test@test.de")
        .build();
    private final RegelungsvorhabenMapper regelungsvorhabenMapper = new RegelungsvorhabenMapperImpl();
    @Value("${plateg.x-api-key}")
    private String apiKey;
    @InjectMocks
    private PlategRequestService plategRequestService;
    @Mock
    private LeageSession session;
    @Mock
    private RestTemplate restTemplate;
    @Mock
    private RBACPermissionService rbacPermissionService;
    @Mock
    private ApplicationContext applicationContext;
    @Mock
    private RegelungsvorhabenService regelungsvorhabenService;


    @BeforeEach
    void init() throws Exception {
        FieldUtils.writeField(plategRequestService, "session", session, true);
        FieldUtils.writeField(plategRequestService, "restTemplate", restTemplate, true);
        FieldUtils.writeField(plategRequestService, "apiKey", apiKey, true);
        FieldUtils.writeField(plategRequestService, "regelungsvorhabenMapper", regelungsvorhabenMapper, true);
        when(session.getUser()).thenReturn(user);

        when(applicationContext.getBean((Class) any())).thenAnswer(
            i -> {
                Class argument = i.getArgument(0);

                if (argument.getCanonicalName().contains("RegelungsvorhabenService")) {
                    return regelungsvorhabenService;
                }

                return rbacPermissionService;
            }
        );
    }


    @Test
    void getAllPropositionDescriptionsByUser_successful_test() {
        List<RegelungsvorhabenEditorDTO> rvDtos = createRVList();
        List<UUID> rvIds = rvDtos.stream().map(rv -> rv.getId()).collect(Collectors.toList());

        when(rbacPermissionService.getRegelungsvorhabenIdsMitNutzerZugriff(any())).thenReturn(rvIds);
        when(regelungsvorhabenService.getRegelungsvorhabenEditorDTO(any(RegelungsVorhabenId.class))).thenReturn(rvDtos.get(0));

        String url = plategRequestService.createUrl(PlategRequestService.EndpointURL.GET_LIST_OF_REGULATORY_PROPOSALS
            , Optional.empty());
        HttpEntity<?> requestEntity = new HttpEntity<>(null, createHeader());
        ResponseEntity<List<RegelungsvorhabenEditorDTO>> response = new ResponseEntity<>(
            rvDtos, HttpStatus.OK);
        setWhenForGetAllPropositionDescriptionsByUser(url, requestEntity, response);

        List<RegelungsvorhabenEditorDTO> actual = plategRequestService.getAllRegulatoryProposalsForUserHeIsAuthorOf();

        assertThat(actual).hasSize(rvDtos.size());

        assertThat(actual.get(0)).hasNoNullFieldsOrPropertiesExcept(
            "bezeichnung",
            "technischesFfRessort",
            "allFfRessorts",
            "initiant",
            "farbe");
    }


    @Test
    void getAllPropositionDescriptionsByUser_unsuccessful_emptylist_test() {

        String url = plategRequestService.createUrl(PlategRequestService.EndpointURL.GET_LIST_OF_REGULATORY_PROPOSALS
            , Optional.empty());
        HttpEntity<?> requestEntity = new HttpEntity<>(null, createHeader());
        ResponseEntity<List<RegelungsvorhabenEditorDTO>> response = new ResponseEntity<>(
            new ArrayList<>(), HttpStatus.NOT_FOUND);
        setWhenForGetAllPropositionDescriptionsByUser(url, requestEntity, response);
        List<RegelungsvorhabenEditorDTO> responseDtos =
            plategRequestService.getAllRegulatoryProposalsForUserHeIsAuthorOf();

        assertTrue(responseDtos.isEmpty());
    }


    @Test
    void getAllPropositionDescriptionsByUser_unsuccessful_otherfailure_test() {
        try {
            String url = plategRequestService.createUrl(
                PlategRequestService.EndpointURL.GET_LIST_OF_REGULATORY_PROPOSALS
                , Optional.empty());
            HttpEntity<?> requestEntity = new HttpEntity<>(null, createHeader());
            ResponseEntity<List<RegelungsvorhabenEditorDTO>> response = new ResponseEntity<>(
                new ArrayList<>(), HttpStatus.BAD_REQUEST);
            setWhenForGetAllPropositionDescriptionsByUser(url, requestEntity, response);
            plategRequestService.getAllRegulatoryProposalsForUserHeIsAuthorOf();
        } catch (ResponseStatusException ex) {
            assertEquals("error occurred while sending request to Plattform", ex.getReason());
        }
    }

    // ============================================================================


    @Test
    void getRegulatoryProposalUserCanAccess_successful_test() {
        RegelungsvorhabenEditorDTO rv = TestDTOUtil.getRandomRegelungsvorhabenEditorDTO();
        UUID regulatoryProposalId = rv.getId();

        when(rbacPermissionService.getRegelungsvorhabenIdsMitNutzerZugriff(any())).thenReturn(List.of(regulatoryProposalId));

        when(regelungsvorhabenService.getRegelungsvorhabenEditorDTO(any(RegelungsVorhabenId.class))).thenReturn(rv);

        RegelungsvorhabenEditorDTO actual = plategRequestService.getRegulatoryProposalUserCanAccess(
            regulatoryProposalId);

        assertThat(actual).isNotNull()
            .hasNoNullFieldsOrPropertiesExcept("farbe");
    }

    // ============================================================================


    @Test
    void getAbstimmungenEinerDokumentenmappe_successful_test() {
        CompoundDocumentId testDocId = new CompoundDocumentId(UUID.randomUUID());
        CompoundDocumentEditorDTO compoundDocumentEditorDTO = TestDTOUtil.getRandomCompoundDocumentEditorDTO();

        String url = plategRequestService.createUrl(
            PlategRequestService.EndpointURL.GET_ABSTIMMUNG_FUER_DOKUMENTENMAPPE
            , Optional.of(testDocId.getId().toString()));
        HttpEntity<?> requestEntity = new HttpEntity<>(null, createHeader());
        ResponseEntity<CompoundDocumentEditorDTO> response = ResponseEntity.ok().body(compoundDocumentEditorDTO);
        setCompoundDocumentEditorResponse(url, requestEntity, response);

        CompoundDocumentEditorDTO actual = plategRequestService.getAbstimmungenEinerDokumentenmappe(
            testDocId);

        assertThat(actual).isNotNull()
            .hasNoNullFieldsOrProperties();
    }

    // ============================================================================


    @Test
    void getAbstimmungUndDokumentenmappe_successful_test() {

        List<CompoundDocumentEditorDTO> compoundDocumentEditorDTOs = Collections.singletonList(
            TestDTOUtil.getRandomCompoundDocumentEditorDTO());

        String url = plategRequestService.createUrl(
            PlategRequestService.EndpointURL.GET_ABSTIMMUNG_UND_DOKUMENTENMAPPE
            , Optional.empty());
        HttpEntity<?> requestEntity = new HttpEntity<>(null, createHeader());
        ResponseEntity<List<CompoundDocumentEditorDTO>> response = new ResponseEntity<>(
            compoundDocumentEditorDTOs, HttpStatus.OK);
        setCompoundDocumentEditorResponse2(url, requestEntity, response);

        List<CompoundDocumentEditorDTO> actual = plategRequestService.getAbstimmungUndDokumentenmappe();

        assertThat(actual).hasSize(compoundDocumentEditorDTOs.size());
        assertThat(actual.get(0)).hasNoNullFieldsOrProperties();
    }

    // ============================================================================


    private void setWhenForgetUserIsPartOfVoting(String url, HttpEntity<?> requestEntity,
        ResponseEntity<Boolean> response) {
        when(restTemplate.exchange(url, HttpMethod.GET, requestEntity, Boolean.class))
            .thenReturn(response);
    }


    private void setWhenForGetAllPropositionDescriptionsByUser(String url, HttpEntity<?> requestEntity,
        ResponseEntity<List<RegelungsvorhabenEditorDTO>> response) {
        when(restTemplate.exchange(
            Mockito.eq(url),
            Mockito.eq(HttpMethod.GET),
            Mockito.eq(requestEntity),
            Mockito.<ParameterizedTypeReference<List<RegelungsvorhabenEditorDTO>>>any()))
            .thenReturn(response);
    }


    private void setRegelungsvorhabenResponse(String url, HttpEntity<?> requestEntity,
        ResponseEntity<RegelungsvorhabenEditorDTO> response) {
        when(restTemplate.exchange(url, HttpMethod.GET, requestEntity, RegelungsvorhabenEditorDTO.class))
            .thenReturn(response);
    }


    private void setCompoundDocumentEditorResponse(String url, HttpEntity<?> requestEntity,
        ResponseEntity<CompoundDocumentEditorDTO> response) {
        when(restTemplate.exchange(url, HttpMethod.GET, requestEntity,
            CompoundDocumentEditorDTO.class))
            .thenReturn(response);
    }


    private void setCompoundDocumentEditorResponse2(String url, HttpEntity<?> requestEntity,
        ResponseEntity<List<CompoundDocumentEditorDTO>> response) {
        when(restTemplate.exchange(Mockito.eq(url), Mockito.eq(HttpMethod.GET), Mockito.eq(requestEntity),
            Mockito.<ParameterizedTypeReference<List<CompoundDocumentEditorDTO>>>any()))
            .thenReturn(response);
    }


    private HttpHeaders createHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.set(API_KEY_NAME, apiKey);
        return headers;
    }


    private List<RegelungsvorhabenEditorDTO> createRVList() {
        return Arrays.asList(TestDTOUtil.getRandomRegelungsvorhabenEditorDTO(),
            TestDTOUtil.getRandomRegelungsvorhabenEditorDTO());
    }

}
