// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.services.filters;

import org.json.simple.JSONObject;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class FilterChainTest {

    @Test
    @SuppressWarnings("unchecked")
    void testFilterChainExecution() {
        // Arrange
        // Mocking filters
        XmlFilter filterA = mock(XmlFilter.class);
        XmlFilter filterB = mock(XmlFilter.class);
        XmlFilter filterC = mock(XmlFilter.class);

        doAnswer((i) -> {
            JSONObject jsonObject = i.getArgument(0);
            FilterChain filterChain = i.getArgument(1);
            filterChain.doFilter(jsonObject);
            return null;
        }).when(filterA).doFilter(any(), any());

        doAnswer((i) -> {
            JSONObject jsonObject = i.getArgument(0);
            FilterChain filterChain = i.getArgument(1);
            filterChain.doFilter(jsonObject);
            return null;
        }).when(filterB).doFilter(any(), any());

        doAnswer((i) -> {
            JSONObject jsonObject = i.getArgument(0);
            FilterChain filterChain = i.getArgument(1);
            filterChain.doFilter(jsonObject);
            return null;
        }).when(filterC).doFilter(any(), any());

        // Creating filter chain
        FilterChain chain = new FilterChain();
        chain.addFilter(filterA);
        chain.addFilter(filterB);
        chain.addFilter(filterC);

        // Testing the filter chain
        JSONObject jsonRequest = new JSONObject();
        jsonRequest.put("key", "value");

        // Act
        chain.doFilter(jsonRequest);

        // Assert
        // Verify that each filter's doFilter method was called once
        verify(filterA).doFilter(eq(jsonRequest), any(FilterChain.class));
        verify(filterB).doFilter(eq(jsonRequest), any(FilterChain.class));
        verify(filterC).doFilter(eq(jsonRequest), any(FilterChain.class));

        // Verify that filterA was called first, followed by filterB, and then filterC
        InOrder inOrder = inOrder(filterA, filterB, filterC);
        inOrder.verify(filterA).doFilter(eq(jsonRequest), any(FilterChain.class));
        inOrder.verify(filterB).doFilter(eq(jsonRequest), any(FilterChain.class));
        inOrder.verify(filterC).doFilter(eq(jsonRequest), any(FilterChain.class));
    }

}


