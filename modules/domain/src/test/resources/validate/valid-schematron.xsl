<?xml version="1.0" encoding="UTF-8" standalone="no"?>

<!--
Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit

SPDX-License-Identifier: MPL-2.0
-->

<xsl:stylesheet
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        version="2.0">

    <xsl:output indent="yes" method="xml" omit-xml-declaration="no" standalone="yes"/>

    <xsl:template match="*">
        <xsl:text>invalid</xsl:text>
    </xsl:template>

</xsl:stylesheet>
