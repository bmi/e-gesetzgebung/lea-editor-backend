<#--
Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit

SPDX-License-Identifier: MPL-2.0
-->

<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8"/>
  <title>Synopse</title>
</head>

<body>
  <h1>Dokumentvergleich</h1>
  <table>
    <thead>
      <tr>
        <th>BASE</th>
        <th>VERSION</th>
      </tr>
    </thead>
    <tbody>
      <#list pairs as pair>
        <tr>
          <td>${pair.getLeft()}</td>
          <td>${pair.getRight()}</td>
        </tr>
      </#list>
    </tbody>
  </table>
</body>

</html>
