#!/bin/bash

# Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
#
# SPDX-License-Identifier: MPL-2.0

scriptname="./tests/test-arbeiten-mit-dokumenten.sh"

# - neue DM-Version anlegen (gleiches RV)     POST /compounddocuments/{id}/version
# - Umbenennen der DM V2                      PATCH /compounddocuments/{id}
# - alle DM des RV abrufen                    GET /compounddocuments/regelungsvorhaben/{id}/list
# - umbenennen des Regelungstextes V2         PATCH /documents/{id}
# - abrufen des Regelungstextes V2            GET /documents/{id}



logInfo "***************************************************************************************************"
logInfo "TEST SUITE: Arbeiten mit Dokumenten"
logInfo "***************************************************************************************************"
TIMEFORMAT="Die Tests benötigten %R Sekunden."
time {

###################################################################################################
#
# Szenario: DM-NEW-VERSION
#   Als Legist in der E-Gesetzgebung möchte ich eine neue Version einer Dokumentenmappe anlegen,
#   um alternative Formulierungen zu schreiben.
#
# Schritte:
#   1.  Authentifizieren und JWToken erhalten
#   2.  Anlegen einer neuen Version der Dokumentenmappe mit den Parametern:
#       - id                    : ${neueDokumentenmappenId}
#       - copyId                : ?copyId=${neueRegelungstextId}
#       - title                 : Regelungstext-<Szenario>
#   3.  Prüfen des Ergebnisses
#
###################################################################################################
szenario="DM-NEW-VERSION"

# -----------------------------------------------
# Schritt 1
# -----------------------------------------------
authenticate

# -----------------------------------------------
# Schritt 2
# -----------------------------------------------
dokumentenmappenVersionErstellen "${neueDokumentenmappenId}" "?copyId=${neueRegelungstextId}" "Neue DM - ${szenario}"

# -----------------------------------------------
# Schritt 3
# -----------------------------------------------
if [[ "${neueDokumentenmappenId}" == "${dokumentenMappenId}" ]]
then
    logFailure "Die IDs beider Mappen sind gleich."
else
    logSuccess "Eine neue Version mit ID ${dokumentenMappenId} wurde erstellt."
fi

if [[ "${neueDokumentenmappenVersion}" == "${version}" ]]
then
    logFailure "Die Versionen beider Mappen sind gleich."
else
    compareVersions "${neueDokumentenmappenVersion}" "${version}"
    if [[ "${biggerVersion}" -eq 2 ]]
    then
        logSuccess "Die neue Version der Dokumentenmappe hat den richtigen Wert: ${neueDokumentenmappenVersion} ---> ${version}"
    else
        logFailure "Die neue Version der Dokumentenmappe hat den falschen Wert: ${neueDokumentenmappenVersion} -/-> ${version}"
    fi
fi

if [[ "${status}" == "DRAFT" ]]
then
    logSuccess "Die neue Version der Dokumentenmappe hat den richtigen Status."
else
    logFailure "Die neue Version der Dokumentenmappe hat den falschen Status: ${status}"
fi

anzahlKopierterDokumente=$(echo ${body} | jq '.documents | length')
typenKopierterDokumente=$(echo ${body} | jq -c '.documents | map(.type)')

if (( anzahlKopierterDokumente == 1 ))
then
    logSuccess "Es wurde wie erwartet genau ein Dokument kopiert."
else
    logFailure "Die Anzahl kopierter Dokumente ist falsch: ${anzahlKopierterDokumente}"
fi

if [[ "${typenKopierterDokumente}" == "[\"REGELUNGSTEXT_STAMMGESETZ\"]" ]]
then
    logSuccess "Der Dokumenttyp des kopierten Dokuments ist korrekt."
else
    logFailure "Der Dokumenttyp des kopierten Dokuments ist falsch: ${typenKopierterDokumente}"
fi

###################################################################################################
#
# Szenario: DM-UPDATE
#
#
###################################################################################################
szenario="DM-UPDATE"

} # Ende der Zeitmessung
