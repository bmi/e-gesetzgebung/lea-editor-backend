#!/bin/bash

# Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
#
# SPDX-License-Identifier: MPL-2.0

scriptname="./tests/test-anlegen-von-dokumenten.sh"
logInfo "***************************************************************************************************"
logInfo "TEST SUITE: Anlegen von Dokumenten"
logInfo "***************************************************************************************************"
TIMEFORMAT="Die Tests benötigten %R Sekunden."
time {

###################################################################################################
#
# Szenario: ANL-DOC-RV
#   Als Legist in der E-Gesetzgebung möchte ich ein neues Regelungsvorhaben anlegen,
#   um eine neue Gesetzesinitiative zu starten.
#
# Schritte:
#   1.  Authentifizieren und JWToken erhalten
#   2.  Anlegen eines neuen Regelungsvorhabens mit den Parametern:
#       - status            :   IN_BEARBEITUNG
#       - langtitel         :   Neues Regelungsvorhaben (SST-Test)
#       - kurzbezeichnung   :   Neues RV anlegen
#       - abkuerzung        :   <Szenario>
#       - vorhabenart       :   GESETZ
#   3.  Anzahl der vorhandenen Dokumentenmappen speichern
#
###################################################################################################
szenario="ANL-DOC-RV"

# -----------------------------------------------
# Schritt 1
# -----------------------------------------------
authenticate

# -----------------------------------------------
# Schritt 2
# -----------------------------------------------
regelungsvorhabenErstellen "IN_BEARBEITUNG" "Neues Regelungsvorhaben (SST-Test)" "Neues RV anlegen" "${szenario}" "GESETZ"
logInfo "RV: ${regelungsvorhabenId}"

# -----------------------------------------------
# Schritt 3
# -----------------------------------------------
dokumentenMappenAuflisten
anzahlDokumentenmappenVorher=${anzahlGefundenerMappen}

###################################################################################################
#
# Szenario: ANL-DOC-DM1
#   Als Legist in der E-Gesetzgebung möchte ich eine neue Dokumentenmappe für ein Stammgesetz
#   anlegen, um darin meine Einzeldokumente zum Vorhaben zu verwalten.
#
# Schritte:
#   1.  Anlegen einer neuen Dokumentenmappe mit den Parametern
#       - title                 :   <Szenario>
#       - type                  :   STAMMGESETZ
#       - regelungsVorhabenId   :   stammt vom erfolgreichen Anlegen eines RV
#   2.  Anzahl der vorhandenen Dokumentenmappen speichern und mit vorheriger Anzahl vergleichen
#
###################################################################################################
szenario="ANL-DOC-DM1"

# -----------------------------------------------
# Schritt 1
# -----------------------------------------------
dokumentenMappeErstellen "${szenario}" "STAMMGESETZ" "${regelungsvorhabenId}"
logInfo "DM: ${dokumentenMappenId}"

# -----------------------------------------------
# Schritt 2
# -----------------------------------------------
dokumentenMappenAuflisten
anzahlDokumentenmappenNachher=${anzahlGefundenerMappen}

# ***********************************************
# TEST: Es gibt jetzt eine Dokumentenmappe mehr.
# ***********************************************
difference=$(expr ${anzahlDokumentenmappenNachher} - ${anzahlDokumentenmappenVorher})
if [[ ${difference} -eq 1 ]]
then
    logSuccess "Anzahl der Dokumentenmappen hat sich um 1 erhöht."
else
    logFailure "Die Anzahl der Dokumentenmappen ist inkonsistent."
fi

# ***********************************************
# TEST: Die neue Dokumentenmappe hat genau 1 
#       Dokument vom Typ REGELUNGSTEXT_STAMMGESETZ
# ***********************************************
jqFilter1=".[] | select(.id==\"${dokumentenMappenId}\") | .documents | length"
logDebug "jqFilter1: '${jqFilter1}'"
jqFilter2=".[] | select(.id==\"${dokumentenMappenId}\") | .documents | .[0].type"
logDebug "jqFilter2: '${jqFilter2}'"
documentCount=$(echo ${body} | jq "${jqFilter1}")
logDebug "Anzahl Default-Dokumente der neuen Mappe: ${documentCount}"
documentType=$(echo ${body} | jq -r "${jqFilter2}")
logDebug "Typ des Default-Dokuments der neuen Mappe: ${documentType}"
if [ "${documentCount}" = "1" ]
then
    logSuccess "Anzahl automatisch angelegter Dokumente ist gleich 1."
else
    logFailure "Unerwartete Anzahl automatisch angelegter Dokumente: ${documentCount}."
fi
if [ "${documentType}" = "REGELUNGSTEXT_STAMMGESETZ" ]
then
    logSuccess "Der Typ des Default-Dokuments ist wie erwartet REGELUNGSTEXT_STAMMGESETZ"
else
    logFailure "Unerwarteter Typ des Default-Dokuments: ${documentType}."
fi

###################################################################################################
#
# Szenario: ANL-DOC-VB
#   Als Legist in der E-Gesetzgebung möchte ich in meiner zuvor erstellten Dokumentenmappe 
#   zusätzlich zum Regelungstext ein Vorblatt anlegen, weil es ein obligatorischer Teil des 
#   Regelungsentwurfs ist.
#
# Schritte:
#   1.  Anlegen eines neuen Einzeldokuments mit den Parametern
#       - id                    :   stammt vom erfolgreichen Anlegen einer Dokumentenmappe
#       - title                 :   Vorblatt-<Szenario>
#       - type                  :   VORBLATT_STAMMGESETZ
#   2.  Auslesen der Typen aller enthaltenen Dokumente der Mappe aus dem Body
#   3.  Abfrage derselben Dokumentenmappe aus der Datenbank per ID
#   4.  Auslesen der Dokumententypen und Abgleich mit Ergebnis aus Schritt 2
#
###################################################################################################
szenario="ANL-DOC-VB"

# -----------------------------------------------
# Schritt 1
# -----------------------------------------------
einzeldokumentErstellen "${dokumentenMappenId}" "Vorblatt-${szenario}" "VORBLATT_STAMMGESETZ"
logInfo "VB: ${documentId}"

# -----------------------------------------------
# Schritt 2
# -----------------------------------------------
returnedDocumentTypes=$(echo ${body} | jq -c '.documents | map(.type)')
logInfo "Vorhandene Dokumente: ${returnedDocumentTypes}"

# -----------------------------------------------
# Schritt 3
# -----------------------------------------------
dokumentenmappeAnfragen "${dokumentenMappenId}"

# -----------------------------------------------
# Schritt 4
# -----------------------------------------------
if  [ $responsecode -eq 200 ]
then
    requestedDocumentTypes=$(echo ${body} | jq -c '.documents | map(.type)')
    logInfo "Abgefragte Dokumente: ${requestedDocumentTypes}"
    if [[ "${returnedDocumentTypes}" == "${requestedDocumentTypes}" ]]
    then
        logSuccess "Persistierte Daten sind konsistent."
    else
        logFailure "Beim Speichern der Daten ist ein Fehler passiert."
    fi
else
    logInfo "Vergleich der Dokumente nicht möglich wegen eines Fehlers"
fi

# =================================================================================================
# Daten für nachfolgende Skripte zur Verfügung stellen
# =================================================================================================
datenVonNeuerDokumentenmappe=$(echo ${body} | jq '{"rvId":.proposition.id, "dmId":.id, "dmVersion":.version, "docs": .documents | map({"docId": .id, "docType": .type})}')

} # Ende der Zeitmessung
