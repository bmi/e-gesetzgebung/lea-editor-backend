#!/bin/bash

# Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
#
# SPDX-License-Identifier: MPL-2.0

scriptname="./helpers.sh"


###################################################################################################
#
# Temporäres Verzeichnis zum Speichern von Rückgabedaten
#
###################################################################################################
fileStorage="../../../../target/temp"
mkdir -p ${fileStorage}


###################################################################################################
#
# Minimales Logging Framework.
# Das gewünschte Log-Level wird im Hauptskript in der Variablen 'logging_level' gesetzt.
#
###################################################################################################
declare -A logLevels=([ALL]=0 [TRACE]=100 [DEBUG]=200 [INFO]=300 [SUCCESS]=350 [WARN]=400 [FAILURE]=450 [ERROR]=500 [FATAL]=600 [OFF]=999)

logThis () {
    local logLevel=$1
    local logMessage=$2

    # check if level exists
    [[ ${logLevels[$logLevel]} ]] || return 1

    # check if level is high enough
    (( ${logLevels[$logLevel]} < ${logLevels[$logging_level]} )) && return 2

    # log message
    local dateTime=$(date --rfc-3339=seconds)
    local level=$(printf %-7s ${logLevel})
    local script=$(printf %-25s ${scriptname:0:25})
    echo -e "${dateTime} [${script}] ${level} : ${logMessage}"
}

logTrace () { 
    local msg=$1 ; logThis "TRACE" "${msg}" 
}

logDebug () { 
    local msg=$1 ; logThis "DEBUG" "${msg}" 
}

logInfo () { 
    local msg=$1 ; logThis "INFO" "${msg}" 
}

logSuccess () { 
    local msg=$1 ; logThis "SUCCESS" "${msg}" 
}

logWarn () { 
    local msg=$1 ; logThis "WARN" "${msg}" 
}

logFailure () { 
    local msg=$1 ; logThis "FAILURE" "${msg}" 
}

logError () { 
    local msg=$1 ; logThis "ERROR" "${msg}" 
}

logFatal () { 
    local msg=$1 ; logThis "FATAL" "${msg}" 
}

###################################################################################################
#
# Hilfsmethode:
# Extrahiert den ResponseCode in die Variable repsonsecode
# und den Body in die Variable body.
#
###################################################################################################
extract () {
responsecode=${response: -3}
body="${response::-3}"
logDebug "Responsecode: ${responsecode}"
logDebug "Body: ${body}"
}

###################################################################################################
#
# Für die Anwendung benötigt man ein JWToken. Dieses holen wir vom IAM.
#
# Es kann immer einer von zwei möglichen Benutzern angemeldet werden. Das wird über den einzigen 
# Parameter gesteuert, wie folgt
#   - Wenn $1 nicht gesetzt ist oder gleich ERSTELLER: Dann Anmeldung als Hauptbenutzer
#   - Wenn $1 gleich MITARBEITER oder irgendein anderer Wert: Dann Anmeldung als Mitarbeiter
#
###################################################################################################
authenticate () {
logInfo "Get JWToken"

if [[ -z "$1" || "$1" == "ERSTELLER" ]]
then
    username=${main_username}
    passwort=${main_passwort}
else
    username=${other_username}
    passwort=${other_passwort}
fi

logDebug "Authentifizierung als ${username}"

response=$(curl -s -w "%{http_code}" -X POST ${iam} \
    -H 'Content-Type: application/x-www-form-urlencoded' \
    -d 'client_id=egg&grant_type=password&client_secret='${secret}'&scope=openid&username='${username}'&password='${passwort})

extract

if  [ $responsecode -eq 401 ]
then
    logError "IAM Authorisierung funktionierte nicht."
    exit 1;
else
    logInfo "Nutzerin ${username} wurde erfolgreich authentifiziert."
fi

token=$(echo ${body} | jq -r '.access_token')
logDebug "JWT: ${token}"
}

###################################################################################################
#
# Vergleich zweier Versionen.
# Es wird angenommen, dass Versionen als Folge von vorzeichenlosen Zahlen getrennt durch Punkte ('.')
# geschrieben werden.
# Das Vergleichsergebnis wird in der Variablen 'biggerVersion' gespeichert und zeigt folgendes an:
#   0:  Beider Versionen sind gleich
#   1:  Die zuerst angegebene Version ist die größere
#   2:  Die als zweites angegebene Version ist die größere
#
###################################################################################################
compareVersions () {
    version1=$1
    version2=$2

    IFS='.' read -ra parts1 <<< "$version1"
    length1="${#parts1[@]}"
    IFS='.' read -ra parts2 <<< "$version2"
    length2="${#parts2[@]}"

    if ((length1 > length2))
    then
        maxLength=$length1
    else
        maxLength=$length2
    fi

    for (( i=0; i<$maxLength; i++ ))
    do 
        next1=${parts1[$i]}
        [[ -z "$next1" ]] && next1=-1
        next2=${parts2[$i]}
        [[ -z "$next2" ]] && next2=-1

        if (( next1 > next2 ))
        then
            biggerVersion=1
            break
        elif (( next1 < next2 ))
        then
            biggerVersion=2
            break
        else
            biggerVersion=0
        fi
    done
}
