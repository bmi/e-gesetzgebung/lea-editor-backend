#!/bin/bash

# Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
#
# SPDX-License-Identifier: MPL-2.0

scriptname="./endpoints/editor/startseite-api.sh"

# GET /propositions -------------------------------------------------------------------------------
alleRegelungsvorhabenDesBenutzersAuflisten () {
method="GET"
path="/propositions"
logInfo "Auflisten aller Regelungsvorhaben des Benutzers: ${method} ${path}"

local url=${domain}${path}
logDebug "Calling URL: ${url}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
        -H 'accept: application/json' \
        -H "Authorization: Bearer ${token}")

extract

if  [ $responsecode -eq 200 ]
then
    anzahlGefundenerRegelungsvorhaben=$(echo ${body} | jq -r 'length')
    logDebug "anzahlGefundenerRegelungsvorhaben: ${anzahlGefundenerRegelungsvorhaben}"
    regelungsvorhabenIds=$(echo ${body} | jq -r 'map(.id+", ") | add | .[:-2]')
    logDebug "regelungsvorhabenIds: ${regelungsvorhabenIds}"
    logInfo "${anzahlGefundenerRegelungsvorhaben} Regelungsvorhaben gefunden"
else
    logError "Die Anfrage nach den Regelungsvorhaben führte zu einem Fehler: ${responsecode}"
fi
}

# alias
get_propositions () {
    alleRegelungsvorhabenDesBenutzersAuflisten "$@"
}


# POST /regulatoryProposal/{id} -------------------------------------------------------------------
#   Parameter $1:   {id} des Regelungsvorhabens
#   Parameter $2:   Seitennummer, integer ≥ 0
#   Parameter $3:   Anzahl Kindelemente pro Seite, integer ≥ 1, default 1
#   Parameter $4:   Sortierkriterium, eines von [ CREATED_AT, ERSTELLT_AM, ZULETZT_BEARBEITET, VERSION ]
#                   default 'CREATED_AT'
#   Parameter $5:   Sortierrichtung, eines von [ ASC, DESC ], default DESC
auflistenAllerMappenUndDokumenteDesRegelungsvorhabens () {
method="POST"
path="/regulatoryProposal/$1"
logInfo "Auflisten aller Dokumentenmappen und Dokumente des RV: ${method} ${path}"

local url=${domain}${path}
logDebug "Calling URL: ${url}"

if [[ -z $3 ]]
then
    pageSize=1
else
    pageSize=$3
fi

if [[ -z $4 ]]
then
    sortBy="CREATED_AT"
else
    sortBy=$4
fi

if [[ -z $5 ]]
then
    sortDir="DESC"
else
    sortDir=$5
fi

payload='{  "pageNumber":'$2',
            "pageSize":'$pageSize',
            "sortBy":"'$sortBy'",
            "sortDirection":"'$sortDir'"
        }'

logDebug "Payload: ${payload}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
        -H 'accept: application/json' \
        -H "Authorization: Bearer ${token}" \
        -H 'Content-Type: application/json' \
        -d "${payload}")

extract

if  [ $responsecode -eq 200 ]
then
    logInfo "Dokumentenmappen und Dokumente zum RV wurden erfolgreich geladen."
else
    logError "Die Anfrage nach den Mappen des Regelungsvorhabens führte zu einem Fehler: ${responsecode}"
fi
}

# alias
post_regulatoryProposal_id () {
    auflistenAllerMappenUndDokumenteDesRegelungsvorhabens "$@"
}


# POST /meineDokumente ----------------------------------------------------------------------------
#   Parameter $1:   Seitennummer, integer ≥ 0
#   Parameter $2:   Anzahl Kindelemente pro Seite, integer ≥ 1, default 1
#   Parameter $3:   Sortierkriterium, eines von [ CREATED_AT, ERSTELLT_AM, ZULETZT_BEARBEITET, VERSION ]
#                   default 'CREATED_AT'
#   Parameter $4:   Sortierrichtung, eines von [ ASC, DESC ], default DESC
meineDokumenteLaden () {
method="POST"
path="/meineDokumente"
logInfo "Daten für Editor-Startseite Tab 1 laden: ${method} ${path}"

local url=${domain}${path}
logDebug "Calling URL: ${url}"

if [[ -z $2 ]]
then
    pageSize=1
else
    pageSize=$2
fi

if [[ -z $3 ]]
then
    sortBy="CREATED_AT"
else
    sortBy=$3
fi

if [[ -z $4 ]]
then
    sortDir="DESC"
else
    sortDir=$4
fi

payload='{  "pageNumber":'$1',
            "pageSize":'$pageSize',
            "sortBy":"'$sortBy'",
            "sortDirection":"'$sortDir'"
        }'

logDebug "Payload: ${payload}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
        -H 'accept: application/json' \
        -H "Authorization: Bearer ${token}" \
        -H 'Content-Type: application/json' \
        -d "${payload}")

extract

if  [ $responsecode -eq 200 ]
then
    logInfo "Die Mappen und Dokumente des Benutzers wurden erfolgreich geladen."
else
    logError "Die Anfrage nach den Mappen und Dokumenten des Benutzers führte zu einem Fehler: ${responsecode}"
fi
}

# alias 
post_meineDokumente () {
    meineDokumenteLaden "$@"
}


# GET /dokumenteAusAbstimmungen -------------------------------------------------------------------
meineAbstimmungsdokumenteLaden () {
method="GET"
path="/dokumenteAusAbstimmungen"
logInfo "Auflisten aller Dokumente aus Abstimmungen des Benutzers: ${method} ${path}"

local url=${domain}${path}
logDebug "Calling URL: ${url}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
        -H 'accept: application/json' \
        -H "Authorization: Bearer ${token}")

extract

if  [ $responsecode -eq 200 ]
then
    logInfo "Dokumente aus Abstimmungen des Benutzers wurden erfolgreich geladen."
else
    logError "Die Anfrage nach den Abstimmungsdokumenten führte zu einem Fehler: ${responsecode}"
fi
}

# alias
get_dokumenteAusAbstimmungen () {
    meineAbstimmungsdokumenteLaden "$@"
}
