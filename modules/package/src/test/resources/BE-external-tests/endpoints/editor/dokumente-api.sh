#!/bin/bash

# Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
#
# SPDX-License-Identifier: MPL-2.0

scriptname="./endpoints/editor/dokumente-api.sh"


# GET /documents/{id} -----------------------------------------------------------------------------
#   Parameter $1:   {id} des Dokuments
einzeldokumentLaden () {
method="GET"
path="/documents/$1"
logInfo "Laden eines Einzeldokuments: ${method} ${path}"

local url=${domain}${path}
logDebug "Calling URL: ${url}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
        -H 'accept: application/json' \
        -H "Authorization: Bearer ${token}")

extract

if  [ $responsecode -eq 200 ]
then
    logInfo "Das Dokument wurde erfolgreich geladen."
else
    logError "Das Laden des Dokuments führte zu einem Fehler: ${responsecode}"
fi
}

# alias
get_documents_id () {
    einzeldokumentLaden "$@"
}


# PATCH /documents/{id} -----------------------------------------------------------------------------
#   Parameter $1:   {id} des Dokuments
#   Parameter $2:   title, String - Klartext für den neuen Titel
#   Parameter $3:   content, String - BASE64-encodierter JSON-String des Dokumentinhalts
#                       Kodierung findet in der Funktion statt, also soll Klartext im Parameter benutzt 
#                       werden.
dokumentAktualisieren () {
method="PATCH"
path="/documents/$1"
logInfo "Aktualisieren eines Einzeldokuments: ${method} ${path}"

local url=${domain}${path}
logDebug "Calling URL: ${url}"

if [[ -z $2 ]]
then
    if [[ -z $3 ]]
    then
        logError "Anfrage konnte nicht ausgeführt werden. Es muss mind. ein Feld 'title' oder 'content' befüllt sein."
        return 1
    else
        content=$(echo "$3" | base64 -w 0)
        payload='{"content":"'$content'"}'
    fi
else
    titleLine='"title":"'$2'"'
    if [[ -z $3 ]]
    then
        payload='{'$titleLine'}'
    else
        content=$(echo "$3" | base64 -w 0)
        payload='{'$titleLine',
                "content":"'$content'"
                }'
    fi 
fi

logDebug "Payload: ${payload}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
        -H 'accept: application/json' \
        -H "Authorization: Bearer ${token}" \
        -H 'Content-Type: application/json' \
        -d "${payload}")

extract

if  [ $responsecode -eq 200 ]
then
    logInfo "Das Dokument wurde erfolgreich aktualisiert."
else
    logError "Die Aktualisierung des Dokuments konnte nicht ausgeführt werden: ${responsecode}"
fi
}

# alias
patch_documents_id () {
    dokumentAktualisieren "$@"
}


# POST /documents/{id}/export ---------------------------------------------------------------------
#   Parameter $1:   {id} des Einzeldokuments - wird vom Service nicht verwendet
#   Parameter $2:   content, String - BASE64-encodierter JSON-String des Dokumentinhalts
#                       Kodierung findet in der Funktion statt, also soll Klartext im Parameter benutzt 
#                       werden.
einzeldokumentExportieren () {
method="POST"
path="/documents/$1/export"
logInfo "Exportieren eines Einzeldokuments: ${method} ${path}"

local url=${domain}${path}
logDebug "Calling URL: ${url}"

if [[ -z $2 ]]
then
    logError "Es muss ein Dokumentinhalt zum Exportieren angegeben werden."
    return 1
else
    content=$(echo "$2" | base64 -w 0)
fi

payload='{  "content":"'$content'"
        }'

logDebug "Payload: ${payload}"

local dateTime=$(date --rfc-3339=seconds)
local filename="${dateTime}-docExport-$1.xml"
local xmlFile=$(realpath "${fileStorage}/${filename}")
logDebug "Das Archiv wird lokal gespeichert: ${xmlFile}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
        -H 'accept: application/xml' \
        -H 'Content-Type: application/json' \
        -H "Authorization: Bearer ${token}" \
        -H 'Origin: http://localhost:8080' \
        -d "${payload}" \
        -o "${xmlFile}")

extract

if  [ $responsecode -eq 200 ]
then
    logInfo "Das Dokument wurde erfolgreich exportiert."
else
    logError "Das Dokument konnte nicht exportiert werden: ${responsecode}"
fi
}

# alias
post_documents_id_export () {
    einzeldokumentExportieren "$@"
}

