#!/bin/bash

# Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
#
# SPDX-License-Identifier: MPL-2.0

scriptname="./endpoints/editor/benutzer-api.sh"

# GET /user ---------------------------------------------------------------------------------------
angemeldetenBenutzerAnzeigen () {
method="GET"
path="/user"
logInfo "Ausgeben der Daten des angemeldeten Benutzers: ${method} ${path}"

local url=${domain}${path}
logDebug "Calling URL: ${url}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
        -H 'accept: application/json' \
        -H "Authorization: Bearer ${token}")

extract

if [[ $responsecode -eq 200 ]]
then
    userName=$(echo ${body} | jq -r '.name')
    logDebug "userName: ${userName}"
    userEmail=$(echo ${body} | jq -r '.email')
    logDebug "userEmail: ${userEmail}"
    userGid=$(echo ${body} | jq -r '.gid')
    logDebug "userGid: ${userGid}"
    logInfo "Benutzerinformationen wurden erfolgreich geladen."
else
    logError "Benutzerinformationen konnten nicht geladen werden: ${responsecode}"
fi
}

# alias 
get_user () {
    angemeldetenBenutzerAnzeigen "$@"
}


# GET /user/settings ------------------------------------------------------------------------------
benutzerEinstellungenLaden () {
method="GET"
path="/user/settings"
logInfo "Ausgeben der Benutzereinstellungen: ${method} ${path}"

local url=${domain}${path}
logDebug "Calling URL: ${url}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
        -H 'accept: application/json' \
        -H "Authorization: Bearer ${token}")

extract

if [[ $responsecode -eq 200 ]]
then
    angepinnteDokumentenmappen=$(echo ${body} | jq '.pinnedDokumentenmappen')
    logDebug "angepinnteDokumentenmappen: ${angepinnteDokumentenmappen}"
    logInfo "Benutzereinstellungen wurden erfolgreich geladen."
else
    logError "Benutzereinstellungen konnten nicht geladen werden: ${responsecode}"
fi
}

# alias 
get_user_settings () {
    benutzerEinstellungenLaden "$@"
}


# PUT /user/settings ------------------------------------------------------------------------------
#   In dieser Implementierung gibt es nur einen String-Parameter, der das vollständige JSON-Array 
#   von Objeken mit der "rvId" und das Array von Strings für "dmIds" darstellt.
benutzerEinstellungenAendern () {
method="PUT"
path="/user/settings"
logInfo "Ändern der Benutzereinstellungen: ${method} ${path}"

local url=${domain}${path}
logDebug "Calling URL: ${url}"

payload='{  "pinnedDokumentenmappen":'$1'
        }'

logDebug "Payload: ${payload}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
        -H 'accept: application/json' \
        -H "Authorization: Bearer ${token}" \
        -H 'Content-Type: application/json' \
        -d "${payload}")

extract

if [[ $responsecode -eq 200 ]]
then
    logInfo "Benutzereinstellungen wurden erfolgreich geändert."
else
    logError "Benutzereinstellungen konnten nicht geändert werden: ${responsecode}"
fi
}

# alias 
put_user_settings () {
    benutzerEinstellungenAendern "$@"
}
