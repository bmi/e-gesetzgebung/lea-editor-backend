#!/bin/bash

# Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
#
# SPDX-License-Identifier: MPL-2.0

scriptname="./endpoints/editor/dokumentenmappen-api.sh"

###################################################################################################
#
# Auflisten aller Dokumentenmappen des angemeldeten Benutzers.
# GET /compounddocuments
#
# Parameter: keine
#
# Gesetzte Variablen nach erfolgreichem Durchlauf:
#   - anzahlGefundenerMappen
#   - dokumentenMappenIds
#
###################################################################################################
dokumentenMappenAuflisten () {
method="GET"
path="/compounddocuments"
logInfo "Auflisten aller Dokumentenmappen: ${method} ${path}"

local url=${domain}${path}
logDebug "Calling URL: ${url}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
       -H 'accept: application/json' \
       -H "Authorization: Bearer ${token}")

extract

if  [ $responsecode -eq 200 ]
then
    anzahlGefundenerMappen=$(echo ${body} | jq -r 'length')
    logDebug "anzahlGefundenerMappen: ${anzahlGefundenerMappen}"
    dokumentenMappenIds=$(echo ${body} | jq -r 'map(.id+", ") | add | .[:-2]')
    logDebug "dokumentenMappenIds: ${dokumentenMappenIds}"
    logInfo "${anzahlGefundenerMappen} Dokumentenmappen gefunden"
else
    logError "Die Anfrage nach den Dokumentenmappen führte zu einem Fehler: ${responsecode}"
fi
}

# alias
get_compounddocuments () {
    dokumentenMappenAuflisten "$@"
}

###################################################################################################
#
# Erstellung einer Dokumentenmappe.
# POST /compounddocuments
#
# Parameter: (müssen alle jeweils von '"' umschlossen werden)
#   1:  title                   , String
#   2:  type                    , Eines aus [ AENDERUNGSVERORDNUNG, MANTELGESETZ, STAMMGESETZ, 
#                                             STAMMVERORDNUNG ]
#   3:  regelungsVorhabenId     , String (Format: UUID)
#
# Konstanten:
#       titleRegulatoryText     := Regelungstext-<Zeitstempel>
#       initialNumberOfLevels   := 0
#
# Gesetzte Variablen nach erfolgreichem Durchlauf:
#   - dokumentenMappenId
#   - version
#   - status
#
###################################################################################################
dokumentenMappeErstellen () {
method="POST"
path="/compounddocuments"
logInfo "Erstellen einer Dokumentenmappe: ${method} ${path}"

local url=${domain}${path}
local dateTime=$(date --rfc-3339=seconds)
local dateFmt=$(date +%Y-%m-%dT%H:%M:%S)
logDebug "Calling URL: ${url}"

payload='{  "title":"'$1' - '${dateTime}'",
            "type":"'$2'",
            "regelungsVorhabenId":"'$3'",
            "titleRegulatoryText":"Regelungstext-'${dateFmt}'",
            "initialNumberOfLevels":"0"
        }'

logDebug "Payload: ${payload}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
        -H 'accept: application/json' \
        -H 'Content-Type: application/json' \
        -H "Authorization: Bearer ${token}" \
        -H "Cookie: XSRF-TOKEN-EDITOR=${xsfr}" \
        -H "X-XSRF-TOKEN-EDITOR: ${xsfr}" \
        -H 'Origin: http://localhost:8080' \
        -d "${payload}")

extract

if  [ $responsecode -eq 201 ]
then
    dokumentenMappenId=$(echo ${body} | jq -r '.id')
    logDebug "dokumentenMappenId: ${dokumentenMappenId}"
    version=$(echo ${body} | jq -r '.version')
    logDebug "version: ${version}"
    status=$(echo ${body} | jq -r '.status')
    logDebug "status: ${status}"
    logInfo "Die Dokumentenmappe wurde erfolgreich erstellt."
else
    logError "Die Dokumentenmappe konnte nicht erstellt werden: ${responsecode}"
fi
}

# alias
post_compounddocuments () {
    dokumentenMappeErstellen "$@"
}

###################################################################################################
#
# Anlegen eines weiteren Einzeldokuments in einer existierenden Dokumentenmappe.
# POST /compounddocuments/{id}/documents
#
# Parameter: (müssen alle jeweils von '"' umschlossen werden)
#   1.  id                      , String (Format: UUID); die DokumentenmappenId
#   2.  title                   , String
#   3.  type                    , Eines aus [ ANLAGE, 
#                                             ANSCHREIBEN_MANTELGESETZ, ANSCHREIBEN_MANTELVERORDNUNG, 
#                                             ANSCHREIBEN_STAMMGESETZ, ANSCHREIBEN_STAMMVERORDNUNG, 
#                                             BEGRUENDUNG_MANTELGESETZ, BEGRUENDUNG_MANTELVERORDNUNG, 
#                                             BEGRUENDUNG_STAMMGESETZ, BEGRUENDUNG_STAMMVERORDNUNG, 
#                                             RECHTSETZUNGSDOKUMENT_MANTELGESETZ, 
#                                             RECHTSETZUNGSDOKUMENT_MANTELVERORDNUNG, 
#                                             RECHTSETZUNGSDOKUMENT_STAMMGESETZ, 
#                                             RECHTSETZUNGSDOKUMENT_STAMMVERORDNUNG, 
#                                             REGELUNGSTEXT_MANTELGESETZ, REGELUNGSTEXT_MANTELVERORDNUNG, 
#                                             REGELUNGSTEXT_STAMMGESETZ, REGELUNGSTEXT_STAMMVERORDNUNG, 
#                                             SYNOPSE, 
#                                             VORBLATT_MANTELGESETZ, VORBLATT_MANTELVERORDNUNG, 
#                                             VORBLATT_STAMMGESETZ, VORBLATT_STAMMVERORDNUNG ]
#
# Konstanten:
#       regelungsvorhabenId     := "" (leerer String, Altlast)
#       initialNumberOfLevels   := 0
#
# Gesetzte Variablen nach erfolgreichem Durchlauf:
#   - documentId
#
###################################################################################################
einzeldokumentErstellen () {
method="POST"
path="/compounddocuments/$1/documents"
logInfo "Anlegen eines Einzeldokuments: ${method} ${path}"

local url=${domain}${path}
local dateFmt=$(date +%Y-%m-%dT%H:%M:%S)
logDebug "Calling URL: ${url}"

local title=$2' - '${dateFmt}
logDebug "Dokumenttitel, parameter: $2"
logDebug "Dokumenttitel, vollständig: ${title}"
payload='{  "regelungsvorhabenId":"",
            "initialNumberOfLevels":"0",
            "title":"'${title}'",
            "type":"'$3'"
        }'

logDebug "Payload: ${payload}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
       -H 'Content-Type: application/json' \
       -H "Authorization: Bearer ${token}" \
       -H "Cookie: XSRF-TOKEN-EDITOR=${xsfr}" \
       -H "X-XSRF-TOKEN-EDITOR: ${xsfr}" \
       -H 'Origin: http://localhost:8080' \
       -d "${payload}")

extract

if  [ $responsecode -eq 201 ]
then
    local jqFilter=".documents | .[] | select(.title==\"${title}\") | .id"
    logDebug "JQ Filter: ${jqFilter}"
    documentId=$(echo ${body} | jq -r "${jqFilter}")
    logDebug "documentId: ${documentId}"
    logInfo "Das Dokument wurde erfolgreich erstellt."
else
    logError "Das Dokument konnte nicht erstellt werden: ${responsecode}"
fi
}

# alias
post_compounddocuments_id_documents () {
    einzeldokumentErstellen "$@"
}

###################################################################################################
#
# Anfrage einer bestimmten Dokumentenmappe per ID.
# GET /compounddocuments/{id}
#
# Parameter: (müssen alle jeweils von '"' umschlossen werden)
#   1.  id                      , String (Format: UUID); die DokumentenmappenId
#
###################################################################################################
dokumentenmappeAnfragen () {
method="GET"
path="/compounddocuments/$1"
logInfo "Abfrage einer Dokumentenmappe: ${method} ${path}"

local url=${domain}${path}
logDebug "Calling URL: ${url}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
       -H 'Content-Type: application/json' \
       -H "Authorization: Bearer ${token}")

extract

if  [ $responsecode -eq 200 ]
then
    logInfo "Die Dokumentenmappe wurde erfolgreich geladen."
else
    logError "Die Dokumentenmappe konnte nicht geladen werden: ${responsecode}"
fi
}

# alias
get_compounddocuments_id () {
    dokumentenmappeAnfragen "$@"
}

###################################################################################################
#
# Erstellen einer neuen Version einer Dokumentenmappe.
# POST /compounddocuments/{id}/version?copyId
#
# Parameter: (müssen alle jeweils von '"' umschlossen werden)
#   1.  id                      , String (Format: UUID); die DokumentenmappenId
#   2.  copyId                  , String (UUIDs getrennt durch '&'); IDs der Dokumente,
#                                 die mitkopiert werden sollen; Wert muss mit '?' beginnen!
#       Bsp.: "?copyId=f3d88cc7-bccd-41fb-b6dc-9c257bc3b2de&copyId=41dfe5ec-3765-46ba-a0de-6823c76f39ef"
#   3.  title                   , String 
#
# Gesetzte Variablen nach erfolgreichem Durchlauf:
#   - dokumentenMappenId
#   - version
#   - status
#
###################################################################################################
dokumentenmappenVersionErstellen () {
method="POST"
path="/compounddocuments/$1/version"
logInfo "Anlegen einer neuen Version einer Dokumentenmappe: ${method} ${path}"

local url=${domain}${path}$2
logDebug "Calling URL: ${url}"

payload='{  "title":"'$3'"
        }'

logDebug "Payload: ${payload}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
        -H 'accept: application/json' \
        -H 'Content-Type: application/json' \
        -H "Authorization: Bearer ${token}" \
        -H "Cookie: XSRF-TOKEN-EDITOR=${xsfr}" \
        -H "X-XSRF-TOKEN-EDITOR: ${xsfr}" \
        -H 'Origin: http://localhost:8080' \
        -d "${payload}")

extract

if  [ $responsecode -eq 200 ]
then
    dokumentenMappenId=$(echo ${body} | jq -r '.id')
    logDebug "dokumentenMappenId: ${dokumentenMappenId}"
    version=$(echo ${body} | jq -r '.version')
    logDebug "version: ${version}"
    status=$(echo ${body} | jq -r '.state')
    logDebug "status: ${status}"
    logInfo "Die Version der Dokumentenmappe wurde erfolgreich erstellt."
else
    logError "Die Version der Dokumentenmappe konnte nicht erstellt werden: ${responsecode}"
fi
}

# alias
post_compounddocuments_id_version () {
    dokumentenmappenVersionErstellen "$@"
}

###################################################################################################
#
# Ändern der Metadaten einer Dokumentenmappe.
# PATCH /compounddocuments/{id}
#
# Parameter: (müssen alle jeweils von '"' umschlossen werden)
#   1.  id                      , String (Format: UUID); die DokumentenmappenId
#   2.  title                   , String 
#   3.  verfahrensTyp           , Eines von [ REFERENTENENTWURF, REGIERUNGSENTWURF ]
#   4.  status                  , Eines von [ DRAFT, FINAL, FREEZE, BEREIT_FUER_KABINETTVERFAHREN, 
#                                             ZUGELEITET_PKP, ZUGESTELLT_BUNDESRAT ]
#
###################################################################################################
updateDokumentenmappe () {
method="PATCH"
path="/compounddocuments/$1"
logInfo "Ändern der Metadaten einer Dokumentenmappe: ${method} ${path}"

local url=${domain}${path}
logDebug "Calling URL: ${url}"

payload='{  "title":"'$2'",
            "verfahrensTyp":"'$3'",
            "status":"'$4'"
        }'

logDebug "Payload: ${payload}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
        -H 'accept: application/json' \
        -H 'Content-Type: application/json' \
        -H "Authorization: Bearer ${token}" \
        -H "Cookie: XSRF-TOKEN-EDITOR=${xsfr}" \
        -H "X-XSRF-TOKEN-EDITOR: ${xsfr}" \
        -H 'Origin: http://localhost:8080' \
        -d "${payload}")

extract

if  [ $responsecode -eq 200 ]
then
    logInfo "Die Aktualisierung der Dokumentenmappe war erfolgreich."
else
    logError "Die Aktualisierung der Dokumentenmappe schlug fehl: ${responsecode}"
fi
}

# alias
patch_compounddocuments_id () {
    updateDokumentenmappe "$@"
}




# GET /compounddocumenttitles ---------------------------------------------------------------------
dokumentenmappenKurzinfosAuflisten () {
method='GET'
path="/compounddocumenttitles"
logInfo "Abruf aller Dokumentenmappentitel des angemeldeten Benutzers: ${method} ${path}"

local url=${domain}${path}
logDebug "Calling URL: ${url}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
       -H 'accept: application/json' \
       -H "Authorization: Bearer ${token}")

extract

if  [ $responsecode -eq 200 ]
then
    logInfo "Der Abruf aller Dokumentenmappentitel war erfolgreich."
else
    logError "Der Abruf aller Dokumentenmappentitel schlug fehl: ${responsecode}"
fi
}

# alias
get_compounddocumenttitles () {
    dokumentenmappenKurzinfosAuflisten "$@"
}


# GET /compounddocuments/{id}/list ----------------------------------------------------------------
#   Einziger Parameter ist die {id} der Dokumentenmappe im Pfad: $1
dokumenteInDokumentenmappeAuflisten () {
method='GET'
path="/compounddocuments/$1/list"
logInfo "Auflisten aller Dokumente (ohne Inhalt) der Dokumentenmappe: ${method} ${path}"

local url=${domain}${path}
logDebug "Calling URL: ${url}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
       -H 'accept: application/json' \
       -H "Authorization: Bearer ${token}")

extract

if  [ $responsecode -eq 200 ]
then
    logInfo "Das Auflisten aller Dokumente war erfolgreich."
else
    logError "Das Auflisten aller Dokumente schlug fehl: ${responsecode}"
fi
}

# alias
get_compounddocuments_id_list () {
    dokumenteInDokumentenmappeAuflisten "$@"
}


# GET /compounddocuments/{id}/egfaStatus ----------------------------------------------------------
#   Einziger Parameter ist die {id} der Dokumentenmappe im Pfad: $1
statusDerGesetzesfolgenabschaetzungAbfragen () {
method='GET'
path="/compounddocuments/$1/egfaStatus"
logInfo "Auflisten des Status der GFA-Module der Dokumentenmappe: ${method} ${path}"

local url=${domain}${path}
logDebug "Calling URL: ${url}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
       -H 'accept: application/json' \
       -H "Authorization: Bearer ${token}")

extract

if  [ $responsecode -eq 200 ]
then
    logInfo "Das Auflisten aller GFA-Module war erfolgreich."
else
    logError "Das Auflisten aller GFA-Module schlug fehl: ${responsecode}"
fi
}

# alias
get_compounddocuments_id_egfaStatus () {
    statusDerGesetzesfolgenabschaetzungAbfragen "$@"
}


# GET /compounddocuments/regelungsvorhaben/{id}/list ----------------------------------------------
#   Einziger Parameter ist die {id} des Regelungsvorhabens im Pfad: $1
alleMappenSortiertNachVersionDesRVsLaden () {
method='GET'
path="/compounddocuments/regelungsvorhaben/$1/list"
logInfo "Auflisten der nach Version sortierten Dokumentenmappen: ${method} ${path}"

local url=${domain}${path}
logDebug "Calling URL: ${url}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
       -H 'accept: application/json' \
       -H "Authorization: Bearer ${token}")

extract

if  [ $responsecode -eq 200 ]
then
    logInfo "Das Auflisten aller Dokumentenmappen des Regelungsvorhabens war erfolgreich."
else
    logError "Das Auflisten aller Dokumentenmappen des Regelungsvorhabens schlug fehl: ${responsecode}"
fi
}

# alias
get_compounddocuments_regelungsvorhaben_id_list () {
    alleMappenSortiertNachVersionDesRVsLaden "$@"
}


# GET /compounddocuments/{id}/allowedusers/{action} -----------------------------------------------
# Parameter $1: {id} der Dokumentenmappe
# Parameter $2: {action}, ein String wie "LESEN" oder "SCHREIBEN"
alleZurAktionBerechtigtenBenutzerAuflisten () {
method='GET'
path="/compounddocuments/$1/allowedusers/$2"
logInfo "Auflisten der berechtigten Benutzer: ${method} ${path}"

local url=${domain}${path}
logDebug "Calling URL: ${url}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
       -H 'accept: application/json' \
       -H "Authorization: Bearer ${token}")

extract

if  [ $responsecode -eq 200 ]
then
    logInfo "Das Auflisten aller berechtigten Benutzer war erfolgreich."
else
    logError "Das Auflisten aller berechtigten Benutzer schlug fehl: ${responsecode}"
fi
}

# alias
get_compounddocuments_id_allowedusers_action () {
    alleZurAktionBerechtigtenBenutzerAuflisten "$@"
}


# POST /compounddocuments/{id}/export -------------------------------------------------------------
#   Der ZIP-Inhalt sollte in einer lokalen Datei gespeichert werden. Das passiert aber nicht.
#   Muss später analysiert werden.
dokumentenmappeExportieren () {
method="POST"
path="/compounddocuments/$1/export"
logInfo "Exportieren der Dokumentenmappe: ${method} ${path}"

local url=${domain}${path}$2
logDebug "Calling URL: ${url}"

local dateFmt=$(date +%Y-%m-%dT%H:%M:%S)
local filename="DM-Export.zip"
local zipFile=$(realpath "${fileStorage}/${dateFmt}-${filename}")
logDebug "Das Archiv wird lokal gespeichert: ${zipFile}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
        -H 'accept: application/zip' \
        -H "Authorization: Bearer ${token}" \
        -d '' \
        -o "${zipFile}")

extract

if  [ $responsecode -eq 200 ]
then
    logInfo "Der Export der Dokumentenmappe war erfolgreich."
else
    logError "Der Export der Dokumentenmappe schlug fehl: ${responsecode}"
fi
}

# alias
post_compounddocuments_id_export () {
    dokumentenmappeExportieren "$@"
}


# POST /compounddocuments/{id}/egfadata -----------------------------------------------------------
#   Parameter $1:   {id} der Dokumentenmappe
#   Parameter $2:   JSON-Array mit Modulnamen als String
#   Parameter $3:   Flag, ob Kopie der Mappe erstellt werden soll, boolean, default: false
inhalteDerGesetzesfolgenabschaetzungImportieren () {
method="POST"
path="/compounddocuments/$1/egfadata"
logInfo "Importieren der GFA-Inhalte: ${method} ${path}"

local url=${domain}${path}
logDebug "Calling URL: ${url}"

if [[ -z $3 ]] 
then
    createCopy="false"
else
    createCopy="$3"
fi

payload='{  "moduleNames":'$2',
            "createCopy": '${createCopy}'
        }'

logDebug "Payload: ${payload}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
        -H 'accept: application/json' \
        -H 'Content-Type: application/json' \
        -H "Authorization: Bearer ${token}" \
        -d "${payload}")

extract

if  [ $responsecode -eq 200 ]
then
    logInfo "Die GFA-Inhalte wurden erfolgreich importiert."
else
    logError "Die GFA-Inhalte konnten nicht importiert werden: ${responsecode}"
fi
}

# alias
post_compounddocuments_id_egfadata () {
    inhalteDerGesetzesfolgenabschaetzungImportieren "$@"
}


# POST /compounddocuments/{id}/documents/import ---------------------------------------------------
#
#   Zur Zeit nicht implementiert. Benötigt XML-Inhalt zum importieren.


# POST /compounddocuments/{id}/changewritepermissions ---------------------------------------------
#   Parameter $1:   {id} der Dokumentenmappe
#   Parameter $2:   user gid, String 
#   Parameter $3:   user email, String
#   Parameter $4:   user name, String
#   Parameter $5:   freigabetyp, String; [ LESERECHTE, SCHREIBRECHTE ]
#   Parameter $6:   befristung, String, date-time im ISO8601-Format mit Millisekunden inkl. "Z"-Symbol
#   Parameter $7:   erstelleNeueVersionBeiDraft, boolean, default: false
#   Konstante:      anmerkungen, String
zugriffAufDokumentenmappeAendern () {
method="POST"
path="/compounddocuments/$1/changewritepermissions"
logInfo "Ändern der Zugriffsrechte auf Dokumentenmappe: ${method} ${path}"

local url=${domain}${path}
local dateTime=$(date +%Y-%m-%dT%H:%M:%S.%3NZ)
logDebug "Calling URL: ${url}"

local anmerkungen="changewritepermissions-${dateTime}"

if [[ -z $7 ]]
then
    neueVersion="false"
else
    neueVersion="$7"
fi

payload='{  "gid":"'$2'",
            "email":"'$3'",
            "name":"'$4'",
            "freigabetyp":"'$5'",
            "anmerkungen":"'${anmerkungen}'",
            "befristung":"'$6'",
            "erstelleNeueVersionBeiDraft":'${neueVersion}'
        }'

logDebug "Payload: ${payload}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
        -H 'accept: */*' \
        -H 'Content-Type: application/json' \
        -H "Authorization: Bearer ${token}" \
        -d "${payload}")

extract

if  [ $responsecode -eq 200 ]
then
    logInfo "Zugriffsrechte wurden erfolgreich geändert."
else
    logError "Zugriffsrechte konnten nicht geändert werden: ${responsecode}"
fi
}

# alias
post_compounddocuments_id_changewritepermissions () {
    zugriffAufDokumentenmappeAendern "$@"
}


# POST /compounddocuments/{id}/allowedusers -------------------------------------------------------
#   Parameter $1:   {id} der Dokumentenmappe
#   Parameter $2:   JSON-Array von UserRightsDTO-Objekten als String
leseRechteVergeben () {
method="POST"
path="/compounddocuments/$1/allowedusers"
logInfo "Ändern der Leserechte auf Dokumentenmappe: ${method} ${path}"

local url=${domain}${path}
local dateTime=$(date +%Y-%m-%dT%H:%M:%S.%3NZ)
logDebug "Calling URL: ${url}"

payload=$2

logDebug "Payload: ${payload}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
        -H 'accept: */*' \
        -H 'Content-Type: application/json' \
        -H "Authorization: Bearer ${token}" \
        -d "${payload}")

extract

if  [ $responsecode -eq 200 ]
then
    logInfo "Leserechte wurden erfolgreich geändert."
else
    logError "Leserechte konnten nicht geändert werden: ${responsecode}"
fi
}

# alias
post_compounddocuments_id_allowedusers () {
    leseRechteVergeben "$@"
}

