#!/bin/bash

# Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
#
# SPDX-License-Identifier: MPL-2.0

scriptname="./endpoints/plattform/regelungsvorhaben-api.sh"

###################################################################################################
#
# Erstellung eines Regelungsvorhabens.
# POST /regelungsvorhaben
#
# Parameter: (müssen alle jeweils von '"' umschlossen werden)
#   1:  status                  , Eines aus [ ARCHIVIERT, IN_BEARBEITUNG, ENTWURF, BUNDESTAG ]
#   2:  langtitel               , String
#   3:  kurzbezeichnung         , String
#   4:  abkuerzung              , String
#   5:  vorhabenart             , Eines aus [ GESETZ, RECHTSVERORDNUNG, VERWALTUNGSVORSCHRIFT ] 
#
# Konstanten:
#       initiant                := BUNDESREGIERUNG
#       technischesFfRessort    := b8aa797e-2cf9-4671-a560-7016960bbf5c
#       allFfRessorts           := ["b8aa797e-2cf9-4671-a560-7016960bbf5c"]
#       beteiligteRessorts      := []
#       sachgebiete             := []
#       grundtyp                := GESETZ_EINFACH
#       besondereUmstaende      := []
#       euVeranlasst            := LEER
#       beteiligungBundestag    := BUNDESTAG
#       beteiligungBundesrat    := BETEILIGUNGSERFORDERNIS_SPAETER_FESTLEGEN
#
###################################################################################################
regelungsvorhabenErstellen () {
method="POST"
path="/regelungsvorhaben"
logInfo "Erstellung eines Regelungsvorhabens: ${method} ${path}"

local url=${plattform}${path}
local dateTime=$(date --rfc-3339=seconds)
logDebug "Calling URL: ${url}"

payload='{  "status":"'$1'",
            "langtitel":"'$2'",
            "kurzbezeichnung":"'$3'",
            "abkuerzung":"'$4' - '${dateTime}'",
            "vorhabenart":"'$5'",
            "initiant":"BUNDESREGIERUNG",
            "technischesFfRessort":"b8aa797e-2cf9-4671-a560-7016960bbf5c",
            "allFfRessorts":["b8aa797e-2cf9-4671-a560-7016960bbf5c"],
            "beteiligteRessorts":[],
            "sachgebiete":[],
            "grundtyp":"GESETZ_EINFACH",
            "besondereUmstaende":[],
            "euVeranlasst":"LEER",
            "beteiligungBundestag":["BUNDESTAG"],
            "beteiligungBundesrat":"BETEILIGUNGSERFORDERNIS_SPAETER_FESTLEGEN"
        }'

logDebug "Payload: ${payload}"

response=$(curl -s -w "%{http_code}" -X ${method} ${url} \
        -H 'Content-Type: application/json' \
        -H "Authorization: Bearer ${token}" \
        -H "Cookie: XSRF-TOKEN-EDITOR=${xsfr}" \
        -H "X-XSRF-TOKEN-EDITOR: ${xsfr}" \
        -H 'Origin: http://localhost:8080' \
        -d "${payload}")

extract

if  [ $responsecode -eq 200 ]
then
    regelungsvorhabenId=$(echo ${body} | jq -r '.base.id')
    logDebug "regelungsvorhabenId: ${regelungsvorhabenId}"
    langtitel=$(echo ${body} | jq -r '.dto.langtitel')
    logDebug "langtitel: ${langtitel}"
    abkuerzung=$(echo ${body} | jq -r '.dto.abkuerzung')
    logDebug "abkuerzung: ${abkuerzung}"
    logInfo "Das Regelungsvorhaben wurde erfolgreich erstellt."
else
    logError "Das Regelungsvorhaben konnte nicht erstellt werden: ${responsecode}"
fi
}
