# Integrationstests der REST-API des Editor-Backends

In diesem Verzeichnis befinden sich Skripte zum Testen der REST-Schnittstelle des Backends des Editors der E-Gesetzgebung. Sie sollen in erster Linie den
Backend-Entwicklern dazu dienen, nach einem Deployment auf einer AWS-Stage (DEV, INT oder DEMO) diverse Anwendungsfälle zu testen, um sicherzustellen, dass
durch die letzten Änderungen am Code keine Fehler in der API entstanden sind. Dadurch wird auch der Funktionstest erleichtert, indem technische Fehler, im
Gegensatz zu fachlichen Fehlern, früh bemerkt werden, um so die Testaufwände gering zu halten.

Die Testskripte in diesem Ordner sind Shell-Skripte, die in Git Bash ausgeführt werden können. Die Aufrufe der API-Endpunkte erfolgt durch `cURL`-Kommandos. Der
prinzipielle Aufbau des Verzeichnisses ist wie folgt gestaltet:

```plantuml
@startsalt
{
{T
+ BE-EXTERNAL-TESTS
++ [endpoints]
+++ [editor]
++++ <API-Skripte>
+++ [plattform]
++++ <API-Skripte>
++ [tests]
+++ <Test-Skripte>
++ README.md
++ helpers.sh
++ run-tests.sh
++ secrets.sh.template
}
}
@endsalt
```

Auf der obersten Ebene befinden sich die Datei "README.md", das ist dieses Dokument, zwei Skripte und ein Template.

## secrets.sh.template

Das Template ist eine Vorlage für die Datei `secrets.sh`. Dieses Skript enthält Schlüssel und Verbindungs- bzw. Benutzerinformationen. Zum Zweck des
Datenschutzes und der allgemeinen Sicherheit der Codebasis sollen solche sensitiven Daten nicht im Git-Repository gespeichert werden. Daher ist die
Datei `secrets.sh` in `.gitignore` aufgeführt und muss vor Begin der Tests von jedem Entwickler selbst angelegt werden. Das geschieht durch **Kopieren** der
Datei `secrets.sh.template` und anschließendem Umbenennen, nicht durch Umbenennen der Vorlage selbst!

Sollte durch Merges oder Checkouts anderer Branches die Datei `secrets.sh` gelöscht werden, muss sie wieder manuell angelegt werden.

## helpers.sh

Das Skript beinhaltet wiederkehrend benutzte Funktionen, die in den API-Skripten und Test-Skripten verwendet werden.

### Logging

Das ist zum Einen ein Logging-Mechanismus, um beim Ausführen der Tests nach Log-Leveln unterscheidbare Meldungen in die Konsole zu schreiben. Dazu wird das
Log-Level der Mitteilung und anschließend der Text als Parameter angegeben. Ein Beispielaufruf wäre:

```
logThis "INFO" "... eine hilfreiche Mitteilung..."
```

Das Log-Level der Tests wird mit der Variablen `logging_level` festgelegt. Wurde das Logging Level mit `logging_level=X` festgelegt, wird eine
Log-Anweisung `logThis "Y" "..."` in der Konsole ausgegeben, wenn Y ≥ X ist, sonst nicht. Die zur Verfügung stehenden Log-Level sind:

```
ALL < TRACE < DEBUG < INFO < SUCCESS < WARN < FAILURE < ERROR < FATAL < OFF
```

Bis auf die Level `SUCCESS` und `FAILURE` sind das die bekannten Log-Level wie zum Beispiel
für [Apache Log4j](https://logging.apache.org/log4j/2.x/manual/customloglevels.html) beschrieben. `SUCCESS` wird für erfolgreiche Tests und `FAILURE` für
fehlgeschlagene Tests verwendet. Sie wurden als Log-Level eingeführt, um eventuelle Auswertungen der Logs zu erleichtern.

Für alle Log-Level außer `ALL` und `OFF` gibt es Convenience-Funktionen nach dem Muster `logInfo` oder `logError`, die nur die zu loggende Meldung als Parameter
erhalten, anstatt `logThis "INFO"` bzw. `logThis "ERROR"` schreiben zu müssen. `All` kann nur zum Setzen des Logging-Levels verwendet werden, um alle
Log-Ausgaben zu ermöglichen. Analog werden mit `OFF` alle Log-Ausgaben unterdrückt.

### Authentifizierung

Um einen der beiden Benutzer, die in der Datei `secrets.sh` definiert sind, auf der jeweiligen Stage gegen den IAM zu authentifizieren, muss ein JWToken
erstellt und für jeden API-Aufruf verwendet werden. Dazu verwendet man die Funktion `authenticate`. Je nachdem, mit welchen Benutzer man sich "anmelden" will,
muss ggf. ein Parameter mitgegeben werden:

- `authenticate ["ERSTELLER"]`: Ohne Parameter oder mit exakt dem Parameter "ERSTELLER" wird der Hauptanwender authentifiziert.
- `authenticate "..."`: Mit Parameter, der nicht gleich "ERSTELLER" ist, wird der Mitarbeiter angemeldet.

Nach erfolgreicher Authentifizierung steht das JWToken in der Variablen **`token`** zur Verfügung. Schlägt die Anmeldung fehl, wird eine Fehlermeldung in die
Konsole geschrieben und das Skript abgebrochen.

### Auswertung der API-Antworten (Responses)

Die Hilfsfunktion `extract` kann nach jedem API-Aufruf verwendet werden, um den HTTP-Status und den "Body" der Antwort zu erhalten. Sie erwartet keine
Parameter. Das setzt voraus, dass die Antwort ("Response) in der Variablen `response` gespeichert wird. Der Return Code steht danach in der Variablen *
*`responsecode`** und der Body, sofern vorhanden, in **`body`**.

## run-tests.sh

Dieses Skript orchestriert den Testablauf durch das Laden der verschiedenen Skripte in der richtigen Reihenfolge. Es ist **das** Skript zum Ausführen der Tests,
nach Erstellen einer validen `secrets.sh`-Datei.

Die Abfolge der Tätigkeiten ist:

1. Laden der Secrets
2. Laden der Helper
3. Setzen des Logging Levels
4. Laden der Funktionen zum Aufrufen der Endpunkte (im Verzeichnis "endpoints")
5. Nacheinanderausführung der Test-Suiten (im Verzeichnis "tests")

Das Anlegen von Testszenarien in verschiedenen Skripten, hat den Vorteil, dass einzelne dieser Szenarien auskommentiert werden können, zum Beispiel in einer
Debug-Session.

## Verzeichnis "endpoints"

In diesem Verzeichnis werden die Endpunkt-Aufrufe als parametrisierte Funktionen definiert. Die API ist aufgeteilt in (eine rudimentäre) Plattform-API,
Unterverzeichnis `plattform`, und mehrere Skripte für die Editor-API, im Unterverzeichnis `editor`. Die Editor-API wurde aus Pflegbarkeitsgründen analog zur
OpenAPI-Dokumentation ("Swagger UI") in Gruppen unterteilt.

Einige der API-Aufrufe stellen nach erfolgreicher Ausführung verschiedene Informationen in Variablen bereit. In jedem Fall können `responsecode` und 'body'
weiterverwendet werden. Nach Anlegen neuer Datenobjekte steht i.d.R. die jeweilige ID zur Verfügung. Beispiel:

Nach Aufruf der Funktion `regelungsvorhabenErstellen` (mit geeigneten Parametern) befindet sich im Erfolgsfall die ID des neuen Regelungsvorhabens in der
Variablen `regelungsvorhabenId`. Diese wird benötigt, um anschließend mit der Funktion `dokumentenMappeErstellen` eine neue Dokumentenmappe zu diesem
Regelungsvorhaben anzulegen. Wenn dies auch erfolgreich verläuft, hat man anschließend mit der Variablen 'dokumentenMappenId' eine Referenz auf diese Mappe.
Damit können wiederum Einzeldokumente in dieser Mappe angelegt werden, etc. etc.

## Verzeichnis "tests"

Dieses Verzeichnis enthält die Test-Suiten. Jede Test-Suite wird in ein eigenes Skript geschrieben. Diese Skripte starten mit der Authentifizierung durch Aufruf
von `authenticate [...]` und rufen dann die zu testenden Endpunkte auf. Nach jedem Endpunkt-Aufruf finden einige Sanity-Tests statt.

I.d.R. sind die Aufrufe in einer Suite verkettet, das heißt, die Ergebnisse bzw. Seiteneffekte (wie Speichern neuer Datenobjekte in der Datenbank) sind
Voraussetzung bzw. Eingabe eines nachfolgenden Aufrufs.

## Test User

Aktuell sind in der Parameterdatei `secrets.sh` zwei User-Profile vorgesehen: ein Hauptanwender und ein Mitarbeiter. Prinzipiell können dort nach Belieben
Benutzerinformationen eingetragen werden, wie sie beim IAM/Keycloak auf der jeweiligen Umgebung registriert sind.

Um die Editorseiten ("Tab1" bzw. "Tab2") nicht mit Daten der Integrationstests zu belasten, wurden für die AWS-Umgebungen (DEV, INT, DEMO) im gemeinsamen
Test-IAM zwei Benutzer wie folgt angelegt.

Bei der Übernahme der Daten in das o.g. Shell Skript muss darauf geachtet werden, Sonderzeichen in den Strings URI-konform zu encodieren; siehe
dazu [URL-Encoding](https://de.wikipedia.org/wiki/URL-Encoding).

## Aliasse

Die Funktionen zum [Aufruf der Endpunkte](#verzeichnis-endpoints) haben sprechende Namen, um ihre Funktion bzw. das erwartete Ergebnis zu beschreiben. Falls
gewünscht, können aber auch technische Aliasse verwendet werden, die den Endpunkt mehr oder weniger direkt benennen, nach dem
Muster `<HTTP-Methode>_<Pfad mit '_' anstelle von '/'>` in Kleinbuchstaben.

Zum Beispiel kann das Abrufen der Benutzerinformationen wahlweise mit einer der folgenden Varianten ausgeführt werden:

1. `angemeldetenBenutzerAnzeigen`
2. `get_user`

Die Aliasse verwenden dieselben Argumente.

