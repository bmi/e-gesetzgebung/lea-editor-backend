// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers.restful;

import de.itzbund.egesetz.bmi.lea.controllers.CompoundDocumentController;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.util.TestDTOUtil;
import org.junit.jupiter.api.Test;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CompoundDocumentModelAssemblerTest {

    @Test
    void testToModel() {
        CompoundDocumentDTO compoundDocumentDTO = TestDTOUtil.getRandomCompoundDocumentDTO();

        CompoundDocumentController controller = mock(CompoundDocumentController.class);
        when(controller.getByIdentifier(compoundDocumentDTO.getId())).thenReturn(null);

        CompoundDocumentModelAssembler assembler = new CompoundDocumentModelAssembler();
        EntityModel<CompoundDocumentDTO> entityModel = assembler.toModel(compoundDocumentDTO);

        Optional<Link> optionalLink = entityModel.getLink("self");
        if (optionalLink.isPresent()) {
            assertEquals("/compounddocuments/" + compoundDocumentDTO.getId(), optionalLink
                .get()
                .getHref());
        } else {
            fail("Link is missing");
        }

        optionalLink = entityModel.getLink("compounddocuments");
        if (optionalLink.isPresent()) {
            assertEquals("/compounddocuments", optionalLink
                .get()
                .getHref());
        } else {
            fail("Link is missing");
        }
    }
}
