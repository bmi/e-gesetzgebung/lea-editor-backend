// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.api.filter.ContentBase64Filter;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.PkpZuleitungDokumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.UpdateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtListDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.DocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.ExportRestPort;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationException;
import de.itzbund.egesetz.bmi.lea.export.document.MultipleDocumentDTO;
import de.itzbund.egesetz.bmi.lea.export.services.PdfService;
import de.itzbund.egesetz.bmi.lea.util.TestDTOUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

class PdfControllerTest {

	@Mock
	private PdfService pdfService;

	@Mock
	private ExportRestPort exportService;

	@Mock
	private DocumentRestPort documentService;

	@InjectMocks
	private PdfController pdfController;

	private DocumentDTO documentDTO;

	private UpdateDocumentDTO updateDocumentDTO;

	@Mock
	private ContentBase64Filter contentBase64Filter;

	private static final String userGid = UUID.randomUUID()
		.toString();

	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);
		String jsonContent = "[{\"xml-model\":\"href=\\\"Grammatiken\\/legalDocML.de.sch\\\" type=\\\"application\\/xml\\\" schematypens=\\\"http:\\/\\/purl.oclc.org\\/dsdl\\/schematron\\\"\",\"children\":[{\"children\":[{\"children\":[{\"children\":[{\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRthis\",\"value\":\"eli\\/dl\\/2023\\/bundesregierung\\/1\\/regelungsentwurf\\/vorblatt-1\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRuri\",\"value\":\"eli\\/dl\\/2023\\/bundesregierung\\/1\\/regelungsentwurf\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"übergreifende-id\",\"type\":\"akn:FRBRalias\",\"value\":\"55a9bbec-a625-4160-aa3e-ac28d7a397f7\"},{\"date\":\"2023-11-08\",\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"entwurfsfassung\",\"type\":\"akn:FRBRdate\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"href\":\"recht.bund.de\\/institution\\/bundesregierung\",\"type\":\"akn:FRBRauthor\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRcountry\",\"value\":\"de\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRnumber\",\"value\":\"1\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRname\",\"value\":\"regelungsentwurf\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRsubtype\",\"value\":\"vorblatt-1\"}],\"type\":\"akn:FRBRWork\"},{\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRthis\",\"value\":\"eli\\/dl\\/2023\\/bundesregierung\\/1\\/regelungsentwurf\\/bundestag\\/2023-04-01\\/1\\/deu\\/vorblatt-1\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRuri\",\"value\":\"eli\\/dl\\/2023\\/bundesregierung\\/1\\/regelungsentwurf\\/bundestag\\/2023-04-01\\/1\\/deu\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"href\":\"recht.bund.de\\/institution\\/bundestag\",\"type\":\"akn:FRBRauthor\"},{\"date\":\"2023-04-01\",\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"version\",\"type\":\"akn:FRBRdate\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"language\":\"deu\",\"type\":\"akn:FRBRlanguage\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRversionNumber\",\"value\":\"1\"}],\"type\":\"akn:FRBRExpression\"},{\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRthis\",\"value\":\"eli\\/dl\\/2023\\/bundesregierung\\/1\\/regelungsentwurf\\/bundestag\\/2023-04-01\\/1\\/deu\\/vorblatt-1.xml\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRuri\",\"value\":\"eli\\/dl\\/2023\\/bundesregierung\\/1\\/regelungsentwurf\\/bundestag\\/2023-04-01\\/1\\/deu\\/vorblatt-1.xml\"},{\"date\":\"2023-04-01\",\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"generierung\",\"type\":\"akn:FRBRdate\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"href\":\"recht.bund.de\",\"type\":\"akn:FRBRauthor\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRformat\",\"value\":\"xml\"}],\"type\":\"akn:FRBRManifestation\"}],\"source\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:identification\"},{\"children\":[{\"children\":[{\"children\":[{\"children\":[{\"text\":\"verordnung\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"meta:typ\"},{\"children\":[{\"children\":[{\"text\":\"mantelform\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"meta:form\"},{\"children\":[{\"children\":[{\"text\":\"entwurfsfassung\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"meta:fassung\"},{\"children\":[{\"children\":[{\"text\":\"vorblatt\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"meta:art\"},{\"children\":[{\"children\":[{\"text\":\"bundesregierung\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"meta:initiant\"},{\"children\":[{\"children\":[{\"text\":\"nicht-vorhanden\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"meta:bearbeitendeInstitution\"}],\"type\":\"meta:legalDocML.de_metadaten\",\"xmlns:meta\":\"http:\\/\\/Metadaten.LegalDocML.de\"}],\"source\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:proprietary\"}],\"type\":\"akn:meta\"},{\"children\":[{\"eId\":\"doktitel-1\",\"children\":[{\"eId\":\"doktitel-1_text-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"Referentenentwurf\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:docStage\"},{\"refersTo\":\"artikel\",\"children\":[{\"children\":[{\"text\":\" des \"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:span\"},{\"children\":[{\"children\":[{\"text\":\"Bundesministeriums des Innern und für Heimat\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:docProponent\"},{\"children\":[{\"children\":[{\"text\":\"08\\/11\\/23 - AK - RV (Bezeichnung)\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:docTitle\"},{\"children\":[{\"text\":\"(\"}],\"type\":\"lea:textWrapper\"},{\"eId\":\"doktitel-1_text-1_kurztitel-1\",\"children\":[{\"children\":[{\"text\":\"08\\/11\\/23 - AK - RV\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"52f446f3-d13d-4dc8-baf4-ab813dac9c1e\",\"type\":\"akn:shortTitle\"},{\"children\":[{\"text\":\" \\u2013 \"}],\"type\":\"lea:textWrapper\"},{\"refersTo\":\"amtliche-abkuerzung\",\"children\":[{\"children\":[{\"text\":\"08\\/11-AK-RV\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:inline\"},{\"children\":[{\"text\":\")\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"efb657dc-2ae1-4c7e-a54b-cff525e380fd\",\"type\":\"akn:p\"}],\"GUID\":\"974a760e-512d-4b94-8ff4-5d7bdb589d25\",\"type\":\"akn:longTitle\"}],\"type\":\"akn:preface\"},{\"children\":[{\"refersTo\":\"vorblattabschnitt-problem-und-ziel\",\"eId\":\"container-a\",\"children\":[{\"eId\":\"container-a_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"a\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"A.\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"5774186e-cdd3-447f-85f8-6ea6356a5014\",\"type\":\"akn:num\"},{\"eId\":\"container-a_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Problem und Ziel\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"8541808e-feec-4e3b-bab6-b770ed88a80a\",\"type\":\"akn:heading\"},{\"eId\":\"container-a_inhalt-1\",\"children\":[{\"eId\":\"container-a_inhalt-1_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"ab8bb046-8009-4eaa-bdc9-202d1cf55ff4\",\"type\":\"akn:p\"}],\"GUID\":\"b765fd4e-0d18-4d8e-a0e0-f989106d1b61\",\"type\":\"akn:content\"}],\"GUID\":\"b994a1c4-a1e4-4500-9121-6a44e9affe39\",\"name\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:hcontainer\"},{\"refersTo\":\"vorblattabschnitt-loesung\",\"eId\":\"container-b\",\"children\":[{\"eId\":\"container-b_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"b\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"B.\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"ab40bc76-9a80-4490-9de4-ee3fda74ebab\",\"type\":\"akn:num\"},{\"eId\":\"container-b_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Lösung\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"8f0557fd-46e4-446d-bb71-e3292daaa0e9\",\"type\":\"akn:heading\"},{\"eId\":\"container-b_inhalt-1\",\"children\":[{\"eId\":\"container-b_inhalt-1_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"0a429181-c0a7-4520-ac09-82a48b8667ef\",\"type\":\"akn:p\"}],\"GUID\":\"37624084-5297-429f-8f67-1db7fd010d0b\",\"type\":\"akn:content\"}],\"GUID\":\"493cd5c2-edd6-4493-bfa8-ac93a59621d1\",\"name\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:hcontainer\"},{\"refersTo\":\"vorblattabschnitt-alternativen\",\"eId\":\"container-c\",\"children\":[{\"eId\":\"container-c_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"c\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"C.\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"4d35aef3-0980-43df-9a25-5629946f9cd8\",\"type\":\"akn:num\"},{\"eId\":\"container-c_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Alternativen\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"2b79f2fa-49ed-4c68-8df2-683a61f5944e\",\"type\":\"akn:heading\"},{\"eId\":\"container-c_inhalt-1\",\"children\":[{\"eId\":\"container-c_inhalt-1_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"72feb7d0-c3a1-4ff3-b49d-2a8fc2f5fe17\",\"type\":\"akn:p\"}],\"GUID\":\"455de1a9-804b-4b0d-b43e-c352bb84c305\",\"type\":\"akn:content\"}],\"GUID\":\"501e0ddc-8781-499b-a8b7-5647a69f092a\",\"name\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:hcontainer\"},{\"refersTo\":\"vorblattabschnitt-haushaltsausgaben-ohne-erfuellungsaufwand\",\"eId\":\"container-d\",\"children\":[{\"eId\":\"container-d_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"d\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"D.\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"56ddc42b-f46b-4c15-8dca-dcbc27ca84ee\",\"type\":\"akn:num\"},{\"eId\":\"container-d_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Haushaltsausgaben ohne Erfüllungsaufwand\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"a6fb8cda-8857-4fda-bdaa-510c46f34c0e\",\"type\":\"akn:heading\"},{\"eId\":\"container-d_inhalt-1\",\"children\":[{\"eId\":\"container-d_inhalt-1_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"e8e0f121-d651-4233-aced-262b1c51e759\",\"type\":\"akn:p\"}],\"GUID\":\"d86be628-f8f7-4fed-8042-2beefbd89e8c\",\"type\":\"akn:content\"}],\"GUID\":\"fc3f849f-4979-4163-a305-c0266b3aee99\",\"name\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:hcontainer\"},{\"refersTo\":\"vorblattabschnitt-erfuellungsaufwand\",\"eId\":\"container-e\",\"children\":[{\"eId\":\"container-e_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"e\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"E.\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"b8edfeb9-7651-48cc-8bc4-062ff24b6aa1\",\"type\":\"akn:num\"},{\"eId\":\"container-e_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Erfüllungsaufwand\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"f3000f98-5cc8-419e-af8d-7ebb8ec752d3\",\"type\":\"akn:heading\"},{\"eId\":\"container-e_inhalt-1\",\"children\":[{\"eId\":\"container-e_inhalt-1_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"b24167ac-050a-4ac9-ada1-f2aeaf799908\",\"type\":\"akn:p\"},{\"refersTo\":\"erfuellungsaufwand-fuer-buergerinnen-und-buerger\",\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.1\",\"children\":[{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.1_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"e.1\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"E.1\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"011322f7-e001-4222-93a8-cfffaff2f3c8\",\"type\":\"akn:num\"},{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.1_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Erfüllungsaufwand für Bürgerinnen und Bürger\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"5090e7b4-9657-43de-bc8f-3e7e40f394ff\",\"type\":\"akn:heading\"},{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.1_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"524775c5-3a81-4c0c-a15a-be06fec416c7\",\"type\":\"akn:p\"}],\"GUID\":\"28ae155a-c6c2-473a-9940-b2e75d22f12f\",\"type\":\"akn:tblock\"},{\"refersTo\":\"erfuellungsaufwand-fuer-die-wirtschaft\",\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.2\",\"children\":[{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.2_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"e.2\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"E.2\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"e464451e-32ff-41f0-a5cb-64e946ca1ec7\",\"type\":\"akn:num\"},{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.2_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Erfüllungsaufwand für die Wirtschaft\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"e684809d-0dec-4652-9938-b507fdcc06e0\",\"type\":\"akn:heading\"},{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.2_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"3ebfea81-9519-4d40-864c-dd7ef55a7d11\",\"type\":\"akn:p\"},{\"refersTo\":\"davon-buerokratiekosten-aus-informationspflichten\",\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.2_inhaltabschnitt-1\",\"children\":[{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.2_inhaltabschnitt-1_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Davon Bürokratiekosten aus Informationspflichten\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"95d23084-86ad-4abd-810c-ec2a757a2912\",\"type\":\"akn:heading\"},{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.2_inhaltabschnitt-1_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"1d64c76e-cd11-4a71-a71a-2ef5bab0ff20\",\"type\":\"akn:p\"}],\"GUID\":\"167bca08-c828-48f2-8387-02b9401d5e67\",\"type\":\"akn:tblock\"}],\"GUID\":\"4b187204-187d-4841-9ca7-526c88f4dff0\",\"type\":\"akn:tblock\"},{\"refersTo\":\"erfuellungsaufwand-der-verwaltung\",\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.3\",\"children\":[{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.3_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"e.3\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"E.3\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"c4f1ac1e-f41b-4912-b399-6794a613fb3e\",\"type\":\"akn:num\"},{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.3_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Erfüllungsaufwand der Verwaltung\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"9e9dad4b-6728-4fe8-9a52-1e8e2b6b6547\",\"type\":\"akn:heading\"},{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.3_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"f33f03d8-ed43-44a4-a371-b4d87ff9e0f6\",\"type\":\"akn:p\"}],\"GUID\":\"bfa296c6-3b90-4c36-91ce-72d688c695ce\",\"type\":\"akn:tblock\"}],\"GUID\":\"1e148fc9-d2b8-46a6-a454-f6f85499e141\",\"type\":\"akn:content\"}],\"GUID\":\"d647a971-5150-41b6-aed7-d93fb5639370\",\"name\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:hcontainer\"},{\"refersTo\":\"vorblattabschnitt-weitere-kosten\",\"eId\":\"container-f\",\"children\":[{\"eId\":\"container-f_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"f\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"F.\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"35402d8a-db0c-484d-b3e9-cd1ec7fd5f61\",\"type\":\"akn:num\"},{\"eId\":\"container-f_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Weitere Kosten\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"d7e1cd08-e936-4445-9a1c-0c04b56d2081\",\"type\":\"akn:heading\"},{\"eId\":\"container-f_inhalt-1\",\"children\":[{\"eId\":\"container-f_inhalt-1_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"ae500a9a-fe77-4cff-9aa7-31e1b4fba50f\",\"type\":\"akn:p\"},{\"lea:editable\":true,\"children\":[{\"lea:editable\":true,\"children\":[{\"children\":[{\"text\":\"Sonstige Kosten\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"bacd79cf-a900-4a6d-bf7e-20a34a537127\",\"type\":\"akn:heading\"},{\"children\":[{\"children\":[{\"text\":\"Tesz\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"1e0c00d0-8dd7-447a-8637-7b55cd13b612\",\"type\":\"akn:p\"}],\"GUID\":\"ca96d012-71e4-4067-ad22-43c71e48b8be\",\"type\":\"akn:tblock\"}],\"GUID\":\"6bd87d8a-5330-4bc2-8ee0-684e173490c0\",\"type\":\"akn:content\"}],\"GUID\":\"a4c09084-2701-4f80-9438-5801b145016c\",\"name\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:hcontainer\"}],\"type\":\"akn:mainBody\"}],\"name\":\"vorblatt\",\"type\":\"akn:doc\"}],\"xsi:schemaLocation\":\"http:\\/\\/Inhaltsdaten.LegalDocML.de\\/1.4\\/ Grammatiken\\/legalDocML.de-vorblatt.xsd http:\\/\\/Metadaten.LegalDocML.de\\/1.4\\/ Grammatiken\\/legalDocML.de-metadaten.xsd\",\"xmlns:xsi\":\"http:\\/\\/www.w3.org\\/2001\\/XMLSchema-instance\",\"type\":\"akn:akomaNtoso\",\"xmlns:akn\":\"http:\\/\\/Inhaltsdaten.LegalDocML.de\\/1.4\\/\"}]";
		documentDTO = new DocumentDTO();
		documentDTO.setContent(jsonContent);
		documentDTO.setType(DocumentType.VORBLATT_STAMMGESETZ);
		documentDTO.setTitle("document");

		updateDocumentDTO = new UpdateDocumentDTO();
		updateDocumentDTO.setContent(
			"[{\"xml-model\":\"href=\\\"Grammatiken\\/legalDocML.de.sch\\\" type=\\\"application\\/xml\\\" schematypens=\\\"http:\\/\\/purl.oclc.org\\/dsdl\\/schematron\\\"\",\"children\":[{\"children\":[{\"children\":[{\"children\":[{\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRthis\",\"value\":\"eli\\/dl\\/2024\\/bundesregierung\\/1\\/regelungsentwurf\\/regelungstext-1\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRuri\",\"value\":\"eli\\/dl\\/2024\\/bundesregierung\\/1\\/regelungsentwurf\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"übergreifende-id\",\"type\":\"akn:FRBRalias\",\"value\":\"122501b6-baee-4fbd-bb19-ab0562f1bdd2\"},{\"date\":\"2024-03-06\",\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"entwurfsfassung\",\"type\":\"akn:FRBRdate\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"href\":\"recht.bund.de\\/institution\\/bundesregierung\",\"type\":\"akn:FRBRauthor\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRcountry\",\"value\":\"de\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRnumber\",\"value\":\"1\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRname\",\"value\":\"regelungsentwurf\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRsubtype\",\"value\":\"regelungstext-1\"}],\"type\":\"akn:FRBRWork\"},{\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRthis\",\"value\":\"eli\\/dl\\/2024\\/bundesregierung\\/1\\/regelungsentwurf\\/bundesregierung\\/2024-03-06\\/1\\/deu\\/regelungstext-1\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRuri\",\"value\":\"eli\\/dl\\/2024\\/bundesregierung\\/1\\/regelungsentwurf\\/bundesregierung\\/2024-03-06\\/1\\/deu\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"href\":\"recht.bund.de\\/institution\\/bundesregierung\",\"type\":\"akn:FRBRauthor\"},{\"date\":\"2024-03-06\",\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"version\",\"type\":\"akn:FRBRdate\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"language\":\"deu\",\"type\":\"akn:FRBRlanguage\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRversionNumber\",\"value\":\"1\"}],\"type\":\"akn:FRBRExpression\"},{\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRthis\",\"value\":\"eli\\/dl\\/2024\\/bundesregierung\\/1\\/regelungsentwurf\\/bundesregierung\\/2024-03-06\\/1\\/deu\\/regelungstext-1.xml\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRuri\",\"value\":\"eli\\/dl\\/2024\\/bundesregierung\\/1\\/regelungsentwurf\\/bundesregierung\\/2024-03-06\\/1\\/deu\\/regelungstext-1.xml\"},{\"date\":\"2024-03-06\",\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"generierung\",\"type\":\"akn:FRBRdate\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"href\":\"recht.bund.de\",\"type\":\"akn:FRBRauthor\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRformat\",\"value\":\"xml\"}],\"type\":\"akn:FRBRManifestation\"}],\"source\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:identification\"},{\"children\":[{\"children\":[{\"children\":[{\"children\":[{\"text\":\"gesetz\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"meta:typ\"},{\"children\":[{\"children\":[{\"text\":\"stammform\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"meta:form\"},{\"children\":[{\"children\":[{\"text\":\"entwurfsfassung\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"meta:fassung\"},{\"children\":[{\"children\":[{\"text\":\"regelungstext\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"meta:art\"},{\"children\":[{\"children\":[{\"text\":\"bundesregierung\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"meta:initiant\"},{\"children\":[{\"children\":[{\"text\":\"bundesregierung\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"meta:bearbeitendeInstitution\"}],\"type\":\"meta:legalDocML.de_metadaten\",\"xmlns:meta\":\"http:\\/\\/Metadaten.LegalDocML.de\"}],\"source\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:proprietary\"}],\"type\":\"akn:meta\"},{\"children\":[{\"eId\":\"doktitel-1\",\"children\":[{\"eId\":\"doktitel-1_text-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"Referentenentwurf\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:docStage\"},{\"refersTo\":\"artikel\",\"children\":[{\"children\":[{\"text\":\" des \"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:span\"},{\"children\":[{\"children\":[{\"text\":\"nicht vorhanden\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:docProponent\"},{\"children\":[{\"children\":[{\"text\":\"nicht vorhanden\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:docTitle\"},{\"children\":[{\"text\":\"(\"}],\"type\":\"lea:textWrapper\"},{\"eId\":\"doktitel-1_text-1_kurztitel-1\",\"children\":[{\"children\":[{\"text\":\"nicht vorhanden\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"5a34ee9d-bef0-4b0f-985c-770c9693a951\",\"type\":\"akn:shortTitle\"},{\"children\":[{\"text\":\" \\u2013 \"}],\"type\":\"lea:textWrapper\"},{\"refersTo\":\"amtliche-abkuerzung\",\"children\":[{\"children\":[{\"text\":\"nicht vorhanden\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:inline\"},{\"children\":[{\"text\":\")\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"e3998b08-0bcd-4265-b486-052c3394416a\",\"type\":\"akn:p\"}],\"GUID\":\"1caa42fe-bf60-4f4e-b9da-af1020c1c02c\",\"type\":\"akn:longTitle\"},{\"eId\":\"block-1\",\"children\":[{\"refersTo\":\"ausfertigung-datum\",\"date\":\"1900-01-01\",\"children\":[{\"children\":[{\"text\":\"Vom \\u2026\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:date\"}],\"GUID\":\"04254c54-7dcd-48bb-b3a4-d0ecc7022fe2\",\"name\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:block\"}],\"type\":\"akn:preface\"},{\"children\":[{\"refersTo\":\"eingangsformel\",\"eId\":\"formel-1\",\"children\":[{\"eId\":\"formel-1_text-1\",\"children\":[{\"children\":[{\"text\":\"Der Bundestag hat das folgende Gesetz beschlossen:\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"7c6d15f8-f2bc-45f0-b338-81f611b63cd1\",\"type\":\"akn:p\"}],\"GUID\":\"fe9afaa7-6ca6-41ec-be01-3b70811c4bb1\",\"name\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:formula\"}],\"type\":\"akn:preamble\"},{\"refersTo\":\"hauptteil-stammform\",\"children\":[{\"eId\":\"abschnitt-1\",\"children\":[{\"eId\":\"abschnitt-1_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"1\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"Abschnitt 1\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"fe3f18eb-5cde-4d93-92b9-8d614bddc98b\",\"type\":\"akn:num\"},{\"eId\":\"abschnitt-1_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"abe5b3f1-c6ed-40e1-a2a9-4bb3cbc2111d\",\"type\":\"akn:heading\"},{\"refersTo\":\"einzelvorschrift-stammform\",\"eId\":\"para-1\",\"children\":[{\"eId\":\"para-1_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"1\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"§ 1\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"a62e30d4-2f5a-4206-b2b8-a0c4e133f619\",\"type\":\"akn:num\"},{\"eId\":\"para-1_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"c4ac763f-b207-44a3-8b64-19c928feb426\",\"type\":\"akn:heading\"},{\"eId\":\"para-1_abs-1\",\"children\":[{\"eId\":\"para-1_abs-1_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"1\",\"type\":\"akn:marker\"}],\"GUID\":\"20125b2f-b128-450c-b0e5-738d1f895c1f\",\"type\":\"akn:num\"},{\"eId\":\"para-1_abs-1_inhalt-1\",\"children\":[{\"eId\":\"para-1_abs-1_inhalt-1_text-1\",\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"76c2d080-cc79-42d0-8562-ddba58e6aa23\",\"type\":\"akn:p\"}],\"GUID\":\"2271c3a7-8b10-4e78-89fa-031ed6dcdf9d\",\"type\":\"akn:content\"}],\"GUID\":\"3df85d08-db67-4f4f-b393-c3cd2fd0647c\",\"type\":\"akn:paragraph\"}],\"GUID\":\"ed7054cf-555d-46a6-829f-9b2c189f476e\",\"type\":\"akn:article\"},{\"refersTo\":\"einzelvorschrift-stammform\",\"eId\":\"para-2\",\"children\":[{\"eId\":\"para-2_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"2\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"§ 2\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"857b5a4c-eaf1-4991-bd47-a203e6df9f35\",\"type\":\"akn:num\"},{\"eId\":\"para-2_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"cf504c48-28b4-4ba9-b28c-f0ee8088e67c\",\"type\":\"akn:heading\"},{\"eId\":\"para-2_abs-1\",\"children\":[{\"eId\":\"para-2_abs-1_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"1\",\"type\":\"akn:marker\"}],\"GUID\":\"e4e0a3e4-3219-4720-a884-3d32debb6755\",\"type\":\"akn:num\"},{\"eId\":\"para-2_abs-1_inhalt-1\",\"children\":[{\"eId\":\"para-2_abs-1_inhalt-1_text-1\",\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"7409f2f3-ffcb-4287-9fa2-96228250d240\",\"type\":\"akn:p\"}],\"GUID\":\"963d3f7a-1210-4c15-bd1a-85d64f6a50ae\",\"type\":\"akn:content\"}],\"GUID\":\"7d270988-3113-45e5-9d72-c4fee5ed6ed7\",\"type\":\"akn:paragraph\"}],\"GUID\":\"83f7bc6a-6f73-43cf-9f6e-8a37aad6df56\",\"type\":\"akn:article\"}],\"GUID\":\"9456c9d0-d11e-4428-add6-e5e6b14952e4\",\"type\":\"akn:section\"},{\"eId\":\"abschnitt-2\",\"children\":[{\"eId\":\"abschnitt-2_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"2\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"Abschnitt 2\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"5d89d5bf-6751-4caa-9ed0-3d369cb173c5\",\"type\":\"akn:num\"},{\"eId\":\"abschnitt-2_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"9f9c23ed-00ac-4023-b402-3b555cd2b2df\",\"type\":\"akn:heading\"},{\"refersTo\":\"einzelvorschrift-stammform\",\"eId\":\"para-3\",\"children\":[{\"eId\":\"para-3_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"3\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"§ 3\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"e6825bd8-8f68-4e02-8931-dc7d8ff58e7a\",\"type\":\"akn:num\"},{\"eId\":\"para-3_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"83bf2ef1-98ff-4ff5-9a93-77d5085c3fc5\",\"type\":\"akn:heading\"},{\"eId\":\"para-3_abs-1\",\"children\":[{\"eId\":\"para-3_abs-1_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"1\",\"type\":\"akn:marker\"}],\"GUID\":\"d501db55-7b4f-4935-8847-a30ac90b39ee\",\"type\":\"akn:num\"},{\"eId\":\"para-3_abs-1_inhalt-1\",\"children\":[{\"eId\":\"para-3_abs-1_inhalt-1_text-1\",\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"6f40bec2-ef23-477b-ace5-f3cd391ad66a\",\"type\":\"akn:p\"}],\"GUID\":\"32f950e1-4300-4a17-bfd0-e2e60bade37e\",\"type\":\"akn:content\"}],\"GUID\":\"3f6c07ea-3bac-4ac6-85aa-4e2e83b42864\",\"type\":\"akn:paragraph\"}],\"GUID\":\"9239b660-14a7-4927-83b3-fe9fd55483f0\",\"type\":\"akn:article\"},{\"refersTo\":\"einzelvorschrift-stammform|geltungszeitregel\",\"eId\":\"para-4\",\"children\":[{\"eId\":\"para-4_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"4\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"§ 4\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"288a5e25-caac-4ec5-89b9-19eb80ccb116\",\"type\":\"akn:num\"},{\"eId\":\"para-4_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"57b56069-9a85-4ec9-a31f-e4272d0b2c2d\",\"type\":\"akn:heading\"},{\"eId\":\"para-4_abs-1\",\"children\":[{\"eId\":\"para-4_abs-1_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"1\",\"type\":\"akn:marker\"}],\"GUID\":\"9fae3495-df0e-4566-8af4-c51fb293e573\",\"type\":\"akn:num\"},{\"eId\":\"para-4_abs-1_inhalt-1\",\"children\":[{\"eId\":\"para-4_abs-1_inhalt-1_text-1\",\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"5ba42d5e-c23c-47fe-91e7-59648b3b39ed\",\"type\":\"akn:p\"}],\"GUID\":\"68b1d31e-e98c-4cfa-bba7-4695174867a1\",\"type\":\"akn:content\"}],\"GUID\":\"6fb700fc-01ca-4134-84fd-beae608fafff\",\"type\":\"akn:paragraph\"}],\"GUID\":\"2cd60d42-63b0-470e-9bc0-e1e2a0ab81df\",\"type\":\"akn:article\"}],\"GUID\":\"823e0b56-b003-40da-a3f3-313559c57ffb\",\"type\":\"akn:section\"}],\"type\":\"akn:body\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:conclusions\"}],\"name\":\"regelungstext\",\"type\":\"akn:bill\"}],\"xsi:schemaLocation\":\"http:\\/\\/Inhaltsdaten.LegalDocML.de\\/1.4\\/ Grammatiken\\/legalDocML.de-regelungstextentwurfsfassung.xsd http:\\/\\/Metadaten.LegalDocML.de\\/1.4\\/ Grammatiken\\/legalDocML.de-metadaten.xsd\",\"xmlns:xsi\":\"http:\\/\\/www.w3.org\\/2001\\/XMLSchema-instance\",\"type\":\"akn:akomaNtoso\",\"xmlns:akn\":\"http:\\/\\/Inhaltsdaten.LegalDocML.de\\/1.4\\/\"}]");
		updateDocumentDTO.setTitle("document");
		updateDocumentDTO.setHdr(false);
	}

	@Test
	void testExportPdf() throws IOException, ValidationException {
		UUID documentId = UUID.randomUUID();
		byte[] pdfContent = "My PDF Content".getBytes();
		InputStream mockInputStream = new ByteArrayInputStream(pdfContent);

		HttpHeaders expectedHeaders = new HttpHeaders();
		expectedHeaders.add("content-disposition", "attachment; filename=document.pdf");
		expectedHeaders.add("Content-Type", "application/pdf");

		when(pdfService.convertDocumentToInputStream(any(MultipleDocumentDTO.class))).thenReturn(mockInputStream);
		when(documentService.getDocumentById(any(UUID.class), anyBoolean())).thenReturn(documentDTO);

		ResponseEntity<byte[]> responseEntity = pdfController.exportPdf(documentId);

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(expectedHeaders, responseEntity.getHeaders());
		assertArrayEquals(pdfContent, responseEntity.getBody());
	}

	@Test
	public void testExportPdfIOException() throws IOException, ValidationException {
		UUID documentId = UUID.randomUUID();
		when(documentService.getDocumentById(any(UUID.class), anyBoolean())).thenReturn(documentDTO);
		when(pdfService.convertDocumentToInputStream(any(MultipleDocumentDTO.class))).thenThrow(IOException.class);

		ResponseEntity<byte[]> responseEntity = pdfController.exportPdf(documentId);
		assertEquals(HttpStatus.CONFLICT, responseEntity.getStatusCode());
	}

	@Test
	void testExportSynopsisPdf() throws IOException {
		byte[] pdfContent = "My PDF Content".getBytes();
		InputStream mockInputStream = new ByteArrayInputStream(pdfContent);

		HttpHeaders expectedHeaders = new HttpHeaders();
		expectedHeaders.add("content-disposition", "attachment; filename=document.pdf");
		expectedHeaders.add("Content-Type", "application/pdf");

		when(contentBase64Filter.decodeUpdateDocumentDTO(any(UpdateDocumentDTO.class))).thenReturn(updateDocumentDTO);
		when(pdfService.convertXmlToInputStream(any(String.class), any(Boolean.class), any(String.class), any(String.class))).thenReturn(mockInputStream);
		when(exportService.exportSingleDocument(any(String.class), any(ArrayList.class))).thenReturn(readFileSpringBootWay("/xml/vorblatt.xml"));

		ResponseEntity<byte[]> responseEntity = pdfController.exportSynopsisPdf(updateDocumentDTO);

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(expectedHeaders, responseEntity.getHeaders());
		assertArrayEquals(pdfContent, responseEntity.getBody());
	}

	private String readFileSpringBootWay(String filePath) throws IOException {
		Resource resource = new ClassPathResource(filePath);
		assertNotNull(resource, "Die Ressource konnte nicht gefunden werden: " + filePath);

		String result = "";
		try (InputStream inputStream = resource.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {

			String line;
			while ((line = reader.readLine()) != null) {
				result = result + line;
			}
		}
		return result;
	}

	@Test
	void updateDrucksacheByCompoundDocumentIdTest() {

		// - Mock Service
		doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			((ArrayList<ServiceState>) args[1]).add(ServiceState.OK);
			return null;
		}).when(pdfService)
			.updateDrucksacheForCompoundDocumentId(any(), any());

		// Act
		ResponseEntity<List<BestandsrechtListDTO>> response = pdfController.updateDrucksache(UUID.randomUUID());

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	void updateDrucksacheByCompoundDocumentIdTest2() {

		// - Mock Service
		doAnswer(invocation -> {
			return null;
		}).when(pdfService)
			.updateDrucksacheForCompoundDocumentId(any(), any());

		// Act
		ResponseEntity<List<BestandsrechtListDTO>> response = pdfController.updateDrucksache(UUID.randomUUID());

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}

	@Test
	void updateDrucksacheByCompoundDocumentIdTest3() {

		// - Mock Service
		doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			((ArrayList<ServiceState>) args[1]).add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
			return null;
		}).when(pdfService)
			.updateDrucksacheForCompoundDocumentId(any(), any());

		// Act
		ResponseEntity<List<BestandsrechtListDTO>> response = pdfController.updateDrucksache(UUID.randomUUID());

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	void getDrucksacheByCompoundDocumentIdTest1() {

		// - Mock Service
		doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			((ArrayList<ServiceState>) args[1]).add(ServiceState.UNKNOWN_ERROR);
			return null;
		}).when(pdfService)
			.getDrucksacheForCompoundDocumentId(any(), any());

		// Act
		ResponseEntity<byte[]> response = pdfController.getDrucksache(UUID.randomUUID());

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}

	@Test
	void getDrucksacheByCompoundDocumentIdTest2() {

		// - Mock Service
		doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			((ArrayList<ServiceState>) args[1]).add(ServiceState.OK);
			return null;
		}).when(pdfService)
			.getDrucksacheForCompoundDocumentId(any(), any());

		// Act
		ResponseEntity<byte[]> response = pdfController.getDrucksache(UUID.randomUUID());

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	void whenDokumentenmappenFuerKabinettverfahren_TheyShouldBeReturned() {
		// Arrange
		List<ServiceState> states = new ArrayList<>();
		PkpZuleitungDokumentDTO randomPkpZuleitungDokumentDTO = TestDTOUtil.getRandomPkpZuleitungDokumentDTO();

		when(pdfService.getCompoundDocumentsForPKP(anyString(), any(RegelungsVorhabenId.class),
			anyList())).thenAnswer(invocation ->
			{
				List<ServiceState> status = invocation.getArgument(2);
				status.add(ServiceState.OK);
				return List.of(randomPkpZuleitungDokumentDTO);
			}
		);

		// Act
		ResponseEntity<List<PkpZuleitungDokumentDTO>> actual = pdfController.getCompoundDocumentForKabinett(
			userGid, UUID.randomUUID());

		// Assert
		assertThat(actual.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
		List<PkpZuleitungDokumentDTO> body = actual.getBody();
		assertThat(body).hasSize(1);
		assertThat(body.get(0)).isEqualTo(randomPkpZuleitungDokumentDTO);
	}


	@Test
	void whenDokumentenmappenFuerKabinettverfahrenFehlt_EinLeeresErgebnisSollZurueckKommen() {
		// Arrange
		List<ServiceState> states = new ArrayList<>();

		when(pdfService.getCompoundDocumentsForPKP(anyString(), any(RegelungsVorhabenId.class),
			anyList())).thenAnswer(invocation ->
			{
				List<ServiceState> status = invocation.getArgument(2);
				status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
				return List.of();
			}
		);

		// Act
		ResponseEntity<List<PkpZuleitungDokumentDTO>> actual = pdfController.getCompoundDocumentForKabinett(
			userGid, UUID.randomUUID());

		// Assert
		assertThat(actual.getStatusCode()).isEqualByComparingTo(HttpStatus.NO_CONTENT);
		assertThat(actual.getBody()).isNull();
	}

}