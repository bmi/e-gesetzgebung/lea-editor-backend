// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers.security;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.EditorRollenUndRechte;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CreateCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class CustomPermissionEvaluatorTest {

    @InjectMocks
    private CustomPermissionEvaluator customPermissionEvaluator;

    @Mock
    private EditorRollenUndRechte editorRollenUndRechte;

    @Mock
    private ApplicationContext applicationContext;

    @Mock
    private Document document;

    @Mock
    private LeageSession session;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    void testHasPermissionToDokumentenmappen() {
        // Arrange
        UUID resourceId = UUID.randomUUID();

        // Mock Authentication
        GrantedAuthority authority = new SimpleGrantedAuthority("DOKUMENTENMAPPE_READ");
        Authentication authentication = new TestAuthentication(authority);
        String gid = TestAuthentication.PERMISSION_UUID.toString();

        // Mock editorRollenUndRechte
        when(session.getUser()).thenReturn(User.builder().gid(new UserId(gid)).build());
        when(editorRollenUndRechte.isAllowed(gid, "READ", "DOKUMENTENMAPPE",
            resourceId.toString())).thenReturn(true);

        boolean hasPermission = customPermissionEvaluator.hasPermission(authentication, resourceId,
            "DOKUMENTENMAPPE",
            "READ");

        assertTrue(hasPermission);
    }


    @Test
    void testHasPermissionToRegelungsvorhaben() {
        // Arrange
        UUID resourceId = UUID.randomUUID();
        CreateCompoundDocumentDTO createCompoundDocumentDTO = TestDTOUtil.getRandomCreateCompoundDocumentDTO();
        createCompoundDocumentDTO.setRegelungsVorhabenId(resourceId.toString());

        // Mock Authentication
        GrantedAuthority authority = new SimpleGrantedAuthority("REGELUNGSVORHABEN_READ");
        Authentication authentication = new TestAuthentication(authority);
        String gid = TestAuthentication.PERMISSION_UUID.toString();

        // Mock editorRollenUndRechte
        when(session.getUser()).thenReturn(User.builder().gid(new UserId(gid)).build());
        when(editorRollenUndRechte.isAllowed(gid, "READ", "REGELUNGSVORHABEN",
            resourceId.toString())).thenReturn(true);

        boolean hasPermission = customPermissionEvaluator.hasPermission(authentication, resourceId.toString(),
            "REGELUNGSVORHABEN",
            "READ");

        assertTrue(hasPermission);
    }


    @Test
    void testHasPermissionToHRA() {
        // Arrange
        UUID resourceId = UUID.randomUUID();
        CreateCompoundDocumentDTO createCompoundDocumentDTO = TestDTOUtil.getRandomCreateCompoundDocumentDTO();
        createCompoundDocumentDTO.setRegelungsVorhabenId(resourceId.toString());

        // Mock Authentication
        GrantedAuthority authority = new SimpleGrantedAuthority("REGELUNGSVORHABEN_READ");
        Authentication authentication = new TestAuthentication(authority);
        when(session.getUser()).thenReturn(User.builder().gid(new UserId("X")).build());

        boolean hasPermission = customPermissionEvaluator.hasPermission(authentication, resourceId,
            "HRA",
            "READ");

        assertFalse(hasPermission);
    }


    @Test
    void testHasPermissionToDokumenteAsOwner() {
        // Arrange
        UUID resourceId = UUID.randomUUID();

        // Mock Authentication
        GrantedAuthority authority = new SimpleGrantedAuthority("DOKUMENTEN_READ");
        Authentication authentication = new TestAuthentication(authority);
        String gid = TestAuthentication.PERMISSION_UUID.toString();

        // Mock editorRollenUndRechte
        when(session.getUser()).thenReturn(User.builder().gid(new UserId(gid)).build());
        when(editorRollenUndRechte.isAllowed(gid, "READ", "DOKUMENTE",
            resourceId.toString())).thenReturn(true);

        boolean hasPermission = customPermissionEvaluator.hasPermission(authentication, resourceId,
            "DOKUMENTE",
            "READ");

        assertTrue(hasPermission);
    }


    @Test
    void testHasPermissionToDokumenteAsForeigner() {
        // Arrange
        CompoundDocumentId compoundDocumentId = new CompoundDocumentId(UUID.randomUUID());

        // Mock applicationContext & Document
        when(applicationContext.getBean(Document.class)).thenReturn(document);
        when(document.getContainingCompoundDocumentId(any(DocumentId.class))).thenReturn(
            compoundDocumentId);

        // Mock Authentication
        GrantedAuthority authority = new SimpleGrantedAuthority("DOKUMENTEN_READ");
        Authentication authentication = new TestAuthentication(authority);
        String gid = TestAuthentication.PERMISSION_UUID.toString();

        // Mock editorRollenUndRechte
        when(session.getUser()).thenReturn(User.builder().gid(new UserId(gid)).build());
        when(editorRollenUndRechte.isAllowed(gid, "KOMMENTIEREN",
            "DOKUMENTENMAPPE",
            compoundDocumentId.getId().toString())).thenReturn(true);

        boolean hasPermission = customPermissionEvaluator.hasPermission(authentication, UUID.randomUUID(),
            "DOKUMENTE",
            "KOMMENTIEREN");

        assertTrue(hasPermission);
    }


    @Test
    void testHasPermissionToDokumenteAsForeignerUnknownPermission() {
        // Arrange

        // Mock Authentication
        GrantedAuthority authority = new SimpleGrantedAuthority("DOKUMENTEN_READ");
        Authentication authentication = new TestAuthentication(authority);
        when(session.getUser()).thenReturn(User.builder().gid(new UserId("X")).build());

        boolean hasPermission = customPermissionEvaluator.hasPermission(authentication, UUID.randomUUID(),
            "DOKUMENTE",
            "IRGENDWAS");

        assertFalse(hasPermission);
    }


    @Test
    void testHasNoPermissionWithSomethingUndefined() {
        // Arrange
        UUID resourceId = UUID.randomUUID();

        // Mock Authentication
        GrantedAuthority authority = new SimpleGrantedAuthority("DOKUMENTEN_READ");
        Authentication authentication = new TestAuthentication(authority);
        when(session.getUser()).thenReturn(User.builder().gid(new UserId("X")).build());

        boolean hasPermission = customPermissionEvaluator.hasPermission(authentication, resourceId,
            "UNDEFINED",
            "READ");

        assertFalse(hasPermission);
    }


    @Test
    void testHasPermissionWithInvalidPrivilege() {
        // Mock Authentication
        GrantedAuthority authority = new SimpleGrantedAuthority("DOKUMENTENMAPPE_READ");
        Authentication authentication = new TestAuthentication(authority);
        when(session.getUser()).thenReturn(User.builder().gid(new UserId("sub")).build());

        // Mock editorRollenUndRechte
        when(editorRollenUndRechte.isAllowed("sub", "WRITE", "DOKUMENTENMAPPE", "123456")).thenReturn(false);

        // Act
        boolean hasPermission = customPermissionEvaluator.hasPermission(authentication, "123456", "DOKUMENTENMAPPE",
            "WRITE");

        // Assert
        assertFalse(hasPermission);
    }


    @Test
    void testHasPermissionForObject() {
        // Arrange
        Comment randomComment = TestObjectsUtil.getRandomComment();

        // Mock Authentication
        GrantedAuthority authority = new SimpleGrantedAuthority("COMMENT_READ");
        Authentication authentication = new TestAuthentication(authority);

        // Act
        boolean hasPermission = customPermissionEvaluator.hasPermission(authentication,
            randomComment,
            "READ");

        // Assert
        assertTrue(hasPermission);
    }
}


