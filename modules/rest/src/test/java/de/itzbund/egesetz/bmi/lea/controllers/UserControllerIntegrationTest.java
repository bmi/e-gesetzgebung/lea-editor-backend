// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.api.filter.ContentBase64Filter;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.PinnedDokumentenmappenDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserSettingsDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.UserRestPort;
import de.itzbund.egesetz.bmi.lea.util.TestObjectsUtil;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@Log4j2
@ActiveProfiles("test")
@ExtendWith({SpringExtension.class}) // JUnit 5
class UserControllerIntegrationTest {

	// ----- Controller ------
	@InjectMocks
	private UserController userController;

	// ----- Service ---------
	@Mock
	private UserRestPort userService;
	@Mock
	private ContentBase64Filter base64Filter;


	@BeforeEach
	public void setUp() {
		userServiceMocks();
	}


	private void userServiceMocks() {
		when(userService.getUser()).thenReturn(
			TestObjectsUtil.getUserEntity());
	}


	@Test
	void getUserTest() {
		// Act
		ResponseEntity<UserDTO> response =
			userController.getUser();

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	void updateUserSettingsTest() {

		// - Mock Service
		doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			((ArrayList) args[1]).add(ServiceState.OK);
			return null;
		}).when(userService)
			.updateUserSettings(any(), any());

		PinnedDokumentenmappenDTO pinnedDokumentenmappenDTO = PinnedDokumentenmappenDTO.builder().rvId(UUID.randomUUID()).dmIds(List.of(UUID.randomUUID()))
			.build();
		UserSettingsDTO userSettingsDTO = UserSettingsDTO.builder().pinnedDokumentenmappen(List.of(pinnedDokumentenmappenDTO)).build();

		// Act
		ResponseEntity<Void> response =
			userController.updateSettings(userSettingsDTO);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	void getUserSettingsTest() {
		// - Mock Service
		PinnedDokumentenmappenDTO pinnedDokumentenmappenDTO = PinnedDokumentenmappenDTO.builder().rvId(UUID.randomUUID()).dmIds(List.of(UUID.randomUUID()))
			.build();
		UserSettingsDTO userSettingsDTO = UserSettingsDTO.builder().pinnedDokumentenmappen(List.of(pinnedDokumentenmappenDTO)).build();
		when(userService.getUserSettings()).thenReturn(userSettingsDTO);

		// Act
		ResponseEntity<UserSettingsDTO> response = userController.getSettings();

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(Objects.requireNonNull(response.getBody()).getPinnedDokumentenmappen().get(0), pinnedDokumentenmappenDTO);
	}

}
