// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers.restful;

import de.itzbund.egesetz.bmi.lea.controllers.FileController;
import de.itzbund.egesetz.bmi.lea.domain.dtos.media.MediaShortSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.media.MediaSummaryDTO;
import de.itzbund.egesetz.bmi.lea.util.TestDTOUtil;
import org.junit.jupiter.api.Test;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class MediaModelAssemblerTest {

    @Test
    void testToModel() {
        MediaSummaryDTO mediaSummaryDTO = TestDTOUtil.getRandomMediaSummeryDTO();

        FileController controller = mock(FileController.class);
        when(controller.get(mediaSummaryDTO.getId()
            .toString())).thenReturn(null);

        MediaModelAssembler assembler = new MediaModelAssembler();
        EntityModel<MediaShortSummaryDTO> entityModel = assembler.toModel(mediaSummaryDTO);

        Optional<Link> optionalLink = entityModel.getLink("self");
        if (optionalLink.isPresent()) {
            assertEquals("/media/" + mediaSummaryDTO.getId(), optionalLink
                .get()
                .getHref());
        } else {
            fail("link not found");
        }
    }
}
