// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.domain.aggregate;

import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionSystemException;

import java.util.Collections;
import java.util.List;
import java.util.Set;

@Component
@SuppressWarnings("unused")
public class EditorRollenUndRechte {

	public List<String> getAlleRessourcenFuerBenutzerUndRolleUndRessourceTyp(String benutzerId, String rolle,
		String ressourcenTyp) {
		return Collections.emptyList();
	}


	public boolean addRessourcePermission(String benutzerId, String ressourcenTyp, String ressourceId, String rolle)
		throws
		TransactionSystemException {
		return false;
	}


	public Set<String> getAlleBenutzerIdsByRessourceIdAndAktion(String ressourcenId, String aktion) {
		return Collections.emptySet();
	}


	public boolean isAllowed(String benutzerId, String aktion, String ressourcenTyp, String ressourcenId) {
		return false;
	}


	public List<String> getAlleBenutzerIdsByRessourceIdAndRolle(String ressourcenId, String rolle) {
		return Collections.emptyList();
	}


	public boolean removeRessourcePermission(String benutzerId, String ressourcenTyp, String ressourceId, String rolle) {
		return false;
	}


	public boolean removeRessourcePermission(String ressourcenTyp, String ressourceId, String rolle) {
		return false;
	}

	
	public String getRessortKurzbezeichnungForNutzer(String userId) {
		return "";
	}
}
