// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.mappers;

import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CommentResponseDTO;
import de.itzbund.egesetz.bmi.lea.domain.mappers.BaseMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import de.itzbund.egesetz.bmi.lea.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for {@link CommentMapperRest}
 */
class CommentMapperRestTest {

    private CommentMapperRest commentMapper;


    @BeforeEach
    void init() {
        commentMapper = new CommentMapperRestImpl(new BaseMapperImpl(), new ReplyMapperRestImpl(
            new BaseMapperImpl()));
    }


    @Test
    void testMapCommentToCommentResponseDTO() {
        Comment comment = TestObjectsUtil.getRandomComment();
        CommentResponseDTO actual = commentMapper.map(comment);
        assertThat(actual).hasNoNullFieldsOrProperties();
    }

}
