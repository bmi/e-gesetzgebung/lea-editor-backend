// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.HomepageRegulatoryProposalDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.RegelungsvorhabengTableDTOs;
import de.itzbund.egesetz.bmi.lea.domain.dtos.paging.PagingUtil;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.PropositionRestPort;
import de.itzbund.egesetz.bmi.lea.util.TestDTOUtil;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

@Log4j2
@ActiveProfiles("test")
@ExtendWith({SpringExtension.class}) // JUnit 5
class PropositionControllerIntegrationTest {

    // ----- Controller ------
    @InjectMocks
    private PropositionController propositionController;

    // ----- Service ---------
    @Mock
    private PropositionRestPort propositionService;


    @BeforeEach
    public void setUp() {
        propositionServiceMocks();
    }


    private void propositionServiceMocks() {
        when(propositionService.getStartseiteMeineDokumente(anyList(), any())).thenAnswer(
            invocation ->
            {
                List<ServiceState> status = invocation.getArgument(0); // status
                status.add(ServiceState.OK);

                return RegelungsvorhabengTableDTOs.builder()
                    .dtos(
                        new PageImpl<>(TestDTOUtil.getRandomHomepageRegulatoryProposalDTOList(), PagingUtil.createDefaultPagingForMeineDokumente(), 0)
                    ).build();
            }
        );

        when(propositionService.getStartseiteDokumenteAusAbstimmungen(anyList())).thenAnswer(
            invocation ->
            {
                List<ServiceState> status = invocation.getArgument(0); // status
                status.add(ServiceState.OK);
                return TestDTOUtil.getRandomHomepageRegulatoryProposalDTOList();
            }
        );
    }


    @Test
    void getAllPropositionsTest() {
        // Act
        ResponseEntity<RegelungsvorhabengTableDTOs> response = propositionController.getMeineDokumente(Optional.empty());

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());

    }


    @Test
    void getDokumenteAusAbstimmungenTest() {
        // Act
        ResponseEntity<List<HomepageRegulatoryProposalDTO>> response =
            propositionController.getDokumenteAusAbstimmungen();

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

}
