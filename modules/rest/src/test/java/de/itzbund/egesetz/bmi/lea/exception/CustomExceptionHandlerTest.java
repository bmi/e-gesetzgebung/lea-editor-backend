// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.exception;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class CustomExceptionHandlerTest {

    @InjectMocks
    private CustomExceptionHandler customExceptionHandler;

    @Mock
    private EgfaException egfaException;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    void testHandleEgfaExceptionWithApiErrorDto() {
        RestApiErrorDto expectedErrorDto = RestApiErrorDto.builder()
            .messageCode("SOME_ERROR")
            .messageDetail("Some error message")
            .build();

        when(egfaException.getApiErrorDto()).thenReturn(expectedErrorDto);
        when(egfaException.getHttpStatus()).thenReturn(HttpStatus.BAD_REQUEST);

        ResponseEntity<Object> response = customExceptionHandler.handleEgfaException(egfaException);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(expectedErrorDto, response.getBody());
    }


    @Test
    void testHandleEgfaExceptionWithNullApiErrorDto() {
        when(egfaException.getApiErrorDto()).thenReturn(null);
        when(egfaException.getHttpStatus()).thenReturn(HttpStatus.INTERNAL_SERVER_ERROR);

        ResponseEntity<Object> response = customExceptionHandler.handleEgfaException(egfaException);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());

        // Test default error message when ApiErrorDto is null
        RestApiErrorDto expectedDefaultErrorDto = RestApiErrorDto.builder()
            .messageCode("NO_ERROR")
            .messageDetail("No error message given. Something went wrong")
            .build();

        assertEquals(expectedDefaultErrorDto, response.getBody());
    }


    @Test
    void testHandleEgfaExceptionWithNullHttpStatus() {
        RestApiErrorDto expectedErrorDto = RestApiErrorDto.builder()
            .messageCode("SOME_ERROR")
            .messageDetail("Some error message")
            .build();

        when(egfaException.getApiErrorDto()).thenReturn(expectedErrorDto);
        when(egfaException.getHttpStatus()).thenReturn(null);

        ResponseEntity<Object> response = customExceptionHandler.handleEgfaException(egfaException);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());

        // Test HttpStatus.INTERNAL_SERVER_ERROR when HttpStatus is null
        assertEquals(expectedErrorDto, response.getBody());
    }
}
