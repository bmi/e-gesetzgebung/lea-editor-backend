// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.api.filter.ContentBase64Filter;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CommentResponseDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CreateCommentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.DeleteCommentsDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.PatchCommentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.ReplyDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.UpdateCommentStatusDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.ReplyId;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CommentRestPort;
import de.itzbund.egesetz.bmi.lea.mappers.CommentMapperRest;
import de.itzbund.egesetz.bmi.lea.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@ExtendWith({SpringExtension.class}) // JUnit 5
@SuppressWarnings("unused")
class CommentControllerIntegrationTest {

	// ----- Controller ------
	@InjectMocks
	private CommentController commentController;
	// ----- Service ---------
	@Mock
	private CommentRestPort commentService;
	// ----- Mapper ----------
	@Mock
	private CommentMapperRest commentMapperRest;
	// ----- Filter ----------
	@Mock
	private ContentBase64Filter contentBase64Filter;


	@BeforeEach
	public void setUp() {
		commentServiceMocks();
	}


	private void commentServiceMocks() {
		doAnswer((i) -> {
			List<ServiceState> status = i.getArgument(3);
			status.add(ServiceState.OK);
			return null;
		}).when(commentService)
			.createReply(any(), any(), any(), anyList());

		doAnswer((i) -> {
			List<ServiceState> status = i.getArgument(3);
			status.add(ServiceState.OK);
			return null;
		}).when(commentService)
			.updateComment(any(), any(), any(), anyList());

		doAnswer((i) -> {
			List<ServiceState> status = i.getArgument(4);
			status.add(ServiceState.OK);
			return null;
		}).when(commentService)
			.updateReply(any(), any(), any(), any(), anyList());

		doAnswer((i) -> {
			List<ServiceState> status = i.getArgument(3);
			status.add(ServiceState.OK);
			return null;
		}).when(commentService)
			.setCommentState(any(), any(), any(), anyList());

		doAnswer((i) -> {
			List<ServiceState> status = i.getArgument(2);
			status.add(ServiceState.OK);
			return null;
		}).when(commentService)
			.deleteCommentOld(any(), any(), anyList());

		doAnswer((i) -> {
			List<ServiceState> status = i.getArgument(2);
			status.add(ServiceState.OK);
			return null;
		}).when(commentService)
			.deleteComments(any(), any(), anyList());

		doAnswer((i) -> {
			List<ServiceState> status = i.getArgument(3);
			status.add(ServiceState.OK);
			return null;
		}).when(commentService)
			.deleteCommentReply(any(), any(), any(), anyList());
	}

	// ================================================================


	@Test
	void whenCompoundDocumentIsFreeze_thenCommentAddingIsAllowed() {
		// Arrange
		when(commentService.createComment(
			any(), any(), any()))
			.thenAnswer(
				invocation ->
				{
					List<ServiceState> status = invocation.getArgument(2);
					status.add(ServiceState.OK);
					return DocumentDTO.builder().build();
				}
			);
		DocumentId documentId = TestObjectsUtil.getDocumentEntity()
			.getDocumentId();
		CreateCommentDTO createCommentDTO = TestDTOUtil.getRandomCreateCommentDTO();

		// Act
		ResponseEntity<DocumentDTO> response =
			commentController.createComment(documentId.getId(), createCommentDTO);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}


	@Test
	void whenCommentNoPermission_thenStatusForbidden() {
		// Arrange
		when(commentService.createComment(
			any(), any(), any()))
			.thenAnswer(
				invocation ->
				{
					List<ServiceState> status = invocation.getArgument(2);
					status.add(ServiceState.NO_PERMISSION);
					return DocumentDTO.builder().build();
				}
			);

		DocumentId documentId = TestObjectsUtil.getDocumentEntity()
			.getDocumentId();
		CreateCommentDTO createCommentDTO = TestDTOUtil.getRandomCreateCommentDTO();

		// Act
		ResponseEntity<DocumentDTO> response =
			commentController.createComment(documentId.getId(), createCommentDTO);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
	}


	@Test
	void whenCommentDocumentNotFound_thenStatusNotFound() {
		// Arrange
		when(commentService.createComment(
			any(), any(), any()))
			.thenAnswer(
				invocation ->
				{
					List<ServiceState> status = invocation.getArgument(2);
					status.add(ServiceState.DOCUMENT_NOT_FOUND);
					return DocumentDTO.builder().build();
				}
			);

		DocumentId documentId = TestObjectsUtil.getDocumentEntity()
			.getDocumentId();
		CreateCommentDTO createCommentDTO = TestDTOUtil.getRandomCreateCommentDTO();

		// Act
		ResponseEntity<DocumentDTO> response =
			commentController.createComment(documentId.getId(), createCommentDTO);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}


	@Test
	void whenCommentUnknownStatus_thenStatusInternalServerError() {
		// Arrange
		when(commentService.createComment(
			any(), any(), any()))
			.thenAnswer(
				invocation ->
				{
					List<ServiceState> status = invocation.getArgument(2);
					status.add(ServiceState.INVALID_STATE);
					return DocumentDTO.builder().build();
				}
			);

		DocumentId documentId = TestObjectsUtil.getDocumentEntity()
			.getDocumentId();
		CreateCommentDTO createCommentDTO = TestDTOUtil.getRandomCreateCommentDTO();

		// Act
		ResponseEntity<DocumentDTO> response =
			commentController.createComment(documentId.getId(), createCommentDTO);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}

	// ================================================================


	@Test
	void replyTest() {
		// Arrange
		DocumentId documentId = TestObjectsUtil.getDocumentEntity()
			.getDocumentId();
		CommentId commentId = TestObjectsUtil.getRandomComment()
			.getCommentId();
		ReplyDTO reply = TestDTOUtil.getRandomReplyDTO();

		// Act
		ResponseEntity<Void> response =
			commentController.createReply(documentId.getId(), commentId.getId(), reply);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
	}


	@Test
	void replyTest_NoPermission() {
		// Arrange
		doAnswer((i) -> {
			List<ServiceState> status = i.getArgument(3);
			status.add(ServiceState.NO_PERMISSION);
			return null;
		}).when(commentService)
			.createReply(any(), any(), any(), anyList());

		DocumentId documentId = TestObjectsUtil.getDocumentEntity()
			.getDocumentId();
		CommentId commentId = TestObjectsUtil.getRandomComment()
			.getCommentId();
		ReplyDTO reply = TestDTOUtil.getRandomReplyDTO();

		// Act
		ResponseEntity<Void> response =
			commentController.createReply(documentId.getId(), commentId.getId(), reply);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
	}


	@Test
	void replyTest_CommentNoFound() {
		// Arrange
		doAnswer((i) -> {
			List<ServiceState> status = i.getArgument(3);
			status.add(ServiceState.COMMENT_NOT_FOUND);
			return null;
		}).when(commentService)
			.createReply(any(), any(), any(), anyList());

		DocumentId documentId = TestObjectsUtil.getDocumentEntity()
			.getDocumentId();
		CommentId commentId = TestObjectsUtil.getRandomComment()
			.getCommentId();
		ReplyDTO reply = TestDTOUtil.getRandomReplyDTO();

		// Act
		ResponseEntity<Void> response =
			commentController.createReply(documentId.getId(), commentId.getId(), reply);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}


	@Test
	void replyTest_InternalError() {
		// Arrange
		doAnswer((i) -> {
			List<ServiceState> status = i.getArgument(3);
			status.add(ServiceState.INVALID_STATE);
			return null;
		}).when(commentService)
			.createReply(any(), any(), any(), anyList());

		DocumentId documentId = TestObjectsUtil.getDocumentEntity()
			.getDocumentId();
		CommentId commentId = TestObjectsUtil.getRandomComment()
			.getCommentId();
		ReplyDTO reply = TestDTOUtil.getRandomReplyDTO();

		// Act
		ResponseEntity<Void> response =
			commentController.createReply(documentId.getId(), commentId.getId(), reply);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}

	// ================================================================


	@Test
	void whenCommmentShoulsBeLoaded_thenTheyShouldBeProvided() {
		// Arrange
		when(commentService.loadAllCommentsOfUserOfDocument(
			any(), any()))
			.thenAnswer(
				invocation ->
				{
					List<ServiceState> status = invocation.getArgument(1);
					status.add(ServiceState.OK);
					return Collections.emptyList();
				}
			);
		DocumentId documentId = TestObjectsUtil.getDocumentEntity()
			.getDocumentId();
		List<UUID> userIds = new ArrayList<>();

		// Act
		ResponseEntity<List<CommentResponseDTO>> response =
			commentController.loadCommentsOfUserOFDocument(documentId.getId(), userIds);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}


	@Test
	void whenCommentShoulsBeLoaded_NoComments() {
		// Arrange
		when(commentService.loadAllCommentsOfUserOfDocument(
			any(), any()))
			.thenAnswer(
				invocation ->
				{
					List<ServiceState> status = invocation.getArgument(1);
					status.add(ServiceState.THERE_ARE_NO_COMMENTS);
					return Collections.emptyList();
				}
			);

		DocumentId documentId = TestObjectsUtil.getDocumentEntity()
			.getDocumentId();
		List<UUID> userIds = new ArrayList<>();

		// Act
		ResponseEntity<List<CommentResponseDTO>> response =
			commentController.loadCommentsOfUserOFDocument(documentId.getId(), userIds);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
	}


	@Test
	void whenCommentShoulsBeLoaded_NoPermission() {
		// Arrange
		when(commentService.loadAllCommentsOfUserOfDocument(
			any(), any()))
			.thenAnswer(
				invocation ->
				{
					List<ServiceState> status = invocation.getArgument(1);
					status.add(ServiceState.NO_PERMISSION);
					return Collections.emptyList();
				}
			);

		DocumentId documentId = TestObjectsUtil.getDocumentEntity()
			.getDocumentId();
		List<UUID> userIds = new ArrayList<>();

		// Act
		ResponseEntity<List<CommentResponseDTO>> response =
			commentController.loadCommentsOfUserOFDocument(documentId.getId(), userIds);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
	}


	@Test
	void whenCommentShoulsBeLoaded_DocumentNotFound() {
		// Arrange
		when(commentService.loadAllCommentsOfUserOfDocument(
			any(), any()))
			.thenAnswer(
				invocation ->
				{
					List<ServiceState> status = invocation.getArgument(1);
					status.add(ServiceState.DOCUMENT_NOT_FOUND);
					return Collections.emptyList();
				}
			);

		DocumentId documentId = TestObjectsUtil.getDocumentEntity()
			.getDocumentId();
		List<UUID> userIds = new ArrayList<>();

		// Act
		ResponseEntity<List<CommentResponseDTO>> response =
			commentController.loadCommentsOfUserOFDocument(documentId.getId(), userIds);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}


	@Test
	void whenCommentShoulsBeLoaded_InternalError() {
		// Arrange
		when(commentService.loadAllCommentsOfUserOfDocument(
			any(), any()))
			.thenAnswer(
				invocation ->
				{
					List<ServiceState> status = invocation.getArgument(1);
					status.add(ServiceState.INVALID_STATE);
					return Collections.emptyList();
				}
			);

		DocumentId documentId = TestObjectsUtil.getDocumentEntity()
			.getDocumentId();
		List<UUID> userIds = new ArrayList<>();

		// Act
		ResponseEntity<List<CommentResponseDTO>> response =
			commentController.loadCommentsOfUserOFDocument(documentId.getId(), userIds);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}
	// ================================================================


	@Test
	void whenCommentShoulsBeUpdated_thenItShouldBeDone() {
		// Arrange
		DocumentId documentId = TestObjectsUtil.getDocumentEntity()
			.getDocumentId();
		CommentId commentId = TestObjectsUtil.getRandomComment()
			.getCommentId();
		PatchCommentDTO patchCommentDTO = TestDTOUtil.getRandomPatchCommentDTO();

		// Act
		ResponseEntity<Void> response =
			commentController.updateComment(documentId.getId(), commentId.getId(), patchCommentDTO);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
	}

	// ================================================================


	@Test
	void whenReplyShouldBeUpdated_thenItShouldBeDone() {
		// Arrange
		DocumentId documentId = TestObjectsUtil.getDocumentEntity()
			.getDocumentId();
		CommentId commentId = TestObjectsUtil.getRandomComment()
			.getCommentId();
		ReplyId replyId = TestObjectsUtil.getRandomReplyEntity()
			.getReplyId();
		ReplyDTO replyDTO = TestDTOUtil.getRandomReplyDTO();

		// Act
		ResponseEntity<Void> response =
			commentController.updateReply(documentId.getId(), commentId.getId(), replyId.getId(), replyDTO);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	// ================================================================


	@Test
	void commentStatusUpdate() {
		// Arrange
		DocumentId documentId = TestObjectsUtil.getDocumentEntity()
			.getDocumentId();
		CommentId commentId = TestObjectsUtil.getRandomComment()
			.getCommentId();
		UpdateCommentStatusDTO updateCommentStatusDTO = TestDTOUtil.getRandomUpdateCommentStatusDTO();

		// Act
		ResponseEntity<Void> response =
			commentController.updateCommentState(documentId.getId(), commentId.getId(), updateCommentStatusDTO);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
	}

	// ================================================================


	@Test
	void deleteCommentOldTest() {
		// Arrange
		DocumentId documentId = TestObjectsUtil.getDocumentEntity()
			.getDocumentId();
		CommentId commentId = TestObjectsUtil.getRandomComment()
			.getCommentId();

		// Act
		ResponseEntity<Void> response =
			commentController.deleteCommentOld(documentId.getId(), commentId.getId());

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
	}

	@Test
	void deleteCommentTest() {
		// Arrange
		DocumentId documentId = TestObjectsUtil.getDocumentEntity()
			.getDocumentId();
		DeleteCommentsDTO deleteCommentsDTO = TestObjectsUtil.getDeleteCommentsDTO();

		// Act
		ResponseEntity<DocumentDTO> response =
			commentController.deleteComments(documentId.getId(), deleteCommentsDTO);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	// ================================================================


	@Test
	void deleteCommentReplyTest() {
		// Arrange
		DocumentId documentId = TestObjectsUtil.getDocumentEntity()
			.getDocumentId();
		CommentId commentId = TestObjectsUtil.getRandomComment()
			.getCommentId();
		ReplyId replyId = TestObjectsUtil.getRandomReplyEntity()
			.getReplyId();

		// Act
		ResponseEntity<Void> response =
			commentController.deleteCommentReply(documentId.getId(), commentId.getId(), replyId.getId());

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
	}
}
