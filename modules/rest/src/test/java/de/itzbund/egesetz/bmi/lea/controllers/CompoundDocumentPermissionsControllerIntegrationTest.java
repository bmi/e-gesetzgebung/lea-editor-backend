// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CompoundDocumentPermissionRestPort;
import de.itzbund.egesetz.bmi.lea.util.TestDTOUtil;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

@Log4j2
@ActiveProfiles("test")
@ExtendWith({SpringExtension.class}) // JUnit 5
class CompoundDocumentPermissionsControllerIntegrationTest {

    // ----- Controller ------
    @InjectMocks
    private CompoundDocumentPermissionsController compoundDocumentPermissionsController;

    @Mock
    private CompoundDocumentPermissionRestPort compoundDocumentPermissionService;


    @BeforeEach
    public void setUp() {
        when(compoundDocumentPermissionService.getPermissionsByRessourceIdAndAktion(any(), any(),
            anyList())).thenAnswer(invocation ->
        {
            List<ServiceState> status = invocation.getArgument(2);
            status.add(ServiceState.OK);
            List<UserDTO> ret = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                ret.add(TestDTOUtil.getRandomUserDTO());
            }
            return ret;
        });
    }


    @Test
    void getAlleBenutzerIdsByRessourceIdAndAktionTest() {
        CompoundDocumentDTO compoundDocumentDTO = TestDTOUtil.getRandomCompoundDocumentDTO();

        ResponseEntity<List<UserDTO>> response =
            compoundDocumentPermissionsController.getByIdentifierAndAction(
                compoundDocumentDTO.getId(), "LESEN"
            );

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}
