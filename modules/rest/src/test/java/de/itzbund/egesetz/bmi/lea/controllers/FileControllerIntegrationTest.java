// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.controllers.restful.MediaModelAssembler;
import de.itzbund.egesetz.bmi.lea.domain.dtos.media.MediaShortSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.media.MediaSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.FileRestPort;
import de.itzbund.egesetz.bmi.lea.util.TestDTOUtil;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Log4j2
@ActiveProfiles("test")
@ExtendWith({SpringExtension.class}) // JUnit 5
class FileControllerIntegrationTest {

    // ----- Controller ------
    @InjectMocks
    private FileController fileController;

    // ----- Service ---------
    @Mock
    private FileRestPort fileService;

    @Mock
    private MediaModelAssembler mediaModelAssembler;


    @BeforeEach
    public void setUp() {
        fileServiceMocks();
        mediaModelAssemblerMocks();
    }


    private void fileServiceMocks() {
        when(fileService.loadFromDB(any(UUID.class))).thenReturn(
            TestDTOUtil.getRandomMediaSummeryDTO());

        when(fileService.save(anyString(), any())).thenReturn(
            UUID.randomUUID());
    }


    private void mediaModelAssemblerMocks() {
        MediaSummaryDTO mediaSummaryDTO = TestDTOUtil.getRandomMediaSummeryDTO();

        MediaShortSummaryDTO x = TestDTOUtil.getRandomMediaShortSummeryDTO();

        when(mediaModelAssembler.toModel(any(MediaSummaryDTO.class))).thenReturn(
            EntityModel.of(x,
                linkTo(methodOn(FileController.class).get(
                    mediaSummaryDTO.getId()
                        .toString())
                ).withSelfRel()));
    }


    @Test
    void getResourceTest() {
        // Act
        ResponseEntity<Resource> response =
            fileController.get(UUID.randomUUID()
                .toString());

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }


    @Test
    void saveResourceTest() {
        // Act
        ResponseEntity<EntityModel<MediaShortSummaryDTO>> response =
            fileController.postFile("something.nojpg", null);

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.UNSUPPORTED_MEDIA_TYPE, response.getStatusCode());
    }


    @Test
    void deleteResourceTest() {
        // Act
        ResponseEntity<Void> response =
            fileController.delete(UUID.randomUUID());

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }
}
