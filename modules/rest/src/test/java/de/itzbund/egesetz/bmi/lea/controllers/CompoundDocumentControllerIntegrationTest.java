// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.api.filter.ContentBase64Filter;
import de.itzbund.egesetz.bmi.lea.controllers.restful.CompoundDocumentModelAssembler;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentTitleDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CopyCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CreateCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.ImportEgfaDataDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentImportDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentMetadataDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaStatusDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtListDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserRightsDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.enums.VerfahrensType;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.BestandsrechtRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CompoundDocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.DocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.EgfaRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.ExportRestPort;
import de.itzbund.egesetz.bmi.lea.domain.services.AuxiliaryService;
import de.itzbund.egesetz.bmi.lea.domain.services.RegelungsvorhabenService;
import de.itzbund.egesetz.bmi.lea.mappers.CompoundDocumentMapperRestImpl;
import de.itzbund.egesetz.bmi.lea.mappers.PropositionMapperRestImpl;
import de.itzbund.egesetz.bmi.lea.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.util.TestObjectsUtil;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Log4j2
@ActiveProfiles("test")
@ExtendWith({SpringExtension.class}) // JUnit 5
@SuppressWarnings("unchecked")
class CompoundDocumentControllerIntegrationTest {

    private final CompoundDocumentSummaryDTO randomCompoundDocumentSummaryDTO =
        TestDTOUtil.getRandomCompoundDocumentSummaryDTO();
    // ----- Controller ------
    @InjectMocks
    private CompoundDocumentController compoundDocumentController;

    // ----- Service ---------
    @Mock
    private CompoundDocumentRestPort compoundDocumentService;
    @Mock
    private BestandsrechtRestPort bestandsrechtService;
    @Mock
    private DocumentRestPort documentService;
    @Mock
    private RegelungsvorhabenService regelungsvorhabenService;
    @Mock
    private AuxiliaryService auxiliaryService;
    @Mock
    private EgfaRestPort egfaService;
    @Mock
    private ExportRestPort exportService;

    // ----- Mapper ----------
    @Mock
    private CompoundDocumentMapperRestImpl compoundDocumentMapperRest;
    @Mock
    private PropositionMapperRestImpl propositionMapperRest;

    //    Mappers.getMapper(SimpleSourceDestinationMapper.class)

    @Mock
    private ContentBase64Filter base64Filter;
    @Mock
    private CompoundDocumentModelAssembler compoundDocumentModelAssembler;


    @BeforeEach
    public void setUp() {
        compoundDocumentServiceMocks();
        documentServiceMocks();
        compoundDocumentModelAssemblerMocks();
        regelungsvorhabenServiceMocks();
        mapperMockups();

        when(base64Filter.encodeCompoundDocumentDTO(any())).thenReturn(TestDTOUtil.getRandomCompoundDocumentDTO());
        when(base64Filter.decodeDocumentImportDTO(any())).thenReturn(TestDTOUtil.getRandomDocumentImportDTO());
        when(auxiliaryService.makeDocument(any(), any())).thenReturn(TestObjectsUtil.getRandomDocument());
    }


    private void mapperMockups() {
        when(compoundDocumentMapperRest.mapFromCompoundDocumentAndProposition(any(), any())).thenReturn(
            TestDTOUtil.getRandomCompoundDocumentDTO());
        when(propositionMapperRest.map(any(RegelungsvorhabenEditorDTO.class))).thenReturn(
            TestDTOUtil.getRandomCompoundDocumentDTO()
                .getProposition());
    }


    private void regelungsvorhabenServiceMocks() {
        when(regelungsvorhabenService.getRegelungsvorhabenEditorDTONEW(any(), anyBoolean())).thenReturn(
            TestDTOUtil.getRandomRegelungsvorhabenEditorDTO());
        when(regelungsvorhabenService.getRegelungsvorhabenEditorDTOForCompoundDocument(any())).thenReturn(
            TestDTOUtil.getRandomRegelungsvorhabenEditorDTO());
        when(regelungsvorhabenService.getProponent(any())).thenReturn(TestDTOUtil.getRandomProponentDTO());
    }


    private void compoundDocumentModelAssemblerMocks() {
        CompoundDocumentDTO randomCompoundDocumentDTO = TestDTOUtil.getRandomCompoundDocumentDTO();

        EntityModel<CompoundDocumentDTO> of = EntityModel.of(randomCompoundDocumentDTO,
            linkTo(methodOn(CompoundDocumentController.class).getByIdentifier(
                randomCompoundDocumentDTO.getId())).withSelfRel()
        );

        when(compoundDocumentModelAssembler.toModel(any())).thenReturn(of);

        CollectionModel<EntityModel<CompoundDocumentDTO>> entityModels = CollectionModel.empty();

        when(compoundDocumentModelAssembler.toCollectionModel(any())).thenReturn(
            entityModels
        );
    }


    private void documentServiceMocks() {
        when(documentService.importDocument(any(CompoundDocumentId.class), any(DocumentImportDTO.class),
            anyLong(), anyList()))
            .thenAnswer(invocation ->
                {
                    List<ServiceState> status = invocation.getArgument(3); // status
                    status.add(ServiceState.OK);
                    return TestDTOUtil.getRandomDocumentMetadataDTO();
                }
            );
    }


    private void compoundDocumentServiceMocks() {
        when(compoundDocumentService.createCompoundDocument(any(), anyList()))
            .thenAnswer(invocation ->
                {
                    List<ServiceState> status = invocation.getArgument(1); // status
                    status.add(ServiceState.OK);
                    return TestDTOUtil.getRandomCompoundDocumentDTO();
                }
            );

        when(compoundDocumentService.getAllCompoundDocumentsRestrictedByAccessRights()).thenReturn(
            TestObjectsUtil.getCompoundDocumentListOfSize(3));

        when(compoundDocumentService.getCompoundDocumentById(any(CompoundDocumentId.class), anyList()))
            .thenAnswer(invocation ->
                {
                    List<ServiceState> status = invocation.getArgument(1); // status
                    status.add(ServiceState.OK);
                    return TestObjectsUtil.getRandomCompoundDocument();
                }
            );

        when(compoundDocumentService.getDocumentsList(any(CompoundDocumentId.class), anyList()))
            .thenAnswer(invocation ->
                {
                    List<ServiceState> status = invocation.getArgument(1); // status
                    status.add(ServiceState.OK);
                    return TestDTOUtil.getRandomCompoundDocumentSummaryDTO();
                }
            );

        when(compoundDocumentService.addDocument(any(CompoundDocumentId.class), any(Document.class), anyList()))
            .thenAnswer(invocation ->
                {
                    List<ServiceState> status = invocation.getArgument(2); // status
                    status.add(ServiceState.OK);
                    return TestDTOUtil.getRandomCompoundDocumentDTO();
                }
            );

        doAnswer((i) -> {
            List<ServiceState> status = i.getArgument(4);
            status.add(ServiceState.OK);
            return null;
        }).when(compoundDocumentService)
            .update(any(CompoundDocumentId.class), anyString(), any(VerfahrensType.class), any(), anyList());

        when(compoundDocumentService.copyCompoundDocument(
            any(CompoundDocumentId.class),
            any(String.class),
            any(List.class),
            any(List.class)
        ))
            .thenAnswer(invocation ->
                {
                    List<ServiceState> status = invocation.getArgument(3); // status
                    status.add(ServiceState.OK);

                    return randomCompoundDocumentSummaryDTO;
                }
            );

        doAnswer((i) -> {
            List<ServiceState> status = i.getArgument(2);
            status.add(ServiceState.OK);
            return null;
        }).when(exportService)
            .exportCompoundDocument(any(), any(), anyList());

        when(compoundDocumentService.getAllTitles(anyList()))
            .thenAnswer(invocation ->
                {
                    List<ServiceState> status = invocation.getArgument(0); // status
                    status.add(ServiceState.OK);

                    return List.of(randomCompoundDocumentSummaryDTO);
                }
            );
    }


    @Test
    void createCompoundDocumentTest() {
        // Arrange
        CreateCompoundDocumentDTO createCompoundDocumentDTO = TestDTOUtil.getRandomCreateCompoundDocumentDTO();

        // Act
        ResponseEntity<EntityModel<CompoundDocumentDTO>> response =
            compoundDocumentController.createCompoundDocument(createCompoundDocumentDTO);

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    // ================================================================


    @Test
    void getAllTest() {
        // Act
        ResponseEntity<List<EntityModel<CompoundDocumentDTO>>> response =
            compoundDocumentController.getAll();

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }


    @Test
    void getAllTitlesTest() {
        // Act
        ResponseEntity<List<CompoundDocumentTitleDTO>> response =
            compoundDocumentController.getAllTitles();

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    // ================================================================


    @Test
    void getByRegulatoryProposalTest() {
        UUID regulatoryProposalId = UUID.randomUUID();

        // Act
        ResponseEntity<List<EntityModel<CompoundDocumentDTO>>> response =
            compoundDocumentController.getByRegulatoryProposalId(regulatoryProposalId);

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }


    @Test
    void getByIdentifierTest() {
        // Act
        ResponseEntity<EntityModel<CompoundDocumentDTO>> response =
            compoundDocumentController.getByIdentifier(UUID.randomUUID());

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    // ================================================================


    @Test
    void getDocumentsListTest() {
        // Act
        ResponseEntity<CompoundDocumentSummaryDTO> response =
            compoundDocumentController.getDocumentsList(UUID.randomUUID());

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    // ================================================================


    @Test
    void addDocumentsTest() {
        // Act
        ResponseEntity<EntityModel<CompoundDocumentDTO>> response =
            compoundDocumentController.addDocuments(UUID.randomUUID(), TestDTOUtil.getRandomCreateDocumentDTO());

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    // ================================================================


    @Test
    void renameAndStatePatchTest() {
        // Act
        ResponseEntity<Void> response =
            compoundDocumentController.renameAndStatePatch(UUID.randomUUID(),
                TestDTOUtil.getRandomUpdateCompoundDocumentDTO());

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    // ================================================================


    @Test
    void importDocumentTest() {
        // Act
        ResponseEntity<DocumentMetadataDTO> response =
            compoundDocumentController.importDocument(null, UUID.randomUUID(),
                TestDTOUtil.getRandomDocumentImportDTO());

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    // ================================================================


    @Test
    void downloadZipFileTest() {
        // Act
        ResponseEntity<Void> response =
            compoundDocumentController.downloadZipFile(null, UUID.randomUUID());

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    // ================================================================


    @Test
    void whenCompoundDocumentIncreaseVersionWithoutDocumentCopy_ItShouldBePossible() {

        // Arrange
        UUID compoundDocumentId = UUID.randomUUID();
        CopyCompoundDocumentDTO randomCopyCompoundDocumentDTO = TestDTOUtil.getRandomCopyCompoundDocumentDTO();
        List<UUID> documentIds = null;

        // Act
        ResponseEntity<CompoundDocumentSummaryDTO> actual = compoundDocumentController.versionIncrement(
            compoundDocumentId,
            randomCopyCompoundDocumentDTO, documentIds);

        // Assert
        assertThat(actual)
            .hasFieldOrPropertyWithValue("status", HttpStatus.OK)
            .isEqualTo(ResponseEntity.ok(randomCompoundDocumentSummaryDTO));
    }


    @Test
    void whenCompoundDocumentIncreaseVersionWithDocumentCopy_ItShouldBePossible() {
        CompoundDocumentSummaryDTO randomCompoundDocumentSummaryDTO = TestDTOUtil.getRandomCompoundDocumentSummaryDTO();

        when(compoundDocumentService.copyCompoundDocument(
            any(CompoundDocumentId.class),
            any(String.class),
            any(List.class),
            any(List.class)
        ))
            .thenAnswer(invocation ->
                {
                    List<ServiceState> status = invocation.getArgument(3); // status
                    status.add(ServiceState.OK);

                    return randomCompoundDocumentSummaryDTO;
                }
            );

        // Arrange
        UUID compoundDocumentId = UUID.randomUUID();
        CopyCompoundDocumentDTO randomCopyCompoundDocumentDTO = TestDTOUtil.getRandomCopyCompoundDocumentDTO();
        List<UUID> documentIds = Arrays.asList(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID());

        // Act
        ResponseEntity<CompoundDocumentSummaryDTO> actual = compoundDocumentController.versionIncrement(
            compoundDocumentId,
            randomCopyCompoundDocumentDTO, documentIds);

        // Assert
        assertThat(actual)
            .hasFieldOrPropertyWithValue("status", HttpStatus.OK)
            .isEqualTo(ResponseEntity.ok(randomCompoundDocumentSummaryDTO));
    }


    @Test
    void whenCompoundDocumentIncreaseVersionWithInvalidId_ItShouldBeDetected() {
        when(compoundDocumentService.copyCompoundDocument(
            any(CompoundDocumentId.class),
            any(String.class),
            any(List.class),
            any(List.class)
        ))
            .thenAnswer(invocation ->
                {
                    List<ServiceState> status = invocation.getArgument(3); // status
                    status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);

                    return TestDTOUtil.getRandomCompoundDocumentSummaryDTO();
                }
            );

        // Arrange
        UUID compoundDocumentId = UUID.randomUUID();
        CopyCompoundDocumentDTO randomCopyCompoundDocumentDTO = TestDTOUtil.getRandomCopyCompoundDocumentDTO();
        List<UUID> documentIds = new ArrayList<>();

        // Act
        ResponseEntity<CompoundDocumentSummaryDTO> actual = compoundDocumentController.versionIncrement(
            compoundDocumentId,
            randomCopyCompoundDocumentDTO, documentIds);

        // Assert
        assertThat(actual).hasFieldOrPropertyWithValue("status", HttpStatus.NOT_FOUND);
    }


    @Test
    void whenCompoundDocumentUserRightsShouldBeChanged_ItShouldBePossible() {
        // Arrange
        // - Mock Service
        when(compoundDocumentService.changeAccessRightsForUsersOnCompoundDocuments(
            any(CompoundDocumentId.class),
            anyList()
        )).thenReturn(ServiceState.OK);

        UUID compoundDocumentId = UUID.randomUUID();
        List<UserRightsDTO> randomUserRightsDTOList = TestDTOUtil.getRandomUserRightsDTOList(3);

        // Act
        ResponseEntity<Void> response = compoundDocumentController.setAccessRights(compoundDocumentId,
            randomUserRightsDTOList);

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void importEgfaDataTest() {

        // - Mock Service
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            ((ArrayList<ServiceState>) args[2]).add(ServiceState.OK);
            return null;
        }).when(egfaService)
            .egfaUebernahmeInDokumentenmappe(any(), any(), any());

        // Act
        ResponseEntity<HashMap<String, HttpStatus>> response = compoundDocumentController.importEgfaData(UUID.randomUUID(),
            new ImportEgfaDataDTO());

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void importEgfaDataErrorTest() {

        // - Mock Service
        doAnswer(invocation -> null).when(egfaService)
            .egfaUebernahmeInDokumentenmappe(any(), any(), any());

        // Act
        ResponseEntity<HashMap<String, HttpStatus>> response = compoundDocumentController.importEgfaData(UUID.randomUUID(),
            new ImportEgfaDataDTO());

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    @Test
    void getEgfaStateByCompoundDocumentIdTest() {

        // - Mock Service
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            ((ArrayList<ServiceState>) args[0]).add(ServiceState.OK);
            return Collections.emptyList();
        }).when(egfaService)
            .getEgfaStatusByCompoundDocumentId(any(), any());

        // Act
        ResponseEntity<List<EgfaStatusDTO>> response = compoundDocumentController.getEgfaStateByCompoundDocumentId(UUID.randomUUID());

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void getEgfaStateByCompoundDocumentIdErrorTest() {

        // - Mock Service
        doAnswer(invocation -> null).when(egfaService)
            .getEgfaStatusByCompoundDocumentId(any(), any());

        // Act
        ResponseEntity<List<EgfaStatusDTO>> response = compoundDocumentController.getEgfaStateByCompoundDocumentId(UUID.randomUUID());

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    @Test
    void whenCompoundDocumentWriteRightsShouldBeChanged_ItShouldBePossible() {
        // Arrange
        // - Mock Service
        when(compoundDocumentService.changeWritePermissionsOnCompoundDocuments(any(CompoundDocumentId.class), any(), anyList()))
            .thenAnswer(invocationOnMock -> {
                List<ServiceState> status = invocationOnMock.getArgument(2); // status
                status.add(ServiceState.OK);

                return TestDTOUtil.getRandomCompoundDocumentSummaryDTO();
            });

        UUID compoundDocumentId = UUID.randomUUID();
        List<UserRightsDTO> randomUserRightsDTOList = TestDTOUtil.getRandomUserRightsDTOList(1);

        // Act
        ResponseEntity<CompoundDocumentSummaryDTO> response = compoundDocumentController.changeWritePermissions(compoundDocumentId,
            Optional.of(randomUserRightsDTOList.get(0)));

        // Assert
        assertNotNull(response);
        assertNotNull(response.getStatusCode());
        HttpStatus statusCode = response.getStatusCode();
        assertTrue((statusCode == HttpStatus.NO_CONTENT) || (statusCode == HttpStatus.OK));
    }

    @Test
    void getBestandsrechtByCompoundDocumentIdErrorTest() {

        // - Mock Service
        doAnswer(invocation -> null).when(bestandsrechtService)
            .getBestandsrechtForCompoundDocumentId(any(), any());

        // Act
        ResponseEntity<List<BestandsrechtListDTO>> response = compoundDocumentController.getBestandsrechtByCompoundDocumentId(UUID.randomUUID());

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    @Test
    void getBestandsrechtByCompoundDocumentIdTest() {

        // - Mock Service
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            ((ArrayList<ServiceState>) args[1]).add(ServiceState.OK);
            return Collections.emptyList();
        }).when(bestandsrechtService)
            .getBestandsrechtForCompoundDocumentId(any(), any());

        // Act
        ResponseEntity<List<BestandsrechtListDTO>> response = compoundDocumentController.getBestandsrechtByCompoundDocumentId(UUID.randomUUID());

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }


}
