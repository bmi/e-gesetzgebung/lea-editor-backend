// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.PkpZuleitungDokumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeParentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.status.UpdateStatusDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.AbstimmungId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.services.CompoundDocumentService;
import de.itzbund.egesetz.bmi.lea.domain.services.DocumentService;
import de.itzbund.egesetz.bmi.lea.domain.services.PropositionService;
import de.itzbund.egesetz.bmi.lea.domain.services.egfa.datenuebernahme.EgfaService;
import de.itzbund.egesetz.bmi.lea.exception.EgfaException;
import de.itzbund.egesetz.bmi.lea.mappers.CompoundDocumentMapperRest;
import de.itzbund.egesetz.bmi.lea.util.TestDTOUtil;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@Log4j2
@ActiveProfiles("test")
@ExtendWith({SpringExtension.class}) // JUnit 5
@SuppressWarnings("unused")
class PlategControllerTest {

	private static final CompoundDocumentId myUUID = new CompoundDocumentId(UUID.randomUUID());
	private static final String userGid = UUID.randomUUID()
		.toString();
	private static UUID uuid;
	private static EgfaDatenuebernahmeDTO egfaDTO;
	private static EgfaDatenuebernahmeParentDTO egfaParentDTO;
	@InjectMocks
	private PlategController plategController;
	@Mock
	private CompoundDocumentService compoundDocumentService;
	@Mock
	private DocumentService documentService;
	@Mock
	private EgfaService egfaService;
	@Mock
	private PropositionService propositionService;
	@Mock
	private ClientRegistrationRepository clientRegistrationRepository;
	@Mock
	private CompoundDocumentMapperRest compoundDocumentMapperRest;

	@BeforeAll
	static void setUp() {
		uuid = UUID.randomUUID();
		egfaDTO = EgfaDatenuebernahmeDTO.builder()
			.build();
		egfaParentDTO = EgfaDatenuebernahmeParentDTO.builder().build();

	}


	@Test
	void whenDocumentIsNotFreezed_ItShouldBeFreezed() {
		// Arrange
		when(compoundDocumentService.changeCompoundDocumentStateByPlatform(userGid, myUUID, DocumentState.FREEZE))
			.thenReturn(ServiceState.OK);

		// Act
		ResponseEntity<?> responseEntity = plategController.veraendereDokumentenmappenStatusDurchPlattform(userGid,
			myUUID.getId()
				.toString(),
			UpdateStatusDTO.builder()
				.state(DocumentState.FREEZE)
				.build());

		// Assert
		assertThat(responseEntity.getStatusCode()).isEqualByComparingTo(HttpStatus.NO_CONTENT);
	}


	@Test
	void whenRequestedDocumentIsNotAvailable_ErrorNotFoundShouldBeShown() {
		// Arrange
		when(compoundDocumentService.changeCompoundDocumentStateByPlatform(userGid, myUUID, DocumentState.FREEZE))
			.thenReturn(ServiceState.DOCUMENT_NOT_FOUND);

		// Act
		ResponseEntity<?> responseEntity = plategController.veraendereDokumentenmappenStatusDurchPlattform(userGid,
			myUUID.getId()
				.toString(),
			UpdateStatusDTO.builder()
				.state(DocumentState.FREEZE)
				.build());

		// Assert
		assertThat(responseEntity.getStatusCode()).isEqualByComparingTo(HttpStatus.NOT_FOUND);

	}


	@Test
	void whenUserHasNoPermission_ErrorUnauthorizedShouldBeShown() {
		// Arrange
		when(compoundDocumentService.changeCompoundDocumentStateByPlatform(userGid, myUUID, DocumentState.FREEZE))
			.thenReturn(ServiceState.NO_PERMISSION);

		// Act
		ResponseEntity<?> responseEntity = plategController.veraendereDokumentenmappenStatusDurchPlattform(userGid,
			myUUID.getId()
				.toString(),
			UpdateStatusDTO.builder()
				.state(DocumentState.FREEZE)
				.build());

		// Assert
		assertThat(responseEntity.getStatusCode()).isEqualByComparingTo(HttpStatus.UNAUTHORIZED);
	}


	@Test
	void whenOtherErrorOccurs_ErrorBadRequestShouldBeShown() {
		// Arrange
		when(compoundDocumentService.changeCompoundDocumentStateByPlatform(userGid, myUUID, DocumentState.FREEZE))
			.thenReturn(ServiceState.DOCUMENT_DUPLICATION);

		// Act
		ResponseEntity<?> responseEntity = plategController.veraendereDokumentenmappenStatusDurchPlattform(userGid,
			myUUID.getId()
				.toString(),
			UpdateStatusDTO.builder()
				.state(DocumentState.FREEZE)
				.build());

		// Assert
		assertThat(responseEntity.getStatusCode()).isEqualByComparingTo(HttpStatus.BAD_REQUEST);
	}


	@Test
	void whenAValidRvIdIsGiven_AListOfCompoundDocumentsShouldBeReturned() {
		// Arrange
		List<CompoundDocumentSummaryDTO> expected = new ArrayList<>();
		expected.add(CompoundDocumentSummaryDTO.builder()
			.build());
		expected.add(CompoundDocumentSummaryDTO.builder()
			.build());

		when(compoundDocumentService.getCompoundDocumentsForRegelungsvorhaben(any(String.class),
			any(UUID.class),
			anyList())).thenAnswer(invocation ->
			{
				List<ServiceState> status = invocation.getArgument(2);
				status.add(ServiceState.OK);

				return expected;
			}
		);

		// Act
		ResponseEntity<List<CompoundDocumentSummaryDTO>> actual = plategController.getCompoundDocumentForRvId(userGid,
			myUUID.getId()
				.toString());

		// Assert
		assertThat(actual.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
		assertThat(actual.getBody()).asList()
			.hasSize(2);
	}


	@Test
	void whenAnInValidRvIdIsGiven_AnEmptyListOfCompoundDocumentsShouldBeReturned() {
		// Arrange
		List<CompoundDocumentSummaryDTO> expected = Collections.emptyList();
		when(compoundDocumentService.getCompoundDocumentsForRegelungsvorhaben(any(String.class),
			any(UUID.class),
			anyList())).thenAnswer(invocation ->
			{
				List<ServiceState> status = invocation.getArgument(2);
				status.add(ServiceState.OK);

				return expected;
			}
		);

		// Act
		ResponseEntity<List<CompoundDocumentSummaryDTO>> actual = plategController.getCompoundDocumentForRvId(userGid,
			myUUID.getId()
				.toString());

		// Assert
		assertThat(actual.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
		assertThat(actual.getBody()).asList()
			.isEmpty();
	}


	@Test
	void whenControllerGetsAnError_AnEmptyListOfCompoundDocumentsShouldBeReturned() {
		// Arrange
		List<CompoundDocumentSummaryDTO> expected = Collections.emptyList();
		when(compoundDocumentService.getCompoundDocumentsForRegelungsvorhaben(any(String.class),
			any(UUID.class),
			anyList())).thenAnswer(invocation ->
			{
				List<ServiceState> status = invocation.getArgument(2);
				status.add(ServiceState.NOT_IMPLEMENTED);

				return expected;
			}
		);

		// Act
		ResponseEntity<List<CompoundDocumentSummaryDTO>> actual = plategController.getCompoundDocumentForRvId(userGid,
			myUUID.getId()
				.toString());

		// Assert
		assertThat(actual.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
		assertThat(actual.getBody()).asList()
			.isEmpty();
	}


	@Test
	void whenAllDocsOfUserAreRequested_thenReturnListOfDescriptions() {
		String gid = Utils.getRandomString();
		String docId1 = UUID.randomUUID()
			.toString();
		String docId2 = UUID.randomUUID()
			.toString();
		List<String> compoundDocumentIds = List.of(docId1, docId2);
		List<ServiceState> status = new ArrayList<>();

		// Arange
		when(compoundDocumentService.getAllDocumentsFromCompoundDocuments(gid, compoundDocumentIds, status))
			.thenAnswer((Answer<List<CompoundDocument>>) invocation -> {
				List<ServiceState> status1 = invocation.getArgument(2);
				status1.add(ServiceState.OK);
				return List.of();
			});

		// Act
		ResponseEntity<List<CompoundDocumentSummaryDTO>> docDescriptions =
			plategController.getAllDocumentsFromCompoundDocument(gid, compoundDocumentIds);

		// Assert
		assertThat(docDescriptions.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
	}


	@Test
	void whenDokumentenmappenFuerAbstimmung_TheyShouldBeReturned() {
		// Arrange
		List<ServiceState> states = new ArrayList<>();

		when(compoundDocumentService.getCompoundDocumentsForAbstimmung(anyString(), any(RegelungsVorhabenId.class),
			any(CompoundDocumentId.class), any(AbstimmungId.class),
			anyList())).thenAnswer(invocation ->
			{
				List<ServiceState> status = invocation.getArgument(4);
				status.add(ServiceState.OK);
				return List.of();
			}
		);

		// Act
		ResponseEntity<List<CompoundDocumentSummaryDTO>> actual = plategController.getDokumentenmappenFuerAbstimmung(
			userGid, UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID());

		// Assert
		assertThat(actual.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
		assertThat(actual.getBody()).asList()
			.isEmpty();
	}


	@Test
	void whenDokumentenmappenFuerKabinettverfahren_TheyShouldBeReturned() {
		// Arrange
		List<ServiceState> states = new ArrayList<>();
		PkpZuleitungDokumentDTO randomPkpZuleitungDokumentDTO = TestDTOUtil.getRandomPkpZuleitungDokumentDTO();

		when(compoundDocumentService.getCompoundDocumentsForKabinett(anyString(), any(RegelungsVorhabenId.class),
			anyList())).thenAnswer(invocation ->
			{
				List<ServiceState> status = invocation.getArgument(2);
				status.add(ServiceState.OK);
				return List.of(randomPkpZuleitungDokumentDTO);
			}
		);

		// Act
		ResponseEntity<List<PkpZuleitungDokumentDTO>> actual = plategController.getCompoundDocumentForKabinett(
			userGid, UUID.randomUUID());

		// Assert
		assertThat(actual.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
		List<PkpZuleitungDokumentDTO> body = actual.getBody();
		assertThat(body).hasSize(1);
		assertThat(body.get(0)).isEqualTo(randomPkpZuleitungDokumentDTO);
	}


	@Test
	void whenDokumentenmappenFuerKabinettverfahrenFehlt_EinLeeresErgebnisSollZurueckKommen() {
		// Arrange
		List<ServiceState> states = new ArrayList<>();

		when(compoundDocumentService.getCompoundDocumentsForKabinett(anyString(), any(RegelungsVorhabenId.class),
			anyList())).thenAnswer(invocation ->
			{
				List<ServiceState> status = invocation.getArgument(2);
				status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
				return List.of();
			}
		);

		// Act
		ResponseEntity<List<PkpZuleitungDokumentDTO>> actual = plategController.getCompoundDocumentForKabinett(
			userGid, UUID.randomUUID());

		// Assert
		assertThat(actual.getStatusCode()).isEqualByComparingTo(HttpStatus.NO_CONTENT);
		assertThat(actual.getBody()).isNull();
	}


	@Test
	void when_UserHasNoReadPermission_then_Error403() {
		// Arrange
		doAnswer(invocation -> {
			List<ServiceState> status = invocation.getArgument(3);
			status.add(ServiceState.NO_PERMISSION);
			return null;
		})
			.when(egfaService)
			.egfaDatenuebernahme(any(), any(), any(), any());

		//Act
		EgfaException exception = Assertions.assertThrows(EgfaException.class, () ->
			plategController.egfaDatenuebernahme("user", uuid, egfaDTO));

		// Assert
		assertNotNull(exception);
		assertEquals(HttpStatus.FORBIDDEN, exception.getHttpStatus());
		assertNotNull(exception.getApiErrorDto());
		assertEquals("NO_PERMISSION", exception.getApiErrorDto()
			.getMessageCode());

	}


	@Test
	void when_RegulatoryProposalNotFound_then_Error404() {
		// Arrange
		doAnswer(invocation -> {
			List<ServiceState> status = invocation.getArgument(3);
			status.add(ServiceState.REGELUNGSVORHABEN_NOT_FOUND);
			return null;
		})
			.when(egfaService)
			.egfaDatenuebernahme(any(), any(), any(), any());

		EgfaException exception = Assertions.assertThrows(EgfaException.class, () ->
			plategController.egfaDatenuebernahme("user", uuid, egfaDTO));

		// Assert
		assertNotNull(exception);
		assertEquals(HttpStatus.NOT_FOUND, exception.getHttpStatus());
		assertNotNull(exception.getApiErrorDto());
		assertEquals("REGELUNGSVORHABEN_NOT_FOUND", exception.getApiErrorDto()
			.getMessageCode());
	}


	@Test
	void when_CompoundDocumentNotFound_then_Error404() {
		// Arrange
		doAnswer(invocation -> {
			List<ServiceState> status = invocation.getArgument(3);
			status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
			return null;
		})
			.when(egfaService)
			.egfaDatenuebernahme(any(), any(), any(), any());

		// Act
		EgfaException exception = Assertions.assertThrows(EgfaException.class, () ->
			plategController.egfaDatenuebernahme("user", uuid, egfaDTO));

		// Assert
		assertNotNull(exception);
		assertEquals(HttpStatus.NOT_FOUND, exception.getHttpStatus());
		assertNotNull(exception.getApiErrorDto());
		assertEquals("COMPOUND_DOCUMENT_NOT_FOUND", exception.getApiErrorDto()
			.getMessageCode());
	}


	@Test
	void when_AnyOtherErrorOccurs_then_Error500() {
		// Arrange
		doAnswer(invocation -> {
			List<ServiceState> status = invocation.getArgument(3);
			status.add(ServiceState.INVALID_STATE);
			return null;
		})
			.when(egfaService)
			.egfaDatenuebernahme(any(), any(), any(), any());

		// Act
		EgfaException exception = Assertions.assertThrows(EgfaException.class, () ->
			plategController.egfaDatenuebernahme("user", uuid, egfaDTO));

		// Assert
		assertNotNull(exception);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, exception.getHttpStatus());
		assertNotNull(exception.getApiErrorDto());
		assertEquals("ERROR_WHILE_PROCESSING", exception.getApiErrorDto()
			.getMessageCode());
	}


	@Test
	void when_ProcessingAlright_then_Success() {
		// Arrange
		doAnswer(invocation -> {
			List<ServiceState> status = invocation.getArgument(3);
			status.add(ServiceState.OK);
			return null;
		})
			.when(egfaService)
			.egfaDatenuebernahme(any(), any(), any(), any());

		// Act
		ResponseEntity<Void> response = plategController.egfaDatenuebernahme("user", UUID.randomUUID(),
			EgfaDatenuebernahmeDTO.builder()
				.build());

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertNull(response.getBody());
	}

	@Test
	void when_SaveAndRequestAlright_then_Success() {
		// Arrange
		doAnswer(invocation -> {
			List<ServiceState> status = invocation.getArgument(3);
			status.add(ServiceState.OK);
			return null;
		})
			.when(egfaService)
			.egfaDatenSpeicherung(any(), any(), any(), any());

		// Act
		ResponseEntity<Void> response = plategController.egfaDatenSpeicherung("user", UUID.randomUUID(),
			EgfaDatenuebernahmeParentDTO.builder()
				.build());

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertNull(response.getBody());
	}

	@Test
	void when_SaveAndUserHasNoReadPermission_then_Error403() {
		// Arrange
		doAnswer(invocation -> {
			List<ServiceState> status = invocation.getArgument(3);
			status.add(ServiceState.INVALID_ARGUMENTS);
			return null;
		})
			.when(egfaService)
			.egfaDatenSpeicherung(any(), any(), any(), any());

		//Act
		EgfaException exception = Assertions.assertThrows(EgfaException.class, () ->
			plategController.egfaDatenSpeicherung("user", uuid, egfaParentDTO));

		// Assert
		assertNotNull(exception);
		assertEquals(HttpStatus.BAD_REQUEST, exception.getHttpStatus());
		assertNotNull(exception.getApiErrorDto());
		assertEquals("INVALID_ARGUMENTS", exception.getApiErrorDto()
			.getMessageCode());

	}

	@Test
	void when_SaveAndAnyOtherErrorOccurs_then_Error500() {
		// Arrange
		doAnswer(invocation -> {
			List<ServiceState> status = invocation.getArgument(3);
			status.add(ServiceState.INVALID_STATE);
			return null;
		})
			.when(egfaService)
			.egfaDatenSpeicherung(any(), any(), any(), any());

		// Act
		EgfaException exception = Assertions.assertThrows(EgfaException.class, () ->
			plategController.egfaDatenSpeicherung("user", uuid, egfaParentDTO));

		// Assert
		assertNotNull(exception);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, exception.getHttpStatus());
		assertNotNull(exception.getApiErrorDto());
		assertEquals("ERROR_WHILE_PROCESSING", exception.getApiErrorDto()
			.getMessageCode());
	}

	@Test
	void when_SaveAndNotModified_then_Error_304() {
		// Arrange
		doAnswer(invocation -> {
			List<ServiceState> status = invocation.getArgument(3);
			status.add(ServiceState.NOT_MODIFIED);
			return null;
		})
			.when(egfaService)
			.egfaDatenSpeicherung(any(), any(), any(), any());

		EgfaException exception = Assertions.assertThrows(EgfaException.class, () ->
			plategController.egfaDatenSpeicherung("user", uuid, egfaParentDTO));

		// Assert
		assertNotNull(exception);
		assertEquals(HttpStatus.NOT_MODIFIED, exception.getHttpStatus());
		assertNotNull(exception.getApiErrorDto());
		assertEquals("NOT_MODIFIED", exception.getApiErrorDto()
			.getMessageCode());
	}


	@Test
	void when_BestandsrechtSaveAndRequestAlright_then_Success() {
		// Arrange
		doAnswer(invocation -> {
			List<ServiceState> status = invocation.getArgument(3);
			status.add(ServiceState.OK);
			return null;
		})
			.when(propositionService)
			.bestandsrechtDatenSpeicherung(any(), any(), any(), any());

		// Act
		ResponseEntity<Void> response = plategController.bestandsrechtDatenSpeicherung("user", UUID.randomUUID(),
			BestandsrechtDTO.builder()
				.build());

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertNull(response.getBody());
	}

	@Test
	void when_BestandsrechtSaveAndError_then_Success() {
		// Arrange
		doAnswer(invocation -> {
			List<ServiceState> status = invocation.getArgument(3);
			status.add(ServiceState.INVALID_STATE);
			return null;
		})
			.when(propositionService)
			.bestandsrechtDatenSpeicherung(any(), any(), any(), any());

		// Act
		ResponseEntity<Void> response = plategController.bestandsrechtDatenSpeicherung("user", UUID.randomUUID(),
			BestandsrechtDTO.builder()
				.build());

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
		assertNull(response.getBody());
	}

}
