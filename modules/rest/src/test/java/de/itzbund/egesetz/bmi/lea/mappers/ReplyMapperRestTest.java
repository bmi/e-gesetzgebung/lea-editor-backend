// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.mappers;

import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.ReplyDTO;
import de.itzbund.egesetz.bmi.lea.domain.mappers.BaseMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for {@link ReplyMapperRest}
 */
class ReplyMapperRestTest {

    private ReplyMapperRest replyMapper;


    @BeforeEach
    void init() {
        replyMapper = new ReplyMapperRestImpl(new BaseMapperImpl());
    }


    @Test
    void testMap() {
        ReplyDTO replyDTO = TestDTOUtil.getRandomReplyDTO();
        Reply actual = replyMapper.map(replyDTO);
        assertThat(actual).hasNoNullFieldsOrPropertiesExcept(
            "commentId",
            "documentId",
            "updatedBy",
            "createdBy",
            "updatedAt",
            "createdAt",
            "replyId");
    }


    @Test
    void testMapToReplyId() {
        Reply reply = TestObjectsUtil.getRandomReplyEntity();
        ReplyDTO actual = replyMapper.mapToReplyId(reply);
        assertThat(actual).hasNoNullFieldsOrProperties();
    }
}
