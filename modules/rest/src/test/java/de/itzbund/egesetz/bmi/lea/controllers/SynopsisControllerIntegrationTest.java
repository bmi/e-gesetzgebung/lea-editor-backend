// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import de.itzbund.egesetz.bmi.lea.api.filter.ContentBase64Filter;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.synopsis.SynopsisRequestDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.synopsis.SynopsisResponseDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.SynopsisRestPort;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@ExtendWith({SpringExtension.class}) // JUnit 5
class SynopsisControllerIntegrationTest {

    private static SynopsisRequestDTO synopsisRequestDTO;
    private static ObjectMapper objectMapper;
    @InjectMocks
    private SynopsisController synopsisController;
    @Mock
    private SynopsisRestPort synopsisService;
    @Mock
    private ContentBase64Filter contentBase64Filter;

    @BeforeAll
    static void setUp() {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new Jdk8Module());
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        synopsisRequestDTO = new SynopsisRequestDTO()
            .base(UUID.randomUUID())
            .addVersionsItem(UUID.randomUUID());
    }


    @Test
    void whenInvalidRequest_then_Error400() {
        // Arrange
        doAnswer(invocation -> {
            List<ServiceState> status = invocation.getArgument(1);
            status.add(ServiceState.INVALID_ARGUMENTS);
            return null;
        })
            .when(synopsisService)
            .getSynopse(any(), anyList());

        // Act
        ResponseEntity<SynopsisResponseDTO> response = synopsisController.getSynopsis(synopsisRequestDTO);

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }


    @Test
    void whenDocumentNotFound_then_Error404() {
        // Arrange
        doAnswer(invocation -> {
            List<ServiceState> status = invocation.getArgument(1);
            status.add(ServiceState.DOCUMENT_NOT_FOUND);
            return null;
        })
            .when(synopsisService)
            .getSynopse(any(), anyList());

        // Act
        ResponseEntity<SynopsisResponseDTO> response = synopsisController.getSynopsis(synopsisRequestDTO);

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }


    @Test
    void whenNoPermissionToRead_then_Error403() {
        // Arrange
        doAnswer(invocation -> {
            List<ServiceState> status = invocation.getArgument(1);
            status.add(ServiceState.NO_PERMISSION);
            return null;
        })
            .when(synopsisService)
            .getSynopse(any(), anyList());

        // Act
        ResponseEntity<SynopsisResponseDTO> response = synopsisController.getSynopsis(synopsisRequestDTO);

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
    }


    @Test
    void whenInternalError_then_Error500() {
        // Arrange
        doAnswer(invocation -> {
            List<ServiceState> status = invocation.getArgument(1);
            status.add(ServiceState.INVALID_STATE);
            return null;
        })
            .when(synopsisService)
            .getSynopse(any(), anyList());

        // Act
        ResponseEntity<SynopsisResponseDTO> response = synopsisController.getSynopsis(synopsisRequestDTO);

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }


    @Test
    @SneakyThrows
    void whenRequestAlright_thenSuccess() {
        // Arrange
        String baseContent = Utils.getRandomString();
        DocumentDTO baseDocumentDTO = DocumentDTO.builder()
            .content(Utils.base64Encode(baseContent))
            .build();

        SynopsisResponseDTO testSynopsisResponseDTO = new SynopsisResponseDTO()
            .type(SynopsisResponseDTO.TypeEnum.SYNOPSE)
            .ersteller(UserDTO.builder()
                .build())
            .erstellt(OffsetDateTime.now())
            .document(baseDocumentDTO);

        doAnswer(invocation -> {
            List<ServiceState> status = invocation.getArgument(1);
            status.add(ServiceState.OK);
            return testSynopsisResponseDTO;
        })
            .when(synopsisService)
            .getSynopse(any(), anyList());

        when(contentBase64Filter.encodeSynopsisResponseDTO(any())).thenReturn(testSynopsisResponseDTO);

        // Act
        ResponseEntity<SynopsisResponseDTO> response = synopsisController.getSynopsis(synopsisRequestDTO);

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        SynopsisResponseDTO synopsisResponseDTO = response.getBody();
        assertNotNull(synopsisResponseDTO);
        assertEquals(SynopsisResponseDTO.TypeEnum.SYNOPSE, synopsisResponseDTO.getType());
        assertNotNull(synopsisResponseDTO.getErsteller());
        assertNotNull(synopsisResponseDTO.getErstellt());
        DocumentDTO documentDTO = synopsisResponseDTO.getDocument();
        assertNotNull(documentDTO);
        assertEquals(baseContent, Utils.base64Decode(documentDTO
            .getContent()));
        String jsonString = objectMapper.writeValueAsString(synopsisResponseDTO);
        assertNotNull(jsonString);
        assertFalse(jsonString.isBlank());
    }

    @Test
    void whenTextCompare_thenGetHtml() {
        // Arrange

        // Act
        ResponseEntity<String> response = synopsisController.getTextCompareResult(
            Utils.getRandomString(),
            Utils.getRandomString());

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        String html = response.getBody();
        assertNotNull(html);
        assertFalse(html.isBlank());
    }

}
