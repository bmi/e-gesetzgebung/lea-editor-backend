// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.api.filter.ContentBase64Filter;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.EditorRollenUndRechte;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.UpdateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CompoundDocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.DocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.ExportRestPort;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.util.TestObjectsUtil;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@Log4j2
@ActiveProfiles("test")
@ExtendWith({SpringExtension.class}) // JUnit 5
class DocumentControllerIntegrationTest {

	// ----- Controller ------
	@InjectMocks
	private DocumentController documentController;

	// ----- Service ---------
	@Mock
	private DocumentRestPort documentService;
	@Mock
	private CompoundDocumentRestPort compoundDocumentService;
	@Mock
	private ExportRestPort exportService;

	@Mock
	private ContentBase64Filter base64Filter;
	@Mock
	private LeageSession session;
	@Mock
	private EditorRollenUndRechte editorRollenUndRechte;


	@BeforeEach
	public void setUp() {
		when(base64Filter.decodeUpdateDocumentDTO(any(UpdateDocumentDTO.class))).thenReturn(
			TestDTOUtil.getRandomUpdateDocumentDTO());
		documentServiceMocks();
	}


	private void documentServiceMocks() {
		when(documentService.getDocumentById(any(UUID.class), anyBoolean())).thenReturn(TestDTOUtil.getRandomDocumentDTO());
		when(compoundDocumentService.getCompoundDocumentDomainEntity(any())).thenReturn(TestObjectsUtil.getRandomCompoundDocument());
		when(editorRollenUndRechte.isAllowed(any(), any(), any(), any())).thenReturn(true);
		when(session.getUser()).thenReturn(TestObjectsUtil.getUserEntity());
		when(documentService.updateDocument(any(UUID.class), any(UpdateDocumentDTO.class), anyList())).thenAnswer(
			invocation ->
			{
				List<ServiceState> status = invocation.getArgument(2); // status
				status.add(ServiceState.OK);
				return TestDTOUtil.getRandomDocumentDTO();
			}
		);
	}


	@Test
	void getDocumentTest() {
		// Act
		ResponseEntity<DocumentDTO> response =
			documentController.get(UUID.randomUUID());

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	void getDocumentWithAnnotationsTest() {
		// Act
		ResponseEntity<DocumentDTO> response =
			documentController.getWithoutAnnotations(UUID.randomUUID());

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}


	@Test
	void updateDocumentTest() {
		// Arrange
		UpdateDocumentDTO updateDocumentDTO = TestDTOUtil.getRandomUpdateDocumentDTO();

		// Act
		ResponseEntity<DocumentDTO> response =
			documentController.update(UUID.randomUUID(), updateDocumentDTO);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}


	@Test
	void renameDocumentTest() {
		// Arrange
		UpdateDocumentDTO updateDocumentDTO = TestDTOUtil.getRandomUpdateDocumentDTO();

		// Act
		ResponseEntity<DocumentDTO> response =
			documentController.update(UUID.randomUUID(), updateDocumentDTO);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	void testWithoutData_expectBadRequest() {
		// Arrange
		UpdateDocumentDTO updateDocumentDTO = UpdateDocumentDTO.builder().build();
		when(base64Filter.decodeUpdateDocumentDTO(any(UpdateDocumentDTO.class))).thenReturn(updateDocumentDTO);

		// Act
		ResponseEntity<DocumentDTO> response =
			documentController.update(UUID.randomUUID(), updateDocumentDTO);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	void testInvalidDocument_expectNotFound() {
		// Arrange
		UpdateDocumentDTO updateDocumentDTO = UpdateDocumentDTO.builder().build();

		doAnswer((i) -> {
			List<ServiceState> status = i.getArgument(2); // status
			status.add(ServiceState.DOCUMENT_NOT_FOUND);
			return null;
		}).when(documentService).updateDocument(any(), any(), anyList());

		// Act
		ResponseEntity<DocumentDTO> response =
			documentController.update(UUID.randomUUID(), updateDocumentDTO);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	void testExport_whenJSONNotValid_thenError500() {
		// Arrange
		UpdateDocumentDTO updateDocumentDTO = TestDTOUtil.getRandomUpdateDocumentDTO();

		when(exportService.exportSingleDocument(anyString(), anyList()))
			.thenAnswer(invocation -> {
				List<ServiceState> status = invocation.getArgument(1);
				status.add(ServiceState.INVALID_ARGUMENTS);
				return null;
			});

		// Act
		ResponseEntity<String> response = documentController.exportDocument(updateDocumentDTO);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}

	@Test
	void testExport_whenXMLNotValid_thenError409() {
		// Arrange
		UpdateDocumentDTO updateDocumentDTO = TestDTOUtil.getRandomUpdateDocumentDTO();

		when(exportService.exportSingleDocument(anyString(), anyList()))
			.thenAnswer(invocation -> {
				List<ServiceState> status = invocation.getArgument(1);
				status.add(ServiceState.XML_TRANSFORM_ERROR);
				return null;
			});

		// Act
		ResponseEntity<String> response = documentController.exportDocument(updateDocumentDTO);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
	}

	@Test
	void testExport_whenNoFailure_thenSuccess200() {
		// Arrange
		UpdateDocumentDTO updateDocumentDTO = TestDTOUtil.getRandomUpdateDocumentDTO();

		when(exportService.exportSingleDocument(anyString(), anyList()))
			.thenAnswer(invocation -> {
				List<ServiceState> status = invocation.getArgument(1);
				status.add(ServiceState.OK);
				return null;
			});

		// Act
		ResponseEntity<String> response = documentController.exportDocument(updateDocumentDTO);

		// Assert
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

}
