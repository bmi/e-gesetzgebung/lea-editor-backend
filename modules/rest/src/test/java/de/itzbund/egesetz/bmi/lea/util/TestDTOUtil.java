// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.util;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.dtos.abstimmung.AbstimmungEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.abstimmung.CompoundDocumentEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CommentPositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CommentResponseDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CreateCommentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.PatchCommentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.ReplyDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.UpdateCommentStatusDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CopyCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CreateCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.UpdateCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.CreateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentImportDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentMetadataDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.PkpZuleitungDokumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.UpdateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.HomepageRegulatoryProposalDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.media.MediaShortSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.media.MediaSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.ProponentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorShortDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RessortEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.synopsis.SynopsisResponseDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserRightsDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.AbstimmungstypType;
import de.itzbund.egesetz.bmi.lea.domain.enums.CommentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentAccessType;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.Freigabetyp;
import de.itzbund.egesetz.bmi.lea.domain.enums.PropositionState;
import de.itzbund.egesetz.bmi.lea.domain.enums.RegelungsvorhabenTypType;
import de.itzbund.egesetz.bmi.lea.domain.enums.VerfahrensType;
import de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentInitiant;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import lombok.Getter;
import lombok.experimental.UtilityClass;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Getter
@UtilityClass
@SuppressWarnings("unused")
public class TestDTOUtil {

    public static final UUID REGELUNGSVORHABEN_ID = UUID.randomUUID();
    public static final String REGELUNGSVORHABEN_ABKUERZUNG = "Abk.";
    public static final String REGELUNGSVORHABEN_BEZEICHNUNG = "Bezeichnung";
    public static final String REGELUNGSVORHABEN_KURZBESCHREIBUNG = "Kurzbeschreibung";
    public static final String REGELUNGSVORHABEN_KURZBEZEICHNUNG = "Titel, aka: Kurz Bezeichnung";
    public static final RegelungsvorhabenTypType REGELUNGSVORHABEN_ART = RegelungsvorhabenTypType.GESETZ;
    public static final VorhabenStatusType REGELUNGSVORHABEN_STATUS = VorhabenStatusType.IN_BEARBEITUNG;
    public static final RechtsetzungsdokumentInitiant REGELUNGSVORHABEN_INITIANT = RechtsetzungsdokumentInitiant.BUNDESREGIERUNG;
    public static final String COMPOUND_DOCUMENT_TITLE = "Compound document title";
    public static final CompoundDocumentTypeVariant COMPOUND_DOCUMENT_TYPE = CompoundDocumentTypeVariant.STAMMGESETZ;
    public static final String DOCUMENT_TITLE = "Document title";
    public static final String DOCUMENT_VERSION = "8";
    public static final DocumentType DOCUMENT_TYPE = DocumentType.REGELUNGSTEXT_STAMMGESETZ;
    public static final String UUID_AS_STRING = "UUID as String";
    public static final String SOME_GID = "Some Gid";
    public static final String USER_NAME = "User name";
    public static final String SOME_EMAIL = "Some Email";
    private static final UUID COMPOUND_DOCUMENT_ID = UUID.randomUUID();
    private static final UUID DOCUMENT_ID = UUID.randomUUID();
    private static final UUID ABSTIMMUNG_ID = UUID.randomUUID();
    private static final String REGULATORY_TITLE_TEXT = "Title Regulatory Text";
    private static final String REPLY_CONTENT = "Reply content";
    private static final String XML_CONTENT = "<?xml version='1.0' encoding='UTF-8'?><?xml-model href=\"Grammatiken/legalDocML.de.sch\" type=\"application/xml\" schematypens=\"http://purl.oclc.org/dsdl/schematron\"?><akn:akomaNtoso xsi:schemaLocation=\"http://Inhaltsdaten.LegalDocML.de/1.4/ Grammatiken/legalDocML.de-regelungstextentwurfsfassung.xsd http://Metadaten.LegalDocML.de/1.4/ Grammatiken/legalDocML.de-metadaten.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:akn=\"http://Inhaltsdaten.LegalDocML.de/1.4/\"><akn:bill name=\"regelungstext\"><akn:meta><akn:identification source=\"attributsemantik-noch-undefiniert\"><akn:FRBRWork><akn:FRBRthis value=\"eli/dl/2023/bundesregierung/1/regelungsentwurf/regelungstext-1\"></akn:FRBRthis><akn:FRBRuri value=\"eli/dl/2023/bundesregierung/1/regelungsentwurf\"></akn:FRBRuri><akn:FRBRalias name=\"übergreifende-id\" value=\"00000000-0000-0000-0000-000000000001\"></akn:FRBRalias><akn:FRBRdate date=\"2023-04-01\" name=\"entwurfsfassung\"></akn:FRBRdate><akn:FRBRauthor href=\"recht.bund.de/institution/bundesregierung\"></akn:FRBRauthor><akn:FRBRcountry value=\"de\"></akn:FRBRcountry><akn:FRBRnumber value=\"1\"></akn:FRBRnumber><akn:FRBRname value=\"regelungsentwurf\"></akn:FRBRname><akn:FRBRsubtype value=\"regelungstext-1\"></akn:FRBRsubtype></akn:FRBRWork><akn:FRBRExpression><akn:FRBRthis value=\"eli/dl/2023/bundesregierung/1/regelungsentwurf/bundestag/2023-04-01/1/deu/regelungstext-1\"></akn:FRBRthis><akn:FRBRuri value=\"eli/dl/2023/bundesregierung/1/regelungsentwurf/bundestag/2023-04-01/1/deu\"></akn:FRBRuri><akn:FRBRauthor href=\"recht.bund.de/institution/bundestag\"></akn:FRBRauthor><akn:FRBRdate date=\"2023-04-01\" name=\"version\"></akn:FRBRdate><akn:FRBRlanguage language=\"deu\"></akn:FRBRlanguage><akn:FRBRversionNumber value=\"1\"></akn:FRBRversionNumber></akn:FRBRExpression><akn:FRBRManifestation><akn:FRBRthis value=\"eli/dl/2023/bundesregierung/1/regelungsentwurf/bundestag/2023-04-01/1/deu/regelungstext-1.xml\"></akn:FRBRthis><akn:FRBRuri value=\"eli/dl/2023/bundesregierung/1/regelungsentwurf/bundestag/2023-04-01/1/deu/regelungstext-1.xml\"></akn:FRBRuri><akn:FRBRdate date=\"2023-04-01\" name=\"generierung\"></akn:FRBRdate><akn:FRBRauthor href=\"recht.bund.de\"></akn:FRBRauthor><akn:FRBRformat value=\"xml\"></akn:FRBRformat></akn:FRBRManifestation></akn:identification><akn:proprietary source=\"attributsemantik-noch-undefiniert\"><meta:legalDocML.de_metadaten xmlns:meta=\"http://Metadaten.LegalDocML.de\"><meta:typ>gesetz</meta:typ><meta:form>stammform</meta:form><meta:fassung>entwurfsfassung</meta:fassung><meta:art>regelungstext</meta:art><meta:initiant>bundesregierung</meta:initiant><meta:bearbeitendeInstitution>bundesregierung</meta:bearbeitendeInstitution></meta:legalDocML.de_metadaten></akn:proprietary></akn:meta><akn:preface><akn:longTitle eId=\"doktitel-1\" GUID=\"646481cc-657e-4ae8-adf5-9f4da60745be\"><akn:p eId=\"doktitel-1_text-1\" GUID=\"d2c84033-c44e-4202-aab6-c1c017db4785\"><akn:docStage>Referentenentwurf</akn:docStage><akn:span refersTo=\"artikel\"> des </akn:span><akn:docProponent>Bundesministeriums des Innern und für Heimat</akn:docProponent><akn:docTitle>2023 - AK-RV - Bezeichnung</akn:docTitle>(<akn:shortTitle eId=\"doktitel-1_text-1_kurztitel-1\" GUID=\"03f5c9ef-6423-4fdf-8b48-15518b4d3938\">2023 - AK-RV - Finger weg ;-)</akn:shortTitle> - <akn:inline refersTo=\"amtliche-abkuerzung\" name=\"attributsemantik-noch-undefiniert\">2023 - AK-RV - Abkürzung</akn:inline>)</akn:p></akn:longTitle><akn:block eId=\"block-1\" GUID=\"cf02647e-b17b-486b-88fe-cd0144aa4760\" name=\"attributsemantik-noch-undefiniert\"><akn:date refersTo=\"ausfertigung-datum\" date=\"1900-01-01\">Vom [Platzhalter]</akn:date></akn:block></akn:preface><akn:preamble><akn:formula refersTo=\"eingangsformel\" eId=\"formel-1\" GUID=\"bb2b4299-e637-418d-8006-2355979832ab\" name=\"attributsemantik-noch-undefiniert\"><akn:p eId=\"formel-1_text-1\" GUID=\"5dfcde34-daf6-47b9-987d-1bbfcdee9346\">Der Bundestag hat das folgende Gesetz beschlossen:</akn:p></akn:formula></akn:preamble><akn:body refersTo=\"hauptteil-stammform\"><akn:article refersTo=\"einzelvorschrift-stammform\" eId=\"para-1\" GUID=\"5c11c7f5-f4c2-4e95-b6c7-cf3f67e08d59\"><akn:num eId=\"para-1_bezeichnung-1\" GUID=\"53c1ab15-b840-40e7-83d9-124154d1d072\"><akn:marker name=\"1\"></akn:marker>§ 1</akn:num><akn:heading eId=\"para-1_überschrift-1\" GUID=\"956b7d0c-9be3-4c42-9024-551d59b1a342\">[Platzhalter]</akn:heading><akn:paragraph eId=\"para-1_abs-1\" GUID=\"d2d4abac-0045-459d-a633-12e3d465c5cd\"><akn:num eId=\"para-1_abs-1_bezeichnung-1\" GUID=\"86a0660a-4650-4999-8735-616a1899ec10\"><akn:marker name=\"1\"></akn:marker></akn:num><akn:content eId=\"para-1_abs-1_inhalt-1\" GUID=\"1d817092-cecf-4136-8498-588a182624d8\"><akn:p eId=\"para-1_abs-1_inhalt-1_text-1\" GUID=\"6ed797fd-e66f-4297-9756-24d9e323ed8d\">[Platzhalter]</akn:p></akn:content></akn:paragraph></akn:article><akn:article refersTo=\"einzelvorschrift-stammform|geltungszeitregel\" eId=\"para-2\" GUID=\"15603272-3fa7-46be-bce4-a72030b4928f\"><akn:num eId=\"para-2_bezeichnung-1\" GUID=\"cc9d2eb7-9b1d-489a-a800-308510682566\"><akn:marker name=\"2\"></akn:marker>§ 2</akn:num><akn:heading eId=\"para-2_überschrift-1\" GUID=\"3c1a1eb8-db0a-4002-8f38-703b509a617e\">[Platzhalter]</akn:heading><akn:paragraph eId=\"para-2_abs-1\" GUID=\"0c55f249-d9fc-41f2-ba8b-5087ba9c7b0c\"><akn:num eId=\"para-2_abs-1_bezeichnung-1\" GUID=\"9d042c3d-6cf2-4dad-8d52-a7e8cb3a7d62\"><akn:marker name=\"1\"></akn:marker></akn:num><akn:content eId=\"para-2_abs-1_inhalt-1\" GUID=\"5065d447-6d2c-4d06-b3f7-4621873fb316\"><akn:p eId=\"para-2_abs-1_inhalt-1_text-1\" GUID=\"a537209a-b0d5-4eeb-924a-bb53f6f5410f\">[Platzhalter]</akn:p></akn:content></akn:paragraph></akn:article></akn:body><akn:conclusions></akn:conclusions></akn:bill></akn:akomaNtoso>";
    private static final String EMPTY_DOCUMENT = "{\"type\":\"test\",\"children\":[{\"type\":\"akn:bill\",\"children\":[{\"type\":\"akn:body\",\"children\":[{\"type\":\"lea:textWrapper\",\"children\":[{\"text\":\"\"}]}]}]}]}";

    public RegelungsvorhabenEditorDTO getRandomRegelungsvorhabenEditorDTO() {
        return RegelungsvorhabenEditorDTO.builder()
            .id(REGELUNGSVORHABEN_ID)
            .abkuerzung(REGELUNGSVORHABEN_ABKUERZUNG)
            .kurzbezeichnung(REGELUNGSVORHABEN_KURZBEZEICHNUNG)
            .kurzbeschreibung(REGELUNGSVORHABEN_KURZBESCHREIBUNG)
            .status(REGELUNGSVORHABEN_STATUS)
            .vorhabenart(REGELUNGSVORHABEN_ART)
            .bearbeitetAm(Instant.now())

            .bezeichnung(REGELUNGSVORHABEN_BEZEICHNUNG)
            .technischesFfRessort(getRandomRessortEditorDTO())
            .allFfRessorts(new ArrayList<>())
            .initiant(REGELUNGSVORHABEN_INITIANT)
            .build();
    }


    public RegelungsvorhabenEditorShortDTO getRandomRegelungsvorhabenEditorShortDTO() {
        return RegelungsvorhabenEditorShortDTO.builder()
            .id(REGELUNGSVORHABEN_ID)
            .abkuerzung(REGELUNGSVORHABEN_ABKUERZUNG)
            .kurzbezeichnung(REGELUNGSVORHABEN_KURZBEZEICHNUNG)
            .kurzbeschreibung(REGELUNGSVORHABEN_KURZBESCHREIBUNG)
            .status(REGELUNGSVORHABEN_STATUS)
            .vorhabenart(REGELUNGSVORHABEN_ART)
            .bearbeitetAm(Instant.now())
            .build();
    }


    public static RessortEditorDTO getRandomRessortEditorDTO() {
        return RessortEditorDTO.builder()
            .id(UUID.randomUUID().toString())
            .kurzbezeichnung(Utils.getRandomString())
            .bezeichnungNominativ(Utils.getRandomString())
            .bezeichnungGenitiv(Utils.getRandomString())
            .build();
    }


    public RegelungsvorhabenEditorDTO getRegelungsvorhabenEditorDTOWithSpecificId(UUID id) {
        RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = getRandomRegelungsvorhabenEditorDTO();
        regelungsvorhabenEditorDTO.setId(id);
        return regelungsvorhabenEditorDTO;
    }


    public RegelungsvorhabenEditorDTO getDummyRegelungsvorhabenEditorDTO() {
        return RegelungsvorhabenEditorDTO.builder()
            .id(UUID.fromString("00000000-0000-0000-0000-000000000000"))
            .bezeichnung("nicht verfügbar")
            .kurzbezeichnung("n.v.")
            .kurzbeschreibung("n.v.")
            .abkuerzung("n.v.")
            .status(VorhabenStatusType.ENTWURF)
            .technischesFfRessort(
                RessortEditorDTO.builder()
                    .id("00000000-0000-0000-0000-000000000000")
                    .bezeichnungNominativ("n.v.")
                    .bezeichnungGenitiv("n.v.")
                    .aktiv(true)
                    .build()
            )
            .initiant(RechtsetzungsdokumentInitiant.BUNDESTAG)
            .vorhabenart(RegelungsvorhabenTypType.GESETZ)
            .build();
    }


    public CreateCompoundDocumentDTO getRandomCreateCompoundDocumentDTO() {
        return CreateCompoundDocumentDTO.builder()
            .regelungsVorhabenId(REGELUNGSVORHABEN_ID.toString())
            .titleRegulatoryText(REGULATORY_TITLE_TEXT)
            .title(COMPOUND_DOCUMENT_TITLE)
            .type(COMPOUND_DOCUMENT_TYPE)
            .build();
    }


    public CreateDocumentDTO getRandomCreateDocumentDTO() {
        return CreateDocumentDTO.builder()
            .title(DOCUMENT_TITLE)
            .initialNumberOfLevels(0)
            .type(DOCUMENT_TYPE)
            .build();
    }


    public CreateCommentDTO getRandomCreateCommentDTO() {
        return CreateCommentDTO.builder()
            .content(Utils.base64Encode(Utils.getRandomString()))
            .anchor(CommentPositionDTO.builder()
                .offset(0)
                .path(List.of())
                .build())
            .focus(CommentPositionDTO.builder()
                .offset(4)
                .path(List.of())
                .build())
            .build();
    }


    public DocumentMetadataDTO getRandomDocumentMetadataDTO() {
        return DocumentMetadataDTO.builder()
            .id(DOCUMENT_ID)
            .compoundDocumentId(COMPOUND_DOCUMENT_ID)
            .title(DOCUMENT_TITLE)
            .type(DOCUMENT_TYPE)
            .state(DocumentState.DRAFT)
            .proposition(getRandomPropositionDTO())
            .createdAt(Instant.now().toString())
            .updatedAt(Instant.now().toString())
            .createdBy(getRandomUserDTO())
            .updatedBy(getRandomUserDTO())
            .build();
    }


    public CopyCompoundDocumentDTO getRandomCopyCompoundDocumentDTO() {
        return CopyCompoundDocumentDTO.builder()
            .title(DOCUMENT_TITLE)
            .compoundDocumentId(new CompoundDocumentId(COMPOUND_DOCUMENT_ID))
            .inheritFromId(new CompoundDocumentId(UUID.randomUUID()))
            .regelungsVorhabenId(new RegelungsVorhabenId(REGELUNGSVORHABEN_ID))
            .type(COMPOUND_DOCUMENT_TYPE)
            .version(DOCUMENT_VERSION)
            .state(DocumentState.DRAFT)
            .createdAt(Instant.now())
            .updatedAt(Instant.now())
            .documents(TestObjectsUtil.getDocumentListOfSize(3))
            .build();
    }


    public CompoundDocumentSummaryDTO getRandomCompoundDocumentSummaryDTO() {
        return CompoundDocumentSummaryDTO.builder()
            .id(COMPOUND_DOCUMENT_ID)
            .documents(Arrays.asList(getRandomDocumentSummaryDTO(), getRandomDocumentSummaryDTO(),
                getRandomDocumentSummaryDTO()))
            .state(DocumentState.DRAFT)
            .version("5")
            .title(COMPOUND_DOCUMENT_TITLE)
            .type(COMPOUND_DOCUMENT_TYPE)
            .createdAt(Instant.now())
            .updatedAt(Instant.now())
            .build();
    }


    public DocumentSummaryDTO getRandomDocumentSummaryDTO() {
        return DocumentSummaryDTO.builder()
            .id(UUID.randomUUID())
            .title(DOCUMENT_TITLE)
            .state(DocumentState.DRAFT)
            .type(DOCUMENT_TYPE)
            .version(DOCUMENT_VERSION)
            .createdAt(Instant.now())
            .updatedAt(Instant.now())
            .build();
    }


    public UserDTO getRandomUserDTO() {
        return UserDTO.builder()
            .gid(SOME_GID)
            .name(USER_NAME)
            .email(SOME_EMAIL)
            .build();
    }


    public ReplyDTO getRandomReplyDTO() {
        return ReplyDTO.builder()
            .replyId(UUID.randomUUID())
            .parentId(UUID_AS_STRING)
            .content(REPLY_CONTENT)
            .parentId(UUID_AS_STRING)
            .createdAt(Instant.now().toString())
            .updatedAt(Instant.now().toString())
            .createdBy(getRandomUserDTO())
            .updatedBy(getRandomUserDTO())
            .build();
    }


    public CommentPositionDTO getRandomCommentPositionDTO() {
        return CommentPositionDTO.builder()
            .path(List.of(1L, 2L, 3L))
            .offset(123L)
            .build();
    }


    public CompoundDocumentDTO getRandomCompoundDocumentDTO() {
        return CompoundDocumentDTO.builder()
            .id(UUID.randomUUID())
            .title(COMPOUND_DOCUMENT_TITLE)
            .version(DOCUMENT_VERSION)
            .proposition(getRandomPropositionDTO())
            .type(COMPOUND_DOCUMENT_TYPE)
            .createdAt(Instant.now())
            .updatedAt(Instant.now())
            .documents(getDocumentDTOListOfSize(3))
            .build();
    }


    public PropositionDTO getRandomPropositionDTO() {
        return PropositionDTO.builder()
            .id(UUID.randomUUID())
            .title("PROPOSITION_TITLE")
            .shortTitle("SHORT_TITLE")
            .abbreviation("ABKUERZUNG")
            .state(PropositionState.IN_BEARBEITUNG)
            .proponentDTO(getRandomProponentDTO())
            .initiant(RechtsetzungsdokumentInitiant.BUNDESREGIERUNG)
            .type(RechtsetzungsdokumentTyp.VERWALTUNGSVORSCHRIFT)
            .build();
    }


    public ProponentDTO getRandomProponentDTO() {
        return ProponentDTO.builder()
            .id(UUID.randomUUID())
            .title("PROPONENT_TITLE")
            .designationGenitive("DES_GENETIVS")
            .designationNominative("DER_NOMINATIV")
            .active(true)
            .build();
    }


    public List<DocumentDTO> getDocumentDTOListOfSize(int size) {
        List<DocumentDTO> documentDtos = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            documentDtos.add(getRandomDocumentDTO());
        }

        return documentDtos;
    }


    public DocumentDTO getRandomDocumentDTO() {
        return DocumentDTO.builder()
            .id(DOCUMENT_ID)
            .compoundDocumentId(COMPOUND_DOCUMENT_ID)
            .title(DOCUMENT_TITLE)
            .type(DOCUMENT_TYPE)
            .state(DocumentState.DRAFT)
            .proposition(getRandomPropositionDTO())
            .createdAt(Instant.now().toString())
            .updatedAt(Instant.now().toString())
            .createdBy(getRandomUserDTO())
            .updatedBy(getRandomUserDTO())
            .content("DOCUMENT_CONTENT")
            .version("1.0")
            .build();
    }


    public CompoundDocumentEditorDTO getRandomCompoundDocumentEditorDTO() {
        return CompoundDocumentEditorDTO.builder()
            .compoundDocumentId(COMPOUND_DOCUMENT_ID)
            .regelungsvorhabenId(REGELUNGSVORHABEN_ID)
            .abstimmungen(getAbstimmungEditorDTOListOfSize(3))
            .build();
    }


    public AbstimmungEditorDTO getRandomAbstimmungEditorDTO() {
        return AbstimmungEditorDTO.builder()
            .abstimmungId(ABSTIMMUNG_ID)
            .erstelltAm(Instant.now())
            .bearbeitetAm(Instant.now())
            .abstimmungsTyp(AbstimmungstypType.HAUSABSTIMMUNG)
            .accessType(CompoundDocumentAccessType.EINLADUNG_AN_TEILNEHMER)
            .build();
    }


    public List<AbstimmungEditorDTO> getAbstimmungEditorDTOListOfSize(int size) {
        List<AbstimmungEditorDTO> abstimmungEditorDtos = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            abstimmungEditorDtos.add(getRandomAbstimmungEditorDTO());
        }

        return abstimmungEditorDtos;
    }


    public PkpZuleitungDokumentDTO getRandomPkpZuleitungDokumentDTO() {
        return PkpZuleitungDokumentDTO.builder()
            .dokumentenMappeId(COMPOUND_DOCUMENT_ID)
            .dokumentName(COMPOUND_DOCUMENT_TITLE)
            .dokumentInhalt("Base64 Coded Stuff")
            .pkpTyp(DocumentType.RECHTSETZUNGSDOKUMENT_STAMMGESETZ)
            .build();
    }


    public PatchCommentDTO getRandomPatchCommentDTO() {
        return PatchCommentDTO.builder()
            .anchor(getRandomCommentPositionDTO())
            .focus(getRandomCommentPositionDTO())
            .content("New content")
            .build();
    }


    public static UpdateCommentStatusDTO getRandomUpdateCommentStatusDTO() {
        return UpdateCommentStatusDTO.builder()
            .state(CommentState.CLOSED)
            .build();
    }


    public static DocumentImportDTO getRandomDocumentImportDTO() {
        return DocumentImportDTO.builder()
            .title(DOCUMENT_TITLE)
            .xmlContent(XML_CONTENT)
            .build();
    }


    public static UpdateCompoundDocumentDTO getRandomUpdateCompoundDocumentDTO() {
        return UpdateCompoundDocumentDTO.builder()
            .title(COMPOUND_DOCUMENT_TITLE)
            .state(DocumentState.BEREIT_FUER_KABINETT)
            .verfahrensTyp(VerfahrensType.REFERENTENENTWURF)
            .build();
    }


    public static UserRightsDTO getRandomUserRightsDTO() {
        UserDTO randomUser = getRandomUserDTO();

        return UserRightsDTO.builder()
            .gid(randomUser.getGid())
            .freigabetyp(Freigabetyp.LESERECHTE)
            .build();
    }


    public static List<UserRightsDTO> getRandomUserRightsDTOList(int size) {
        if (size == 0) {
            return Collections.emptyList();
        }

        List<UserRightsDTO> answer = new ArrayList<>();
        answer.add(getRandomUserRightsDTO());

        for (int i = 1; i < size; i++) {
            UserRightsDTO another = UserRightsDTO.builder()
                .gid(UUID.randomUUID().toString())
                .freigabetyp(Freigabetyp.LESERECHTE)
                .build();
        }

        return answer;
    }


    public static UpdateDocumentDTO getRandomUpdateDocumentDTO() {
        return UpdateDocumentDTO.builder()
            .content("TmV1ZXIgQ29udGVudA==").build();
    }


    public static List<HomepageRegulatoryProposalDTO> getRandomHomepageRegulatoryProposalDTOList() {
        return Collections.singletonList(HomepageRegulatoryProposalDTO.builder()
            .abbreviation("Abk")
            .id(UUID.randomUUID())
            .children(Collections.emptyList())
            .build());
    }


    public static MediaSummaryDTO getRandomMediaSummeryDTO() {
        return MediaSummaryDTO.builder()
            .id(UUID.randomUUID())
            .name("Some name")
            .mediaType(MediaType.APPLICATION_PDF)
            .length(100)
            .byteArrayResource(new ByteArrayResource(new byte[1], ""))
            .build();
    }


    public static MediaShortSummaryDTO getRandomMediaShortSummeryDTO() {
        return MediaShortSummaryDTO.builder()
            .id(UUID.randomUUID())
            .name("Some name")
            .mediaType(MediaType.APPLICATION_PDF)
            .length(100)
            .build();
    }


    public static CommentResponseDTO getRandomCommentResponseDTO() {
        return CommentResponseDTO.builder()
            .content("Some content")
            .replies(Collections.emptyList())
            .build();
    }

    public static SynopsisResponseDTO getRandomSynopsisResponseDTO() {
        SynopsisResponseDTO dto = new SynopsisResponseDTO();
        dto.setType(SynopsisResponseDTO.TypeEnum.SYNOPSE);
        dto.setDocument(
            DocumentDTO.builder()
                .content(EMPTY_DOCUMENT)
                .build());
        return dto;
    }

}
