// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.DrucksacheRestPort;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith({SpringExtension.class}) // JUnit 5
@SuppressWarnings("unused")
class DrucksachenPlategControllerIntegrationTest {

    // ----- Controller ------
    @InjectMocks
    private DrucksachenPlategPlategController drucksachenPlategController;
    // ----- Service ---------
    @Mock
    private DrucksacheRestPort drucksachenService;

    // ================================================================

    @Test
    void whenCompoundDocumentIsFreeze_thenCommentAddingIsAllowed() {
        // Arrange
        when(drucksachenService.drucksachenNummerAnlegen(any(), any())).thenReturn(List.of(ServiceState.OK));

        // Act
        ResponseEntity<Void> response = drucksachenPlategController.drucksachenNummerUebermittlung("anyGID", UUID.randomUUID(), "123/2004");

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void whenFehlenderBundesratStatus() {
        // Arrange
        when(drucksachenService.drucksachenNummerAnlegen(any(), any())).thenReturn(List.of(ServiceState.FEHLENDER_STATUS_BUNDESRAT));

        // Act
        ResponseEntity<Void> response = drucksachenPlategController.drucksachenNummerUebermittlung("anyGID", UUID.randomUUID(), "123/2004");

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode());
    }

    @Test
    void whenDrucksachennummerVergeben() {
        // Arrange
        when(drucksachenService.drucksachenNummerAnlegen(any(), any())).thenReturn(List.of(ServiceState.DRUCKSACHENNUMMER_BEREITS_VERGEBEN));

        // Act
        ResponseEntity<Void> response = drucksachenPlategController.drucksachenNummerUebermittlung("anyGID", UUID.randomUUID(), "123/2004");

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.NOT_ACCEPTABLE, response.getStatusCode());
    }

    @Test
    void whenDrucksacheFuerDasRegelungsvorhabenVergeben() {
        // Arrange
        when(drucksachenService.drucksachenNummerAnlegen(any(), any())).thenReturn(List.of(ServiceState.DRUCKSACHE_FUER_REGELUNGSVORHABEN_VORHANDEN));

        // Act
        ResponseEntity<Void> response = drucksachenPlategController.drucksachenNummerUebermittlung("anyGID", UUID.randomUUID(), "123/2004");

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.NOT_ACCEPTABLE, response.getStatusCode());
    }

    @Test
    void whenPersistierenNichtFunktioniert() {
        // Arrange
        when(drucksachenService.drucksachenNummerAnlegen(any(), any())).thenReturn(List.of(ServiceState.UNKNOWN_ERROR));

        // Act
        ResponseEntity<Void> response = drucksachenPlategController.drucksachenNummerUebermittlung("anyGID", UUID.randomUUID(), "123/2004");

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.SERVICE_UNAVAILABLE, response.getStatusCode());
    }

}
