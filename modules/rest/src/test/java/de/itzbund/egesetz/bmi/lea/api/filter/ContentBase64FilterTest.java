// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.api.filter;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CommentResponseDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CreateCommentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.PatchCommentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.ReplyDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentImportDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.UpdateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.synopsis.SynopsisResponseDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.mappers.ReplyMapperRest;
import de.itzbund.egesetz.bmi.lea.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class ContentBase64FilterTest {

    @InjectMocks
    private ContentBase64Filter contentBase64Filter;

    @Mock
    private ReplyMapperRest replyMapperRest;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    void testEncodeDocumentDTO() {
        DocumentDTO documentDTO = new DocumentDTO();
        documentDTO.setContent("SampleContent");

        DocumentDTO encodedDTO = contentBase64Filter.encodeDocumentDTO(documentDTO);

        assertEquals(Utils.base64Encode("SampleContent"), encodedDTO.getContent());
    }


    @Test
    void testEncodeDocumentDTOList() {
        DocumentDTO documentDTO1 = new DocumentDTO();
        documentDTO1.setContent("SampleContent1");

        DocumentDTO documentDTO2 = new DocumentDTO();
        documentDTO2.setContent("SampleContent2");

        List<DocumentDTO> documentDTOS = Arrays.asList(documentDTO1, documentDTO2);

        List<DocumentDTO> encodedList = contentBase64Filter.encodeDocumentDTOList(documentDTOS);

        assertEquals(Utils.base64Encode("SampleContent1"), encodedList.get(0).getContent());
        assertEquals(Utils.base64Encode("SampleContent2"), encodedList.get(1).getContent());
    }


    @Test
    void testDecodeUpdateDocumentDTO() {
        UpdateDocumentDTO updateDocumentDTO = new UpdateDocumentDTO();
        updateDocumentDTO.setContent(Utils.base64Encode("EncodedContent"));

        UpdateDocumentDTO decodedDTO = contentBase64Filter.decodeUpdateDocumentDTO(updateDocumentDTO);

        assertEquals("EncodedContent", decodedDTO.getContent());
    }


    @Test
    void testEncodeCompoundDocumentDTO() {
        DocumentDTO documentDTO = new DocumentDTO();
        documentDTO.setContent("SampleContent");

        CompoundDocumentDTO compoundDocumentDTO = new CompoundDocumentDTO();
        compoundDocumentDTO.setDocuments(Collections.singletonList(documentDTO));

        CompoundDocumentDTO encodedDTO = contentBase64Filter.encodeCompoundDocumentDTO(compoundDocumentDTO);

        assertEquals(Utils.base64Encode("SampleContent"), encodedDTO.getDocuments().get(0).getContent());
    }


    @Test
    void testEncodeCompoundDocumentDTOList() {
        DocumentDTO documentDTO1 = new DocumentDTO();
        documentDTO1.setContent("SampleContent1");

        DocumentDTO documentDTO2 = new DocumentDTO();
        documentDTO2.setContent("SampleContent2");

        CompoundDocumentDTO compoundDocumentDTO1 = new CompoundDocumentDTO();
        compoundDocumentDTO1.setDocuments(Collections.singletonList(documentDTO1));

        CompoundDocumentDTO compoundDocumentDTO2 = new CompoundDocumentDTO();
        compoundDocumentDTO2.setDocuments(Collections.singletonList(documentDTO2));

        List<CompoundDocumentDTO> compoundDocumentDTOS = Arrays.asList(compoundDocumentDTO1, compoundDocumentDTO2);

        List<CompoundDocumentDTO> encodedList = contentBase64Filter.encodeCompoundDocumentDTOList(compoundDocumentDTOS);

        assertEquals(Utils.base64Encode("SampleContent1"), encodedList.get(0).getDocuments().get(0).getContent());
        assertEquals(Utils.base64Encode("SampleContent2"), encodedList.get(1).getDocuments().get(0).getContent());
    }


    @Test
    void testDecodeDocumentImportDTO() {
        DocumentImportDTO documentImportDTO = new DocumentImportDTO();
        documentImportDTO.setXmlContent(Utils.base64Encode("EncodedXML"));

        DocumentImportDTO decodedDTO = contentBase64Filter.decodeDocumentImportDTO(documentImportDTO);

        assertEquals("EncodedXML", decodedDTO.getXmlContent());
    }


    @Test
    void testEncodeCommentResponseDTO() {
        CommentResponseDTO commentResponseDTO = TestDTOUtil.getRandomCommentResponseDTO();
        commentResponseDTO.setContent("SampleContent");

        CommentResponseDTO encodedDTO = contentBase64Filter.encodeCommentResponseDTO(commentResponseDTO);

        assertEquals(Utils.base64Encode("SampleContent"), encodedDTO.getContent());
    }


    @Test
    void testDecodeCreateCommentDTO() {
        CreateCommentDTO createCommentDTO = new CreateCommentDTO();
        createCommentDTO.setContent(Utils.base64Encode("EncodedContent"));

        CreateCommentDTO decodedDTO = contentBase64Filter.decodeCreateCommentDTO(createCommentDTO);

        assertEquals("EncodedContent", decodedDTO.getContent());
    }


    @Test
    void testEncodeCommentResponseDTOList() {
        CommentResponseDTO commentResponseDTO1 = TestDTOUtil.getRandomCommentResponseDTO();
        commentResponseDTO1.setContent("SampleContent1");

        CommentResponseDTO commentResponseDTO2 = TestDTOUtil.getRandomCommentResponseDTO();
        commentResponseDTO2.setContent("SampleContent2");

        List<CommentResponseDTO> commentResponseDTOS = Arrays.asList(commentResponseDTO1, commentResponseDTO2);

        List<CommentResponseDTO> encodedList = contentBase64Filter.encodeCommentResponseDTOList(commentResponseDTOS);

        assertEquals(Utils.base64Encode("SampleContent1"), encodedList.get(0).getContent());
        assertEquals(Utils.base64Encode("SampleContent2"), encodedList.get(1).getContent());
    }


    @Test
    void testDecodePatchCommentDTO() {
        PatchCommentDTO patchCommentDTO = new PatchCommentDTO();
        patchCommentDTO.setContent(Utils.base64Encode("EncodedContent"));

        PatchCommentDTO decodedDTO = contentBase64Filter.decodePatchCommentDTO(patchCommentDTO);

        assertEquals("EncodedContent", decodedDTO.getContent());
    }


    @Test
    void testEncodeReplyDTO() {
        ReplyDTO replyDTO = new ReplyDTO();
        replyDTO.setContent("SampleContent");

        ReplyDTO encodedDTO = contentBase64Filter.encodeReplyDTO(replyDTO);

        assertEquals(Utils.base64Encode("SampleContent"), encodedDTO.getContent());
    }


    @Test
    void testEncodeSynopsisResponseDTO() {
        DocumentDTO documentDTO = new DocumentDTO();
        documentDTO.setContent("SampleContent");

        SynopsisResponseDTO synopsisResponseDTO = new SynopsisResponseDTO();
        synopsisResponseDTO.setDocument(documentDTO);

        SynopsisResponseDTO encodedDTO = contentBase64Filter.encodeSynopsisResponseDTO(synopsisResponseDTO);

        assertEquals(Utils.base64Encode("SampleContent"), encodedDTO.getDocument().getContent());
    }


    @Test
    void testDecodeReplyDTO() {
        ReplyDTO replyDTO = new ReplyDTO();
        replyDTO.setContent(Utils.base64Encode("EncodedContent"));

        Reply decodedReply = TestObjectsUtil.getRandomReplyEntity();
        decodedReply.setContent("DecodedContent");

        when(replyMapperRest.map(replyDTO)).thenReturn(decodedReply);

        Reply result = contentBase64Filter.decodeReplyDTO(replyDTO);

        assertEquals(decodedReply.getContent(), result.getContent());
    }
}
