// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.mappers;

import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import de.itzbund.egesetz.bmi.lea.util.TestDTOUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static de.itzbund.egesetz.bmi.lea.domain.enums.RegelungsvorhabenTypType.GESETZ;
import static de.itzbund.egesetz.bmi.lea.domain.enums.RegelungsvorhabenTypType.RECHTSVERORDNUNG;
import static de.itzbund.egesetz.bmi.lea.domain.enums.RegelungsvorhabenTypType.VERWALTUNGSVORSCHRIFT;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for {@link PropositionMapperRest}
 */
class PropositionMapperRestTest {

    private PropositionMapperRest propositionMapper;


    @BeforeEach
    void init() {
        propositionMapper = new PropositionMapperRestImpl();
    }


    @Test
    void testMap() {
        RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = TestDTOUtil.getRandomRegelungsvorhabenEditorDTO();

        PropositionDTO actual = propositionMapper.map(regelungsvorhabenEditorDTO);
        assertThat(actual).hasNoNullFieldsOrPropertiesExcept("farbe");
    }


    @Test
    void toRechtsetzungsdokumentTypEnumTest() {
        assertThat(PropositionMapperRest.toRechtsetzungsdokumentTyp(GESETZ)).isEqualTo(RechtsetzungsdokumentTyp.GESETZ);
        assertThat(PropositionMapperRest.toRechtsetzungsdokumentTyp(RECHTSVERORDNUNG)).isEqualTo(
            RechtsetzungsdokumentTyp.VERORDNUNG);
        assertThat(PropositionMapperRest.toRechtsetzungsdokumentTyp(VERWALTUNGSVORSCHRIFT)).isEqualTo(
            RechtsetzungsdokumentTyp.VERWALTUNGSVORSCHRIFT);
        assertThat(PropositionMapperRest.toRechtsetzungsdokumentTyp(null)).isNull();
    }

}
