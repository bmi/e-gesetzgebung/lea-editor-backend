// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.mappers;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CreateCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.mappers.BaseMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.mappers.DocumentMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.DocumentMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.mappers.PropositionMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.mappers.UserMapperImpl;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.util.TestDTOUtil;
import de.itzbund.egesetz.bmi.lea.util.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for {@link CommentMapperRest}
 */
class CompoundDocumentMapperRestTest {

	private CompoundDocumentMapperRest commentMapper;
	private DocumentMapper documentMapper;


	@BeforeEach
	void init() {
		commentMapper = new CompoundDocumentMapperRestImpl(
			new DocumentMapperImpl(new BaseMapperImpl(), new PropositionMapperImpl(new BaseMapperImpl()),
				new UserMapperImpl()),
			new PropositionMapperRestImpl());
		documentMapper = new DocumentMapperImpl(new BaseMapperImpl(), new PropositionMapperImpl(new BaseMapperImpl()),
			new UserMapperImpl());
	}


	@Test
	void testMapFromCompoundDocumentAndProposition() {
		final int NUMBER_OF_DOCUMENTS = 3;
		CompoundDocumentDomain compoundDocumentEntity = TestObjectsUtil.getCompoundDocumentEntity();
		List<Document> documentList = TestObjectsUtil.getDocumentListOfSize(NUMBER_OF_DOCUMENTS);
		Document firstDocument = documentList.get(0);

		CompoundDocument compoundDocument = new CompoundDocument();
		compoundDocument.setCompoundDocumentEntity(compoundDocumentEntity);
		compoundDocument.setDocuments(documentList);

		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = TestDTOUtil.getRandomRegelungsvorhabenEditorDTO();

		CompoundDocumentDTO actual = commentMapper.mapFromCompoundDocumentAndProposition(compoundDocument,
			regelungsvorhabenEditorDTO);

		// Test elements (without list) are mapped correctly
		assertThat(actual).hasNoNullFieldsOrPropertiesExcept("documentPermissions", "commentPermissions");

		// Test proposition
		assertThat(actual.getProposition()).hasNoNullFieldsOrPropertiesExcept("farbe");

		// Test list ist correctly mapped
		assertThat(actual.getDocuments()).hasSize(NUMBER_OF_DOCUMENTS);

		DocumentDTO documentDTO = actual.getDocuments()
			.get(0);
		DocumentDTO expected = documentMapper.mapToDto(firstDocument);
		assertThat(expected).hasNoNullFieldsOrPropertiesExcept("state", "proposition", "documentPermissions", "commentPermissions", "version", "mappenName");
		assertThat(documentDTO).isEqualTo(expected);
	}


	@Test
	void testMapForTest() {
		CompoundDocument compoundDocument = TestObjectsUtil.getRandomCompoundDocument();
		CompoundDocumentSummaryDTO actual = commentMapper.mapForTest(compoundDocument);

		assertThat(actual).hasNoNullFieldsOrPropertiesExcept("documentPermissions", "commentPermissions");
	}


	@Test
	void testMapCompoundDocument() {
		CompoundDocument randomCompoundDocument = TestObjectsUtil.getRandomCompoundDocument();

		List<CompoundDocument> compoundDocuments = Arrays.asList(
			randomCompoundDocument,
			TestObjectsUtil.getRandomCompoundDocument(),
			TestObjectsUtil.getRandomCompoundDocument()
		);

		List<CompoundDocumentSummaryDTO> actual = commentMapper.mapCompoundDocument(compoundDocuments);

		// Test list in general is mapped
		assertThat(actual).isNotEmpty()
			.hasSize(3);

		// Test elements (without list) are mapped correctly
		CompoundDocumentSummaryDTO expected = commentMapper.mapForTest(randomCompoundDocument);
		assertThat(expected).hasNoNullFieldsOrPropertiesExcept("documentPermissions", "commentPermissions");
		assertThat(actual.get(0)).isEqualTo(expected);

		// Test document list is mapped correctly
		DocumentSummaryDTO documentSummaryDTO = documentMapper.mapSummary(randomCompoundDocument.getDocuments()
			.get(0));
		assertThat(expected.getDocuments()
			.get(0)).isEqualTo(documentSummaryDTO);
	}


	@Test
	void testMapIntoDomain() {
		CreateCompoundDocumentDTO createCompoundDocumentDTO = TestDTOUtil.getRandomCreateCompoundDocumentDTO();
		CreateCompoundDocumentDTO actual = commentMapper.mapIntoDomain(createCompoundDocumentDTO);

		assertThat(actual).hasNoNullFieldsOrProperties();
	}

}
