// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.api.DocumentsApi;
import de.itzbund.egesetz.bmi.lea.api.filter.ContentBase64Filter;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.EditorRollenUndRechte;
import de.itzbund.egesetz.bmi.lea.domain.dtos.bestand.CreateBestandsrechtDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.UpdateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CompoundDocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.DocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.ExportRestPort;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Controller to define endpoints handling creation, retrieval and saving documents.
 */
@RestController
@RequiredArgsConstructor
@Log4j2
@SuppressWarnings("unused")
public class DocumentController implements DocumentsApi {

	@Autowired
	private DocumentRestPort documentService;
	@Autowired
	private CompoundDocumentRestPort compoundDocumentService;
	@Autowired
	private ExportRestPort exportService;
	@Autowired
	private ContentBase64Filter base64Filter;
	@Autowired
	private EditorRollenUndRechte editorRollenUndRechte;
	@Autowired
	private LeageSession session;


	/*
	 * The returned instance of DocumentDTO will use a Base64-encoded content string.
	 */
	@Override
	@PreAuthorize("hasPermission(#documentId, 'DOKUMENTE', 'LESEN') OR hasPermission(#documentId, 'DOKUMENTE', 'SCHREIBEN')")
	public ResponseEntity<DocumentDTO> getWithoutAnnotations(UUID documentId) {
		log.debug("Received a request on path '/documents/noAnnotations/{documentId}' with parameters documentId='{}'.", documentId);

		DocumentDTO doc = documentService.getDocumentById(documentId, true);
		log.debug("Received documentDTO from service with content '{}'.", doc);

		if (doc != null) {
			return ResponseEntity.ok(base64Filter.encodeDocumentDTO(doc)); // 200
		}

		return ResponseEntity.notFound().build(); // 404
	}


	/*
	 * The returned instance of DocumentDTO will use a Base64-encoded content string.
	 */
	@Override
	@PreAuthorize("hasPermission(#documentId, 'DOKUMENTE', 'LESEN') OR hasPermission(#documentId, 'DOKUMENTE', 'SCHREIBEN')")
	public ResponseEntity<DocumentDTO> get(UUID documentId) {
		log.debug("Received a request on path '/documents/{documentId}' with parameters documentId='{}'.", documentId);

		User user = session.getUser();

		DocumentDTO doc = documentService.getDocumentById(documentId, false);
		log.debug("Got documentDTO from service with content '{}'.", doc);
		if (doc == null) {
			return ResponseEntity.notFound().build(); // 404
		}

		return ResponseEntity.ok(base64Filter.encodeDocumentDTO(doc)); // 200
	}


	/*
	 * The 'content' attribute of UpdateDocumentDTO will be Base64 encoded and must be decoded before first use. The
	 * other way around, the returned DocumentDTO will use a Base64-encoded content string.
	 */
	@Override
	@PreAuthorize("hasPermission(#documentId, 'DOKUMENTE', 'SCHREIBEN') OR hasPermission(#documentId, 'DOKUMENTE', 'SUCHENundERSETZEN')")
	public ResponseEntity<DocumentDTO> update(UUID documentId, UpdateDocumentDTO updateDocumentDTO) {
		List<ServiceState> states = new ArrayList<>();
		UpdateDocumentDTO decodedBody = base64Filter.decodeUpdateDocumentDTO(updateDocumentDTO);

		DocumentDTO documentDTO = null;
		if (Utils.isMissing(decodedBody.getContent()) && Utils.isMissing(decodedBody.getTitle())) {
			states.add(ServiceState.INVALID_ARGUMENTS);
		} else {
			documentDTO = documentService.updateDocument(documentId, decodedBody, states);
		}

		ServiceState state = states.get(0);
		switch (state) {
			case OK:
				assert documentDTO != null;
				return ResponseEntity.ok().body(base64Filter.encodeDocumentDTO(documentDTO)); // 200
			case DOCUMENT_NOT_FOUND:
				return ResponseEntity.notFound().build(); // 404
			case DOCUMENT_IS_NOT_IN_DRAFT:
			case NO_PERMISSION:
				return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build(); // 405
			case INVALID_ARGUMENTS:
				return ResponseEntity.badRequest().build(); // 400
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}
	}

	@Override
	public ResponseEntity<String> exportDocument(UpdateDocumentDTO updateDocumentDTO) {
		List<ServiceState> status = new ArrayList<>();
		String decodedJson = Utils.base64Decode(updateDocumentDTO.getContent());
		String xmlContent = exportService.exportSingleDocument(decodedJson, status);

		ServiceState serviceState = status.get(0);
		switch (serviceState) {
			case OK:
				return ResponseEntity.ok(xmlContent); // 200
			case XML_TRANSFORM_ERROR:
				return ResponseEntity.status(HttpStatus.CONFLICT).build(); // 409
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}
	}

	@Override
	@PreAuthorize("hasPermission(#documentId, 'DOKUMENTE', 'LESEN')")
	public ResponseEntity<DocumentSummaryDTO> saveAsBestandsrecht(UUID documentId, CreateBestandsrechtDTO createBestandsrechtDTO) {
		List<ServiceState> status = new ArrayList<>();
		DocumentSummaryDTO documentSummaryDTO = documentService.createBestandsrecht(new DocumentId(documentId), createBestandsrechtDTO, status);

		ServiceState state = status.get(0);
		switch (state) {
			case OK:
				assert documentSummaryDTO != null;
				return ResponseEntity.ok().body(documentSummaryDTO); // 200
			case DOCUMENT_NOT_FOUND:
			case COMPOUND_DOCUMENT_NOT_FOUND:
				return ResponseEntity.notFound().build(); // 404
			case NO_PERMISSION:
				return ResponseEntity.status(HttpStatus.FORBIDDEN).build(); // 403
			case INVALID_ARGUMENTS:
				return ResponseEntity.badRequest().build(); // 400
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}
	}

}
