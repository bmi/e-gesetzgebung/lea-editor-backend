// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.api.SynopseApi;
import de.itzbund.egesetz.bmi.lea.api.filter.ContentBase64Filter;
import de.itzbund.egesetz.bmi.lea.domain.dtos.synopsis.SynopsisRequestDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.synopsis.SynopsisResponseDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.SynopsisRestPort;
import de.itzbund.egesetz.bmi.lea.domain.services.synopse.SynopsisUtil;
import lombok.extern.log4j.Log4j2;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Log4j2
@RestController
@SuppressWarnings({"unused", "java:S1192"})
public class SynopsisController implements SynopseApi {

    @Autowired
    private SynopsisRestPort synopsisService;

    @Autowired
    private ContentBase64Filter contentBase64Filter;


    @Override
    @PreAuthorize("hasPermission(#synopsisRequestDTO.base, 'DOKUMENTE', 'LESEN') AND hasPermission(#synopsisRequestDTO.versions[0], 'DOKUMENTE', 'LESEN')")
    public ResponseEntity<SynopsisResponseDTO> getSynopsis(SynopsisRequestDTO synopsisRequestDTO) {
        List<ServiceState> status = new ArrayList<>();
        SynopsisResponseDTO synopsisResponseDTO = synopsisService.getSynopse(synopsisRequestDTO, status);
        ServiceState state = status.get(0);

        switch (state) {
            case OK: // 200
                return ResponseEntity.ok(contentBase64Filter.encodeSynopsisResponseDTO(synopsisResponseDTO));
            case INVALID_ARGUMENTS: // 400
                return ResponseEntity.badRequest()
                    .build();
            case NO_PERMISSION: // 403
                return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .build();
            case COMPOUND_DOCUMENT_NOT_FOUND:
            case DOCUMENT_NOT_FOUND: // 404
                return ResponseEntity.notFound()
                    .build();
            default: // 500
                return ResponseEntity.internalServerError()
                    .build();
        }
    }


    @Override
    public ResponseEntity<String> getTextCompareResult(String baseText, String versionText) {
        DiffMatchPatch dmp = new DiffMatchPatch();
        LinkedList<DiffMatchPatch.Diff> diffs = dmp.diffMain(baseText, versionText);
        dmp.diffCleanupSemantic(diffs);
        return ResponseEntity.ok(SynopsisUtil.getHtmlPage(diffs));
    }

}
