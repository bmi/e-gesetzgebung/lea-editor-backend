// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.mappers;

import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.ReplyDTO;
import de.itzbund.egesetz.bmi.lea.domain.mappers.BaseMapper;
import de.itzbund.egesetz.bmi.lea.domain.mappers.UserMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true),
    uses = {BaseMapper.class, UserMapper.class})
public interface ReplyMapperRest {

    // Input only parentId and content, all other stuff only as output
    @Mapping(target = "updatedBy", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "replyId", ignore = true)
    Reply map(ReplyDTO replyDTO);

    @Mapping(target = "replyId", source = "replyId", qualifiedByName = "replyIdUuidMapping")
    @Mapping(target = "updatedBy.gid", source = "updatedBy.gid", qualifiedByName = "userIdStringMapping")
    @Mapping(target = "createdBy.gid", source = "createdBy.gid", qualifiedByName = "userIdStringMapping")
    ReplyDTO mapToReplyId(Reply reply);
}
