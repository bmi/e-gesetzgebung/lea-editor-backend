/*
 * SPDX-License-Identifier: MPL-2.0
 *
 * Copyright (C) 2021-2023 Bundesministerium des Innern und für Heimat, \
 * Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
 */
package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.api.PropositionsApi;
import de.itzbund.egesetz.bmi.lea.api.filter.ContentBase64Filter;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.DokumentenmappeTableDTOs;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.HomepageRegulatoryProposalDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.homepage.RegelungsvorhabengTableDTOs;
import de.itzbund.egesetz.bmi.lea.domain.dtos.paging.PaginierungDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.synopsis.SynopsisRequestDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.PropositionRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.SynopsisRestPort;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Controller for returning all propositions of the current user. Makes a call to platform.
 */
@RestController
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class PropositionController implements PropositionsApi {

	@Autowired
	private PropositionRestPort propositionService;
	@Autowired
	private SynopsisRestPort synopsisService;
	@Autowired
	private ContentBase64Filter base64Filter;


	/**
	 * @return List of PropositionDTO
	 */
	@Override
	public ResponseEntity<List<PropositionDTO>> getAllPropositions() {
		return ResponseEntity.ok(propositionService.getAllPropositionDescriptionsByUser());
	}


	/**
	 * @return List of HomepageRegulatoryProposalDTO
	 * @deprecated
	 */
	@Override
	@Deprecated(forRemoval = true)
	public ResponseEntity<List<HomepageRegulatoryProposalDTO>> getAllRegulatoryProposals() {
		List<ServiceState> status = new ArrayList<>();
		List<HomepageRegulatoryProposalDTO> regulatoryProposalDTOS = propositionService.getRegulatoryProposals(status);
		ServiceState state = status.get(0);

		switch (state) {
			case OK:
				return ResponseEntity.ok().body(regulatoryProposalDTOS); // 200
			case NO_PERMISSION:
				return ResponseEntity.notFound().build(); // 401
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}
	}

	// @formatter:off
    /**
     * Listet alle Dokumente auf, die der Benutzer selbst erstellt hat.
     * (Paging funktioniert ohne Sortierungsanteil)
     *
     * Die Rechte sind abhängig von:
     *   - dem Status der Dokumentenmappe
     *   - ob (Schreib-)Rechte weitergegeben wurden
     *
     * @param paginierungDTO Optional kann Paging verwendet werden
     * @return Ein Baum/Tabelle von Regelungsvorhaben mit angehängten Dokumentenmappen und Dokumenten
     */
    // @formatter:on
	@Override
	public ResponseEntity<RegelungsvorhabengTableDTOs> getMeineDokumente(Optional<PaginierungDTO> paginierungDTO) {

		List<ServiceState> status = new ArrayList<>();

		RegelungsvorhabengTableDTOs regulatoryProposalDTOS = propositionService.getStartseiteMeineDokumente(status, paginierungDTO);
		ServiceState state = status.get(0);

		switch (state) {
			case OK:
				return ResponseEntity.ok().body(regulatoryProposalDTOS); // 200
			case NO_PERMISSION:
				return ResponseEntity.notFound().build(); // 401
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}
	}


	/**
	 * Listet alle Dokumente auf, die der Benutzer sehen/bearbeiten darf, aber nicht von ihm erstellt worden sind.
	 *
	 * @return
	 */
	// @formatter:on
	@Override
	public ResponseEntity<List<HomepageRegulatoryProposalDTO>> getDokumenteAusAbstimmungen() {

		List<ServiceState> status = new ArrayList<>();
		List<HomepageRegulatoryProposalDTO> regulatoryProposalDTOS = propositionService.getStartseiteDokumenteAusAbstimmungen(status);
		ServiceState state = status.get(0);

		switch (state) {
			case OK:
				return ResponseEntity.ok().body(regulatoryProposalDTOS); // 200
			case NO_PERMISSION:
				return ResponseEntity.notFound().build(); // 401
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}
	}


	@Override
	public ResponseEntity<DokumentenmappeTableDTOs> getByRegulatoryProposalId(UUID regulatoryProposalId, Optional<PaginierungDTO> paginierungDTO) {

		List<ServiceState> status = new ArrayList<>();

		DokumentenmappeTableDTOs regulatoryProposalDTO = propositionService.getVersionshistorie(status, regulatoryProposalId, paginierungDTO);

		ServiceState state = status.get(0);

		switch (state) {
			case OK:
				return ResponseEntity.ok().body(regulatoryProposalDTO); // 200
			case NO_PERMISSION:
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build(); //401
			case DOCUMENT_NOT_FOUND:
			case REGELUNGSVORHABEN_NOT_FOUND:
				return ResponseEntity.notFound().build(); // 404
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}
	}


	@Override
	public ResponseEntity<DocumentDTO> erzeugeAenderungsbefehle(UUID regulatoryProposalId, SynopsisRequestDTO synopsisRequestDTO) {
		List<ServiceState> status = new ArrayList<>();
		UUID bestandsrechtId = synopsisRequestDTO.getBase();
		List<UUID> versionIds = synopsisRequestDTO.getVersions();
		UUID regelungstextId = versionIds.isEmpty() ? null : versionIds.get(0);

		if (bestandsrechtId == null || regelungstextId == null) {
			return ResponseEntity.badRequest().build(); // 400
		}

		DocumentDTO documentDTO = synopsisService.erzeugeAenderungsbefehle(regulatoryProposalId, bestandsrechtId, regelungstextId, status);
		ServiceState state = status.get(0);

		switch (state) {
			case OK:
				return ResponseEntity.ok(base64Filter.encodeDocumentDTO(documentDTO)); // 200
			case NO_PERMISSION:
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build(); //401
			case DOCUMENT_NOT_FOUND:
				return ResponseEntity.notFound().build(); // 404
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}
	}

}
