// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.api;

import de.itzbund.egesetz.bmi.lea.domain.dtos.media.MediaShortSummaryDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.api.ApiConstants.API_DIV_MEDIA;

@Validated
public interface MediaApi {

    /**
     * DELETE /media : Deletes a media file
     *
     * @param mediaId ID of the media file (required)
     * @return No content (status code 204) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Not Found
     * &lt;/br&gt;File not found. (status code 404)
     */
    @Operation(
        operationId = "delete",
        summary = "Deletes a media file",
        tags = {API_DIV_MEDIA},
        responses = {
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid"),
            @ApiResponse(responseCode = "403", description = "User has invalid rights"),
            @ApiResponse(responseCode = "404", description = "Not Found </br>File not found.")
        }
    )
    @DeleteMapping(
        value = "/media/{mediaId}"
    )
    ResponseEntity<Void> delete(
        @Parameter(
            in = ParameterIn.PATH,
            description = "ID of the media file",
            required = true,
            schema = @Schema())
        @PathVariable("mediaId") UUID mediaId
    );

    /**
     * GET /media/{mediaId} : Find media by UUID
     *
     * @param mediaId (required)
     * @return successful operation (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or
     * Media not found (status code 404)
     */
    @Operation(
        operationId = "get",
        summary = "Find media by UUID",
        tags = {API_DIV_MEDIA},
        responses = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = Resource.class))),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
                content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Media not found", content = @Content())
        }
    )
    @GetMapping(
        value = "/media/{mediaId}",
        produces = {"*/*"}
    )
    ResponseEntity<Resource> get(
        @Parameter(name = "mediaId", required = true, schema = @Schema())
        @PathVariable("mediaId") String mediaId
    );

    /**
     * POST /media : Creates a media resource
     *
     * @param filename (required)
     * @param data     (optional)
     * @return (status code 201) or Currently only jpg (Extension) is allowed. (status code 422) or Internal Server Error &lt;/br&gt; Some error during
     * server-side processing occurred. (status code 500)
     */
    @Operation(
        operationId = "postFile",
        summary = "Creates a media resource",
        tags = {API_DIV_MEDIA},
        responses = {
            @ApiResponse(responseCode = "201",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = MediaShortSummaryDTO.class))),
            @ApiResponse(responseCode = "422", description = "Currently only jpg (Extension) is allowed.",
                content = @Content()),
            @ApiResponse(responseCode = "500",
                description = "Internal Server Error </br> Some error during server-side processing "
                    + "occurred.",
                content = @Content())
        }
    )
    @PostMapping(
        value = "/media",
        consumes = {"multipart/form-data"}
    )
    ResponseEntity<EntityModel<MediaShortSummaryDTO>> postFile(
        @Parameter(name = "filename", required = true, schema = @Schema())
        @Valid @RequestParam(value = "filename") String filename,
        @Parameter(name = "data", required = true, schema = @Schema())
        @RequestPart(value = "data") MultipartFile data
    );

}
