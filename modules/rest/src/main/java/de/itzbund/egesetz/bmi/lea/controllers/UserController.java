// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.api.UserApi;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserSettingsDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.UserRestPort;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class UserController implements UserApi {

    @Autowired
    private final UserRestPort userService;


    @Override
    public ResponseEntity<UserDTO> getUser() {
        User user = userService.getUser();

        UserDTO userDTO = UserDTO.builder()
            .gid(user.getGid().getId())
            .name(user.getName())
            .email(user.getEmail())
            .build();

        return ResponseEntity.ok(userDTO);
    }

    @Override
    public ResponseEntity<Void> updateSettings(UserSettingsDTO userSettingsDTO) {
        List<ServiceState> status = new ArrayList<>();

        userService.updateUserSettings(userSettingsDTO, status);

        ServiceState serviceState = status.get(0);
        switch (serviceState) {
            case OK:
                return ResponseEntity.status(HttpStatus.OK).build(); // 200
            case INVALID_ARGUMENTS:
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build(); //400
            default:
                return ResponseEntity.internalServerError().build(); // 500
        }
    }

    @Override
    public ResponseEntity<UserSettingsDTO> getSettings() {

        UserSettingsDTO userSettingsDTO = userService.getUserSettings();

        if (userSettingsDTO != null) {
            return ResponseEntity.ok(userSettingsDTO);
        }

        return ResponseEntity.ok(UserSettingsDTO.builder().pinnedDokumentenmappen(Collections.emptyList()).build());
    }

}
