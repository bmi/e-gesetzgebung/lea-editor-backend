// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.api.filter;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CommentResponseDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CreateCommentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.PatchCommentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.ReplyDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentImportDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.UpdateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.synopsis.SynopsisResponseDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.mappers.ReplyMapperRest;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A filter to convert document content from or to Base64 in order to circumvent issues with WAF rules.
 */
@Component
@Log4j2
public class ContentBase64Filter {

	@Autowired
	ReplyMapperRest replyMapperRest;


	public DocumentDTO encodeDocumentDTO(@NonNull DocumentDTO documentDTO) {
		String content = documentDTO.getContent();
		if (content != null) {
			documentDTO.setContent(Utils.base64Encode(content));
		}
		return documentDTO;
	}


	public List<DocumentDTO> encodeDocumentDTOList(@NonNull List<DocumentDTO> documentDTOS) {
		List<DocumentDTO> newList = new ArrayList<>();
		for (DocumentDTO doc : documentDTOS) {
			newList.add(encodeDocumentDTO(doc));
		}
		return newList;
	}


	public UpdateDocumentDTO decodeUpdateDocumentDTO(@NonNull UpdateDocumentDTO updateDocumentDTO) {
		String content = updateDocumentDTO.getContent();
		if (content != null) {
			updateDocumentDTO.setContent(Utils.base64Decode(content));
		}
		return updateDocumentDTO;
	}

	public CompoundDocumentDTO encodeCompoundDocumentDTO(@NonNull CompoundDocumentDTO compoundDocumentDTO) {
		List<DocumentDTO> newList = encodeDocumentDTOList(compoundDocumentDTO.getDocuments());
		compoundDocumentDTO.setDocuments(newList);
		return compoundDocumentDTO;
	}


	public List<CompoundDocumentDTO> encodeCompoundDocumentDTOList(
		@NonNull List<CompoundDocumentDTO> compoundDocumentDTOList) {
		List<CompoundDocumentDTO> newList = new ArrayList<>();
		for (CompoundDocumentDTO doc : compoundDocumentDTOList) {
			newList.add(encodeCompoundDocumentDTO(doc));
		}
		return newList;
	}


	public DocumentImportDTO decodeDocumentImportDTO(@NonNull DocumentImportDTO documentImportDTO) {
		String content = documentImportDTO.getXmlContent();
		documentImportDTO.setXmlContent(Utils.base64Decode(content));
		return documentImportDTO;
	}


	public CommentResponseDTO encodeCommentResponseDTO(@NonNull CommentResponseDTO commentResponseDTO) {
		String content = commentResponseDTO.getContent();
		if (content != null) {
			commentResponseDTO.setContent(Utils.base64Encode(content));
		}

		commentResponseDTO.setReplies(commentResponseDTO.getReplies()
			.stream()
			.map(this::encodeReplyDTO)
			.collect(Collectors.toList()));

		return commentResponseDTO;
	}


	public CreateCommentDTO decodeCreateCommentDTO(@NonNull CreateCommentDTO createCommentDTO) {
		CreateCommentDTO createComment = CreateCommentDTO.builder()
			.focus(createCommentDTO.getFocus())
			.anchor(createCommentDTO.getAnchor())
			.commentId(createCommentDTO.getCommentId())
			.documentContent(createCommentDTO.getDocumentContent())
			.build();

		String content = createCommentDTO.getContent();
		if (content != null) {
			createComment.setContent(Utils.base64Decode(content));
		}
		return createComment;
	}


	public List<CommentResponseDTO> encodeCommentResponseDTOList(@NonNull List<CommentResponseDTO> commentResponseDTOS) {
		List<CommentResponseDTO> newList = new ArrayList<>();
		for (CommentResponseDTO comment : commentResponseDTOS) {
			newList.add(encodeCommentResponseDTO(comment));
		}
		return newList;
	}


	public PatchCommentDTO decodePatchCommentDTO(@NonNull PatchCommentDTO patchCommentDTO) {

		PatchCommentDTO patchComment = PatchCommentDTO.builder()
			.focus(patchCommentDTO.getFocus())
			.anchor(patchCommentDTO.getAnchor())
			.build();

		String content = patchCommentDTO.getContent();
		if (content != null && !content.isBlank()) {
			patchComment.setContent(Utils.base64Decode(content));
		}
		return patchComment;
	}


	public Reply decodeReplyDTO(@NonNull ReplyDTO replyDTO) {
		Reply reply = replyMapperRest.map(replyDTO);

		String content = replyDTO.getContent();
		if (content != null && !content.isBlank()) {
			reply.setContent(Utils.base64Decode(content));
		}

		return reply;
	}


	public ReplyDTO encodeReplyDTO(@NonNull ReplyDTO replyDTO) {
		String content = replyDTO.getContent();
		if (content != null && !content.isBlank()) {
			replyDTO.setContent(Utils.base64Encode(content));
		}
		return replyDTO;
	}


	public SynopsisResponseDTO encodeSynopsisResponseDTO(@NonNull SynopsisResponseDTO synopsisResponseDTO) {
		DocumentDTO document = synopsisResponseDTO.getDocument();
		if (document != null) {
			document = encodeDocumentDTO(document);
			synopsisResponseDTO.setDocument(document);

		}
		return synopsisResponseDTO;
	}

}
