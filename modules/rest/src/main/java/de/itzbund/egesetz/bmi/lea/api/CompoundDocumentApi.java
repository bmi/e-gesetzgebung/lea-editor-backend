// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.api;

import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentTitleDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CopyCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CreateCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.ImportEgfaDataDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.UpdateCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.CreateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentImportDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentMetadataDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaStatusDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtListDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtShortDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserRightsDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.api.ApiConstants.API_DIV_BESTANDSRECHT;
import static de.itzbund.egesetz.bmi.lea.api.ApiConstants.API_DIV_DOCUMENT_COLLECTIONS;

public interface CompoundDocumentApi {

    /**
     * GET /compounddocumenttitles : Returns the titles of all compound documents of a user to which the user has access. Further attributes and the containing
     * documents are omitted. To be used in select lists, etc. If no compound documents are found, only an empty array is returned.
     *
     * @return OK (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403)
     */
    @Operation(summary = "Abruf aller Dokumentenmappentiteln des angemeldeten Benutzers",
        description = "Menüpunkte:"
            + "<ul>"
            + "<li>Neues Einzeldokument anlegen</li>"
            + "<li>Dokumentenmappe exportieren</li>"
            + "</ul>",
        tags = {API_DIV_DOCUMENT_COLLECTIONS},
        responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                content = @Content(mediaType = "application/json",
                    array = @ArraySchema(
                        schema = @Schema(implementation = CompoundDocumentTitleDTO.class)))),
            @ApiResponse(responseCode = "204", description = "There are no compound documents for the user.",
                content = @Content()),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
                content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content())
        }
    )
    @GetMapping(value = "/compounddocumenttitles",
        produces = {"application/json"})
    ResponseEntity<List<CompoundDocumentTitleDTO>> getAllTitles();

    /**
     * POST /compounddocuments : Creates a new compound document.
     *
     * @param createCompoundDocumentDTO (required)
     * @return Created (status code 201) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Invalid
     * Arguments (status code 422)
     */
    @Operation(summary = "Erstellt eine neue Dokumentenmappe.",
        description = "Menüpunkt: <b>Dokumentenmappe anlegen</b>; vorher wird <i>GET /propositions</i> aufgerufen.",
        tags = {API_DIV_DOCUMENT_COLLECTIONS},
        responses = {
            @ApiResponse(responseCode = "201", description = "Created",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = EntityModel.class))),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
                content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
            @ApiResponse(responseCode = "422", description = "Invalid Arguments", content = @Content())
        })
    @PostMapping(value = "/compounddocuments",
        produces = {"application/json"},
        consumes = {"application/json"})
    ResponseEntity<EntityModel<CompoundDocumentDTO>> createCompoundDocument(
        @Parameter(in = ParameterIn.DEFAULT, schema = @Schema())
        @Valid @RequestBody CreateCompoundDocumentDTO createCompoundDocumentDTO);

    /**
     * GET /compounddocuments : Returns all compound documents of a user to which the user has access. If no compound documents are found, only an empty array
     * is returned.
     *
     * @return OK (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403)
     */
    @Operation(summary = "Returns all compound documents of a user to which the user has access.",
        description = "If no compound documents are found, only an empty array is returned.",
        tags = {API_DIV_DOCUMENT_COLLECTIONS},
        responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                content = @Content(mediaType = "application/json",
                    array = @ArraySchema(
                        schema = @Schema(implementation = CompoundDocumentDTO.class)))),
            @ApiResponse(responseCode = "204", description = "There are no compound documents for the user.",
                content = @Content()),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
                content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content())
        }
    )
    @GetMapping(value = "/compounddocuments",
        produces = {"application/json"})
    ResponseEntity<List<EntityModel<CompoundDocumentDTO>>> getAll();

    /**
     * GET /compounddocuments/regelungsvorhaben/{id}/list : Returns a list of compounddocuments from the given regulatory proposal id, sorted by version.
     *
     * @param regulatoryProposalId UUID of the regulatory proposal (required)
     * @return OK (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Not Found
     * &lt;/br&gt; Compound Document not found or **documentId** invalid. (status code 404)
     */
    @Operation(
        summary = "Returns a list of compounddocuments from the given regulatory proposal id, sorted by version.",
        tags = {API_DIV_DOCUMENT_COLLECTIONS},
        responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                content = @Content(mediaType = "application/json",
                    array = @ArraySchema(
                        schema = @Schema(implementation = CompoundDocumentDTO.class)))),
            @ApiResponse(responseCode = "204",
                description = "There are no compound documents for the user/regulatory proposal.",
                content = @Content()),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
                content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content())
        }
    )
    @GetMapping(value = "/compounddocuments/regelungsvorhaben/{id}/list",
        produces = {"application/json"})
    ResponseEntity<List<EntityModel<CompoundDocumentDTO>>> getByRegulatoryProposalId(
        @Parameter(in = ParameterIn.PATH, description = "UUID of the regulatory proposal", required = true,
            schema = @Schema())
        @PathVariable("id") UUID regulatoryProposalId);

    /**
     * GET /compounddocuments/{id} : Returns a compound document if it exists and the user has the right to access it.
     *
     * @param compoundDocumentId UUID of the compound document (required)
     * @return OK (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Not Found
     * &lt;/br&gt; Compound Document not found or **documentId** invalid. (status code 404)
     */
    @Operation(summary = "Returns a compound document if it exists and the user has the right to access it.",
        tags = {API_DIV_DOCUMENT_COLLECTIONS},
        responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = CompoundDocumentDTO.class))),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
                content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
            @ApiResponse(responseCode = "404",
                description = "Not Found </br> Compound Document not found or **documentId** invalid.",
                content = @Content()),
            @ApiResponse(responseCode = "409", description = "Status invalid (maybe set before)",
                content = @Content()),
        }
    )
    @GetMapping(value = "/compounddocuments/{id}",
        produces = {"application/json"})
    ResponseEntity<EntityModel<CompoundDocumentDTO>> getByIdentifier(
        @Parameter(in = ParameterIn.PATH, description = "UUID of the compound document", required = true,
            schema = @Schema())
        @PathVariable("id") UUID compoundDocumentId);

    /**
     * GET /compounddocuments/{id}/list : Returns information about documents in a compound document if it exists and the user has the right to access it.
     *
     * @param compoundDocumentId – UUID of the compound document (required)
     * @return OK (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Not Found
     * &lt;/br&gt; Compound Document not found or **documentId** invalid. (status code 404)
     */
    @Operation(summary = "Returns information about documents in a compound document if it exists and the user has "
        + "the right to access it.",
        tags = {API_DIV_DOCUMENT_COLLECTIONS},
        responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = CompoundDocumentDTO.class))),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
                content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
            @ApiResponse(responseCode = "404",
                description = "Not Found </br> Compound Document not found or **documentId** invalid.",
                content = @Content())
        }
    )
    @GetMapping(value = "/compounddocuments/{id}/list",
        produces = {"application/json"})
    ResponseEntity<CompoundDocumentSummaryDTO> getDocumentsList(
        @Parameter(in = ParameterIn.PATH, description = "UUID of the compound document", required = true,
            schema = @Schema())
        @PathVariable("id") UUID compoundDocumentId);

    /**
     * POST /compounddocuments/{id}/documents : Creates document in compound document
     *
     * @param compoundDocumentId Uuid of the compound document (required)
     * @param createDocumentDTO  (required)
     * @return OK (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Compound document
     * not found (status code 404) or Conflict, max. number of document types exceeded (status code 409) or Document Validation Error (status code 422) or Maybe
     * template missing or other server error (status code 500)
     */
    @Operation(summary = "Anlegen eines neuen Dokuments in der Dokumentenmappe",
        description = "Menüpunkt: <b>Neues Einzeldokument anlegen</b>; vorher wird <i>GET /compounddocumenttitles</i> aufgerufen.",
        tags = {API_DIV_DOCUMENT_COLLECTIONS},
        responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = EntityModel.class))),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
                content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Compound document not found",
                content = @Content()),
            @ApiResponse(responseCode = "409", description = "Conflict, max. number of document types exceeded",
                content = @Content()),
            @ApiResponse(responseCode = "422", description = "Document Validation Error", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Maybe template missing or other server error",
                content = @Content())
        }
    )
    @PostMapping(value = "/compounddocuments/{id}/documents",
        produces = {"application/json"},
        consumes = {"application/json"})
    ResponseEntity<EntityModel<CompoundDocumentDTO>> addDocuments(
        @Parameter(in = ParameterIn.PATH, description = "Uuid of the compound document", required = true,
            schema = @Schema())
        @PathVariable("id") UUID compoundDocumentId,

        @Parameter(in = ParameterIn.DEFAULT, description = "The document", schema = @Schema())
        @Valid @RequestBody CreateDocumentDTO createDocumentDTO);

    /**
     * POST /compounddocuments/{id}/export : Exports compound document as ZIP file Takes the UUID of a compound document and puts every single document
     * belonging to it after conversion to XML in a ZIP OutputStream. Additionally another XML document for describing the compound document itself and its
     * links to the single documents is created and included in the ZIP file.
     *
     * @param compoundDocumentId UUID of the compound document (required)
     * @return OK (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Compound document
     * with given UUID could not be found. (status code 404) or Unknown server error while export. (status code 500)
     */
    @Operation(summary = "Exportieren einer Dokumentenmappe in eine ZIP-Datei.",
        description = "Menüpunkt: <b>Export</b>; vorher wird <i>GET /compounddocumenttitles</i> aufgerufen.<br>"
            + "Die ZIP-Datei enthält eine Beschreibungsdatei, sowie die Dokumente der Dokumentenmappe im XML-Format.",
        tags = {API_DIV_DOCUMENT_COLLECTIONS},
        responses = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid"),
            @ApiResponse(responseCode = "403", description = "User has invalid rights"),
            @ApiResponse(responseCode = "404",
                description = "Compound document with given UUID could not be found."),
            @ApiResponse(responseCode = "500", description = "Unknown server error while export.")
        }
    )
    @PostMapping(value = "/compounddocuments/{id}/export",
        produces = {"application/zip"})
    ResponseEntity<Void> downloadZipFile(
        HttpServletResponse response,
        @Parameter(in = ParameterIn.PATH, description = "UUID of the compound document", required = true,
            schema = @Schema())
        @PathVariable("id") UUID compoundDocumentId);

    /**
     * POST /compounddocuments/{id}/documents/import : Imports a single XML document This imports an XML document that is assumed to be a document of German
     * eLegislation and not bigger than 20MB. The imported document will be assigned to a compound document, if allowed. If not or if the document is not XML or
     * its bigger than 20MB, import fails.
     *
     * @param compoundDocumentId UUID of the compound document (required)
     * @param documentImportDTO  (required)
     * @return OK (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Compound document
     * with given UUID could not be found (status code 404) or Wrong file size or format (status code 415) or Unprocessable or invalid XML content (status code
     * 422)
     */
    @Operation(summary = "Importiert ein XML Dokument in die Dokumentenmappe",
        description = "Menüpunkt: <b>Dokument importieren</b>.<br> Das Dokument muss aus validem XML bestehen und nicht größer als 20MB sein.",
        tags = {API_DIV_DOCUMENT_COLLECTIONS},
        responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = DocumentDTO.class))),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
                content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
            @ApiResponse(responseCode = "404",
                description = "Compound document with given UUID could not be found", content = @Content()),
            @ApiResponse(responseCode = "415", description = "Wrong file size or format", content = @Content()),
            @ApiResponse(responseCode = "422", description = "Unprocessable or invalid XML content",
                content = @Content())
        }
    )
    @PostMapping(value = "/compounddocuments/{id}/documents/import",
        produces = {"application/json"},
        consumes = {"application/json"}
    )
    ResponseEntity<DocumentMetadataDTO> importDocument(
        HttpServletRequest request,
        @Parameter(in = ParameterIn.PATH, description = "UUID of the compound document", required = true,
            schema = @Schema())
        @PathVariable("id") UUID compoundDocumentId,
        @Parameter(in = ParameterIn.DEFAULT, description = "The document content and title", schema = @Schema())
        @Valid @RequestBody DocumentImportDTO documentImportDTO);

    @Operation(
        summary = "Nimmt Veränderungen an der Dokumentenmappe vor.",
        description = "Menüpunkte:"
            + "<ul>"
            + "<li>Status der Dokumentenmappe ändern, z.B. In Bereit für das Kabinettverfahren setzen</li>"
            + "<li>Die Dokumentenmappe umbenennen.</li>"
            + "</ul>",
        tags = {API_DIV_DOCUMENT_COLLECTIONS},
        responses = {
            @ApiResponse(responseCode = "200", description = "Operation successful", content = @Content()),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
                content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
            @ApiResponse(responseCode = "404",
                description = "Compound document with given UUID could not be found", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Unknown server error", content = @Content())
        }
    )
    @PatchMapping(
        value = "/compounddocuments/{id}",
        consumes = "application/json"
    )
    ResponseEntity<Void> renameAndStatePatch(
        @Parameter(in = ParameterIn.PATH, description = "UUID of the compound document", required = true,
            schema = @Schema())
        @PathVariable("id")
        UUID compoundDocumentId,

        @Parameter(in = ParameterIn.DEFAULT, description = "New title for compound document", schema = @Schema())
        @Valid
        @RequestBody
        UpdateCompoundDocumentDTO updateCompoundDocumentDTO
    );

    /**
     * POST /compounddocuments/{id}/version : A new version of compound documents creation. Creates a new version of compound documents with/without containing
     * elements.
     *
     * @param id                      UUID of the compound document (required)
     * @param copyCompoundDocumentDTO (required)
     * @param copyId                  A list of containing documents that should be copied (optional)
     * @return OK (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Compound document
     * with given UUID could not be found. (status code 404) or Unknown server error while export. (status code 500)
     */
    @Operation(summary = "Neue Version eine Dokumentenmappe erstellen.",
        description = "Menüpunkt: <b>Neue Version anlegen</b>. Es können zusätzlich die Dokumente gewählt werden, die (als Kopie) in die neue Version übernommen werden sollen.",
        operationId = "versionIncrement",
        tags = {API_DIV_DOCUMENT_COLLECTIONS},
        responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = CompoundDocumentSummaryDTO.class))),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
                content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
            @ApiResponse(responseCode = "404",
                description = "Compound document with given UUID could not be found., content = @Content"
                    + "()"),
            @ApiResponse(responseCode = "500", description = "Unknown server error while export.",
                content = @Content())
        }
    )
    @PostMapping(value = "/compounddocuments/{id}/version",
        produces = {"application/json"},
        consumes = {"application/json"}
    )
    ResponseEntity<CompoundDocumentSummaryDTO> versionIncrement(
        @Parameter(in = ParameterIn.PATH, description = "UUID of the compound document", required = true,
            schema = @Schema())
        @PathVariable("id") UUID id,
        @Parameter(description = "", schema = @Schema(), required = true)
        @Valid @RequestBody CopyCompoundDocumentDTO copyCompoundDocumentDTO,
        @Parameter(description = "A list of containing documents that should be copied", schema = @Schema())
        @Valid @RequestParam(value = "copyId", required = false) List<UUID> copyId
    );

    /**
     * PUT /compounddocuments/{id}/allowedusers : Changes rights for access compound document
     *
     * @param id                UUID of the compound document (required)
     * @param userRightsDTOList (required)
     * @return OK (status code 204) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Compound Document
     * not found or **documentId** invalid. (status code 404)
     */
    @Operation(
        summary = "Vergibt für die Dokumentenmappe die Leserechte an andere Benutzer.",
        description = "Menüpunkt: <b>Leserechte vergeben</b>; vorher wird <i>GET /compounddocuments/{id}/allowedusers/{action}</i> aufgerufen.",
        operationId = "setAccessRights",
        tags = {API_DIV_DOCUMENT_COLLECTIONS},
        responses = {
            @ApiResponse(responseCode = "204", description = "OK", content = @Content()),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
                content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
            @ApiResponse(responseCode = "404",
                description = "Compound Document not found or **documentId** invalid.",
                content = @Content())
        }
    )
    @PostMapping(
        value = "/compounddocuments/{id}/allowedusers",
        consumes = {"application/json"}
    )
    ResponseEntity<Void> setAccessRights(
        @Parameter(in = ParameterIn.PATH, description = "UUID of the compound document", required = true,
            schema = @Schema())
        @PathVariable("id") UUID id,

        @Parameter(in = ParameterIn.DEFAULT, required = true, schema = @Schema())
        @Valid
        @RequestBody List<UserRightsDTO> userRightsDTOList
    );

    /**
     * POST /compounddocuments/{id}/egfadata : Imports the egfadata from the given modules in the given compound document
     *
     * @param compoundDocumentId Uuid of the compound document (required)
     * @param importEgfaDataDTO  (required)
     * @return OK (status code 200) or Bad Request (status code 400) or Access token is missing or invalid (status code 401) or User has invalid rights (status
     * code 403) or other server error (status code 500)
     */
    @Operation(summary = "Importiert EGFA-Daten auswählbarer Module in die Dokumentenmappe",
        description = "Menüpunkt: <b>EGFA Datenübernahme</b>; vorher wird <i>GET /compounddocument/{id}/egfaStatus</i> aufgerufen.",
        tags = {API_DIV_DOCUMENT_COLLECTIONS},
        responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = EntityModel.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request",
                content = @Content()),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid", content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                content = @Content())
        }
    )
    @PostMapping(value = "/compounddocuments/{id}/egfadata",
        produces = {"application/json"},
        consumes = {"application/json"})
    ResponseEntity<HashMap<String, HttpStatus>> importEgfaData(
        @Parameter(in = ParameterIn.PATH, description = "Uuid of the compound document", required = true,
            schema = @Schema())
        @PathVariable("id") UUID compoundDocumentId,
        @Parameter(in = ParameterIn.DEFAULT, description = "The document", schema = @Schema())
        @Valid @RequestBody ImportEgfaDataDTO importEgfaDataDTO);

    /**
     * POST /compounddocuments/{id}/changewritepermissions : Changes rights to write for compound document
     *
     * @param compoundDocumentId UUID of the compound document (required)
     * @param userRightsDTO      (required)
     * @return OK (status code 200 or 204) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Compound
     * Document not found or **documentId** invalid. (status code 404)
     */
    @Operation(
        summary = "Changes rights for access compound document.",
        description = "Changes rights for access compound document.",
        operationId = "setAccessRights",
        tags = {API_DIV_DOCUMENT_COLLECTIONS},
        responses = {
            @ApiResponse(responseCode = "204", description = "OK", content = @Content()),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
                content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
            @ApiResponse(responseCode = "404",
                description = "Compound Document not found or **documentId** invalid.",
                content = @Content()),
            @ApiResponse(responseCode = "409", description = "Wrong data in request", content = @Content())
        }
    )
    @PostMapping(
        value = "/compounddocuments/{id}/changewritepermissions",
        consumes = {"application/json"}
    )
    ResponseEntity<CompoundDocumentSummaryDTO> changeWritePermissions(
        @Parameter(in = ParameterIn.PATH, description = "UUID of the compound document", required = true,
            schema = @Schema())
        @PathVariable("id") UUID compoundDocumentId,

        @Parameter(in = ParameterIn.DEFAULT, required = true, schema = @Schema())
        @Valid
        @RequestBody(required = false) Optional<UserRightsDTO> userRightsDTO
    );

    /**
     * GET /compounddocuments/{id}/egfaStatus : Returns information about states of all eGFA Modules for a given compound document if it exists and the user has
     * the right to access it.
     *
     * @param compoundDocumentId – UUID of the compound document (required)
     * @return OK (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Not Found
     * &lt;/br&gt; Compound Document not found or **compoundDocumentId** invalid. (status code 404)
     */
    @Operation(summary = "Enthält informationen über die eGFA Module zur Dokumentenmappe",
        description = "Menüpunkt: <b>eGFA Datenübernahme</b>.",
        tags = {API_DIV_DOCUMENT_COLLECTIONS},
        responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = CompoundDocumentDTO.class))),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
                content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
            @ApiResponse(responseCode = "404",
                description = "Not Found </br> Compound Document not found or **compoundDocumentId** invalid.",
                content = @Content())
        }
    )
    @GetMapping(value = "/compounddocuments/{id}/egfaStatus",
        produces = {"application/json"})
    ResponseEntity<List<EgfaStatusDTO>> getEgfaStateByCompoundDocumentId(
        @Parameter(in = ParameterIn.PATH, description = "UUID of the compound document", required = true,
            schema = @Schema())
        @PathVariable("id") UUID compoundDocumentId);

    /**
     * GET /compounddocuments/{id}/bestandsrecht : Returns information about the related Bestandsrecht to the RV associated with the DM
     *
     * @param compoundDocumentId – UUID of the compound document (required)
     * @return OK (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Not Found, Compound
     * @return OK (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Not Found, Compound
     * Document not found or **compoundDocumentId** invalid. (status code 404) or error (status code 500)
     */
    @Operation(summary = "Gibt das zum RV der Dokumentenmappe verknüpfte Bestandsrecht zurück.",
        description = "Gibt das zum RV der Dokumentenmappe verknüpfte Bestandsrecht zurück.",
        tags = {API_DIV_DOCUMENT_COLLECTIONS},
        responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                content = @Content(mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = BestandsrechtListDTO.class)))),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
                content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
            @ApiResponse(responseCode = "404",
                description = "Not Found </br> Compound Document not found or **compoundDocumentId** invalid.",
                content = @Content())
        }
    )
    @GetMapping(value = "/compounddocuments/{id}/bestandsrecht",
        produces = {"application/json"})
    ResponseEntity<List<BestandsrechtListDTO>> getBestandsrechtByCompoundDocumentId(
        @Parameter(in = ParameterIn.PATH, description = "UUID of the compound document", required = true,
            schema = @Schema())
        @PathVariable("id") UUID compoundDocumentId);

    /**
     * GET /compounddocuments/{id}/bestandsrecht/dm : Returns information about the related Bestandsrecht to the DM
     *
     * @param compoundDocumentId – UUID of the compound document (required)
     * @return OK (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Not Found, Compound
     * Document not found or **compoundDocumentId** invalid. (status code 404) or error (status code 500)
     */
    @Operation(summary = "Gibt die zum Regelungsvorhaben gehörigen Verknüpfungen zwischen Dokumentenmappe und verknüpftem Bestandsrechte zurück.",
        description = "Gibt die zum Regelungsvorhaben gehörigen Verknüpfungen zwischen Dokumentenmappe und verknüpftem Bestandsrechte zurück.",
        tags = {API_DIV_BESTANDSRECHT},
        responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                content = @Content(mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = BestandsrechtListDTO.class)))),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
                content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
            @ApiResponse(responseCode = "404",
                description = "Not Found </br> Compound Document not found or **compoundDocumentId** invalid.",
                content = @Content())
        }
    )
    @GetMapping(value = "/compounddocuments/{id}/bestandsrecht/dm",
        produces = {"application/json"})
    ResponseEntity<List<BestandsrechtListDTO>> getBestandsrechtFromCompoundDocumentByCompoundDocumentId(
        @Parameter(in = ParameterIn.PATH, description = "UUID der Dokumentenmappe", required = true,
            schema = @Schema())
        @PathVariable("id") UUID compoundDocumentId);

    /**
     * PUT /compounddocuments/{id}/bestandsrechte
     *
     * @param compoundDocumentId – UUID of the compound document (required)
     * @return OK (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Not Found, Compound
     * Document not found or **compoundDocumentId** invalid. (status code 404) or error (status code 500)
     */
    @Operation(summary = "Ordnet die Bestandsrechte einer Dokumentenmappe zu.",
        description = "Ordnet die Bestandsrechte einer Dokumentenmappe zu.",
        tags = {API_DIV_BESTANDSRECHT},
        responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                content = @Content(mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = BestandsrechtListDTO.class)))),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
                content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
            @ApiResponse(responseCode = "404",
                description = "Not Found </br> Compound Document not found or **compoundDocumentId** invalid.",
                content = @Content())
        }
    )
    @PutMapping(value = "/compounddocuments/{id}/bestandsrechte",
        produces = {"application/json"})
    ResponseEntity<List<BestandsrechtListDTO>> setBestandsrechteForCompoundDocumentByCompoundDocumentId(
        @Parameter(in = ParameterIn.PATH, description = "UUID der Dokumentenmappe", required = true,
            schema = @Schema())
        @PathVariable("id") UUID compoundDocumentId,
        @Parameter(name = "DMBestandsrechtLinkDTO", description = "DMBestandsrechtLinkDTO", required = true)
        @Valid @RequestBody
        List<BestandsrechtShortDTO> bestandsrechtDTOs
    );


}
