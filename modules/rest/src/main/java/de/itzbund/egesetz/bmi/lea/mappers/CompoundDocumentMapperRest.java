// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.mappers;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentTitleDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CreateCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.mappers.DocumentMapper;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true),
    uses = {DocumentMapper.class, PropositionMapperRest.class}
)
@SuppressWarnings("unused")
public interface CompoundDocumentMapperRest {

    @Mapping(target = "id", source = "compoundDocument.compoundDocumentEntity.compoundDocumentId.id")
    @Mapping(target = "title", source = "compoundDocument.compoundDocumentEntity.title")
    @Mapping(target = "type", source = "compoundDocument.compoundDocumentEntity.type")
    @Mapping(target = "version", source = "compoundDocument.compoundDocumentEntity.version")
    @Mapping(target = "createdAt", source = "compoundDocument.compoundDocumentEntity.createdAt")
    @Mapping(target = "updatedAt", source = "compoundDocument.compoundDocumentEntity.updatedAt")
    @Mapping(target = "documents", source = "compoundDocument.documents")
    @Mapping(target = "proposition", source = "regelungsVorhabenDTO")
    CompoundDocumentDTO mapFromCompoundDocumentAndProposition(CompoundDocument compoundDocument,
        RegelungsvorhabenEditorDTO regelungsVorhabenDTO);

    @Mapping(target = "id", source = "compoundDocument.compoundDocumentEntity.compoundDocumentId.id")
    @Mapping(target = "title", source = "compoundDocument.compoundDocumentEntity.title")
    @Mapping(target = "type", source = "compoundDocument.compoundDocumentEntity.type")
    @Mapping(target = "numberAttachments", constant = "0")
    CompoundDocumentTitleDTO mapTitleFromCompoundDocument(CompoundDocument compoundDocument);

    List<CompoundDocumentSummaryDTO> mapCompoundDocument(List<CompoundDocument> compoundDocuments);

    @Mapping(target = "id", source = "compoundDocument.compoundDocumentEntity.compoundDocumentId.id")
    @Mapping(target = "title", source = "compoundDocument.compoundDocumentEntity.title")
    @Mapping(target = "type", source = "compoundDocument.compoundDocumentEntity.type")
    @Mapping(target = "version", source = "compoundDocument.compoundDocumentEntity.version")
    @Mapping(target = "createdAt", source = "compoundDocument.compoundDocumentEntity.createdAt")
    @Mapping(target = "updatedAt", source = "compoundDocument.compoundDocumentEntity.updatedAt")
    @Mapping(target = "documents", source = "compoundDocument.documents")
    CompoundDocumentSummaryDTO mapForTest(CompoundDocument compoundDocument);

    CreateCompoundDocumentDTO mapIntoDomain(CreateCompoundDocumentDTO createCompoundDocumentDTO);
}
