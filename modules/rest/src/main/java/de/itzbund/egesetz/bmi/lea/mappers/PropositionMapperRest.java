// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.mappers;

import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.ProponentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RessortEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.RegelungsvorhabenTypType;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true))

public interface PropositionMapperRest {

    @Named("RegelungsvorhabenTypType_to_RechtsetzungsdokumentTyp")
    static RechtsetzungsdokumentTyp toRechtsetzungsdokumentTyp(RegelungsvorhabenTypType type) {

        if (type != null) {
            switch (type) {
                case GESETZ:
                    return RechtsetzungsdokumentTyp.GESETZ;
                case RECHTSVERORDNUNG:
                    return RechtsetzungsdokumentTyp.VERORDNUNG;
                case VERWALTUNGSVORSCHRIFT:
                    return RechtsetzungsdokumentTyp.VERWALTUNGSVORSCHRIFT;
            }
        }

        return null;
    }

    @Mapping(target = "title", constant = "bezeichnung")
    @Mapping(target = "shortTitle", source = "kurzbezeichnung")
    @Mapping(target = "abbreviation", source = "abkuerzung")
    @Mapping(target = "state", source = "status")
    @Mapping(target = "proponentDTO", source = "technischesFfRessort")
    @Mapping(target = "type", source = "vorhabenart", qualifiedByName = "RegelungsvorhabenTypType_to_RechtsetzungsdokumentTyp")
    PropositionDTO map(RegelungsvorhabenEditorDTO dto);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "title", source = "kurzbezeichnung")
    @Mapping(target = "designationGenitive", source = "bezeichnungGenitiv")
    @Mapping(target = "designationNominative", source = "bezeichnungNominativ")
    @Mapping(target = "active", source = "aktiv")
    ProponentDTO map(RessortEditorDTO ressortEditorDTO);

}
