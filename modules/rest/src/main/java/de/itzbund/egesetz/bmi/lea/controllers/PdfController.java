// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.api.PdfApi;
import de.itzbund.egesetz.bmi.lea.api.filter.ContentBase64Filter;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.PkpZuleitungDokumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.UpdateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtListDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CompoundDocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.DocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.ExportRestPort;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationException;
import de.itzbund.egesetz.bmi.lea.export.document.MultipleDocumentDTO;
import de.itzbund.egesetz.bmi.lea.export.services.PdfService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState.INVALID_STATE;

@RestController
@RequiredArgsConstructor
@Log4j2
@SuppressWarnings("unused")
public class PdfController implements PdfApi {

	private static final String HEADER_NAME = "content-disposition";

	private static final String HEADER_VALUE_PREFIX = "attachment; filename=";

	@Autowired
	private PdfService pdfService;

	@Autowired
	private DocumentRestPort documentService;

	@Autowired
	private CompoundDocumentRestPort compoundDocumentService;

	@Autowired
	private ExportRestPort exportService;

	@Autowired
	private ContentBase64Filter contentBase64Filter;

	/**
	 * Creates a PDF document based on the given documentId.
	 *
	 * @param documentId the documentId of the document which has to be exported
	 * @return response containing a ByteArray with the PDF content or status 409 in case of an error
	 */
	@Override
	//Übergangsweise die Berechtigungsprüfung auskommentiert, um BT-Usern den Export zu ermöglichen
	//@PreAuthorize("hasPermission(#documentId, 'DOKUMENTE', 'LESEN')")
	public ResponseEntity<byte[]> exportPdf(UUID documentId) {
		MultipleDocumentDTO multipleDocumentDTO = new MultipleDocumentDTO();
		multipleDocumentDTO.getDocuments().add(documentId);
		multipleDocumentDTO.setTitle(documentService.getDocumentById(documentId, false).getTitle());
		multipleDocumentDTO.setMergeDocuments(true);
		return exportPdf(multipleDocumentDTO);
	}

	/**
	 * Creates a PDF document based on the given documentId.
	 *
	 * @param multipleDocumentDTO the DTO containing documents which have to be exported
	 * @return response containing a ByteArray with the PDF content or status 409 in case of an error
	 */
	@Override
	//@PreAuthorize("hasPermission(#documentId, 'DOKUMENTE', 'LESEN')")
	public ResponseEntity<byte[]> exportPdf(MultipleDocumentDTO multipleDocumentDTO) {
		try (InputStream inputStream = pdfService.convertDocumentToInputStream(multipleDocumentDTO)) {
			HttpHeaders headers = new HttpHeaders();
			headers.add(HEADER_NAME, HEADER_VALUE_PREFIX + multipleDocumentDTO.getTitle() + "."
				+ (multipleDocumentDTO.getMergeDocuments() ? "pdf" : "zip"));
			headers.setContentType(multipleDocumentDTO.getMergeDocuments() ? MediaType.APPLICATION_PDF : MediaType.APPLICATION_OCTET_STREAM);
			return new ResponseEntity<>(inputStream.readAllBytes(), headers, HttpStatus.OK);
		} catch (IOException | ValidationException e) {
			return handlePdfCreationError(e);
		}
	}

	@Override
	public ResponseEntity<byte[]> exportSynopsisPdf(UpdateDocumentDTO synopsis) {
		String content = contentBase64Filter.decodeUpdateDocumentDTO(synopsis).getContent();
		String xml = exportService.exportSingleDocument(content, new ArrayList<>());
		try (InputStream inputStream = pdfService.convertXmlToInputStream(xml, synopsis.getHdr(), synopsis.getTitle(), "Synopse")) {
			HttpHeaders headers = new HttpHeaders();
			headers.add(HEADER_NAME, HEADER_VALUE_PREFIX + synopsis.getTitle() + ".pdf");
			headers.setContentType(MediaType.APPLICATION_PDF);
			return new ResponseEntity<>(inputStream.readAllBytes(), headers, HttpStatus.OK);
		} catch (IOException e) {
			return handlePdfCreationError(e);
		}
	}

	private ResponseEntity<byte[]> handlePdfCreationError(Exception e) {
		log.error("PDF creation error: ", e);
		return ResponseEntity.status(HttpStatus.CONFLICT).build(); // 409
	}

	@Override
	public ResponseEntity<List<BestandsrechtListDTO>> updateDrucksache(UUID compoundDocumentId) {

		List<ServiceState> status = new ArrayList<>();

		pdfService.updateDrucksacheForCompoundDocumentId(compoundDocumentId, status);

		if (status.isEmpty()) {
			status.add(INVALID_STATE);
		}
		ServiceState state = status.get(0);
		switch (state) {
			case OK:
				return ResponseEntity.ok().build(); // 200
			case NO_PERMISSION:
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build(); //401
			case COMPOUND_DOCUMENT_NOT_FOUND:
			case DOCUMENT_NOT_FOUND:
				return ResponseEntity.notFound().build(); // 404
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}
	}

	@Override
//	@PreAuthorize("hasPermission(#compoundDocumentId, 'DOKUMENTENMAPPE', 'LESEN')")
	public ResponseEntity<byte[]> getDrucksache(UUID compoundDocumentId) {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_PDF);

		List<ServiceState> status = new ArrayList<>();

		byte[] returnedBytes = pdfService.getDrucksacheForCompoundDocumentId(compoundDocumentId, status);

		if (status.isEmpty()) {
			status.add(INVALID_STATE);
		}
		ServiceState state = status.get(0);
		switch (state) {
			case OK:
				return new ResponseEntity<>(returnedBytes, headers, HttpStatus.OK);// 200
			case NO_PERMISSION:
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build(); //401
			case DRUCK_NOT_FOUND:
				return ResponseEntity.status(HttpStatus.NOT_FOUND).build(); //404
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}
	}


	@Override
	public ResponseEntity<List<PkpZuleitungDokumentDTO>> getCompoundDocumentForKabinett(String gid,
		UUID regelungsvorhabenId) {
		log.debug("Get compound documents for regelungsvorhabenId {}", regelungsvorhabenId);
		List<ServiceState> status = new ArrayList<>();

		List<PkpZuleitungDokumentDTO> pkpZuleitungDokumentDTOs =
			pdfService.getCompoundDocumentsForPKP(
				gid, new RegelungsVorhabenId(regelungsvorhabenId), status);

		ServiceState state = status.get(0);
		log.debug("Got state {} from Service", state); //NOSONAR

		switch (state) {
			case COMPOUND_DOCUMENT_NOT_FOUND: // 204
				return ResponseEntity.noContent()
					.build();
			case REGELUNGSVORHABEN_NOT_FOUND: // 404
				return ResponseEntity.notFound()
					.build();
			case OK: // 200
				return ResponseEntity.ok(pkpZuleitungDokumentDTOs);
			default: // 500
				log.error("An error occurred.");
				return ResponseEntity.internalServerError()
					.build();
		}
	}


}
