// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.UUID;

public interface DrucksachenPlategApi {

    /**
     * Schnittstelle um vom Bundestag die Drucksachennummer in den Editor zu übernehmen
     * <p>
     * PUT /plattform/user/{gid}/drucksachen/{rvid}/drucksachennummer/?drucksachenNummer
     *
     * @param regelungsvorhabenId ID des Regelungsvorhabens zur Drucksache (required)
     * @param drucksachenNummer   Die Nummer der Drucksache
     * @return OK (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or see below
     */
    @Operation(summary = "Übergabe der Drucksachennummer vom Bundestag in den Editor",
        tags = {ApiConstants.API_DIV_DRUCKSACHE},
        responses = {
            @ApiResponse(responseCode = "200", description = "Drucksachennummer angelegt", content = @Content()),
            @ApiResponse(responseCode = "400", description = "Drucksachennummer ungültig.", content = @Content()),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid.", content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights.", content = @Content()),
            @ApiResponse(responseCode = "405", description = "Keine Dokumentenmappe im Status “Bundestag” für Regelungsvorhaben gefunden.",
                content = @Content()),
            @ApiResponse(responseCode = "406", description = "Drucksachennummer existiert schon und kann nicht überschrieben werden.", content = @Content())
        }
    )
    @PutMapping(value = "/plattform/user/{gid}/drucksachen/{rvid}/drucksachennummer/")
    ResponseEntity<Void> drucksachenNummerUebermittlung(
        @Parameter(name = "gid", description = "ID of the user", required = true)
        @PathVariable("gid") String gid,

        @Parameter(in = ParameterIn.PATH, description = "ID des Regelungsvorhabens", required = true, schema = @Schema(implementation = UUID.class))
        @Valid @PathVariable("rvid") UUID regelungsvorhabenId,

        @Parameter(in = ParameterIn.QUERY, description = "Die Nummer der Drucksache", required = true, schema = @Schema(implementation = String.class))
        @Pattern(regexp = "^\\d+/\\d+$", message = "Ungültige Drucknummer. (Gültig wäre so etwas: xxx/yyy)")
        @RequestParam("drucksachenNummer") String drucksachenNummer
    );


}
