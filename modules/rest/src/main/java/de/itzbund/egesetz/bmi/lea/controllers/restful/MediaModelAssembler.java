// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers.restful;

import de.itzbund.egesetz.bmi.lea.controllers.FileController;
import de.itzbund.egesetz.bmi.lea.domain.dtos.media.MediaShortSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.media.MediaSummaryDTO;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class MediaModelAssembler
    implements RepresentationModelAssembler<MediaSummaryDTO, EntityModel<MediaShortSummaryDTO>> {

    @Override
    public EntityModel<MediaShortSummaryDTO> toModel(MediaSummaryDTO mediaSummaryDTO) {

        MediaShortSummaryDTO x =
            MediaShortSummaryDTO.builder()
                .id(mediaSummaryDTO.getId())
                .name((mediaSummaryDTO.getName()))
                .mediaType(mediaSummaryDTO.getMediaType())
                .length(mediaSummaryDTO.getLength())
                .build();

        return EntityModel.of(x,
            linkTo(methodOn(FileController.class).get(
                mediaSummaryDTO.getId()
                    .toString())
            ).withSelfRel()
        );
    }

}
