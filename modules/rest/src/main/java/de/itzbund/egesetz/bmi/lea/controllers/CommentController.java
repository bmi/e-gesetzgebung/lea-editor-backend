// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.api.CommentsApi;
import de.itzbund.egesetz.bmi.lea.api.filter.ContentBase64Filter;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CommentResponseDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CreateCommentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.DeleteCommentsDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.PatchCommentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.ReplyDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.UpdateCommentStatusDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.ReplyId;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CommentRestPort;
import de.itzbund.egesetz.bmi.lea.mappers.CommentMapperRest;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@Log4j2
@SuppressWarnings("unused")
public class CommentController implements CommentsApi {

	private static final String DBG_MSG = "ReplyDTO: {} seems not correct Base64 encoded.\n{}";

	// ----- Services ---------
	@Autowired
	private CommentRestPort commentService;

	// ----- Mapper -----------
	@Autowired
	private CommentMapperRest commentMapperRest;

	// ----- Filter -----------
	@Autowired
	private ContentBase64Filter contentBase64Filter;


	@Override
	public ResponseEntity<CommentResponseDTO> createCommentOld(UUID documentId, CreateCommentDTO createCommentDTO) {
		CreateCommentDTO decodedCreateCommentDTO;
		List<ServiceState> status = new ArrayList<>();
		try {
			decodedCreateCommentDTO = contentBase64Filter.decodeCreateCommentDTO(createCommentDTO);
		} catch (RuntimeException e) {
			log.debug("CreateCommentDTO: {} seems not correct Base64 encoded.\n{}", createCommentDTO, e);
			return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).build(); // 415
		}

		Comment comment = commentService.createCommentOld(
			new DocumentId(documentId),
			decodedCreateCommentDTO,
			status);

		ServiceState serviceState = status.get(0);
		log.debug("Got comment: {} from service with status {}", comment, serviceState);

		CommentResponseDTO encodedCommentResponseDTO = null;
		if (comment != null) {
			encodedCommentResponseDTO =
				contentBase64Filter.encodeCommentResponseDTO(commentMapperRest.map(comment));
		}

		switch (serviceState) {
			case OK:
				return ResponseEntity.ok(encodedCommentResponseDTO); // 200
			case NO_PERMISSION:
				return ResponseEntity.status(HttpStatus.FORBIDDEN).build(); // 403
			case DOCUMENT_NOT_FOUND:
				return ResponseEntity.status(HttpStatus.NOT_FOUND).build(); // 404
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}

	}


	/**
	 * Info Rollen und Rechte: Regel: INSERT INTO `regeln` * (`aktion`, `ressource_typ`, `rolle`, `status`) VALUES ('DOKUMENTE_ERSTELLEN', 'REGELUNGSVORHABEN',
	 * 'MITARBEITER', * 'DONT CARE');
	 *
	 * @param documentId       ID of the document (required)
	 * @param createCommentDTO (required)
	 * @return data of newly created comment
	 */
	@Override
	public ResponseEntity<DocumentDTO> createComment(UUID documentId, CreateCommentDTO createCommentDTO) {
		CreateCommentDTO decodedCreateCommentDTO;
		List<ServiceState> status = new ArrayList<>();
		try {
			decodedCreateCommentDTO = contentBase64Filter.decodeCreateCommentDTO(createCommentDTO);
		} catch (RuntimeException e) {
			log.debug("CreateCommentDTO: {} seems not correct Base64 encoded.\n{}", createCommentDTO, e);
			return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).build(); // 415
		}

		DocumentDTO documentDTO = commentService.createComment(
			new DocumentId(documentId),
			decodedCreateCommentDTO,
			status);

		ServiceState serviceState = status.get(0);

		CommentResponseDTO encodedCommentResponseDTO = null;

		switch (serviceState) {
			case OK:
				return ResponseEntity.ok(documentDTO); // 200
			case NO_PERMISSION:
				return ResponseEntity.status(HttpStatus.FORBIDDEN).build(); // 403
			case DOCUMENT_NOT_FOUND:
				return ResponseEntity.status(HttpStatus.NOT_FOUND).build(); // 404
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}

	}


	@Override
	public ResponseEntity<Void> createReply(UUID documentId, UUID commentId, ReplyDTO replyDTO) {
		List<ServiceState> status = new ArrayList<>();
		Reply decodedReplyDTO;

		try {
			decodedReplyDTO = contentBase64Filter.decodeReplyDTO(replyDTO);
		} catch (RuntimeException e) {
			log.debug(DBG_MSG, replyDTO, e);
			return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).build(); // 415
		}

		commentService.createReply(
			new DocumentId(documentId),
			new CommentId(commentId),
			decodedReplyDTO,
			status
		);

		ServiceState serviceState = status.get(0);
		switch (serviceState) {
			case OK:
				return ResponseEntity.status(HttpStatus.CREATED).build(); // 201
			case NO_PERMISSION:
				return ResponseEntity.status(HttpStatus.FORBIDDEN).build(); // 403
			case REPLY_NOT_FOUND:
			case COMMENT_NOT_FOUND:
				return ResponseEntity.status(HttpStatus.NOT_FOUND).build(); // 404
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}
	}


	@Override
	@PreAuthorize("hasPermission(#documentId, 'DOKUMENTE', 'LESEN')")
	public ResponseEntity<List<CommentResponseDTO>> loadCommentsOfUserOFDocument(UUID documentId, List<UUID> userIds) {
		List<ServiceState> status = new ArrayList<>();
		DocumentId docId = new DocumentId(documentId);

		List<Comment> comments = commentService.loadAllCommentsOfUserOfDocument(docId, status);

		ServiceState serviceState = status.get(0);
		// It's overriden by 'übertragene Leserechte', but should be better implemented.
		if (status.size() > 1) {
			serviceState = status.get(1);
		}

		switch (serviceState) {
			case OK: // 200
				List<CommentResponseDTO> commentDTOs = comments.stream()
					.map(comment -> commentMapperRest.map(comment))
					.collect(Collectors.toList());
				return ResponseEntity.ok(contentBase64Filter.encodeCommentResponseDTOList(commentDTOs));
			case THERE_ARE_NO_COMMENTS:
				return ResponseEntity.status(HttpStatus.NO_CONTENT).build(); // 204
			case NO_PERMISSION:
				return ResponseEntity.status(HttpStatus.FORBIDDEN).build(); // 403
			case DOCUMENT_NOT_FOUND:
				return ResponseEntity.status(HttpStatus.NOT_FOUND).build(); // 404
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}
	}


	@Override
	public ResponseEntity<Void> updateComment(UUID documentId, UUID commentId,
		PatchCommentDTO patchCommentDTO) {
		PatchCommentDTO decodedPatchCommentDTO;
		List<ServiceState> status = new ArrayList<>();
		try {
			decodedPatchCommentDTO = contentBase64Filter.decodePatchCommentDTO(patchCommentDTO);
		} catch (RuntimeException e) {
			log.debug("PatchCommentDTO: {} seems not correct Base64 encoded.\n{}", patchCommentDTO, e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build(); // 500
		}

		commentService.updateComment(
			new DocumentId(documentId),
			new CommentId(commentId),
			decodedPatchCommentDTO,
			status
		);

		ServiceState serviceState = status.get(0);
		log.debug("Updated comment: {} from service with status {}", patchCommentDTO, serviceState);

		switch (serviceState) {
			case OK:
				return ResponseEntity.noContent().build(); // 204
			case COMMENT_NOT_FOUND:
			case DOCUMENT_NOT_FOUND:
				return ResponseEntity.notFound().build(); // 404
			case NO_PERMISSION:
				return ResponseEntity.status(HttpStatus.FORBIDDEN).build(); // 403
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}
	}


	@Override
	public ResponseEntity<Void> updateReply(UUID documentId, UUID commentId, UUID replyId, ReplyDTO replyDTO) {
		List<ServiceState> status = new ArrayList<>();
		Reply decodedReplyDTO;

		try {
			decodedReplyDTO = contentBase64Filter.decodeReplyDTO(replyDTO);
		} catch (RuntimeException e) {
			log.debug(DBG_MSG, replyDTO, e);
			return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).build(); // 415
		}

		commentService.updateReply(
			new DocumentId(documentId),
			new CommentId(commentId),
			new ReplyId(replyId),
			decodedReplyDTO,
			status
		);

		ServiceState serviceState = status.get(0);
		switch (serviceState) {
			case OK:
				return ResponseEntity.ok().build(); // 200
			case NO_PERMISSION:
				return ResponseEntity.status(HttpStatus.FORBIDDEN).build(); // 403
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}
	}


	@Override
	public ResponseEntity<Void> updateCommentState(UUID documentId, UUID commentId,
		UpdateCommentStatusDTO updateCommentStatusDTO) {
		List<ServiceState> status = new ArrayList<>();
		commentService.setCommentState(new DocumentId(documentId), new CommentId(commentId),
			updateCommentStatusDTO.getState(), status);

		ServiceState serviceState = status.get(0);
		switch (serviceState) {
			case OK:
				return ResponseEntity.status(HttpStatus.NO_CONTENT).build(); // 204
			case NO_PERMISSION:
				return ResponseEntity.status(HttpStatus.FORBIDDEN).build(); // 403
			case COMMENT_NOT_FOUND:
			case DOCUMENT_NOT_FOUND:
				return ResponseEntity.notFound().build(); // 404
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}
	}


	@Override
	public ResponseEntity<Void> deleteCommentOld(UUID documentId, UUID commentId) {
		List<ServiceState> status = new ArrayList<>();
		commentService.deleteCommentOld(new DocumentId(documentId), new CommentId(commentId), status);

		ServiceState serviceState = status.get(0);
		switch (serviceState) {
			case OK:
				return ResponseEntity.status(HttpStatus.NO_CONTENT).build(); // 204
			case NO_PERMISSION:
				return ResponseEntity.status(HttpStatus.FORBIDDEN).build(); // 403
			case COMMENT_NOT_FOUND:
			case DOCUMENT_NOT_FOUND:
				return ResponseEntity.notFound().build(); // 404
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}
	}

	@Override
	@PreAuthorize("hasPermission(#documentId, 'DOKUMENTE', 'LESEN')")
	public ResponseEntity<DocumentDTO> deleteComments(UUID documentId, DeleteCommentsDTO deleteCommentsDTO) {
		List<ServiceState> status = new ArrayList<>();

		DocumentDTO documentDTO = commentService.deleteComments(new DocumentId(documentId), deleteCommentsDTO, status);

		ServiceState serviceState = status.get(0);
		switch (serviceState) {
			case OK:
				return ResponseEntity.ok(documentDTO); // 200
			case NO_PERMISSION:
				return ResponseEntity.status(HttpStatus.FORBIDDEN).build(); // 403
			case COMMENT_NOT_FOUND:
			case DOCUMENT_NOT_FOUND:
				return ResponseEntity.notFound().build(); // 404
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}
	}


	@Override
	public ResponseEntity<Void> deleteCommentReply(UUID documentId, UUID commentId, UUID replyId) {
		List<ServiceState> status = new ArrayList<>();
		commentService.deleteCommentReply(new DocumentId(documentId), new CommentId(commentId), new ReplyId(replyId),
			status);

		ServiceState serviceState = status.get(0);
		switch (serviceState) {
			case OK:
				return ResponseEntity.status(HttpStatus.NO_CONTENT).build(); // 204
			case NO_PERMISSION:
				return ResponseEntity.status(HttpStatus.FORBIDDEN).build(); // 403
			case COMMENT_REPLY_NOT_FOUND:
			case COMMENT_NOT_FOUND:
			case DOCUMENT_NOT_FOUND:
				return ResponseEntity.notFound().build(); // 404
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}
	}
}
