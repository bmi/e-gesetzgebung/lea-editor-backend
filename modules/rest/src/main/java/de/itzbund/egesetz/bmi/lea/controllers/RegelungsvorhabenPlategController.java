// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.api.DrucksachenPlategApi;
import de.itzbund.egesetz.bmi.lea.api.RegelungsvorhabenPlategApi;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.RegelungsvorhabenPlategRestPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.validation.Valid;
import java.util.List;

@RequiredArgsConstructor
@Log4j2
public class RegelungsvorhabenPlategController extends DrucksachenPlategPlategController implements RegelungsvorhabenPlategApi, DrucksachenPlategApi {

    @Autowired
    private RegelungsvorhabenPlategRestPort regelungsvorhabenPlategService;

    @Override
    public ResponseEntity<Void> regelungsvorhabenUebermittlung(String gid, @Valid List<RegelungsvorhabenEditorDTO> regelungsvorhabenEditorDTOs) {

        log.debug("Gid der Regelungsvorhabenübermittlung: {}", gid);
        log.debug("Anlagen / Updaten der Regelungsvorhaben, beginnend mit '{}' ", regelungsvorhabenEditorDTOs.get(0).getId());

        List<ServiceState> serviceStates = regelungsvorhabenPlategService.regelungsvorhabenCreateOrUpdate(regelungsvorhabenEditorDTOs);

        switch (serviceStates.get(0)) {
            case OK:
                return ResponseEntity.ok().build(); // 200
            case CREATED:
                return ResponseEntity.status(HttpStatus.CREATED).build(); // 201
            case NOT_MODIFIED:
                return ResponseEntity.status(HttpStatus.CONFLICT).build(); // 409
            default:
                return ResponseEntity.internalServerError().build(); // 500
        }

    }

}
