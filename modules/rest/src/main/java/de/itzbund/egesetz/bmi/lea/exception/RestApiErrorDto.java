// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.exception;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * Simple Error dto for the rest api
 */
@Data
@Builder
public class RestApiErrorDto implements Serializable {

    private static final long serialVersionUID = 1234567L;
    private String messageCode;
    private String messageDetail;

}
