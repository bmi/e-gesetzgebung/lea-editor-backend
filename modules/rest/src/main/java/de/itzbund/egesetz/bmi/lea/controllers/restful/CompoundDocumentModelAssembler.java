// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers.restful;

import de.itzbund.egesetz.bmi.lea.controllers.CompoundDocumentController;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class CompoundDocumentModelAssembler
    implements RepresentationModelAssembler<CompoundDocumentDTO, EntityModel<CompoundDocumentDTO>> {

    @Override
    public EntityModel<CompoundDocumentDTO> toModel(CompoundDocumentDTO compoundDocumentDTO) {
        return EntityModel.of(compoundDocumentDTO,
            linkTo(methodOn(CompoundDocumentController.class).getByIdentifier(
                compoundDocumentDTO.getId())
            ).withSelfRel(),
            linkTo(methodOn(CompoundDocumentController.class).getAll()).withRel("compounddocuments"));
    }

}