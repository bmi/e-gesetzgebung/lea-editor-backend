// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.api.CompoundDocumentPermissionsApi;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CompoundDocumentPermissionRestPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Log4j2
@RequiredArgsConstructor
@RestController
@SuppressWarnings("unused")
public class CompoundDocumentPermissionsController implements CompoundDocumentPermissionsApi {

    @Autowired
    private CompoundDocumentPermissionRestPort compoundDocumentPermissionRestPort;


    @Override
    public ResponseEntity<List<UserDTO>> getByIdentifierAndAction(UUID compoundDocumentId,
        String action) {
        log.debug(
            "Received a request on path '/compounddocuments/{id}/allowedusers/{action}' with parameters "
                + "compoundDocumentId='{}' and action={}.",
            compoundDocumentId, action);

        List<ServiceState> status = new ArrayList<>();

        List<UserDTO> ret =
            compoundDocumentPermissionRestPort.getPermissionsByRessourceIdAndAktion(compoundDocumentId, action,
                status);

        log.debug("Got set of allowed userIds with content '{}'.", ret);

        if (status.contains(ServiceState.INVALID_ARGUMENTS)) {
            return ResponseEntity.badRequest()
                .build(); //400
        }

        if (!ret.isEmpty()) {
            return ResponseEntity.ok(ret); // 200
        }

        return ResponseEntity.noContent()
            .build(); // 204
    }
}
