// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.api;

/**
 * Central location of API-related constants.
 */
public final class ApiConstants {

    public static final String API_DIV_COMMENTS = "Kommentare";
    public static final String API_DIV_COMMENT_ANSWERS = "Antworten auf Kommentare";
    public static final String API_DIV_DOCUMENTS = "Dokumente";
    public static final String API_DIV_USERS = "Benutzer";
    public static final String API_DIV_DOCUMENT_COLLECTIONS = "Dokumentenmappen";
    public static final String API_DIV_MEDIA = "Medien";
    public static final String API_DIV_PROPOSITIONS = "Proposition Controller";
    public static final String API_DIV_PDF = "PDF";
    public static final String API_DIV_DRUCKSACHE = "Drucksachen";
    public static final String API_DIV_KABINETTVERFAHREN = "Kabinettverfahren";
    public static final String API_DIV_GESETZFOLGENABSCHAETZUNG = "Gesetzfolgenabschätzung";
    public static final String API_DIV_HRA = "Haus- und Ressourtabstimmungen";
    public static final String API_DIV_REGELUNGSVORHABEN = "Regelungsvorhaben";
    public static final String API_DIV_BESTANDSRECHT = "Bestandsrecht";

    private ApiConstants() {
    }

}
