// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

@Data
@EqualsAndHashCode(callSuper = false)
public class EgfaException extends RuntimeException {

    private final RestApiErrorDto apiErrorDto;
    private final HttpStatus httpStatus;


    public EgfaException(RestApiErrorDto apiErrorDto, HttpStatus httpStatus) {
        this.apiErrorDto = apiErrorDto;
        this.httpStatus = httpStatus;
    }
}
