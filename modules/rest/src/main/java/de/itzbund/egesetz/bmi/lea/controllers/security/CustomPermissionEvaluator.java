// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers.security;

import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.EditorRollenUndRechte;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.core.Constants.RECHT_LESEN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RESSORT_KURZBEZEICHNUNG_BR;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RESSORT_KURZBEZEICHNUNG_BT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RESSOURCENTYP_DOKUMENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RESSOURCENTYP_DOKUMENTENMAPPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RESSOURCENTYP_HRA;
import static de.itzbund.egesetz.bmi.lea.core.Constants.RESSOURCENTYP_REGELUNGSVORHABEN;

/**
 * @see de.itzbund.egesetz.bmi.lea.domain.aggregate.EditorRollenUndRechte
 */
@Log4j2
@Component
@SuppressWarnings("unused")
public class CustomPermissionEvaluator implements PermissionEvaluator {

	@Autowired
	private EditorRollenUndRechte editorRollenUndRechte;

	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	private LeageSession session;

	private String extractUserGid() {
		return session.getUser().getGid().getId();
	}

	/**
	 * Führt die permission-Prüfung für Berechtigungen, die GrantedAuthority-Objekten zugeordnet sind, aus. Berechtigungen, die GrantedAuthority-Objekten
	 * zugeordnet sind, können beispielsweise "ROLE_USER", "ROLE_ADMIN", "READ_PRIVILEGES", usw. sein. Sie repräsentieren die Berechtigungen, die einem Benutzer
	 * in einer Anwendung gewährt wurden.
	 *
	 * @param auth               The GrantedAuthority
	 * @param targetDomainObject The (domain) object
	 * @param permission         The permission
	 * @return If permission is granted or not
	 */
	@Override
	public boolean hasPermission(
		Authentication auth, Object targetDomainObject, Object permission) {
		log.debug("HasPermission for GrantedAuthority-Objekte");
		if ((auth == null) || (targetDomainObject == null) || !(permission instanceof String)) {
			return false;
		}
		String targetType = targetDomainObject.getClass()
			.getSimpleName()
			.toUpperCase();

		return hasPrivilege(auth, targetType, permission.toString()
			.toUpperCase());
	}

	/**
	 * Führt die permission-Prüfung für Berechtigungen durch, die per Annotation an den Methoden stehen, z.B. "hasPermission(#documentId, 'DOKUMENTE', 'LESEN')
	 * OR hasPermission(#documentId, 'DOKUMENTE', 'SCHREIBEN')"
	 *
	 * @param auth       The GrantedAuthority
	 * @param targetId   Id
	 * @param targetType Type
	 * @param permission Permission that should be proofed
	 * @return If permission is granted or not
	 */
	@Override
	public boolean hasPermission(
		Authentication auth, Serializable targetId, String targetType, Object permission) {
		log.debug("HasPermission for fixed target types");
		if ((auth == null) || (targetType == null) || !(permission instanceof String)) {
			return false;
		}

		String gid = extractUserGid();
		if (gid == null) {
			log.debug("GID had problems");
			return false;
		}

		switch (targetType) {
			case RESSOURCENTYP_DOKUMENTENMAPPE:
				log.debug("DOKUMENTENMAPPE");
				return dokumentenmappePrivilegien(gid, (String) permission, targetType, targetId);
			case RESSOURCENTYP_HRA:
				log.error("HRA not implemented");
				return false;
			case RESSOURCENTYP_REGELUNGSVORHABEN:
				log.debug("REGELUNGSVORHABEN");
				return regelungsvorhabenPrivilegien(gid, (String) permission, targetType, UUID.fromString((String) targetId));
			case RESSOURCENTYP_DOKUMENT:
				log.debug("DOKUMENTE");
				return dokumentePrivilegien(gid, (String) permission, targetType, targetId);
			default:
				log.debug("Undefined targetType.");
				return false;
		}

	}

	private boolean dokumentenmappePrivilegien(String gid, String permission, String targetType, Serializable targetId) {

		//Übergangslösung für BT-User
		String ressort = editorRollenUndRechte.getRessortKurzbezeichnungForNutzer(session.getUser().getGid().getId());
		if (ressort != null && (ressort.equals(RESSORT_KURZBEZEICHNUNG_BR) || ressort.equals(RESSORT_KURZBEZEICHNUNG_BT)) && permission.equals(RECHT_LESEN)) {
			return true;
		}

		if (targetId instanceof UUID) {
			String uuid = targetId.toString();
			return editorRollenUndRechte.isAllowed(gid, permission, targetType, uuid);
		}
		return false;
	}


	private boolean regelungsvorhabenPrivilegien(String gid, String permission, String targetType,
		Serializable targetId) {
		if (targetId instanceof UUID) {
			String regelungsVorhabenId = targetId.toString();
			return editorRollenUndRechte.isAllowed(gid, permission, targetType, regelungsVorhabenId);
		}
		return false;
	}


	private boolean dokumentePrivilegien(String gid, String permission, String targetType, Serializable targetId) {

		boolean teilnehmerAccess = false;
		boolean usersAccess = false;

		//Übergangslösung für BT-User
		String ressort = editorRollenUndRechte.getRessortKurzbezeichnungForNutzer(session.getUser().getGid().getId());
		if (ressort != null && ressort.equals(RESSORT_KURZBEZEICHNUNG_BT)) {
			return true;
		}

		if (targetId instanceof UUID) {
			String uuid = targetId.toString();
			usersAccess = hasUserPriviledges(gid, permission, targetType, uuid);

			// z.B. Teilnehmer, aber nur, wenn der Test davor nicht erfolgreich war.
			if (!usersAccess) {
				teilnehmerAccess = hasOtherUserPriviledges(gid, permission, uuid);
			}
		}

		return usersAccess || teilnehmerAccess;

	}


	@SuppressWarnings("java:S1067")
	private boolean hasOtherUserPriviledges(String gid, String permission, String uuid) {
		boolean teilnehmerAccess = false;
		if ("KOMMENTIEREN".equals(permission)
			|| "KOMMENTAR_ANTWORTEN".equals(permission)
			|| "LESEN".equals(permission)
			|| "SCHREIBEN".equals(permission)
			|| "SUCHENundERSETZEN".equals(permission)
		) {
			// Expliziter Teilnehmer.
			// In der Zuordnungstabelle steht aber leider nur die Dokumentenmappe
			Document document = applicationContext.getBean(Document.class);
			CompoundDocumentId containingCompoundDocumentId = document.getContainingCompoundDocumentId(new DocumentId(UUID.fromString(uuid)));

			if (containingCompoundDocumentId != null) {
				teilnehmerAccess = editorRollenUndRechte.isAllowed(gid, permission, RESSOURCENTYP_DOKUMENTENMAPPE,
					containingCompoundDocumentId.getId().toString());
			}
		} else {
			log.error("unerwartete Permission: {}", permission);
		}
		return teilnehmerAccess;
	}


	private boolean hasUserPriviledges(String gid, String permission, String targetType, String uuid) {
		boolean usersAccess;

		usersAccess = editorRollenUndRechte.isAllowed(gid, permission, targetType, uuid);
		return usersAccess;
	}


	private boolean hasPrivilege(Authentication auth, String targetType, String permission) {
		for (GrantedAuthority grantedAuth : auth.getAuthorities()) {
			if (grantedAuth.getAuthority()
				.startsWith(targetType) &&
				grantedAuth.getAuthority()
					.contains(permission)) {
				return true;
			}
		}
		return false;
	}
}