// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.api.CompoundDocumentApi;
import de.itzbund.egesetz.bmi.lea.api.filter.ContentBase64Filter;
import de.itzbund.egesetz.bmi.lea.controllers.restful.CompoundDocumentModelAssembler;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentTitleDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CopyCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CreateCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.ImportEgfaDataDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.UpdateCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.CreateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentImportDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentMetadataDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaStatusDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.proposition.PropositionDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtListDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtShortDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserRightsDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.enums.VerfahrensType;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.AuxiliaryRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.BestandsrechtRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CompoundDocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.DocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.EgfaRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.ExportRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.RegelungsvorhabenRestPort;
import de.itzbund.egesetz.bmi.lea.mappers.CompoundDocumentMapperRest;
import de.itzbund.egesetz.bmi.lea.mappers.PropositionMapperRest;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static de.itzbund.egesetz.bmi.lea.core.Constants.HTTP_HEADER_CONTENT_LENGTH;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState.COMPOUND_DOCUMENT_NOT_FOUND;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState.INVALID_STATE;

/**
 * A controller for compound documents It provides REST endpoints, Restful responses and delegates functions to its service
 */
@RestController
@RequiredArgsConstructor
@Log4j2
@SuppressWarnings("unused")
public class CompoundDocumentController implements CompoundDocumentApi {

	// ----- Services ---------
	@Autowired
	private CompoundDocumentRestPort compoundDocumentService;
	@Autowired
	private DocumentRestPort documentService;
	@Autowired
	private RegelungsvorhabenRestPort regelungsvorhabenService;
	@Autowired
	private AuxiliaryRestPort auxiliaryService;
	@Autowired
	private EgfaRestPort egfaService;
	@Autowired
	private ExportRestPort exportService;
	@Autowired
	private BestandsrechtRestPort bestandsrechtService;

	// ----- Mapper -----------
	@Autowired
	private CompoundDocumentMapperRest compoundDocumentMapperRest;
	@Autowired
	private PropositionMapperRest propositionMapperRest;

	// ----- Other ------------
	@Autowired
	private CompoundDocumentModelAssembler compoundDocumentModelAssembler;
	@Autowired
	private ContentBase64Filter base64Filter;

	// @formatter:off
    /**
     * Retrieve all stored compound documents:
     *  - has read access
     *  - has write access
     *
     * @return The HTTP state and a representation of the created object's list
     */
    // @formatter:on
	@Override
	public ResponseEntity<List<CompoundDocumentTitleDTO>> getAllTitles() {
		List<ServiceState> status = new ArrayList<>();
		List<CompoundDocumentTitleDTO> compoundDocumentTitleList = compoundDocumentService.getAllTitles(status);
		ServiceState state = status.get(0);

		if (state == ServiceState.OK) {
			if (compoundDocumentTitleList.isEmpty()) {
				return ResponseEntity.status(HttpStatus.NO_CONTENT)
					.body(compoundDocumentTitleList); // 204
			} else {
				return ResponseEntity.ok(compoundDocumentTitleList); // 200
			}
		} else {
			return ResponseEntity.internalServerError()
				.build(); // 500
		}
	}

	// @formatter:off
    /**
     * Creates a new compound document and send it back to caller.
     *
     * Info Rollen und Rechte:
     * - Regel: INSERT INTO `regeln` (`aktion`, `ressource_typ`, `rolle`,`status`) VALUES ('KOMMENTIEREN', 'DOKUMENTE', 'ERSTELLER', 'FINAL');
     *
     * @param createCompoundDocumentDTO A transfer object for compound document creation
     * @return The HTTP state and a representation of the created object
     */
    // @formatter:on
	@Override
	@PreAuthorize("hasPermission(#createCompoundDocumentDTO.regelungsVorhabenId, 'REGELUNGSVORHABEN', 'DOKUMENTE_ERSTELLEN')")
	public ResponseEntity<EntityModel<CompoundDocumentDTO>> createCompoundDocument(
		CreateCompoundDocumentDTO createCompoundDocumentDTO) {
		log.debug("Create compound document with DTO: {}", createCompoundDocumentDTO);
		List<ServiceState> status = new ArrayList<>();

		CreateCompoundDocumentDTO createCompoundDocument = compoundDocumentMapperRest.mapIntoDomain(
			createCompoundDocumentDTO);

		CompoundDocumentDTO compoundDocumentDTO = compoundDocumentService.createCompoundDocument(
			createCompoundDocument, status);

		ServiceState state = status.get(0);

		log.debug("Returned from service with state '{}' and compoundDocumentDTO '{}'", state, compoundDocumentDTO);

		switch (state) {
			case OK: // 201

				// the 'content' attributes of all documents within the compound document need to be Base64 encoded
				return createdResponse(base64Filter.encodeCompoundDocumentDTO(compoundDocumentDTO));
			case INVALID_ARGUMENTS: // 422
				return unprocessableEntity();
			default:
				return unprocessableRequest(); // 500
		}
	}

	/**
	 * Retrieve all stored compound documents actual user can access.
	 *
	 * @return The HTTP state and a representation of the created object's list
	 */
	@Override
	public ResponseEntity<List<EntityModel<CompoundDocumentDTO>>> getAll() {
		List<CompoundDocument> allCompoundDocumentsRestrictedByAccessRights = compoundDocumentService.getAllCompoundDocumentsRestrictedByAccessRights();

		if (allCompoundDocumentsRestrictedByAccessRights.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build(); // 204
		}

		List<CompoundDocumentDTO> compoundDocumentsList = allCompoundDocumentsRestrictedByAccessRights.stream()
			.map(
				compoundDocument -> {
					RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO =
						regelungsvorhabenService.getRegelungsvorhabenEditorDTONEW(compoundDocument.getRegelungsVorhabenId(), true);

					return base64Filter.encodeCompoundDocumentDTO(
						getCompoundDocumentDTOFromAllParts(compoundDocument, regelungsvorhabenEditorDTO));
				}
			)
			.collect(Collectors.toList());

		// Sort compoundDocuments by 'createdAt'
		compoundDocumentsList = compoundDocumentsList.stream()
			.sorted(Comparator.comparing(CompoundDocumentDTO::getCreatedAt).reversed())
			.collect(Collectors.toList());

		CollectionModel<EntityModel<CompoundDocumentDTO>> entityModels = compoundDocumentModelAssembler.toCollectionModel(compoundDocumentsList);

		return ResponseEntity.ok(new ArrayList<>(entityModels.getContent())); // 200
	}

	@Override
	public ResponseEntity<List<EntityModel<CompoundDocumentDTO>>> getByRegulatoryProposalId(UUID regulatoryProposalId) {
		List<CompoundDocument> allCompoundDocumentsRestrictedByAuthor =
			compoundDocumentService.getAllCompoundDocumentsByAuthorAndRegulatoryProposal(regulatoryProposalId);

		if (allCompoundDocumentsRestrictedByAuthor.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT)
				.build(); // 204
		}

		List<CompoundDocumentDTO> compoundDocumentsList = allCompoundDocumentsRestrictedByAuthor.stream()
			.map(
				compoundDocument -> {
					RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO =
						regelungsvorhabenService.getRegelungsvorhabenEditorDTONEW(
							compoundDocument.getRegelungsVorhabenId(),
							compoundDocument.getCompoundDocumentEntity()
								.getState()
								.equals(DocumentState.DRAFT));

					return base64Filter.encodeCompoundDocumentDTO(
						getCompoundDocumentDTOFromAllParts(compoundDocument, regelungsvorhabenEditorDTO));
				}
			)
			.collect(Collectors.toList());

		// Sort compoundDocuments by Version
		compoundDocumentsList = compoundDocumentsList.stream()
			.sorted(
				Comparator.comparing(CompoundDocumentDTO::getVersion)
					.reversed()
			)
			.collect(Collectors.toList());

		CollectionModel<EntityModel<CompoundDocumentDTO>> entityModels
			= compoundDocumentModelAssembler.toCollectionModel(compoundDocumentsList);

		return ResponseEntity
			.ok(new ArrayList<>(entityModels.getContent())); // 200
	}

	@Override
	@PreAuthorize("hasPermission(#compoundDocumentId, 'DOKUMENTENMAPPE', 'LESEN')")
	public ResponseEntity<EntityModel<CompoundDocumentDTO>> getByIdentifier(UUID compoundDocumentId) {
		List<ServiceState> status = new ArrayList<>();
		CompoundDocument compoundDocument = compoundDocumentService.getCompoundDocumentById(
			new CompoundDocumentId(compoundDocumentId), status);
		ServiceState state = status.get(0);

		if (state.equals(COMPOUND_DOCUMENT_NOT_FOUND)) {
			return ResponseEntity.notFound()
				.build(); // 404
		}

		// Get from REST or Database
		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO;
		if (compoundDocument.getCompoundDocumentEntity()
			.getState()
			.equals(DocumentState.DRAFT)) {

			regelungsvorhabenEditorDTO = regelungsvorhabenService.getRegelungsvorhabenEditorDTOForCompoundDocument(compoundDocument);
		} else {
			regelungsvorhabenEditorDTO = regelungsvorhabenService.getRegelungsvorhabenEditorDTONEW(
				compoundDocument.getRegelungsVorhabenId(), false);
		}

		CompoundDocumentDTO compoundDocumentDTO = getCompoundDocumentDTOFromAllParts(
			compoundDocument, regelungsvorhabenEditorDTO);

		// the 'content' attribute of the document within the compound document needs to be Base64 encoded
		return okResponse(base64Filter.encodeCompoundDocumentDTO(compoundDocumentDTO)); // 200
	}

	@Override
	@PreAuthorize("hasPermission(#compoundDocumentId, 'DOKUMENTENMAPPE', 'LESEN')")
	public ResponseEntity<CompoundDocumentSummaryDTO> getDocumentsList(UUID compoundDocumentId) {
		List<ServiceState> status = new ArrayList<>();
		CompoundDocumentSummaryDTO compoundDocumentSummaryDTO =
			compoundDocumentService.getDocumentsList(new CompoundDocumentId(compoundDocumentId), status);

		ServiceState state = status.get(0);
		switch (state) {
			case OK:
				return ResponseEntity.ok(compoundDocumentSummaryDTO);
			case COMPOUND_DOCUMENT_NOT_FOUND:
				return ResponseEntity.notFound()
					.build();
			default:
				return ResponseEntity.internalServerError()
					.build();
		}
	}

	private CompoundDocumentDTO getCompoundDocumentDTOFromAllParts(CompoundDocument compoundDocument,
		RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO) {
		CompoundDocumentDTO compoundDocumentDTO = compoundDocumentMapperRest.mapFromCompoundDocumentAndProposition(
			compoundDocument,
			regelungsvorhabenEditorDTO);

		// Maybe this could be done by the mappers?!
		List<DocumentDTO> documents = compoundDocumentDTO.getDocuments();
		documents
			.forEach(
				documentDTO -> {
					PropositionDTO propositionDTO = propositionMapperRest.map(regelungsvorhabenEditorDTO);
					propositionDTO.setProponentDTO(regelungsvorhabenService.getProponent(
						new RegelungsVorhabenId(regelungsvorhabenEditorDTO.getId())));
					documentDTO.setProposition(propositionDTO);

				});
		return compoundDocumentDTO;
	}

	@Override
	@PreAuthorize("hasPermission(#compoundDocumentId, 'DOKUMENTENMAPPE', 'SCHREIBEN')")
	public ResponseEntity<EntityModel<CompoundDocumentDTO>> addDocuments(UUID compoundDocumentId,
		CreateDocumentDTO createDocumentDTO) {
		List<ServiceState> status = new ArrayList<>();

		Document document = auxiliaryService.makeDocument(new CompoundDocumentId(compoundDocumentId), createDocumentDTO);
		CompoundDocumentDTO compoundDocumentDTO = compoundDocumentService.addDocument(
			new CompoundDocumentId(compoundDocumentId),
			document, status);

		ServiceState state = status.get(0);

		switch (state) {
			case OK: // 200
				// the 'content' attribute of the document within the compound document needs to be Base64 encoded
				return createdResponse(base64Filter.encodeCompoundDocumentDTO(compoundDocumentDTO));
			case COMPOUND_DOCUMENT_NOT_FOUND:
			case COMPOUND_DOCUMENT_ID_MISSING:
			case DOCUMENT_DUPLICATION: // 409
				return ResponseEntity.status(HttpStatus.CONFLICT)
					.build();
			case INVALID_STATE:
			case DOCUMENT_HAS_WRONG_TYPE:
				return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED)
					.build();
			case DOCUMENT_VALIDATION_ERROR: // 422
			case DOCUMENT_NOT_VALID:
				return unprocessableEntity();
			default:
				return unprocessableRequest(); // 500
		}

	}

	@Override
	@PreAuthorize("hasPermission(#compoundDocumentId, 'DOKUMENTENMAPPE', 'SCHREIBEN')")
	public ResponseEntity<DocumentMetadataDTO> importDocument(HttpServletRequest request, UUID compoundDocumentId,
		DocumentImportDTO documentImportDTO) {
		long contentLength = 0L;
		try {
			contentLength = Long.parseLong(request.getHeader(HTTP_HEADER_CONTENT_LENGTH));
		} catch (RuntimeException e) {
			log.error(e);
		}

		List<ServiceState> status = new ArrayList<>();
		DocumentImportDTO decodedDTO = base64Filter.decodeDocumentImportDTO(documentImportDTO);
		if (contentLength == 0L) {
			contentLength = decodedDTO.getXmlContent()
				.getBytes(StandardCharsets.UTF_8).length;
			log.debug("take content for determining content length: {}", contentLength);
		}
		DocumentMetadataDTO documentMetadataDTO = documentService.importDocument(
			new CompoundDocumentId(compoundDocumentId),
			decodedDTO, contentLength, status);

		ServiceState state = status.get(0);

		switch (state) {
			case OK: // 200
				return ResponseEntity.ok(documentMetadataDTO);
			case NO_PERMISSION: // 401
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
					.build();
			case COMPOUND_DOCUMENT_NOT_FOUND: // 404
				return ResponseEntity.notFound()
					.build();
			case INVALID_ARGUMENTS: // 400
				return ResponseEntity.badRequest()
					.build();
			case INVALID_STATE:
				return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED)
					.build();
			default: // 422
				return ResponseEntity.unprocessableEntity()
					.build();
		}
	}

	@Override
	public ResponseEntity<Void> renameAndStatePatch(UUID compoundDocumentId,
		UpdateCompoundDocumentDTO updateCompoundDocumentDTO) {
		ServiceState state = ServiceState.UNKNOWN_ERROR;
		List<ServiceState> status = new ArrayList<>();

		String newTitle = updateCompoundDocumentDTO.getTitle();
		VerfahrensType verfahrensType = updateCompoundDocumentDTO.getVerfahrensTyp();
		DocumentState compoundDOcumentState = updateCompoundDocumentDTO.getState();

		if (!((newTitle == null) && (verfahrensType == null) && (compoundDOcumentState == null))) {
			compoundDocumentService.update(new CompoundDocumentId(compoundDocumentId),
				newTitle, verfahrensType, compoundDOcumentState, status);

			state = status.get(0);
		}

		return getPatchResult(state);
	}

	private ResponseEntity<Void> getPatchResult(ServiceState state) {
		switch (state) {
			case OK: // 200
				return ResponseEntity.ok()
					.build();
			case INVALID_STATE:
			case NO_PERMISSION: // 403
				return ResponseEntity.status(HttpStatus.FORBIDDEN)
					.build();
			case COMPOUND_DOCUMENT_NOT_FOUND: // 404
				return ResponseEntity.notFound()
					.build();
			case ONLY_ONE_KABINETT_VOTING:
			case KABINETT_VOTING_ONGOING:
			case FEHLENDER_STATUS_BUNDESRAT: // 409
				return ResponseEntity.status(HttpStatus.CONFLICT)
					.build();
			default: // 500
				return ResponseEntity.internalServerError()
					.build();
		}
	}

	@Override
	public ResponseEntity<CompoundDocumentSummaryDTO> versionIncrement(UUID compoundDocumentId,
		CopyCompoundDocumentDTO copyCompoundDocumentDTO, List<UUID> copyIds) {
		List<ServiceState> status = new ArrayList<>();
		List<DocumentId> documentIds = new ArrayList<>();
		if (copyIds != null) {
			copyIds.forEach(docId -> documentIds.add(new DocumentId(docId)));
		}

		CompoundDocumentSummaryDTO documentMetadataDTO = compoundDocumentService.copyCompoundDocument(
			new CompoundDocumentId(compoundDocumentId), copyCompoundDocumentDTO.getTitle(), documentIds, status);
		ServiceState state = status.get(0);

		switch (state) {
			case OK:
				return ResponseEntity.ok(documentMetadataDTO); // 200
			case NO_PERMISSION: // 403 ---
				return ResponseEntity.status(HttpStatus.FORBIDDEN)
					.build();
			case COMPOUND_DOCUMENT_NOT_FOUND: // 404 ---
				return ResponseEntity.notFound()
					.build();
			default:
				return ResponseEntity.internalServerError()
					.build(); // 500
		}
	}

	/**
	 * Request handler method to produce a ZIP archive file of all the documents contained in a compound document
	 *
	 * @param response           the response object
	 * @param compoundDocumentId the id of the compound document
	 * @return response
	 */
	@Override
	@PreAuthorize("hasPermission(#compoundDocumentId, 'DOKUMENTENMAPPE', 'LESEN')")
	public ResponseEntity<Void> downloadZipFile(HttpServletResponse response, UUID compoundDocumentId) {
		List<ServiceState> status = new ArrayList<>();
		exportService.exportCompoundDocument(response, compoundDocumentId, status);
		ServiceState state = status.get(0);

		switch (state) {
			case OK:
				return ResponseEntity.ok()
					.build(); // 200
			case COMPOUND_DOCUMENT_NOT_FOUND:
				return ResponseEntity.notFound()
					.build(); // 404
			default:
				return ResponseEntity.internalServerError()
					.build(); // 500
		}
	}

	@Override
	@PreAuthorize("hasPermission(#compoundDocumentId, 'DOKUMENTENMAPPE', 'LESEN')")
	public ResponseEntity<Void> setAccessRights(UUID compoundDocumentId, List<UserRightsDTO> userRightsDTOList) {
		ServiceState state = compoundDocumentService.changeAccessRightsForUsersOnCompoundDocuments(
			new CompoundDocumentId(compoundDocumentId), userRightsDTOList);

		switch (state) {
			case OK:
			case REMOVED_ALL_PERMISSIONS:
				return ResponseEntity.noContent()
					.build(); // 204
			case NO_PERMISSION: // 403
				return ResponseEntity.status(HttpStatus.FORBIDDEN)
					.build();
			case COMPOUND_DOCUMENT_NOT_FOUND:
				return ResponseEntity.notFound()
					.build(); // 404
			default:
				return ResponseEntity.internalServerError()
					.build(); // 500
		}
	}

	@Override
	@PreAuthorize("hasPermission(#compoundDocumentId, 'DOKUMENTENMAPPE', 'SCHREIBEN')")
	public ResponseEntity<HashMap<String, HttpStatus>> importEgfaData(UUID compoundDocumentId,
		ImportEgfaDataDTO importEgfaDataDTO) {
		List<ServiceState> status = new ArrayList<>();

		UUID zielMappenId = compoundDocumentId;

		if (importEgfaDataDTO.isCreateCopy()) {
			CompoundDocumentId dmId = new CompoundDocumentId(compoundDocumentId);
			CompoundDocument compoundDocument =
				compoundDocumentService.getCompoundDocumentById(dmId, new ArrayList<>());
			CompoundDocumentSummaryDTO compoundDocumentSummaryDTO = compoundDocumentService.copyCompoundDocument(
				dmId,
				compoundDocument.getCompoundDocumentEntity().getTitle(),
				compoundDocument.getDocuments().parallelStream().map(Document::getDocumentId).collect(Collectors.toList()),
				new ArrayList<>());
			zielMappenId = compoundDocumentSummaryDTO.getId();
		}

		HashMap<String, HttpStatus> moduleStates = egfaService.egfaUebernahmeInDokumentenmappe(zielMappenId, importEgfaDataDTO, status);

		if (status.isEmpty()) {
			status.add(INVALID_STATE);
		}

		ServiceState state = status.get(0);
		switch (state) {
			case OK: // 200
				return ResponseEntity.ok(moduleStates);
			case INVALID_ARGUMENTS: // 403
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
			default: // 500
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build(); // 500
		}

	}

	@Override
	@PreAuthorize("hasPermission(#compoundDocumentId, 'DOKUMENTENMAPPE', 'LESEN')")
	public ResponseEntity<CompoundDocumentSummaryDTO> changeWritePermissions(UUID compoundDocumentId, Optional<UserRightsDTO> userRight) {
		List<ServiceState> status = new ArrayList<>();

		CompoundDocumentSummaryDTO documentMetadataDTO = compoundDocumentService.changeWritePermissionsOnCompoundDocuments(
			new CompoundDocumentId(compoundDocumentId), userRight.orElse(null), status);

		ServiceState state = status.get(0);

		switch (state) {
			case OK:
				return ResponseEntity.ok(documentMetadataDTO); // 200
			case REMOVED_ALL_PERMISSIONS:
				return ResponseEntity.noContent().build(); // 204
			case NO_PERMISSION: // 403
				return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
			case COMPOUND_DOCUMENT_NOT_FOUND:
				return ResponseEntity.notFound().build(); // 404
			case INCONSISTENT_DATA:
				return ResponseEntity.status(HttpStatus.CONFLICT).build(); // 409
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}
	}

	@Override
	@PreAuthorize("hasPermission(#compoundDocumentId, 'DOKUMENTENMAPPE', 'LESEN')")
	public ResponseEntity<List<EgfaStatusDTO>> getEgfaStateByCompoundDocumentId(UUID compoundDocumentId) {

		List<ServiceState> status = new ArrayList<>();

		List<EgfaStatusDTO> responseEgfaStatusList = egfaService.getEgfaStatusByCompoundDocumentId(status, compoundDocumentId);

		if (status.isEmpty()) {
			status.add(INVALID_STATE);
		}
		ServiceState state = status.get(0);
		switch (state) {
			case OK:
				return ResponseEntity.ok().body(responseEgfaStatusList); // 200
			case NO_PERMISSION:
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build(); //401
			case COMPOUND_DOCUMENT_NOT_FOUND:
				return ResponseEntity.notFound().build(); // 404
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}

	}

	@Override
	@PreAuthorize("hasPermission(#compoundDocumentId, 'DOKUMENTENMAPPE', 'LESEN')")
	public ResponseEntity<List<BestandsrechtListDTO>> getBestandsrechtByCompoundDocumentId(UUID compoundDocumentId) {

		List<ServiceState> status = new ArrayList<>();

		List<BestandsrechtListDTO> bestandsrechtList = bestandsrechtService.getBestandsrechtForCompoundDocumentId(compoundDocumentId, status);

		if (status.isEmpty()) {
			status.add(INVALID_STATE);
		}
		ServiceState state = status.get(0);
		switch (state) {
			case OK:
				return ResponseEntity.ok().body(bestandsrechtList); // 200
			case NO_PERMISSION:
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build(); //401
			case COMPOUND_DOCUMENT_NOT_FOUND:
				return ResponseEntity.notFound().build(); // 404
			default:
				return ResponseEntity.internalServerError().build(); // 500
		}

	}

	@Override
	public ResponseEntity<List<BestandsrechtListDTO>> getBestandsrechtFromCompoundDocumentByCompoundDocumentId(UUID compoundDocumentId) {

		List<ServiceState> errorStates = new ArrayList<>();
		List<BestandsrechtListDTO> bestandsrechtList = bestandsrechtService.getBestandsrechteFromRegelungsvorhabenEnhancedByCompoundDocumentAssignments(
			new CompoundDocumentId(compoundDocumentId),
			errorStates);

		if (bestandsrechtList == null) {

			ServiceState errorState = errorStates.get(0);
			switch (errorState) {
				case NO_PERMISSION:
					return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build(); //401
				case COMPOUND_DOCUMENT_NOT_FOUND:
					return ResponseEntity.notFound().build(); // 404
				default:
					return ResponseEntity.internalServerError().build(); // 500
			}
		}

		return ResponseEntity.ok().body(bestandsrechtList); // 200
	}

	@Override
	public ResponseEntity<List<BestandsrechtListDTO>> setBestandsrechteForCompoundDocumentByCompoundDocumentId(UUID compoundDocumentId,
		List<BestandsrechtShortDTO> bestandsrechtDTOs) {

		List<ServiceState> errorStates = new ArrayList<>();
		List<BestandsrechtListDTO> bestandsrechtList = bestandsrechtService.setBestandsrechteForCompoundDocumentByCompoundDocumentId(
			new CompoundDocumentId(compoundDocumentId), bestandsrechtDTOs, errorStates);

		if (bestandsrechtList.isEmpty()) {

			// leer List -> Anlegen hat nicht geklappt. Ursache:
			ServiceState errorState = errorStates.get(0);
			switch (errorState) {
				case OK:
					break;
				case NO_PERMISSION:
					return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build(); //401
				case COMPOUND_DOCUMENT_NOT_FOUND:
				case BESTANDSRECHT_NICHT_VORHANDEN:
					return ResponseEntity.notFound().build(); // 404
				default:
					return ResponseEntity.internalServerError().build(); // 500
			}

		}

		return ResponseEntity.ok().body(bestandsrechtList); // 200
	}

	// --------- Responses --------------

	private ResponseEntity<EntityModel<CompoundDocumentDTO>> createdResponse(
		CompoundDocumentDTO compoundDocumentCreated) {
		EntityModel<CompoundDocumentDTO> compoundDocumentDTOEntityModel = compoundDocumentModelAssembler.toModel(
			compoundDocumentCreated);

		return ResponseEntity
			.created(compoundDocumentDTOEntityModel.getRequiredLink(IanaLinkRelations.SELF)
				.toUri())
			.body(compoundDocumentDTOEntityModel);
	}

	private ResponseEntity<EntityModel<CompoundDocumentDTO>> okResponse(
		CompoundDocumentDTO compoundDocumentCreated) {
		EntityModel<CompoundDocumentDTO> compoundDocumentDTOEntityModel = compoundDocumentModelAssembler.toModel(
			compoundDocumentCreated);

		return ResponseEntity
			.ok(compoundDocumentDTOEntityModel);
	}

	private ResponseEntity<EntityModel<CompoundDocumentDTO>> unprocessableEntity() {
		return ResponseEntity
			.unprocessableEntity()
			.build();
	}

	private ResponseEntity<EntityModel<CompoundDocumentDTO>> unprocessableRequest() {
		return ResponseEntity.internalServerError()
			.build();
	}

}