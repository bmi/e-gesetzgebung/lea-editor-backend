// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.PkpZuleitungDokumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeParentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.status.UpdateStatusDTO;
import de.itzbund.egesetz.bmi.lea.exception.RestApiErrorDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

/**
 * Alle Schnittstellen, die Plattform aufrufen darf.
 */
public interface PlategApi {

	/**
	 * Sets the state of a document to freeze. This called by platform
	 */
	@Operation(summary = "Verändert den Status einer Dokumentenmappe",
		tags = {ApiConstants.API_DIV_HRA}
	)
	@ApiResponses(value = {
		@ApiResponse(responseCode = "204", description = "successful operation",
			content = @Content(schema = @Schema(hidden = true))),
		@ApiResponse(responseCode = "400", description = "bad request, e.g. wrong status value",
			content = @Content(schema = @Schema(hidden = true))),
		@ApiResponse(responseCode = "404", description = "document could not be found",
			content = @Content(schema = @Schema(hidden = true))),
		@ApiResponse(responseCode = "409", description = "invalid state change with reason",
			content = @Content(mediaType = "application/rtf",
				schema = @Schema(implementation = String.class))
		)
	})
	@PutMapping(value = "/plattform/user/{gid}/compoundDocuments/{id}/status",
		consumes = {MediaType.APPLICATION_JSON_VALUE})
	ResponseEntity<String> veraendereDokumentenmappenStatusDurchPlattform(
		@Parameter(in = ParameterIn.PATH, description = "Gid of the user", required = true,
			schema = @Schema()) @PathVariable("gid") String gid,
		@Parameter(in = ParameterIn.PATH, description = "Uuid of the compound document", required = true,
			schema = @Schema()) @PathVariable("id") String docId,
		@Parameter(in = ParameterIn.DEFAULT, description = "the status to set", schema = @Schema())
		@Valid
		@RequestBody
		UpdateStatusDTO statusDTO);

	/**
	 * Retrieves all documents which are in the compound document
	 */
	@Operation(
		operationId = "getAllDocumentsFromCompoundDocument",
		summary = "Returns documents for a list of compound documents",
		tags = {ApiConstants.API_DIV_KABINETTVERFAHREN},
		responses = {
			@ApiResponse(responseCode = "200", description = "Successful operation",
				content = @Content(mediaType = "application/json",
					array = @ArraySchema(
						schema = @Schema(implementation = CompoundDocumentSummaryDTO.class))
				)
			),
			@ApiResponse(responseCode = "401",
				description = "No permission to get document from compound document",
				content = @Content(schema = @Schema(hidden = true))
			)
		}
	)
	@PostMapping(
		value = "/plattform/user/{gid}/compounddocuments",
		produces = {MediaType.APPLICATION_JSON_VALUE}
	)
	ResponseEntity<List<CompoundDocumentSummaryDTO>> getAllDocumentsFromCompoundDocument(
		@PathVariable("gid") String gid,
		@RequestParam("ids") List<String> compoundDocumentId);

	/**
	 * GET /plattform/user/{gid}/regelungsvorhaben/{rvid}/kabinett/dokumenteOld: Gibt die Dokumente, die Bereit für das Kabinettverfahren sind, zurück
	 *
	 * @param gid  ID of the user (required)
	 * @param rvid ID of the Regelungsvorhaben (required)
	 * @return OK (status code 200) or Keine Dokumentenmappen Bereit für Kabinettverfahren (status code 204) or Authentication information is missing or invalid
	 * (status code 401) or Regelungsvorhaben nicht gefunden (status code 404)
	 */
	@Operation(
		operationId = "getCompoundDocumentForKabinett",
		summary = "Gibt die Dokumente, die Bereit für das Kabinettverfahren sind, zurück",
		tags = {ApiConstants.API_DIV_KABINETTVERFAHREN},
		responses = {
			@ApiResponse(responseCode = "200", description = "OK",
				content = @Content(mediaType = "application/json",
					array = @ArraySchema(
						schema = @Schema(implementation = PkpZuleitungDokumentDTO.class))
				)
			),
			@ApiResponse(responseCode = "204",
				description = "Keine Dokumentenmappen Bereit für Kabinettverfahren",
				content = @Content(schema = @Schema(hidden = true))),
			@ApiResponse(responseCode = "401",
				description = "Authentication information is missing or invalid",
				content = @Content(schema = @Schema(hidden = true))),
			@ApiResponse(responseCode = "404",
				description = "Regelungsvorhaben nicht gefunden",
				content = @Content(schema = @Schema(hidden = true)))
		}
	)
	@GetMapping(
		value = "/plattform/user/{gid}/regelungsvorhaben/{rvid}/kabinett/dokumenteOld",
		produces = {MediaType.APPLICATION_JSON_VALUE}
	)
	ResponseEntity<List<PkpZuleitungDokumentDTO>> getCompoundDocumentForKabinett(
		@Parameter(name = "gid", description = "ID of the user", required = true) @PathVariable("gid") String gid,
		@Parameter(name = "rvid", description = "ID of the 'Regelungsvorhaben'", required = true)
		@PathVariable("rvid") UUID rvid
	);

	/**
	 * Gets a list of compound documents for a given 'Regelungsvorhaben'
	 */
	@Operation(
		operationId = "getCompoundDocumentForRvId",
		summary = "Liefert eine Liste von Dokumentenmappen für ein gegebenes Regelungsvorhaben OHNE Dokumente",
		tags = {ApiConstants.API_DIV_HRA},
		responses = {
			@ApiResponse(responseCode = "200", description = "Successful operation",
				content = @Content(mediaType = "application/json",
					array = @ArraySchema(
						schema = @Schema(implementation = CompoundDocumentSummaryDTO.class))
				)
			),
			@ApiResponse(responseCode = "400", description = "Compound document could not be created",
				content = @Content(schema = @Schema(hidden = true))
			),
			@ApiResponse(responseCode = "401", description = "No permission to create compound document",
				content = @Content(schema = @Schema(hidden = true))
			)
		}
	)
	@GetMapping(
		value = "/plattform/user/{gid}/regelungsvorhaben/{rvid}/compoundDocuments",
		produces = {"application/json"}
	)
	ResponseEntity<List<CompoundDocumentSummaryDTO>> getCompoundDocumentForRvId(@PathVariable("gid") String gid,
		@PathVariable("rvid") String rvid);

	/**
	 * Gibt die Dokumentenmappen des Benutzers zurück, die als Antwort für eine laufende Abstimmung geeignet sind
	 */
	@Operation(
		operationId = "getDokumentenmappenFuerAbstimmung",
		summary = "Liefert Dokumentenmappen, die als Antwort auf eine laufende Abstimmung geeignet sind",
		description = "Die Dokumente liefern nur die Matadaten. <i>cdId</i> und <i>abstimmungsId</i> werden nicht"
			+ " ausgewertet.",
		tags = {ApiConstants.API_DIV_HRA},
		responses = {
			@ApiResponse(responseCode = "200", description = "Successful operation",
				content = @Content(mediaType = "application/json",
					array = @ArraySchema(
						schema = @Schema(implementation = CompoundDocumentSummaryDTO.class))
				)
			),
			@ApiResponse(responseCode = "401",
				description = "No permission to get document from compound document",
				content = @Content(schema = @Schema(hidden = true))
			)
		}
	)
	@GetMapping(
		value = "/plattform/user/{gid}/regelungsvorhaben/{rvid}/compoundDocuments/{cdId}/abstimmung/{abstimmungId}",
		produces = {MediaType.APPLICATION_JSON_VALUE}
	)
	ResponseEntity<List<CompoundDocumentSummaryDTO>> getDokumentenmappenFuerAbstimmung(
		@PathVariable("gid") String gid,
		@PathVariable("rvid") UUID regelungsvorhabenId,
		@PathVariable("cdId") UUID compoundDocumentId,
		@PathVariable("abstimmungId") UUID abstimmungId
	);

	/**
	 * PUT /plattform/user/{gid}/egfa/{regelungsvorhabenId} : Übernimmt eGFA-Daten von der Plattform und integriert sie in die jüngste bearbeitbare
	 * Dokumentenmappe
	 *
	 * @param gid                    ID of the user (required)
	 * @param regelungsvorhabenId    ID of the regulatory proposal (required)
	 * @param egfaDatenuebernahmeDTO (required)
	 * @return OK (status code 200) or Regulatory proposal or compound document not found (status code 204) or Authentication information is missing or invalid
	 * (status code 401) or No access rights to documents (status code 403) or Unknown processing error occurred (status code 500)
	 */
	@Operation(
		operationId = "egfaDatenuebernahme",
		summary = "Übernimmt eGFA-Daten von der Plattform und integriert sie in die jüngste bearbeitbare "
			+ "Dokumentenmappe",
		tags = {ApiConstants.API_DIV_GESETZFOLGENABSCHAETZUNG},
		responses = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "400",
				description = "Request data is not valid. Can contain response body with RestApiErrorDto.",
				content = {@Content(mediaType = "application/json",
					schema = @Schema(nullable = true, implementation = RestApiErrorDto.class))}),
			@ApiResponse(responseCode = "401",
				description = "Authentication information is missing or invalid. "
					+ "Can contain response body with RestApiErrorDto.",
				content = {@Content(mediaType = "application/json",
					schema = @Schema(nullable = true, implementation = RestApiErrorDto.class))}),
			@ApiResponse(responseCode = "403",
				description = "No access rights to documents. Can contain response body with "
					+ "RestApiErrorDto.",
				content = {@Content(mediaType = "application/json",
					schema = @Schema(nullable = true, implementation = RestApiErrorDto.class))}),
			@ApiResponse(responseCode = "404",
				description =
					"The compound document, the Begründung, the Vorblatt or the Regelungstext. Can "
						+ "contain response body with RestApiErrorDto."
						+ "not found",
				content = {@Content(mediaType = "application/json",
					schema = @Schema(nullable = true, implementation = RestApiErrorDto.class))}),
			@ApiResponse(responseCode = "500",
				description = "Unknown processing error occurred. Can contain response body with "
					+ "RestApiErrorDto.",
				content = {@Content(mediaType = "application/json",
					schema = @Schema(nullable = true, implementation = RestApiErrorDto.class))}),
			@ApiResponse(responseCode = "501",
				description = "Module is not yet implemented. Can contain response body with "
					+ "RestApiErrorDto.",
				content = {@Content(
					schema = @Schema(nullable = true, implementation = RestApiErrorDto.class))}
			)
		}
	)
	@PutMapping(
		value = "/plattform/user/{gid}/egfa/{regelungsvorhabenId}",
		consumes = {"application/json"}
	)
	ResponseEntity<Void> egfaDatenuebernahme(
		@Parameter(name = "gid", description = "ID of the user", required = true)
		@PathVariable("gid")
		String gid,
		@Parameter(name = "regelungsvorhabenId", description = "ID of the regulatory proposal", required = true)
		@PathVariable("regelungsvorhabenId")
		UUID regelungsvorhabenId,
		@Parameter(name = "EgfaDatenuebernahmeDTO", description = "EGFA data", required = true)
		@Valid @RequestBody
		EgfaDatenuebernahmeDTO egfaDatenuebernahmeDTO
	);

	/**
	 * PUT /plattform/user/{gid}/egfa/save/{regelungsvorhabenId} : Übernimmt eGFA-Daten von der Plattform und speichert sie in der egfaData Tabelle (Editor DB)
	 *
	 * @param gid                          ID of the user (required)
	 * @param regelungsvorhabenId          ID of the regulatory proposal (required)
	 * @param egfaDatenuebernahmeParentDTO (required)
	 * @return OK (status code 200) or Not modified (status code 304) Regulatory proposal or Request data not valid (status code 400) or No permission (status
	 * code 403) or Unknown processing error occurred (status code 500)
	 */
	@Operation(
		operationId = "egfaDatenSpeicherung",
		summary = "Übernimmt eGFA-Daten von der Plattform und integriert sie in der editor Datenbank.",
		tags = {ApiConstants.API_DIV_GESETZFOLGENABSCHAETZUNG},
		responses = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "304",
				description = "Not modified",
				content = {@Content(mediaType = "application/json",
					schema = @Schema(nullable = true, implementation = RestApiErrorDto.class))}),
			@ApiResponse(responseCode = "400",
				description = "Request data is not valid.",
				content = {@Content(mediaType = "application/json",
					schema = @Schema(nullable = true, implementation = RestApiErrorDto.class))}),
			@ApiResponse(responseCode = "500",
				description = "Unknown processing error occurred. Can contain response body with "
					+ "RestApiErrorDto.",
				content = {@Content(mediaType = "application/json",
					schema = @Schema(nullable = true, implementation = RestApiErrorDto.class))}),
		}
	)
	@PutMapping(
		value = "/plattform/user/{gid}/egfa/save/{regelungsvorhabenId}",
		consumes = {"application/json"}
	)
	ResponseEntity<Void> egfaDatenSpeicherung(
		@Parameter(name = "gid", description = "ID of the user", required = true)
		@PathVariable("gid")
		String gid,
		@Parameter(name = "regelungsvorhabenId", description = "ID of the regulatory proposal", required = true)
		@PathVariable("regelungsvorhabenId")
		UUID regelungsvorhabenId,
		@Parameter(name = "EgfaDatenuebernahmeParentDTO", description = "EGFA data to save", required = true)
		@Valid @RequestBody
		EgfaDatenuebernahmeParentDTO egfaDatenuebernahmeParentDTO
	) throws JsonProcessingException;

	/**
	 * PUT /plattform/user/{gid}/bestandsrecht/save/{regelungsvorhabenId} : Übernimmt Bestandsrecht-Daten von der Plattform, speichert sie in den Bestandsrecht
	 * Tabellen (Editor DB) und stellt eine Verknüpfung zwischen Bestandsrecht und Regelungsvorhaben her
	 *
	 * @param gid                 ID of the user (required)
	 * @param regelungsvorhabenId ID of the regulatory proposal (required)
	 * @param bestandsrechtDTO    (required)
	 * @return OK (status code 200) or Request data not valid (status code 400) or No permission (status code 403) or Unprocessable Entity (status code 422) or
	 * Unknown processing error occurred (status code 500)
	 */
	@Operation(
		operationId = "bestandsrechtDatenSpeicherung",
		summary = "Speicherung des Bestandsrechts (von PKP) im Editor.",
		tags = {ApiConstants.API_DIV_BESTANDSRECHT},
		responses = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "403",
				description = "No permission",
				content = {@Content(mediaType = "application/json",
					schema = @Schema(nullable = true, implementation = RestApiErrorDto.class))}),
			@ApiResponse(responseCode = "400",
				description = "Request data is not valid.",
				content = {@Content(mediaType = "application/json",
					schema = @Schema(nullable = true, implementation = RestApiErrorDto.class))}),
			@ApiResponse(responseCode = "422",
				description = "Request content could not be processed.",
				content = {@Content(mediaType = "application/json",
					schema = @Schema(nullable = true, implementation = RestApiErrorDto.class))}),
			@ApiResponse(responseCode = "500",
				description = "Unknown processing error occurred. Can contain response body with "
					+ "RestApiErrorDto.",
				content = {@Content(mediaType = "application/json",
					schema = @Schema(nullable = true, implementation = RestApiErrorDto.class))}),
		}
	)
	@PutMapping(
		value = "/plattform/user/{gid}/bestandsrecht/save/{regelungsvorhabenId}",
		consumes = {"application/json"}
	)
	ResponseEntity<Void> bestandsrechtDatenSpeicherung(
		@Parameter(name = "gid", description = "ID of the user", required = true)
		@PathVariable("gid")
		String gid,
		@Parameter(name = "regelungsvorhabenId", description = "ID of the regulatory proposal", required = true)
		@PathVariable("regelungsvorhabenId")
		UUID regelungsvorhabenId,
		@Parameter(name = "EgfaDatenuebernahmeParentDTO", description = "EGFA data to save", required = true)
		@Valid @RequestBody
		BestandsrechtDTO bestandsrechtDTO
	) throws JsonProcessingException;

}

