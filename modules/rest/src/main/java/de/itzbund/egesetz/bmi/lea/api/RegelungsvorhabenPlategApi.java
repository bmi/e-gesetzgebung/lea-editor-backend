// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.api;

import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

public interface RegelungsvorhabenPlategApi {

    /**
     * Schnittstelle um ein Regelungsvorhaben in den Editor zu übernehmen
     * <p>
     * PUT /plattform/user/{gid}/regelungsvorhaben/create_or_update
     */
    @Operation(summary = "Erstellung oder Update eines Regelungsvorhabens.",
        tags = {ApiConstants.API_DIV_REGELUNGSVORHABEN},
        responses = {
            @ApiResponse(responseCode = "200", description = "Regelungsvorhaben aktualisiert.", content = @Content()),
            @ApiResponse(responseCode = "201", description = "Regelungsvorhaben erstellt.", content = @Content()),
            @ApiResponse(responseCode = "400", description = "Request Parameter fehlerhaft.", content = @Content()),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid.", content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights.", content = @Content()),
            @ApiResponse(responseCode = "409", description = "Regelungsvorhaben darf nicht mehr verändert werden.", content = @Content())
        }
    )
    @PutMapping(value = "/plattform/user/{gid}/regelungsvorhaben/create_or_update",
        consumes = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<Void> regelungsvorhabenUebermittlung(
        @Parameter(name = "gid", description = "ID of the user", required = true)
        @PathVariable("gid") String gid,

        @Valid
        @Parameter(description = "Eine Liste von Regelungsvorhaben.")
        @RequestBody List<RegelungsvorhabenEditorDTO> regelungsvorhabenEditorDTOs
    );

}
