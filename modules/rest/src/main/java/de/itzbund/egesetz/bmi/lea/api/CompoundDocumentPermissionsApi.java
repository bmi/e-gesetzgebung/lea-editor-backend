// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.api;

import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.api.ApiConstants.API_DIV_DOCUMENT_COLLECTIONS;

public interface CompoundDocumentPermissionsApi {

    /**
     * GET /compounddocuments/{id}/allowedusers/{action} : Returns a list of users which are allowed to perform the given action on the given compound
     * document.
     *
     * @param compoundDocumentId UUID of the compound document (required)
     * @param action             action to be evaluated against as String (required)
     * @return OK (status code 200) or no users with permissions for this action on this CompoundDocument found (status code 204) or Access token is missing or
     * invalid (status code 401) or CompoundDocumentID not found or action invalid (status code 400), or User has invalid rights (status code 403).
     */
    @Operation(summary = "Liefert die Benutzer, die die mitgegebene Aktionen durchführen dürfen.",
        description = "Menüpunkt: <b>Leserechte vergeben</b>..",
        tags = {API_DIV_DOCUMENT_COLLECTIONS},
        responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                content = @Content(mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = UserDTO.class)))),
            @ApiResponse(responseCode = "204", description = "No users found.",
                content = @Content()),
            @ApiResponse(responseCode = "400",
                description = "Bad Request.",
                content = @Content()),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
                content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
        }
    )
    @GetMapping(value = "/compounddocuments/{id}/allowedusers/{action}",
        produces = {"application/json"})
    ResponseEntity<List<UserDTO>> getByIdentifierAndAction(
        @Parameter(in = ParameterIn.PATH, description = "UUID of the compound document", required = true,
            schema = @Schema())
        @PathVariable("id") UUID compoundDocumentId,

        @Parameter(in = ParameterIn.PATH, description = "Name of the action, e.g. LESEN", required = true,
            schema = @Schema())
        @PathVariable("action") String action
    );
}
