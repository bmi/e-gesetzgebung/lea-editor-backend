// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.api.DrucksachenPlategApi;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.DrucksacheRestPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Log4j2
public class DrucksachenPlategPlategController implements DrucksachenPlategApi {

    @Autowired
    private DrucksacheRestPort drucksachenService;

    @Override
    public ResponseEntity<Void> drucksachenNummerUebermittlung(String gid, UUID regelungsvorhabenId, String drucksachenNummer) {

        log.debug("Gid der Drucksachenübermittlung: {}", gid);
        log.debug("Anlageversuch der Drucksachennummer '{}' für das Regelungsvorhaben mit der ID: '{}'", drucksachenNummer, regelungsvorhabenId);

        List<ServiceState> serviceStates = drucksachenService.drucksachenNummerAnlegen(new RegelungsVorhabenId(regelungsvorhabenId), drucksachenNummer);

        switch (serviceStates.get(0)) {
            case OK:
                return ResponseEntity.ok().build(); // 200
            case DRUCKSACHENNUMMER_UNGUELTIG:
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build(); // 400
            case FEHLENDER_STATUS_BUNDESRAT:
                return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build(); // 405
            case DRUCKSACHENNUMMER_BEREITS_VERGEBEN:
            case DRUCKSACHE_FUER_REGELUNGSVORHABEN_VORHANDEN:
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build(); // 406
            case UNKNOWN_ERROR:
                return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build(); // 503
            default:
                return ResponseEntity.internalServerError().build(); // 500
        }

    }

}
