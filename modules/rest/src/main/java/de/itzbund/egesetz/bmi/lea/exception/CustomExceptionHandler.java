// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.exception;

import de.itzbund.egesetz.bmi.lea.domain.services.exceptions.DokumentenmappeNotFoundException;
import de.itzbund.egesetz.bmi.lea.domain.services.exceptions.KeinRechteAnDerRessourceException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@RequiredArgsConstructor
@RestControllerAdvice
@Log4j2
public class CustomExceptionHandler {

    private final DefaultExceptionHandler defaultExceptionHandler;

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> customHandleException(final Exception ex, WebRequest request) throws Exception {
        log.info("Passing exception to default exception handler", ex);
        return defaultExceptionHandler.handleException(ex, request);
    }

    @ExceptionHandler({EgfaException.class})
    public ResponseEntity<Object> handleEgfaException(EgfaException ex) {
        var error = ex.getApiErrorDto();
        var status = ex.getHttpStatus();
        if (error == null) {
            log.warn("Not valid error message given for EgfaException.");
            error = RestApiErrorDto.builder()
                .messageCode("NO_ERROR")
                .messageDetail("No error message given. Something went wrong")
                .build();
        }
        if (status == null) {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(
            error, new HttpHeaders(), status);
    }

    // Exception Handler für DokumentenmappeNotFoundException
    @ExceptionHandler(DokumentenmappeNotFoundException.class)
    public ResponseEntity<String> handleDokumentenmappeNotFoundException(DokumentenmappeNotFoundException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    // Exception Handler für KeinRechteAnDerRessourceException
    @ExceptionHandler(KeinRechteAnDerRessourceException.class)
    public ResponseEntity<String> handleKeinRechteAnDerRessourceException(KeinRechteAnDerRessourceException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.FORBIDDEN);
    }
}
