// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.api;

import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserSettingsDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

import static de.itzbund.egesetz.bmi.lea.api.ApiConstants.API_DIV_USERS;

@Validated
public interface UserApi {

    /**
     * @return The current user
     */
    @Operation(summary = "Get current user",
        tags = {API_DIV_USERS},
        responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = UserDTO.class))),
            @ApiResponse(responseCode = "301", description = "No more used", content = @Content()),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid", content = @Content()),
            @ApiResponse(responseCode = "404", description = "User not found.", content = @Content())
        }
    )
    @Parameter(in = ParameterIn.HEADER, name = "X-DE.BUND.EGG-Surrogate-For", schema = @Schema(type = "gid"))
    @GetMapping(value = "/user",
        produces = {"application/json"})
    ResponseEntity<UserDTO> getUser();


    /**
     * PUT /user/settings : Updates the settings of the current user
     *
     * @param userSettingsDTO (required)
     * @return the settings of the current user
     */
    @Operation(summary = "Updates the users settings.",
        tags = {API_DIV_USERS},
        responses = {
            @ApiResponse(responseCode = "200", description = "OK.",
                content = @Content(mediaType = "application/json", schema = @Schema(implementation = DocumentDTO.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request </br> Content is invalid.", content = @Content()),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid-", content = @Content()),
            @ApiResponse(responseCode = "405", description = "No Permission.", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal Server Error.", content = @Content())
        }
    )
    @PutMapping(value = "/user/settings",
        produces = {"application/json"},
        consumes = {"application/json"})
    ResponseEntity<Void> updateSettings(
        @Parameter(name = "UserSettingsDTO", required = true, schema = @Schema()) @Valid @RequestBody UserSettingsDTO userSettingsDTO
    );

    /**
     * GET /user/settings : Retrieves the settings of the current user
     *
     * @return the settings of the current user
     */
    @Operation(summary = "Retrieves the users settings.",
        tags = {API_DIV_USERS},
        responses = {
            @ApiResponse(responseCode = "200", description = "OK.",
                content = @Content(mediaType = "application/json", schema = @Schema(implementation = UserSettingsDTO.class))),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid-", content = @Content()),
            @ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
        }
    )
    @GetMapping(value = "/user/settings",
        produces = {"application/json"})
    ResponseEntity<UserSettingsDTO> getSettings();

}
