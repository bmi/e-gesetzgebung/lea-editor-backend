// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.mappers;

import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CommentResponseDTO;
import de.itzbund.egesetz.bmi.lea.domain.mappers.BaseMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true),
    uses = {BaseMapper.class, ReplyMapperRest.class})
public interface CommentMapperRest {

    // qualifiedByName defined in BaseMapper
    @Mapping(target = "documentId", source = "documentId", qualifiedByName = "documentIdStringMapping")
    @Mapping(target = "commentId", source = "commentId", qualifiedByName = "commentIdStringMapping")
    @Mapping(target = "updatedBy.gid", source = "updatedBy.gid", qualifiedByName = "userIdStringMapping")
    @Mapping(target = "createdBy.gid", source = "createdBy.gid", qualifiedByName = "userIdStringMapping")
    CommentResponseDTO map(Comment comment);

}
