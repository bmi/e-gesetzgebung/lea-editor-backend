// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.api;

import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CommentResponseDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.CreateCommentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.DeleteCommentsDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.PatchCommentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.ReplyDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.comment.UpdateCommentStatusDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Validated
public interface CommentsApi {

	/**
	 * GET /documents/{id}/comments : Loads all comments of a document from a specific user
	 *
	 * @param id ID of the document (required)
	 * @return OK (status code 200) or No content found (status code 204) or Access token is missing or invalid (status code 401) or User has invalid rights
	 * (status code 403) or Not Found &lt;/br&gt; Document not found or **documentId** invalid. (status code 404)
	 */
	@Operation(
		summary = "Loads all comments of a document from a specific user",
		tags = {ApiConstants.API_DIV_COMMENTS},
		responses = {
			@ApiResponse(responseCode = "200", description = "OK",
				content = @Content(mediaType = "application/json",
					schema = @Schema(implementation = CommentResponseDTO.class))),
			@ApiResponse(responseCode = "204", description = "No content found", content = @Content()),
			@ApiResponse(responseCode = "401", description = "Access token is missing or invalid", content = @Content()),
			@ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
			@ApiResponse(responseCode = "404", description = "Not Found </br> Document not found or **documentId** invalid.", content = @Content())
		}
	)
	@GetMapping(value = "/documents/{id}/comments",
		produces = {"application/json"})
	ResponseEntity<List<CommentResponseDTO>> loadCommentsOfUserOFDocument(
		@Parameter(name = "id", description = "ID of the document", required = true, schema = @Schema())
		@PathVariable("id") UUID id,
		@Parameter(name = "userid", description = "Filter comments for user, but not implemented", schema = @Schema())
		@Valid @RequestParam(value = "userid", required = false) List<UUID> userids
	);

	/**
	 * POST /documents/{id}/comments : Creates a comment (old)
	 *
	 * @param documentId       ID of the document (required)
	 * @param createCommentDTO (required)
	 * @return OK (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Not Found
	 * &lt;/br&gt; Document not found or **documentId** invalid. (status code 404)
	 */
	@Operation(summary = "Creates a comment",
		tags = {ApiConstants.API_DIV_COMMENTS},
		responses = {
			@ApiResponse(responseCode = "200", description = "OK",
				content = @Content(mediaType = "application/json",
					schema = @Schema(implementation = CommentResponseDTO.class))),
			@ApiResponse(responseCode = "401", description = "Access token is missing or invalid", content = @Content()),
			@ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
			@ApiResponse(responseCode = "404", description = "Not Found </br> Document not found or **documentId** invalid.", content = @Content()),
			@ApiResponse(responseCode = "415", description = "Problems with BASE64Encoding.", content = @Content())
		}
	)
	@PostMapping(value = "/documents/{id}/comments",
		produces = {"application/json"},
		consumes = {"application/json"})
	ResponseEntity<CommentResponseDTO> createCommentOld(
		@Parameter(in = ParameterIn.PATH, description = "ID of the document", required = true, schema = @Schema())
		@PathVariable("id") UUID documentId,

		@Parameter(in = ParameterIn.DEFAULT, schema = @Schema())
		@Valid @RequestBody CreateCommentDTO createCommentDTO
	);

	/**
	 * POST /documents/{id}/commentsNEW : Creates a comment
	 *
	 * @param documentId       ID of the document (required)
	 * @param createCommentDTO (required)
	 * @return OK (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Not Found
	 * &lt;/br&gt; Document not found or **documentId** invalid. (status code 404)
	 */
	@Operation(summary = "Creates a comment",
		tags = {ApiConstants.API_DIV_COMMENTS},
		responses = {
			@ApiResponse(responseCode = "200", description = "OK",
				content = @Content(mediaType = "application/json",
					schema = @Schema(implementation = DocumentDTO.class))),
			@ApiResponse(responseCode = "401", description = "Access token is missing or invalid", content = @Content()),
			@ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
			@ApiResponse(responseCode = "404", description = "Not Found </br> Document not found or **documentId** invalid.", content = @Content()),
			@ApiResponse(responseCode = "415", description = "Problems with BASE64Encoding.", content = @Content())
		}
	)
	@PostMapping(value = "/documents/{id}/commentsNEW",
		produces = {"application/json"},
		consumes = {"application/json"})
	ResponseEntity<DocumentDTO> createComment(
		@Parameter(in = ParameterIn.PATH, description = "ID of the document", required = true, schema = @Schema())
		@PathVariable("id") UUID documentId,

		@Parameter(in = ParameterIn.DEFAULT, schema = @Schema())
		@Valid @RequestBody CreateCommentDTO createCommentDTO
	);


	@Operation(
		summary = "Creates a reply to a comment",
		tags = {ApiConstants.API_DIV_COMMENT_ANSWERS},
		responses = {
			@ApiResponse(responseCode = "201", description = "OK", content = @Content()),
			@ApiResponse(responseCode = "401", description = "Access Token is missing", content = @Content()),
			@ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
			@ApiResponse(responseCode = "500", description = "Some unexpected error occured during request processing", content = @Content())
		}
	)
	@PostMapping(
		value = "/documents/{id}/comments/{commentId}/reply",
		consumes = {"application/json"}
	)
	ResponseEntity<Void> createReply(
		@Parameter(in = ParameterIn.PATH, description = "ID of the document", required = true, schema = @Schema())
		@PathVariable("id")
		UUID documentId,

		@Parameter(in = ParameterIn.PATH, description = "ID of the comment", required = true, schema = @Schema())
		@PathVariable("commentId")
		UUID commentId,

		@Parameter(in = ParameterIn.DEFAULT, schema = @Schema())
		@RequestBody
		@Valid
		ReplyDTO replyDTO
	);

	/**
	 * PATCH /documents/{id}/comments/{commentId} : Updates a comment
	 *
	 * @param documentId      ID of the document (required)
	 * @param commentId       ID of the comment (required)
	 * @param patchCommentDTO (required)
	 * @return OK (status code 200) or No content (status code 204) or Access token is missing or invalid (status code 401) or User has invalid rights (status
	 * code 403) or Not Found &lt;/br&gt;  Document or Comment not found. (status code 404) or Internal Server Error &lt;/br&gt; Some error during server-side
	 * processing occurred. (status code 500)
	 */
	@Operation(summary = "Updates a comment",
		tags = {ApiConstants.API_DIV_COMMENTS},
		responses = {
			@ApiResponse(responseCode = "204", description = "No content", content = @Content()),
			@ApiResponse(responseCode = "401", description = "Access token is missing or invalid", content = @Content()),
			@ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
			@ApiResponse(responseCode = "404", description = "Not Found </br>  Document or Comment not found.", content = @Content()),
			@ApiResponse(responseCode = "500", description = "Internal Server Error </br> Some error during server-side processing occurred.",
				content = @Content()),
		}
	)
	@PatchMapping(value = "/documents/{id}/comments/{commentId}",
		consumes = {"application/json"})
	ResponseEntity<Void> updateComment(
		@Parameter(name = "id", description = "ID of the document", required = true)
		@PathVariable("id") UUID documentId,

		@Parameter(name = "commentId", description = "ID of the comment", required = true)
		@PathVariable("commentId") UUID commentId,

		@Parameter(name = "CreateCommentDTO", required = true)
		@Valid @RequestBody PatchCommentDTO patchCommentDTO);


	/**
	 * DELETE /documents/{id}/comments/{commentId} : Deletes a comment (OLD)
	 *
	 * @param documentId ID of the document (required)
	 * @param commentId  ID of the comment (required)
	 * @return No content (status code 204) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Not Found
	 * &lt;/br&gt;  Document or Comment not found. (status code 404)
	 */
	@Operation(
		summary = "Deletes a comment",
		tags = {ApiConstants.API_DIV_COMMENTS},
		responses = {
			@ApiResponse(responseCode = "204", description = "No content", content = @Content()),
			@ApiResponse(responseCode = "401", description = "Access token is missing or invalid", content = @Content()),
			@ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
			@ApiResponse(responseCode = "404", description = "Not Found </br>  Document or Comment not found.",
				content = @Content()),
			@ApiResponse(responseCode = "500", description = "Internal Server Error </br> Some error during server-side processing occurred.",
				content = @Content())
		}
	)
	@DeleteMapping(
		value = "/documents/{id}/comments/{commentId}",
		produces = {"application/json"}
	)
	ResponseEntity<Void> deleteCommentOld(
		@Parameter(
			in = ParameterIn.PATH,
			description = "ID of the document",
			required = true,
			schema = @Schema()
		)
		@PathVariable("id") UUID documentId,

		@Parameter(
			in = ParameterIn.PATH,
			required = true
		)
		@PathVariable("commentId") UUID commentId
	);


	/**
	 * DELETE /documents/{id}/commentsNEW : Deletes a list of comments
	 *
	 * @param documentId        ID of the document (required)
	 * @param deleteCommentsDTO (required)
	 * @return No content (status code 204) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Not Found
	 * &lt;/br&gt;  Document or Comment not found. (status code 404)
	 */
	@Operation(
		summary = "Deletes a comment",
		tags = {ApiConstants.API_DIV_COMMENTS},
		responses = {
			@ApiResponse(responseCode = "204", description = "No content", content = @Content()),
			@ApiResponse(responseCode = "401", description = "Access token is missing or invalid", content = @Content()),
			@ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
			@ApiResponse(responseCode = "404", description = "Not Found </br>  Document or Comment not found.",
				content = @Content()),
			@ApiResponse(responseCode = "500", description = "Internal Server Error </br> Some error during server-side processing occurred.",
				content = @Content())
		}
	)
	@DeleteMapping(
		value = "/documents/{id}/commentsNEW",
		produces = {"application/json"}
	)
	ResponseEntity<DocumentDTO> deleteComments(
		@Parameter(
			in = ParameterIn.PATH,
			description = "ID of the document",
			required = true,
			schema = @Schema()
		)
		@PathVariable("id") UUID documentId,
		@Parameter(name = "DeleteCommentsDTO", description = "", required = true, schema = @Schema()) @Valid @RequestBody DeleteCommentsDTO deleteCommentsDTO
	);


	@Operation(
		summary = "Update a reply",
		tags = {ApiConstants.API_DIV_COMMENT_ANSWERS},
		responses = {
			@ApiResponse(responseCode = "201", description = "OK", content = @Content()),
			@ApiResponse(responseCode = "401", description = "Access Token is missing", content = @Content()),
			@ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
			@ApiResponse(responseCode = "500", description = "Some unexpected error occured during request processing", content = @Content())
		}
	)
	@PatchMapping(
		value = "/documents/{id}/comments/{commentId}/reply/{replyId}",
		consumes = {"application/json"}
	)
	ResponseEntity<Void> updateReply(
		@Parameter(in = ParameterIn.PATH, description = "ID of the document", required = true, schema = @Schema())
		@PathVariable("id")
		UUID documentId,

		@Parameter(in = ParameterIn.PATH, description = "ID of the comment", required = true, schema = @Schema())
		@PathVariable("commentId")
		UUID commentId,

		@Parameter(in = ParameterIn.PATH, description = "ID of the reply", required = true, schema = @Schema())
		@PathVariable("replyId")
		UUID replyId,

		@Parameter(in = ParameterIn.DEFAULT, schema = @Schema())
		@RequestBody
		@Valid
		ReplyDTO replyDTO
	);

	@Operation(
		summary = "Updates a comment's state",
		tags = {ApiConstants.API_DIV_COMMENTS},
		responses = {
			@ApiResponse(responseCode = "204", description = "Operation successful, no content", content =
			@Content()),
			@ApiResponse(responseCode = "401", description = "Access token is missing or invalid", content = @Content()),
			@ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
			@ApiResponse(responseCode = "404", description = "Document or Comment not found", content = @Content()),
			@ApiResponse(responseCode = "500", description = "Some error during server-side processing occurred",
				content = @Content()),
		}
	)
	@PutMapping(
		value = "/documents/{id}/comments/{commentId}/status",
		consumes = {MediaType.APPLICATION_JSON_VALUE}
	)
	ResponseEntity<Void> updateCommentState(
		@Parameter(in = ParameterIn.PATH, name = "id", description = "ID of the document", required = true)
		@PathVariable("id") UUID documentId,

		@Parameter(in = ParameterIn.PATH, name = "commentId", description = "ID of the comment", required = true)
		@PathVariable("commentId") UUID commentId,

		@Parameter(in = ParameterIn.DEFAULT, name = "An object containing the new state's value", required = true)
		@Valid @RequestBody UpdateCommentStatusDTO updateCommentStatusDTO);

	/**
	 * DELETE /documents/{id}/comments/{commentId}/reply/{replyId} : Deletes a reply on a comment
	 *
	 * @param id        ID of the document (required)
	 * @param commentId ID of the comment (required)
	 * @param replyId   ID of the comment (required)
	 * @return No content (status code 204) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Not Found
	 * &lt;/br&gt;  Document, Comment or Reply not found. (status code 404)
	 */
	@Operation(
		summary = "Deletes a reply on a comment",
		tags = {ApiConstants.API_DIV_COMMENT_ANSWERS},
		responses = {
			@ApiResponse(responseCode = "204", description = "No content", content = @Content()),
			@ApiResponse(responseCode = "401", description = "Access token is missing or invalid", content = @Content()),
			@ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
			@ApiResponse(responseCode = "404", description = "Not Found </br>  Document, Comment or Reply not found.", content = @Content())
		}
	)
	@DeleteMapping(
		value = "/documents/{id}/comments/{commentId}/reply/{replyId}"
	)
	ResponseEntity<Void> deleteCommentReply(
		@Parameter(name = "id", description = "ID of the document", required = true) @PathVariable("id") UUID id,
		@Parameter(name = "commentId", description = "ID of the comment", required = true) @PathVariable("commentId") UUID commentId,
		@Parameter(name = "replyId", description = "ID of the comment", required = true) @PathVariable("replyId") UUID replyId
	);

}
