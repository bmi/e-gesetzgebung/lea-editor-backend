// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.api;

import de.itzbund.egesetz.bmi.lea.domain.dtos.synopsis.SynopsisRequestDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.synopsis.SynopsisResponseDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Validated
public interface SynopseApi {

    /**
     * POST /synopse : Gets contents of two or more documents which are compared to each other. There is exactly one base document and at least one version
     * document to be compared to base.
     *
     * @param synopsisRequestDTO (required)
     * @return OK (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Not Found
     * &lt;/br&gt; Compound Document not found or **documentId** invalid. (status code 404) or Internal error during comparison (status code 500)
     */
    @Operation(
        operationId = "getSynopsis",
        summary = "Gets contents of two or more documents which are compared to each other.",
        tags = {"Synopse"},
        responses = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                @Content(mediaType = "application/json",
                    schema = @Schema(implementation = SynopsisResponseDTO.class))
            }),
            @ApiResponse(responseCode = "401", description = "Access token is missing or invalid"),
            @ApiResponse(responseCode = "403", description = "User has invalid rights"),
            @ApiResponse(responseCode = "404",
                description = "Not Found </br> Compound Document not found or **documentId** invalid."),
            @ApiResponse(responseCode = "500", description = "Internal error during comparison")
        }
    )
    @PostMapping(
        value = "/synopse",
        produces = {"application/json"},
        consumes = {"application/json"}
    )
    ResponseEntity<SynopsisResponseDTO> getSynopsis(
        @Parameter(name = "SynopsisRequestDTO", required = true)
        @Valid @RequestBody
        SynopsisRequestDTO synopsisRequestDTO
    );

    @SuppressWarnings("unused")
    @Operation(operationId = "getTextCompareResult", tags = {"Synopse"})
    @PostMapping(value = "/textCompare", produces = {MediaType.TEXT_HTML_VALUE})
    ResponseEntity<String> getTextCompareResult(
        @RequestParam @Schema(example = "Hello, World!", required = true)
        String baseText,
        @RequestParam @Schema(example = "Goodbye, World!", required = true)
        String versionText
    );

}
