// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.api.MediaApi;
import de.itzbund.egesetz.bmi.lea.controllers.restful.MediaModelAssembler;
import de.itzbund.egesetz.bmi.lea.domain.dtos.media.MediaShortSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.media.MediaSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.FileRestPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Controller class for Files. Purpose of this class is to pass http requests to the {@link FileRestPort}.
 */

@RestController
@RequiredArgsConstructor
@Log4j2
@SuppressWarnings("unused")
public class FileController implements MediaApi {

    @Autowired
    private final FileRestPort fileService;

    private final CacheControl cacheControl = CacheControl.maxAge(3600, TimeUnit.SECONDS)
        .cachePrivate();

    @Autowired
    private MediaModelAssembler mediaModelAssembler;


    /**
     * Request handler method to retrieve data from repository with given id
     *
     * @param mediaId ID of the data to be retrieved
     * @return Response with file encapsulated as {@link Resource}
     */
    public ResponseEntity<Resource> get(String mediaId) {
        MediaSummaryDTO mediaSummaryDTO = fileService.loadFromDB(UUID.fromString(mediaId));

        return ResponseEntity.ok() // 200
            .contentLength(mediaSummaryDTO.getLength())
            .contentType(mediaSummaryDTO.getMediaType())
            .cacheControl(cacheControl)
            .body(mediaSummaryDTO.getByteArrayResource());
    }


    /**
     * Request handler method to save data encapsulated {@link MultipartFile} with given name
     *
     * @param fileName Name of the file
     * @param data     MultipartFile which contains data to be saved
     * @return the path where the resource can be downloaded.
     */
    public ResponseEntity<EntityModel<MediaShortSummaryDTO>> postFile(String fileName,
        MultipartFile data) {
        // As the service is implemented bad, we catch it here at the moment
        if (!fileName.endsWith(".jpg")) {
            return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).build();
        }

        UUID mediaId = fileService.save(fileName, data);
        MediaSummaryDTO mediaSummaryDTO = fileService.loadFromDB(mediaId);

        EntityModel<MediaShortSummaryDTO> mediaSummeryDTOEntityModel = mediaModelAssembler.toModel(
            mediaSummaryDTO);

        return ResponseEntity
            .created(mediaSummeryDTOEntityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
            .body(mediaSummeryDTOEntityModel);
    }


    /**
     * Request handler method to delete data from repository
     *
     * @param mediaId ID of the data to be deleted
     */
    //    @Hidden
    public ResponseEntity<Void> delete(UUID mediaId) {
        fileService.deleteFromDB(mediaId);
        return ResponseEntity.noContent().build(); // 202
    }

}
