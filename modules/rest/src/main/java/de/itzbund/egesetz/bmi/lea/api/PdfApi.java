// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.api;

import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.PkpZuleitungDokumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.UpdateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtListDTO;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationException;
import de.itzbund.egesetz.bmi.lea.export.document.MultipleDocumentDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.api.ApiConstants.API_DIV_PDF;

@RequestMapping(path = "/")
public interface PdfApi {

	/**
	 *
	 */
	@Operation(
		operationId = "exportPdf",
		summary = "Produces a PDF document.",
		description =
			"Receives a documentID and produces a PDF document.",
		tags = {API_DIV_PDF},
		responses = {
			@ApiResponse(responseCode = "200", description = "OK",
				content = @Content(mediaType = "application/pdf",
					schema = @Schema(implementation = String.class))),
			@ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
				content = @Content()),
			@ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
			@ApiResponse(responseCode = "409",
				description = "Document could not be exported due to either XML parsing error or failed "
					+ "XML validation",
				content = @Content()),
			@ApiResponse(responseCode = "500",
				description = "Internal Server Error </br> Some error during server-side processing "
					+ "occurred.",
				content = @Content())
		}
	)
	@GetMapping(
		value = "/documents/{id}/export/pdf",
		produces = MediaType.APPLICATION_PDF_VALUE
	)
	ResponseEntity<byte[]> exportPdf(
		@Parameter(
			name = "id",
			description = "Uuid of the document",
			required = true,
			schema = @Schema())
		@Valid
		@PathVariable("id") UUID documentId
	) throws IOException;

	@Operation(
		operationId = "exportMultiplePdf",
		summary = "Produces a PDF document containing multiple documents.",
		description =
			"Receives a list of documentID which have to be joined in one PDF document.",
		tags = {API_DIV_PDF},
		responses = {
			@ApiResponse(responseCode = "200", description = "OK",
				content = @Content(mediaType = "application/pdf",
					schema = @Schema(implementation = String.class))),
			@ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
				content = @Content()),
			@ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
			@ApiResponse(responseCode = "409",
				description = "Document could not be exported due to either XML parsing error or failed "
					+ "XML validation",
				content = @Content()),
			@ApiResponse(responseCode = "500",
				description = "Internal Server Error </br> Some error during server-side processing "
					+ "occurred.",
				content = @Content())
		}
	)
	@PostMapping(
		value = "/documents/multiple/export/pdf",
		consumes = {"application/json"}
	)
	ResponseEntity<byte[]> exportPdf(
		@Parameter(name = "MultipleDocumentDTO",
			description = "DTO containing a list of documentID, a flag for merging the documents, and a title for the exported file.",
			required = true, schema = @Schema())
		@Valid @RequestBody MultipleDocumentDTO multipleDocumentDTO
	);

	@Operation(
		operationId = "exportSynopsisPdf",
		summary = "Produces a PDF document containing the synopsis.",
		description =
			"Receives a base64 encoded JSON containing a synopsis and displays this synopsis in a PDF document.",
		tags = {API_DIV_PDF},
		responses = {
			@ApiResponse(responseCode = "200", description = "OK",
				content = @Content(mediaType = "application/pdf",
					schema = @Schema(implementation = String.class))),
			@ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
				content = @Content()),
			@ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
			@ApiResponse(responseCode = "409",
				description = "Document could not be exported due to either XML parsing error or failed "
					+ "XML validation",
				content = @Content()),
			@ApiResponse(responseCode = "500",
				description = "Internal Server Error </br> Some error during server-side processing "
					+ "occurred.",
				content = @Content())
		}
	)
	@PostMapping(
		value = "/synopsis/export/pdf",
		consumes = {"application/json"},
		produces = MediaType.APPLICATION_PDF_VALUE
	)
	ResponseEntity<byte[]> exportSynopsisPdf(
		@Parameter(name = "Synopsis",
			description = "The synopsis with a title and a base64 encoded content.",
			required = true, schema = @Schema())
		@Valid @RequestBody UpdateDocumentDTO synopsis
	) throws ValidationException;


	/**
	 * PUT /compounddocuments/{id}/druckSpeichern : Speichert die Drucke des Bundestags, damit sie als Links bzw. Verweise von einer Dokumentenmappe
	 * referenziert werden können.
	 *
	 * @param compoundDocumentId – UUID of the compound document (required)
	 * @return OK (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or Not Found, Compound
	 * Document not found or **compoundDocumentId** invalid. (status code 404) or error (status code 500)
	 */
	@Operation(summary = "Speichert die Drucke des Bundestags, damit sie als Links bzw. Verweise von einer Dokumentenmappe referenziert werden können.",
		description = "Speichert die Drucke des Bundestags, damit sie als Links bzw. Verweise von einer Dokumentenmappe referenziert werden können.",
		tags = {API_DIV_PDF},
		responses = {
			@ApiResponse(responseCode = "200", description = "OK",
				content = @Content(mediaType = "application/json",
					schema = @Schema(implementation = CompoundDocumentDTO.class))),
			@ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
				content = @Content()),
			@ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
			@ApiResponse(responseCode = "404",
				description = "Not Found </br> Compound Document not found or **compoundDocumentId** invalid.",
				content = @Content())
		}
	)
	@PutMapping(value = "/compounddocuments/{id}/druckSpeichern",
		produces = {"application/json"})
	ResponseEntity<List<BestandsrechtListDTO>> updateDrucksache(
		@Parameter(in = ParameterIn.PATH, description = "UUID of the compound document", required = true,
			schema = @Schema())
		@PathVariable("id") UUID compoundDocumentId);


	/**
	 * GET /druck/{compoundDocumentId}: Gibt die gespeicherte Drucksache zu einer Dokumentenmappe zurück.
	 *
	 * @param compoundDocumentId – UUID of the compound document (required)
	 * @return successful operation (status code 200) or Access token is missing or invalid (status code 401) or User has invalid rights (status code 403) or
	 * druck not found (status code 404)
	 */
	@Operation(
		operationId = "getDruck",
		summary = "Gibt die gespeicherte Drucksache zu einer Dokumentenmappe zurück.",
		description = "Gibt die gespeicherte Drucksache zu einer Dokumentenmappe zurück.",
		tags = {API_DIV_PDF},
		responses = {
			@ApiResponse(responseCode = "200", description = "successful operation",
				content = @Content(mediaType = "application/json",
					schema = @Schema(implementation = Resource.class))),
			@ApiResponse(responseCode = "401", description = "Access token is missing or invalid",
				content = @Content()),
			@ApiResponse(responseCode = "403", description = "User has invalid rights", content = @Content()),
			@ApiResponse(responseCode = "404", description = "Drucksache not found", content = @Content())
		}
	)
	@GetMapping(
		value = "/druck/{compoundDocumentId}",
		produces = {"*/*"}
	)
	ResponseEntity<byte[]> getDrucksache(
		@Parameter(name = "compoundDocumentId", required = true, schema = @Schema())
		@PathVariable("compoundDocumentId") UUID compoundDocumentId
	);


	/**
	 * GET /plattform/user/{gid}/regelungsvorhaben/{rvid}/kabinett/dokumente: Gibt die Dokumente, die Bereit für das Kabinettverfahren sind, zurück
	 *
	 * @param gid  ID of the user (required)
	 * @param rvid ID of the Regelungsvorhaben (required)
	 * @return OK (status code 200) or Keine Dokumentenmappen Bereit für Kabinettverfahren (status code 204) or Authentication information is missing or invalid
	 * (status code 401) or Regelungsvorhaben nicht gefunden (status code 404)
	 */
	@Operation(
		operationId = "getCompoundDocumentForKabinett",
		summary = "Gibt die Dokumente, die Bereit für das Kabinettverfahren sind, zurück",
		tags = {ApiConstants.API_DIV_KABINETTVERFAHREN},
		responses = {
			@ApiResponse(responseCode = "200", description = "OK",
				content = @Content(mediaType = "application/json",
					array = @ArraySchema(
						schema = @Schema(implementation = PkpZuleitungDokumentDTO.class))
				)
			),
			@ApiResponse(responseCode = "204",
				description = "Keine Dokumentenmappen Bereit für Kabinettverfahren",
				content = @Content(schema = @Schema(hidden = true))),
			@ApiResponse(responseCode = "401",
				description = "Authentication information is missing or invalid",
				content = @Content(schema = @Schema(hidden = true))),
			@ApiResponse(responseCode = "404",
				description = "Regelungsvorhaben nicht gefunden",
				content = @Content(schema = @Schema(hidden = true)))
		}
	)
	@GetMapping(
		value = "/plattform/user/{gid}/regelungsvorhaben/{rvid}/kabinett/dokumente",
		produces = {MediaType.APPLICATION_JSON_VALUE}
	)
	ResponseEntity<List<PkpZuleitungDokumentDTO>> getCompoundDocumentForKabinett(
		@Parameter(name = "gid", description = "ID of the user", required = true) @PathVariable("gid") String gid,
		@Parameter(name = "rvid", description = "ID of the 'Regelungsvorhaben'", required = true)
		@PathVariable("rvid") UUID rvid
	);

}
