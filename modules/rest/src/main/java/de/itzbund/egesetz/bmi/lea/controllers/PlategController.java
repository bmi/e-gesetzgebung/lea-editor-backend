// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.controllers;

import de.itzbund.egesetz.bmi.lea.api.DrucksachenPlategApi;
import de.itzbund.egesetz.bmi.lea.api.PlategApi;
import de.itzbund.egesetz.bmi.lea.api.RegelungsvorhabenPlategApi;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.PkpZuleitungDokumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.egfa.datenuebernahme.EgfaDatenuebernahmeParentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.status.UpdateStatusDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.AbstimmungId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CompoundDocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.DocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.EgfaRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.PropositionRestPort;
import de.itzbund.egesetz.bmi.lea.exception.EgfaException;
import de.itzbund.egesetz.bmi.lea.exception.RestApiErrorDto;
import de.itzbund.egesetz.bmi.lea.mappers.CompoundDocumentMapperRest;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import static de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState.BEGRUENDUNG_NOT_FOUND;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState.COMPOUND_DOCUMENT_NOT_FOUND;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState.INVALID_ARGUMENTS;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState.INVALID_STATE;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState.NOT_IMPLEMENTED;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState.NOT_MODIFIED;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState.NO_PERMISSION;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState.REGELUNGSVORHABEN_NOT_FOUND;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState.VORBLATT_NOT_FOUND;

@RestController
@RequiredArgsConstructor
@Log4j2
@SuppressWarnings("unused")
public class PlategController extends RegelungsvorhabenPlategController implements PlategApi, RegelungsvorhabenPlategApi, DrucksachenPlategApi {

    private static final String DEBUG_MSG = "Returned list: {}";

    @Autowired
    private DocumentRestPort documentService;
    @Autowired
    private PropositionRestPort propositionService;
    @Autowired
    private CompoundDocumentRestPort compoundDocumentService;
    @Autowired
    private CompoundDocumentMapperRest compoundDocumentMapperRest;
    @Autowired
    private EgfaRestPort egfaRestPort;

    /**
     * Sets the state of a document to freeze. This called by platform
     *
     * @param gid       User's id
     * @param docId     Id of the compound document
     * @param statusDTO JSON object with field 'status' and value of the status to set
     * @return OK or error
     */
    @Override
    public ResponseEntity<String> veraendereDokumentenmappenStatusDurchPlattform(String gid, String docId,
        UpdateStatusDTO statusDTO) {
        ServiceState serviceState = ServiceState.INVALID_STATE;

        // Check, values are set
        if (!((gid == null) || (docId == null) || (statusDTO == null))) {
            DocumentState state = statusDTO.getState();
            if (state != null) {
                serviceState = compoundDocumentService.changeCompoundDocumentStateByPlatform(gid,
                    new CompoundDocumentId(UUID.fromString(docId)), state);
            }
        }

        return getResult(serviceState);
    }

    private ResponseEntity<String> getResult(ServiceState serviceState) {
        switch (serviceState) {
            case OK:
                return ResponseEntity.noContent()
                    .build(); // 204
            case COMPOUND_DOCUMENT_NOT_FOUND:
            case DOCUMENT_NOT_FOUND:
                return ResponseEntity.notFound()
                    .build(); // 404
            case ONLY_ONE_KABINETT_VOTING:
                return new ResponseEntity<>("Only one kabinett voting allowed", HttpStatus.CONFLICT); // 409
            case INVALID_STATE:
                return new ResponseEntity<>("State transition invalid for platform", HttpStatus.CONFLICT); // 409
            case NO_PERMISSION:
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                    .build(); // 401
            default:
                return ResponseEntity.badRequest()
                    .build(); // 400
        }
    }

    /**
     * Retrieves all documents which are in the compound document
     *
     * @param compoundDocumentId UUID of
     * @return Documents from given compound document
     */
    @Override
    public ResponseEntity<List<CompoundDocumentSummaryDTO>> getAllDocumentsFromCompoundDocument(String gid,
        List<String> compoundDocumentId) {
        List<ServiceState> status = new ArrayList<>();
        List<CompoundDocument> compoundDocuments = compoundDocumentService.getAllDocumentsFromCompoundDocuments(gid,
            compoundDocumentId, status);

        switch (status.get(0)) {
            case OK:
                List<CompoundDocumentSummaryDTO> documentSummaryDTOs = compoundDocumentMapperRest.mapCompoundDocument(
                    compoundDocuments);
                return ResponseEntity.ok(documentSummaryDTOs);
            case COMPOUND_DOCUMENT_NOT_FOUND:
                return ResponseEntity.notFound()
                    .build();
            default:
                return ResponseEntity.unprocessableEntity()
                    .build();
        }
    }

    /**
     * GET /plattform/user/{gid}/regelungsvorhaben/{rvid}/kabinett/dokumente : Gibt die Dokumente, die Bereit für das Kabinettverfahren sind, zurück
     *
     * @param gid                 ID of the user (required)
     * @param regelungsvorhabenId ID of the Regelungsvorhaben (required)
     * @return OK (status code 200) or Keine Dokumentenmappen Bereit für Kabinettverfahren (status code 204) or Authentication information is missing or invalid
     * (status code 401) or Regelungsvorhaben nicht gefunden (status code 404)
     */
    @Override
    public ResponseEntity<List<PkpZuleitungDokumentDTO>> getCompoundDocumentForKabinett(String gid,
        UUID regelungsvorhabenId) {
        log.debug("Get compound documents for regelungsvorhabenId {}", regelungsvorhabenId);
        List<ServiceState> status = new ArrayList<>();

        List<PkpZuleitungDokumentDTO> pkpZuleitungDokumentDTOs =
            compoundDocumentService.getCompoundDocumentsForKabinett(
                gid, new RegelungsVorhabenId(regelungsvorhabenId), status);

        ServiceState state = status.get(0);
        log.debug("Got state {} from Service", state); //NOSONAR

        switch (state) {
            case COMPOUND_DOCUMENT_NOT_FOUND: // 204
                return ResponseEntity.noContent()
                    .build();
            case REGELUNGSVORHABEN_NOT_FOUND: // 404
                return ResponseEntity.notFound()
                    .build();
            case OK: // 200
                log.debug(DEBUG_MSG, pkpZuleitungDokumentDTOs);
                return ResponseEntity.ok(pkpZuleitungDokumentDTOs);
            default: // 500
                log.error("An error occurred.");
                return ResponseEntity.internalServerError()
                    .build();
        }
    }

    /**
     * Liefert eine Liste von Dokumentenmappen für ein gegebenes Regelungsvorhaben OHNE Dokumente
     *
     * @param rvid Id of a 'Regelungsvorhaben'
     * @return List of compound documents
     */
    @Override
    public ResponseEntity<List<CompoundDocumentSummaryDTO>> getCompoundDocumentForRvId(String gid, String rvid) {
        log.debug("Get compound documents for rvid {}.", rvid);
        List<ServiceState> status = new ArrayList<>();
        List<CompoundDocumentSummaryDTO> allCompoundDocumentsForRegelungsvorhaben =
            compoundDocumentService.getCompoundDocumentsForRegelungsvorhaben(gid, UUID.fromString(rvid), status);

        // Additional filter while developing
        List<CompoundDocumentSummaryDTO> compoundDocumentsForRegelungsvorhaben = allCompoundDocumentsForRegelungsvorhaben.stream()
            .filter(cd -> !cd.getState().equals(DocumentState.BESTANDSRECHT))
            .collect(Collectors.toList());

        ServiceState state = status.get(0);
        log.debug("Got state {} from Service", state); //NOSONAR

        if (state == ServiceState.OK) {
            log.debug(DEBUG_MSG, compoundDocumentsForRegelungsvorhaben); //NOSONAR
            return ResponseEntity.ok(compoundDocumentsForRegelungsvorhaben);
        }

        log.error("An error occurred,so I gave an empty list back.");
        return ResponseEntity.ok(Collections.emptyList());
    }

    /**
     * Gibt die Dokumentenmappen des Benutzers zurück, die als Antwort für eine laufende Abstimmung geeignet sind
     */
    @Override
    public ResponseEntity<List<CompoundDocumentSummaryDTO>> getDokumentenmappenFuerAbstimmung(String gid,
        UUID regelungsvorhabenId, UUID compoundDocumentId, UUID abstimmungId) {
        log.debug("Get compound documents for regelungsvorhabenId {}, compoundDocumentId {} and abstimmungId {}.",
            regelungsvorhabenId, compoundDocumentId, abstimmungId);
        List<ServiceState> status = new ArrayList<>();

        List<CompoundDocumentSummaryDTO> allCompoundDocumentSummaryDTOList =
            compoundDocumentService.getCompoundDocumentsForAbstimmung(
                gid, new RegelungsVorhabenId(regelungsvorhabenId),
                new CompoundDocumentId(compoundDocumentId), new AbstimmungId(abstimmungId), status);

        // Additional filter while developing
        List<CompoundDocumentSummaryDTO> compoundDocumentsForRegelungsvorhaben = allCompoundDocumentSummaryDTOList.stream()
            .filter(cd -> !cd.getState().equals(DocumentState.BESTANDSRECHT))
            .collect(Collectors.toList());

        ServiceState state = status.get(0);
        log.debug("Got state {} from Service", state); //NOSONAR

        switch (state) {
            case COMPOUND_DOCUMENT_NOT_FOUND:
                return ResponseEntity.noContent()
                    .build();
            case OK:
                log.debug(DEBUG_MSG, compoundDocumentsForRegelungsvorhaben); //NOSONAR
                return ResponseEntity.ok(compoundDocumentsForRegelungsvorhaben);
            default:
                log.error("An error occurred.");
                return ResponseEntity.unprocessableEntity()
                    .build();
        }

    }

    @Override
    public ResponseEntity<Void> egfaDatenuebernahme(String gid, UUID regelungsvorhabenId,
        EgfaDatenuebernahmeDTO egfaDatenuebernahmeDTO) {
        List<ServiceState> status = new ArrayList<>();
        egfaRestPort.egfaDatenuebernahme(gid, regelungsvorhabenId, egfaDatenuebernahmeDTO, status);

        ServiceState state = status.get(0);
        log.debug("Got state {} from Service", state); //NOSONAR
        Pair<RestApiErrorDto, HttpStatus> errorPair;
        switch (state) {
            case OK: // 200
                return ResponseEntity.ok()
                    .build();
            case NO_PERMISSION: // 403
                errorPair = createEgfaError(NO_PERMISSION);
                break;
            case NOT_IMPLEMENTED:
                errorPair = createEgfaError(NOT_IMPLEMENTED);
                break;
            case INVALID_ARGUMENTS:
                errorPair = createEgfaError(INVALID_ARGUMENTS);
                break;
            case COMPOUND_DOCUMENT_NOT_FOUND:
                errorPair = createEgfaError(COMPOUND_DOCUMENT_NOT_FOUND);
                break;
            case BEGRUENDUNG_NOT_FOUND:
                errorPair = createEgfaError(BEGRUENDUNG_NOT_FOUND);
                break;
            case VORBLATT_NOT_FOUND:
                errorPair = createEgfaError(VORBLATT_NOT_FOUND);
                break;
            case REGELUNGSVORHABEN_NOT_FOUND:
                errorPair = createEgfaError(REGELUNGSVORHABEN_NOT_FOUND);
                break;
            case INVALID_STATE:
                errorPair = createEgfaError(INVALID_STATE);
                break;
            default: // 500
                log.warn("Unknown error/state while processing EGFA for regelungsvorhaben {} and gid {}",
                    regelungsvorhabenId, gid);
                errorPair = createEgfaError(INVALID_STATE);
        }
        throw new EgfaException(errorPair.getKey(), errorPair.getValue());
    }

    @Override
    public ResponseEntity<Void> egfaDatenSpeicherung(String gid, UUID regelungsvorhabenId,
        EgfaDatenuebernahmeParentDTO egfaDatenuebernahmeParentDTO) {

        List<ServiceState> statusSpeicherung = new ArrayList<>();

        egfaRestPort.egfaDatenSpeicherung(gid, regelungsvorhabenId, egfaDatenuebernahmeParentDTO, statusSpeicherung);

        if (statusSpeicherung.isEmpty()) {
            statusSpeicherung.add(INVALID_STATE);
        }
        ServiceState state = statusSpeicherung.get(0);
        log.debug("Got state {} from Service", state); //NOSONAR
        Pair<RestApiErrorDto, HttpStatus> errorPair;
        switch (state) {
            case OK: // 200
                return ResponseEntity.ok()
                    .build();
            case NOT_MODIFIED: //304
                errorPair = createEgfaError(NOT_MODIFIED);
                break;
            case INVALID_ARGUMENTS: //400
                errorPair = createEgfaError(INVALID_ARGUMENTS);
                break;
            case NOT_IMPLEMENTED:
                errorPair = createEgfaError(NOT_IMPLEMENTED);
                break;
            default: // 500
                log.warn("No return status given while processing EGFA for regelungsvorhaben {} and gid {}",
                    regelungsvorhabenId, gid);
                errorPair = createEgfaError(INVALID_STATE);
        }
        throw new EgfaException(errorPair.getKey(), errorPair.getValue());
    }

    protected Pair<RestApiErrorDto, HttpStatus> createEgfaError(ServiceState state) {
        Map<ServiceState, Pair<HttpStatus, String>> egfaErrorStateMap = new EnumMap<>(ServiceState.class);

        egfaErrorStateMap.put(NO_PERMISSION, Pair.of(HttpStatus.FORBIDDEN, "NO_PERMISSION"));
        egfaErrorStateMap.put(NOT_IMPLEMENTED, Pair.of(HttpStatus.NOT_IMPLEMENTED, "EGFA_MODULE_NOT_IMPLEMENTED"));
        egfaErrorStateMap.put(INVALID_ARGUMENTS, Pair.of(HttpStatus.BAD_REQUEST, "INVALID_ARGUMENTS"));
        egfaErrorStateMap.put(COMPOUND_DOCUMENT_NOT_FOUND,
            Pair.of(HttpStatus.NOT_FOUND, "COMPOUND_DOCUMENT_NOT_FOUND"));
        egfaErrorStateMap.put(BEGRUENDUNG_NOT_FOUND, Pair.of(HttpStatus.NOT_FOUND, "BEGRUENDUNG_NOT_FOUND"));
        egfaErrorStateMap.put(VORBLATT_NOT_FOUND, Pair.of(HttpStatus.NOT_FOUND, "VORBLATT_NOT_FOUND"));
        egfaErrorStateMap.put(REGELUNGSVORHABEN_NOT_FOUND,
            Pair.of(HttpStatus.NOT_FOUND, "REGELUNGSVORHABEN_NOT_FOUND"));
        egfaErrorStateMap.put(INVALID_STATE, Pair.of(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR_WHILE_PROCESSING"));
        egfaErrorStateMap.put(NOT_MODIFIED, Pair.of(HttpStatus.NOT_MODIFIED, "NOT_MODIFIED"));

        var error = egfaErrorStateMap.get(state);
        var restError = RestApiErrorDto.builder()
            .messageCode(error.getValue())
            .build();
        return Pair.of(restError, error.getKey());

    }

    @Override
    public ResponseEntity<Void> bestandsrechtDatenSpeicherung(String gid, UUID regelungsvorhabenId,
        BestandsrechtDTO bestandsrechtDTO) {

        List<ServiceState> status = new ArrayList<>();

        propositionService.bestandsrechtDatenSpeicherung(gid, regelungsvorhabenId, bestandsrechtDTO, status);

        if (status.isEmpty()) {
            status.add(INVALID_STATE);
        }
        ServiceState state = status.get(0);
        log.debug("Got state {} from Service", state); //NOSONAR
        Pair<RestApiErrorDto, HttpStatus> errorPair;
        switch (state) {
            case OK: // 200
                return ResponseEntity.ok()
                    .build();
            case INVALID_ARGUMENTS: //400
                return ResponseEntity.badRequest().build();
            case DOCUMENT_NOT_VALID: //422
                return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
            case NO_PERMISSION:
                return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .build();
            default: // 500
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .build();
        }
    }

}
