// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.time.Instant;
import java.time.temporal.TemporalAccessor;

import static de.itzbund.egesetz.bmi.lea.core.Constants.DATE_TIME_FORMATTER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.NS_LEGALDOCML;
import static de.itzbund.egesetz.bmi.lea.core.Constants.PATT_AKN_NS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.PATT_SCHEMA_LOC;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Testsuite for {@link Constants}.
 */
class ConstantsTest {

    private static final String SCHEMA_LOC_REGTEXT =
        "http://docs.oasis-open.org/legaldocml/ns/akn/3.0 "
            + "../../../02_schema/legalDocML.de/legalDocML.de-regelungstextentwurfsfassung.xsd";
    private static final String SCHEMA_LOC_VORBLATT =
        "http://docs.oasis-open.org/legaldocml/ns/akn/3.0 "
            + "../../../02_schema/legalDocML.de/legalDocML.de-vorblatt.xsd";
    private static final String SCHEMA_LOC_EXAMPLE_UN =
        "http://docs.oasis-open.org/legaldocml/ns/akn/3.0 ../../../main/resources/schemas/akomantoso30.xsd";


    /**
     * Tests the private constructor.
     */
    @Test
    void testPrivateConstructor() {
        try {
            Constructor<Constants> constructor = Constants.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            InvocationTargetException exc = assertThrows(InvocationTargetException.class, constructor::newInstance);
            assertNotNull(exc.getTargetException());
            Throwable cause = exc.getTargetException();
            assertNotNull(cause.getMessage());
            assertEquals("Utility class", cause.getMessage());
        } catch (NoSuchMethodException e) {
            fail(e);
        }
    }


    /**
     * Tests the regex patterns.
     */
    @Test
    void testPatterns() {
        assertNotNull(PATT_AKN_NS);
        assertTrue(PATT_AKN_NS.matcher(String.format("xmlns=\"%s\"", NS_LEGALDOCML)).matches());
        assertTrue(PATT_AKN_NS.matcher(String.format("xmlns:akn=\"%s\"", NS_LEGALDOCML)).matches());

        assertNotNull(PATT_SCHEMA_LOC);
        assertTrue(PATT_SCHEMA_LOC.matcher(String.format("schemaLocation=\"%s\"", SCHEMA_LOC_REGTEXT)).matches());
        assertTrue(PATT_SCHEMA_LOC.matcher(String.format("schemaLocation=\"%s\"", SCHEMA_LOC_VORBLATT)).matches());
        assertTrue(PATT_SCHEMA_LOC.matcher(String.format("schemaLocation=\"%s\"", SCHEMA_LOC_EXAMPLE_UN)).matches());
    }


    @Test
    void testDateTimeFormatter() {
        String timeStr = "2021-11-12T10:00:13.456";
        TemporalAccessor time = DATE_TIME_FORMATTER.parse(timeStr);
        assertEquals(timeStr, DATE_TIME_FORMATTER.format(time));
        Instant now = Instant.now();
        assertNotNull(DATE_TIME_FORMATTER.format(now));
    }

}
