// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.json;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.stream.Collectors;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_EID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_GUID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_NAME;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_AKOMANTOSO;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DIV;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_P;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SECTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TBLOCK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_ID_GUID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_TYPE;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDocumentObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getEffectiveTextValue;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getLocalName;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getStringAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewTextNode;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Log4j2
@SuppressWarnings("unchecked")
class TraversableJSONDocumentTest {

    private static JSONObject testDocument;
    private static JSONObject object5;
    private static JSONObject object8;
    private static JSONObject object9;
    private static JSONObject object20;


    /**
     * Test document:</br>
     * <p><img src="doc-files/sample-tree.png"/></p>
     */
    @BeforeAll
    static void setUp() {
        Map<Integer, JSONObject> container = new HashMap<>();
        List<Integer> leaves = Arrays.asList(3, 4, 7, 11, 12, 13, 15, 16, 18, 19, 20);

        for (int i = 1; i <= 20; i++) {
            container.put(i, makeObject(
                Pair.of(JSON_KEY_TYPE, leaves.contains(i) ? ELEM_P : ELEM_SECTION),
                Pair.of(ATTR_NAME, String.valueOf(i)),
                Pair.of(JSON_KEY_ID_GUID, UUID.randomUUID()
                    .toString())
            ));
        }

        addChildren(container.get(2), container.get(3), container.get(4));
        addChildren(container.get(6), container.get(7));
        addChildren(container.get(10), container.get(11), container.get(12));
        addChildren(container.get(14), container.get(15), container.get(16));
        addChildren(container.get(9), container.get(10));
        addChildren(container.get(8), container.get(9), container.get(13), container.get(14));
        addChildren(container.get(5), container.get(6), container.get(8));
        addChildren(container.get(17), container.get(18), container.get(19));
        addChildren(container.get(1), container.get(2), container.get(5), container.get(17), container.get(20));

        testDocument = container.get(1);
        object5 = container.get(5);
        object8 = container.get(8);
        object9 = container.get(9);
        object20 = container.get(20);

        assertNotNull(testDocument);
        assertEquals(4, getChildren(testDocument).size());
    }

    private static JSONObject makeObject(Pair<String, String>... keyValuePairs) {
        JSONObject jsonObject = new JSONObject();
        Arrays.stream(keyValuePairs)
            .forEach(p -> jsonObject.put(p.getKey(), p.getValue()));
        return jsonObject;
    }

    private static TraversableJSONDocument getSimpleDocument(String rootType, String... texts) {
        JSONObject root = makeObject(
            Pair.of(JSON_KEY_TYPE, rootType),
            Pair.of(ATTR_GUID, UUID.randomUUID()
                .toString()));

        if (texts.length == 0) {
            addChildren(root);
        }

        Arrays.stream(texts)
            .forEach(text -> {
                JSONObject p = makeObject(
                    Pair.of(JSON_KEY_TYPE, ELEM_P),
                    Pair.of(ATTR_GUID, UUID.randomUUID()
                        .toString()));
                addChildren(p, makeNewTextNode(text));
                addChildren(root, p);
            });

        assertEquals(String.join(" ", texts), getEffectiveTextValue(root));

        TraversableJSONDocument document = new TraversableJSONDocument(root);
        assertNotNull(document);
        return document;
    }

    @Test
    void testCreate() {
        assertThrows(NullPointerException.class, () -> new TraversableJSONDocument(null));

        TraversableJSONDocument document = new TraversableJSONDocument(testDocument);
        assertNotNull(document);

        List<JSONObject> divs = document.getElementsOfType(ELEM_DIV);
        assertNotNull(divs);
        assertTrue(divs.isEmpty());

        List<JSONObject> ps = document.getElementsOfType(ELEM_P);
        assertNotNull(ps);
        assertEquals(document.getAllLeaveObjects(), ps);
    }

    @Test
    void testWithRoot() {
        TraversableJSONDocument document = new TraversableJSONDocument(testDocument);
        JSONObject root = document.getDocumentObject();

        assertEquals(3, document.getAttributes(root)
            .size());
        String guid = document.getAttribute(root, JSON_KEY_ID_GUID);
        assertEquals(getStringAttribute(root, JSON_KEY_ID_GUID), guid);
        assertEquals(root, document.getObject(guid));

        assertNull(document.getParent(root));
        assertEquals(List.of(), document.getAncestors(root, false));
        assertEquals(List.of(root), document.getAncestors(root, true));

        List<JSONObject> children = document.getChildren(root);
        assertEquals(4, children.size());
        JSONObject child4 = document.getChild(root, 3);
        assertNotNull(child4);
        assertEquals("20", getStringAttribute(child4, ATTR_NAME));

        List<JSONObject> descendants = document.getDescendants(root, false);
        assertNotNull(descendants);
        assertEquals(19, descendants.size());
        assertEquals(Utils.range(2, 21), getNodeNums(descendants));
        descendants = document.getDescendants(root, true);
        assertEquals(20, descendants.size());
        assertEquals(root, descendants.get(0));

        List<JSONObject> followingObjects = document.getFollowingObjects(root);
        assertNotNull(followingObjects);
        assertEquals(List.of(), followingObjects);

        List<JSONObject> followingSiblings = document.getFollowingSiblings(root);
        assertNotNull(followingSiblings);
        assertEquals(List.of(), followingSiblings);

        List<JSONObject> precedingObjects = document.getPrecedingObjects(root);
        assertNotNull(precedingObjects);
        assertEquals(List.of(), precedingObjects);

        List<JSONObject> precedingSiblings = document.getPrecedingSiblings(root);
        assertNotNull(precedingSiblings);
        assertEquals(List.of(), precedingSiblings);

        assertFalse(document.isTopLevelObject());
        assertEquals("1", document.getAttribute(document.getRoot(), ATTR_NAME));
    }

    @Test
    void testWithObject9() {
        TraversableJSONDocument document = new TraversableJSONDocument(testDocument);
        document.setCurrentObject(object9);
        assertEquals(object9, document.getCurrentObject());
        assertEquals("8", getStringAttribute(document.getParent(), ATTR_NAME));
        assertEquals(List.of(8, 5, 1), getNodeNums(document.getAncestors(false)));
        assertEquals(List.of(9, 8, 5, 1), getNodeNums(document.getAncestors(true)));
        assertEquals(List.of(10), getNodeNums(document.getChildren()));
        assertEquals(List.of(9, 10, 11, 12), getNodeNums(document.getDescendants(true)));
        assertEquals(List.of(10, 11, 12), getNodeNums(document.getDescendants(false)));
        assertEquals(List.of(), getNodeNums(document.getPrecedingSiblings()));
        assertEquals(List.of(), getNodeNums(document.getPrecedingObjects()));
        assertEquals(List.of(13, 14), getNodeNums(document.getFollowingSiblings()));
        assertEquals(List.of(13, 14, 15, 16), getNodeNums(document.getFollowingObjects()));
        assertFalse(document.isTopLevelObject());
    }

    @Test
    void testWithObject5() {
        TraversableJSONDocument document = new TraversableJSONDocument(testDocument);
        document.setCurrentObject(object5);
        assertEquals(List.of(2), getNodeNums(document.getPrecedingSiblings()));
        assertEquals(List.of(4, 3, 2), getNodeNums(document.getPrecedingObjects()));
        assertEquals(List.of(17, 20), getNodeNums(document.getFollowingSiblings()));
        assertEquals(List.of(17, 18, 19, 20), getNodeNums(document.getFollowingObjects()));
        assertFalse(document.isTopLevelObject());
    }

    @Test
    void testLeaveObjects() {
        TraversableJSONDocument document = new TraversableJSONDocument(testDocument);
        assertFalse(document.isLeaveObject());
        assertEquals(List.of(3, 4, 7, 11, 12, 13, 15, 16, 18, 19, 20),
            getNodeNums(document.getAllLeaveObjects()));
        assertEquals(List.of(3, 4), getNodeNums(document.getLeaveObjects()));
        assertEquals(List.of(11, 12), getNodeNums(document.getLeaveObjects(object9)));
        assertFalse(document.isLeaveObject(object9));
        assertEquals(List.of(11, 12, 13, 15, 16), getNodeNums(document.getLeaveObjects(object8)));
        assertFalse(document.isLeaveObject(object8));
        assertEquals(List.of(), document.getLeaveObjects(object20));
        assertTrue(document.isLeaveObject(object20));

        assertFalse(document.isTopLevelObject(object8));
        assertFalse(document.isTopLevelObject(object20));
    }

    @Test
    void test_withBegruendung() {
        String jsonString = Utils.getContentOfTextResource(
            "/compare/20230511_1359_Export_Begruendung-Stammgesetz_BG-SF.json");
        JSONObject jsonObject = getDocumentObject(jsonString);
        assertNotNull(jsonObject);
        TraversableJSONDocument document = new TraversableJSONDocument(jsonObject);
        assertNotNull(document);
        assertFalse(document.isTopLevelObject());
        assertTrue(document.isTopLevelObject(document.getParent()));
        assertEquals(ELEM_AKOMANTOSO, getLocalName(getType(document.getRoot())));
        assertEquals(74, document.getSize());
        JSONObject firstNode = document.getCurrentObject();

        log.debug("iterating over document ...");
        iterateDocumentAll(document);
        document.reset();
        assertEquals(74, document.getSize());
        assertEquals(firstNode, document.getCurrentObject());

        log.debug("iterating leaves ...");
        iterateDocumentLeaves(document);
        document.reset();
        assertEquals(74, document.getSize());
        assertEquals(firstNode, document.getCurrentObject());
    }

    @Test
    void testUpdate() {
        TraversableJSONDocument document = getSimpleDocument(ELEM_TBLOCK, "P1", "P2");
        assertEquals(3, document.getSize());

        JSONObject substitute = makeObject(
            Pair.of(JSON_KEY_TYPE, ELEM_P),
            Pair.of(ATTR_GUID, UUID.randomUUID()
                .toString()));
        addChildren(substitute, makeNewTextNode("X"));
        log.debug("[update] :: " + getEffectiveTextValue(document.getDocumentObject()));
        document.replaceBy(document.getCurrentObject(), substitute);
        log.debug("[update] :: " + getEffectiveTextValue(document.getDocumentObject()));
        assertEquals(3, document.getSize());
        assertEquals("X P2", getEffectiveTextValue(document.getDocumentObject()));

        substitute = makeObject(
            Pair.of(JSON_KEY_TYPE, ELEM_P),
            Pair.of(ATTR_GUID, UUID.randomUUID()
                .toString()));
        addChildren(substitute, makeNewTextNode("Y"));
        document.replaceBy(document.getCurrentObject(), substitute);
        log.debug("[update] :: " + getEffectiveTextValue(document.getDocumentObject()));
        assertEquals(3, document.getSize());
        assertEquals("Y P2", getEffectiveTextValue(document.getDocumentObject()));

        substitute = makeObject(
            Pair.of(JSON_KEY_TYPE, ELEM_P),
            Pair.of(ATTR_GUID, UUID.randomUUID()
                .toString()));
        addChildren(substitute, makeNewTextNode("Z"));
        document.replaceBy(document.getFollowingObjects()
            .get(0), substitute);
        log.debug("[update] :: " + getEffectiveTextValue(document.getDocumentObject()));
        assertEquals(3, document.getSize());
        assertEquals("Y Z", getEffectiveTextValue(document.getDocumentObject()));
    }

    @Test
    void testInsert() {
        TraversableJSONDocument document = new TraversableJSONDocument(getInsertTestObject());
        assertEquals(3, document.getSize());

        JSONObject newElement = makeObject(
            Pair.of(JSON_KEY_TYPE, ELEM_P),
            Pair.of(ATTR_GUID, UUID.randomUUID()
                .toString()));
        addChildren(newElement, makeNewTextNode("P1"));
        log.debug("[insert] :: " + getEffectiveTextValue(document.getDocumentObject()));
        document.insertNewChild(document.getCurrentObject(), newElement, 0);
        log.debug("[insert] :: " + getEffectiveTextValue(document.getDocumentObject()));
        assertEquals(4, document.getSize());
        assertEquals("P1 P", getEffectiveTextValue(document.getDocumentObject()));

        newElement = makeObject(
            Pair.of(JSON_KEY_TYPE, ELEM_P),
            Pair.of(ATTR_GUID, UUID.randomUUID()
                .toString()));
        addChildren(newElement, makeNewTextNode("P2"));
        document.insertNewChild(document.getCurrentObject(), newElement, 5);
        log.debug("[insert] :: " + getEffectiveTextValue(document.getDocumentObject()));
        assertEquals(5, document.getSize());
        assertEquals("P1 P P2", getEffectiveTextValue(document.getDocumentObject()));
    }

    private void iterateDocumentAll(TraversableJSONDocument document) {
        JSONObject current = document.getCurrentObject();
        int count = 0;
        int leaves = 0;
        String empty = "      ";
        String leaveCount;
        while (current != null) {
            count++;
            if (document.isLeaveObject()) {
                leaves++;
                leaveCount = String.format(" *%03d*", leaves);
            } else {
                leaveCount = empty;
            }

            System.out.printf("(%03d%s) %s [%s] -- %s :: %s%n",
                count,
                leaveCount,
                getType(current),
                getStringAttribute(current, JSON_KEY_ID_GUID),
                getStringAttribute(current, ATTR_EID),
                Utils.reduceString(getEffectiveTextValue(current), 75));
            if (document.hasNext()) {
                current = document.next();
            } else {
                current = null;
            }
        }
    }

    private void iterateDocumentLeaves(TraversableJSONDocument document) {
        int count = 0;
        try {
            document.advanceToNextLeave();
            do {
                count++;
                System.out.println(getStringAttribute(document.getCurrentObject(), JSON_KEY_ID_GUID));
                document.advanceToNextLeave();
            } while (document.isLeaveObject());
        } catch (NoSuchElementException e) {
            // found end of hierarchy
        }
        assertEquals(45, count);
    }

    private List<Integer> getNodeNums(List<JSONObject> jsonObjects) {
        if (jsonObjects == null || jsonObjects.isEmpty()) {
            return List.of();
        } else {
            return jsonObjects.stream()
                .map(o -> Integer.valueOf(getStringAttribute(o, ATTR_NAME)))
                .collect(Collectors.toList());
        }
    }

    private JSONObject getInsertTestObject() {
        JSONObject root = makeObject(
            Pair.of(JSON_KEY_TYPE, ELEM_TBLOCK),
            Pair.of(ATTR_GUID, UUID.randomUUID()
                .toString())
        );

        JSONObject child = makeObject(
            Pair.of(JSON_KEY_TYPE, ELEM_TBLOCK),
            Pair.of(ATTR_GUID, UUID.randomUUID()
                .toString())
        );

        JSONObject leave = makeObject(
            Pair.of(JSON_KEY_TYPE, ELEM_P),
            Pair.of(ATTR_GUID, UUID.randomUUID()
                .toString())
        );

        addChildren(leave, makeNewTextNode("P"));
        addChildren(child, leave);
        addChildren(root, child);

        return root;
    }


    @Test
    void testHierarchyProcessing() {
        String jsonString = Utils.getContentOfTextResource("/json/regelungstext-mit-gliederung.json");
        JSONObject document = getDocumentObject(jsonString);
        assertNotNull(document);
        TraversableJSONDocument traversableJSONDocument = new TraversableJSONDocument(document);
        assertNotNull(traversableJSONDocument);

        List<Pair<String, List<String>>> expectedLevelsPerNorm = List.of(
            Pair.of("c876bdb9-2252-49ff-ae32-8794c6260739", List.of(
                "111f11dc-1049-4103-aefc-ddc86ea06f40",
                "2629ab06-a52f-4eb0-93c0-5ed5c4609c28")),
            Pair.of("8fb8f839-4762-4a7a-a179-61d0ca361509", List.of(
                "5f74116d-ccc0-4aff-bda6-0eec850bc2d2",
                "88941467-95a4-4050-b569-0653393c2094")),
            Pair.of("ba352408-fcfb-41cf-8c12-fc8b31770e74", List.of("c697e589-76c6-4d17-95e5-41b664aeeac1")),
            Pair.of("6798970b-0cd8-409b-8846-4cf4c2afbe14", List.of(
                "36856411-c6b7-409e-9268-3c8c5907d24a",
                "8bbdce2c-ad1a-4c28-abae-599f3897b77f",
                "2de7c5d9-7708-4ca0-95df-845ee4657fa0")),
            Pair.of("7a7a7d6a-3c8f-4f7a-ba5f-d6d0f26f2aa8", List.of("46a55d52-cf88-4fdc-866b-16a3f40fdae2")),
            Pair.of("a6c8406a-973f-495c-8fa7-ede600dc3f37", List.of(
                "5a3ded1e-4178-4975-85c2-5dcd5e64b652",
                "57990e82-b247-47d1-aab5-18b7b321b685",
                "7b6d6259-3785-4b59-acba-da8678099426")),
            Pair.of("c5f79c97-f422-45b6-b283-75c1e068b2c5", List.of("e104db1b-3c8b-465a-b7c5-8554a6acab10")),
            Pair.of("c9132fdb-1963-471f-9925-10251af1be27", List.of("f86202bd-b082-4a86-8e9a-baee92358564"))
        );

        expectedLevelsPerNorm.forEach(p -> {
            String normGUID = p.getKey();
            List<String> expectedGUIDs = p.getValue();
            assertEquals(expectedGUIDs, traversableJSONDocument.getUnprocessedLevels(UUID.fromString(normGUID))
                .stream()
                .map(JSONUtils::getGuid)
                .collect(Collectors.toList()));
        });
    }


    @AfterAll
    static void tearDown() {
        testDocument = null;
    }

}
