// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import lombok.SneakyThrows;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Testsuite for {@link HtmlComparisonReporter}.
 */
class HtmlComparisonReporterTest {

    private static final Class<?> THIS = HtmlComparisonReporterTest.class;

    private static final String TS_MARKER = "_TIMESTAMP_";


    @Test
    @SneakyThrows
    void testBasicStructureFixedFormat() {
        testBasicStructure(DocumentComparer.IntermediateFormat.FIXED_FORMAT);
    }


    @Test
    @SneakyThrows
    void testBasicStructureDSVSTyle() {
        testBasicStructure(DocumentComparer.IntermediateFormat.DSV_STYLE);
    }


    @SneakyThrows
    private void testBasicStructure(DocumentComparer.IntermediateFormat format) {
        String docId1 = "compare/min-doc-2.json";
        String docId2 = "compare/min-doc-2.xml";

        ComparisonDataSet cds = new ComparisonDataSet();
        cds.setFstDocumentId(docId1);
        cds.setSndDocumentId(docId2);

        DocumentComparer dc = new DocumentComparer();
        DocumentComparer.CompareResult compareResult = dc.compare(
            DocumentComparer.DocumentType.JSON_DOCUMENT,
            THIS.getResourceAsStream("/" + docId1),
            DocumentComparer.DocumentType.XML_DOCUMENT,
            THIS.getResourceAsStream("/" + docId2),
            format
        );

        cds.setCompareResult(compareResult);
        cds.setDiffs(dc.getDiffs());

        assertNotNull(cds.getFstDocumentId());
        assertFalse(cds.getFstDocumentId().isBlank());
        assertNotNull(cds.getSndDocumentId());
        assertFalse(cds.getSndDocumentId().isBlank());
        assertEquals(DocumentComparer.CompareResult.DIFFERENT, cds.getCompareResult());
        assertNotNull(cds.getDiffs());
        assertEquals(4, countInsertionsOrDeletions(cds.getDiffs()));

        StringWriter stringWriter = new StringWriter();
        HtmlComparisonReporter reporter = new HtmlComparisonReporter(stringWriter);
        reporter.report(cds);

        String actualHtml = stringWriter.toString();
        String expectedHtml = getExpectedHtml(reporter.getFormattedTimestamp());
        assertNotNull(actualHtml);
        assertNotNull(expectedHtml);

        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
            String.format("target/compare-report-%s.html", format.name().toLowerCase())
        ));
        bufferedWriter.write(actualHtml);
        bufferedWriter.close();
    }


    @Test
    void testEmptyDataset() {
        StringWriter stringWriter = new StringWriter();
        HtmlComparisonReporter reporter = new HtmlComparisonReporter(stringWriter);
        reporter.report();
        String actualHtml = stringWriter.toString();
        assertNotNull(actualHtml);
    }


    @SneakyThrows
    private String getExpectedHtml(String ts) {
        String html = Files.readString(Paths.get(
            Objects.requireNonNull(THIS.getResource("/compare/min-doc-2-report-expected.html")).toURI()));
        html = html.replace(TS_MARKER, ts);
        return html.replaceAll(">\\s+<", "><");
    }


    private int countInsertionsOrDeletions(List<DiffMatchPatch.Diff> diffs) {
        int count = 0;
        for (DiffMatchPatch.Diff diff : diffs) {
            if (diff.operation != DiffMatchPatch.Operation.EQUAL) {
                count++;
            }
        }
        return count;
    }

}
