// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests for {@link de.itzbund.egesetz.bmi.lea.core.VersionInfo}
 */
@Log4j2
class VersionInfoTest {

    private static final Pattern PATT_VERSION_INFO =
        Pattern.compile("([-.\\w]+) v(\\d+)\\.(\\d+)\\.(\\d+) built with Java (\\d+)");


    @Test
    void testVersionInfo() {
        String info = VersionInfo.getVersionInfo();
        Matcher m = PATT_VERSION_INFO.matcher(info);
        log.debug(info);
        assertTrue(m.matches());
        assertEquals(
            String.format("%s.%s.%s", m.group(2), m.group(3), m.group(4))
            , VersionInfo.getAppVersion());
        assertNotNull(VersionInfo.getAppVersion());
    }
    
}
