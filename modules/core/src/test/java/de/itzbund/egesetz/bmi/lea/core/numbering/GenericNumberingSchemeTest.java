// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.numbering;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GenericNumberingSchemeTest {

    private static final int MAX_DEPTH = 20;

    @Test
    void testNumberSchemeWithDefaultDelimiterAndRecurring() {
        NumberingScheme ns = new GenericNumberingScheme(
            "1.;a);(i)",
            "•", true);
        assertNotNull(ns);

        for (int i = 0; i < MAX_DEPTH; i++) {
            int mod = i % 3;

            if (mod == 2) {
                assertEquals("(", ns.getPrefix(i));
            } else {
                assertEquals("", ns.getPrefix(i));
            }

            if (mod == 0) {
                assertEquals(".", ns.getSuffix(i));
            } else {
                assertEquals(")", ns.getSuffix(i));
            }

            switch (mod) {
                case 0:
                    assertEquals("1", ns.getType(i));
                    break;
                case 1:
                    assertEquals("a", ns.getType(i));
                    break;
                case 2:
                    assertEquals("i", ns.getType(i));
            }
        }

        assertEquals("•", ns.getSymbol(1, 0, false));
        assertEquals("1.", ns.getSymbol(1, 0, true));

        assertEquals("•", ns.getSymbol(11, 1, false));
        assertEquals("k)", ns.getSymbol(11, 1, true));

        assertEquals("•", ns.getSymbol(13, 2, false));
        assertEquals("(xiii)", ns.getSymbol(13, 2, true));

        assertEquals("•", ns.getSymbol(7, 3, false));
        assertEquals("7.", ns.getSymbol(7, 3, true));

        assertEquals("•", ns.getSymbol(2, 4, false));
        assertEquals("b)", ns.getSymbol(2, 4, true));

        assertEquals("•", ns.getSymbol(2, 5, false));
        assertEquals("(ii)", ns.getSymbol(2, 5, true));
    }

    @Test
    void testNumberSchemeWithCustomDelimiterAndNotRecurring() {
        NumberingScheme ns = new GenericNumberingScheme(
            "I.|A.|1.|a)|(1)|(a)|(i)",
            "\uF076\uF0D8\uF0A7•⬧",
            "|", false);
        assertNotNull(ns);

        // @formatter:off
        // array index:                     0         1         2       3       4      5       6       7         8        9          10          11      12       13     14         15       16     17     18        19
        int[] indexes =             {      32,       24,       23,     33,     36,    11,     20,     15,       49,      14,         37,         28,      9,     19,     10,        39,       7,    50,     1,       13};
        // modulo depth (ordered)           0         1         2       3       4      5       6       6         6        6           6           6       6        6      6          6        6      6      6         6
        String[] orderedSymbols =   {"XXXII.",     "X.",    "23.",  "gg)", "(36)", "(k)", "(xx)", "(xv)", "(xlix)", "(xiv)", "(xxxvii)", "(xxviii)", "(ix)", "(xix)", "(x)", "(xxxix)", "(vii)", "(l)", "(i)", "(xiii)"};
        // modulo depth (unordered)         0         1         2       3       4      4       4       4         4        4           4           4       4        4      4          4        4      4      4         4
        String[] unorderedSymbols = {"\uF076", "\uF0D8", "\uF0A7",    "•",    "⬧",  "⬧",    "⬧",    "⬧",     "⬧",     "⬧",       "⬧",        "⬧",    "⬧",    "⬧",   "⬧",       "⬧",    "⬧",   "⬧",   "⬧",     "⬧"};
        // @formatter:on

        for (int i = 0; i < MAX_DEPTH; i++) {
            assertEquals(orderedSymbols[i], ns.getSymbol(indexes[i], i, true));
            assertEquals(unorderedSymbols[i], ns.getSymbol(indexes[i], i, false));
        }
    }

}
