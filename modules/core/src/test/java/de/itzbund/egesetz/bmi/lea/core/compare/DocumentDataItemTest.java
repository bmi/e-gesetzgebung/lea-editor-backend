// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * Testsuite for {@link DocumentDataItem}.
 */
class DocumentDataItemTest {

    @Test
    void testElementTypeDataItemConstructorDocument() {
        ElementTypeDataItem ddi = new ElementTypeDataItem(1, "bill", false, null);
        assertNotNull(ddi);
        assertEquals("D00000001", ddi.getId());
        assertEquals(DocumentDataItem.ItemType.DOCUMENT, ddi.getItemType());
        assertNull(ddi.getParentId());
        assertEquals("bill", ddi.getName());
        assertEquals(0, ddi.getAttributes().size());

        final String parentId = "E00000003";
        ddi = new ElementTypeDataItem(57, "act", false, parentId, "name", "regelungstext", "");
        assertNotNull(ddi);
        assertEquals("D00000039", ddi.getId());
        assertEquals(DocumentDataItem.ItemType.DOCUMENT, ddi.getItemType());
        assertEquals(parentId, ddi.getParentId());
        assertEquals("act", ddi.getName());
        assertEquals(1, ddi.getAttributes().size());
        assertEquals("regelungstext", ddi.getAttribute("name"));
        assertNull(ddi.getAttribute("foo"));

        ddi.setAttribute("foo", null);
        assertEquals(1, ddi.getAttributes().size());

        ddi.setAttribute("foo", "bar");
        assertEquals(2, ddi.getAttributes().size());
        assertEquals("regelungstext", ddi.getAttribute("name"));
        assertEquals("bar", ddi.getAttribute("foo"));

        ddi.setAttribute("name", "baz");
        assertEquals(2, ddi.getAttributes().size());
        assertEquals("baz", ddi.getAttribute("name"));
        assertEquals("bar", ddi.getAttribute("foo"));
    }


    @Test
    void testElementTypeDataItemConstructorElement() {
        final String parentId = "E00000003";

        ElementTypeDataItem edi = new ElementTypeDataItem(72, "block", true, parentId);
        assertNotNull(edi);
        assertEquals("E00000048", edi.getId());
        assertEquals(DocumentDataItem.ItemType.ELEMENT, edi.getItemType());
        assertEquals(parentId, edi.getParentId());
        assertEquals("block", edi.getName());
        assertEquals(0, edi.getAttributes().size());

        edi.addAttributes("foo", "bar", "baz");
        assertEquals(1, edi.getAttributes().size());
        assertEquals("bar", edi.getAttribute("foo"));
        assertNull(edi.getAttribute("baz"));

        edi.addAttributes("foo", "x", "bar", "y", "baz", "z");
        assertEquals(3, edi.getAttributes().size());
        assertEquals("x", edi.getAttribute("foo"));
        assertEquals("y", edi.getAttribute("bar"));
        assertEquals("z", edi.getAttribute("baz"));
    }


    @Test
    void testTextTypeDataItemConstructor() {
        final String parentId = "E00000003";

        TextTypeDataItem tdi = new TextTypeDataItem(1, parentId, null);
        assertNotNull(tdi);
        assertEquals("T00000001", tdi.getId());
        assertEquals(parentId, tdi.getParentId());
        assertEquals("", tdi.getContent());

        tdi = new TextTypeDataItem(11, parentId, "  ");
        assertNotNull(tdi);
        assertEquals("T0000000B", tdi.getId());
        assertEquals(parentId, tdi.getParentId());
        assertEquals(" ", tdi.getContent());

        tdi = new TextTypeDataItem(1234, parentId, "\t");
        assertEquals("T000004D2", tdi.getId());
        assertEquals(" ", tdi.getContent());

        tdi.setContent(" \r\n  \t");
        assertEquals(" ", tdi.getContent());

        tdi.setContent("\nx ");
        assertEquals("\nx ", tdi.getContent());

        assertEquals(DocumentDataItem.ItemType.TEXT, tdi.getItemType());
    }

}
