// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.util;

import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class CustomResourceProviderTest {

    @Test
    void testGetSingleInputSource() {
        CustomResourceProvider provider = CustomResourceProvider.getInstance();
        assertNotNull(provider);

        ReusableInputSource ris = provider.getSingleInputSource(
            "/ldml/akn4un/cl_Sesion56_2.xml");
        assertNotNull(ris);
    }

}
