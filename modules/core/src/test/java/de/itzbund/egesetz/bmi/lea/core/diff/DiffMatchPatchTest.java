// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.diff;

import com.github.difflib.text.DiffRow;
import com.github.difflib.text.DiffRowGenerator;
import de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DiffMatchPatchTest {

    private DiffMatchPatch dmp;
    private DiffRowGenerator generator;

    private static Stream<Arguments> getSampleTexts() {
        return Stream.of(
            Arguments.of(
                List.of("§ 7 werden die folgenden Absätze 5 bis 7 angefügt:"),
                List.of("Dem § 7 werden die folgenden Absätze 5 und 6 angefügt:")
            ),
            Arguments.of(
                List.of("Ein Betrieb muss insgesamt zehn Personen umfassen."),
                List.of("Ein Betrieb muss insgesamt zehn Personen umfassen.")
            ),
            Arguments.of(
                List.of("Über Ausnahmen entscheidet die zuständige Landesbehörde."),
                List.of("")
            ),
            Arguments.of(
                List.of("Das Sachenrechtsbereinigungsgesetz vom 15. August 1997 (BGBl. I S. 1955) wird wie "
                        + "folgt geändert:",
                    "1. § 2 wird wie folgt gefasst: \"§ 2 Geltungsbereich",
                    "(1) Dieses Gesetz gilt nicht für Meeresufer.",
                    "(2) Die Regelungen dieses Gesetzes sind nicht abdingbar.\""),
                List.of("Das Sachenrechtsbereinigungsgesetz vom 15. August 1997 (BGBl. I S. 1955) wird wie "
                        + "folgt geändert:",
                    "1. § 2 wird wie folgt gefasst: \"§ 2 Geltungsbereich",
                    "(1) Dieses Gesetz gilt nicht für Meeresufer.",
                    "(2) Die Regelungen dieses Gesetzes sind weder abdingbar, noch kann auf sie einseitig"
                        + " verzichtet werden.\"")
            )
        );
    }

    @BeforeEach
    void init() {
        dmp = new DiffMatchPatch();
        generator = DiffRowGenerator.create()
            .showInlineDiffs(true)
            .inlineDiffByWord(true)
            .oldTag(f -> "~")
            .newTag(f -> "**")
            .build();
    }

    @AfterEach
    void clean() {
        System.out.println("=========================================================================================");
    }

    @Test
    void testSameTexts() {
        String randomText = Utils.getRandomString();
        LinkedList<DiffMatchPatch.Diff> diffs = dmp.diffMain(randomText, randomText);
        dmp.diffCleanupSemantic(diffs);
        System.out.println(diffs);
        assertNotNull(diffs);
        assertEquals(1, diffs.size());

        DiffMatchPatch.Diff diff = diffs.get(0);
        assertEquals(DiffMatchPatch.Operation.EQUAL, diff.operation);
        assertEquals(randomText, diff.text);

        assertThrows(IllegalArgumentException.class, () -> dmp.diffMain(null, null));
    }

    @Test
    void testTextDeleted() {
        String randomText = Utils.getRandomString();
        LinkedList<DiffMatchPatch.Diff> diffs = dmp.diffMain(randomText, "");
        dmp.diffCleanupSemantic(diffs);
        System.out.println(diffs);
        assertNotNull(diffs);
        assertEquals(1, diffs.size());

        DiffMatchPatch.Diff diff = diffs.get(0);
        assertEquals(DiffMatchPatch.Operation.DELETE, diff.operation);
        assertEquals(randomText, diff.text);

        assertThrows(IllegalArgumentException.class, () -> dmp.diffMain(randomText, null));
    }

    @Test
    void testTextAdded() {
        String randomText = Utils.getRandomString();
        LinkedList<DiffMatchPatch.Diff> diffs = dmp.diffMain("", randomText);
        dmp.diffCleanupSemantic(diffs);
        System.out.println(diffs);
        assertNotNull(diffs);
        assertEquals(1, diffs.size());

        DiffMatchPatch.Diff diff = diffs.get(0);
        assertEquals(DiffMatchPatch.Operation.INSERT, diff.operation);
        assertEquals(randomText, diff.text);

        assertThrows(IllegalArgumentException.class, () -> dmp.diffMain(null, randomText));
    }

    @Test
    void testEmptyText() {
        LinkedList<DiffMatchPatch.Diff> diffs = dmp.diffMain("", "");
        dmp.diffCleanupSemantic(diffs);
        System.out.println(diffs);
        assertNotNull(diffs);
        assertEquals(0, diffs.size());
    }

    @ParameterizedTest
    @MethodSource("getSampleTexts")
    void testDiff_Example01(List<String> baseTexts, List<String> versionTexts) {
        List<List<DiffMatchPatch.Diff>> diffsList = getDiffs(
            baseTexts,
            versionTexts

        );
        assertNotNull(diffsList);
        assertFalse(diffsList.isEmpty());

        diffsList.forEach(diffs -> {
            String baseDiffs = CompareUtils.getHdRconformantBaseText(diffs);
            String versionDiffs = CompareUtils.getHdRconformantVersionText(diffs);
            System.out.println(printLine("DMP", baseDiffs, versionDiffs));
        });

        List<DiffRow> rows = generator.generateDiffRows(
            baseTexts,
            versionTexts
        );

        rows.forEach(row -> System.out.println(printLine("JDU", row.getOldLine(), row.getNewLine())));
    }

    private String printLine(String tool, String left, String right) {
        String rightCol = right;

        if (left.equals(right)) {
            rightCol = "unverändert";
        } else if (right.isBlank()) {
            rightCol = "entfällt";
        }

        return String.format("%s :: %s  --  %s", tool, left, rightCol);
    }

    private List<List<DiffMatchPatch.Diff>> getDiffs(List<String> baseTexts, List<String> versionTexts) {
        int baseCount = baseTexts.size();
        int versionCount = versionTexts.size();
        int maxCount = Math.max(baseCount, versionCount);
        List<List<DiffMatchPatch.Diff>> diffColl = new ArrayList<>();

        for (int i = 0; i < maxCount; i++) {
            Pair<String, String> row = getRow(i, baseTexts, versionTexts);
            LinkedList<DiffMatchPatch.Diff> diffs = dmp.diffMain(row.getLeft(), row.getRight());
            dmp.diffCleanupSemantic(diffs);
            diffColl.add(diffs);
        }

        return diffColl;
    }

    private Pair<String, String> getRow(int index, List<String> baseTexts, List<String> versionTexts) {
        String base = index < baseTexts.size() ? baseTexts.get(index) : "";
        String version = index < versionTexts.size() ? versionTexts.get(index) : "";
        return Pair.of(base, version);
    }

}
