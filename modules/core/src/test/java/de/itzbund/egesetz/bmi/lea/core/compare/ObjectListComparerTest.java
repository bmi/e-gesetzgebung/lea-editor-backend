// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import de.itzbund.egesetz.bmi.lea.core.json.JSONUtils;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_P;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.setGuid;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ObjectListComparerTest {

    @Test
    void testObjectsCompare() {
        assertEquals(28, getDeletedIds().size() + getEqualIds().size() + getInsertedIds().size());
        ObjectListComparer<JSONObject, UUID> comparer = new JSONObjectListComparer(null, null);
        Map<UUID, DiffMatchPatch.Operation> result = comparer.compare(getBaseObjects(), getVersionObjects());
        assertNotNull(result);
        assertEquals(28, result.size());
        assertEquals(28, comparer.getKeyDiffs().size());
        getDeletedIds().forEach(s -> checkDiffOperation(s, result, DiffMatchPatch.Operation.DELETE));
        getEqualIds().forEach(s -> checkDiffOperation(s, result, DiffMatchPatch.Operation.EQUAL));
        getInsertedIds().forEach(s -> checkDiffOperation(s, result, DiffMatchPatch.Operation.INSERT));
    }

    private List<JSONObject> getBaseObjects() {
        return Stream.of(
                "01000000-0000-0000-0000-000000000000", "01000000-0100-0000-0000-000000000000",
                "00000000-0000-0000-0000-010000000000", "00000000-0000-0000-0000-020000000000",
                "00000000-0000-0000-0000-030000000000", "01000000-0200-0000-0000-000000000000",
                "00000000-0000-0000-0000-040000000000", "00000000-0000-0000-0000-050000000000",
                "02000000-0000-0000-0000-000000000000", "00000000-0000-0000-0000-060000000000",
                "02000000-0100-0000-0000-000000000000", "02000000-0100-0100-0000-000000000000",
                "00000000-0000-0000-0000-070000000000", "00000000-0000-0000-0000-080000000000",
                "02000000-0100-0200-0000-000000000000", "00000000-0000-0000-0000-090000000000",
                "02000000-0200-0000-0000-000000000000", "00000000-0000-0000-0000-100000000000",
                "00000000-0000-0000-0000-110000000000", "00000000-0000-0000-0000-120000000000",
                "03000000-0000-0000-0000-000000000000", "00000000-0000-0000-0000-130000000000",
                "00000000-0000-0000-0000-140000000000", "00000000-0000-0000-0000-150000000000")
            .map(this::mapIdToJSONObject)
            .collect(Collectors.toList());
    }

    private List<JSONObject> getVersionObjects() {
        return Stream.of(
                "01000000-0100-0000-0000-000000000000", "00000000-0000-0000-0000-020000000000",
                "00000000-0000-0000-0000-040000000000", "00000000-0000-0000-0000-050000000000",
                "a0000000-0000-0000-0000-040000000000", "00000000-0000-0000-0000-070000000000",
                "02000000-0100-0200-0000-000000000000", "00000000-0000-0000-0000-090000000000",
                "a0000000-0000-0000-0000-070000000000", "02000000-0100-a200-0000-000000000000",
                "00000000-0000-0000-0000-120000000000", "02000000-0100-b200-0000-000000000000",
                "00000000-0000-0000-0000-130000000000", "00000000-0000-0000-0000-140000000000")
            .map(this::mapIdToJSONObject)
            .collect(Collectors.toList());
    }

    private List<String> getEqualIds() {
        return Stream.of(
                "00000000-0000-0000-0000-020000000000", "00000000-0000-0000-0000-040000000000",
                "00000000-0000-0000-0000-050000000000", "00000000-0000-0000-0000-070000000000",
                "00000000-0000-0000-0000-090000000000", "00000000-0000-0000-0000-120000000000",
                "00000000-0000-0000-0000-130000000000", "00000000-0000-0000-0000-140000000000",
                "01000000-0100-0000-0000-000000000000", "02000000-0100-0200-0000-000000000000")
            .collect(Collectors.toList());
    }

    private List<String> getDeletedIds() {
        return Stream.of(
                "00000000-0000-0000-0000-010000000000", "00000000-0000-0000-0000-030000000000",
                "00000000-0000-0000-0000-060000000000", "00000000-0000-0000-0000-080000000000",
                "00000000-0000-0000-0000-100000000000", "00000000-0000-0000-0000-110000000000",
                "00000000-0000-0000-0000-150000000000", "01000000-0000-0000-0000-000000000000",
                "01000000-0200-0000-0000-000000000000", "02000000-0000-0000-0000-000000000000",
                "02000000-0100-0000-0000-000000000000", "02000000-0100-0100-0000-000000000000",
                "02000000-0200-0000-0000-000000000000", "03000000-0000-0000-0000-000000000000")
            .collect(Collectors.toList());
    }

    private List<String> getInsertedIds() {
        return Stream.of(
                "02000000-0100-a200-0000-000000000000", "02000000-0100-b200-0000-000000000000",
                "a0000000-0000-0000-0000-040000000000", "a0000000-0000-0000-0000-070000000000")
            .collect(Collectors.toList());
    }

    private JSONObject mapIdToJSONObject(String s) {
        JSONObject jsonObject = JSONUtils.makeNewJSONObject(ELEM_P, true, false);
        setGuid(jsonObject, UUID.fromString(s));
        return jsonObject;
    }

    private void checkDiffOperation(String s, Map<UUID, DiffMatchPatch.Operation> diffMap, DiffMatchPatch.Operation operation) {
        assertEquals(operation, diffMap.get(UUID.fromString(s)));
    }

}
