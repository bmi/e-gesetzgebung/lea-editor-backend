// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.util;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Testsuite for {@link FlatGroupedList}.
 */
class FlatGroupedListTest {

    private static final String NAME = "heinz";
    private static final String NAME2 = "berta";
    private static final String GROUP = "GROUP";


    /**
     * Tests for {@link ListGroup}.
     */
    @Test
    void testListGroup() {
        ListGroup<String> listGroup = new ListGroup<>(NAME);
        assertNotNull(listGroup);
        assertEquals(NAME, listGroup.getName());
        assertEquals(listGroup, new ListGroup<>(NAME));
        assertNotNull(listGroup.toString());
        assertEquals(listGroup, new ListGroup<>(NAME));

        ListGroup<String> anotherGroup = new ListGroup<>("TEST");
        assertNotEquals(listGroup, anotherGroup);
    }


    /**
     * Tests methods of {@link FlatGroupedList}.
     */
    @Test
    void testFlatGroupedList() {
        FlatGroupedList<String> fgl = new FlatGroupedList<>();
        assertNotNull(fgl);

        fgl.addGroup(new ListGroup<>(NAME));
        assertNotNull(fgl.makeGroup(NAME2));

        fgl.makeGroups(NAME, NAME2);
        assertTrue(fgl.conformsTo(new ArrayList<>()));

        FlatGroupedList<String> finalFgl1 = fgl;
        assertThrows(IllegalArgumentException.class,
            () -> finalFgl1.addElement(null, NAME, true));

        fgl.addElement("A", NAME, true);
        FlatGroupedList<String> finalFgl = fgl;
        assertThrows(IllegalArgumentException.class,
            () -> finalFgl.addElement("B", GROUP, false));
        assertTrue(fgl.conformsTo(Collections.singletonList("A")));

        fgl.addElement("B", GROUP, true);
        assertTrue(fgl.conformsTo(Arrays.asList("A", "B")));

        fgl = new FlatGroupedList<>();
        assertFalse(fgl.contains("", "group"));
        assertFalse(fgl.containsAll(Collections.singletonList("A"), "group"));
        assertFalse(fgl.conformsTo(Arrays.asList("A", "B")));

        fgl.addElement("X", NAME, true);
        assertFalse(fgl.conformsTo(Arrays.asList("A", "B")));

        fgl = new FlatGroupedList<>();
        fgl.addElements("group1", true, "A", "B");
        fgl.addElements("group2", true, "C", "D", "E");
        assertTrue(fgl.conformsTo(Arrays.asList("A", "B", "D", "C", "E")));

        fgl = new FlatGroupedList<>();
        fgl.addElements("group1", true, "A", "B");
        fgl.addElements("group2", true, "C", "D", "E");
        assertFalse(fgl.conformsTo(Arrays.asList("A", "X", "D", "C", "E")));
    }

}
