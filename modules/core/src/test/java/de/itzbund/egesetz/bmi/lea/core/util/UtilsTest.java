// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.util;

import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_P;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_CMP_TYPE;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewDefaultJSONObjectWithText;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.LIST_OF_ADJECTIVES;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.LIST_OF_NAMES;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.areAllNull;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.combineSets;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.getElementAtIndex;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.getFirstOrDefault;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.getPrettyPrintedXml;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.getRandomIntBetween;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.getRandomString;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.isJSONValid;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.isMissing;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.message;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.pickItem;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.printTable;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.range;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.reduceString;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.replaceItemBy;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.setReflectiveAttribute;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.writeTable;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.xmlLinearize;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.xor;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.zipList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Testsuite for {@link Utils}.
 */
@Log4j2
class UtilsTest {

    private static final Pattern PATT_LEA_CMP_START = Pattern
        .compile(String.format("[{]\\s*\"%s\":", JSON_KEY_CMP_TYPE));

    private static final String SAMPLE_TEXT = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr";

    private static final Map<Integer, String> TEXTS = new HashMap<>();

    private static final Map<String, String> BASE64_TESTS = new HashMap<>();

    static {
        TEXTS.put(6, "Lor...");
        TEXTS.put(10, "Lorem i...");
        TEXTS.put(23, "Lorem ipsum dolor si...");
        TEXTS.put(50, "Lorem ipsum dolor sit amet, consetetur sadipsci...");
        TEXTS.put(SAMPLE_TEXT.length(), SAMPLE_TEXT);
        TEXTS.put(SAMPLE_TEXT.length() + 1, SAMPLE_TEXT);
    }


    static {
        BASE64_TESTS.put("", "");
        BASE64_TESTS.put("Hello, world!", "SGVsbG8sIHdvcmxkIQ==");
        BASE64_TESTS.put(
            "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut "
                + "labore et dolore magna",
            "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNldGV0dXIgc2FkaXBzY2luZyBlbGl0ciwgc2VkIGRpYW0gbm9udW15IGVpcm1vZCB0ZW1wb3IgaW52aWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYQ==");
        BASE64_TESTS.put("Lorem ipsum dolor sit amet, consetetur sadipscing elitr,\nsed diam nonumy eirmod tempor "
                + "invidunt ut labore et dolore magna",
            "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNldGV0dXIgc2FkaXBzY2luZyBlbGl0ciwKc2VkIGRpYW0gbm9udW15IGVpcm1vZCB0ZW1wb3IgaW52aWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYQ==");
        BASE64_TESTS.put("<note>\n"
                + "    <to>Tove</to>\n"
                + "    <from>Jani</from>\n"
                + "    <heading>Reminder</heading>\n"
                + "    <body>Don't forget me this weekend!</body>\n"
                + "</note>",
            "PG5vdGU+CiAgICA8dG8+VG92ZTwvdG8+CiAgICA8ZnJvbT5KYW5pPC9mcm9tPgogICAgPGhlYWRpbmc"
                + "+UmVtaW5kZXI8L2hlYWRpbmc"
                + "+CiAgICA8Ym9keT5Eb24ndCBmb3JnZXQgbWUgdGhpcyB3ZWVrZW5kITwvYm9keT4KPC9ub3RlPg==");
        BASE64_TESTS.put("{}", "e30=");
        BASE64_TESTS.put("{\n"
                + "    \"glossary\": {\n"
                + "        \"title\": \"example glossary\",\n"
                + "\t\t\"GlossDiv\": {\n"
                + "            \"title\": \"S\",\n"
                + "\t\t\t\"GlossList\": {\n"
                + "                \"GlossEntry\": {\n"
                + "                    \"ID\": \"SGML\",\n"
                + "\t\t\t\t\t\"SortAs\": \"SGML\",\n"
                + "\t\t\t\t\t\"GlossTerm\": \"Standard Generalized Markup Language\",\n"
                + "\t\t\t\t\t\"Acronym\": \"SGML\",\n"
                + "\t\t\t\t\t\"Abbrev\": \"ISO 8879:1986\",\n"
                + "\t\t\t\t\t\"GlossDef\": {\n"
                + "                        \"para\": \"A meta-markup language, used to create markup "
                + "languages such as DocBook.\",\n"
                + "\t\t\t\t\t\t\"GlossSeeAlso\": [\"GML\", \"XML\"]\n"
                + "                    },\n"
                + "\t\t\t\t\t\"GlossSee\": \"markup\"\n"
                + "                }\n"
                + "            }\n"
                + "        }\n"
                + "    }\n"
                + "}",
            "ewogICAgImdsb3NzYXJ5IjogewogICAgICAgICJ0aXRsZSI6ICJleGFtcGxlIGdsb3NzYXJ5IiwKCQkiR2xvc3NEaXYiOiB7CiAgICAgICAgICAgICJ0aXRsZSI6ICJTIiwKCQkJIkdsb3NzTGlzdCI6IHsKICAgICAgICAgICAgICAgICJHbG9zc0VudHJ5IjogewogICAgICAgICAgICAgICAgICAgICJJRCI6ICJTR01MIiwKCQkJCQkiU29ydEFzIjogIlNHTUwiLAoJCQkJCSJHbG9zc1Rlcm0iOiAiU3RhbmRhcmQgR2VuZXJhbGl6ZWQgTWFya3VwIExhbmd1YWdlIiwKCQkJCQkiQWNyb255bSI6ICJTR01MIiwKCQkJCQkiQWJicmV2IjogIklTTyA4ODc5OjE5ODYiLAoJCQkJCSJHbG9zc0RlZiI6IHsKICAgICAgICAgICAgICAgICAgICAgICAgInBhcmEiOiAiQSBtZXRhLW1hcmt1cCBsYW5ndWFnZSwgdXNlZCB0byBjcmVhdGUgbWFya3VwIGxhbmd1YWdlcyBzdWNoIGFzIERvY0Jvb2suIiwKCQkJCQkJIkdsb3NzU2VlQWxzbyI6IFsiR01MIiwgIlhNTCJdCiAgICAgICAgICAgICAgICAgICAgfSwKCQkJCQkiR2xvc3NTZWUiOiAibWFya3VwIgogICAgICAgICAgICAgICAgfQogICAgICAgICAgICB9CiAgICAgICAgfQogICAgfQp9");
    }

    @RepeatedTest(value = 17, name = "UtilsTest.testWordLists :: {currentRepetition}/{totalRepetitions}")
    void testWordLists() {
        assertThat(Utils.LIST_OF_ADJECTIVES).isNotNull();
        assertFalse(LIST_OF_ADJECTIVES.isEmpty());

        assertThat(Utils.LIST_OF_NAMES).isNotNull();
        assertFalse(LIST_OF_NAMES.isEmpty());

        String rand1 = Utils.getRandomString();
        assertNotNull(rand1);
        assertFalse(rand1.isBlank());

        String rand2 = Utils.getRandomString();
        assertNotNull(rand2);
        assertFalse(rand2.isBlank());

        log.debug("rand1 = " + rand1);
        log.debug("rand2 = " + rand2);

        assertNotEquals(rand1, rand2);
        assertEquals(2, rand1.split("_").length);
        assertEquals(2, rand2.split("_").length);

        int randLength = ThreadLocalRandom.current()
            .nextInt(45, 72);
        log.debug("random length = " + randLength);
        String rand3 = Utils.getRandomString(randLength, 0);
        String rand4 = Utils.getRandomString(randLength, Utils.RANDOM_STRING_MAX_LENGTH);
        String rand5 = Utils.getRandomString(randLength, Utils.RANDOM_STRING_MIN_LENGTH);
        String rand6 = Utils.getRandomString(randLength,
            Utils.RANDOM_STRING_MIN_LENGTH | Utils.RANDOM_STRING_MAX_LENGTH);

        log.debug("rand3 = " + rand3);
        log.debug("rand4 = " + rand4);
        log.debug("rand5 = " + rand5);
        log.debug("rand6 = " + rand6);

        assertNotNull(rand3);
        assertEquals(2, rand3.split("_").length);

        assertNotNull(rand4);
        assertTrue(rand4.length() <= randLength);

        assertNotNull(rand5);
        assertTrue(rand5.length() >= randLength);

        assertNotNull(rand6);
        assertEquals(randLength, rand6.length());
    }


    /**
     * Tests {@link Utils#asString(String)}.
     */
    @Test
    void testQuotingStrings() {
        assertEquals("\"test\"", Utils.asString("test"));
        assertEquals("\"\"test\"\"", Utils.asString("\"test\""));
        assertEquals("\"\"", Utils.asString(""));
    }


    @Test
    void testReplaceItemBy() {
        List<String> oldItems = List.of("0", "1", "2", "3", "4");
        List<String> newItems = List.of("a", "b", "c");

        assertNull(replaceItemBy(null, 0, List.of()));
        assertEquals(oldItems, replaceItemBy(oldItems, -1, List.of()));
        assertEquals(oldItems, replaceItemBy(oldItems, 100, List.of()));
        assertEquals(oldItems, replaceItemBy(oldItems, 1, List.of()));
        assertEquals(List.of("0", "1", "2", "a", "b", "c", "4"), replaceItemBy(oldItems, 3, newItems));
        assertEquals(List.of("a", "b", "c", "1", "2", "3", "4"), replaceItemBy(oldItems, 0, newItems));
        assertEquals(List.of("0", "1", "2", "3", "a", "b", "c"), replaceItemBy(oldItems, 4, newItems));
        assertEquals(List.of("0", "1", "2", "3", "4"), replaceItemBy(oldItems, 5, newItems));
    }


    /**
     * Tests {@link Utils#xmlNormalize(String)}.
     */
    @Test
    void testXmlNormalize() {
        assertNull(Utils.xmlNormalize(null));
        assertEquals("", Utils.xmlNormalize(""));
        assertEquals(" ", Utils.xmlNormalize(" "));
        assertEquals(" ", Utils.xmlNormalize(" \t \r \n"));
        assertEquals("test", Utils.xmlNormalize("test"));
        assertEquals(" test", Utils.xmlNormalize("  test"));
        assertEquals("test ", Utils.xmlNormalize("test\n"));
        assertEquals(" another test ", Utils.xmlNormalize("\tanother test \r"));
        assertEquals(" another test ", Utils.xmlNormalize("\tanother  test \r"));
        assertEquals(" the quick brown fox ", Utils.xmlNormalize(" the quick \t brown fox  "));
    }


    /**
     * Tests {@link Utils#getContent(ReusableInputSource)}.
     */
    @Test
    void testGetContentReusableInputSource() {
        ReusableInputSource source = new ReusableInputSource(
            UtilsTest.class.getResourceAsStream("/gui/block1.json")
        );
        assertNotNull(source);

        String content = Utils.getContent(source);
        assertNotNull(content);
        assertTrue(PATT_LEA_CMP_START.matcher(content)
            .find());

        assertEquals("", Utils.getContent(null));
    }


    /**
     * Tests {@link Utils#getContentOfTextResource(String)}.
     */
    @Test
    void testGetContentOfTextResource() {
        assertThrows(NullPointerException.class, () -> Utils.getContentOfTextResource(null));
        assertNull(Utils.getContentOfTextResource("xxx"));

        String content = Utils.getContentOfTextResource("/gui/block1.json");
        assertNotNull(content);
        assertTrue(PATT_LEA_CMP_START.matcher(content)
            .find());
    }


    /**
     * Tests {@link Utils#isMissing(Object)}.
     */
    @Test
    void testIsMissing() {
        String testString;
        assertTrue(isMissing(null));
        testString = "";
        assertTrue(isMissing(testString));
        testString = " ";
        assertTrue(isMissing(testString));
        testString = "\t";
        assertTrue(isMissing(testString));
        assertFalse(isMissing("\nx"));
        assertFalse(isMissing(0));
    }


    /**
     * Tests {@link Utils#isJSONValid(String)}.
     */
    @Test
    void testValidJson() {
        assertTrue(isJSONValid("{}"));
        assertTrue(isJSONValid("[]"));
        assertTrue(isJSONValid("{\"key\":\"\"}"));
        assertTrue(isJSONValid("{\"key\":false}"));
        assertTrue(isJSONValid("{\"key\": null}"));
        assertFalse(isJSONValid(""));
        assertFalse(isJSONValid(" "));
        assertFalse(isJSONValid("[\"key\": \"value\"]"));
    }


    /**
     * Tests {@link Utils#reduceString(String, int)}.
     */
    @Test
    void testReduceString() {
        assertNull(reduceString(null, 0));
        assertNull(reduceString(null, -1));
        assertNull(reduceString(null, 10));

        for (String sampleText : Arrays.asList("", " ", "          ", "\t", "\n", "\r\n ")) {
            assertEquals(sampleText, reduceString(sampleText, 0));
            assertEquals(sampleText, reduceString(sampleText, -1));
            assertEquals(sampleText, reduceString(sampleText, 10));
        }

        for (int i = 0; i < 6; i++) {
            assertEquals(SAMPLE_TEXT, reduceString(SAMPLE_TEXT, i));
        }

        // k is the desired length
        for (int k = 6; k < 20; k++) {
            // i is the length of text
            for (int i = 0; i <= k; i++) {
                String text = SAMPLE_TEXT.substring(0, i);
                assertEquals(i, text.length());
                assertEquals(text, reduceString(text, k));
            }
        }

        for (Map.Entry<Integer, String> entry : TEXTS.entrySet()) {
            assertEquals(entry.getValue(), reduceString(SAMPLE_TEXT, entry.getKey()));
        }
    }


    /**
     * Tests {@link Utils#getPrettyPrintedXml(Reader)}.
     */
    @Test
    @SneakyThrows
    void testGetPrettyPrintedXml() {
        assertEquals("", getPrettyPrintedXml((Reader) null));

        InputStream inputStream = UtilsTest.class.getResourceAsStream("/xml/sample-lin.xml");
        assertNotNull(inputStream);
        String xml = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))
            .lines()
            .collect(Collectors.joining("\n"));
        assertNotNull(xml);
        assertFalse(xml.isBlank());
        log.debug("original XML: " + xml);
        StringReader input = new StringReader(xml);

        String pretty = Utils.getPrettyPrintedXml(input);
        assertNotNull(pretty);
        assertFalse(pretty.isBlank());
        log.debug(String.format("pretty XML:\n-----\n%s-----", pretty));

        BufferedReader reader = new BufferedReader(new StringReader(pretty));
        StringBuilder sb = new StringBuilder();
        String line;

        while ((line = reader.readLine()) != null) {
            sb.append(line.trim());
        }

        assertEquals(xml.replaceAll("(\\S)\\s+[/]", "$1/"), sb.toString());
    }


    @Test
    void testGetPrettyPrintedXmlString() {
        String xmlContent = Utils.getContentOfTextResource("/xml/sample-lin.xml");
        String prettyPrinted = Utils.getPrettyPrintedXml(xmlContent);
        assertNotNull(prettyPrinted);
        assertFalse(prettyPrinted.isBlank());
    }


    @Test
    void testUnmask() {
        assertNull(Utils.unmask(null));

        String testString = " ";
        assertEquals(testString, Utils.unmask(testString));

        testString = "abc/05";
        assertEquals(testString, Utils.unmask(testString));

        testString =
            "[{\"xml-model\":\"href=\\\\\"Grammatiken\\\\/legalDocML.de.sch\\\\\" "
                + "type=\\\\\"application\\\\/xml\\\\\" schematypens=\\\\\"http:\\\\/\\\\/purl.oclc"
                + ".org\\\\/dsdl\\\\/schematron\\\\\"\", \"type\":\"akn:akomaNtoso\","
                + "\"xmlns:akn\":\"http:\\\\/\\\\/Inhaltsdaten.LegalDocML.de\\\\/1.4\\\\/\"}]";
        String expected =
            "[{\"xml-model\":\"href=\\\"Grammatiken\\/legalDocML.de.sch\\\" type=\\\"application\\/xml\\\" "
                + "schematypens=\\\"http:\\/\\/purl.oclc.org\\/dsdl\\/schematron\\\"\", "
                + "\"type\":\"akn:akomaNtoso\",\"xmlns:akn\":\"http:\\/\\/Inhaltsdaten.LegalDocML.de\\/1"
                + ".4\\/\"}]";
        assertEquals(expected, Utils.unmask(testString));
        assertEquals(expected, Utils.unmask(expected));
    }


    @Test
    void testBytesToHex() {
        assertEquals("", Utils.bytesToHex(new byte[]{}));
        assertEquals("20", Utils.bytesToHex(" ".getBytes(StandardCharsets.UTF_8)));
        assertEquals("74657374", Utils.bytesToHex("test".getBytes(StandardCharsets.UTF_8)));
        assertEquals("48656C6C6F2045676F6E21", Utils.bytesToHex("Hello Egon!".getBytes(StandardCharsets.UTF_8)));
    }


    @Test
    void testGetHash() {
        // example from Horstmann, C. S. (2019). Core Java: Advanced Features (Vol. 2). Prentice Hall, p. 584.
        String sampleText = "Upon my death, my property shall be divided equally among my children;"
            + " however, my son George shall receive nothing.\n";
        assertEquals("125F0903E73130192EA6E7E4904384B438998F67",
            Utils.bytesToHex(Utils.getHash(Utils.DigestAlgorithm.SHA1, sampleText, StandardCharsets.UTF_8)));
    }


    @Test
    void testGetLines() {
        assertEquals(0, Utils.getLines("")
            .size());
        assertEquals(1, Utils.getLines(" ")
            .size());
        assertEquals(3, Utils.getLines("hello\n and \nhi")
            .size());
    }


    @Test
    void testFindLine() {
        assertFalse(Utils.findLine(Utils.getLines(""), ""));
        assertTrue(Utils.findLine(Utils.getLines(" "), ""));
        assertTrue(Utils.findLine(Utils.getLines(" hello "), "hello"));
        assertTrue(Utils.findLine(Utils.getLines("hello\nand\nhi"), "hi"));
        assertTrue(Utils.findLine(Utils.getLines("hello\n and \nhi"), "hello"));
        assertTrue(Utils.findLine(Utils.getLines("hello\nand\n\thi"), "and"));
    }


    @Test
    void testFindLines() {
        assertFalse(Utils.findLines(Utils.getLines(""), true, ""));
        assertFalse(Utils.findLines(Utils.getLines(""), false, ""));
        assertTrue(Utils.findLines(Utils.getLines(" "), true, ""));
        assertTrue(Utils.findLines(Utils.getLines(" hello "), false, "hello"));
        assertTrue(Utils.findLines(Utils.getLines("hello\nand\nhi"), true, "hi"));
        assertTrue(Utils.findLines(Utils.getLines("hello\n and \nhi"), false, "hello"));
        assertTrue(Utils.findLines(Utils.getLines("hello\nand\n\thi"), true, "hi", "and"));
        assertFalse(Utils.findLines(Utils.getLines("hello\nand\n\thi"), true, "hi", "X"));
        assertTrue(Utils.findLines(Utils.getLines("hello\nand\n\thi"), false, "hi", "X"));
    }


    @Test
    void testBase64Encode() {
        assertThrows(NullPointerException.class, () -> Utils.base64Encode(null));

        for (Map.Entry<String, String> entry : BASE64_TESTS.entrySet()) {
            assertEquals(entry.getValue(), Utils.base64Encode(entry.getKey()));
        }
    }


    @Test
    void testBase64Decode() {
        assertThrows(NullPointerException.class, () -> Utils.base64Decode(null));

        for (Map.Entry<String, String> entry : BASE64_TESTS.entrySet()) {
            assertEquals(entry.getKey(), Utils.base64Decode(entry.getValue()));
        }
    }


    @Test
    void testPickItem() {
        assertThrows(NullPointerException.class, () -> Utils.pickItem(null));

        int count = 100;

        List<Integer> intList = List.of();
        assertNull(Utils.pickItem(intList));
        intList = List.of(75, 0, -7, 83, 99, -2, Integer.MAX_VALUE);
        checkPickedItems(count, intList);

        List<String> stringList = List.of();
        assertNull(Utils.pickItem(stringList));
        stringList = Stream.of(
                Utils.getRandomString(),
                Utils.getRandomString(),
                Utils.getRandomString(),
                Utils.getRandomString(),
                Utils.getRandomString(),
                Utils.getRandomString(),
                Utils.getRandomString(),
                Utils.getRandomString(),
                Utils.getRandomString(),
                Utils.getRandomString())
            .collect(Collectors.toList());
        checkPickedItems(count, stringList);

        List<Map<Integer, String>> mapList = List.of();
        assertNull(Utils.pickItem(mapList));
        mapList = List.of(Map.of(), Map.of(1, "One"), Map.of(2, "Two", 3, "Three"), Map.of(), Map.of(2,
            Utils.getRandomString(), 3, Utils.getRandomString(), 5, Utils.getRandomString(), 7,
            Utils.getRandomString(), 11, Utils.getRandomString()));
        checkPickedItems(count, mapList);
    }


    private <E> void checkPickedItems(int count, List<E> list) {
        for (int i = 0; i < count; i++) {
            E item = pickItem(list);
            assertTrue(list.contains(item));
        }
    }


    @Test
    void testGetDefaultPrefix() {
        assertEquals(List.of("ns0", "ns1", "ns2", "ns75", "ns3"),
            Arrays.stream(new int[]{0, 1, 2, 75, -3})
                .boxed()
                .map(Utils::getDefaultPrefix)
                .collect(
                    Collectors.toList()));
    }


    @Test
    void testGetRandomPrefix() {
        assertThrows(IllegalArgumentException.class, () -> Utils.getRandomPrefix(0, 2));
        assertThrows(IllegalArgumentException.class, () -> Utils.getRandomPrefix(2, -1));

        checkRandomPrefix(Utils.getRandomPrefix(1, 3), 1, 3);
        checkRandomPrefix(Utils.getRandomPrefix(2, 2), 2, 2);
        checkRandomPrefix(Utils.getRandomPrefix(7, 3), 3, 7);
    }


    private void checkRandomPrefix(String prefix, int min, int max) {
        assertNotNull(prefix);
        assertFalse(prefix.isBlank());
        assertTrue(min <= prefix.length());
        assertTrue(prefix.length() <= max);
        assertTrue(Character.isLetter(prefix.toCharArray()[0]));
    }


    @Test
    void testIsXmlValid() {
        assertThrows(IllegalArgumentException.class, () -> Utils.isXMLValid(null));

        assertFalse(Utils.isXMLValid(""));
        assertFalse(Utils.isXMLValid(Utils.getRandomString()));
        assertTrue(Utils.isXMLValid("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<test/>"));
        assertTrue(Utils.isXMLValid("<Root attr1=\"0\">\n"
            + "    <child>Some text\n"
            + "        <inline spec=\"...\"/>.\n"
            + "    </child>\n"
            + "</Root>\n"));
    }


    @RepeatedTest(12)
    void testGetRandomEnumValue() {
        Object value1 = Utils.getRandomValue(Utils.DigestAlgorithm.class);
        Object value2 = Utils.getRandomValue(WeekDay.class);
        Object value3 = Utils.getRandomValue(Elements.class);

        assertTrue(value1 instanceof Utils.DigestAlgorithm);
        assertFalse(value2 instanceof Utils.DigestAlgorithm);
        assertFalse(value3 instanceof Utils.DigestAlgorithm);

        assertFalse(value1 instanceof WeekDay);
        assertTrue(value2 instanceof WeekDay);
        assertFalse(value3 instanceof WeekDay);

        assertFalse(value1 instanceof Elements);
        assertFalse(value2 instanceof Elements);
        assertTrue(value3 instanceof Elements);
    }


    @Test
    void testIsUUID() {
        assertFalse(Utils.isUUID(null));
        assertFalse(Utils.isUUID(""));
        assertFalse(Utils.isUUID(" "));
        assertTrue(Utils.isUUID(UUID.randomUUID()
            .toString()));
        assertTrue(Utils.isUUID("00000000-0000-0000-0000-000000000005"));
        assertFalse(Utils.isUUID("0000000o-0000-0000-0000-000000000005"));
        assertFalse(Utils.isUUID("00000000-00000000-0000-000000000005"));
        assertFalse(Utils.isUUID(Utils.getRandomString()));
    }


    @Test
    void testGetRandomLong() {
        Long longObject = Utils.getRandomLong();
        assertNotNull(longObject);
    }


    @Test
    void testGetRandomListOfLong() {
        List<Long> longList = Utils.getRandomListOfLong(2);
        assertNotNull(longList);
        assertEquals(2, longList.size());

        longList = Utils.getRandomListOfLong(0);
        assertNotNull(longList);
        assertFalse(longList.isEmpty());
    }


    @Test
    void testCapitalizeString() {
        assertNull(Utils.capitalizeString(null));
        assertEquals("", Utils.capitalizeString(""));
        assertEquals("", Utils.capitalizeString("   "));
        assertEquals("", Utils.capitalizeString("\n"));

        assertEquals("A", Utils.capitalizeString("a"));
        assertEquals("A", Utils.capitalizeString("A"));
        assertEquals("9", Utils.capitalizeString("9"));
        assertEquals("-", Utils.capitalizeString("-"));
        assertEquals("_", Utils.capitalizeString("_"));

        assertEquals("Otto", Utils.capitalizeString("otto"));
        assertEquals("The train was late.", Utils.capitalizeString("the train was late."));
    }


    @Test
    void testGetTitleCase() {
        assertNull(Utils.getTitleCase(null));
        assertEquals("", Utils.getTitleCase(""));
        assertEquals("", Utils.getTitleCase("   "));
        assertEquals("", Utils.getTitleCase("\n"));

        assertEquals("Otto", Utils.getTitleCase("otto"));
        assertEquals("Otto", Utils.getTitleCase("Otto"));
        assertEquals("Otto II.", Utils.getTitleCase("otto II."));
        assertEquals("Otto ii.", Utils.getTitleCase("otto ii."));
        assertEquals("Otto-Ii.", Utils.getTitleCase("otto-ii."));

        assertEquals("A-B-C", Utils.getTitleCase("A-b-c"));
        assertEquals("1_2_3", Utils.getTitleCase("1_2_3"));
        assertEquals("Xyz_123--AbC", Utils.getTitleCase("xyz_123--abC"));
        assertEquals("Snake_Case", Utils.getTitleCase("snake_case"));
        assertEquals("Number-Of-Donuts", Utils.getTitleCase("number-of-donuts"));
        assertEquals("Number_Of-Donuts", Utils.getTitleCase("number_of-donuts"));
        assertEquals("NUMBER_OF_DONUTS", Utils.getTitleCase("NUMBER_OF_DONUTS"));
        assertEquals("NumberOfDonuts ", Utils.getTitleCase("numberOfDonuts "));
        assertEquals(" NumberOfDonuts", Utils.getTitleCase(" numberOfDonuts"));
        assertEquals("    NumberOfDonuts", Utils.getTitleCase("    numberOfDonuts"));
        assertEquals("  NumberOfDonuts ", Utils.getTitleCase("  NumberOfDonuts "));

        assertEquals("The train was late.", Utils.getTitleCase("the train was late."));

        assertEquals("Number-Of-Donuts", Utils.getTitleCase("number-of-donuts", true));
        assertEquals("Number_Of-Donuts", Utils.getTitleCase("number_of-donuts", true));
        assertEquals("Number_Of_Donuts", Utils.getTitleCase("NUMBER_OF_DONUTS", true));
        assertEquals("Numberofdonuts ", Utils.getTitleCase("numberOfDonuts ", true));
    }


    @Test
    void testGetRandomUserData() {
        Utils.UserData userData = Utils.getRandomUserData();
        assertNotNull(userData);
        assertNotNull(userData.getGid());
        assertFalse(userData.getGid()
            .isBlank());
        assertNotNull(userData.getName());
        assertFalse(userData.getName()
            .isBlank());
        assertNotNull(userData.getEmail());
        assertFalse(userData.getEmail()
            .isBlank());

        assertTrue(Utils.isUUID(userData.getGid()));
        String[] names = userData.getName()
            .split(" ");
        assertEquals(2, names.length);
        assertEquals(String.format("%s.%s@example.org", names[0].toLowerCase(), names[1].toLowerCase()),
            userData.getEmail());
    }


    @RepeatedTest(17)
    void testGetRandomVersion() {
        String version = Utils.getRandomVersion();
        assertNotNull(version);
        assertFalse(version.isBlank());
        String[] parts = version.split("\\.");
        assertEquals(3, parts.length);
        Arrays.stream(parts)
            .forEach(s -> {
                int v = Integer.parseInt(s);
                assertTrue(v >= 0);
                assertTrue(v < 10);
            });
    }


    @Test
    void testReplaceTokens_TitleCaseExample() {
        assertThat(Utils.replaceTokens(
            "First 3 Capital Words! then 10 TLAs, I Found",
            Pattern.compile("(?<=^|[^A-Za-z])([A-Z][a-z]*)(?=[^A-Za-z]|$)"),
            match -> match.group(1)
                .toLowerCase())
        ).isEqualTo("first 3 capital words! then 10 TLAs, i found");
    }


    @Test
    void testReplaceTokens_EscapingSpecialCharacters() {
        Pattern regexCharacters = Pattern.compile("[<(\\[{\\\\^\\-=$!|\\]})?*+.>]");

        assertThat(Utils.replaceTokens(
            "A regex character like [",
            regexCharacters,
            match -> "\\" + match.group())
        ).isEqualTo("A regex character like \\[");
    }


    @Test
    void testReplaceTokens_ReplacingPlaceholders() {
        Map<String, String> placeholderValues = new HashMap<>();
        placeholderValues.put("name", "Bill");
        placeholderValues.put("company", "Baeldung");

        assertThat(Utils.replaceTokens(
            "Hi ${name} at ${company}",
            Pattern.compile("\\$\\{(?<placeholder>[A-Za-z0-9-_]+)}"),
            match -> placeholderValues.get(match.group("placeholder")))
        ).isEqualTo("Hi Bill at Baeldung");
    }


    @Test
    void testReplaceTokens_GeneratingGUIDs() {
        String input = Utils.getContentOfTextResource("/utils/template-stammgesetz-vorblatt.LDML.de-1.2.json");
        assertNotNull(input);
        assertFalse(input.isBlank());

        Pattern emptyGuidPattern = Pattern.compile("\"GUID\":\\s?\"\"");
        Pattern nonEmptyGuidPattern = Pattern.compile(
            "\"GUID\":\\s?\"([\\dabcdef]{8}\\-[\\dabcdef]{4}\\-[\\dabcdef]{4}\\-[\\dabcdef]{4}\\-[\\dabcdef]{12})\"");

        // assert that: input string has no guids
        List<String> noGuids = Utils.findAll(input, nonEmptyGuidPattern);
        assertNotNull(noGuids);
        assertEquals(0, noGuids.size());

        // act: replace guid placeholders with a random UUID value
        String output = Utils.replaceTokens(input, emptyGuidPattern, m -> generateGUID());
        assertNotNull(output);
        assertFalse(output.isBlank());

        // assert that: output string has empty guid placeholders replaces with UUID values
        List<String> guids = Utils.findAll(output, nonEmptyGuidPattern, 1);
        assertNotNull(guids);
        assertEquals(48, guids.size());

        guids.forEach(guid -> assertTrue(Utils.isUUID(guid)));

        // assert that: all UUIDs are different, i.e. no two GUIDs have the same value
        Set<String> setOfGuids = new HashSet<>(guids);
        assertEquals(guids.size(), setOfGuids.size());
    }


    @Test
    void testReplaceTokens_NotFound() {
        String text = Utils.getRandomString();
        assertEquals(text, Utils.replaceTokens(text, Pattern.compile("\\s"), m -> "_"));
    }


    @Test
    void testFindAll_NoMatch() {
        assertTrue(Utils.findAll("ABC", "\\s")
            .isEmpty());
    }


    @Test
    void testFindAll_TitleCaseExample() {
        String[] capitalWords = new String[]{"First", "Capital", "Words", "I", "Found"};
        String input = String.format("%s 3 %s %s! then 10 TLAs, %s %s", capitalWords[0], capitalWords[1],
            capitalWords[2], capitalWords[3], capitalWords[4]);
        Pattern pattern = Pattern.compile("(?<=^|[^A-Za-z])([A-Z][a-z]*)(?=[^A-Za-z]|$)");

        List<String> output = Utils.findAll(input, pattern);
        assertNotNull(output);
        assertEquals(5, output.size());
        IntStream.range(0, capitalWords.length)
            .forEach(i -> assertEquals(capitalWords[i], output.get(i)));
    }


    @Test
    void testFindAll_FindPlaceholders() {
        String patternString = "\\$\\{(?<placeholder>[A-Za-z0-9-_]+)}";
        String input = "Hi ${name} at ${company}";

        List<String> output = Utils.findAll(input, patternString, 1);
        assertNotNull(output);
        assertEquals(2, output.size());
        assertEquals("name", output.get(0));
        assertEquals("company", output.get(1));

        output = Utils.findAll(input, patternString);
        assertNotNull(output);
        assertEquals(2, output.size());
        assertEquals("${name}", output.get(0));
        assertEquals("${company}", output.get(1));
    }


    @Test
    void testPadList() {
        List<String> testList = Utils.pad(null, "X", 0);
        assertNotNull(testList);
        assertTrue(testList.isEmpty());

        testList = Utils.pad(List.of(), "X", 0);
        assertNotNull(testList);
        assertTrue(testList.isEmpty());

        testList = Utils.pad(null, "X", 1);
        assertNotNull(testList);
        assertEquals(List.of("X"), testList);

        testList = Utils.pad(List.of(), "X", 2);
        assertNotNull(testList);
        assertEquals(List.of("X", "X"), testList);

        testList = Utils.pad(List.of("A", "B", "C"), "", 5);
        assertNotNull(testList);
        assertEquals(List.of("A", "B", "C", "", ""), testList);

        List<Integer> intList = Utils.pad(List.of(1), 0, 0);
        assertNotNull(intList);
        assertEquals(List.of(1), intList);

        intList = Utils.pad(List.of(1), 72, 1);
        assertNotNull(intList);
        assertEquals(List.of(1), intList);

        intList = Utils.pad(List.of(1), 72, 11);
        assertNotNull(intList);
        assertEquals(List.of(1, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72), intList);
    }


    @Test
    void testRange() {
        assertEquals(List.of(), range(-1, true, 4, true));
        assertEquals(List.of(), range(12, false, 11, true));

        assertEquals(List.of(7), range(7, true, 7, true));
        assertEquals(List.of(7), range(7, true, 8, false));
        assertEquals(List.of(7), range(6, false, 7, true));
        assertEquals(List.of(7), range(6, false, 8, false));

        assertEquals(List.of(0, 1, 2, 3, 4, 5), range(0, true, 5, true));
        assertEquals(List.of(1, 2, 3, 4, 5), range(0, false, 5, true));
        assertEquals(List.of(0, 1, 2, 3, 4), range(0, true, 5, false));
        assertEquals(List.of(1, 2, 3, 4), range(0, false, 5, false));

        assertEquals(List.of(123, 124, 125), range(123, true, 125, true));
        assertEquals(List.of(124, 125), range(123, false, 125, true));
        assertEquals(List.of(123, 124), range(123, true, 125, false));
        assertEquals(List.of(124), range(123, false, 125, false));

        assertEquals(List.of(0, 1, 2), range(0, 3));
        assertEquals(List.of(1, 2), range(1, 3));
        assertEquals(List.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9), range(0, 10));
    }


    @Test
    void testGetFirstOrDefault() {
        assertNull(getFirstOrDefault(null, null));
        assertEquals("X", getFirstOrDefault(null, "X"));
        assertEquals(72, getFirstOrDefault(null, 72));

        assertNull(getFirstOrDefault(new ArrayList<>(), null));
        assertEquals("X", getFirstOrDefault(new HashMap<>().keySet(), "X"));
        assertEquals(72, getFirstOrDefault(new TreeSet<>(), 72));

        assertEquals('a', getFirstOrDefault(List.of('a'), 'x'));
        assertEquals('a', getFirstOrDefault(Arrays.asList('a', 'b', 'c'), 'x'));
        // TreeSet: sorted according to the natural ordering of its elements
        assertEquals(1, getFirstOrDefault(new TreeSet<>(List.of(3, 1, 4)), 0));
    }


    @Test
    void testGetElementAtIndex() {
        assertNull(getElementAtIndex(null, 0, false, null));
        assertEquals(8, getElementAtIndex(List.of(), 1, true, 8));
        assertNull(getElementAtIndex(List.of(1), -1, false, null));
        assertEquals("X", getElementAtIndex(List.of("X"), 0, false, null));
        assertNull(getElementAtIndex(List.of("X"), 1, false, null));
        assertNull(getElementAtIndex(List.of("X"), 1, true, null));
        assertEquals("Y", getElementAtIndex(List.of("X"), 1, true, "Y"));
    }


    @Test
    void testZipList() {
        assertEquals(List.of(), zipList(null, null, false, null, null));
        assertEquals(List.of(), zipList(List.of(1), null, true, null, 'a'));
        assertEquals(List.of(), zipList(null, List.of('a'), true, 1, null));

        assertEquals(List.of(), zipList(List.of(), List.of(), false, null, null));
        assertEquals(List.of(), zipList(List.of("Hi"), List.of(), false, null, null));
        assertEquals(List.of(), zipList(List.of(), List.of(BigDecimal.ZERO), false, null, null));

        assertEquals(List.of(Pair.of("Hi", null)),
            zipList(List.of("Hi"), List.of(), true, null, null));
        assertEquals(List.of(Pair.of("Hi", BigDecimal.ONE)),
            zipList(List.of("Hi"), List.of(), true, "Hello", BigDecimal.ONE));
        assertEquals(List.of(Pair.of(null, BigDecimal.ZERO)),
            zipList(List.of(), List.of(BigDecimal.ZERO), true, null, null));
        assertEquals(List.of(Pair.of("Hello", BigDecimal.ZERO)),
            zipList(List.of(), List.of(BigDecimal.ZERO), true, "Hello", BigDecimal.ONE));

        assertEquals(List.of(Pair.of("Hi", BigDecimal.ZERO)),
            zipList(List.of("Hi"), List.of(BigDecimal.ZERO), false, null, null));
    }


    @Test
    void testGetRandomIntBetween() {
        assertThrows(IllegalArgumentException.class, () -> getRandomIntBetween(0, -1));
        assertThrows(IllegalArgumentException.class, () -> getRandomIntBetween(2, 1));

        for (int i = 0; i < 20; i++) {
            int random = getRandomIntBetween(0, 3);
            assertTrue(0 <= random && random < 3);
        }
    }

    @Test
    void testXmlLinearize() {
        assertEquals("", xmlLinearize(null));
        assertEquals("", xmlLinearize(""));
        assertEquals("", xmlLinearize(" "));
        assertEquals("", xmlLinearize("\n"));

        String xml =
            "<test a=\"b\" x=\"y\">\n"
                + "    <c1>first child</c1>\n"
                + "    <c2>second <i>(following)</i> child</c2>\n"
                + "</test>\n";

        assertEquals("<test a=\"b\" x=\"y\"><c1>first child</c1><c2>second <i>(following)</i> child</c2></test>"
            , xmlLinearize(xml));
    }

    @Test
    void test_format_messages() {
        assertEquals("", message(""));
        assertEquals("", message("", "X"));

        assertEquals("X", message("X"));
        assertEquals("XY", message("X{0}", "Y"));
    }

    @ParameterizedTest
    @MethodSource("de.itzbund.egesetz.bmi.lea.core.util.UtilsTestDataProvider#provideTestTableData")
    void testWriteTable(List<List<String>> data, String expectedTable, int... padding) {
        String table = writeTable(data, padding);
        assertNotNull(table);
        assertFalse(table.isBlank());
        assertEquals(expectedTable, table);
    }

    @Test
    void testSetReflectiveAttribute() {
        MyTestObject myTestObject = new MyTestObject();
        assertNull(myTestObject.getMessage());

        setReflectiveAttribute(myTestObject, "message", Utils.getRandomString());
        assertNotNull(myTestObject.getMessage());
    }

    @Test
    void testPrintTable() {
        List<Pair<JSONObject, JSONObject>> pairs = List.of(
            Pair.of(
                makeNewDefaultJSONObjectWithText(ELEM_P, getRandomString()),
                makeNewDefaultJSONObjectWithText(ELEM_P, getRandomString())
            )
        );
        String table = printTable(pairs);
        assertFalse(isMissing(table));
    }


    @Test
    void testAreAllNull() {
        assertTrue(areAllNull());
        assertTrue(areAllNull((Object) null));
        assertTrue(areAllNull(null, null));
        assertTrue(areAllNull(null, null, null));

        assertFalse(areAllNull("", null, null));
        assertFalse(areAllNull((Object) new JSONObject()));
        assertFalse(areAllNull(1, 2, 3));
    }


    @Test
    void testCombineSets() {
        assertEquals(Set.of(), combineSets(null));
        assertEquals(Set.of(), combineSets());

        Set<String> stringSet1 = Set.of("A", "B", "C");
        Set<String> stringSet2 = new HashSet<>();

        assertEquals(Set.of(), combineSets(stringSet2));
        assertEquals(Set.of(), combineSets(stringSet2, stringSet2, stringSet2));
        assertEquals(Set.of("A", "B", "C"), combineSets(stringSet1));
        assertEquals(Set.of("A", "B", "C"), combineSets(stringSet1, stringSet2));
        assertEquals(Set.of("A", "B", "C"), combineSets(stringSet2, stringSet1));
        assertEquals(Set.of("A", "B", "C"), combineSets(stringSet1, stringSet1));

        Set<Integer> intSet1 = Set.of(1, 2, 3);
        Set<Integer> intSet2 = Set.of(2, 3);
        Set<Integer> intSet3 = Set.of(4);

        assertEquals(Set.of(4, 3, 2, 1), combineSets(intSet1, intSet2, intSet3));
    }


    @Test
    void testXOR() {
        assertTrue(xor(true));
        assertFalse(xor(false));
        assertFalse(xor(false, false));
        assertTrue(xor(false, true));
        assertTrue(xor(true, false));
        assertFalse(xor(true, true));
        assertFalse(xor(true, true, true));
        assertFalse(xor(false, false, false, false));
        assertTrue(xor(true, false, false));
        assertTrue(xor(false, false, false, true));
        assertFalse(xor(false, false, true, true));
    }


    private String generateGUID() {
        return String.format("\"GUID\":\"%s\"", UUID.randomUUID());
    }


    private enum WeekDay {
        MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
    }

    private enum Elements {
        EARTH, FIRE, WATER, AIR
    }

    @Data
    private static class MyTestObject {

        private String message;
        private MyTestObject child;

    }

}
