// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.util;

import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.regex.Pattern;

import static de.itzbund.egesetz.bmi.lea.core.util.Utils.getContent;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Log4j2
class TestResourceProviderTest {

    private static final Pattern PATT_AKN_ROOT = Pattern.compile("<(\\w+:)?akomaNtoso");

    private static final TestResourceProvider TR_PROVIDER = TestResourceProvider.getInstance();


    @Test
    void testGetAllXmlInputSources() {
        checkResult(TR_PROVIDER.getInputSources(), 135);
    }


    @Test
    void testGetAllAkn4unInputSources() {
        checkResult(TR_PROVIDER.getInputSources("akn4un"), 0);
    }


    @Test
    void testGetAllAkn4euAndAkn4deInputSources() {
        checkResult(TR_PROVIDER.getInputSources("akn4eu", "legalDocML.de-1.6"), 135);
    }


    @Test
    void testGetAllAkn4deRegelungstextInputSources() {
        checkResult(TR_PROVIDER.getInputSources((dir, name) -> name.contains("regelungstext.xml"), "legalDocML.de-1.6"), 32);
    }


    @Test
    void testGetSingleInputSource() {
        String testpath = "/ldml/legalDocML.de-1.6/01-01_Gesetz_Stammform_Entwurf/01-01_instanz_01/01-01_instanz_01_regelungstext.xml";
        ReusableInputSource src = TestResourceProvider.getInstance()
            .getSingleInputSource(testpath);
        assertNotNull(src);

        String content = getContent(src);
        checkAssertions(content);
    }


    private void checkResult(Iterator<ReusableInputSource> sources, int expectedCount) {
        int counter = 0;

        assertNotNull(sources);

        while (sources.hasNext()) {
            counter++;
            ReusableInputSource inputSource = sources.next();
            String content = getContent(inputSource);

            checkAssertions(content);
        }

        assertEquals(expectedCount, counter);
    }


    private void checkAssertions(String content) {
        assertFalse(content.isBlank());
        assertTrue(PATT_AKN_ROOT.matcher(content)
            .find());
    }

}
