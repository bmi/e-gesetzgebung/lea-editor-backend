// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.xml;

import de.itzbund.egesetz.bmi.lea.core.Constants;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.xml.XMLConstants;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Tests for {@link AppNamespaceContext}.
 */
class AppNamespaceContextTest {

    private static final List<String> prefixes = List.of("", "abc", "akn", "meta", "xsi");

    private static AppNamespaceContext namespaceContext;


    @BeforeAll
    static void setup() {
        namespaceContext = new AppNamespaceContext();
    }


    @Test
    void testGetNamespaceURI() {
        assertThrows(IllegalArgumentException.class, () -> namespaceContext.getNamespaceURI(null));

        List<String> uris = List.of(Constants.TARGET_NS, XMLConstants.NULL_NS_URI, Constants.TARGET_NS,
            XMLConstants.NULL_NS_URI, Constants.NS_SCHEMA_INSTANCE);
        IntStream.range(0, prefixes.size()).forEach(i ->
            assertEquals(uris.get(i), namespaceContext.getNamespaceURI(prefixes.get(i))));

        for (int i = 0; i < 17; i++) {
            assertEquals(XMLConstants.NULL_NS_URI, namespaceContext.getNamespaceURI(Utils.getRandomPrefix(1, 5)));
        }
    }


    @Test
    void testGetPrefix() {
        assertThrows(IllegalArgumentException.class, () -> namespaceContext.getPrefix(null));
        assertEquals("akn", namespaceContext.getPrefix(Constants.TARGET_NS));
        assertEquals("xsi", namespaceContext.getPrefix(Constants.NS_SCHEMA_INSTANCE));
        assertEquals(XMLConstants.DEFAULT_NS_PREFIX, namespaceContext.getPrefix(Utils.getRandomString()));
    }


    @Test
    void testGetPrefixes() {
        assertThrows(IllegalArgumentException.class, () -> namespaceContext.getPrefixes(null));
        assertEquals("akn", namespaceContext.getPrefixes(Constants.TARGET_NS).next());
        assertEquals("xsi", namespaceContext.getPrefixes(Constants.NS_SCHEMA_INSTANCE).next());
        assertEquals(XMLConstants.DEFAULT_NS_PREFIX, namespaceContext.getPrefixes(Utils.getRandomString()).next());
    }

}
