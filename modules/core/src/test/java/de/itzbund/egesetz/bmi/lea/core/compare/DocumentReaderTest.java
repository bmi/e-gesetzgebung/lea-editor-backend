// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Testsuite for {@link DocumentReader}.
 */
class DocumentReaderTest {

    @Test
    void testCounter() {
        DocumentReader.Counter counter = new DocumentReader.Counter();

        assertEquals(0, counter.getElemCount());
        assertEquals(0, counter.getTextCount());

        counter.setTextCount(1);
        counter.setElemCount(counter.getTextCount() + 1);

        assertEquals(2, counter.getElemCount());
        assertEquals(1, counter.getTextCount());
    }

}
