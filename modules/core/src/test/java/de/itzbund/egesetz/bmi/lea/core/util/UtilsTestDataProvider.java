// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.util;


import org.junit.jupiter.params.provider.Arguments;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Named.named;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class UtilsTestDataProvider {

    private UtilsTestDataProvider() {
    }

    private static Stream<Arguments> provideTestTableData() {
        return Stream.of(
            //@formatter:off
            arguments(named("1x1 Table w/o padding", List.of(
                List.of("X")
            )),   "+-+\n"
                + "|X|\n"
                + "+-+\n", new int[]{}),
            arguments(named("1x1 Table w/ uniform padding 1", List.of(
                List.of("X")
            )),   "+---+\n"
                + "|   |\n"
                + "| X |\n"
                + "|   |\n"
                + "+---+\n", new int[]{1}),
            arguments(named("1x1 Table w/ padding 1 2", List.of(
                List.of("X")
            )),   "+-----+\n"
                + "|     |\n"
                + "|  X  |\n"
                + "|     |\n"
                + "+-----+\n", new int[]{1, 2}),
            arguments(named("4x4 Table w/ padding 1 2 3 4", List.of(
                List.of("Alice"),
                List.of("Bob", "Charlie", "Daniel"),
                List.of("Elena", "Frank"),
                List.of("George", "Heidi", "Ina", "Yvonne")
            )),   "+------------+-------------+------------+------------+\n"
                + "|            |             |            |            |\n"
                + "|    Alice   |             |            |            |\n"
                + "|            |             |            |            |\n"
                + "|            |             |            |            |\n"
                + "|            |             |            |            |\n"
                + "+------------+-------------+------------+------------+\n"
                + "|            |             |            |            |\n"
                + "|    Bob     |    Charlie  |    Daniel  |            |\n"
                + "|            |             |            |            |\n"
                + "|            |             |            |            |\n"
                + "|            |             |            |            |\n"
                + "+------------+-------------+------------+------------+\n"
                + "|            |             |            |            |\n"
                + "|    Elena   |    Frank    |            |            |\n"
                + "|            |             |            |            |\n"
                + "|            |             |            |            |\n"
                + "|            |             |            |            |\n"
                + "+------------+-------------+------------+------------+\n"
                + "|            |             |            |            |\n"
                + "|    George  |    Heidi    |    Ina     |    Yvonne  |\n"
                + "|            |             |            |            |\n"
                + "|            |             |            |            |\n"
                + "|            |             |            |            |\n"
                + "+------------+-------------+------------+------------+\n", new int[]{1, 2, 3, 4})
            //@formatter:on
        );
    }

}
