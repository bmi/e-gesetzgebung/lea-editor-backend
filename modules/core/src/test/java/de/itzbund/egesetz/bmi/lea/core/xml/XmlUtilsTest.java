// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.xml;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class XmlUtilsTest {

    @Test
    void testGetNamespaceMappings() {
        assertEquals(List.of(), XmlUtils.getNamespaceMappings(null));
        assertEquals(List.of(), XmlUtils.getNamespaceMappings(""));
        assertEquals(List.of(), XmlUtils.getNamespaceMappings(" "));
        assertEquals(List.of(), XmlUtils.getNamespaceMappings("X"));
        assertEquals(List.of(Pair.of("X", "Y")), XmlUtils.getNamespaceMappings("X Y"));
        assertEquals(List.of(Pair.of("X", "Y")), XmlUtils.getNamespaceMappings("X \nY"));
        assertEquals(List.of(Pair.of("X", "Y")), XmlUtils.getNamespaceMappings("X Y Z"));

        String testSchemaLocation = "http://Metadaten.LegalDocML.de/1.6/ ../../../Grammatiken/legalDocML.de-metadaten.xsd "
            + "http://MetadatenBundestag.LegalDocML.de/1.6/ ../../../Grammatiken/legalDocML.de-metadaten-bundestag.xsd "
            + "http://Inhaltsdaten.LegalDocML.de/1.6/ ../../../Grammatiken/legalDocML.de-regelungstextentwurfsfassung.xsd";
        List<Pair<String, String>> expectedPairs = List.of(
            Pair.of("http://Metadaten.LegalDocML.de/1.6/", "../../../Grammatiken/legalDocML.de-metadaten.xsd"),
            Pair.of("http://MetadatenBundestag.LegalDocML.de/1.6/", "../../../Grammatiken/legalDocML.de-metadaten-bundestag.xsd"),
            Pair.of("http://Inhaltsdaten.LegalDocML.de/1.6/", "../../../Grammatiken/legalDocML.de-regelungstextentwurfsfassung.xsd")
        );
        assertEquals(expectedPairs, XmlUtils.getNamespaceMappings(testSchemaLocation));
    }

}
