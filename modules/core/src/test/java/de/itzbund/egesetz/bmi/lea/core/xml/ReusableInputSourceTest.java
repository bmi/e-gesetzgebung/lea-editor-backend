// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.xml;

import de.itzbund.egesetz.bmi.lea.core.util.TestResourceProvider;
import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static de.itzbund.egesetz.bmi.lea.core.util.Utils.getContent;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ReusableInputSourceTest {

    private static final String ENCODING = StandardCharsets.UTF_8.name();
    private static final String PUBLIC_ID = "publicId";
    private static final String SYSTEM_ID = "systemId";


    @Test
    void testReusableInputSourceMultipleRead() {
        String path = "/compare/test-document-01.xml";
        InputStream input = ReusableInputSourceTest.class.getResourceAsStream(path);
        assertNotNull(input);
        ReusableInputSource reusableInputSource = new ReusableInputSource(input);
        reusableInputSource.setEncoding(ENCODING);
        reusableInputSource.setPublicId(PUBLIC_ID);
        reusableInputSource.setSystemId(SYSTEM_ID);

        String content1 = getContent(reusableInputSource);
        String content2 = getContent(reusableInputSource);
        String content3 = getContent(reusableInputSource);

        assertEquals(content1, content2);
        assertEquals(content2, content3);
        assertEquals(content3, content1);

        assertEquals(ENCODING, reusableInputSource.getEncoding());
        assertEquals(PUBLIC_ID, reusableInputSource.getPublicId());
        assertEquals(SYSTEM_ID, reusableInputSource.getSystemId());
    }


    @Test
    void testGettersAndSetters() {
        ReusableInputSource reusableInputSource = new ReusableInputSource(null);
        assertNotNull(reusableInputSource);

        reusableInputSource.setEncoding(ENCODING);
        reusableInputSource.setPublicId(PUBLIC_ID);
        reusableInputSource.setSystemId(SYSTEM_ID);

        assertEquals(ENCODING, reusableInputSource.getEncoding());
        assertEquals(PUBLIC_ID, reusableInputSource.getPublicId());
        assertEquals(SYSTEM_ID, reusableInputSource.getSystemId());
    }


    @Test
    void testDataExtract() {
        ReusableInputSource risBG = TestResourceProvider.getInstance()
            .getSingleInputSource("/ldml/legalDocML.de-1.6/01-01_Gesetz_Stammform_Entwurf/01-01_instanz_01/01-01_instanz_01_begruendung.xml");
        XmlDocumentContentExtractor extractor = risBG.getExtractor();
        checkRisData(risBG, "begruendung");

        ReusableInputSource risRT = TestResourceProvider.getInstance()
            .getSingleInputSource("/ldml/legalDocML.de-1.6/01-01_Gesetz_Stammform_Entwurf/01-01_instanz_01/01-01_instanz_01_regelungstext.xml");
        checkRisData(risRT, "regelungstext");

        ReusableInputSource risVB = TestResourceProvider.getInstance()
            .getSingleInputSource("/ldml/legalDocML.de-1.6/01-01_Gesetz_Stammform_Entwurf/01-01_instanz_01/01-01_instanz_01_vorblatt.xml");
        checkRisData(risVB, "vorblatt");

        assertEquals(extractor, risBG.getExtractor());
    }


    private void checkRisData(ReusableInputSource ris, String docType) {
        assertNotNull(ris);
        assertNotNull(ris.getRisId());
        assertFalse(ris.getRisId()
            .isBlank());
        assertNotNull(ris.getExtractor());

        XmlDocumentContentExtractor extractor = ris.getExtractor();
        assertEquals(ris.getRisId(), extractor.getRisId());
        assertNotNull(extractor.getXsiSchemaLocation());
        assertFalse(extractor.getXsiSchemaLocation()
            .isBlank());
        assertNotNull(extractor.getDocumentType());
        assertEquals(docType, extractor.getDocumentType());

        assertNotNull(extractor.getMetadaten());
        assertEquals(docType, extractor.getMetadatum(LegalDocMLdeMetadatenFeld.META_ART));
        assertEquals("stammform", extractor.getMetadatum(LegalDocMLdeMetadatenFeld.META_FORM));
    }

}
