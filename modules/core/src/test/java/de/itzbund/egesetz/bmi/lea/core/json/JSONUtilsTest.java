// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.json;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import lombok.SneakyThrows;
import org.apache.commons.lang3.tuple.Pair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.xml.namespace.QName;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_EID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_GUID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_HREF;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_MARKER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_NAME;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_PLACEMENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_PLACEMENTBASE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_SHOWAS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.CHANGE_TRACK_ATTR_TYPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ARTICLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_AUTHORIALNOTE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_B;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BLOCK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DIV;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_HEADING;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_INLINE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_NUM;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_P;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PARAGRAPH;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TBLOCK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.IS_SUBSTITUTE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_CHILDREN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_ID_REF_GUID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_TEXT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_TYPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_TYPE_COMPONENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_TYPE_DOCUMENTREF;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_CHANGE_WRAPPER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_TEXT_WRAPPER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.META_BREG_TARGET_NS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.META_BT_TARGET_NS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.TARGET_NS;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addAttributes;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addDocumentRef;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.appendText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.areSameObjects;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.clearChildrenArray;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.cloneJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.findByEId;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.findChild;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.fromJSONArray;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDescendant;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDocumentObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDummy;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getEffectiveTextValue;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getFirstChild;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getGuid;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getHeadingOfElement;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getIndex;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getListOfGUIDs;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getListOfStringAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getLocalName;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getMarker;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getMetadata;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getNthChild;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getNumElement;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getParas;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getQName;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getRefGuid;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getStringAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getTextParagraphs;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getTextWrapper;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.hasContent;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.hasInlines;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.hasNoChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.hasOnlyText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.isEffectivelyEmpty;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.isElementOfType;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.isLegalNorm;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.isLegalParagraph;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.isOneOf;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.isSubstitute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.isTextWrapper;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewChangeWrapper;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewDefaultJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewDefaultJSONObjectWithText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewTextNode;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeSimpleTableObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeTBlock;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.mergeTextContent;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.prependText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.replaceChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.setGuid;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.setText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.splitAfter;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.toJSONArray;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.updateTextContent;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.withDefaultPrefix;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.wrapUp;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.getRandomString;
import static javax.xml.XMLConstants.DEFAULT_NS_PREFIX;
import static javax.xml.XMLConstants.NULL_NS_URI;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class JSONUtilsTest {

    private static final JSONParser JSON_PARSER = new JSONParser();

    private static final String TEST_JSON_MANY_CHILDREN = Utils.getContentOfTextResource(
        "/json/test-many-descendants.json");
    private static final String TEST_JSON_META_CONTAINER = Utils.getContentOfTextResource(
        "/json/test-meta-container.json");
    private static final String KEY_EXPECTED = "expected";

    @SneakyThrows
    @SuppressWarnings("unchecked")
    private static Stream<Arguments> getJSONSamples() {
        String jsonString = Utils.getContentOfTextResource("/json/sample-normalize-1.json");
        JSONArray jsonArray = (JSONArray) JSON_PARSER.parse(jsonString);
        assertNotNull(jsonArray);
        assertFalse(jsonArray.isEmpty());

        List<Arguments> args = new ArrayList<>();
        jsonArray.forEach(obj -> {
            if (obj instanceof JSONObject) {
                JSONObject jsonObject = (JSONObject) obj;
                int count = jsonObject.size();
                String expected = (String) jsonObject.get(KEY_EXPECTED);
                assertNotNull(expected);
                jsonObject.remove(KEY_EXPECTED);
                assertEquals(count - 1, jsonObject.size());
                args.add(Arguments.of(jsonObject, expected));
            } else {
                JSONArray ary = (JSONArray) obj;
                int count = ary.size();
                JSONObject exp = (JSONObject) ary.get(count - 1);
                String expected = (String) exp.get(KEY_EXPECTED);
                assertNotNull(expected);
                ary.remove(count - 1);
                assertEquals(count - 1, ary.size());
                args.add(Arguments.of(ary, expected));
            }
        });

        return args.stream();
    }


    @Test
    @SneakyThrows
    void testGetType_EmptyObject() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse("{}");
        assertNotNull(jsonObject);
        assertNull(getType(jsonObject));
    }


    @Test
    @SneakyThrows
    void testGetType_NoTypeAttribute() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse("{\"key\": \"value\"}");
        assertNotNull(jsonObject);
        assertNull(getType(jsonObject));
    }


    @Test
    @SneakyThrows
    void testGetType_FindTypeAttribute() {
        String expectedType = getRandomString();
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(String.format("{\"type\": \"%s\"}", expectedType));
        assertNotNull(jsonObject);
        assertEquals(expectedType, getType(jsonObject));
    }


    @Test
    @SneakyThrows
    void testGetFirstChild_NoChildren() {
        assertNull(getFirstChild(null));
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse("{\"key\": \"value\"}");
        assertNotNull(jsonObject);
        assertNull(getFirstChild(jsonObject));
    }


    @Test
    @SneakyThrows
    void testGetFirstChild_EmptyChildrenArray() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(
            "{"
                + "    \"type\": \"test\","
                + "    \"children\": []"
                + "}");
        assertNotNull(jsonObject);
        assertNull(getFirstChild(jsonObject));
    }


    @Test
    @SneakyThrows
    void testGetFirstChild_OneChild() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(
            "{"
                + "    \"type\": \"test\","
                + "    \"children\": ["
                + "        {"
                + "            \"type\": \"child\""
                + "        }"
                + "    ]"
                + "}");
        assertNotNull(jsonObject);

        JSONObject child = getFirstChild(jsonObject);
        assertNotNull(child);
        assertEquals("child", getType(child));
    }


    @Test
    @SneakyThrows
    void testGetFirstChild_SeveralChildren() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(
            "{"
                + "    \"type\": \"test\","
                + "    \"children\": ["
                + "        {"
                + "            \"type\": \"child1\""
                + "        },"
                + "        {"
                + "            \"type\": \"child2\""
                + "        },"
                + "        {"
                + "            \"type\": \"child3\""
                + "        }"
                + "    ]"
                + "}");
        assertNotNull(jsonObject);

        JSONObject child = getFirstChild(jsonObject);
        assertNotNull(child);
        assertEquals("child1", getType(child));
    }


    @Test
    @SneakyThrows
    void testFindChild_NoTypes() {
        assertNull(findChild(null));
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse("{\"key\": \"value\"}");
        assertNotNull(jsonObject);
        assertNull(findChild(jsonObject));
    }


    @Test
    @SneakyThrows
    void testFindChild_NoChildren() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse("{\"key\": \"value\"}");
        assertNotNull(jsonObject);
        assertNull(findChild(jsonObject, "child"));
    }


    @Test
    @SneakyThrows
    void testFindChild_EmptyChildrenArray() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(
            "{"
                + "    \"type\": \"test\","
                + "    \"children\": []"
                + "}");
        assertNotNull(jsonObject);
        assertNull(findChild(jsonObject, "child"));
    }


    @Test
    @SneakyThrows
    void testFindChild_OneChild_WrongType() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(
            "{"
                + "    \"type\": \"test\","
                + "    \"children\": ["
                + "        {"
                + "            \"type\": \"child-X\""
                + "        }"
                + "    ]"
                + "}");
        assertNotNull(jsonObject);
        assertNull(findChild(jsonObject, "child"));
    }


    @Test
    @SneakyThrows
    void testFindChild_OneChild_CorrectType() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(
            "{"
                + "    \"type\": \"test\","
                + "    \"children\": ["
                + "        {"
                + "            \"type\": \"child\""
                + "        }"
                + "    ]"
                + "}");
        assertNotNull(jsonObject);
        assertNotNull(findChild(jsonObject, "child"));
    }


    @Test
    @SneakyThrows
    void testFindChild_SeveralChildren_NotFound() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(
            "{"
                + "    \"type\": \"test\","
                + "    \"children\": ["
                + "        {"
                + "            \"type\": \"child1\""
                + "        },"
                + "        {"
                + "            \"type\": \"child2\""
                + "        },"
                + "        {"
                + "            \"type\": \"child3\""
                + "        }"
                + "    ]"
                + "}");
        assertNotNull(jsonObject);
        assertNull(findChild(jsonObject, "child"));
    }


    @Test
    @SneakyThrows
    void testFindChild_SeveralChildren_FoundExact() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(
            "{"
                + "    \"type\": \"test\","
                + "    \"children\": ["
                + "        {"
                + "            \"type\": \"child1\""
                + "        },"
                + "        {"
                + "            \"type\": \"child2\""
                + "        },"
                + "        {"
                + "            \"type\": \"child3\""
                + "        }"
                + "    ]"
                + "}");
        assertNotNull(jsonObject);
        assertNotNull(findChild(jsonObject, "child3"));
    }


    @Test
    @SneakyThrows
    void testFindChild_SeveralChildren_Found_1stAlternative() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(
            "{"
                + "    \"type\": \"test\","
                + "    \"children\": ["
                + "        {"
                + "            \"type\": \"child1\""
                + "        },"
                + "        {"
                + "            \"type\": \"child2\""
                + "        },"
                + "        {"
                + "            \"type\": \"child3\""
                + "        }"
                + "    ]"
                + "}");
        assertNotNull(jsonObject);
        assertNotNull(findChild(jsonObject, "child2", "child"));
    }


    @Test
    @SneakyThrows
    void testFindChild_SeveralChildren_Found_2ndAlternative() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(
            "{"
                + "    \"type\": \"test\","
                + "    \"children\": ["
                + "        {"
                + "            \"type\": \"child1\""
                + "        },"
                + "        {"
                + "            \"type\": \"child2\""
                + "        },"
                + "        {"
                + "            \"type\": \"child3\""
                + "        }"
                + "    ]"
                + "}");
        assertNotNull(jsonObject);
        assertNotNull(findChild(jsonObject, "child", "child2"));
    }


    @Test
    @SneakyThrows
    void testFindChild_UseArray_NotVarargs() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(
            "{"
                + "    \"type\": \"test\","
                + "    \"children\": ["
                + "        {"
                + "            \"type\": \"child1\""
                + "        },"
                + "        {"
                + "            \"type\": \"child2\""
                + "        },"
                + "        {"
                + "            \"type\": \"child3\""
                + "        }"
                + "    ]"
                + "}");
        assertNotNull(jsonObject);
        String[] childNames = new String[]{"child", "child2"};
        assertNotNull(findChild(jsonObject, childNames));
    }


    @Test
    @SneakyThrows
    void testGetMarker_NoChildOfTypeNum() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(
            "{"
                + "    \"type\": \"test\","
                + "    \"children\": ["
                + "        {"
                + "            \"type\": \"child\""
                + "        }"
                + "    ]"
                + "}");
        assertNotNull(jsonObject);
        assertNull(getMarker(jsonObject));
    }


    @Test
    @SneakyThrows
    void testGetMarker_NoMarker() {
        assertNull(getMarker(null));
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(
            "{"
                + "    \"type\": \"test\","
                + "    \"children\": ["
                + "        {"
                + "            \"type\": \"akn:num\","
                + "            \"children\": []"
                + "        }"
                + "    ]"
                + "}");
        assertNotNull(jsonObject);
        assertNull(getMarker(jsonObject));
    }


    @Test
    @SneakyThrows
    void testGetMarker_NoNameAttributeInMarker() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(
            "{"
                + "    \"type\": \"test\","
                + "    \"children\": ["
                + "        {"
                + "            \"type\": \"akn:num\","
                + "            \"children\": ["
                + "                {"
                + "                    \"type\": \"akn:marker\""
                + "                }"
                + "            ]"
                + "        }"
                + "    ]"
                + "}");
        assertNotNull(jsonObject);
        assertNull(getMarker(jsonObject));
    }


    @Test
    @SneakyThrows
    void testGetMarker_Found() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(
            "{"
                + "    \"type\": \"test\","
                + "    \"children\": ["
                + "        {"
                + "            \"type\": \"akn:num\","
                + "            \"children\": ["
                + "                {"
                + "                    \"type\": \"akn:marker\","
                + "                    \"name\": \"marker-name\""
                + "                }"
                + "            ]"
                + "        }"
                + "    ]"
                + "}");
        assertNotNull(jsonObject);
        String marker = getMarker(jsonObject);
        assertNotNull(marker);
        assertEquals("marker-name", marker);
    }


    @Test
    @SneakyThrows
    void testGetText() {
        assertNull(getText(null));
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse("{}");
        assertEquals("", getText(jsonObject));
        jsonObject = (JSONObject) JSON_PARSER.parse(String.format(
            "{\"%s\": [{"
                + "\"%s\": \"%s\","
                + "\"%s\": ["
                + "{\"%s\": \"ABC\"}"
                + "]}]}",
            JSON_KEY_CHILDREN,
            JSON_KEY_TYPE,
            JSON_VAL_TEXT_WRAPPER,
            JSON_KEY_CHILDREN,
            JSON_KEY_TEXT
        ));
        assertNotNull(jsonObject);
        assertEquals("ABC", getText(jsonObject));

        jsonObject = getFirstChild(jsonObject);
        assertNotNull(jsonObject);
        assertEquals("ABC", getText(jsonObject));

        jsonObject = makeNewDefaultJSONObject(ELEM_P);
        addChildren(jsonObject, makeNewTextNode("A"), makeNewTextNode("B"), makeNewTextNode("C"), makeNewTextNode(" DE"));
        assertEquals("ABC DE", getText(jsonObject));
    }


    @Test
    @SneakyThrows
    void testHasContent() {
        assertFalse(hasContent(null));
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse("{}");
        assertFalse(hasContent(jsonObject));
        jsonObject = (JSONObject) JSON_PARSER.parse(String.format("{\"%s\":\"\"}", JSON_KEY_TEXT));
        assertTrue(hasContent(jsonObject));
    }


    @Test
    void testGetLocalName() {
        assertNull(getLocalName(null));
        assertEquals("", getLocalName(""));
        assertEquals("", getLocalName("\t"));
        assertEquals("", getLocalName("x:"));
        assertEquals("y", getLocalName(":y"));
        assertEquals("z", getLocalName("y:z"));
    }


    @Test
    @SneakyThrows
    void testAddText() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse("{}");
        addText(jsonObject, "");
        JSONArray children = (JSONArray) jsonObject.get(JSON_KEY_CHILDREN);
        assertNotNull(children);
        assertEquals(1, children.size());
        assertTrue(hasOnlyText(jsonObject));
        assertEquals("", getText(jsonObject));

        addText(jsonObject, "Xyz");
        assertEquals(2, children.size());
    }


    @Test
    void testIsOneOf() {
        assertFalse(isOneOf(null, Set.of()));
        assertFalse(isOneOf("", Set.of()));
        assertFalse(isOneOf("x", null));
        assertFalse(isOneOf("x", Set.of("y")));

        assertTrue(isOneOf("x", Set.of("x")));
        assertTrue(isOneOf("x", Set.of("y", "x")));
        assertTrue(isOneOf("a:x", Set.of("x")));
        assertTrue(isOneOf("b:x", Set.of("y", "x")));
    }


    @Test
    @SuppressWarnings("unchecked")
    void testGetListOfStringAttribute() {
        JSONArray jsonArray = null;
        assertEquals(List.of(), getListOfStringAttribute(jsonArray, 0, ""));
        jsonArray = new JSONArray();
        assertEquals(List.of(), getListOfStringAttribute(jsonArray, 4, "x"));

        for (String value : List.of("A", "B", "C", "D", "E")) {
            JSONObject jsonObject = new JSONObject();
            String key = "BD".contains(value) ? "X" : "Y";
            jsonObject.put(key, value);
            jsonArray.add(jsonObject);
        }

        assertEquals(List.of("B", "D"), getListOfStringAttribute(jsonArray, 0, "X"));
        assertEquals(List.of("A", "C", "E"), getListOfStringAttribute(jsonArray, 0, "Y"));
        assertEquals(List.of(), getListOfStringAttribute(jsonArray, 0, "Z"));

        assertEquals(List.of("D"), getListOfStringAttribute(jsonArray, 2, "X"));
        assertEquals(List.of(), getListOfStringAttribute(jsonArray, 4, "X"));
        assertEquals(List.of("C", "E"), getListOfStringAttribute(jsonArray, 2, "Y"));
        assertEquals(List.of("E"), getListOfStringAttribute(jsonArray, 3, "Y"));
    }


    @Test
    @SuppressWarnings("unchecked")
    void testGetListOfGUIDs() {
        JSONArray testArray = new JSONArray();
        for (int i = 0; i < 5; i++) {
            if (i % 3 == 0) {
                testArray.add(new JSONObject());
            } else {
                testArray.add(makeNewDefaultJSONObject("X"));
            }
        }
        assertEquals(5, testArray.size());
        assertEquals(3, getListOfGUIDs(testArray).size());
        assertEquals(2, getListOfGUIDs(testArray, 2).size());
    }


    @Test
    void testGetChildren() {
        assertNull(getChildren(null));
        assertNull(getChildren(null, true));

        JSONObject jsonObject = new JSONObject();
        assertNull(getChildren(jsonObject));
        assertNotNull(getChildren(jsonObject, true));
    }


    @Test
    @SneakyThrows
    void testGetDescendant_NullObject() {
        assertNull(getDescendant(null, false, ""));
    }


    @Test
    @SneakyThrows
    void testGetDescendant_EmptyPath() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(TEST_JSON_MANY_CHILDREN);
        assertNotNull(jsonObject);
        assertEquals(jsonObject, getDescendant(jsonObject, false));
    }


    @Test
    @SneakyThrows
    void testGetDescendant_AnyChild() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(TEST_JSON_MANY_CHILDREN);
        assertNotNull(jsonObject);
        JSONObject child = getDescendant(jsonObject, false, "*");
        assertNotNull(child);
        assertEquals("level-1.1", getType(child));
    }


    @Test
    @SneakyThrows
    void testGetDescendant_Child() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(TEST_JSON_MANY_CHILDREN);
        assertNotNull(jsonObject);
        JSONObject child = getDescendant(jsonObject, false, "level-1.3");
        assertNotNull(child);
        assertEquals("level-1.3", getType(child));
    }


    @Test
    @SneakyThrows
    void testGetDescendant_AnyGrandChild() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(TEST_JSON_MANY_CHILDREN);
        assertNotNull(jsonObject);
        JSONObject child = getDescendant(jsonObject, false, "level-1.2", "*");
        assertNotNull(child);
        assertEquals("level-1.2.1", getType(child));
    }


    @Test
    @SneakyThrows
    void testGetDescendant_AnyGrandChild_NotFound() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(TEST_JSON_MANY_CHILDREN);
        assertNotNull(jsonObject);
        JSONObject child = getDescendant(jsonObject, false, "*", "*");
        assertNull(child);
    }


    @Test
    @SneakyThrows
    void testGetDescendant_GrandChild() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(TEST_JSON_MANY_CHILDREN);
        assertNotNull(jsonObject);
        JSONObject child = getDescendant(jsonObject, false, "level-1.2", "level-1.2.2");
        assertNotNull(child);
        assertEquals("level-1.2.2", getType(child));
    }


    @Test
    @SneakyThrows
    void testGetDescendant_FindMetaProprietary() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(TEST_JSON_META_CONTAINER);
        assertNotNull(jsonObject);
        JSONObject child = getDescendant(jsonObject, false, "*", "akn:meta", "akn:proprietary");
        assertNotNull(child);
        assertEquals("akn:proprietary", getType(child));
    }


    @Test
    @SneakyThrows
    void testGetDescendant_FindMetaInitiant() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(TEST_JSON_META_CONTAINER);
        assertNotNull(jsonObject);
        JSONObject child = getDescendant(jsonObject, false, "*", "akn:meta", "akn:proprietary",
            "meta:legalDocML.de_metadaten", "meta:initiant");
        assertNotNull(child);
        assertEquals("meta:initiant", getType(child));
        assertEquals("bundesregierung", getText(child));
    }


    @Test
    @SneakyThrows
    void testUpdateTextContent_NoCorrectStructure() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse("{\"type\": \"test\"}");
        assertNotNull(jsonObject);

        String before = jsonObject.toJSONString();
        updateTextContent(jsonObject, getRandomString());
        String after = jsonObject.toJSONString();
        assertEquals(before, after);
    }


    @Test
    @SneakyThrows
    void testUpdateTextContent_UpdateContent() {
        JSONObject jsonObject = (JSONObject) JSON_PARSER.parse(
            "{"
                + "    \"type\": \"test\","
                + "    \"children\": ["
                + "        {"
                + "            \"type\": \"lea:textWrapper\","
                + "            \"children\": ["
                + "                {"
                + "                    \"text\": \"test\""
                + "                }"
                + "            ]"
                + "        }"
                + "    ]"
                + "}");
        assertNotNull(jsonObject);
        assertEquals("test", getText(jsonObject));

        String updatedText = getRandomString();
        updateTextContent(jsonObject, updatedText);
        assertEquals(updatedText, getText(jsonObject));
    }


    @Test
    void testAddDocumentRef_NoCollectionBody() {
        assertThrows(NullPointerException.class, () -> addDocumentRef(null, "", "", 0));
    }


    @Test
    void testAddDocumentRef_NoCollectionBodyType() {
        JSONObject testObject = makeNewDefaultJSONObject(ELEM_P);
        JSONObject clonedObject = cloneJSONObject(testObject);
        addDocumentRef(testObject, "", "", 1);
        assertEquals(clonedObject, testObject);
    }


    @Test
    @SneakyThrows
    void testAddDocumentRef_EmptyCollectionBody_NoContent() {
        JSONObject collectionBody = (JSONObject) JSON_PARSER.parse(
            "{"
                + "    \"type\": \"akn:collectionBody\","
                + "    \"children\": [{\"children\": [{\"text\": \"\"}], \"type\": \"lea:textWrapper\"}]"
                + "}");

        String testPath = getRandomString();
        String testLiteral = getRandomString();
        addDocumentRef(collectionBody, testPath, testLiteral, 1);

        assertNotNull(getFirstChild(collectionBody));
        checkComponent(getFirstChild(collectionBody),
            "rdokhauptteil-1_tldokverweis-1", "rdokhauptteil-1_tldokverweis-1_verweis-1",
            testPath, testLiteral);
    }


    @Test
    @SneakyThrows
    void testAddDocumentRef_EmptyCollectionBody_OnlyText() {
        JSONObject collectionBody = (JSONObject) JSON_PARSER.parse(
            "{"
                + "    \"type\": \"akn:collectionBody\","
                + "    \"children\": ["
                + "        {"
                + "            \"children\": ["
                + "                {"
                + "                    \"text\": \"\""
                + "                }"
                + "            ],"
                + "            \"type\": \"lea:textWrapper\""
                + "        }"
                + "    ]"
                + "}");
        assertNotNull(getFirstChild(collectionBody));
        assertTrue(hasOnlyText(collectionBody));

        String testPath = getRandomString();
        String testLiteral = getRandomString();
        addDocumentRef(collectionBody, testPath, testLiteral, 7);

        assertNotNull(getFirstChild(collectionBody));
        checkComponent((JSONObject) getFirstChild(collectionBody),
            "rdokhauptteil-1_tldokverweis-7", "rdokhauptteil-1_tldokverweis-7_verweis-1",
            testPath, testLiteral);
    }


    @Test
    @SneakyThrows
    void testAddDocumentRef_NonEmptyCollectionBody() {
        JSONObject collectionBody = (JSONObject) JSON_PARSER.parse(
            "{"
                + "    \"type\": \"akn:collectionBody\","
                + "    \"children\": ["
                + "        {"
                + "            \"type\": \"akn:component\","
                + "            \"GUID\": \"45f44259-4374-4a0e-8a0a-f75230d015e5\","
                + "            \"eId\": \"tldokverweis-1\","
                + "            \"children\": ["
                + "                {"
                + "                    \"type\": \"akn:documentRef\","
                + "                    \"GUID\": \"e4337c7a-c287-4d00-bca5-c61698928deb\","
                + "                    \"eId\": \"tldokverweis-1_verweis-1\","
                + "                    \"href\": \"doc1.xml\","
                + "                    \"showAs\": \"doc1\","
                + "                    \"children\": [{\"children\": [{\"text\": \"\"}], \"type\": "
                + "\"lea:textWrapper\"}]"
                + "                }"
                + "            ]"
                + "        },"
                + "        {"
                + "            \"type\": \"akn:component\","
                + "            \"GUID\": \"ce2e000f-b4c2-4cb4-b7a6-25a844cc00f3\","
                + "            \"eId\": \"tldokverweis-2\","
                + "            \"children\": ["
                + "                {"
                + "                    \"type\": \"akn:documentRef\","
                + "                    \"GUID\": \"4e2f9968-4971-4713-b47a-716c91a60656\","
                + "                    \"eId\": \"tldokverweis-2_verweis-1\","
                + "                    \"href\": \"doc2.xml\","
                + "                    \"showAs\": \"doc2\","
                + "                    \"children\": [{\"children\": [{\"text\": \"\"}], \"type\": "
                + "\"lea:textWrapper\"}]"
                + "                }"
                + "            ]"
                + "        }"
                + "    ]"
                + "}");

        JSONArray children = (JSONArray) collectionBody.get(JSON_KEY_CHILDREN);
        assertEquals(2, children.size());

        addDocumentRef(collectionBody, "", "", 3);
        assertEquals(3, children.size());
    }

    private void checkComponent(JSONObject component, String componentEId, String docRefEId, String href,
        String literal) {
        assertEquals(JSON_TYPE_COMPONENT, getType(component));
        assertEquals(componentEId, component.get(ATTR_EID));
        assertTrue(Utils.isUUID((String) component.get(ATTR_GUID)));

        JSONArray children = (JSONArray) component.get(JSON_KEY_CHILDREN);
        assertEquals(1, children.size());

        JSONObject docRef = (JSONObject) children.get(0);
        assertEquals(JSON_TYPE_DOCUMENTREF, getType(docRef));
        assertEquals(docRefEId, docRef.get(ATTR_EID));
        assertTrue(Utils.isUUID((String) docRef.get(ATTR_GUID)));
        assertEquals(href, docRef.get(ATTR_HREF));
        assertEquals(literal, docRef.get(ATTR_SHOWAS));
    }


    @Test
    @SneakyThrows
    void testGetEffectiveTextValue_Objects() {
        assertEquals("", JSONUtils.getEffectiveTextValue(null));
        assertEquals("", JSONUtils.getEffectiveTextValue(JSON_PARSER.parse("[]")));
        assertEquals("", JSONUtils.getEffectiveTextValue(JSON_PARSER.parse("{}")));
        assertEquals("", JSONUtils.getEffectiveTextValue(JSON_PARSER.parse("[{}]")));
        assertEquals("", JSONUtils.getEffectiveTextValue(JSON_PARSER.parse("{\"key\":\"\"}")));
        assertEquals("", JSONUtils.getEffectiveTextValue(JSON_PARSER.parse("{\"text\":\"\"}")));
        assertEquals("", JSONUtils.getEffectiveTextValue(JSON_PARSER.parse("{\"text\":\"  \"}")));
        assertEquals("", JSONUtils.getEffectiveTextValue(List.of()));
        assertEquals("", JSONUtils.getEffectiveTextValue(BigDecimal.valueOf(7)));
    }


    @ParameterizedTest
    @MethodSource("getJSONSamples")
    @SneakyThrows
    void testGetEffectiveTextValue_Samples(Object jsonSample, String expectedText) {
        assertEquals(expectedText, JSONUtils.getEffectiveTextValue(jsonSample));
    }


    @Test
    @SneakyThrows
    void testGetEffectiveTextValue_Document() {
        String jsonString = Utils.getContentOfTextResource("/json/sample-normalize-2.json");
        Object object = JSON_PARSER.parse(jsonString);
        assertNotNull(object);

        String expectedText = Utils.getContentOfTextResource("/json/document-expected-text.txt");
        assertNotNull(expectedText);
        assertFalse(expectedText.isBlank());
        String actualText = JSONUtils.getEffectiveTextValue(object);
        assertEquals(expectedText, actualText);
    }


    @Test
    @SuppressWarnings("unchecked")
    void testGetEffectiveTextValue_IgnoreFootnotes() {
        String textFragment1 = getRandomString();
        String textFragment2 = getRandomString();
        JSONObject testObject = makeNewDefaultJSONObject(ELEM_P);
        JSONObject preText = makeNewTextNode(textFragment1);
        JSONObject afterText = makeNewTextNode(textFragment2);
        JSONObject footnote = makeNewDefaultJSONObject(ELEM_AUTHORIALNOTE);
        addAttributes(footnote,
            Pair.of(ATTR_MARKER, "1"),
            Pair.of(ATTR_PLACEMENT, "bottom"),
            Pair.of(ATTR_PLACEMENTBASE, ""));
        JSONObject footnoteContent = makeNewDefaultJSONObjectWithText(ELEM_P, getRandomString());
        addChildren(footnote, footnoteContent);
        addChildren(testObject, preText, footnote, afterText);

        String normalizedText = getEffectiveTextValue(testObject);

        assertEquals(textFragment1 + " " + textFragment2, normalizedText);
    }


    @Test
    @SuppressWarnings("unchecked")
    void testHasInlines() {
        assertFalse(hasInlines(null));
        JSONArray jsonArray = new JSONArray();
        assertFalse(hasInlines(jsonArray));

        JSONObject text1 = makeNewTextNode("text1");
        jsonArray.add(text1);
        assertFalse(hasInlines(jsonArray));

        JSONObject text2 = new JSONObject();
        addText(text2, "text2");
        jsonArray.add(text2);
        assertTrue(hasInlines(jsonArray));
    }


    @Test
    void textAddChildren() {
        JSONObject parent = new JSONObject();
        assertNull(getChildren(parent));

        addChildren(parent);
        assertNotNull(getChildren(parent));
        JSONArray children = getChildren(parent);
        assertEquals(0, children.size());

        addChildren(parent, new JSONObject(), new JSONObject());
        assertEquals(2, children.size());
    }


    @Test
    void testGetDocumentObject() {
        assertNull(getDocumentObject(""));
        assertNull(getDocumentObject("[]"));
        assertNotNull(getDocumentObject("{}"));
        assertNotNull(getDocumentObject("[{}]"));
    }


    @Test
    void testMakeNewJSONObject_NakedObject() {
        JSONObject testObject = makeNewJSONObject(null, false, false);
        assertNotNull(testObject);
        assertTrue(testObject.isEmpty());

        testObject = JSONUtils.makeNewJSONObject("", false, false);
        assertNotNull(testObject);
        assertTrue(testObject.isEmpty());

        testObject = JSONUtils.makeNewJSONObject("", false, true);
        assertNotNull(testObject);
        assertEquals(1, testObject.size());
    }


    @Test
    void testMakeNewJSONObject_TypedObject() {
        JSONObject testObject = makeNewJSONObject("X", false, false);
        assertNotNull(testObject);
        assertEquals(1, testObject.size());
        assertEquals("X", getType(testObject));

        testObject = makeNewJSONObject("p", true, false);
        assertNotNull(testObject);
        assertEquals(1, testObject.size());
        assertEquals("akn:p", getType(testObject));

        testObject = makeNewJSONObject(ELEM_BODY, true, true);
        assertNotNull(testObject);
        assertEquals(2, testObject.size());
        assertEquals("akn:body", getType(testObject));
    }


    @Test
    void testMakeNewJSONObject_TypedObjectWithGUID() {
        JSONObject testObject1 = makeNewJSONObject("X", false, true);
        assertNotNull(testObject1);
        assertEquals(2, testObject1.size());
        JSONObject testObject2 = makeNewJSONObject("X", false, true);
        assertNotNull(testObject2);
        assertEquals(2, testObject2.size());

        assertEquals(getType(testObject1), getType(testObject2));
        assertNotEquals(getGuid(testObject1), getGuid(testObject2));
    }


    @Test
    void testMakeNewDefaultJSONObject() {
        JSONObject testObject = makeNewDefaultJSONObject("akn:block");
        assertNotNull(testObject);
        assertEquals(2, testObject.size());
        assertEquals("akn:block", getType(testObject));
    }


    @Test
    void testMakeSimpleTableObject_NoCaption_NoData() {
        JSONObject testTable1 = makeSimpleTableObject(null, null);
        assertNotNull(testTable1);
        assertEquals(3, testTable1.size());
        assertEquals("akn:table", getType(testTable1));
        JSONArray children = getChildren(testTable1);
        assertNotNull(children);
        assertEquals(1, children.size());
        assertEquals("akn:tr", getType((JSONObject) children.get(0)));

        JSONObject testTable2 = makeSimpleTableObject("", List.of());
        assertNotNull(testTable2);
        assertEquals(3, testTable2.size());
        children = getChildren(testTable2);
        assertNotNull(children);
        assertEquals(1, children.size());
    }


    @Test
    void testMakeSimpleTableObject_WithCaption_NoData() {
        String testCaption = getRandomString();
        JSONObject testTable = makeSimpleTableObject(testCaption, List.of(List.of()));
        assertNotNull(testTable);
        assertEquals(3, testTable.size());

        JSONArray children = getChildren(testTable);
        assertNotNull(children);
        assertEquals(2, children.size());

        JSONObject child1 = (JSONObject) children.get(0);
        assertEquals("akn:caption", getType(child1));
        assertEquals(testCaption, getText(child1));

        JSONObject child2 = (JSONObject) children.get(1);
        assertEquals("akn:tr", getType(child2));

        JSONArray cells = getChildren(child2);
        assertNotNull(cells);
        assertEquals(1, cells.size());
        assertEquals("", getText((JSONObject) cells.get(0)));
    }


    @Test
    void testMakeSimpleTableObject_WithCaption_WithData() {
        String testCaption = getRandomString();
        List<List<String>> testData = List.of(
            List.of(""),
            List.of(getRandomString(), getRandomString(), getRandomString()),
            List.of(),
            List.of(getRandomString()),
            List.of(getRandomString(), getRandomString(), getRandomString())
        );

        JSONObject testTable = makeSimpleTableObject(testCaption, testData);
        assertNotNull(testTable);

        JSONArray children = getChildren(testTable);
        assertEquals(6, children.size());
        JSONObject caption = (JSONObject) children.get(0);
        assertEquals(testCaption, getText(caption));

        List<?> rows = children.subList(1, children.size());
        assertNotNull(rows);
        assertEquals(5, rows.size());

        JSONObject row1 = (JSONObject) rows.get(0);
        JSONArray cells1 = getChildren(row1);
        assertEquals(3, cells1.size());
        assertEquals("", getEffectiveTextValue(cells1.get(0)));
        assertEquals("", getEffectiveTextValue(cells1.get(1)));
        assertEquals("", getEffectiveTextValue(cells1.get(2)));

        JSONObject row2 = (JSONObject) rows.get(1);
        JSONArray cells2 = getChildren(row2);
        assertEquals(3, cells2.size());
        assertEquals(testData.get(1)
            .get(0), getEffectiveTextValue(cells2.get(0)));
        assertEquals(testData.get(1)
            .get(1), getEffectiveTextValue(cells2.get(1)));
        assertEquals(testData.get(1)
            .get(2), getEffectiveTextValue(cells2.get(2)));

        JSONObject row3 = (JSONObject) rows.get(2);
        JSONArray cells3 = getChildren(row3);
        assertEquals(3, cells3.size());
        assertEquals("", getEffectiveTextValue(cells3.get(0)));
        assertEquals("", getEffectiveTextValue(cells3.get(1)));
        assertEquals("", getEffectiveTextValue(cells3.get(2)));

        JSONObject row4 = (JSONObject) rows.get(3);
        JSONArray cells4 = getChildren(row4);
        assertEquals(3, cells4.size());
        assertEquals(testData.get(3)
            .get(0), getEffectiveTextValue(cells4.get(0)));
        assertEquals("", getEffectiveTextValue(cells4.get(1)));
        assertEquals("", getEffectiveTextValue(cells4.get(2)));

        JSONObject row5 = (JSONObject) rows.get(4);
        JSONArray cells5 = getChildren(row5);
        assertEquals(3, cells5.size());
        assertEquals(testData.get(4)
            .get(0), getEffectiveTextValue(cells5.get(0)));
        assertEquals(testData.get(4)
            .get(1), getEffectiveTextValue(cells5.get(1)));
        assertEquals(testData.get(4)
            .get(2), getEffectiveTextValue(cells5.get(2)));
    }


    @Test
    void testGetMetadata() {
        String jsonContent = Utils.getContentOfTextResource(
            "/utils/template-stammgesetz-vorblatt.LDML.de-1.2.json");
        JSONObject document = getDocumentObject(jsonContent);
        assertNotNull(document);

        assertEquals("gesetz", getMetadata(document, "meta:typ"));
        assertEquals("entwurfsfassung", getMetadata(document, "meta:fassung"));
        assertEquals("bundesregierung", getMetadata(document, "meta:initiant"));
        assertEquals("stammform", getMetadata(document, "meta:form"));
    }


    @Test
    @SuppressWarnings("unchecked")
    void testGetTextParagraphs() {
        List<String> paras = List.of();
        assertEquals(List.of(), getTextParagraphs(paras));

        paras = Arrays.asList(
            "Text A",
            "",
            null,
            "Text B",
            "\t"
        );
        List<JSONObject> paraObjects = getTextParagraphs(paras);
        assertNotNull(paraObjects);
        assertEquals(2, paraObjects.size());
        JSONArray ps = new JSONArray();
        ps.addAll(paraObjects);
        assertEquals(List.of("akn:p", "akn:p"), getListOfStringAttribute(ps, 0, JSON_KEY_TYPE));

        assertEquals("Text A", getText(paraObjects.get(0)));
        assertFalse(Utils.isMissing(getGuid(paraObjects.get(0))));
        assertEquals("Text B", getText(paraObjects.get(1)));
        assertFalse(Utils.isMissing(getGuid(paraObjects.get(1))));
    }


    @Test
    @SuppressWarnings("unchecked")
    void testMakeTBlock() {
        String heading = "A. VI. 4. a) Erfüllungsaufwand für Bürgerinnen und Bürger";
        String textA = getRandomString(50, Utils.RANDOM_STRING_MAX_LENGTH);
        String textB = getRandomString() + " \n" + getRandomString();
        JSONObject testTBlock = makeTBlock(heading, List.of(textA, textB));
        assertNotNull(testTBlock);
        assertEquals("akn:tblock", getType(testTBlock));
        assertFalse(Utils.isMissing(getGuid(testTBlock)));
        assertEquals(3, testTBlock.size());
        JSONArray tblockChildren = getChildren(testTBlock);
        assertEquals(4, tblockChildren.size());

        JSONObject testNum = (JSONObject) tblockChildren.get(0);
        assertEquals("akn:num", getType(testNum));
        assertFalse(Utils.isMissing(getGuid(testTBlock)));
        JSONArray numChildren = getChildren(testNum);
        assertEquals(2, numChildren.size());
        JSONObject marker = (JSONObject) numChildren.get(0);
        assertEquals("akn:marker", getType(marker));
        assertTrue(Utils.isMissing(getGuid(marker)));
        assertEquals("a", getStringAttribute(marker, ATTR_NAME));

        JSONObject testHeading = (JSONObject) tblockChildren.get(1);
        assertEquals("akn:heading", getType(testHeading));
        assertFalse(Utils.isMissing(getGuid(testTBlock)));
        assertEquals("Erfüllungsaufwand für Bürgerinnen und Bürger", getText(testHeading));

        JSONObject testPara1 = (JSONObject) tblockChildren.get(2);
        assertEquals("akn:p", getType(testPara1));
        assertFalse(Utils.isMissing(getGuid(testTBlock)));
        assertEquals(textA, getText(testPara1));

        JSONObject testPara2 = (JSONObject) tblockChildren.get(3);
        assertEquals("akn:p", getType(testPara2));
        assertFalse(Utils.isMissing(getGuid(testTBlock)));
        assertEquals(textB, getText(testPara2));
    }


    @Test
    void testWrapUp() {
        assertEquals(new JSONArray(), wrapUp(null));

        JSONObject document = new JSONObject();
        JSONArray wrappedDocument = wrapUp(document);
        assertNotNull(wrappedDocument);
        assertEquals(1, wrappedDocument.size());
        assertEquals(document, wrappedDocument.get(0));
    }


    @Test
    @SuppressWarnings("unchecked")
    void testAddAttribute() {
        JSONObject jsonObject = new JSONObject();
        assertTrue(jsonObject.isEmpty());
        Pair<String, String> attribute = Pair.of("key", "value");
        addAttribute(jsonObject, attribute);
        assertFalse(jsonObject.isEmpty());
        assertEquals(1, jsonObject.size());
        assertTrue(jsonObject.containsKey("key"));
        assertEquals("value", jsonObject.get("key"));

        JSONObject anotherObject = makeNewJSONObject(null, false, false);
        assertTrue(anotherObject.isEmpty());
        addAttributes(anotherObject);
        assertTrue(anotherObject.isEmpty());
        addAttributes(anotherObject,
            Pair.of("One", "1"));
        assertEquals(1, anotherObject.size());
        addAttributes(anotherObject,
            Pair.of("Two", "2"),
            Pair.of("Three", "3"));
        assertEquals(3, anotherObject.size());
        assertEquals(Set.of("One", "Two", "Three"), anotherObject.keySet());
        assertEquals(List.of("1", "2", "3"), new ArrayList<String>(anotherObject.values()));
    }


    @Test
    @SuppressWarnings("unchecked")
    void testSplitAfter() {
        assertEquals(List.of(), splitAfter(null, null));
        assertEquals(List.of(), splitAfter(null, ELEM_P));

        assertEquals(List.of(), splitAfter(new JSONArray(), null));
        assertEquals(List.of(new JSONArray()), splitAfter(new JSONArray(), ELEM_P));

        JSONArray testJsonArray = new JSONArray();
        testJsonArray.add(makeNewDefaultJSONObject(ELEM_P));

        assertEquals(List.of(testJsonArray), splitAfter(testJsonArray, ELEM_DIV));
        assertEquals(List.of(testJsonArray), splitAfter(testJsonArray, ELEM_P));

        testJsonArray.add(makeNewDefaultJSONObject(ELEM_P));
        assertEquals(List.of(testJsonArray), splitAfter(testJsonArray, ELEM_B));
        assertEquals(List.of(
                toJSONArray(List.of((JSONObject) testJsonArray.get(0))),
                toJSONArray(List.of((JSONObject) testJsonArray.get(1))))
            , splitAfter(testJsonArray, ELEM_P));

        testJsonArray.add(makeNewDefaultJSONObject(ELEM_B));
        List<JSONArray> arrays = splitAfter(testJsonArray, ELEM_B);
        assertEquals(List.of(testJsonArray), arrays);

        testJsonArray.add(makeNewDefaultJSONObject(ELEM_B));
        assertEquals(4, testJsonArray.size());
        arrays = splitAfter(testJsonArray, ELEM_B);
        assertEquals(2, arrays.size());
        assertEquals(3, arrays.get(0)
            .size());
        assertEquals(1, arrays.get(1)
            .size());

        // p, p, b,b, p
        testJsonArray.add(makeNewDefaultJSONObject(ELEM_P));
        assertEquals(5, testJsonArray.size());
        arrays = splitAfter(testJsonArray, ELEM_B);
        assertEquals(3, arrays.size());
        assertEquals(3, arrays.get(0)
            .size());
        assertEquals(1, arrays.get(1)
            .size());
        assertEquals(1, arrays.get(2)
            .size());

        // p, p, b,b, p
        assertEquals(5, testJsonArray.size());
        arrays = splitAfter(testJsonArray, ELEM_P);
        assertEquals(3, arrays.size());
        assertEquals(1, arrays.get(0)
            .size());
        assertEquals(1, arrays.get(1)
            .size());
        assertEquals(3, arrays.get(2)
            .size());
    }


    @Test
    @SuppressWarnings("unchecked")
    void testFromAndToJSONArray() {
        assertNull(toJSONArray(null));
        assertEquals(new JSONArray(), toJSONArray(List.of()));

        assertNull(fromJSONArray(null));
        assertEquals(List.of(), fromJSONArray(new JSONArray()));

        JSONObject jsonObject1 = makeNewDefaultJSONObject(ELEM_P);
        JSONObject jsonObject2 = makeNewDefaultJSONObject(ELEM_BODY);
        JSONObject jsonObject3 = makeNewDefaultJSONObject(ELEM_AUTHORIALNOTE);

        List<JSONObject> list = List.of(jsonObject1, jsonObject2, jsonObject3);
        JSONArray array = new JSONArray();
        array.addAll(list);
        assertEquals(list.size(), array.size());

        assertEquals(list, fromJSONArray(toJSONArray(list)));
        assertEquals(array, toJSONArray(fromJSONArray(array)));
    }


    @Test
    void testGetTextWrapper() {
        JSONObject jsonObject = getTextWrapper("TEST");
        assertNotNull(jsonObject);
        assertEquals(JSON_VAL_TEXT_WRAPPER, getType(jsonObject));
        assertEquals("TEST", getEffectiveTextValue(jsonObject));
    }


    @Test
    void testClearChildrenArray() {
        JSONObject jsonObject = null;
        clearChildrenArray(jsonObject);
        assertNull(jsonObject);

        jsonObject = new JSONObject();
        clearChildrenArray(jsonObject);
        assertEquals(new JSONObject(), jsonObject);

        addText(jsonObject, "TEST");
        assertEquals(1, getChildren(jsonObject).size());
        clearChildrenArray(jsonObject);
        assertEquals(0, getChildren(jsonObject).size());
    }


    @Test
    void testGetNumElement() {
        assertNull(getNumElement(null));
        assertNull(getNumElement(new JSONObject()));

        JSONObject parent = makeNewDefaultJSONObject(ELEM_TBLOCK);
        assertNull(getNumElement(parent));

        addText(parent, "TEXT");
        assertNull(getNumElement(parent));

        clearChildrenArray(parent);
        JSONObject p = makeNewDefaultJSONObject(ELEM_P);
        addChildren(parent, p);
        assertNull(getNumElement(parent));

        clearChildrenArray(parent);
        JSONObject num = makeNewDefaultJSONObject(ELEM_NUM);
        addChildren(parent, num);
        assertEquals(num, getNumElement(parent));

        addChildren(parent, p);
        assertEquals(num, getNumElement(parent));

        assertNull(getHeadingOfElement(parent));
        addChildren(parent, makeNewDefaultJSONObject(ELEM_HEADING));
        assertNotNull(getHeadingOfElement(parent));
    }


    @Test
    void testIsElementOfType() {
        JSONObject p = makeNewDefaultJSONObject(ELEM_P);
        JSONObject art = makeNewDefaultJSONObject(ELEM_ARTICLE);
        JSONObject para = makeNewDefaultJSONObject(ELEM_PARAGRAPH);

        assertTrue(isElementOfType(p, ELEM_P));
        assertTrue(isLegalNorm(art));
        assertTrue(isLegalParagraph(para));

        assertFalse(isLegalParagraph(art));
        assertFalse(isLegalNorm(para));
        assertFalse(isElementOfType(p, ELEM_PARAGRAPH));
    }


    @Test
    void testGetParas() {
        assertEquals(List.of(), getParas(null));
        assertEquals(List.of(), getParas(new JSONObject()));

        JSONObject jsonObject = makeNewDefaultJSONObject(ELEM_P);
        assertEquals(List.of(), getParas(jsonObject));

        jsonObject = makeNewDefaultJSONObject(ELEM_ARTICLE);
        assertEquals(List.of(), getParas(jsonObject));

        addText(jsonObject, "");
        assertEquals(List.of(), getParas(jsonObject));

        clearChildrenArray(jsonObject);
        JSONObject numObject = makeNewDefaultJSONObject(ELEM_NUM);
        addChildren(jsonObject, numObject);
        assertEquals(List.of(), getParas(jsonObject));

        addChildren(jsonObject,
            makeNewDefaultJSONObject(ELEM_PARAGRAPH),
            makeNewDefaultJSONObject(ELEM_PARAGRAPH));
        assertEquals(3, getChildren(jsonObject).size());
        assertEquals(2, getParas(jsonObject).size());
    }


    @Test
    void testIsSubstitute() {
        assertFalse(isSubstitute(null));
        assertFalse(isSubstitute(new JSONObject()));

        JSONObject jsonObject = makeNewDefaultJSONObject(ELEM_P);
        assertFalse(isSubstitute(jsonObject));

        addAttribute(jsonObject, Pair.of(IS_SUBSTITUTE, "true"));
        assertTrue(isSubstitute(jsonObject));
    }


    @Test
    void testWithDefaultPrefix() {
        assertEquals("akn:p", withDefaultPrefix(ELEM_P));
        assertEquals("akn:p", withDefaultPrefix("akn:" + ELEM_P));
        assertEquals("akn:div", withDefaultPrefix(ELEM_DIV));
        assertEquals("akn:div", withDefaultPrefix("akn:" + ELEM_DIV));
    }


    @Test
    void testGetNthChild() {
        assertNull(getNthChild(null, 0));
        assertNull(getNthChild(new JSONObject(), 0));

        JSONObject jsonObject = makeNewDefaultJSONObject(ELEM_TBLOCK);
        JSONObject p1 = makeNewDefaultJSONObject(ELEM_P);
        JSONObject p2 = makeNewDefaultJSONObject(ELEM_P);
        JSONObject p3 = makeNewDefaultJSONObject(ELEM_P);
        addChildren(jsonObject, p1, p2, p3);

        assertNull(getNthChild(jsonObject, -1));
        assertNull(getNthChild(jsonObject, 5));
        assertEquals(p1, getNthChild(jsonObject, 0));
        assertEquals(p2, getNthChild(jsonObject, 1));
        assertEquals(p3, getNthChild(jsonObject, 2));
    }


    @Test
    void testGetIndex() {
        assertEquals(-1, getIndex(null, null));
        assertEquals(-1, getIndex(null, new JSONObject()));
        assertEquals(-1, getIndex(new JSONObject(), null));
        assertEquals(-1, getIndex(new JSONObject(), new JSONObject()));

        JSONObject testParent = makeNewDefaultJSONObject(ELEM_TBLOCK);
        addText(testParent, "");
        assertEquals(-1, getIndex(testParent, new JSONObject()));

        JSONObject p1 = makeNewDefaultJSONObject(ELEM_P);
        assertEquals(-1, getIndex(testParent, p1));
        clearChildrenArray(testParent);
        addChildren(testParent, p1);
        assertEquals(0, getIndex(testParent, p1));

        JSONObject p2 = makeNewDefaultJSONObject(ELEM_P);
        JSONObject p3 = makeNewDefaultJSONObject(ELEM_P);
        addChildren(testParent, p2, p3);
        assertEquals(1, getIndex(testParent, p2));
        assertEquals(2, getIndex(testParent, p3));

        assertEquals(-1, getIndex(testParent, makeNewDefaultJSONObject(ELEM_P)));

        JSONObject anonymous = new JSONObject();
        addChildren(p1, anonymous);
        assertEquals(0, getIndex(p1, anonymous));
    }


    @Test
    void testSetText() {
        JSONObject jsonObject = makeNewDefaultJSONObject(ELEM_P);
        assertEquals("", getText(jsonObject));

        setText(jsonObject, "text");
        assertEquals("text", getText(jsonObject));

        JSONObject textWrapper = getTextWrapper("XXX");
        assertEquals("XXX", getText(textWrapper));

        setText(textWrapper, "NEW");
        assertEquals("NEW", getText(textWrapper));
    }


    @Test
    @SuppressWarnings("unchecked")
    void testIsEffectivelyEmpty() {
        assertTrue(isEffectivelyEmpty((Collection<? extends Object>) null));
        assertTrue(isEffectivelyEmpty((JSONObject) null));

        assertTrue(isEffectivelyEmpty(List.of()));
        assertFalse(isEffectivelyEmpty(List.of(1)));
        assertFalse(isEffectivelyEmpty(List.of("")));

        JSONArray jsonArray = new JSONArray();
        assertTrue(isEffectivelyEmpty(jsonArray));
        jsonArray.add(new JSONObject());
        assertFalse(isEffectivelyEmpty(jsonArray));

        JSONObject jsonObject = new JSONObject();
        assertTrue(isEffectivelyEmpty(jsonObject));
        jsonObject.put("a", "b");
        assertFalse(isEffectivelyEmpty(jsonObject));
    }


    @Test
    @SuppressWarnings("unchecked")
    void testFindByEId() {
        assertEquals(List.of(), findByEId(null, ""));
        assertEquals(List.of(), findByEId(new JSONObject(), ""));

        JSONObject testObject = new JSONObject();
        testObject.put(ATTR_EID, "X");
        assertEquals(List.of(), findByEId(testObject));
        assertEquals(List.of(), findByEId(testObject, "Y"));
        assertEquals(List.of(), findByEId(testObject, "Y", "Z"));
        assertEquals(List.of(testObject), findByEId(testObject, "Y", "Z", "X"));
    }


    @Test
    void testMergeTextContent() {
        assertNull(mergeTextContent(0, null, null, (JSONObject[]) null));
        assertNull(mergeTextContent(0, null, null, (JSONObject) null));
        assertNull(mergeTextContent(-1, " ", null, new JSONObject(), new JSONObject()));

        JSONObject firstObject = makeNewDefaultJSONObject(ELEM_P);
        addText(firstObject, "One");
        assertEquals(firstObject, mergeTextContent(0, null, null, firstObject));

        JSONObject secondObject = getTextWrapper("Two");
        JSONObject mergedObject = mergeTextContent(-1, " ", ELEM_BLOCK, firstObject, secondObject);
        assertNotNull(mergedObject);
        assertEquals(ELEM_BLOCK, getLocalName(getType(mergedObject)));
        assertEquals("One Two", getEffectiveTextValue(mergedObject));

        mergedObject = mergeTextContent(0, "--", null, firstObject, secondObject);
        assertNotNull(mergedObject);
        assertEquals(firstObject, mergedObject);
        assertEquals("One -- Two", getEffectiveTextValue(mergedObject));

        setText(firstObject, "One");
        JSONObject thirdObject = makeNewDefaultJSONObject(ELEM_DIV);
        mergedObject = mergeTextContent(2, "***", "", firstObject, secondObject, thirdObject);
        assertEquals(thirdObject, mergedObject);
        assertEquals("One *** Two", getEffectiveTextValue(mergedObject));
    }


    @Test
    void testAppendAndPrependText() {
        JSONObject jsonObject = makeNewDefaultJSONObject(ELEM_P);
        assertEquals("", getEffectiveTextValue(jsonObject));

        appendText(jsonObject, "X");
        appendText(jsonObject, ")");
        prependText(jsonObject, "(");
        assertEquals("( X )", getEffectiveTextValue(jsonObject));

        clearChildrenArray(jsonObject);
        assertEquals("", getEffectiveTextValue(jsonObject));

        addText(jsonObject, "Y");
        prependText(jsonObject, "<");
        appendText(jsonObject, ">");
        assertEquals("< Y >", getEffectiveTextValue(jsonObject));

        JSONObject textNode = makeNewTextNode("Test");
        appendText(textNode, " 1");
        assertEquals("Test", getEffectiveTextValue(textNode));
        prependText(textNode, "2. ");
        assertEquals("Test", getEffectiveTextValue(textNode));
        addText(textNode, "Finish");
        assertEquals("Finish", getEffectiveTextValue(textNode));
    }


    @Test
    void testGetRefGuid() {
        assertEquals("", getRefGuid(null));
        assertNull(getRefGuid(new JSONObject()));

        JSONObject testObject = makeNewDefaultJSONObject(ELEM_P);
        assertNull(getRefGuid(testObject));

        String value = getRandomString();
        addAttribute(testObject, Pair.of(JSON_KEY_ID_REF_GUID, value));
        assertEquals(value, getRefGuid(testObject));
    }


    @Test
    void testSetGuid() {
        UUID guid1 = UUID.randomUUID();
        JSONObject testObject = new JSONObject();

        assertNull(getGuid(testObject));
        setGuid(testObject, guid1);
        assertEquals(guid1.toString(), getGuid(testObject));

        UUID guid2 = UUID.randomUUID();
        assertNotEquals(guid1, guid2);
        setGuid(testObject, guid2);
        assertEquals(guid2.toString(), getGuid(testObject));
    }


    @Test
    void testReplaceChildren() {
        JSONObject heading = makeNewDefaultJSONObject(ELEM_HEADING);
        JSONObject pObject = makeNewDefaultJSONObject(ELEM_P);

        JSONObject tblock = makeNewDefaultJSONObject(ELEM_TBLOCK);
        JSONArray tblockChildren = getChildren(tblock);

        assertNull(tblockChildren);

        replaceChildren(tblock, heading, pObject);
        tblockChildren = getChildren(tblock);
        assertNotNull(tblockChildren);
        assertEquals(2, tblockChildren.size());
        assertEquals(ELEM_HEADING, getLocalName(getType((JSONObject) tblockChildren.get(0))));
        assertEquals(ELEM_P, getLocalName(getType((JSONObject) tblockChildren.get(1))));
    }


    @Test
    void testHasNoChildren() {
        assertTrue(hasNoChildren(null));
        assertTrue(hasNoChildren(new JSONObject()));

        JSONObject testObject = makeNewDefaultJSONObject(ELEM_DIV);
        assertTrue(hasNoChildren(testObject));

        setText(testObject, getRandomString());
        assertFalse(hasNoChildren(testObject));
    }


    @Test
    void testGetDummy() {
        JSONObject dummy = getDummy();
        assertNotNull(dummy);
        assertEquals("DUMMY", getLocalName(getType(dummy)));
    }


    @Test
    void testAreSameObjects() {
        assertTrue(areSameObjects(null, null));
        assertTrue(areSameObjects(null, null, (JSONObject) null));
        assertTrue(areSameObjects(null, null, null, null));

        JSONObject testObject = new JSONObject();
        assertFalse(areSameObjects(null, testObject));
        assertFalse(areSameObjects(null, testObject, (JSONObject) null));
        assertFalse(areSameObjects(testObject, null));
        assertTrue(areSameObjects(testObject, testObject));
        assertTrue(areSameObjects(testObject, new JSONObject()));
    }


    @Test
    void testCloneJSONObject() {
        assertNull(cloneJSONObject(null));
        assertEquals(new JSONObject(), cloneJSONObject(new JSONObject()));

        JSONObject pObject = makeNewDefaultJSONObject(ELEM_P);
        JSONObject pClone = cloneJSONObject(pObject);
        assertNotNull(pClone);
        assertEquals(pObject, pClone);

        addChildren(pObject, getTextWrapper(getRandomString()), makeNewDefaultJSONObject(ELEM_INLINE));
        pClone = cloneJSONObject(pObject);
        assertNotNull(pClone);
        assertEquals(pObject, pClone);
    }


    @Test
    void testMakeNewChangeWrapper() {
        String text = getRandomString();
        String changeType = getRandomString();
        JSONObject changeWrapper = makeNewChangeWrapper(text, changeType);

        assertNotNull(changeWrapper);
        assertEquals(JSON_VAL_CHANGE_WRAPPER, getType(changeWrapper));
        assertEquals(changeType, getStringAttribute(changeWrapper, CHANGE_TRACK_ATTR_TYPE));

        JSONArray children = getChildren(changeWrapper, true);
        assertEquals(1, children.size());
        JSONObject child = (JSONObject) children.get(0);
        assertEquals(text, getStringAttribute(child, JSON_KEY_TEXT));
    }


    @Test
    void testIsTextWrapper() {
        assertFalse(isTextWrapper(null));
        assertFalse(isTextWrapper(new JSONObject()));

        JSONObject textWrapper = getTextWrapper(getRandomString());
        assertTrue(isTextWrapper(textWrapper));
    }


    @Test
    void testGetQName_NoObjects() {
        assertNull(getQName(null));
        assertNull(getQName(new JSONObject()));
    }


    @ParameterizedTest
    @MethodSource("provideQNameTestData")
    void testGetQName_WithObjects(JSONObject testObject, String nsUri, String localName, String prefix) {
        QName qName = getQName(testObject);
        assertNotNull(qName);
        assertEquals(nsUri, qName.getNamespaceURI());
        assertEquals(localName, qName.getLocalPart());
        assertEquals(prefix, qName.getPrefix());

    }


    @SuppressWarnings("unchecked")
    private static Stream<Arguments> provideQNameTestData() {
        String elementName1 = "test";
        JSONObject testObject1 = new JSONObject();
        testObject1.put(JSON_KEY_TYPE, elementName1);

        String elementName2 = ELEM_P;
        JSONObject testObject2 = makeNewDefaultJSONObjectWithText(elementName2, getRandomString());

        String elementName3 = "meta:legalDocML.de_metadaten";
        JSONObject testObject3 = new JSONObject();
        addAttributes(testObject3,
            Pair.of(JSON_KEY_TYPE, elementName3),
            Pair.of("xmlns:meta", META_BREG_TARGET_NS));
        addChildren(testObject3, makeNewDefaultJSONObjectWithText("meta:typ", "gesetz"));

        JSONObject testObject4 = new JSONObject();
        addAttributes(testObject4,
            Pair.of(JSON_KEY_TYPE, elementName3),
            Pair.of("xmlns:meta", META_BT_TARGET_NS));
        addChildren(testObject4, makeNewDefaultJSONObjectWithText("meta:herausgeber", "deutscher-bundestag"));

        return Stream.of(
            arguments(testObject1, NULL_NS_URI, elementName1, DEFAULT_NS_PREFIX),
            arguments(testObject2, TARGET_NS, elementName2, "akn"),
            arguments(testObject3, META_BREG_TARGET_NS,
                elementName3.substring(elementName3.indexOf(':') + 1),
                elementName3.substring(0, elementName3.indexOf(':'))),
            arguments(testObject4, META_BT_TARGET_NS,
                elementName3.substring(elementName3.indexOf(':') + 1),
                elementName3.substring(0, elementName3.indexOf(':')))
        );
    }

}
