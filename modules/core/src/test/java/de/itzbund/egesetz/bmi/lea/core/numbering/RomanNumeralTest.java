// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.numbering;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RomanNumeralTest {

    /*
     * Roman numerals can only represent integers between 1 and 4000!
     */

    private static final String[] FIRST_20 = {"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII",
        "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX"};

    @Test
    void testArabicToRoman() {
        assertThrows(IllegalArgumentException.class, () -> RomanNumeral.arabicToRoman(0));
        assertThrows(IllegalArgumentException.class, () -> RomanNumeral.arabicToRoman(4001));

        for (int i = 0; i < FIRST_20.length; i++) {
            assertEquals(FIRST_20[i], RomanNumeral.arabicToRoman(i + 1));
        }

        assertEquals("MMMM", RomanNumeral.arabicToRoman(4000));
        assertEquals("MCMXCIX", RomanNumeral.arabicToRoman(1999));
        assertEquals("MMXXIV", RomanNumeral.arabicToRoman(2024));
    }

    @Test
    void testRomanToArabic() {
        for (int i = 0; i < FIRST_20.length; i++) {
            assertEquals(i + 1, RomanNumeral.romanToArabic(FIRST_20[i]));
        }

        assertEquals(4000, RomanNumeral.romanToArabic("MMMM"));
        assertEquals(1999, RomanNumeral.romanToArabic("MCMXCIX"));
        assertEquals(2024, RomanNumeral.romanToArabic("MMXXIV"));

        assertThrows(IllegalArgumentException.class, () -> RomanNumeral.romanToArabic(""));
        assertThrows(IllegalArgumentException.class, () -> RomanNumeral.romanToArabic("  "));
    }

}
