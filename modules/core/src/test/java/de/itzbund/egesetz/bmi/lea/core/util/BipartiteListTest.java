// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.util;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BipartiteListTest {

    @Test
    void testEmptyList() {
        assertThrows(IllegalArgumentException.class, () -> new BipartiteList<>(null, -1));

        List<Character> chars = List.of();
        assertThrows(IllegalArgumentException.class, () -> new BipartiteList<>(chars, -1));

        List<Integer> ints = List.of(1, 2, 3);
        assertThrows(IllegalArgumentException.class, () -> new BipartiteList<>(ints, 3));
        assertThrows(IllegalArgumentException.class, () -> new BipartiteList<>(ints, 4));

        List<Object> objects = List.of(List.of(), Map.of());
        new BipartiteList<>(objects, 0);
    }


    @Test
    void testContains() {
        BipartiteList<Integer> bipInts = new BipartiteList<>(List.of(1, 2, 3), 2);
        assertTrue(bipInts.contains(1));
        assertTrue(bipInts.contains(2));
        assertTrue(bipInts.contains(3));
        assertFalse(bipInts.contains(0));
        assertFalse(bipInts.contains(73));
    }


    @Test
    void testIsBefore_orAtIndex() {
        BipartiteList<Double> bipDoubles = new BipartiteList<>(List.of(1.2, 8.7, 3.14, (double) 9), 2);
        assertTrue(bipDoubles.isPrevious(8.7));
        assertFalse(bipDoubles.isPrevious(1.2));
        assertFalse(bipDoubles.isPrevious((double) 9));
        assertFalse(bipDoubles.isPrevious(3.14));
        assertEquals(2, bipDoubles.isBefore(1.2));
        assertEquals(1, bipDoubles.isBefore(8.7));
        assertEquals(0, bipDoubles.isBefore(3.14));
        assertEquals(0, bipDoubles.isBefore((double) 9));
        assertEquals(0, bipDoubles.isBefore((double) 19));
        assertTrue(bipDoubles.isAtIndex(3.14));
        assertFalse(bipDoubles.isAtIndex(8.7));
        assertFalse(bipDoubles.isAtIndex((double) 9));
        assertFalse(bipDoubles.isAtIndex(2.71828));
    }


    @Test
    void testIsAfter_orAtIndex() {
        BipartiteList<String> bipStrings = new BipartiteList<>(List.of("A", "B", "C", "D", "E"), 2);
        assertTrue(bipStrings.isNext("D"));
        assertFalse(bipStrings.isNext("C"));
        assertFalse(bipStrings.isNext("B"));
        assertFalse(bipStrings.isNext("X"));
        assertEquals(0, bipStrings.isAfter("A"));
        assertEquals(0, bipStrings.isAfter("B"));
        assertEquals(0, bipStrings.isAfter("C"));
        assertEquals(1, bipStrings.isAfter("D"));
        assertEquals(2, bipStrings.isAfter("E"));
        assertEquals(0, bipStrings.isAfter("X"));
        assertTrue(bipStrings.isAtIndex("C"));
        assertFalse(bipStrings.isAtIndex("A"));
        assertFalse(bipStrings.isAtIndex("D"));
        assertFalse(bipStrings.isAtIndex("X"));
    }


    @Test
    void testSetIndex() {
        BigInteger zero = BigInteger.ZERO;
        BigInteger one = BigInteger.ONE;
        BipartiteList<BigInteger> bigInts = new BipartiteList<>(List.of(zero, one), 0);
        assertTrue(bigInts.isAtIndex(zero));
        assertTrue(bigInts.isNext(one));

        bigInts.setIndex(1);
        assertFalse(bigInts.isAtIndex(zero));
        assertFalse(bigInts.isNext(one));
        assertTrue(bigInts.isAtIndex(one));
        assertFalse(bigInts.isNext(zero));
        assertTrue(bigInts.isPrevious(zero));

        assertThrows(IllegalArgumentException.class, () -> bigInts.setIndex(2));
    }


    @Test
    void testIncrIndex() {
        BipartiteList<String> bipStrings = new BipartiteList<>(List.of("A", "B", "C", "D", "E"), 2);
        assertTrue(bipStrings.isAtIndex("C"));
        bipStrings.incrIndex();
        assertTrue(bipStrings.isAtIndex("D"));
        bipStrings.incrIndex();
        assertTrue(bipStrings.isAtIndex("E"));
        assertThrows(IllegalArgumentException.class, bipStrings::incrIndex);
        bipStrings.setIndex(0);
        assertTrue(bipStrings.isAtIndex("A"));
        bipStrings.increaseIndex(3);
        assertTrue(bipStrings.isAtIndex("D"));
        assertThrows(IllegalArgumentException.class, () -> bipStrings.increaseIndex(3));
        bipStrings.increaseIndex(-2);
        assertTrue(bipStrings.isAtIndex("B"));
        assertThrows(IllegalArgumentException.class, () -> bipStrings.increaseIndex(-2));
    }


    @Test
    void testDecrIndex() {
        BipartiteList<Character> bipChars = new BipartiteList<>(List.of('u', 'v', 'w', 'x', 'y', 'z'), 3);
        assertTrue(bipChars.isAtIndex('x'));
        bipChars.decrIndex();
        assertTrue(bipChars.isAtIndex('w'));
        bipChars.decreaseIndex(2);
        assertTrue(bipChars.isAtIndex('u'));
        assertThrows(IllegalArgumentException.class, bipChars::decrIndex);
        bipChars.decreaseIndex(-5);
        assertTrue(bipChars.isAtIndex('z'));
        assertThrows(IllegalArgumentException.class, () -> bipChars.decreaseIndex(-1));
        bipChars.decreaseIndex(1);
        assertTrue(bipChars.isAtIndex('y'));
    }

}
