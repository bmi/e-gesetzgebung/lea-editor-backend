// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test suite for {@link JsonDocumentReader}.
 */
class JsonDocumentReaderTest {

    private JsonDocumentReader reader;


    @BeforeEach
    void initTestcase() {
        reader = new JsonDocumentReader();
    }


    @Test
    void testReadDocument01() {
        InputStream inputStream = JsonDocumentReaderTest.class.getResourceAsStream("/compare/test-document-01.json");
        assertNotNull(inputStream);

        DocumentDataSet ds = reader.readDocument(inputStream);
        assertNotNull(ds);
        assertEquals(1, ds.getItems().size());
        assertTrue(ds.getItems().get(0) instanceof ElementTypeDataItem);

        ElementTypeDataItem ddi = (ElementTypeDataItem) ds.getItems().get(0);
        assertEquals("D00000001", ddi.getId());
        assertNull(ddi.getParentId());
        assertEquals("bill", ddi.getName());
        assertEquals(1, ddi.getAttributes().size());
        assertEquals("regelungstext", ddi.getAttribute("name"));
    }


    @Test
    void testReadDocument02() {
        InputStream inputStream = JsonDocumentReaderTest.class.getResourceAsStream("/compare/test-document-02.json");
        assertNotNull(inputStream);

        DocumentDataSet ds = reader.readDocument(inputStream);
        assertNotNull(ds);
        assertEquals(6, ds.getItems().size());
        assertTrue(ds.getItems().get(0) instanceof ElementTypeDataItem);
        assertTrue(ds.getItems().get(1) instanceof ElementTypeDataItem);
        assertTrue(ds.getItems().get(2) instanceof ElementTypeDataItem);
        assertTrue(ds.getItems().get(3) instanceof ElementTypeDataItem);
        assertTrue(ds.getItems().get(4) instanceof ElementTypeDataItem);
        assertTrue(ds.getItems().get(5) instanceof TextTypeDataItem);

        ElementTypeDataItem edi = (ElementTypeDataItem) ds.getItems().get(4);
        assertEquals("E00000004", edi.getId());

        TextTypeDataItem tdi = (TextTypeDataItem) ds.getItems().get(5);
        assertEquals("T00000001", tdi.getId());
        assertEquals(edi.getId(), tdi.getParentId());
        assertEquals("Der Bundestag hat das folgende Gesetz beschlossen:", tdi.getContent());
    }


    @SneakyThrows
    @Test
    void testReadDocument03() {
        InputStream inputStream = JsonDocumentReaderTest.class.getResourceAsStream("/compare/test-document-03.json");
        assertNotNull(inputStream);

        BufferedReader txtReader = new BufferedReader(new InputStreamReader(
            Objects.requireNonNull(
                JsonDocumentReaderTest.class.getResourceAsStream("/compare/test-document-03.txt"))));
        assertNotNull(txtReader);
        List<String> expectedTextsAndNames = new ArrayList<>();
        String nextString;

        while ((nextString = txtReader.readLine()) != null) {
            expectedTextsAndNames.add(nextString);
        }
        txtReader.close();
        int numDataItems = expectedTextsAndNames.size();
        assertEquals(82, numDataItems);

        DocumentDataSet ds = reader.readDocument(inputStream);
        assertNotNull(ds);
        assertEquals(numDataItems, ds.getItems().size());

        int countElements = 0;
        int countTexts = 0;
        for (int i = 0; i < numDataItems; i++) {
            String expected = expectedTextsAndNames.get(i);
            DocumentDataItem dataItem = ds.getItems().get(i);

            if (dataItem instanceof ElementTypeDataItem) {
                ElementTypeDataItem edi = (ElementTypeDataItem) dataItem;
                assertEquals(expected, edi.getName());
                countElements++;
            } else {
                TextTypeDataItem tdi = (TextTypeDataItem) dataItem;
                assertEquals(expected, tdi.getContent());
                countTexts++;
            }
        }

        assertEquals(51, countElements);
        assertEquals(31, countTexts);
        assertEquals("T0000001F", ds.getItems().get(ds.getItems().size() - 1).getId());
    }


    @Test
    void testInvalidJSONInput() {

        InputStream inputStream = JsonDocumentReaderTest.class.getResourceAsStream("/compare/invalid.json");
        assertNotNull(inputStream);

        DocumentDataSet ds = reader.readDocument(inputStream);
        assertTrue(ds.getItems().isEmpty());
    }


    @Test
    void testNullText() {
        InputStream inputStream = JsonDocumentReaderTest.class.getResourceAsStream("/compare/test-document-04.json");
        assertNotNull(inputStream);

        DocumentDataSet ds = reader.readDocument(inputStream);
        assertNotNull(ds);
        assertEquals(2, ds.getItems().size());
        assertTrue(ds.getItems().get(0) instanceof ElementTypeDataItem);
        assertTrue(ds.getItems().get(1) instanceof TextTypeDataItem);

        TextTypeDataItem tdi = (TextTypeDataItem) ds.getItems().get(1);
        assertTrue(tdi.getContent().isEmpty());
    }

}
