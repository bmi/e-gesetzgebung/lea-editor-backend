// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import de.itzbund.egesetz.bmi.lea.core.ChangeMarker;
import de.itzbund.egesetz.bmi.lea.core.json.JSONUtils;
import de.itzbund.egesetz.bmi.lea.core.util.BipartiteList;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_NAME;
import static de.itzbund.egesetz.bmi.lea.core.Constants.CHANGE_MARK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ARTICLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BLOCK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CONTENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DIV;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_HEADING;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_INLINE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_INTRO;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LIST;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_NUM;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_P;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PARAGRAPH;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_POINT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TBLOCK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_WRAPUP;
import static de.itzbund.egesetz.bmi.lea.core.Constants.IS_SUBSTITUTE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_CHILDREN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_ID_GUID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_TYPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_TEXT_WRAPPER;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.getChangeMarker;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.getContentChildren;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.getGuids;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.getHdRconformantBaseText;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.getHdRconformantVersionText;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.getTextWrapper;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.getTopLevelStructures;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.isAssignable;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.isChangeMarked;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.isChangedMarkedOneOf;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.isLeave;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.isList;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.isTable;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.isTextualElement;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.jsonTreeToFlatList;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.makeSubstituteFor;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.makeSubstituteForStructure;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.markAllContent;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.markChangesInText;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.markChangesInTextWrapper;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.markContent;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.markContentP;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.markElementPair;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.markListContent;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.markMatchingPair;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.markMovedElements;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.markText;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.toTextChangeMarkerLiteral;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addAttributes;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getEffectiveTextValue;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getGuid;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getHeadingOfElement;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getNumElement;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getStringAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewDefaultJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewTextNode;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.replaceChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.setGuid;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.setText;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CompareUtilsTest {

    private static final DiffMatchPatch DMP = new DiffMatchPatch();

    private static final List<DiffMatchPatch.Diff> DIFFS = new LinkedList<>(
        Arrays.asList(
            new DiffMatchPatch.Diff(DiffMatchPatch.Operation.DELETE, "Hello"),
            new DiffMatchPatch.Diff(DiffMatchPatch.Operation.INSERT, "Goodbye"),
            new DiffMatchPatch.Diff(DiffMatchPatch.Operation.EQUAL, " World!")
        )
    );

    private static JSONObject testDocument;


    @BeforeAll
    static void setUp() {
        String jsonContent = Utils.getContentOfTextResource(
            "/compare/20230511_1359_Export_Begruendung-Stammgesetz_BG-SF.json");
        testDocument = JSONUtils.getDocumentObject(jsonContent);
        assertNotNull(testDocument);
    }

    @AfterAll
    static void tearDown() {
        testDocument = null;
    }

    @Test
    void testJsonTreeToFlatList() {
        assertEquals(45, jsonTreeToFlatList(testDocument).size());
    }

    @Test
    @SuppressWarnings("unchecked")
    void testGetGuids() {
        assertNull(getGuids(null, 0));
        assertNull(getGuids(List.of(), 0));

        List<String> guidsExpected = List.of(
            UUID.randomUUID()
                .toString(),
            UUID.randomUUID()
                .toString(),
            UUID.randomUUID()
                .toString(),
            UUID.randomUUID()
                .toString(),
            UUID.randomUUID()
                .toString(),
            UUID.randomUUID()
                .toString()
        );
        List<JSONObject> jsonObjects = guidsExpected.stream()
            .map(id -> {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(JSON_KEY_ID_GUID, id);
                return jsonObject;
            })
            .collect(Collectors.toList());

        BipartiteList<String> guidsCalculated = getGuids(jsonObjects, 0);
        assertNotNull(guidsCalculated);
        assertEquals(guidsExpected.size(), guidsCalculated.getSize());
        assertEquals(guidsExpected, guidsCalculated.getElementList());
    }

    @Test
    void testIsList() {
        assertTrue(isList("akn:list"));
        assertTrue(isList("blockList"));
        assertTrue(isList("x:ol"));
        assertTrue(isList("akn:ul"));
        assertFalse(isList("akn:p"));
    }

    @Test
    void testIsTable() {
        assertTrue(isTable("akn:table"));
        assertTrue(isTable("table"));
        assertFalse(isTable("akn:p"));
        assertFalse(isTable("ol"));
    }

    @Test
    void testIsTextualElement() {
        assertTrue(isTextualElement("akn:p"));
        assertTrue(isTextualElement("p"));
        assertTrue(isTextualElement("block"));
        assertTrue(isTextualElement("a:block"));
        assertTrue(isTextualElement("num"));
        assertTrue(isTextualElement("heading"));
        assertTrue(isTextualElement("head:heading"));
        assertTrue(isTextualElement("tocItem"));
        assertFalse(isTextualElement("list"));
        assertFalse(isTextualElement("akn:table"));
    }

    @Test
    void testGetTopLevelStructures() {
        Map<String, JSONObject> topLevelStructures = getTopLevelStructures(testDocument);
        assertNotNull(topLevelStructures);
        assertEquals(3, topLevelStructures.size());

        for (String type : Arrays.asList("meta", "preface", "mainBody")) {
            assertNotNull(topLevelStructures.get(type));
            assertEquals(type, JSONUtils.getLocalName(JSONUtils.getType(topLevelStructures.get(type))));
        }
    }

    @Test
    void testMarkChangesInTextWrapper() {
        JSONObject baseObject = JSONUtils.getTextWrapper("ABC");
        JSONObject versionObject = JSONUtils.getTextWrapper("");

        LinkedList<DiffMatchPatch.Diff> diffs = new LinkedList<>();
        diffs.add(new DiffMatchPatch.Diff(DiffMatchPatch.Operation.EQUAL, "ABC"));
        markChangesInTextWrapper(baseObject, versionObject, diffs);
        assertEquals(TextChangeMarker.UNCHANGED.getType(), getStringAttribute(baseObject, CHANGE_MARK));
        assertEquals(TextChangeMarker.UNCHANGED.getType(), getStringAttribute(versionObject, CHANGE_MARK));

        baseObject = JSONUtils.getTextWrapper("ABC");
        versionObject = JSONUtils.getTextWrapper("");
        diffs = DMP.diffMain(getEffectiveTextValue(baseObject), getEffectiveTextValue(versionObject));
        DMP.diffCleanupSemantic(diffs);
        markChangesInTextWrapper(baseObject, versionObject, diffs);
        assertEquals(TextChangeMarker.ERASED.getType(), getStringAttribute(baseObject, CHANGE_MARK));
        assertEquals(TextChangeMarker.INSERTED.getType(), getStringAttribute(versionObject, CHANGE_MARK));

        baseObject = JSONUtils.getTextWrapper("ABC");
        versionObject = JSONUtils.getTextWrapper("12345");
        diffs = DMP.diffMain(getEffectiveTextValue(baseObject), getEffectiveTextValue(versionObject));
        DMP.diffCleanupSemantic(diffs);
        markChangesInTextWrapper(baseObject, versionObject, diffs);
        assertEquals(TextChangeMarker.ERASED.getType(), getStringAttribute(baseObject, CHANGE_MARK));
        assertEquals(TextChangeMarker.INSERTED.getType(), getStringAttribute(versionObject, CHANGE_MARK));
    }


    @Test
    void testGetTextWrapper() {
        JSONObject jsonObject = getTextWrapper("ABC", TextChangeMarker.UNCHANGED);
        assertNotNull(jsonObject);
        assertEquals("ABC", getEffectiveTextValue(jsonObject));
        assertEquals(TextChangeMarker.UNCHANGED.getType(), getStringAttribute(jsonObject, CHANGE_MARK));

        jsonObject = getTextWrapper("XYZ", TextChangeMarker.INSERTED);
        assertNotNull(jsonObject);
        assertEquals("XYZ", getEffectiveTextValue(jsonObject));
        assertEquals(TextChangeMarker.INSERTED.getType(), getStringAttribute(jsonObject, CHANGE_MARK));
    }


    @Test
    @SuppressWarnings("java:S5778")
    void testMarkChangesInText() {
        String markMatched = ElementChangeMarker.MATCHED.getType();
        String markAdded = ElementChangeMarker.ADDED.getType();
        String markDeleted = ElementChangeMarker.DELETED.getType();
        String markEmpty = ElementChangeMarker.EMPTY.getType();

        assertThrows(NullPointerException.class, () -> markChangesInText(null, new JSONObject(), List.of()));
        assertThrows(NullPointerException.class, () -> markChangesInText(new JSONObject(), null, List.of()));
        assertThrows(NullPointerException.class, () -> markChangesInText(new JSONObject(), new JSONObject(), null));

        JSONObject baseObject = makeNewDefaultJSONObject(ELEM_P);
        assertNotNull(baseObject);
        assertEquals(Set.of(JSON_KEY_TYPE, JSON_KEY_ID_GUID), baseObject.keySet());

        JSONObject versionObject = makeNewDefaultJSONObject(ELEM_P);
        assertNotNull(versionObject);
        assertEquals(Set.of(JSON_KEY_TYPE, JSON_KEY_ID_GUID), versionObject.keySet());
        assertNotEquals(baseObject, versionObject);

        markChangesInText(baseObject, versionObject, DIFFS);
        assertEquals(Set.of(JSON_KEY_TYPE, JSON_KEY_ID_GUID, JSON_KEY_CHILDREN, CHANGE_MARK), baseObject.keySet());
        assertEquals(Set.of(JSON_KEY_TYPE, JSON_KEY_ID_GUID, JSON_KEY_CHILDREN, CHANGE_MARK), versionObject.keySet());

        assertEquals(markMatched, getStringAttribute(baseObject, CHANGE_MARK));
        JSONArray baseChildren = getChildren(baseObject);
        assertNotNull(baseChildren);
        assertEquals(2, baseChildren.size());
        assertEquals("Hello World!", getEffectiveTextValue(baseObject));

        assertEquals(markMatched, getStringAttribute(versionObject, CHANGE_MARK));
        JSONArray versionChildren = getChildren(versionObject);
        assertNotNull(versionChildren);
        assertEquals(2, versionChildren.size());
        assertEquals("Goodbye World!", getEffectiveTextValue(versionObject));

        baseObject = makeNewDefaultJSONObject(ELEM_P);
        versionObject = makeNewDefaultJSONObject(ELEM_P);
        LinkedList<DiffMatchPatch.Diff> allInserted = new LinkedList<>(List.of(new DiffMatchPatch.Diff(DiffMatchPatch.Operation.INSERT, "NEW")));
        markChangesInText(baseObject, versionObject, allInserted);
        assertEquals(markEmpty, getChangeMarker(baseObject));
        assertEquals(markAdded, getChangeMarker(versionObject));

        LinkedList<DiffMatchPatch.Diff> allDeleted = new LinkedList<>(List.of(new DiffMatchPatch.Diff(DiffMatchPatch.Operation.DELETE, "OLD")));
        markChangesInText(baseObject, versionObject, allDeleted);
        assertEquals(markDeleted, getChangeMarker(baseObject));
    }


    @Test
    void testIsLeave() {
        assertTrue(isLeave(makeNewDefaultJSONObject(ELEM_P)));
        assertTrue(isLeave(makeNewDefaultJSONObject(ELEM_BLOCK)));
        assertTrue(isLeave(makeNewDefaultJSONObject(ELEM_NUM)));
        assertFalse(isLeave(makeNewDefaultJSONObject(ELEM_DIV)));
        assertFalse(isLeave(makeNewDefaultJSONObject(ELEM_ARTICLE)));
    }


    @Test
    void testGetHdRconformantBaseText() {
        String baseText = getHdRconformantBaseText(DIFFS);
        assertNotNull(baseText);
        assertTrue(baseText.contains("Hello"));
        assertTrue(baseText.contains("World"));
    }


    @Test
    void testGetHdRconformantVersionText() {
        String versionText = getHdRconformantVersionText(DIFFS);
        assertNotNull(versionText);
        assertTrue(versionText.contains("Goodbye"));
        assertTrue(versionText.contains("World"));
    }


    @Test
    void testMarkMovedElements() {
        assertDoesNotThrow(() -> markMovedElements(null, null));
        assertDoesNotThrow(() -> markMovedElements(new JSONObject(), null));
        assertDoesNotThrow(() -> markMovedElements(null, new JSONObject()));

        JSONObject left = makeNewDefaultJSONObject(ELEM_P);
        JSONObject right = makeNewDefaultJSONObject(ELEM_P);
        assertNotEquals(getGuid(left), getGuid(right));

        markMovedElements(left, right);
        assertEquals(ElementChangeMarker.MOVED.getType(), getStringAttribute(left, CHANGE_MARK));
        assertEquals(ElementChangeMarker.MOVED.getType(), getStringAttribute(right, CHANGE_MARK));
    }


    @Test
    void testMarkContentP() {
        JSONObject pObject = makeNewDefaultJSONObject(ELEM_P);
        JSONObject contentObject = makeNewDefaultJSONObject(ELEM_CONTENT);
        JSONObject jsonObject = makeNewDefaultJSONObject(ELEM_PARAGRAPH);
        addChildren(jsonObject, contentObject);
        addChildren(contentObject, pObject);

        markContentP(pObject, ElementChangeMarker.ADDED);
        assertNull(getStringAttribute(pObject, CHANGE_MARK));

        markContentP(jsonObject, ElementChangeMarker.DELETED);
        assertEquals(ElementChangeMarker.DELETED.getType(), getStringAttribute(pObject, CHANGE_MARK));

        markContentP(jsonObject, ElementChangeMarker.EMPTY);
        assertEquals(ElementChangeMarker.EMPTY.getType(), getStringAttribute(pObject, CHANGE_MARK));
    }


    @Test
    void testMarkListContent() {
        JSONObject jsonObject = makeNewDefaultJSONObject(ELEM_PARAGRAPH);
        JSONObject numObject = makeNewDefaultJSONObject(ELEM_NUM);
        JSONObject listObject = makeNewDefaultJSONObject(ELEM_LIST);
        addChildren(jsonObject, numObject, listObject);

        assertNull(getStringAttribute(listObject, CHANGE_MARK));
        markListContent(jsonObject, ElementChangeMarker.DELETED);
        assertEquals(ElementChangeMarker.DELETED.getType(), getStringAttribute(jsonObject, CHANGE_MARK));

        JSONObject introObject = makeNewDefaultJSONObject(ELEM_INTRO);
        JSONObject pointObject = makeNewDefaultJSONObject(ELEM_POINT);
        JSONObject wrapUpObject = makeNewDefaultJSONObject(ELEM_WRAPUP);
        addChildren(listObject, introObject, pointObject, wrapUpObject);

        markListContent(jsonObject, ElementChangeMarker.ADDED);
        assertEquals(ElementChangeMarker.ADDED.getType(), getStringAttribute(introObject, CHANGE_MARK));
        assertEquals(ElementChangeMarker.ADDED.getType(), getStringAttribute(pointObject, CHANGE_MARK));
        assertEquals(ElementChangeMarker.ADDED.getType(), getStringAttribute(wrapUpObject, CHANGE_MARK));
    }

    @Test
    @SuppressWarnings("unchecked")
    void testMakeSubstituteForStructure() {

        // text wrapper

        JSONObject testObject = getTextWrapper(Utils.getRandomString(), null);
        JSONObject substitute = makeSubstituteForStructure(testObject, null);
        assertNotNull(substitute);
        assertEquals(getType(testObject), getType(substitute));
        assertNull(getStringAttribute(testObject, CHANGE_MARK));
        assertNull(getStringAttribute(substitute, CHANGE_MARK));
        assertEquals("", getText(substitute));

        // simple object

        testObject = makeNewDefaultJSONObject(ELEM_P);
        substitute = makeSubstituteForStructure(testObject, ElementChangeMarker.ADDED);
        assertNotNull(substitute);
        assertEquals(getType(testObject), getType(substitute));
        assertEquals(ElementChangeMarker.ADDED.getType(), getStringAttribute(testObject, CHANGE_MARK));

        JSONArray substituteChildren = getChildren(substitute);
        assertNotNull(substituteChildren);
        assertEquals(1, substituteChildren.size());
        JSONObject substituteChild = (JSONObject) substituteChildren.get(0);
        assertEquals(JSON_VAL_TEXT_WRAPPER, getType(substituteChild));
        assertEquals("", getText(substituteChild));

        // complex object

        testObject = makeNewDefaultJSONObject(ELEM_TBLOCK);
        JSONObject heading = makeNewDefaultJSONObject(ELEM_HEADING);
        setText(heading, Utils.getRandomString());
        JSONObject tblockContent = makeNewDefaultJSONObject(ELEM_P);
        JSONObject pText1 = getTextWrapper(Utils.getRandomString(), null);
        JSONObject pText2 = getTextWrapper(Utils.getRandomString(), null);
        JSONObject pInline = makeNewDefaultJSONObject(ELEM_INLINE);
        setText(pInline, Utils.getRandomString());
        addAttribute(pInline, Pair.of(ATTR_NAME, Utils.getRandomString()));
        JSONArray tblockContentChildren = new JSONArray();
        tblockContentChildren.add(pText1);
        tblockContentChildren.add(pInline);
        tblockContentChildren.add(pText2);
        replaceChildren(tblockContent, tblockContentChildren);
        JSONArray testObjectChildren = new JSONArray();
        testObjectChildren.add(heading);
        testObjectChildren.add(tblockContent);
        replaceChildren(testObject, testObjectChildren);

        substitute = makeSubstituteForStructure(testObject, ElementChangeMarker.DELETED);
        assertNotNull(substitute);
        assertEquals(getType(testObject), getType(substitute));
        assertEquals(ElementChangeMarker.DELETED.getType(), getStringAttribute(testObject, CHANGE_MARK));
        assertFalse(getEffectiveTextValue(testObject).isBlank());
        assertTrue(getEffectiveTextValue(substitute).isBlank());

        testObjectChildren = getChildren(testObject);
        substituteChildren = getChildren(substitute);
        assertEquals(testObjectChildren.size(), substituteChildren.size());

        for (int i = 0; i < testObjectChildren.size(); i++) {
            JSONObject testObjectChild = (JSONObject) testObjectChildren.get(i);
            substituteChild = (JSONObject) substituteChildren.get(i);
            assertEquals(getType(testObjectChild), getType(substituteChild));
            assertEquals(ElementChangeMarker.DELETED.getType(), getStringAttribute(testObjectChild, CHANGE_MARK));
            assertEquals(getChildren(testObjectChild).size(), getChildren(substituteChild).size());
        }
    }

    @Test
    void testMarkAllContent() {
        JSONObject testObject = makeNewDefaultJSONObject(ELEM_PARAGRAPH);
        JSONObject contentObject = makeNewDefaultJSONObject(ELEM_CONTENT);
        JSONObject pObject1 = makeNewDefaultJSONObject(ELEM_P);
        JSONObject pObject2 = makeNewDefaultJSONObject(ELEM_P);
        replaceChildren(contentObject, pObject1, pObject2);
        replaceChildren(testObject, contentObject);

        ElementChangeMarker mark = ElementChangeMarker.ADDED;
        markAllContent(testObject, mark);
        assertEquals(mark.getType(), getStringAttribute(pObject1, CHANGE_MARK));
        assertEquals(mark.getType(), getStringAttribute(pObject2, CHANGE_MARK));
    }

    @Test
    void testMarkText() {
        JSONObject testObject = makeNewDefaultJSONObject(ELEM_P);
        setText(testObject, Utils.getRandomString());

        TextChangeMarker mark = TextChangeMarker.INSERTED;
        markText(testObject, mark);

        assertNull(getStringAttribute(testObject, CHANGE_MARK));
        JSONArray children = getChildren(testObject);
        assertNotNull(children);
        assertEquals(1, children.size());
        JSONObject child = (JSONObject) children.get(0);
        assertEquals(JSON_VAL_TEXT_WRAPPER, getType(child));
        assertEquals(mark.getType(), getStringAttribute(child, CHANGE_MARK));
    }

    @Test
    void testIsAssignable() {
        JSONObject pivot = makeNewDefaultJSONObject(ELEM_DIV);
        JSONObject different = makeNewDefaultJSONObject(ELEM_DIV);
        JSONObject assignable = makeNewJSONObject(ELEM_DIV, true, false);
        setGuid(assignable, UUID.fromString(getGuid(pivot)));

        assertFalse(isAssignable(pivot, different));
        assertTrue(isAssignable(pivot, assignable));
    }

    @Test
    void testMarkElementPair() {
        ElementChangeMarker addMark = ElementChangeMarker.ADDED;
        ElementChangeMarker delMark = ElementChangeMarker.DELETED;
        ElementChangeMarker matchMark = ElementChangeMarker.MATCHED;

        JSONObject jsonObject = makeNewDefaultJSONObject(ELEM_P);
        JSONObject substitute = makeSubstituteFor(jsonObject);
        assertNull(getStringAttribute(jsonObject, CHANGE_MARK));

        markElementPair(jsonObject, substitute);
        assertEquals(delMark.getType(), getStringAttribute(jsonObject, CHANGE_MARK));

        jsonObject = makeNewDefaultJSONObject(ELEM_P);
        markElementPair(substitute, jsonObject);
        assertEquals(addMark.getType(), getStringAttribute(jsonObject, CHANGE_MARK));

        jsonObject = makeNewDefaultJSONObject(ELEM_P);
        JSONObject otherObject = makeNewDefaultJSONObject(ELEM_P);
        assertNull(getStringAttribute(otherObject, CHANGE_MARK));
        markElementPair(jsonObject, otherObject);
        assertEquals(matchMark.getType(), getStringAttribute(jsonObject, CHANGE_MARK));
        assertEquals(matchMark.getType(), getStringAttribute(otherObject, CHANGE_MARK));
    }

    @Test
    void testGetContentChildren() {
        assertEquals(new JSONArray(), getContentChildren(null));
        assertEquals(new JSONArray(), getContentChildren(new JSONObject()));

        JSONObject testObject = makeNewDefaultJSONObject(ELEM_P);
        setText(testObject, Utils.getRandomString());
        assertEquals(new JSONArray(), getContentChildren(testObject));

        testObject = makeNewDefaultJSONObject(ELEM_POINT);
        JSONObject contentObject = makeNewDefaultJSONObject(ELEM_CONTENT);
        JSONObject p1 = makeNewDefaultJSONObject(ELEM_P);
        JSONObject p2 = makeNewDefaultJSONObject(ELEM_P);
        replaceChildren(contentObject, p1, p2);
        replaceChildren(testObject, contentObject);
        assertEquals(List.of(p1, p2), getContentChildren(testObject));
    }

    @Test
    void testToTextChangeMarkerLiteral() {
        String unchanged = TextChangeMarker.UNCHANGED.getType();
        String inserted = TextChangeMarker.INSERTED.getType();
        String erased = TextChangeMarker.ERASED.getType();

        assertEquals(unchanged, toTextChangeMarkerLiteral(null));
        assertEquals(unchanged, toTextChangeMarkerLiteral(""));
        assertEquals(unchanged, toTextChangeMarkerLiteral(" "));
        assertEquals(unchanged, toTextChangeMarkerLiteral("\t"));

        assertEquals(unchanged, toTextChangeMarkerLiteral(unchanged));
        assertEquals(inserted, toTextChangeMarkerLiteral(inserted));
        assertEquals(erased, toTextChangeMarkerLiteral(erased));

        assertEquals(inserted, toTextChangeMarkerLiteral(ElementChangeMarker.ADDED.getType()));
        assertEquals(erased, toTextChangeMarkerLiteral(ElementChangeMarker.DELETED.getType()));
        assertEquals(unchanged, toTextChangeMarkerLiteral(ElementChangeMarker.MATCHED.getType()));
        assertEquals(unchanged, toTextChangeMarkerLiteral(ElementChangeMarker.MOVED.getType()));
        assertEquals(unchanged, toTextChangeMarkerLiteral(ElementChangeMarker.MATCHED.getType()));
    }

    @Test
    void testGetChangeMarker() {
        assertEquals("", getChangeMarker(null));
        assertEquals("", getChangeMarker(new JSONObject()));

        JSONObject testObject = makeNewDefaultJSONObject(ELEM_P);
        assertEquals("", getChangeMarker(testObject));

        JSONObject textWrapper = makeNewTextNode(Utils.getRandomString());
        assertEquals("", getChangeMarker(textWrapper));

        markContent(testObject, ElementChangeMarker.ADDED);
        assertEquals("cp_added", getChangeMarker(testObject));

        markContent(textWrapper, TextChangeMarker.ERASED);
        assertEquals("cp_erased", getChangeMarker(textWrapper));
    }

    @Test
    @SuppressWarnings("unchecked")
    void testMarkMatchingPair() {
        ElementChangeMarker marker = ElementChangeMarker.MATCHED;
        ElementChangeMarker empty = ElementChangeMarker.EMPTY;

        JSONObject left = new JSONObject();
        JSONObject right = new JSONObject();
        markMatchingPair(left, right);
        assertEquals(marker.getType(), getChangeMarker(left));
        assertEquals(marker.getType(), getChangeMarker(right));

        left = makeNewDefaultJSONObject(ELEM_P);
        addChildren(left, makeNewDefaultJSONObject(ELEM_NUM), makeNewDefaultJSONObject(ELEM_HEADING));
        right = makeNewDefaultJSONObject(ELEM_P);
        addChildren(right, makeNewDefaultJSONObject(ELEM_NUM), makeNewDefaultJSONObject(ELEM_HEADING));
        markMatchingPair(left, right);
        assertEquals(marker.getType(), getChangeMarker(left));
        assertEquals(marker.getType(), getChangeMarker(getNumElement(left)));
        assertEquals(marker.getType(), getChangeMarker(getHeadingOfElement(left)));
        assertEquals(marker.getType(), getChangeMarker(right));
        assertEquals(marker.getType(), getChangeMarker(getNumElement(right)));
        assertEquals(marker.getType(), getChangeMarker(getHeadingOfElement(right)));

        right = makeNewDefaultJSONObject(ELEM_P);
        addChildren(right, makeNewDefaultJSONObject(ELEM_NUM), makeNewDefaultJSONObject(ELEM_HEADING));
        addAttributes(right, Pair.of(IS_SUBSTITUTE, Boolean.TRUE.toString()), Pair.of(CHANGE_MARK, empty.getType()));
        markMatchingPair(left, right);
        assertNotEquals(marker.getType(), getChangeMarker(right));
        assertEquals(empty.getType(), getChangeMarker(right));
    }

    @Test
    void testIsChangeMarked() {
        List<ChangeMarker> allChangeMarkers = new ArrayList<>(Arrays.asList(ElementChangeMarker.values()));
        allChangeMarkers.addAll(Arrays.asList(TextChangeMarker.values()));
        allChangeMarkers = Collections.unmodifiableList(allChangeMarkers);

        allChangeMarkers.forEach(m -> assertFalse(isChangeMarked(null, m)));

        JSONObject testObject = new JSONObject();
        allChangeMarkers.forEach(m -> assertFalse(isChangeMarked(testObject, m)));

        ElementChangeMarker testMarker = ElementChangeMarker.MATCHED;
        markContent(testObject, testMarker);
        allChangeMarkers.forEach(m -> {
            if (m == testMarker) {
                assertTrue(isChangeMarked(testObject, m));
            } else {
                assertFalse(isChangeMarked(testObject, m));
            }
        });

        assertTrue(isChangedMarkedOneOf(testObject, testMarker));
        assertTrue(isChangedMarkedOneOf(testObject, testMarker, TextChangeMarker.UNCHANGED));
        assertFalse(isChangedMarkedOneOf(testObject, TextChangeMarker.UNCHANGED));
    }

}
