// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Testsuite for {@link XmlDocumentReader}.
 */
class XmlDocumentReaderTest {

    private XmlDocumentReader reader;


    @BeforeEach
    void initTestcase() {
        reader = new XmlDocumentReader();
    }


    @Test
    void testReadDocument01() {
        InputStream inputStream = XmlDocumentReaderTest.class.getResourceAsStream("/compare/test-document-01.xml");
        assertNotNull(inputStream);

        DocumentDataSet ds = reader.readDocument(inputStream);
        assertNotNull(ds);
        assertEquals(15, ds.getItems().size());

        assertElementFeatures(ds, 0, "bill", "D00000001", null,
            "name", "regelungstext");
        assertElementFeatures(ds, 1, "preface", "E00000001", "D00000001");
        assertElementFeatures(ds, 2, "longTitle", "E00000002", "E00000001");
        assertElementFeatures(ds, 3, "p", "E00000003", "E00000002");
        assertElementFeatures(ds, 4, "docStage", "E00000004", "E00000003");
        assertTextFeatures(ds, 5, "T00000001", "E00000004", "Referentenentwurf");
        assertElementFeatures(ds, 6, "docProponent", "E00000005", "E00000003");
        assertTextFeatures(ds, 7, "T00000002", "E00000005",
            "des Bundesministeriums für Wirtschaft und Energie ");
        assertElementFeatures(ds, 8, "docTitle", "E00000006", "E00000003");
        assertTextFeatures(ds, 9, "T00000003", "E00000006",
            "Entwurf eines Gesetzes über die Akkreditierungsstelle");
        assertElementFeatures(ds, 10, "shortTitle", "E00000007", "E00000003");
        assertTextFeatures(ds, 11, "T00000004", "E00000007",
            "(Akkreditierungsstellengesetz - ");
        assertElementFeatures(ds, 12, "inline", "E00000008", "E00000007",
            "refersTo", "amtliche-abkuerzung");
        assertTextFeatures(ds, 13, "T00000005", "E00000008", "AkkStelleG");
        assertTextFeatures(ds, 14, "T00000006", "E00000007",
            ") ");
    }


    @Test
    void testInvalidXMLInput() {

        InputStream inputStream = JsonDocumentReaderTest.class.getResourceAsStream("/compare/invalid.xml");
        assertNotNull(inputStream);

        DocumentDataSet ds = reader.readDocument(inputStream);
        assertTrue(ds.getItems().isEmpty());
    }


    @Test
    void testInvalidXMLName() {

        InputStream inputStream = JsonDocumentReaderTest.class.getResourceAsStream("/compare/invalid-name.xml");
        assertNotNull(inputStream);

        DocumentDataSet ds = reader.readDocument(inputStream);
        assertEquals(2, ds.getItems().size());
        assertTrue(ds.getItems().get(0) instanceof ElementTypeDataItem);
        ElementTypeDataItem edi = (ElementTypeDataItem) ds.getItems().get(0);
        assertNull(edi.getName());
    }


    private void assertElementFeatures(DocumentDataSet ds, int index, String expectedName, String expectedId,
        String expectedParentId, String... expectedAttributes) {
        assertTrue(ds.getItems().get(index) instanceof ElementTypeDataItem);
        ElementTypeDataItem edi = (ElementTypeDataItem) ds.getItems().get(index);

        assertEquals(expectedName, edi.getName());
        assertEquals(expectedId, edi.getId());
        assertEquals(expectedParentId, edi.getParentId());

        if (expectedAttributes == null || expectedAttributes.length == 0) {
            assertEquals(0, edi.getAttributes().size());
        } else {
            assertEquals(expectedAttributes.length / 2, edi.getAttributes().size());
            for (int i = 0; i < expectedAttributes.length; i += 2) {
                String name = expectedAttributes[i];
                assertEquals(expectedAttributes[i + 1], edi.getAttribute(name));
            }
        }
    }


    private void assertTextFeatures(DocumentDataSet ds, int index, String expectedId,
        String expectedParentId, String expectedContent) {
        assertTrue(ds.getItems().get(index) instanceof TextTypeDataItem);
        TextTypeDataItem tdi = (TextTypeDataItem) ds.getItems().get(index);

        assertEquals(expectedId, tdi.getId());
        assertEquals(expectedParentId, tdi.getParentId());
        assertEquals(expectedContent, tdi.getContent());
    }

}
