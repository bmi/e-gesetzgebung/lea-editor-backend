// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.util;

import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Instances of this class provide test resources from lea-core module other modules.
 */

@Log4j2
@SuppressWarnings("java:S5042")
public class TestResourceProvider {

    private static final Class<TestResourceProvider> CLS = TestResourceProvider.class;

    private static final String URL_PROTOCOL_FILE = "file";

    private static final String URL_PROTOCOL_JAR = "jar";

    private static final int JAR_PREFIX_WIN_LENGTH = 10;

    private static final int JAR_PREFIX_NIX_LENGTH = 9;

    private static final Pattern PATT_LDML_FILE = Pattern.compile("ldml/(\\w+)/.+\\.xml");

    private static final Pattern PATT_WIN_FILE = Pattern.compile("jar:file:/\\w:");

    private static TestResourceProvider instance;

    private final Map<String, Collection<ReusableInputSource>> resources;


    private TestResourceProvider() {
        // collect file resources and cache them
        resources = new HashMap<>();
        collectResources(TestResourceProvider.class.getResource("/ldml"));
    }


    /**
     * Returns the same instance of a TestResourceProvider.
     *
     * @return the instance of TestResourceProvider
     */
    public static TestResourceProvider getInstance() {
        if (instance == null) {
            instance = new TestResourceProvider();
        }
        return instance;
    }


    /**
     * Provides a single XML document as {@link ReusableInputSource} available at the specified path.
     *
     * @param path location of the resource file
     * @return an instance of {@link ReusableInputSource}
     */
    public ReusableInputSource getSingleInputSource(String path) {
        return new ReusableInputSource(CLS.getResourceAsStream(path));
    }


    /**
     * Provides an Iterator over instances of {@link ReusableInputSource} given several locations.
     *
     * @param paths collection of path specifications
     * @return an iterator over XML documents
     */
    public Iterator<ReusableInputSource> getInputSources(String... paths) {
        return getInputSources(null, paths);
    }


    /**
     * Provides an Iterator over instances of {@link ReusableInputSource} given several locations and filtered by a {@link FilenameFilter}.
     *
     * @param filter a filter to match some criteria regarding file names
     * @param paths  collection of path specifications
     * @return an iterator over XML documents
     */
    @SneakyThrows
    public Iterator<ReusableInputSource> getInputSources(FilenameFilter filter, String... paths) {
        // filter resources
        Collection<ReusableInputSource> filteredResources = new HashSet<>();
        String[] groups = paths.length > 0 ? paths : resources.keySet().toArray(new String[0]);

        for (String path : groups) {
            addFilteredResource(path, filter, filteredResources);
        }

        return filteredResources.iterator();
    }


    private void addFilteredResource(String path, FilenameFilter filter,
        Collection<ReusableInputSource> filteredResources) {
        Collection<ReusableInputSource> found = resources.get(path);
        if (found != null) {
            for (ReusableInputSource source : found) {
                if (filter == null) {
                    filteredResources.add(source);
                } else {
                    applyFilter(source, filter, filteredResources);
                }
            }
        }
    }


    private void applyFilter(ReusableInputSource source, FilenameFilter filter,
        Collection<ReusableInputSource> filteredResources) {
        String systemId = source.getSystemId();
        if (filter.accept(null, systemId)) {
            filteredResources.add(source);
        }
    }


    @SneakyThrows
    private void collectResources(URL url) {
        log.debug("root resource url: " + url);

        if (url == null) {
            log.error("provided URL is null");
            return;
        }

        if (URL_PROTOCOL_FILE.equals(url.getProtocol())) {
            collectFromFile(url);
        } else if (URL_PROTOCOL_JAR.equals(url.getProtocol())) {
            collectFromJar(url);
        }
    }


    @SneakyThrows
    private void collectFromFile(URL url) {
        File root = new File(url.toURI());
        if (root.canRead()) {
            log.debug(
                "[util.TestResourceProvider.collectResourcesFS] "
                    + "collecting resources from:");
            log.debug(root.getAbsolutePath());

            collectResourcesFS(root, "__default__", 0);
        } else {
            log.error("cannot read file " + root.getAbsolutePath());
        }
    }


    @SneakyThrows
    private void collectFromJar(URL url) {
        // remove 'jar:' from start of string representation
        String urlString = url.toString();
        String path;

        if (PATT_WIN_FILE.matcher(urlString).find()) {
            // windows-style path
            path = urlString.substring(JAR_PREFIX_WIN_LENGTH, urlString.indexOf('!'));
        } else {
            // *nix-style path
            path = urlString.substring(JAR_PREFIX_NIX_LENGTH, urlString.indexOf('!'));
        }

        File root = new File(path);
        if (root.canRead()) {
            JarFile jarFile = new JarFile(root);

            log.debug("changed root url: " + path);
            log.debug(
                "[util.TestResourceProvider.collectResourcesJAR] "
                    + "collecting resources from:");
            log.debug(jarFile.getName());

            collectResourcesJAR(jarFile);
        } else {
            log.error("cannot read file " + root.getAbsolutePath());
        }
    }


    @SneakyThrows
    private void collectResourcesFS(File dir, String group, int level) {
        if (dir == null) {
            return;
        }

        for (File child : Objects.requireNonNull(dir.listFiles())) {
            if (child.isDirectory()) {
                if (level == 0) {
                    collectResourcesFS(child, child.getName(), level + 1);
                } else {
                    collectResourcesFS(child, group, level + 1);
                }
            } else {
                if (child.getName().endsWith(".xml")) {
                    ReusableInputSource source = new ReusableInputSource(new FileInputStream(child));
                    source.setSystemId(child.getAbsolutePath());
                    resources.computeIfAbsent(group, k -> new HashSet<>()).add(source);
                }
            }
        }
    }


    @SneakyThrows
    private void collectResourcesJAR(JarFile jarFile) {
        for (Enumeration<JarEntry> entries = jarFile.entries(); entries.hasMoreElements(); ) {
            JarEntry jarEntry = entries.nextElement();

            if (!jarEntry.isDirectory()) {
                String entryName = jarEntry.getName();
                Matcher m = PATT_LDML_FILE.matcher(entryName);

                if (m.matches()) {
                    String group = m.group(1);
                    ReusableInputSource source = new ReusableInputSource(jarFile.getInputStream(jarEntry));
                    source.setSystemId(entryName);
                    resources.computeIfAbsent(group, k -> new HashSet<>()).add(source);
                }
            }
        }
    }

}
