// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.StringWriter;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Testsuite for {@link DocumentDataSetWriter}.
 */
class DocumentDataSetWriterTest {

    @SuppressWarnings("unused")
    private static Stream<Arguments> provideTextTypeDataItems() {
        return Stream.of(
            Arguments.of(TextTypeDataItem.builder()
                    .count(1)
                    .parentId("E00000004")
                    .content("Referentenentwurf")
                    .build(),
                "T00000001  text      E00000004  Referentenentwurf",
                "text:T00000001;parent:E00000004;content:'Referentenentwurf'"),
            Arguments.of(TextTypeDataItem.builder()
                    .count(2)
                    .parentId("E00000005")
                    .content("des Bundesministeriums für Wirtschaft und Energie")
                    .build(),
                "T00000002  text      E00000005  des␣Bundesministeriums␣für␣Wirtschaft␣und␣Energie",
                "text:T00000002;parent:E00000005;content:'des␣Bundesministeriums␣für␣Wirtschaft␣und␣Energie'"),
            Arguments.of(TextTypeDataItem.builder()
                    .count(4)
                    .parentId("E00000007")
                    .content("(Akkreditierungsstellengesetz - ")
                    .build(),
                "T00000004  text      E00000007  (Akkreditierungsstellengesetz␣-␣",
                "text:T00000004;parent:E00000007;content:'(Akkreditierungsstellengesetz␣-␣'"),
            Arguments.of(TextTypeDataItem.builder()
                    .count(6)
                    .parentId("E00000007")
                    .content(")")
                    .build(),
                "T00000006  text      E00000007  )",
                "text:T00000006;parent:E00000007;content:')'"),
            Arguments.of(TextTypeDataItem.builder()
                    .count(7)
                    .parentId("E0000000A")
                    .content("Vom ... ")
                    .build(),
                "T00000007  text      E0000000A  Vom␣...␣",
                "text:T00000007;parent:E0000000A;content:'Vom␣...␣'")
        );
    }


    @SuppressWarnings("unused")
    private static Stream<Arguments> provideElementTypeDataItems() {
        return Stream.of(
            Arguments.of(ElementTypeDataItem.builder()
                    .count(1)
                    .name("bill")
                    .isElement(false)
                    .attributes(new String[]{"name", "regelungstext"})
                    .build(),
                "D00000001  document             bill                                                "
                    + "@name=regelungstext",
                "document:D00000001;tag:bill;attributes:{@name='regelungstext'}"),
            Arguments.of(ElementTypeDataItem.builder()
                    .count(2)
                    .name("longTitle")
                    .isElement(true)
                    .parentId("E00000001")
                    .build(),
                "E00000002  element   E00000001  longTitle",
                "element:E00000002;parent:E00000001;tag:longTitle;attributes:{}"),
            Arguments.of(ElementTypeDataItem.builder()
                    .count(8)
                    .name("inline")
                    .isElement(true)
                    .parentId("E00000007")
                    .attributes(new String[]{"refersTo", "amtliche-abkuerzung"})
                    .build(),
                "E00000008  element   E00000007  inline                                              @refersTo=amtliche-abkuerzung",
                "element:E00000008;parent:E00000007;tag:inline;attributes:{@refersTo='amtliche-abkuerzung'}"),
            Arguments.of(ElementTypeDataItem.builder()
                    .count(13)
                    .name("p")
                    .isElement(true)
                    .parentId("E0000000C")
                    .build(),
                "E0000000D  element   E0000000C  p",
                "element:E0000000D;parent:E0000000C;tag:p;attributes:{}")
        );
    }


    @SuppressWarnings("unused")
    @ParameterizedTest
    @MethodSource("provideElementTypeDataItems")
    void testElementTypeFixedFormatLine(ElementTypeDataItem dataItem, String expectedFixedFormat,
        String expectedDSVStyle) {
        StringWriter stringWriter = new StringWriter();
        FixedFormatDocumentDataSetWriter writer = new FixedFormatDocumentDataSetWriter(stringWriter);
        DocumentDataSet dataSet = new DocumentDataSet();
        dataSet.addDataItem(dataItem);

        writer.write(dataSet);
        assertEquals(expectedFixedFormat, stringWriter.toString().trim());
    }


    @SuppressWarnings("unused")
    @ParameterizedTest
    @MethodSource("provideElementTypeDataItems")
    void testElementTypeDSVStyleLine(ElementTypeDataItem dataItem, String expectedFixedFormat,
        String expectedDSVStyle) {
        StringWriter stringWriter = new StringWriter();
        DSVStyleDocumentDataSetWriter writer = new DSVStyleDocumentDataSetWriter(stringWriter);
        DocumentDataSet dataSet = new DocumentDataSet();
        dataSet.addDataItem(dataItem);

        writer.write(dataSet);
        assertEquals(expectedDSVStyle, stringWriter.toString().trim());
    }


    @SuppressWarnings("unused")
    @ParameterizedTest
    @MethodSource("provideTextTypeDataItems")
    void testTextTypeFixedFormatLine(TextTypeDataItem dataItem, String expectedFixedFormat,
        String expectedDSVStyle) {
        StringWriter stringWriter = new StringWriter();
        FixedFormatDocumentDataSetWriter writer = new FixedFormatDocumentDataSetWriter(stringWriter);
        DocumentDataSet dataSet = new DocumentDataSet();
        dataSet.addDataItem(dataItem);

        writer.write(dataSet);
        assertEquals(expectedFixedFormat, stringWriter.toString().trim());
    }


    @SuppressWarnings("unused")
    @ParameterizedTest
    @MethodSource("provideTextTypeDataItems")
    void testTextTypeDSVStyleLine(TextTypeDataItem dataItem, String expectedFixedFormat,
        String expectedDSVStyle) {
        StringWriter stringWriter = new StringWriter();
        DSVStyleDocumentDataSetWriter writer = new DSVStyleDocumentDataSetWriter(stringWriter);
        DocumentDataSet dataSet = new DocumentDataSet();
        dataSet.addDataItem(dataItem);

        writer.write(dataSet);
        assertEquals(expectedDSVStyle, stringWriter.toString().trim());
    }


    @Test
    void testGetTypeString() {
        DocumentDataSetWriter writer = new FixedFormatDocumentDataSetWriter(null);
        assertEquals("document", writer.getTypeString(DocumentDataItem.ItemType.DOCUMENT));
        assertEquals("text    ", writer.getTypeString(DocumentDataItem.ItemType.TEXT));
        assertEquals("element ", writer.getTypeString(DocumentDataItem.ItemType.ELEMENT));
    }


    @Test
    void testDifferentDelimiter() {
        StringWriter stringWriter = new StringWriter();
        DSVStyleDocumentDataSetWriter writer = new DSVStyleDocumentDataSetWriter(stringWriter,
            DSVStyleDocumentDataSetWriter.Delimiter.COLON);
        DocumentDataSet dataSet = new DocumentDataSet();
        dataSet.addDataItem(
            ElementTypeDataItem.builder()
                .count(8)
                .name("inline")
                .isElement(true)
                .parentId("E00000007")
                .attributes(new String[]{"refersTo", "amtliche-abkuerzung"})
                .build()
        );
        writer.write(dataSet);
        String line = stringWriter.toString();
        assertNotNull(line);
        assertEquals("element:E00000008:parent:E00000007:tag:inline:attributes:{@refersTo='amtliche-abkuerzung'}\n",
            line);
    }

}
