// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Testsuite for {@link WhitespaceSubstitution}.
 */
@Log4j2
class WhitespaceSubstitutionTest {

    private static final WhitespaceSubstitution substitution = WhitespaceSubstitution.getInstance();


    @SuppressWarnings("unused")
    private static Stream<String> provideTestStringsSetA() {
        return Stream.of("", "ABC", "xy_0815", "abc-123", "....");
    }


    @SuppressWarnings("unused")
    private static Stream<Arguments> provideTestStringsSetB() {
        return Stream.of(
            Arguments.of(" ", "␣"),
            Arguments.of("AB C", "AB␣C"),
            Arguments.of("  xy_0815", "␣␣xy_0815"),
            Arguments.of("abc  123", "abc␣␣123"),
            Arguments.of("   .... ", "␣␣␣....␣"));
    }


    @SuppressWarnings("unused")
    private static Stream<Arguments> provideTestStringsSetC() {
        return Stream.of(
            Arguments.of(
                "Der Bundestag hat das folgende Gesetz beschlossen:",
                "Der␣Bundestag␣hat␣das␣folgende␣Gesetz␣beschlossen:"
            ),
            Arguments.of(
                "Vom ... ",
                "Vom␣...␣"
            ),
            Arguments.of(
                "(Akkreditierungsstellengesetz - ",
                "(Akkreditierungsstellengesetz␣-␣"
            ),
            Arguments.of(
                "(Akkreditierungsstellengesetz\u00A0-\u00A0",
                "(Akkreditierungsstellengesetz☐-☐"
            ),
            Arguments.of(
                " des Bundesministeriums für Wirtschaft und Energie",
                "␣des␣Bundesministeriums␣für␣Wirtschaft␣und␣Energie"
            ),
            Arguments.of(
                " an Konformitätsbewertungsstellen konkretisieren oder ergänzen, ",
                "␣an␣Konformitätsbewertungsstellen␣konkretisieren␣oder␣ergänzen,␣"
            ),
            Arguments.of(
                "2.",
                "2."
            ),
            Arguments.of(
                "\n"
                    + "                  Abschnitt 1 Allgemeines\n"
                    + "               ",
                "↵␣␣␣␣␣␣␣␣␣␣␣␣␣␣␣␣␣␣Abschnitt␣1␣Allgemeines↵␣␣␣␣␣␣␣␣␣␣␣␣␣␣␣"
            ),
            Arguments.of(
                " des Bundesministeriums für\n"
                    + "                        Wirtschaft und Energie",
                "␣des␣Bundesministeriums␣für↵␣␣␣␣␣␣␣␣␣␣␣␣␣␣␣␣␣␣␣␣␣␣␣␣Wirtschaft␣und␣Energie"
            ),
            Arguments.of(
                "\tTEXT\r\nTEXT\u00A0\u00A0\r\n.",
                "⭾TEXT↵TEXT☐☐↵."
            )
        );
    }


    @ParameterizedTest
    @MethodSource("provideTestStringsSetA")
    void testConvertIdentical(String testString) {
        assertEquals("", substitution.convert(null));
        assertEquals(testString, substitution.convert(testString));
    }


    @ParameterizedTest
    @MethodSource("provideTestStringsSetB")
    void testConvertSpaces(String testString, String expected) {
        assertEquals(expected, substitution.convert(testString));
    }


    @ParameterizedTest
    @MethodSource("provideTestStringsSetC")
    void testConvertWhitespaceMix(String testString, String expected) {
        String converted = substitution.convert(testString);
        log.debug(String.format("'%s' => '%s'", testString, converted));
        assertEquals(expected, converted);
    }

}
