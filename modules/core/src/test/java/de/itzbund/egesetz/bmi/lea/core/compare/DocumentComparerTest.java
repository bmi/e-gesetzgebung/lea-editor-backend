// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import org.junit.jupiter.api.Test;

import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Testsuite for {@link DocumentComparer}.
 */
class DocumentComparerTest {

    @Test
    void testCompareJsonJsonEqual() {
        InputStream doc1 = DocumentComparerTest.class.getResourceAsStream("/compare/min-doc-1a.json");
        InputStream doc2 = DocumentComparerTest.class.getResourceAsStream("/compare/min-doc-1b.json");
        DocumentComparer dc = new DocumentComparer();
        assertEquals(DocumentComparer.CompareResult.EQUAL,
            dc.compare(DocumentComparer.DocumentType.JSON_DOCUMENT, doc1,
                DocumentComparer.DocumentType.JSON_DOCUMENT, doc2,
                DocumentComparer.IntermediateFormat.FIXED_FORMAT));
    }


    @Test
    void testCompareXmlXmlEqual() {
        InputStream doc1 = DocumentComparerTest.class.getResourceAsStream("/compare/min-doc-1a.xml");
        InputStream doc2 = DocumentComparerTest.class.getResourceAsStream("/compare/min-doc-1b.xml");
        DocumentComparer dc = new DocumentComparer();
        assertEquals(DocumentComparer.CompareResult.EQUAL,
            dc.compare(DocumentComparer.DocumentType.XML_DOCUMENT, doc1,
                DocumentComparer.DocumentType.XML_DOCUMENT, doc2,
                DocumentComparer.IntermediateFormat.FIXED_FORMAT));
    }


    @Test
    void testCompareXmlJsonEqual() {
        InputStream doc1 = DocumentComparerTest.class.getResourceAsStream("/compare/min-doc-1a.xml");
        InputStream doc2 = DocumentComparerTest.class.getResourceAsStream("/compare/min-doc-2.json");
        DocumentComparer dc = new DocumentComparer();
        assertEquals(DocumentComparer.CompareResult.EQUAL,
            dc.compare(DocumentComparer.DocumentType.XML_DOCUMENT, doc1,
                DocumentComparer.DocumentType.JSON_DOCUMENT, doc2,
                DocumentComparer.IntermediateFormat.FIXED_FORMAT));
    }


    @Test
    void testCompareXmlJsonDifferent() {
        InputStream doc1 = DocumentComparerTest.class.getResourceAsStream("/compare/min-doc-2.xml");
        InputStream doc2 = DocumentComparerTest.class.getResourceAsStream("/compare/min-doc-2.json");
        DocumentComparer dc = new DocumentComparer();
        assertEquals(DocumentComparer.CompareResult.DIFFERENT,
            dc.compare(DocumentComparer.DocumentType.XML_DOCUMENT, doc1,
                DocumentComparer.DocumentType.JSON_DOCUMENT, doc2,
                DocumentComparer.IntermediateFormat.FIXED_FORMAT));
    }

}
