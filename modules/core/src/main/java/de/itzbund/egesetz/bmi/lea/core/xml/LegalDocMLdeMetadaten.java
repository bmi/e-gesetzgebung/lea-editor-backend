// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.xml;

import java.util.EnumMap;
import java.util.Map;

public class LegalDocMLdeMetadaten {

    private Map<LegalDocMLdeMetadatenFeld, String> values;


    public String getMetadatum(LegalDocMLdeMetadatenFeld name) {
        if (values == null) {
            return "";
        }

        return values.computeIfAbsent(name, k -> "");
    }


    public void setMetadatum(LegalDocMLdeMetadatenFeld name, String wert) {
        if (values == null) {
            values = new EnumMap<>(LegalDocMLdeMetadatenFeld.class);
        }

        values.put(name, wert);
    }

}
