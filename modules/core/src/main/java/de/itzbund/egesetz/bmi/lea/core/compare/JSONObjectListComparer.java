// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import de.itzbund.egesetz.bmi.lea.core.json.TraversableJSONDocument;
import lombok.RequiredArgsConstructor;
import org.json.simple.JSONObject;

import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getGuid;

@RequiredArgsConstructor
@SuppressWarnings("unused")
public class JSONObjectListComparer extends ObjectListComparer<JSONObject, UUID> {

    private final TraversableJSONDocument baseDocument;
    private final TraversableJSONDocument versionDocument;

    @Override
    public UUID getKey(JSONObject jsonObject) {
        return UUID.fromString(getGuid(jsonObject));
    }

}
