// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.util;

import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.CHANGE_MARK;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getGuid;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getStringAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;

/**
 * Utility class that contains static methods that deliver functionality of general use.
 */

@Log4j2
public class Utils {

    public static final int RANDOM_STRING_MIN_LENGTH = 0x0001;
    public static final int RANDOM_STRING_MAX_LENGTH = 0x0010;
    protected static final List<String> LIST_OF_ADJECTIVES = getWordsFromFile("/words/adjectives.txt");
    protected static final List<String> LIST_OF_NAMES = getWordsFromFile("/words/names.txt");
    private static final int DIGITS_OFFSET = 48;
    private static final int UPCASE_CHARS_OFFSET = 65;
    private static final int LOCASE_CHARS_OFFSET = 97;
    private static final int NUM_DIGITS = 10;
    private static final int NUM_CHARS = 26;
    // used in reduceString() as symbol for left out text
    private static final String ELLIPSIS = "...";
    // used in reduceString() to determine the minimum prefix length before the ellipsis
    private static final int TEXT_MIN_LENGTH = 3;
    private static final int TEXT_LENGTH_DELTA = 5;
    private static final int ARRAY_SIZE_FACTOR = 2;
    private static final int RND_LIST_MAX_SIZE = 20;
    private static final int RND_VERSION_MAX_VALUE = 10;
    private static final char RANDOM_STRING_DELIMITER = ' ';

    private static final List<Character> DELIMITERS = List.of('-', '_');

    private static final List<Character> LOCASE_CHARS =
        IntStream.range(LOCASE_CHARS_OFFSET, LOCASE_CHARS_OFFSET + NUM_CHARS)
            .boxed()
            .map(i -> (char) i.intValue())
            .collect(Collectors.toList());

    private static final List<Character> UPCASE_CHARS =
        IntStream.range(UPCASE_CHARS_OFFSET, UPCASE_CHARS_OFFSET + NUM_CHARS)
            .boxed()
            .map(i -> (char) i.intValue())
            .collect(Collectors.toList());

    private static final List<Character> DIGITS =
        IntStream.range(DIGITS_OFFSET, DIGITS_OFFSET + NUM_DIGITS)
            .boxed()
            .map(i -> (char) i.intValue())
            .collect(Collectors.toList());

    private static final JSONParser JSON_PARSER = new JSONParser();
    private static final SecureRandom random = new SecureRandom();


    private Utils() {
    }


    public static TransformerFactory getTransformerFactory() {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        return transformerFactory;
    }


    public static DocumentBuilderFactory getDocumentBuilderFactory() {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            // Timothy Morgan's 2014 paper: "XML Schema, DTD, and Entity Attacks"
            documentBuilderFactory.setXIncludeAware(false);
            documentBuilderFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, Boolean.TRUE);
            documentBuilderFactory.setNamespaceAware(true);
        } catch (ParserConfigurationException e) {
            log.error(e);
        }
        return documentBuilderFactory;
    }


    @SuppressWarnings("java:S1067")
    public static <T> List<T> replaceItemBy(List<T> oldItems, int index, List<T> newItems) {
        if (oldItems == null || index < 0 || oldItems.isEmpty() || index >= oldItems.size() || newItems.isEmpty()) {
            return oldItems;
        }

        List<T> resultList = new ArrayList<>();

        int i = 0;
        while (i < index) {
            resultList.add(oldItems.get(i));
            i++;
        }

        int cut = i;
        i = 0;
        while (i < newItems.size()) {
            resultList.add(newItems.get(i));
            i++;
        }

        i = cut + 1;
        while (i < oldItems.size()) {
            resultList.add(oldItems.get(i));
            i++;
        }

        return resultList;
    }


    /**
     * This method is used in context of JSON generation, to put keys or values inside quotes.
     *
     * @param text the text to be quoted
     */
    public static String asString(String text) {
        return String.format("\"%s\"", text);
    }


    /**
     * Simulates the XML normalisation of text values. Any series of whitespace characters (0x09, 0x0A, 0x0D, 0x20) is replaced by a single space character
     * (0x20). Leading and trailing whitespace is eliminated. See also
     * <a href="https://www.w3.org/TR/xpath-functions-31/#func-normalize-space">fn:normalize-space</a>.
     *
     * @param text the text to be normalized
     * @return the normalized text without sequences of whitespace
     */
    public static String xmlNormalize(String text) {
        if (text == null) {
            return null;
        } else {
            return text.replaceAll("\\s+", " ");
        }
    }

    public static String xmlLinearize(String text) {
        if (Utils.isMissing(text)) {
            return "";
        } else {
            return text.replaceAll("\\r?\\n\\s*", " ")
                .replace("> <", "><")
                .trim();
        }
    }


    /**
     * Returns the XML content from a {@link ReusableInputSource} object as string.
     *
     * @param source the ReusableInputSource containing the XML content
     * @return the XML serialization as {@link String}
     */
    public static String getContent(ReusableInputSource source) {
        if (source == null) {
            return "";
        }

        StringWriter writer = new StringWriter();

        try {
            IOUtils.copy(source.getByteStream(), writer, StandardCharsets.UTF_8.name());
        } catch (IOException e) {
            log.error(e);
        }

        return writer.toString();
    }


    /**
     * Reads the textual content of some resource specified by path and returns it as string.
     *
     * @param path the file path of the resource to read
     * @return the resource's content as {@link String}.
     */
    @SneakyThrows
    public static String getContentOfTextResource(String path) {
        InputStream data = Utils.class.getResourceAsStream(path);

        if (data == null) {
            log.error("could not load content from path " + path);
            return null;
        } else {
            StringWriter writer = new StringWriter();
            IOUtils.copy(data, writer, StandardCharsets.UTF_8.name());
            return writer.toString();
        }
    }


    /**
     * Replaces the given string by a copy which is a prefix of the original but at most {@code length} characters long. Used for XML or JSON content in Debug
     * Messages mainly.
     *
     * @param text   the text to shorten
     * @param length the number of characters the text should not exceed
     * @return the shortened text, or the original if it is not longer then {@code length} characters
     */
    public static String reduceString(String text, int length) {
        int eps = ELLIPSIS.length();

        if (text == null || text.isBlank() || length < (TEXT_MIN_LENGTH + eps) || text.length() <= length) {
            return text;
        } else {
            return String.format("%s%s", text.substring(0, length - eps), ELLIPSIS);
        }
    }


    /**
     * Tests whether the given object is "missing", i.e. has no business-relevant value. All null-References are interpreted as missing. Especially Strings that
     * are blank (contain nothing but whitespace) are considered missing. All other situations are meant to be not missing.
     *
     * @param object the object to check
     * @return true if this object is null or in case it is a string whether it is blank, false otherwise
     */
    public static boolean isMissing(Object object) {
        if (object == null) {
            return true;
        } else if (object instanceof String) {
            return ((String) object).isBlank();
        } else {
            return false;
        }
    }


    /**
     * Checks whether the given string represents a valid JSON structure
     *
     * @param jsonInString input string to test for valid JSON format
     * @return true if the string content can be parsed into correct JSON object, false otherwise
     */
    public static boolean isJSONValid(String jsonInString) {
        try {
            JSON_PARSER.parse(jsonInString);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }


    /**
     * Checks whether the given string represents a valid XML structure
     *
     * @param xmlAsString input string to test for valid XML format
     * @return true if the string content can be parsed into correct XML, false otherwise
     */
    public static boolean isXMLValid(String xmlAsString) {
        if (xmlAsString == null) {
            throw new IllegalArgumentException("XML content must not be null or empty");
        }

        try {
            SAXParserFactory.newDefaultInstance()
                .newSAXParser()
                .getXMLReader()
                .parse(new InputSource(new StringReader(xmlAsString)));
        } catch (ParserConfigurationException | SAXException | IOException e) {
            log.error(e);
            return false;
        }
        return true;
    }


    public static String getPrettyPrintedXml(String input) {
        return getPrettyPrintedXml(new StringReader(input));
    }


    /**
     * Returns pretty-printed XML content wrapped in a writer.
     *
     * @param input a reader to read the original XML content
     * @return pretty-printed XML content wrapped in a writer
     */
    public static String getPrettyPrintedXml(Reader input) {
        String prettyXml = "";

        if (input == null) {
            return prettyXml;
        }

        try {
            SAXSource source = new SAXSource(new InputSource(input));

            Transformer transformer = getTransformerFactory().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, StandardCharsets.UTF_8.name());
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            StreamResult sr = new StreamResult(new OutputStreamWriter(bos, StandardCharsets.UTF_8));
            transformer.transform(source, sr);
            prettyXml = bos.toString(StandardCharsets.UTF_8);
        } catch (IllegalArgumentException | TransformerException e) {
            log.error("Pretty-print of XML content failed.", e);
        }

        return prettyXml;
    }


    /**
     * Unmask text, i.e. replace unnecessary backslashes
     *
     * @param text original text
     * @return unmasked text
     */
    @SuppressWarnings("java:S5361")
    public static String unmask(String text) {
        return text == null
            ? null
            : text.replaceAll("\\\\\\\\", "\\\\");
    }


    @SneakyThrows
    private static List<String> getWordsFromFile(String path) {
        InputStream inputStream = Utils.class.getResourceAsStream(path);
        assert inputStream != null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
        return reader.lines()
            .collect(Collectors.toList());
    }


    public static String getRandomString() {
        return String.format("%s_%s",
            LIST_OF_ADJECTIVES.get(random.nextInt(LIST_OF_ADJECTIVES.size())),
            LIST_OF_NAMES.get(random.nextInt(LIST_OF_NAMES.size())));
    }


    public static String getRandomString(int length, int flags) {
        StringBuilder sb = new StringBuilder();
        sb.append(getRandomString());

        if (flags == RANDOM_STRING_MIN_LENGTH) {
            // return random string at least as long as 'length'
            while (sb.length() < length) {
                sb.append(RANDOM_STRING_DELIMITER)
                    .append(getRandomString());
            }
            return sb.toString();
        } else if (flags == RANDOM_STRING_MAX_LENGTH) {
            // return random string at most as long as 'length' but near that limit
            String candidate = LIST_OF_NAMES.get(random.nextInt(LIST_OF_NAMES.size()));
            while (sb.length() < length - TEXT_LENGTH_DELTA) {
                candidate = sb.toString();
                sb.append(RANDOM_STRING_DELIMITER)
                    .append(getRandomString());
            }
            return sb.length() > length ? candidate : sb.toString();
        } else if (flags == 0) {
            // return short and simple random string
            return sb.toString();

        } else {
            // return random string exactly as long as 'length'
            while (sb.length() < length) {
                sb.append(RANDOM_STRING_DELIMITER)
                    .append(getRandomString());
            }
            return sb.length() > length ? reduceString(sb.toString(), length) : sb.toString();
        }
    }


    public static String bytesToHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder(ARRAY_SIZE_FACTOR * bytes.length);
        for (byte aByte : bytes) {
            String hex = Integer.toHexString(0xff & aByte);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString()
            .toUpperCase();
    }


    public static byte[] getHash(DigestAlgorithm algorithm, String string, Charset charset) {
        return getHash(algorithm, string.getBytes(charset));
    }


    public static byte[] getHash(DigestAlgorithm algorithm, byte[] bytes) {
        try {
            MessageDigest md = MessageDigest.getInstance(algorithm.getId());
            return md.digest(bytes);

        } catch (NoSuchAlgorithmException e) {
            log.error(e);
        }
        return new byte[]{};
    }


    public static boolean findLine(Map<String, String> textLines, String line) {
        return textLines.containsKey(Utils.bytesToHex(Utils.getHash(Utils.DigestAlgorithm.SHA256, line,
            StandardCharsets.UTF_8)));
    }


    public static boolean findLines(Map<String, String> textLines, boolean findAll, String... lines) {
        boolean found = findAll;
        for (String line : lines) {
            if (findAll) {
                found &= findLine(textLines, line);
            } else {
                found |= findLine(textLines, line);
            }
        }
        return found;
    }


    public static String base64Encode(@NonNull String text) {
        return Base64.getEncoder()
            .encodeToString(text.getBytes(StandardCharsets.UTF_8));
    }


    public static String base64Decode(@NonNull String text) {
        return new String(Base64.getDecoder()
            .decode(text), StandardCharsets.UTF_8);
    }


    public static Map<String, String> getLines(String text) {
        Map<String, String> map = new HashMap<>();
        text.lines()
            .map(String::trim)
            .forEach(line -> map.put(
                Utils.bytesToHex(Utils.getHash(Utils.DigestAlgorithm.SHA256, line, StandardCharsets.UTF_8)),
                line));
        return map;
    }


    public static String getDefaultPrefix(int count) {
        return String.format("ns%s", Math.abs(count));
    }


    public static String getRandomPrefix(int minLength, int maxLength) {
        int min = Math.min(minLength, maxLength);
        int max = Math.max(minLength, maxLength);

        if (min < 1) {
            throw new IllegalArgumentException("a prefix must have at least one character");
        }

        int randomLength = min == max ?
            min :
            getRandomIntBetween(min, max);
        List<Character> validStartChars = Stream.of(LOCASE_CHARS, UPCASE_CHARS)
            .flatMap(Collection::stream)
            .collect(
                Collectors.toList());
        List<Character> validNameChars = Stream.of(LOCASE_CHARS, UPCASE_CHARS, DIGITS)
            .flatMap(Collection::stream)
            .collect(
                Collectors.toList());
        StringBuilder sb = new StringBuilder();

        sb.append(pickItem(validStartChars));
        for (int i = 1; i < randomLength; i++) {
            sb.append(pickItem(validNameChars));
        }

        return sb.toString();
    }


    public static <E> E pickItem(@NonNull List<E> list) {
        if (list.isEmpty()) {
            return null;
        }

        return list.get(getRandomIntBetween(0, list.size()));
    }


    public static <E extends Enum<E>> Enum<E> getRandomValue(Class<E> enumClass) {
        E[] values = enumClass.getEnumConstants();
        int randomIndex = random.nextInt(values.length);
        return values[randomIndex];
    }


    @SuppressWarnings("unused")
    public static boolean isUUID(String value) {
        if (value == null || value.isBlank()) {
            return false;
        }

        try {
            UUID uuid = UUID.fromString(value);
            return true;
        } catch (IllegalArgumentException e) {
            log.error(e);
            return false;
        }
    }


    public static long getRandomLong() {
        return random.nextLong();
    }


    /**
     * Returns a random list of long values.
     *
     * @param size the length of the list, i.e. the number of long values to generate; 0 or negative value means random length but at least 1
     * @return a list of long numbers
     */
    public static List<Long> getRandomListOfLong(int size) {
        int length = size <= 0 ?
            getRandomIntBetween(1, RND_LIST_MAX_SIZE) :
            size;
        return random.longs(length)
            .boxed()
            .collect(Collectors.toList());
    }


    public static UserData getRandomUserData() {
        String randomString = getRandomString();
        return UserData.builder()
            .gid(UUID.randomUUID()
                .toString())
            .name(getNameFromRandomString(randomString))
            .email(getEmailFromRandomString(randomString))
            .build();
    }


    private static String getNameFromRandomString(String randomString) {
        return Arrays.stream(randomString.split("_"))
            .map(Utils::capitalizeString)
            .collect(Collectors.joining(" "));
    }


    private static String getEmailFromRandomString(String randomString) {
        return randomString.replace("_", ".") + "@example.org";
    }


    public static String capitalizeString(String text) {
        if (text == null) {
            return null;
        } else if (text.isBlank()) {
            return "";
        } else {
            String first = text.substring(0, 1);
            String rest = text.substring(1);
            return first.toUpperCase() + rest;
        }
    }


    @SuppressWarnings({"java:S134", "java:S3776"})
    public static String getTitleCase(String string, boolean... lowerCase) {
        if (string == null) {
            return null;
        }

        if (string.isBlank()) {
            return "";
        }

        char[] chars = string.toCharArray();
        boolean isStart = true;
        boolean lowcase = lowerCase.length != 0 && lowerCase[0];

        int x = 0;
        while (chars[x] == ' ') {
            x++;
        }

        for (int i = x; i < chars.length; i++) {
            char c = chars[i];
            if (DELIMITERS.contains(c)) {
                isStart = true;
            } else {
                if (isStart) {
                    chars[i] = Character.toUpperCase(c);
                } else {
                    if (lowcase) {
                        chars[i] = Character.toLowerCase(c);
                    }
                }
                isStart = false;
            }
        }
        return String.copyValueOf(chars);
    }


    public static String getRandomVersion() {
        return String.format("%s.%s.%s",
            random.nextInt(RND_VERSION_MAX_VALUE),
            random.nextInt(RND_VERSION_MAX_VALUE),
            random.nextInt(RND_VERSION_MAX_VALUE));
    }


    /**
     * Replace all the tokens in an input using the algorithm provided for each.
     *
     * @param original     original string
     * @param tokenPattern the pattern to match with
     * @param converter    the conversion to apply
     * @return the substituted string
     */
    @SuppressWarnings("java:S103")
    public static String replaceTokens(String original, Pattern tokenPattern,
        Function<Matcher, String> converter) {
        int lastIndex = 0;
        StringBuilder output = new StringBuilder();
        Matcher matcher = tokenPattern.matcher(original);
        while (matcher.find()) {
            output.append(original, lastIndex, matcher.start())
                .append(converter.apply(matcher));

            lastIndex = matcher.end();
        }
        if (lastIndex < original.length()) {
            output.append(original, lastIndex, original.length());
        }
        return output.toString();
    }


    public static List<String> findAll(String original, String patternString) {
        return findAll(original, Pattern.compile(patternString), 0);
    }


    public static List<String> findAll(String original, String patternString, int matchGroup) {
        return findAll(original, Pattern.compile(patternString), matchGroup);
    }


    public static List<String> findAll(String original, Pattern pattern) {
        return findAll(original, pattern, 0);
    }


    public static List<String> findAll(String original, Pattern pattern, int matchGroup) {
        List<String> matches = new ArrayList<>();

        Matcher matcher = pattern.matcher(original);
        while (matcher.find()) {
            matches.add(matcher.group(matchGroup));
        }

        return matches;
    }


    public static List<Integer> range(int min, boolean minInclude, int max, boolean maxInclude) {
        List<Integer> nums = new ArrayList<>();
        if (min < 0 || max < min) {
            return nums;
        } else {
            int start = minInclude ? min : min + 1;
            int end = maxInclude ? max : max - 1;
            for (int i = start; i <= end; i++) {
                nums.add(i);
            }
        }

        return nums;
    }


    public static List<Integer> range(int min, int max) {
        return range(min, true, max, false);
    }


    public static <E> List<E> pad(List<E> list, E elem, int length) {
        List<E> resultList = list == null ? new ArrayList<>() : new ArrayList<>(list);

        int d = length - resultList.size();

        if (d > 0) {
            for (int i = 0; i < d; i++) {
                resultList.add(elem);
            }
        }

        return resultList;
    }


    public static <E> E getFirstOrDefault(Collection<E> collection, E defaultValue) {
        if (collection == null || collection.isEmpty()) {
            return defaultValue;
        } else {
            Optional<E> optionalElement = collection.stream()
                .findFirst();
            return optionalElement.orElse(defaultValue);
        }
    }


    public static <L, R> List<Pair<L, R>> zipList(List<L> leftElements, List<R> rightElements,
        boolean paddingAllowed, L leftDefault, R rightDefault) {
        if (leftElements == null || rightElements == null) {
            return new ArrayList<>();
        }

        int resultSize;
        if (!paddingAllowed && leftElements.size() != rightElements.size()) {
            resultSize = Math.min(leftElements.size(), rightElements.size());
        } else {
            resultSize = Math.max(leftElements.size(), rightElements.size());
        }

        List<Pair<L, R>> zippedList = new ArrayList<>();

        for (int i = 0; i < resultSize; i++) {
            L left = getElementAtIndex(leftElements, i, paddingAllowed, leftDefault);
            R right = getElementAtIndex(rightElements, i, paddingAllowed, rightDefault);
            zippedList.add(Pair.of(left, right));
        }

        return zippedList;
    }


    public static <E> E getElementAtIndex(List<E> list, int index, boolean useDefault, E defaultValue) {
        if (list == null || list.isEmpty() || index < 0) {
            return useDefault ? defaultValue : null;
        }

        if (index < list.size()) {
            return list.get(index);
        } else {
            return useDefault ? defaultValue : null;
        }
    }


    public static int getRandomIntBetween(int lowerBoundInclusive, int upperBoundExclusive) {
        if (upperBoundExclusive <= 0) {
            throw new IllegalArgumentException("bound must be positive");
        }

        if (lowerBoundInclusive >= upperBoundExclusive) {
            throw new IllegalArgumentException("bound must be greater than origin");
        }

        SecureRandom random = new SecureRandom();
        int bound = upperBoundExclusive - lowerBoundInclusive;
        return random.nextInt(bound) + lowerBoundInclusive;
    }

    public static String message(String messageString, String... values) {
        return MessageFormat.format(messageString, (Object[]) values);
    }

    @SneakyThrows
    public static void setReflectiveAttribute(Object object, String attribute, Object value) {
        Field field = object.getClass()
            .getDeclaredField(attribute);
        field.setAccessible(true);
        field.set(object, value);
    }


    public static boolean areAllNull(Object... objects) {
        return areAllNull(Arrays.asList(objects));
    }


    public static boolean areAllNull(List<Object> objects) {
        final boolean[] allNullSoFar = {true};
        objects.forEach(obj -> {
            if (allNullSoFar[0] && obj != null) {
                allNullSoFar[0] = false;
            }
        });
        return allNullSoFar[0];
    }


    @SafeVarargs
    public static <T> Set<T> combineSets(Set<T>... sets) {
        if (sets == null || sets.length == 0) {
            return Set.of();
        } else if (sets.length == 1) {
            return sets[0];
        } else {
            Set<T> resultSet = new HashSet<>();
            Arrays.stream(sets).forEach(resultSet::addAll);
            return resultSet;
        }
    }


    public static boolean xor(boolean firstCondition, boolean... furtherConditions) {
        int countTrues = firstCondition ? 1 : 0;
        for (boolean condition : furtherConditions) {
            if (condition) {
                countTrues++;
            }
        }
        return countTrues == 1;
    }


    public static boolean containsOneOfSubstrings(String text, Collection<String> substrings) {
        for (String s : substrings) {
            if (text.contains(s)) {
                return true;
            }
        }
        return false;
    }


    public static String printTable(List<Pair<JSONObject, JSONObject>> pairs) {
        List<List<String>> data = new ArrayList<>();

        pairs.forEach(p -> {
            List<String> row = new ArrayList<>();
            row.add(getCellContent(p.getLeft()));
            row.add(getCellContent(p.getRight()));
            data.add(row);
        });

        return writeTable(data);
    }

    private static String getCellContent(JSONObject jsonObject) {
        String type = getType(jsonObject);
        String content = getText(jsonObject);
        if (isMissing(content)) {
            content = getGuid(jsonObject);
        }
        String marker = getStringAttribute(jsonObject, CHANGE_MARK);
        if (marker == null) {
            marker = "";
        }
        return String.format("%s: %s [%s]", type, content, marker);
    }

    /*
     * Padding: array of up to 4 values
     * size == 4: 0 - top, 1 - right, 2 - bottom, 3 - left
     * size == 3: 0 - top, 1 - right and left, 2 - bottom
     * size == 2: 0 - top and bottom, 1 - right and left
     * size == 1: 0 - top and right and bottom and left
     */
    public static String writeTable(List<List<String>> data, int... padding) {
        StringBuilder sb = new StringBuilder();
        Padding pad = new Padding(padding);
        List<Integer> maxColSizes = new ArrayList<>();
        int maxColNum = 0;

        for (List<String> row : data) {
            int numberOfColumns = row.size();
            if (maxColNum < numberOfColumns) {
                maxColNum = numberOfColumns;
            }

            iterateColumns(maxColNum, maxColSizes, row);
        }
        assert maxColNum == maxColSizes.size();

        for (List<String> row : data) {
            appendRow(sb, row, maxColSizes, pad);
        }
        appendHRule(sb, maxColSizes, pad.getLeft(), pad.getRight());

        return sb.toString();
    }

    private static void iterateColumns(int maxColNum, List<Integer> maxColSizes, List<String> row) {
        for (int c = 0; c < maxColNum; c++) {
            int colLength = c < row.size() ? row.get(c).length() : 0;
            if (c < maxColSizes.size()) {
                if (colLength > maxColSizes.get(c)) {
                    maxColSizes.set(c, colLength);
                }
            } else {
                maxColSizes.add(colLength);
            }
        }
    }

    private static void appendRow(StringBuilder sb, List<String> row, List<Integer> maxColSizes, Padding padding) {
        int padLeft = padding.getLeft();
        int padRight = padding.getRight();

        appendHRule(sb, maxColSizes, padLeft, padRight);
        for (int i = 0; i < padding.getTop(); i++) {
            appendEmptyLine(sb, maxColSizes, padLeft, padRight);
        }

        String v = "|";
        String l = " ";
        for (int i = 0; i < maxColSizes.size(); i++) {
            int colSize = maxColSizes.get(i);
            String content = i < row.size() ? row.get(i) : "";
            sb.append(v).append(l.repeat(padLeft)).append(content).append(l.repeat(colSize - content.length())).append(l.repeat(padRight));
        }
        sb.append(v).append('\n');

        for (int i = 0; i < padding.getBottom(); i++) {
            appendEmptyLine(sb, maxColSizes, padLeft, padRight);
        }
    }

    private static void appendHRule(StringBuilder sb, List<Integer> maxColSizes, int padLeft, int padRight) {
        appendLine(sb, maxColSizes, "+-", padLeft, padRight);
    }

    private static void appendEmptyLine(StringBuilder sb, List<Integer> maxColSizes, int padLeft, int padRight) {
        appendLine(sb, maxColSizes, "| ", padLeft, padRight);
    }

    private static void appendLine(StringBuilder sb, List<Integer> maxColSizes, String symbols, int padLeft, int padRight) {
        String v = String.valueOf(symbols.charAt(0));
        String l = String.valueOf(symbols.charAt(1));
        maxColSizes.forEach(i -> sb.append(v).append(l.repeat(padLeft)).append(l.repeat(i)).append(l.repeat(padRight)));
        sb.append(v).append('\n');
    }


    @RequiredArgsConstructor
    @Getter
    public enum DigestAlgorithm {
        MD5("MD5"), SHA1("SHA-1"), SHA256("SHA-256");
        private final String id;
    }

    @Getter
    @AllArgsConstructor
    @Builder
    public static class UserData {

        private String gid;
        private String name;
        private String email;
    }

    @Getter
    @SuppressWarnings("java:S109")
    private static class Padding {

        private static final int MAX_PADDING_VALUES = 4;

        int top = 0;
        int right = 0;
        int bottom = 0;
        int left = 0;

        public Padding(int... padding) {
            assert padding == null || padding.length <= MAX_PADDING_VALUES;
            if (padding != null && padding.length > 0) {
                int padDim = padding.length;
                top = padding[0];
                bottom = padDim < 3 ? padding[0] : padding[2];
                right = padDim < 2 ? padding[0] : padding[1];
                left = padDim < 2 ? padding[0] : (
                    padDim > 3 ? padding[3] : padding[1]);
            }
        }
    }

}
