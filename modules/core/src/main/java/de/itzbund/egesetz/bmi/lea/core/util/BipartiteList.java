// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.util;

import lombok.Getter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

@SuppressWarnings("unused")
public class BipartiteList<E> {

    private static final String ERR_MSG = "index must be one of [0..%s], but was %s";

    @Getter
    private final List<E> elementList;

    private final Map<E, Integer> elements;

    @Getter
    private int size;

    private int index;


    public BipartiteList(List<E> elementList, int index) {
        this.index = index;
        this.elementList = elementList;
        this.elements = new HashMap<>();

        if (elementList != null && !elementList.isEmpty()) {
            checkIndex(index, elementList.size());
            for (int i = 0; i < elementList.size(); i++) {
                elements.put(elementList.get(i), i);
            }
        } else {
            throw new IllegalArgumentException("element list must not be empty");
        }
        this.size = elements.size();
    }


    public E getAtIndex() {
        return elementList.get(index);
    }


    public E get(int index) {
        return elementList.get(index);
    }


    public boolean contains(E element) {
        return elements.get(element) != null;
    }


    public boolean isPrevious(E element) {
        return isBefore(element) == 1;
    }


    public boolean isNext(E element) {
        return isAfter(element) == 1;
    }


    public int isBefore(E element) {
        Integer found = elements.get(element);
        if (found == null || found >= index) {
            return 0;
        } else {
            return index - found;
        }
    }


    public int isAfter(E element) {
        Integer found = elements.get(element);

        if (found == null || found <= index) {
            return 0;
        } else {
            return found - index;
        }
    }


    public boolean isAtIndex(E element) {
        Integer found = elements.get(element);
        return found != null && found == index;
    }


    public void setIndex(int newIndex) {
        checkIndex(newIndex, size);
        this.index = newIndex;
    }


    public void incrIndex() {
        setIndex(index + 1);
    }


    public void decrIndex() {
        setIndex(index - 1);
    }


    public void increaseIndex(int amount) {
        setIndex(index + amount);
    }


    public void decreaseIndex(int amount) {
        setIndex(index - amount);
    }


    private void checkIndex(int indexValue, int listSize) {
        if (indexValue < 0 || indexValue >= listSize) {
            throw new IllegalArgumentException(String.format(ERR_MSG, listSize - 1, indexValue));
        }
    }


    public boolean hasNext() {
        int nextIndex = index + 1;
        return nextIndex < elements.size();
    }


    public int getIndexOf(E element) throws NoSuchElementException {
        Integer i = elements.get(element);
        if (i != null) {
            return i;
        } else {
            throw new NoSuchElementException("index is null");
        }
    }


    public void exchange(E original, E substitute) {
        if (contains(original)) {
            int i = elements.get(original);
            elementList.remove(original);
            elementList.add(i, substitute);
            elements.remove(original);
            elements.put(substitute, i);
        }
    }


    public void insertAfter(E pivot, E elem) {
        if (pivot == null || elements.get(pivot) == null) {
            return;
        }

        int pivotIndex = elements.get(pivot) + 1;
        elementList.add(pivotIndex, elem);
        for (int i = pivotIndex; i < elementList.size(); i++) {
            elements.put(elementList.get(i), i);
        }

        assert elements.size() == elementList.size();
        size = elements.size();
    }


    public void prepend(E elem) {
        elementList.add(0, elem);
        for (int i = 0; i < elementList.size(); i++) {
            elements.put(elementList.get(i), i);
        }
        size = elementList.size();
    }

}
