// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core;

import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Properties;

/**
 * This class encapsulates the version info of this application which is automatically updated during the CI build. It provides some static methods to return
 * all or part of the version data.
 */

@Log4j2
public class VersionInfo {

    private static final Properties versionProps;

    static {
        versionProps = new Properties();
        try {
            versionProps.load(VersionInfo.class.getResourceAsStream("/version.properties"));
        } catch (IOException | NullPointerException | IllegalArgumentException e) {
            log.error("failed to load version info", e);
        }
    }

    private VersionInfo() {
    }


    /**
     * Returns a readable string containing the app name, its version and the Java version this application was compiled with.
     *
     * @return a verbose version string
     */
    public static String getVersionInfo() {
        return String.format("%s v%s built with Java %s",
            versionProps.getProperty("lea.backend.app.name"),
            versionProps.getProperty("lea.backend.app.version"), //NOSONAR
            versionProps.getProperty("lea.backend.java.version"));
    }


    /**
     * Returns the application's version set by the build system.
     *
     * @return the version of the application
     */
    public static String getAppVersion() {
        return versionProps.getProperty("lea.backend.app.version");
    } //NOSONAR

}
