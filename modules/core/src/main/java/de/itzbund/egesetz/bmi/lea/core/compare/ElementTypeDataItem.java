// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.Singular;
import lombok.ToString;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * An element-like data item. Those are items that represent hierarchy. At the end of the hierarchy they contain text. In general elements contain other
 * elements.
 */
@Getter
@Setter
@ToString
public class ElementTypeDataItem extends DocumentDataItem {

    private final String name;

    @Singular
    private final SortedMap<String, String> attributes;


    /**
     * Constructs an instance of a data item.
     *
     * @param count      the ID of this kind of data item in the document
     * @param name       some textual identifier like XML tag or JSON key
     * @param isElement  true, if this is a normal element, false if this is the start (root) of a document
     * @param parentId   a reference to the parent element if any, document data items have no parent
     * @param attributes varargs parameter to include attributes (as pairs of strings)
     */
    @Builder
    public ElementTypeDataItem(int count, String name, boolean isElement, String parentId, String... attributes) {
        super();
        this.itemType = isElement ? ItemType.ELEMENT : ItemType.DOCUMENT;
        makeId(this.itemType, count);
        this.parentId = parentId;
        this.name = name;
        this.attributes = new TreeMap<>();
        addAttributes(attributes);
    }


    public String getAttribute(String name) {
        return attributes.get(name);
    }


    public void setAttribute(String name, Object value) {
        if (value != null) {
            attributes.put(name, value.toString());
        }
    }


    /**
     * Pairs of strings are transformed into attributes like key=value. If a single string remains it it ignored.
     *
     * @param attributes varargs parameter for the attribute names and values
     */
    @SuppressWarnings({"java:S127", "java:S881", "java:S109"})
    public void addAttributes(String... attributes) {
        if (attributes == null) {
            return;
        }

        if (attributes.length > 1) {
            for (int k = 0; k < (attributes.length / 2 * 2); k++) {
                this.attributes.put(attributes[k++], attributes[k]);
            }
        }
    }

}
