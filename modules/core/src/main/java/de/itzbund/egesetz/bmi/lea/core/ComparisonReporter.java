// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core;

import de.itzbund.egesetz.bmi.lea.core.compare.ComparisonDataSet;

import java.util.Arrays;
import java.util.Collection;

/**
 * Abstract class to describe a Reporter of comparison results. Extending classes are expected to define some kind of report format.
 */
public interface ComparisonReporter {

    /**
     * The report method generates a comparison report from a collection of comparisons represented as instances of comparison datasets.
     *
     * @param dataSets a collection of comparison datasets
     */
    void report(Collection<ComparisonDataSet> dataSets);

    /**
     * Convenience method to specify the datasets in the varargs parameter.
     *
     * @param dataSets varargs parameter for selecting arbitrary many datasets
     */
    default void report(ComparisonDataSet... dataSets) {
        report(Arrays.asList(dataSets));
    }

}
