// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.json;

import de.itzbund.egesetz.bmi.lea.core.Hierarchy;
import de.itzbund.egesetz.bmi.lea.core.NavigableDocument;
import de.itzbund.egesetz.bmi.lea.core.UpdatableDocument;
import de.itzbund.egesetz.bmi.lea.core.util.BipartiteList;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_REFERSTO;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_AMENDMENTBODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ARTICLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_COLLECTIONBODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DEBATEBODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_JUDGMENTBODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_MAINBODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_META;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PORTIONBODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_CHILDREN;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.getLowestLevelElements;
import static de.itzbund.egesetz.bmi.lea.core.compare.CompareUtils.isLeave;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.GLIEDERUNGSEINHEITEN;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.HIERARCHY_ELEMENTS;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getFirstChild;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getGuid;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getLocalName;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getStringAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.hasOnlyText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.setGuid;

@Log4j2
public class TraversableJSONDocument
    implements NavigableDocument, UpdatableDocument, Hierarchy<UUID, JSONObject>, Iterator<JSONObject> {

    private static final Set<String> BODY_ELEMENTS = Stream.of(
            ELEM_AMENDMENTBODY, ELEM_COLLECTIONBODY, ELEM_DEBATEBODY, ELEM_BODY, ELEM_JUDGMENTBODY, ELEM_MAINBODY, ELEM_PORTIONBODY)
        .collect(Collectors.toCollection(HashSet::new));

    @Getter
    private JSONObject documentObject;
    @Getter
    private JSONObject currentObject;
    @Getter
    private JSONObject root;

    private Map<String, JSONObject> objectMap;
    private Map<String, String> parentMap;

    private Map<String, JSONObject> topLevelStructures;

    private Map<String, JSONObject> objectsWithRefersToAttr;

    private Map<String, List<JSONObject>> elementsByType;

    private Map<String, List<JSONObject>> hierarchySignatures;

    @Getter
    private List<JSONObject> hierarchyLevels;

    @Getter
    private List<JSONObject> gliederungseinheiten;

    @Getter
    private List<JSONObject> identifiableObjects;

    private BipartiteList<String> guidsInDocumentOrder;

    private BipartiteList<String> leaveObjects;

    private String startGuid;

    public TraversableJSONDocument(@NonNull JSONObject documentObject) {
        this.documentObject = documentObject;
        this.currentObject = getFirstChild(documentObject);
        initialize();
    }


    @Override
    public boolean isHierarchyLevel(JSONObject jsonObject) {
        return HIERARCHY_ELEMENTS.contains(getLocalName(getType(jsonObject)));
    }


    @Override
    public boolean isLegalNorm(JSONObject jsonObject) {
        return ELEM_ARTICLE.equals(getLocalName(getType(jsonObject)));
    }


    public boolean isGliederungseinheit(JSONObject jsonObject) {
        return GLIEDERUNGSEINHEITEN.contains(getLocalName(getType(jsonObject)));
    }


    @Override
    public List<JSONObject> getUnprocessedLevels(UUID key) {
        return hierarchySignatures.get(key.toString());
    }


    private enum KindOfSibling {FOLLOWING, PRECEDING}

    public enum RelativePosition {
        BEFORE, PREVIOUS, AT_POSITION, NEXT, AFTER, NOT_FOUND
    }


    public void setCurrentObject(JSONObject jsonObject) {
        String guid = getGuid(jsonObject);
        currentObject = jsonObject;
        guidsInDocumentOrder.setIndex(guidsInDocumentOrder.getElementList()
            .indexOf(guid));

        int index = leaveObjects.getElementList()
            .indexOf(guid);
        if (index >= 0) {
            leaveObjects.setIndex(index);
        }
    }

    @Override
    public JSONObject getObject(String guid) {
        return objectMap.get(guid);
    }

    @Override
    public List<JSONObject> getChildren() {
        return getChildren(currentObject);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<JSONObject> getChildren(JSONObject context) {
        return (List<JSONObject>) JSONUtils.getChildren(context)
            .stream()
            .map(o -> (JSONObject) o)
            .collect(Collectors.toList());
    }

    @Override
    public JSONObject getChild(String guid) {
        return getChild(currentObject, guid);
    }

    @Override
    public JSONObject getChild(JSONObject context, String guid) {
        JSONObject jsonObject = objectMap.get(guid);
        List<JSONObject> children = getChildren(context);
        return jsonObject == null ? null : children.get(children.indexOf(jsonObject));
    }

    @Override
    public JSONObject getChild(int index) {
        return getChild(currentObject, index);
    }

    @Override
    public JSONObject getChild(JSONObject context, int index) {
        return getChildren(context)
            .get(index);
    }

    @Override
    public JSONObject getParent() {
        return getParent(currentObject);
    }

    @Override
    public JSONObject getParent(JSONObject context) {
        return objectMap.get(parentMap.get(getGuid(context)));
    }

    @Override
    public List<JSONObject> getAncestors(boolean includeSelf) {
        return getAncestors(currentObject, includeSelf);
    }

    @Override
    public List<JSONObject> getAncestors(JSONObject context, boolean includeSelf) {
        List<JSONObject> ancestors = new ArrayList<>();
        if (includeSelf) {
            ancestors.add(context);
        }

        JSONObject parent = getParent(context);
        while (parent != null) {
            ancestors.add(parent);
            parent = getParent(parent);
        }

        return ancestors;
    }

    @Override
    public List<JSONObject> getDescendants(boolean includeSelf) {
        return getDescendants(currentObject, includeSelf);
    }

    @Override
    public List<JSONObject> getDescendants(JSONObject context, boolean includeSelf) {
        List<JSONObject> descendants = new ArrayList<>();
        if (includeSelf) {
            descendants.add(context);
        }

        if (!isLeave(context)) {
            List<JSONObject> children = getChildren(context);
            if (children != null && !children.isEmpty()) {
                children.forEach(child -> descendants.addAll(getDescendants(child, true)));
            }
        }

        return descendants;
    }

    @Override
    public List<JSONObject> getFollowingObjects() {
        return getFollowingObjects(currentObject);
    }

    @Override
    public List<JSONObject> getFollowingObjects(JSONObject context) {
        List<JSONObject> siblings = getSiblings(context).get(KindOfSibling.FOLLOWING);
        List<JSONObject> following = new ArrayList<>();

        siblings.forEach(s -> following.addAll(getDescendants(s, true)));

        return following;
    }

    @Override
    public List<JSONObject> getFollowingSiblings() {
        return getFollowingSiblings(currentObject);
    }

    @Override
    public List<JSONObject> getFollowingSiblings(JSONObject context) {
        return getSiblings(context).get(KindOfSibling.FOLLOWING);
    }

    @Override
    public List<JSONObject> getPrecedingObjects() {
        return getPrecedingObjects(currentObject);
    }

    @Override
    public List<JSONObject> getPrecedingObjects(JSONObject context) {
        List<JSONObject> siblings = getSiblings(context).get(KindOfSibling.PRECEDING);
        List<JSONObject> preceding = new ArrayList<>();

        siblings.forEach(s -> {
            List<JSONObject> desc = getDescendants(s, true);
            Collections.reverse(desc);
            preceding.addAll(desc);
        });

        return preceding;
    }

    @Override
    public List<JSONObject> getPrecedingSiblings() {
        return getPrecedingSiblings(currentObject);
    }

    @Override
    public List<JSONObject> getPrecedingSiblings(JSONObject context) {
        return getSiblings(context).get(KindOfSibling.PRECEDING);
    }

    private Map<KindOfSibling, List<JSONObject>> getSiblings(JSONObject context) {
        EnumMap<KindOfSibling, List<JSONObject>> map = new EnumMap<>(KindOfSibling.class);
        List<JSONObject> preceding = new ArrayList<>();
        List<JSONObject> following = new ArrayList<>();
        map.put(KindOfSibling.PRECEDING, preceding);
        map.put(KindOfSibling.FOLLOWING, following);

        if (!isRoot(context)) {
            JSONObject parent = getParent(context);
            List<JSONObject> children = getChildren(parent);
            int contextIndex = children.indexOf(context);

            for (int i = contextIndex - 1; i >= 0; i--) {
                preceding.add(children.get(i));
            }

            for (int i = contextIndex + 1; i < children.size(); i++) {
                following.add(children.get(i));
            }
        }

        return map;
    }

    @Override
    public Set<Pair<String, String>> getAttributes() {
        return getAttributes(currentObject);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Set<Pair<String, String>> getAttributes(JSONObject context) {
        return (Set<Pair<String, String>>) context.keySet()
            .stream()
            .filter(k -> !k.equals(JSON_KEY_CHILDREN))
            .map(k -> {
                String key = String.valueOf(k);
                String value = String.valueOf(context.get(k));
                return Pair.of(key, value);
            })
            .collect(Collectors.toSet());
    }

    @Override
    public String getAttribute(String key) {
        return getAttribute(currentObject, key);
    }

    @Override
    public String getAttribute(JSONObject context, String key) {
        Object v = context.get(key);
        return v == null ? "" : String.valueOf(v);
    }

    @Override
    public List<JSONObject> getAllLeaveObjects() {
        return leaveObjects.getElementList()
            .stream()
            .map(s -> objectMap.get(s))
            .collect(Collectors.toList());
    }

    @Override
    public List<JSONObject> getLeaveObjects() {
        return getLeaveObjects(currentObject);
    }

    @Override
    public List<JSONObject> getLeaveObjects(JSONObject context) {
        return getDescendants(context, false).stream()
            .filter(this::isLeaveObject)
            .collect(Collectors.toList());
    }

    @Override
    public boolean isLeaveObject() {
        return isLeaveObject(currentObject);
    }

    @Override
    public boolean isLeaveObject(JSONObject context) {
        return leaveObjects.contains(getGuid(context));
    }

    @Override
    public void advanceToNextLeave() throws NoSuchElementException {
        advanceCurrentObject();
        while (!isLeaveObject()) {
            advanceCurrentObject();
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void replaceBy(JSONObject original, JSONObject substitute) {
        JSONObject parent = getParent(original);
        if (parent == null || !isLeave(original) || !isLeave(substitute)) {
            return;
        }

        boolean concernsCurrentObject = currentObject.equals(original);
        JSONArray children = JSONUtils.getChildren(parent);
        int index = children.indexOf(original);

        children.remove(original);
        children.add(index, substitute);
        guidsInDocumentOrder.exchange(getGuid(original), getGuid(substitute));
        if (isLeaveObject(original)) {
            leaveObjects.exchange(getGuid(original), getGuid(substitute));
        }

        parentMap.remove(getGuid(original));
        parentMap.put(getGuid(substitute), getGuid(parent));
        if (concernsCurrentObject) {
            setCurrentObject(substitute);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void insertNewChild(JSONObject parent, JSONObject child, int index) {
        JSONArray children;
        if (parent == null || (children = JSONUtils.getChildren(parent)) == null || isLeave(parent)) {
            return;
        }

        JSONObject pivot;
        if (index < children.size()) {
            if (index <= 0) {
                List<JSONObject> precedingObjects = getPrecedingObjects((JSONObject) children.get(0));
                pivot = precedingObjects.isEmpty() ? parent : precedingObjects.get(0);
            } else {
                pivot = (JSONObject) children.get(index - 1);
            }
            children.add(Math.max(0, index), child);
        } else {
            pivot = (JSONObject) children.get(children.size() - 1);
            children.add(child);
        }

        guidsInDocumentOrder.insertAfter(getGuid(pivot), getGuid(child));
        leaveObjects.insertAfter(getGuid(pivot), getGuid(child));
        parentMap.put(getGuid(child), getGuid(parent));

        String refersTo = getStringAttribute(child, ATTR_REFERSTO);
        if (refersTo != null) {
            objectsWithRefersToAttr.put(refersTo, child);
        }
    }

    public void insertNewChild(JSONObject parent, JSONObject child) {
        JSONArray children = JSONUtils.getChildren(parent);
        if (children != null) {
            insertNewChild(parent, child, children.size());
        }
    }


    public boolean isRoot() {
        return isRoot(currentObject);
    }

    public boolean isRoot(JSONObject context) {
        return context != null && context.equals(documentObject);
    }

    public boolean isTopLevelObject() {
        return isTopLevelObject(currentObject);
    }

    public boolean isTopLevelObject(JSONObject context) {
        String type = getLocalName(getType(context));
        return topLevelStructures.get(type) != null;
    }

    public int getSize() {
        return guidsInDocumentOrder.getSize();
    }

    @Override
    public void advanceCurrentObject() throws NoSuchElementException {
        next();
    }

    @Override
    public boolean hasNext() {
        return guidsInDocumentOrder.hasNext();
    }

    @Override
    public JSONObject next() {
        if (hasNext()) {
            guidsInDocumentOrder.incrIndex();
            String guid = guidsInDocumentOrder.getAtIndex();
            currentObject = objectMap.get(guid);
            if (leaveObjects.contains(guid)) {
                leaveObjects.setIndex(leaveObjects.getIndexOf(guid));
            }
            return currentObject;
        } else {
            throw new NoSuchElementException();
        }
    }

    // resets iterating, i.e. set 'pointer' onto start position
    public void reset() {
        currentObject = objectMap.get(startGuid);
        guidsInDocumentOrder.setIndex(guidsInDocumentOrder.getIndexOf(startGuid));
        leaveObjects.setIndex(0);
    }

    public boolean contains(JSONObject jsonObject) {
        String guid = getGuid(jsonObject);
        if (guid == null || guid.isBlank()) {
            return false;
        } else {
            return guidsInDocumentOrder.contains(guid);
        }
    }

    public RelativePosition find(JSONObject jsonObject) {
        String guid = getGuid(jsonObject);
        if (guid == null || guid.isBlank()) {
            return RelativePosition.NOT_FOUND;
        } else if (guidsInDocumentOrder.contains(guid)) {
            if (guidsInDocumentOrder.isAtIndex(guid)) {
                return RelativePosition.AT_POSITION;
            } else if (guidsInDocumentOrder.isPrevious(guid)) {
                return RelativePosition.PREVIOUS;
            } else if (guidsInDocumentOrder.isBefore(guid) > 1) {
                return RelativePosition.BEFORE;
            } else if (guidsInDocumentOrder.isNext(guid)) {
                return RelativePosition.NEXT;
            } else {
                return RelativePosition.AFTER;
            }
        } else {
            return RelativePosition.NOT_FOUND;
        }
    }

    @SuppressWarnings("unused")
    public void setParent(@NonNull JSONObject child, @NonNull JSONObject parent) {
        parentMap.put(getGuid(child), getGuid(parent));
        objectMap.put(getGuid(child), child);
    }


    public List<JSONObject> getElementsOfType(String type) {
        return elementsByType.getOrDefault(getLocalName(type), new ArrayList<>());
    }


    public JSONObject getObjectWithRefersTo(@NotBlank String refersToAttr) {
        return objectsWithRefersToAttr.get(refersToAttr);
    }


    public JSONObject getLastOfType(String type) {
        List<JSONObject> elements = elementsByType.get(type);
        if (elements == null || elements.isEmpty()) {
            return null; // NOSONAR
        } else {
            return elements.get(elements.size() - 1);
        }
    }

    private void initialize() {
        objectMap = new HashMap<>();
        parentMap = new HashMap<>();
        topLevelStructures = new HashMap<>();
        objectsWithRefersToAttr = new HashMap<>();
        elementsByType = new HashMap<>();
        hierarchySignatures = new HashMap<>();
        hierarchyLevels = new ArrayList<>();
        gliederungseinheiten = new ArrayList<>();
        identifiableObjects = new ArrayList<>();
        root = documentObject;

        List<String> leaves = new ArrayList<>();
        List<String> guids = new ArrayList<>();
        List<JSONObject> ancestorLevels = new ArrayList<>();
        Map<String, Integer> hierarchyLevelCount = getNewHierarchyLevelCount();

        collectObjects(documentObject, null, guids, leaves, hierarchyLevelCount, ancestorLevels, false);
        if (!ancestorLevels.isEmpty()) {
            hierarchySignatures.put(null, new ArrayList<>(ancestorLevels));
        }

        setCurrentObject(guids);
        String currentGuid = getGuid(currentObject);
        guidsInDocumentOrder = new BipartiteList<>(guids, guids.indexOf(currentGuid));
        leaveObjects = new BipartiteList<>(leaves, Math.max(0, leaves.indexOf(currentGuid)));
    }

    @SuppressWarnings("unchecked")
    private void collectObjects(@NonNull JSONObject jsonObject, JSONObject parent,
        List<String> allGuids, List<String> leaveGuids, Map<String, Integer> hierarchyLevelCount,
        List<JSONObject> ancestorLevels, boolean foundBody) {
        String type = getLocalName(getType(jsonObject));
        String guid = getGuid(jsonObject);

        boolean hasFoundBody = BODY_ELEMENTS.contains(type) || foundBody;

        // ignore meta container
        if (ELEM_META.equals(type)) {
            return;
        }

        if (!Utils.isMissing(guid) && hasFoundBody) {
            identifiableObjects.add(jsonObject);
        }

        handleTopLevelStructures(jsonObject, type, guid, allGuids);
        objectMap.put(getGuid(jsonObject), jsonObject);
        parentMap.put(getGuid(jsonObject), parent == null ? null : getGuid(parent));
        registerElementByType(type, jsonObject);
        handleRefersToAttribute(jsonObject);
        handleHierarchy(jsonObject, type, hierarchyLevelCount, ancestorLevels);

        if (getLowestLevelElements().contains(type) || hasOnlyText(jsonObject)) {
            leaveGuids.add(guid);
        } else {
            JSONArray children = JSONUtils.getChildren(jsonObject);
            if (children != null) {
                children.forEach(obj -> {
                    JSONObject child = (JSONObject) obj;
                    collectObjects(child, jsonObject, allGuids, leaveGuids, hierarchyLevelCount, ancestorLevels, hasFoundBody);
                });
            }
        }

        if (isHierarchyLevel(jsonObject)) {
            resetLegalNormCount(hierarchyLevelCount);
        }
    }


    private void handleTopLevelStructures(JSONObject jsonObject, String type, String guid,
        List<String> allGuids) {
        if (guid == null) {
            topLevelStructures.put(type, jsonObject);
            setGuid(jsonObject, UUID.randomUUID());
            allGuids.add(getGuid(jsonObject));
        } else {
            allGuids.add(guid);
        }
    }


    private void handleRefersToAttribute(JSONObject jsonObject) {
        String refersTo = getStringAttribute(jsonObject, ATTR_REFERSTO);
        if (!Utils.isMissing(refersTo)) {
            objectsWithRefersToAttr.put(refersTo, jsonObject);
        }
    }


    private void handleHierarchy(JSONObject jsonObject, String type, Map<String, Integer> hierarchyLevelCount,
        List<JSONObject> ancestorLevels) {

        if (isHierarchyLevel(jsonObject)) {
            // extend hierarchy by this level
            hierarchyLevels.add(jsonObject);
            if (!JSONUtils.isLegalNorm(jsonObject)) {
                ancestorLevels.add(jsonObject);
                increaseCount(type, hierarchyLevelCount);
            }
        }

        if (isLegalNorm(jsonObject)) {
            // if first norm in current level then store hierarchy data
            increaseCount(type, hierarchyLevelCount);
            if (hierarchyLevelCount.get(type) == 1) {
                hierarchySignatures.put(getGuid(jsonObject), new ArrayList<>(ancestorLevels));
                ancestorLevels.clear();
            }
        }

        if (isGliederungseinheit(jsonObject)) {
            gliederungseinheiten.add(jsonObject);
        }
    }


    private void registerElementByType(String type, JSONObject jsonObject) {
        List<JSONObject> elementsOfType = elementsByType.computeIfAbsent(type, t -> new ArrayList<>());
        elementsOfType.add(jsonObject);
    }

    private void setCurrentObject(List<String> guids) {
        String currentGuid = getGuid(currentObject);
        int currentIndex = guids.indexOf(currentGuid);
        JSONObject jsonObject = objectMap.get(currentGuid);

        while (isTopLevelObject(jsonObject)) {
            currentIndex++;
            jsonObject = objectMap.get(guids.get(currentIndex));
        }

        currentObject = jsonObject;
        startGuid = getGuid(currentObject);
    }


    private Map<String, Integer> getNewHierarchyLevelCount() {
        Map<String, Integer> hierarchyLevelCount = new HashMap<>();
        HIERARCHY_ELEMENTS.forEach(h -> hierarchyLevelCount.put(h, 0));
        return hierarchyLevelCount;
    }


    private void increaseCount(String type, Map<String, Integer> hierarchyLevelCount) {
        int count = hierarchyLevelCount.get(type);
        count++;
        hierarchyLevelCount.put(type, count);
    }


    private void resetLegalNormCount(Map<String, Integer> hierarchyLevelCount) {
        hierarchyLevelCount.put(ELEM_ARTICLE, 0);
    }

}
