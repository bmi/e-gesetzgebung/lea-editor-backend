// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import lombok.Getter;
import lombok.Setter;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;

import java.util.List;

/**
 * POJO class to describe comparison data, two file references for the documents to compare and the outcome of the comparison.
 */

@Getter
@Setter
public class ComparisonDataSet {

    private String fstDocumentId;
    private String sndDocumentId;

    private List<DiffMatchPatch.Diff> diffs;

    private DocumentComparer.CompareResult compareResult;

}
