// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.numbering;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GenericNumberingScheme implements NumberingScheme {

    private static final String DEFAULT_DELIMITER = ";";

    private static final List<String> LOWCASE_CHARS = IntStream.range(97, 123).mapToObj(i -> String.valueOf((char) i)).collect(Collectors.toList());

    private static final List<String> UPCASE_CHARS = IntStream.range(65, 91).mapToObj(i -> String.valueOf((char) i)).collect(Collectors.toList());

    private static boolean recurring;

    private final List<LevelNumbering> levelNumberings = new ArrayList<>();

    private final List<String> symbols;

    @Getter
    @RequiredArgsConstructor
    public enum NumberingType {
        DECIMAL("1"), LOWER_ROMAN("i"), UPPER_ROMAN("I"), LOWER_ALPHA("a"), UPPER_ALPHA("A");

        private static final Map<String, NumberingType> symbols;

        static {
            symbols = new HashMap<>();
            Arrays.stream(values()).forEach(n -> symbols.put(n.getSymbol(), n));
        }

        private final String symbol;

        public static NumberingType fromSymbol(String symbol) {
            return symbols.get(symbol);
        }

        public static Set<String> typeSymbols() {
            return symbols.keySet();
        }
    }

    @Getter
    static class LevelNumbering {

        private static final int SPEC_LENGTH = 3;

        private final String prefix;
        private final String suffix;
        private final NumberingType type;

        /**
         * Given a specification this implementation results in a numbering template for a level.
         *
         * @param specification a string of at least one and up to three characters representing prefix - type - suffix
         */
        public LevelNumbering(String specification) {
            int specSize;
            if (Utils.isMissing(specification) || (specSize = specification.length()) > SPEC_LENGTH) {
                throw new IllegalArgumentException("invalid numbering scheme specification: " + specification);
            }

            String first = String.valueOf(specification.charAt(0));
            if (NumberingType.typeSymbols().contains(first)) {
                type = NumberingType.fromSymbol(first);
                prefix = "";
                assert specSize < SPEC_LENGTH;
                suffix = specSize == (SPEC_LENGTH - 1) ? String.valueOf(specification.charAt(1)) : "";
            } else {
                prefix = first;
                assert specSize > 1;
                String snd = String.valueOf(specification.charAt(1));
                if (NumberingType.typeSymbols().contains(snd)) {
                    type = NumberingType.fromSymbol(snd);
                    suffix = specSize == SPEC_LENGTH ? String.valueOf(specification.charAt(SPEC_LENGTH - 1)) : "";
                } else {
                    throw new IllegalArgumentException("invalid syntax of numbering scheme specification: " + specification);
                }
            }
        }

        public String getSymbol(int index) {
            String num;

            switch (type) {
                case DECIMAL:
                    num = String.valueOf(index);
                    break;
                case LOWER_ALPHA:
                    num = getAlpha(index - 1, LOWCASE_CHARS);
                    break;
                case LOWER_ROMAN:
                    num = RomanNumeral.arabicToRoman(index).toLowerCase();
                    break;
                case UPPER_ALPHA:
                    num = getAlpha(index - 1, UPCASE_CHARS);
                    break;
                case UPPER_ROMAN:
                    num = RomanNumeral.arabicToRoman(index);
                    break;
                default:
                    num = "";
            }

            return String.format("%s%s%s", prefix, num, suffix);
        }

        private String getAlpha(int index, List<String> alphas) {
            return getAlpha(index, 1, alphas);
        }

        private String getAlpha(int index, int recursion, List<String> alphas) {
            if (index < alphas.size()) {
                return alphas.get(index).repeat(recursion);
            } else {
                return getAlpha(index - alphas.size(), recursion + 1, alphas);
            }
        }

    }

    /**
     * Given a specification this implementation results in a concrete numbering scheme.
     *
     * @param orderedSpecification a list of level specifications separated by default delimiter
     */
    public GenericNumberingScheme(String orderedSpecification, String unorderedSpecification, boolean recurring) {
        this(orderedSpecification, unorderedSpecification, DEFAULT_DELIMITER, recurring);
    }

    /**
     * Given a orderedSpecification this implementation results in a concrete numbering scheme.
     *
     * @param orderedSpecification a list of level specifications separated by <code>delimiter</code> in the order of increasing depth
     * @param delimiter            the string that separates single level specs; default is ';'
     */
    public GenericNumberingScheme(String orderedSpecification, String unorderedSpecification, String delimiter, boolean recurring) {
        GenericNumberingScheme.recurring = recurring;
        String dlm = Utils.isMissing(delimiter) ? DEFAULT_DELIMITER : delimiter;
        Arrays.stream(orderedSpecification.split(Pattern.quote(dlm))).forEach(s -> levelNumberings.add(new LevelNumbering(s)));
        symbols = unorderedSpecification.chars().mapToObj(i -> String.valueOf((char) i)).collect(Collectors.toList());
    }

    @Override
    public String getPrefix(int depth) {
        return levelNumberings.get(getAdjustedIndex(depth, levelNumberings)).getPrefix();
    }

    @Override
    public String getSuffix(int depth) {
        return levelNumberings.get(getAdjustedIndex(depth, levelNumberings)).getSuffix();
    }

    @Override
    public String getType(int depth) {
        return levelNumberings.get(getAdjustedIndex(depth, levelNumberings)).getType().getSymbol();
    }

    @Override
    public String getSymbol(int index, int depth, boolean ordered) {
        if (ordered) {
            return levelNumberings.get(getAdjustedIndex(depth, levelNumberings)).getSymbol(index);
        } else {
            return symbols.get(getAdjustedIndex(depth, symbols));
        }
    }

    private static int getAdjustedIndex(int index, Collection<?> collection) {
        if (index < collection.size()) {
            return index;
        } else {
            return recurring ? index % collection.size() : collection.size() - 1;
        }
    }

}
