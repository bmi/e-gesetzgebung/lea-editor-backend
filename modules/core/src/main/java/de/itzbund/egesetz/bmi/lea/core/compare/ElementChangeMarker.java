// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import de.itzbund.egesetz.bmi.lea.core.ChangeMarker;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Getter
@RequiredArgsConstructor
public enum ElementChangeMarker implements ChangeMarker {

    ADDED("cp_added"),
    DELETED("cp_deleted"),
    EMPTY("cp_empty"),
    MATCHED("cp_matched"),
    MOVED("cp_moved");

    private static final Map<String, ElementChangeMarker> LITERALS;

    static {
        LITERALS = new HashMap<>();
        Arrays.stream(values()).forEach(marker -> LITERALS.put(marker.getType(), marker));
    }

    final String type;

    public static ElementChangeMarker fromLiteral(String literal) {
        return LITERALS.get(literal);
    }

}
