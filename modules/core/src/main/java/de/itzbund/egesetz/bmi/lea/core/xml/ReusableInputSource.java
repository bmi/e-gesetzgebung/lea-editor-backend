// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.xml;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.IOUtils;
import org.xml.sax.InputSource;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * A ReusableInputSource encapsulates XML content as an instance of {@link InputSource} would but can be re-read several times.
 */
@Log4j2
public class ReusableInputSource {

    @Getter
    private String risId;

    private ByteArrayInputStream cache;

    private InputSource inputSource;

    private XmlDocumentContentExtractor extractor;


    /**
     * Constructor to yield an object of type {@link ReusableInputSource}, takes an {@link InputStream} as parameter to convey to XML content. The content of
     * this input stream is cached to be read again more than once.
     *
     * @param byteStream an {@link InputStream} that contains the XML content
     */
    public ReusableInputSource(InputStream byteStream) {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            IOUtils.copy(byteStream, baos);
            cache = new ByteArrayInputStream(baos.toByteArray());
            risId = Utils.bytesToHex(Utils.getHash(Utils.DigestAlgorithm.SHA256, baos.toByteArray()));
        } catch (Exception e) {
            log.error(e);
        }

        inputSource = new InputSource(cache);
    }


    /**
     * Returns the XML content stored in this instance of {@link ReusableInputSource} as an {@link InputStream} object. The cached content is automatically
     * reset so that the same content can be read again.
     *
     * @return an {@link InputStream} that contains the XML content
     */
    public InputStream getByteStream() {
        cache.reset();
        String encoding = inputSource.getEncoding();
        String publicId = inputSource.getPublicId();
        String systemId = inputSource.getSystemId();

        inputSource = new InputSource(cache);
        inputSource.setEncoding(encoding);
        inputSource.setPublicId(publicId);
        inputSource.setSystemId(systemId);

        return inputSource.getByteStream();
    }


    public XmlDocumentContentExtractor getExtractor() {
        if (extractor == null) {
            extractor = new XmlDocumentContentExtractor(this);
        }
        return extractor;
    }


    public String getEncoding() {
        return inputSource.getEncoding();
    }


    public void setEncoding(String encoding) {
        inputSource.setEncoding(encoding);
    }


    public String getPublicId() {
        return inputSource.getPublicId();
    }


    public void setPublicId(String publicId) {
        inputSource.setPublicId(publicId);
    }


    public String getSystemId() {
        return inputSource.getSystemId();
    }


    public void setSystemId(String systemId) {
        inputSource.setSystemId(systemId);
    }

}
