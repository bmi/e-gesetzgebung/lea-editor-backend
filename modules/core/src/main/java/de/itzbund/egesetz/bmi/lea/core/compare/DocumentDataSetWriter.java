// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import java.io.Writer;

/**
 * Abstract class that writes out a {@link DocumentDataSet} into a special textual format. That format is defined in concrete sub-classes.
 */
public abstract class DocumentDataSetWriter {

    private static final String TYPE_STRING_FORMAT = String.format("%%1$-%ss", getTypeStringMaxLength());
    protected Writer writer;


    private static int getTypeStringMaxLength() {
        int maxLength = 0;
        for (DocumentDataItem.ItemType itemType : DocumentDataItem.ItemType.values()) {
            int length = itemType.name().length();
            if (length > maxLength) {
                maxLength = length;
            }
        }
        return maxLength;
    }


    /**
     * Writes out the document content.
     *
     * @param dataSet A dataset of document data items
     */
    public abstract void write(DocumentDataSet dataSet);


    // dispatch method deciding on type of data item
    public void writeLine(DocumentDataItem dataItem) {
        switch (dataItem.getItemType()) {
            case DOCUMENT:
            case ELEMENT:
                writeLine((ElementTypeDataItem) dataItem);
                break;
            case TEXT:
                writeLine((TextTypeDataItem) dataItem);
        }
    }


    /**
     * Write one line of text regarding an element-type data item.
     *
     * @param dataItem a data item that represents an element
     */
    public abstract void writeLine(ElementTypeDataItem dataItem);

    /**
     * Write one line of text regarding a text data item.
     *
     * @param dataItem a data items that represents text content
     */
    public abstract void writeLine(TextTypeDataItem dataItem);


    public String getTypeString(DocumentDataItem.ItemType itemType) {
        return String.format(TYPE_STRING_FORMAT, itemType.name().toLowerCase());
    }

}
