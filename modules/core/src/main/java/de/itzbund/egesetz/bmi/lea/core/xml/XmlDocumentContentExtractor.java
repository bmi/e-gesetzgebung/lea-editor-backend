// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.xml;

import lombok.Getter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.util.Properties;

import static de.itzbund.egesetz.bmi.lea.core.util.Utils.getDocumentBuilderFactory;

/**
 * Class providing cachable instances that are related to instances of {@link ReusableInputSource} and store data extracted from XML content.
 */
@Log4j2
@Getter
public class XmlDocumentContentExtractor {

    private static final Properties XPATH_PROPERTIES = new Properties();
    private static final NamespaceContext NS_CONTEXT = new AppNamespaceContext();

    private static XPathExpression LOCATION_EXPRESSION;
    private static XPathExpression DOCTYPE_EXPRESSION;
    private static XPathExpression META_EXPRESSION;

    static {
        try {
            XPATH_PROPERTIES.load(XmlDocumentContentExtractor.class.getResourceAsStream("/xml/xpath.properties"));

            XPath xpath = XPathFactory.newInstance().newXPath();
            xpath.setNamespaceContext(NS_CONTEXT);
            LOCATION_EXPRESSION = xpath.compile(XPATH_PROPERTIES.getProperty("xpath.xsi.schemalocation"));
            DOCTYPE_EXPRESSION = xpath.compile(XPATH_PROPERTIES.getProperty("xpath.doc.type"));
            META_EXPRESSION = xpath.compile(XPATH_PROPERTIES.getProperty("xpath.meta.proprietary"));
        } catch (IOException | XPathExpressionException e) {
            log.error(e);
        }
    }

    private final String risId;
    private String xsiSchemaLocation;
    private String documentType;

    private LegalDocMLdeMetadaten metadaten;


    public XmlDocumentContentExtractor(@NonNull ReusableInputSource ris) {
        risId = ris.getRisId();

        try {
            // Load XML content into memory

            DocumentBuilder db = getDocumentBuilderFactory().newDocumentBuilder();
            Document doc = db.parse(ris.getByteStream());

            // Get schema location
            xsiSchemaLocation = LOCATION_EXPRESSION.evaluate(doc);

            // Get document type
            documentType = DOCTYPE_EXPRESSION.evaluate(doc);

            // Get proprietary metadata
            Element metaNode = (Element) META_EXPRESSION.evaluate(doc, XPathConstants.NODE);
            if (metaNode != null) {
                collectMetadata(metaNode);
            }

        } catch (RuntimeException | ParserConfigurationException | SAXException | IOException | XPathExpressionException e) {
            log.error(e);
        }
    }


    public String getMetadatum(LegalDocMLdeMetadatenFeld feld) {
        return metadaten.getMetadatum(feld);
    }


    private void collectMetadata(Element metaNode) {
        metadaten = new LegalDocMLdeMetadaten();
        NodeList children = metaNode.getChildNodes();

        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;
                String metaType = elem.getTagName();
                String value = elem.getTextContent();
                metadaten.setMetadatum(LegalDocMLdeMetadatenFeld.fromLiteral(metaType), value);
            }
        }
    }

}
