// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.xml;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;

@UtilityClass
public class XmlUtils {

    public List<Pair<String, String>> getNamespaceMappings(String schemaLocation) {
        List<Pair<String, String>> reaultList = new ArrayList<>();

        if (schemaLocation != null && !schemaLocation.trim().isBlank()) {
            String[] parts = schemaLocation.split("\\s+");
            int numPairs = (parts.length / 2) * 2; // NOSONAR
            int count = 0;

            while (count < numPairs) {
                String nsUri = parts[count];
                count++;
                String loc = parts[count];
                count++;
                reaultList.add(Pair.of(nsUri, loc));
            }
        }

        return reaultList;
    }

}
