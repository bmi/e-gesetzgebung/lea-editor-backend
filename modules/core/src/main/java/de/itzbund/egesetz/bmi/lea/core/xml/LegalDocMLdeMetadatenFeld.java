// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.xml;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@Getter
public enum LegalDocMLdeMetadatenFeld {

    META_TYP("meta:typ"),
    META_FORM("meta:form"),
    META_FASSUNG("meta:fassung"),
    META_ART("meta:art"),
    META_INITIANT("meta:initiant"),
    META_BEARBEITENDE_INSTITUTION("meta:bearbeitendeInstitution");

    private static final Map<String, LegalDocMLdeMetadatenFeld> literals;

    static {
        literals = new HashMap<>();
        Arrays.stream(values()).forEach(f -> literals.put(f.getLiteral(), f));
    }

    private final String literal;


    public static LegalDocMLdeMetadatenFeld fromLiteral(String literal) {
        return literals.get(literal);
    }

}
