// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;

/**
 * A writer for a text format in record style. It is assumed to be more readable.
 */
@Log4j2
public class FixedFormatDocumentDataSetWriter extends DocumentDataSetWriter {

    private static final int MAX_NAME_LENGTH = 50;

    private static final String NAME_FORMAT = String.format("%%1$-%ss", MAX_NAME_LENGTH);
    private static final String NO_PARENT_ID = "         ";


    public FixedFormatDocumentDataSetWriter(Writer writer) {
        this.writer = writer;
    }


    /**
     * {@inheritDoc}
     *
     * @param dataSet A dataset of document data items
     */
    @Override
    public void write(DocumentDataSet dataSet) {
        for (DocumentDataItem dataItem : dataSet.getItems()) {
            writeLine(dataItem);
        }
    }


    /**
     * Writes a single line of element-like content.
     *
     * @param dataItem a data item that represents an element
     */
    public void writeLine(ElementTypeDataItem dataItem) {
        String parentId = dataItem.getParentId();
        if (parentId == null) {
            parentId = NO_PARENT_ID;
        }

        try {
            writer.write(String.format("%s  %s  %s  %s  %s",
                dataItem.getId(),
                getTypeString(dataItem.getItemType()),
                parentId,
                getNameString(dataItem.getName()),
                getAttributesString(dataItem.getAttributes())));
            writer.write('\n');
        } catch (IOException e) {
            log.error(e);
        }
    }


    /**
     * {@inheritDoc}
     *
     * @param dataItem a data items that represents text content
     */
    public void writeLine(TextTypeDataItem dataItem) {
        try {
            writer.write(String.format("%s  %s  %s  %s",
                dataItem.getId(),
                getTypeString(dataItem.getItemType()),
                dataItem.getParentId(),
                WhitespaceSubstitution.getInstance().convert(dataItem.getContent())));
            writer.write('\n');
        } catch (IOException e) {
            log.error(e);
        }
    }


    private String getNameString(String name) {
        return String.format(NAME_FORMAT, name);
    }


    private String getAttributesString(SortedMap<String, String> attributes) {
        StringBuilder sb = new StringBuilder();

        for (Iterator<Map.Entry<String, String>> it = attributes.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, String> entry = it.next();
            sb.append(String.format("@%s=%s", entry.getKey(), entry.getValue()));

            if (it.hasNext()) {
                sb.append(", ");
            }
        }

        return sb.toString();
    }

}
