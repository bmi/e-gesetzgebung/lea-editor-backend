// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * A textual data item. It contains only text, and a reference to its parent element.
 */
@Getter
@Setter
@ToString
public class TextTypeDataItem extends DocumentDataItem {

    private String content;


    /**
     * Constructs a new instance of a text data item.
     *
     * @param count    the id of this item
     * @param parentId reference to the directly containing element
     * @param content  the text data
     */
    @Builder
    public TextTypeDataItem(int count, String parentId, String content) {
        this.itemType = ItemType.TEXT;
        makeId(this.itemType, count);
        this.parentId = parentId;
        setContent(content);
    }


    public void setContent(String content) {
        if (content == null) {
            this.content = "";
        } else if (content.isBlank()) {
            this.content = content.isEmpty() ? "" : " ";
        } else {
            this.content = content;
        }
    }

}
