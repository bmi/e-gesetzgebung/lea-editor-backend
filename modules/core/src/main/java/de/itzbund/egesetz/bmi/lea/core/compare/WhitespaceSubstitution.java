// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import lombok.Getter;

import java.util.Iterator;

/**
 * A singleton that converts whitespace in text content into visible symbols to make them distinguishable.
 */
public class WhitespaceSubstitution {

    @Getter
    private static final WhitespaceSubstitution instance = new WhitespaceSubstitution();


    private WhitespaceSubstitution() {
    }


    /**
     * Convert a string into another string such that all whitespace is replaced by visible substitutes. Makes use of a Map that describes the substitutions (so
     * its configurable).
     *
     * @param text the original text
     * @return the resulting text with all whitespace characters as visible substitutes
     */
    public String convert(String text) {
        if (text == null) {
            return "";
        }

        String result = text;

        for (Iterator<WhitespaceSubstitutionMap.Substitution> it = WhitespaceSubstitutionMap.getIterator();
            it.hasNext(); ) {
            WhitespaceSubstitutionMap.Substitution substitution = it.next();
            result = result.replaceAll(substitution.getRegex(), substitution.getSubstitute());
        }

        return result;
    }

}
