// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.util;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashSet;

/**
 * A named {@link HashSet} for use in {@link FlatGroupedList}. It stores a set of elements.
 *
 * @param <E> any type
 */

@Data
@EqualsAndHashCode(callSuper = false)
public class ListGroup<E> extends HashSet<E> {

    private String name;


    public ListGroup(String name) {
        super();
        setName(name);
    }

}
