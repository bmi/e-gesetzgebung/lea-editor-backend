// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import lombok.Getter;
import lombok.Setter;

import java.io.InputStream;

/**
 * Abstract class of a reader that takes an original document and transforms it into the neutral in-memory format, i.e. an instance of a document dataset.
 */
public abstract class DocumentReader {

    protected Counter counter = new Counter();


    /**
     * Reads the document's content and transforms it into adocument dataset.
     *
     * @param inputStream the content of the original document
     * @return an in-memory representation of the documents as set of data items
     */
    public abstract DocumentDataSet readDocument(InputStream inputStream);


    public int getElemCount() {
        return counter.getElemCount();
    }


    public int getTextCount() {
        return counter.getTextCount();
    }


    public void incElemCount() {
        counter.elemCount++;
    }


    public void incTextCount() {
        counter.textCount++;
    }


    /**
     * Manages counts of element-type and textual data items. Those numbers are used as IDs.
     */
    @Getter
    @Setter
    public static class Counter {

        private int elemCount = 0;
        private int textCount = 0;
    }

}
