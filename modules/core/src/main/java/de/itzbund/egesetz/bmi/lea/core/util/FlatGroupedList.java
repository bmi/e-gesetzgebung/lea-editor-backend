// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.util;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A FlatGroupedList is a generic data structure that holds a list of {@link ListGroup} objects, a kind of list of lists but with only one level of nesting. Its
 * main method {@link FlatGroupedList#getFlatList()} makes a flattened or linearized list out of the {@link ListGroup}s. Main purpose of this class is to
 * organize a collection of tasks that might have dependencies to other tasks into a linear workflow.
 *
 * @param <E> any Type
 */

@Component
public class FlatGroupedList<E> {

    private final List<ListGroup<E>> groupedList = new ArrayList<>();

    private final Map<String, ListGroup<E>> groups = new HashMap<>();


    /**
     * Add another {@link ListGroup} object to this list.
     *
     * @param group the instance to add
     */
    public void addGroup(ListGroup<E> group) {
        groupedList.add(group);
        groups.put(group.getName(), group);
    }


    /**
     * Create a new {@link ListGroup} with the given name and add it to this list.
     *
     * @param name the name of the group
     * @return a new {@link ListGroup} object
     */
    public ListGroup<E> makeGroup(String name) {
        ListGroup<E> group = new ListGroup<>(name);
        groupedList.add(group);
        groups.put(name, group);
        return group;
    }


    /**
     * Create several instances of {@link ListGroup} with the given names and add them to this list.
     *
     * @param names array of names for the groups
     */
    public void makeGroups(String... names) {
        for (String name : names) {
            makeGroup(name);
        }
    }


    /**
     * Add a new element to the named group in this list. If the element is {@code null} an {@link IllegalArgumentException} is thrown. If the stated group is
     * not already available in this {@link FlatGroupedList} the flag {@code makeGroupIfNotExists} decides whether the group should be created on the fly or an
     * {@link IllegalArgumentException} is thrown.
     *
     * @param element              the element to include
     * @param groupName            name of the group to put this element into
     * @param makeGroupIfNotExists if true the named group is creates in this list, if false and there is no group with the given name in this list then an
     *                             {@link IllegalArgumentException} is thrown
     * @throws IllegalArgumentException when
     *                                  <ul>
     *                                      <li>the element is null or</li>
     *                                      <li>the named group does not exist and
     *                                      {@code makeGroupIfNotExists == false}</li>
     *                                  </ul>
     */
    public void addElement(E element, String groupName, boolean makeGroupIfNotExists) {
        if (element == null) {
            throw new IllegalArgumentException("it is not useful to add a null element to a list");
        }

        ListGroup<E> group = groups.get(groupName);

        if (group == null) {
            if (makeGroupIfNotExists) {
                group = makeGroup(groupName);
            } else {
                throw new IllegalArgumentException(
                    String.format("there is no group '%s' in this grouped list", groupName));
            }
        }

        group.add(element);
    }


    /**
     * Like {@link #addElement(Object, String, boolean)} but adds several elements in one go to the same group.
     *
     * @param groupName            name of the group to put this element into
     * @param makeGroupIfNotExists if true the named group is creates in this list, if false and there is no group with the given name in this list then an
     *                             {@link IllegalArgumentException} is thrown
     * @param elements             the elements to include
     */
    @SafeVarargs
    public final void addElements(String groupName, boolean makeGroupIfNotExists, E... elements) {
        for (E element : elements) {
            addElement(element, groupName, makeGroupIfNotExists);
        }
    }


    /**
     * Checks whether the given element is already contained in the stated group in this list.
     *
     * @param element   the element to look for
     * @param groupName the name of the group to search
     * @return true if the element is found in the group with the given name, false otherwise
     */
    public boolean contains(E element, String groupName) {
        ListGroup<E> group = groups.get(groupName);
        if (group == null) {
            return false;
        }
        return group.contains(element);
    }


    /**
     * Checks whether the given element is found in this list regardless in which group.
     *
     * @param element the element to look for
     * @return true if the element is found in any group in this list, false otherwise
     */
    public boolean contains(E element) {
        for (ListGroup<E> group : groupedList) {
            if (group.contains(element)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Checks that all elements from the stated collection are found in this list.
     *
     * @param elements collection of elements to look for
     * @return true if all elements are found in this list, false otherwise
     */
    public boolean containsAll(Collection<E> elements) {
        for (E element : elements) {
            if (!contains(element)) {
                return false;
            }
        }
        return true;
    }


    /**
     * Checks that all elements from the stated collection are found in the named group in this list.
     *
     * @param elements  collection of elements to look for
     * @param groupName name of the group to search
     * @return true if all elements are found in the named group in this list, false otherwise
     */
    public boolean containsAll(Collection<E> elements, String groupName) {
        for (E element : elements) {
            if (!contains(element, groupName)) {
                return false;
            }
        }
        return true;
    }


    /**
     * Makes a flat list of elements from this list of sets. The order of elements depends on the order of the groups in this {@link FlatGroupedList}, but is
     * undetermined within each group.
     *
     * @return a flat list of all elements contained in this list
     */
    public List<E> getFlatList() {
        List<E> flatList = new ArrayList<>();
        for (ListGroup<E> group : groupedList) {
            flatList.addAll(group);
        }
        return flatList;
    }


    /**
     * Checks whether a flat list of elements is sufficiently equal to this {@link FlatGroupedList}. Sufficiently equal in this context means:
     * <ol>
     *     <li>there exists a partition of the given list that is equal to the list of groups in this
     *     {@link FlatGroupedList}</li>
     *     <li>the partitions have the same order as the groups in this {@link FlatGroupedList}</li>
     * </ol>
     *
     * @param list the list of elements to check
     * @return true if subsets of elements from the given list are equal to some group in this list and all elements in the given list are contained in this
     * list, false otherwise.
     */
    public boolean conformsTo(List<E> list) {
        List<Integer> sizes = getSizes();
        List<Integer> indexes = getStartIndexes();

        int numElems = sizes.stream().mapToInt(Integer::intValue).sum();
        if (numElems != list.size()) {
            return false;
        }

        for (int i = 0; i < groupedList.size(); i++) {
            ListGroup<E> group = groupedList.get(i);
            List<E> sublist = list.subList(indexes.get(i), indexes.get(i) + sizes.get(i));
            if (!containsAll(sublist, group.getName())) {
                return false;
            }
        }

        return true;
    }


    // auxiliary method to return the list of group sizes in the same order
    private List<Integer> getSizes() {
        List<Integer> sizes = new ArrayList<>();

        for (ListGroup<E> group : groupedList) {
            sizes.add(group.size());
        }

        return sizes;
    }


    // auxiliary method to return the list of group start indexes in the same order of groups in this list
    // as they would appear in a flattened list
    private List<Integer> getStartIndexes() {
        List<Integer> indexes = new ArrayList<>();
        List<Integer> sizes = getSizes();

        int index = 0;
        for (int size : sizes) {
            indexes.add(index);
            index += size;
        }
        return indexes;
    }

}
