// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.util;

import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This is a (smaller) copy of TestResourceProvider We should merge both classes
 */
@Log4j2
public class CustomResourceProvider {

    private static final Class<CustomResourceProvider> CLS = CustomResourceProvider.class;

    private static final String URL_PROTOCOL_FILE = "file";

    private static final String URL_PROTOCOL_JAR = "jar";

    private static final int JAR_PREFIX_WIN_LENGTH = 10;

    private static final int JAR_PREFIX_NIX_LENGTH = 9;

    private static final int THRESHOLD_ENTRIES = 10000;

    // 1 MB
    private static final int THRESHOLD_ENTRY_SIZE = 1000000;

    // 10 MB
    private static final int THRESHOLD_ARCHIVE_SIZE = 10000000;

    private static final int BUFFER_SIZE = 4096;

    private static final double THRESHOLD_RATIO = 10;

    private static final Pattern PATT_LDML_FILE = Pattern.compile("ldml/(\\w+)/.+\\.xml");

    private static final Pattern PATT_WIN_FILE = Pattern.compile("jar:file:/\\w:");

    private static CustomResourceProvider instance;

    private final Map<String, Collection<ReusableInputSource>> resources;


    private CustomResourceProvider() {
        // collect file resources and cache them
        resources = new HashMap<>();
        collectResources(CustomResourceProvider.class.getResource("/ldml"));
    }


    /**
     * Returns the same instance of a TestResourceProvider.
     *
     * @return the instance of TestResourceProvider
     */
    public static CustomResourceProvider getInstance() {
        if (instance == null) {
            instance = new CustomResourceProvider();
        }
        return instance;
    }


    /**
     * Provides a single XML document as {@link ReusableInputSource} available at the specified path.
     *
     * @param path location of the resource file
     * @return an instance of {@link ReusableInputSource}
     */
    public ReusableInputSource getSingleInputSource(String path) {
        return new ReusableInputSource(CLS.getResourceAsStream(path));
    }


    @SneakyThrows
    private void collectResources(URL url) {
        log.debug("root resource url: " + url);

        if (url == null) {
            log.error("provided URL is null");
            return;
        }

        if (URL_PROTOCOL_FILE.equals(url.getProtocol())) {
            collectFromFile(url);
        } else if (URL_PROTOCOL_JAR.equals(url.getProtocol())) {
            collectFromJar(url);
        }
    }


    @SneakyThrows
    private void collectFromFile(URL url) {
        File root = new File(url.toURI());
        if (root.canRead()) {
            log.debug(
                "[util.TestResourceProvider.collectResourcesFS] "
                    + "collecting resources from:"); //NOSONAR
            log.debug(root.getAbsolutePath());

            collectResourcesFS(root, "__default__", 0);
        } else {
            log.error("cannot read file " + root.getAbsolutePath()); //NOSONAR
        }
    }


    @SneakyThrows
    private void collectFromJar(URL url) {
        // remove 'jar:' from start of string representation
        String urlString = url.toString();
        String path;

        if (PATT_WIN_FILE.matcher(urlString)
            .find()) {
            // windows-style path
            path = urlString.substring(JAR_PREFIX_WIN_LENGTH, urlString.indexOf('!'));
        } else {
            // *nix-style path
            path = urlString.substring(JAR_PREFIX_NIX_LENGTH, urlString.indexOf('!'));
        }

        File root = new File(path);
        if (root.canRead()) {
            JarFile jarFile = new JarFile(root);

            log.debug("changed root url: " + path);
            log.debug(
                "[util.TestResourceProvider.collectResourcesJAR] "
                    + "collecting resources from:"); //NOSONAR
            log.debug(jarFile.getName());

            collectResourcesJAR(jarFile);
        } else {
            log.error("cannot read file " + root.getAbsolutePath()); //NOSONAR
        }
    }


    @SneakyThrows
    private void collectResourcesFS(File dir, String group, int level) {
        if (dir == null) {
            return;
        }

        for (File child : Objects.requireNonNull(dir.listFiles())) {
            if (child.isDirectory()) {
                if (level == 0) {
                    collectResourcesFS(child, child.getName(), level + 1);
                } else {
                    collectResourcesFS(child, group, level + 1);
                }
            } else {
                if (child.getName()
                    .endsWith(".xml")) {
                    ReusableInputSource source = new ReusableInputSource(new FileInputStream(child));
                    source.setSystemId(child.getAbsolutePath());
                    resources.computeIfAbsent(group, k -> new HashSet<>())
                        .add(source);
                }
            }
        }
    }


    private void collectResourcesJAR(JarFile jarFile) {
        int totalSizeArchive = 0;
        int totalEntryArchive = 0;
        boolean moveOn = true;
        Enumeration<JarEntry> entries = jarFile.entries();

        while (entries.hasMoreElements() && moveOn) {
            totalEntryArchive++;

            JarEntry jarEntry = entries.nextElement();

            if (!jarEntry.isDirectory()) {
                moveOn = processEntry(jarFile, jarEntry, totalSizeArchive);
            }

            if (totalEntryArchive > THRESHOLD_ENTRIES) {
                log.error("too many entries in this archive, can lead to inodes exhaustion of the system: {}",
                    jarFile.getName());
                break;
            }
        }
    }


    @SneakyThrows
    private boolean processEntry(JarFile jarFile, JarEntry jarEntry, int totalSizeArchive) {
        boolean isOK = true;
        String entryName = jarEntry.getName();
        Matcher m = PATT_LDML_FILE.matcher(entryName);

        if (m.matches()) {
            String group = m.group(1);
            InputStream in = new BufferedInputStream(jarFile.getInputStream(jarEntry));
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            int nBytes;
            byte[] buffer = new byte[BUFFER_SIZE];
            int totalSizeEntry = 0;

            while ((nBytes = in.read(buffer)) > 0) {
                out.write(buffer, 0, nBytes);
                totalSizeEntry += nBytes;
                totalSizeArchive += nBytes;
                isOK = checkEntryStats(jarFile, jarEntry, totalSizeEntry);
            }

            if (totalSizeArchive > THRESHOLD_ARCHIVE_SIZE) {
                log.error(
                    "the uncompressed data size is too much for the application resource capacity: {}",
                    jarFile.getName());
                isOK = false;
            }

            InputStream contentStream = new ByteArrayInputStream(out.toByteArray());
            ReusableInputSource source = new ReusableInputSource(contentStream);
            source.setSystemId(entryName);
            resources.computeIfAbsent(group, k -> new HashSet<>())
                .add(source);
        }

        return isOK;
    }


    private boolean checkEntryStats(JarFile jarFile, JarEntry jarEntry, int totalSizeEntry) {
        boolean isOK = true;
        double compressionRatio = (double) totalSizeEntry / jarEntry.getCompressedSize();

        if (compressionRatio > THRESHOLD_RATIO) {
            log.error("ratio between compressed and uncompressed data is highly suspicious, "
                + "looks like a Zip Bomb Attack: {}", jarFile.getName());
            isOK = false;
        } else if (totalSizeEntry > THRESHOLD_ENTRY_SIZE) {
            log.error("entry is bigger than allowed: {}", jarFile.getName());
            isOK = false;
        }

        return isOK;
    }

}
