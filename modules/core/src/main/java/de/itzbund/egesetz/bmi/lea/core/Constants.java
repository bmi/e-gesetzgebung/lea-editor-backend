// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Central class comprising often used constants.
 */
@SuppressWarnings({"unused", "java:S103"})
public class Constants {

	private static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd";

	public static final DateTimeFormatter DATE_TIME_FORMATTER =
		DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS")
			.withZone(ZoneId.systemDefault());

	public static final DateTimeFormatter DATE_FORMATTER =
		DateTimeFormatter.ofPattern(DATE_FORMAT_PATTERN);

	public static final DateTimeFormatter INSTANT_TO_DATE_FORMATTER =
		DateTimeFormatter.ofPattern(DATE_FORMAT_PATTERN)
			.withZone(ZoneId.systemDefault());

	public static final String ERRMSG_USER_NO_PERMISSION =
		"Das angeforderte Dokument existiert nicht oder Sie haben keine Berechtigung das Dokument anzuzeigen";

	public static final String ERRMSG_DOC_READONLY = "Document is readonly";

	public static final Pattern PATT_AKN_NS = Pattern
		.compile("xmlns(:\\w+)?=\"http://docs\\.oasis-open\\.org/legaldocml/ns/akn/3\\.0\"");
	public static final Pattern PATT_SCHEMA_LOC = Pattern
		.compile("schemaLocation=\"http://docs\\.oasis-open\\.org/legaldocml/ns/akn/3\\.0(.+.xsd)\"");
	public static final String NS_LEGALDOCML = "http://docs.oasis-open.org/legaldocml/ns/akn/3.0";
	public static final String TARGET_NS = "http://Inhaltsdaten.LegalDocML.de/1.6/";
	public static final String META_BREG_TARGET_NS = "http://Metadaten.LegalDocML.de/1.6/";
	public static final String META_BT_TARGET_NS = "http://MetadatenBundestag.LegalDocML.de/1.6/";
	public static final String NS_SCHEMA_INSTANCE = "http://www.w3.org/2001/XMLSchema-instance";
	public static final String NS_SCHEMA_LOCATION =
		"http://docs.oasis-open.org/legaldocml/ns/akn/3.0 ../Schema/LegalDocML.de.xsd";
	public static final String VRT_OASIS_AKN =
		"https://www.oasis-open.org/legaldocml-akomantoso/akomantoso30.xsd";
	public static final String VRT_LDML_DE =
		"http://www.recht.bund.de/legaldocml.de/LegalDocML.de.xsd";
	public static final String OASIS_SCHEMA_NAME = "akomantoso30.xsd";
	public static final String LDML_DE_SCHEMA_NAME = "LegalDocML.de.xsd";
	public static final String PREFIX_XSI = "xsi";
	public static final String ATTR_SCHEMALOCATION = "schemaLocation";
	public static final String ELEM_A = "a";
	public static final String ELEM_ABBR = "abbr";
	public static final String ELEM_ACT = "act";
	public static final String ELEM_ACTIVEMODIFICATIONS = "activeModifications";
	public static final String ELEM_ACTIVEREF = "activeRef";
	public static final String ELEM_ADDRESS = "address";
	public static final String ELEM_ADJOURNMENT = "adjournment";
	public static final String ELEM_ADMINISTRATIONOFOATH = "administrationOfOath";
	public static final String ELEM_AFFECTEDDOCUMENT = "affectedDocument";
	public static final String ELEM_AKOMANTOSO = "akomaNtoso";
	public static final String ELEM_ALINEA = "alinea";
	public static final String ELEM_ALTERNATIVEREFERENCE = "alternativeReference";
	public static final String ELEM_AMENDMENT = "amendment";
	public static final String ELEM_AMENDMENTBODY = "amendmentBody";
	public static final String ELEM_AMENDMENTCONTENT = "amendmentContent";
	public static final String ELEM_AMENDMENTHEADING = "amendmentHeading";
	public static final String ELEM_AMENDMENTJUSTIFICATION = "amendmentJustification";
	public static final String ELEM_AMENDMENTLIST = "amendmentList";
	public static final String ELEM_AMENDMENTREFERENCE = "amendmentReference";
	public static final String ELEM_ANALYSIS = "analysis";
	public static final String ELEM_ANSWER = "answer";
	public static final String ELEM_APPLICATION = "application";
	public static final String ELEM_APPLIES = "applies";
	public static final String ELEM_ARGUMENT = "argument";
	public static final String ELEM_ARGUMENTS = "arguments";
	public static final String ELEM_ARTICLE = "article";
	public static final String ELEM_ATTACHMENT = "attachment";
	public static final String ELEM_ATTACHMENTOF = "attachmentOf";
	public static final String ELEM_ATTACHMENTS = "attachments";
	public static final String ELEM_AUTHORIALNOTE = "authorialNote";
	public static final String ELEM_B = "b";
	public static final String ELEM_BACKGROUND = "background";
	public static final String ELEM_BILL = "bill";
	public static final String ELEM_BLOCK = "block";
	public static final String ELEM_BLOCKCONTAINER = "blockContainer";
	public static final String ELEM_BLOCKLIST = "blockList";
	public static final String ELEM_BODY = "body";
	public static final String ELEM_BOOK = "book";
	public static final String ELEM_BR = "br";
	public static final String ELEM_CAPTION = "caption";
	public static final String ELEM_CHANGE = "change";
	public static final String ELEM_CHAPTER = "chapter";
	public static final String ELEM_CITATION = "citation";
	public static final String ELEM_CITATIONS = "citations";
	public static final String ELEM_CLASSIFICATION = "classification";
	public static final String ELEM_CLAUSE = "clause";
	public static final String ELEM_COLLECTIONBODY = "collectionBody";
	public static final String ELEM_COMMUNICATION = "communication";
	public static final String ELEM_COMPONENT = "component";
	public static final String ELEM_COMPONENTDATA = "componentData";
	public static final String ELEM_COMPONENTINFO = "componentInfo";
	public static final String ELEM_COMPONENTREF = "componentRef";
	public static final String ELEM_COMPONENTS = "components";
	public static final String ELEM_CONCEPT = "concept";
	public static final String ELEM_CONCLUSIONS = "conclusions";
	public static final String ELEM_CONDITION = "condition";
	public static final String ELEM_CONTAINER = "container";
	public static final String ELEM_CONTENT = "content";
	public static final String ELEM_CONTRASTS = "contrasts";
	public static final String ELEM_COUNT = "count";
	public static final String ELEM_COURTTYPE = "courtType";
	public static final String ELEM_COVERPAGE = "coverPage";
	public static final String ELEM_CROSSHEADING = "crossHeading";
	public static final String ELEM_DATE = "date";
	public static final String ELEM_DEBATE = "debate";
	public static final String ELEM_DEBATEBODY = "debateBody";
	public static final String ELEM_DEBATEREPORT = "debateReport";
	public static final String ELEM_DEBATESECTION = "debateSection";
	public static final String ELEM_DECISION = "decision";
	public static final String ELEM_DECLARATIONOFVOTE = "declarationOfVote";
	public static final String ELEM_DECORATION = "decoration";
	public static final String ELEM_DEF = "def";
	public static final String ELEM_DEL = "del";
	public static final String ELEM_DEROGATES = "derogates";
	public static final String ELEM_DESTINATION = "destination";
	public static final String ELEM_DISSENTSFROM = "dissentsFrom";
	public static final String ELEM_DISTINGUISHES = "distinguishes";
	public static final String ELEM_DIV = "div";
	public static final String ELEM_DIVISION = "division";
	public static final String ELEM_DOC = "doc";
	public static final String ELEM_DOCAUTHORITY = "docAuthority";
	public static final String ELEM_DOCCOMMITTEE = "docCommittee";
	public static final String ELEM_DOCDATE = "docDate";
	public static final String ELEM_DOCINTRODUCER = "docIntroducer";
	public static final String ELEM_DOCJURISDICTION = "docJurisdiction";
	public static final String ELEM_DOCKETNUMBER = "docketNumber";
	public static final String ELEM_DOCNUMBER = "docNumber";
	public static final String ELEM_DOCPROPONENT = "docProponent";
	public static final String ELEM_DOCPURPOSE = "docPurpose";
	public static final String ELEM_DOCSTAGE = "docStage";
	public static final String ELEM_DOCSTATUS = "docStatus";
	public static final String ELEM_DOCTITLE = "docTitle";
	public static final String ELEM_DOCTYPE = "docType";
	public static final String ELEM_DOCUMENTCOLLECTION = "documentCollection";
	public static final String ELEM_DOCUMENTREF = "documentRef";
	public static final String ELEM_DOMAIN = "domain";
	public static final String ELEM_DURATION = "duration";
	public static final String ELEM_EFFICACY = "efficacy";
	public static final String ELEM_EFFICACYMOD = "efficacyMod";
	public static final String ELEM_EMBEDDEDSTRUCTURE = "embeddedStructure";
	public static final String ELEM_EMBEDDEDTEXT = "embeddedText";
	public static final String ELEM_ENTITY = "entity";
	public static final String ELEM_EOL = "eol";
	public static final String ELEM_EOP = "eop";
	public static final String ELEM_EVENT = "event";
	public static final String ELEM_EVENTREF = "eventRef";
	public static final String ELEM_EXTENDS = "extends";
	public static final String ELEM_FILLIN = "fillIn";
	public static final String ELEM_FORCE = "force";
	public static final String ELEM_FORCEMOD = "forceMod";
	public static final String ELEM_FOREIGN = "foreign";
	public static final String ELEM_FORMULA = "formula";
	public static final String ELEM_FRAGMENT = "fragment";
	public static final String ELEM_FRBRALIAS = "FRBRalias";
	public static final String ELEM_FRBRAUTHOR = "FRBRauthor";
	public static final String ELEM_FRBRAUTHORITATIVE = "FRBRauthoritative";
	public static final String ELEM_FRBRCOUNTRY = "FRBRcountry";
	public static final String ELEM_FRBRDATE = "FRBRdate";
	public static final String ELEM_FRBREXPRESSION = "FRBRExpression";
	public static final String ELEM_FRBRFORMAT = "FRBRformat";
	public static final String ELEM_FRBRITEM = "FRBRItem";
	public static final String ELEM_FRBRLANGUAGE = "FRBRlanguage";
	public static final String ELEM_FRBRMANIFESTATION = "FRBRManifestation";
	public static final String ELEM_FRBRMASTEREXPRESSION = "FRBRmasterExpression";
	public static final String ELEM_FRBRNAME = "FRBRname";
	public static final String ELEM_FRBRNUMBER = "FRBRnumber";
	public static final String ELEM_FRBRPORTION = "FRBRportion";
	public static final String ELEM_FRBRPRESCRIPTIVE = "FRBRprescriptive";
	public static final String ELEM_FRBRSUBTYPE = "FRBRsubtype";
	public static final String ELEM_FRBRTHIS = "FRBRthis";
	public static final String ELEM_FRBRTRANSLATION = "FRBRtranslation";
	public static final String ELEM_FRBRURI = "FRBRuri";
	public static final String ELEM_FRBRVERSIONNUMBER = "FRBRversionNumber";
	public static final String ELEM_FRBRWORK = "FRBRWork";
	public static final String ELEM_FROM = "from";
	public static final String ELEM_HASATTACHMENT = "hasAttachment";
	public static final String ELEM_HCONTAINER = "hcontainer";
	public static final String ELEM_HEADER = "header";
	public static final String ELEM_HEADING = "heading";
	public static final String ELEM_I = "i";
	public static final String ELEM_IDENTIFICATION = "identification";
	public static final String ELEM_IMG = "img";
	public static final String ELEM_IMPLICITREFERENCE = "implicitReference";
	public static final String ELEM_INDENT = "indent";
	public static final String ELEM_INLINE = "inline";
	public static final String ELEM_INS = "ins";
	public static final String ELEM_INTERSTITIAL = "interstitial";
	public static final String ELEM_INTRO = "intro";
	public static final String ELEM_INTRODUCTION = "introduction";
	public static final String ELEM_ISANALOGTO = "isAnalogTo";
	public static final String ELEM_ITEM = "item";
	public static final String ELEM_JUDGE = "judge";
	public static final String ELEM_JUDGMENT = "judgment";
	public static final String ELEM_JUDGMENTBODY = "judgmentBody";
	public static final String ELEM_JUDICIAL = "judicial";
	public static final String ELEM_JURISPRUDENCE = "jurisprudence";
	public static final String ELEM_KEYWORD = "keyword";
	public static final String LEA_PREFIX = "lea:";
	public static final String ELEM_LAWYER = "lawyer";
	public static final String ELEM_LEGALSYSTEMMOD = "legalSystemMod";
	public static final String ELEM_LEGISLATURE = "legislature";
	public static final String ELEM_LEVEL = "level";
	public static final String ELEM_LI = "li";
	public static final String ELEM_LIFECYCLE = "lifecycle";
	public static final String ELEM_LIST = "list";
	public static final String ELEM_LISTINTRODUCTION = "listIntroduction";
	public static final String ELEM_LISTWRAPUP = "listWrapUp";
	public static final String ELEM_LOCATION = "location";
	public static final String ELEM_LONGTITLE = "longTitle";
	public static final String ELEM_MAINBODY = "mainBody";
	public static final String ELEM_MAPPING = "mapping";
	public static final String ELEM_MAPPINGS = "mappings";
	public static final String ELEM_MARKER = "marker";
	public static final String ELEM_MEANINGMOD = "meaningMod";
	public static final String ELEM_META = "meta";
	public static final String ELEM_MINISTERIALSTATEMENTS = "ministerialStatements";
	public static final String ELEM_MMOD = "mmod";
	public static final String ELEM_MOD = "mod";
	public static final String ELEM_MOTIVATION = "motivation";
	public static final String ELEM_MREF = "mref";
	public static final String ELEM_NARRATIVE = "narrative";
	public static final String ELEM_NATIONALINTEREST = "nationalInterest";
	public static final String ELEM_NEUTRALCITATION = "neutralCitation";
	public static final String ELEM_NEW = "new";
	public static final String ELEM_NOTE = "note";
	public static final String ELEM_NOTEREF = "noteRef";
	public static final String ELEM_NOTES = "notes";
	public static final String ELEM_NOTICESOFMOTION = "noticesOfMotion";
	public static final String ELEM_NUM = "num";
	public static final String ELEM_OBJECT = "object";
	public static final String ELEM_OFFICIALGAZETTE = "officialGazette";
	public static final String ELEM_OL = "ol";
	public static final String ELEM_OLD = "old";
	public static final String ELEM_OMISSIS = "omissis";
	public static final String ELEM_OPINION = "opinion";
	public static final String ELEM_ORALSTATEMENTS = "oralStatements";
	public static final String ELEM_ORGANIZATION = "organization";
	public static final String ELEM_ORIGINAL = "original";
	public static final String ELEM_OTHER = "other";
	public static final String ELEM_OTHERANALYSIS = "otherAnalysis";
	public static final String ELEM_OTHERREFERENCES = "otherReferences";
	public static final String ELEM_OUTCOME = "outcome";
	public static final String ELEM_OVERRULES = "overrules";
	public static final String ELEM_P = "p";
	public static final String ELEM_PAPERS = "papers";
	public static final String ELEM_PARAGRAPH = "paragraph";
	public static final String ELEM_PARLIAMENTARY = "parliamentary";
	public static final String ELEM_PART = "part";
	public static final String ELEM_PARTY = "party";
	public static final String ELEM_PASSIVEMODIFICATIONS = "passiveModifications";
	public static final String ELEM_PASSIVEREF = "passiveRef";
	public static final String ELEM_PERSON = "person";
	public static final String ELEM_PERSONALSTATEMENTS = "personalStatements";
	public static final String ELEM_PETITIONS = "petitions";
	public static final String ELEM_PLACEHOLDER = "placeholder";
	public static final String ELEM_POINT = "point";
	public static final String ELEM_POINTOFORDER = "pointOfOrder";
	public static final String ELEM_PORTION = "portion";
	public static final String ELEM_PORTIONBODY = "portionBody";
	public static final String ELEM_PRAYERS = "prayers";
	public static final String ELEM_PREAMBLE = "preamble";
	public static final String ELEM_PREFACE = "preface";
	public static final String ELEM_PRESENTATION = "presentation";
	public static final String ELEM_PRESERVATION = "preservation";
	public static final String ELEM_PREVIOUS = "previous";
	public static final String ELEM_PROCEDURALMOTIONS = "proceduralMotions";
	public static final String ELEM_PROCESS = "process";
	public static final String ELEM_PROPRIETARY = "proprietary";
	public static final String ELEM_PROVISO = "proviso";
	public static final String ELEM_PUBLICATION = "publication";
	public static final String ELEM_PUTSINQUESTION = "putsInQuestion";
	public static final String ELEM_QUANTITY = "quantity";
	public static final String ELEM_QUESTION = "question";
	public static final String ELEM_QUESTIONS = "questions";
	public static final String ELEM_QUORUM = "quorum";
	public static final String ELEM_QUORUMVERIFICATION = "quorumVerification";
	public static final String ELEM_QUOTEDSTRUCTURE = "quotedStructure";
	public static final String ELEM_QUOTEDTEXT = "quotedText";
	public static final String ELEM_RECITAL = "recital";
	public static final String ELEM_RECITALS = "recitals";
	public static final String ELEM_RECORDEDTIME = "recordedTime";
	public static final String ELEM_RECOUNT = "recount";
	public static final String ELEM_REF = "ref";
	public static final String ELEM_REFERENCES = "references";
	public static final String ELEM_RELATEDDOCUMENT = "relatedDocument";
	public static final String ELEM_REMARK = "remark";
	public static final String ELEM_REMEDIES = "remedies";
	public static final String ELEM_RESOLUTIONS = "resolutions";
	public static final String ELEM_RESTRICTION = "restriction";
	public static final String ELEM_RESTRICTIONS = "restrictions";
	public static final String ELEM_RESTRICTS = "restricts";
	public static final String ELEM_RESULT = "result";
	public static final String ELEM_RMOD = "rmod";
	public static final String ELEM_ROLE = "role";
	public static final String ELEM_ROLLCALL = "rollCall";
	public static final String ELEM_RREF = "rref";
	public static final String ELEM_RULE = "rule";
	public static final String ELEM_SCENE = "scene";
	public static final String ELEM_SCOPEMOD = "scopeMod";
	public static final String ELEM_SECTION = "section";
	public static final String ELEM_SESSION = "session";
	public static final String ELEM_SHORTTITLE = "shortTitle";
	public static final String ELEM_SIGNATURE = "signature";
	public static final String ELEM_SOURCE = "source";
	public static final String ELEM_SPAN = "span";
	public static final String ELEM_SPEECH = "speech";
	public static final String ELEM_SPEECHGROUP = "speechGroup";
	public static final String ELEM_STATEMENT = "statement";
	public static final String ELEM_STEP = "step";
	public static final String ELEM_SUB = "sub";
	public static final String ELEM_SUBCHAPTER = "subchapter";
	public static final String ELEM_SUBCLAUSE = "subclause";
	public static final String ELEM_SUBDIVISION = "subdivision";
	public static final String ELEM_SUBFLOW = "subFlow";
	public static final String ELEM_SUBHEADING = "subheading";
	public static final String ELEM_SUBLIST = "sublist";
	public static final String ELEM_SUBPARAGRAPH = "subparagraph";
	public static final String ELEM_SUBPART = "subpart";
	public static final String ELEM_SUBRULE = "subrule";
	public static final String ELEM_SUBSECTION = "subsection";
	public static final String ELEM_SUBTITLE = "subtitle";
	public static final String ELEM_SUMMARY = "summary";
	public static final String ELEM_SUP = "sup";
	public static final String ELEM_SUPPORTS = "supports";
	public static final String ELEM_TABLE = "table";
	public static final String ELEM_TBLOCK = "tblock";
	public static final String ELEM_TD = "td";
	public static final String ELEM_TEMPORALDATA = "temporalData";
	public static final String ELEM_TEMPORALGROUP = "temporalGroup";
	public static final String ELEM_TERM = "term";
	public static final String ELEM_TEXTUALMOD = "textualMod";
	public static final String ELEM_TH = "th";
	public static final String ELEM_TIME = "time";
	public static final String ELEM_TIMEINTERVAL = "timeInterval";
	public static final String ELEM_TITLE = "title";
	public static final String ELEM_TLCCONCEPT = "TLCConcept";
	public static final String ELEM_TLCEVENT = "TLCEvent";
	public static final String ELEM_TLCLOCATION = "TLCLocation";
	public static final String ELEM_TLCOBJECT = "TLCObject";
	public static final String ELEM_TLCORGANIZATION = "TLCOrganization";
	public static final String ELEM_TLCPERSON = "TLCPerson";
	public static final String ELEM_TLCPROCESS = "TLCProcess";
	public static final String ELEM_TLCREFERENCE = "TLCReference";
	public static final String ELEM_TLCROLE = "TLCRole";
	public static final String ELEM_TLCTERM = "TLCTerm";
	public static final String ELEM_TOC = "toc";
	public static final String ELEM_TOCITEM = "tocItem";
	public static final String ELEM_TOME = "tome";
	public static final String ELEM_TR = "tr";
	public static final String ELEM_TRANSITIONAL = "transitional";
	public static final String ELEM_U = "u";
	public static final String ELEM_UL = "ul";
	public static final String ELEM_VOTE = "vote";
	public static final String ELEM_VOTING = "voting";
	public static final String ELEM_WORKFLOW = "workflow";
	public static final String ELEM_WRAPUP = "wrapUp";
	public static final String ELEM_WRITTENSTATEMENTS = "writtenStatements";
	public static final String ATTR_GUID = "GUID";
	public static final String ATTR_ALT = "alt";
	public static final String ATTR_ALTERNATIVETO = "alternativeTo";
	public static final String ATTR_AS = "as";
	public static final String ATTR_AUTHORITATIVE = "authoritative";
	public static final String ATTR_BORDER = "border";
	public static final String ATTR_BREAKAT = "breakAt";
	public static final String ATTR_BREAKWITH = "breakWith";
	public static final String ATTR_BY = "by";
	public static final String ATTR_CELLPADDING = "cellpadding";
	public static final String ATTR_CELLSPACING = "cellspacing";
	public static final String ATTR_CHOICE = "choice";
	public static final String ATTR_CLASS = "class";
	public static final String ATTR_COLSPAN = "colspan";
	public static final String ATTR_COMMENTID = "lea:commentId";

	public static final String ATTR_COMMENTSONELEMENT = "lea:commentsOnElement";
	public static final String ATTR_CONTAINS = "contains";
	public static final String ATTR_CURRENT = "current";
	public static final String ATTR_DATE = "date";
	public static final String ATTR_DICTIONARY = "dictionary";
	public static final String ATTR_DURATION = "duration";
	public static final String ATTR_EID = "eId";
	public static final String ATTR_EMPOWEREDBY = "empoweredBy";
	public static final String ATTR_END = "end";
	public static final String ATTR_ENDQUOTE = "endQuote";
	public static final String ATTR_ENDTIME = "endTime";
	public static final String ATTR_EXCLUSION = "exclusion";
	public static final String ATTR_FOR = "for";
	public static final String ATTR_FROM = "from";
	public static final String ATTR_FROMLANGUAGE = "fromLanguage";
	public static final String ATTR_FROZEN = "frozen";
	public static final String ATTR_HEIGHT = "height";
	public static final String ATTR_HREF = "href";
	public static final String ATTR_INCLUDEDIN = "includedIn";
	public static final String ATTR_INCOMPLETE = "incomplete";
	public static final String ATTR_INLINEQUOTE = "inlineQuote";
	public static final String ATTR_LANGUAGE = "language";
	public static final String ATTR_LEVEL = "level";
	public static final String ATTR_MARKER = "marker";
	public static final String ATTR_NAME = "name";
	public static final String ATTR_NORMALIZED = "normalized";
	public static final String ATTR_NUMBER = "number";
	public static final String ATTR_ORIGINAL = "original";
	public static final String ATTR_ORIGINALTEXT = "originalText";
	public static final String ATTR_ORIGINATINGEXPRESSION = "originatingExpression";
	public static final String ATTR_OUTCOME = "outcome";
	public static final String ATTR_PERIOD = "period";
	public static final String ATTR_PIVOT = "pivot";
	public static final String ATTR_PLACEMENT = "placement";
	public static final String ATTR_PLACEMENTBASE = "placementBase";
	public static final String ATTR_POS = "pos";
	public static final String ATTR_REFERSTO = "refersTo";
	public static final String ATTR_ROWSPAN = "rowspan";
	public static final String ATTR_SHORTFORM = "shortForm";
	public static final String ATTR_SHOWAS = "showAs";
	public static final String ATTR_SOURCE = "source";
	public static final String ATTR_SRC = "src";
	public static final String ATTR_START = "start";
	public static final String ATTR_STARTQUOTE = "startQuote";
	public static final String ATTR_STARTTIME = "startTime";
	public static final String ATTR_STATUS = "status";
	public static final String ATTR_STYLE = "style";
	public static final String ATTR_TARGET = "target";
	public static final String ATTR_TEXT = "text";

	public static final String ATTR_TIME = "time";
	public static final String ATTR_TITLE = "title";
	public static final String ATTR_TO = "to";
	public static final String ATTR_TYPE = "type";
	public static final String ATTR_UPTO = "upTo";
	public static final String ATTR_VALUE = "value";
	public static final String ATTR_WID = "wId";
	public static final String ATTR_WIDTH = "width";
	public static final String SHORTCUT_ALINEA = "al";
	public static final String SHORTCUT_ARTICLE = "art";
	public static final String SHORTCUT_ATTACHMENT = "att";
	public static final String SHORTCUT_AMENDMENTBODY = "body";
	public static final String SHORTCUT_BLOCKLIST = "list";
	public static final String SHORTCUT_CHAPTER = "chp";
	public static final String SHORTCUT_CITATION = "cit";
	public static final String SHORTCUT_CITATIONS = "cits";
	public static final String SHORTCUT_CLAUSE = "cl";
	public static final String SHORTCUT_COMPONENT = "cmp";
	public static final String SHORTCUT_COMPONENTREF = "cref";
	public static final String SHORTCUT_COMPONENTS = "cmpnts";
	public static final String SHORTCUT_DEBATEBODY = "body";
	public static final String SHORTCUT_DEBATESECTION = "dbsect";
	public static final String SHORTCUT_DIVISION = "dvs";
	public static final String SHORTCUT_DOCUMENTREF = "dref";
	public static final String SHORTCUT_EVENTREF = "eref";
	public static final String SHORTCUT_FRAGMENT = "frag";
	public static final String SHORTCUT_JUDGMENTBODY = "body";
	public static final String SHORTCUT_LISTINTRODUCTION = "intro";
	public static final String SHORTCUT_LISTWRAPUP = "wrap";
	public static final String SHORTCUT_PARAGRAPH = "para";
	public static final String SHORTCUT_QUOTEDTEXT = "qtext";
	public static final String SHORTCUT_QUOTEDSTRUCTURE = "qstr";
	public static final String SHORTCUT_RECITAL = "rec";
	public static final String SHORTCUT_RECITALS = "recs";
	public static final String SHORTCUT_SECTION = "sec";
	public static final String SHORTCUT_SUBCHAPTER = "subchp";
	public static final String SHORTCUT_SUBCLAUSE = "subcl";
	public static final String SHORTCUT_SUBSECTION = "subsec";
	public static final String SHORTCUT_SUBDIVISION = "subdvs";
	public static final String SHORTCUT_SUBPARAGRAPH = "subpara";
	public static final String JSON_KEY_CMP_TYPE = "leaType";
	public static final String JSON_KEY_CMP_NAME = "componentName";
	public static final String JSON_KEY_NS_URI = "nsUri";
	public static final String JSON_KEY_NS_LOCAL_NAME = "localName";
	public static final String JSON_KEY_NS_PREFIX = "prefix";
	public static final String JSON_KEY_ID = "ident";
	public static final String JSON_KEY_HREF = "href";
	public static final String JSON_KEY_SHOWAS = "showAs";
	public static final String JSON_KEY_ID_EID = "eId";
	public static final String JSON_KEY_ID_WID = "wId";
	public static final String JSON_KEY_ID_GUID = "GUID";
	public static final String JSON_KEY_ID_REF_GUID = "refGUID";
	public static final String JSON_KEY_CTX = "context";
	public static final String JSON_KEY_CTX_XPATH = "xpath";
	public static final String JSON_KEY_ATTRS = "attributes";
	public static final String JSON_KEY_ATTR_NAME = "attrName";
	public static final String JSON_KEY_ATTR_VALUE = "attrValue";
	public static final String JSON_KEY_NS_LIST = "nss";
	public static final String JSON_KEY_CHILDREN = "children";
	public static final String JSON_KEY_MAIN_DOC = "mainDoc";
	public static final String JSON_KEY_CMPTS = "components";
	public static final String JSON_KEY_DECL_NSS = "declaredNss";
	public static final String JSON_KEY_SCHEMA_LOCS = "schemaLocations";
	public static final String JSON_KEY_LOC_URI = "locUri";
	public static final String JSON_KEY_TEXT = "text";
	public static final String JSON_KEY_LIST_INTRO = "intro";
	public static final String JSON_KEY_LIST_OUTRO = "outro";
	public static final String JSON_KEY_LIST_ITEMS = "items";
	public static final String JSON_KEY_TBL_CAPTION = "caption";
	public static final String JSON_KEY_TBL_ROWS = "children";
	public static final String JSON_KEY_TBL_CELLS = "children";
	public static final String JSON_KEY_TYPE = "type";

	public static final String JSON_KEY_VALUE = "value";
	public static final String JSON_KEY_TAG_NAME = "tagName";
	public static final String JSON_KEY_AKN = "akn";
	public static final String JSON_KEY_CNT_TYPE = "contentType";
	public static final String JSON_KEY_XMLNS = "xmlns";
	public static final String JSON_KEY_PREFIX = "prefix";
	public static final String JSON_KEY_URI = "uri";
	public static final String JSON_KEY_EID = "eId";
	public static final String JSON_KEY_VAL_ERR_COUNT = "errorCount";
	public static final String JSON_KEY_VAL_WARN_COUNT = "warningCount";
	public static final String JSON_KEY_VAL_MSG_LIST = "messages";
	public static final String JSON_KEY_VAL_MSG = "message";
	public static final String JSON_KEY_VAL_SEVERITY = "severity";
	public static final String JSON_VAL_TEXT_WRAPPER = "lea:textWrapper";
	public static final String JSON_VAL_CHANGE_WRAPPER = "lea:changeWrapper";
	public static final String JSON_VAL_COMMENT_WRAPPER = "lea:commentMarkerWrapper";
	public static final String JSON_VAL_COMMENT_MARKER = "lea:commentMarker";
	public static final String JSON_VAL_COMMENTS_ON_ELEMENT = "lea:commentsOnElement";
	public static final String JSON_VAL_COMMENT_COLOR = "lea:commentColor";
	public static final String JSON_KEY_COMMENT_ID = "lea:commentId";
	public static final String JSON_KEY_COMMENT_POSITION = "lea:commentPosition";
	public static final String JSON_TYPE_COLLECTIONBODY = "akn:collectionBody";
	public static final String JSON_TYPE_COMPONENT = "akn:component";
	public static final String JSON_TYPE_CONTENT = "akn:content";
	public static final String JSON_TYPE_DOCUMENTREF = "akn:documentRef";
	public static final String JSON_DOCTYPE_ANLAGE = "anlage";
	public static final String JSON_DOCTYPE_ANSCHREIBEN = "anschreiben";
	public static final String JSON_DOCTYPE_BEGRUENDUNG = "begruendung";
	public static final String JSON_DOCTYPE_REGT_ENTW = "regelungstextEntwurfsfassung";
	public static final String JSON_DOCTYPE_VORBLATT = "vorblatt";
	public static final String JSON_TYPE_INLINE = "inline";
	public static final String JSON_TAGNAME_LDML_DE_METADATEN = "legalDocML.de_metadaten";
	public static final String JSON_TYPE_DOC = "document";
	public static final String JSON_KEY_METADATA = "metadata";
	public static final String JSON_VAL_VALIDATION_RESULT = "ValidationResult";
	public static final String JSON_KEY_XML_MODEL = "xml-model";
	public static final String JSON_KEY_XML_STYLESHEET = "xml-stylesheet";
	public static final String LDML_DE_TYPE_AENDERUNG_TEXTINHALT_ALT_URI = "aenderungTextinhaltAltURI";
	public static final String LDML_DE_TYPE_AENDERUNG_TEXTINHALT_NEU_URI = "aenderungTextinhaltNeuURI";
	public static final String LDML_DE_TYPE_AENDERUNG_URSPRUNGS_URI = "aenderungUrsprungsURI";
	public static final String LDML_DE_TYPE_AENDERUNG_URSPRUNGSDOKUMENT_URI = "aenderungUrsprungsdokumentURI";
	public static final String LDML_DE_TYPE_AENDERUNG_ZIELDOKUMENT_URI = "aenderungZieldokumentURI";
	public static final String LDML_DE_TYPE_AENDERUNG_ZIELELEMENT_URI = "aenderungZielelementURI";
	public static final String LDML_DE_TYPE_AENDERUNG_ZITAT_STRUKTUR = "aenderungZitatStruktur";
	public static final String LDML_DE_TYPE_AENDERUNG_ZITAT_TEXT = "aenderungZitatText";
	public static final String LDML_DE_TYPE_AENDERUNGEN_AKTIV = "aenderungenAktiv";
	public static final String LDML_DE_TYPE_AENDERUNGEN_PASSIV = "aenderungenPassiv";
	public static final String LDML_DE_TYPE_AENDERUNGSBEFEHL = "aenderungsbefehl";
	public static final String LDML_DE_TYPE_ANLAGE = "anlage";
	public static final String LDML_DE_TYPE_ANLAGE_DOKUMENTENKOPF = "anlageDokumentenkopf";
	public static final String LDML_DE_TYPE_ANLAGE_HAUPTTEIL = "anlageHauptteil";
	public static final String LDML_DE_TYPE_ANLAGENCONTAINER = "anlagencontainer";
	public static final String LDML_DE_TYPE_ANLAGENELEMENT = "anlagenelement";
	public static final String LDML_DE_TYPE_ANLAGENINHALT = "anlageninhalt";
	public static final String LDML_DE_TYPE_ANLAGENVERWEIS = "anlagenverweis";
	public static final String LDML_DE_TYPE_ANLAGENVERWEIS_EINZELVORSCHRIFT = "anlagenverweisEinzelvorschrift";
	public static final String LDML_DE_TYPE_ANSCHREIBEN = "anschreiben";
	public static final String LDML_DE_TYPE_ANSCHREIBEN_DOKUMENTENKOPF = "anschreibenDokumentenkopf";
	public static final String LDML_DE_TYPE_ANSCHREIBEN_HAUPTTEIL = "anschreibenHauptteil";
	public static final String LDML_DE_TYPE_ANY_TYPE = "anyType";
	public static final String LDML_DE_TYPE_ART_UND_ZAEHLBEZEICHNUNG = "artUndZaehlbezeichnung";
	public static final String LDML_DE_TYPE_AUFZAEHLUNGSELEMENT = "aufzaehlungselement";
	public static final String LDML_DE_TYPE_AUSSCHUSSUEBERWEISUNG = "ausschussueberweisung";
	public static final String LDML_DE_TYPE_AUTOR_FRBR = "autorFRBR";
	public static final String LDML_DE_TYPE_BEARBEITENDE_INSTITUTION = "bearbeitendeInstitution";
	public static final String LDML_DE_TYPE_BEGRUENDUNG = "begruendung";
	public static final String LDML_DE_TYPE_BEGRUENDUNG_DOKUMENTENKOPF = "begruendungDokumentenkopf";
	public static final String LDML_DE_TYPE_BEGRUENDUNG_HAUPTTEIL = "begruendungHauptteil";
	public static final String LDML_DE_TYPE_BEGRUENDUNG_VERT_RASCHLUSSBEMERKUNG =
		"begruendungVertRASchlussbemerkung";
	public static final String LDML_DE_TYPE_BEGRUENDUNGSTEIL = "begruendungsteil";
	public static final String LDML_DE_TYPE_BEGRUENDUNGSTEIL_ABSCHNITT = "begruendungsteilAbschnitt";
	public static final String LDML_DE_TYPE_BIBLIOGRAPHISCHE_METADATEN_FRBR = "bibliographischeMetadatenFRBR";
	public static final String LDML_DE_TYPE_BILD = "bild";
	public static final String LDML_DE_TYPE_BILDBESCHREIBUNG = "bildbeschreibung";
	public static final String LDML_DE_TYPE_BILDCONTAINER = "bildcontainer";
	public static final String LDML_DE_TYPE_DATUM = "datum";
	public static final String LDML_DE_TYPE_DATUM_CONTAINER = "datumContainer";
	public static final String LDML_DE_TYPE_DATUM_DOKUMENTENVERSION = "datumDokumentenversion";
	public static final String LDML_DE_TYPE_DATUM_FRBR = "datumFRBR";
	public static final String LDML_DE_TYPE_DENKSCHRIFT_ABSCHNITT = "denkschriftAbschnitt";
	public static final String LDML_DE_TYPE_DENKSCHRIFT_HAUPTTEIL = "denkschriftHauptteil";
	public static final String LDML_DE_TYPE_DENKSCHRIFT_TEIL = "denkschriftTeil";
	public static final String LDML_DE_TYPE_DOKUMENT_URI_FRBR = "dokumentUriFRBR";
	public static final String LDML_DE_TYPE_DOKUMENTAUSWERTUNG = "dokumentauswertung";
	public static final String LDML_DE_TYPE_DOKUMENTEN_STATUS = "dokumentenStatus";
	public static final String LDML_DE_TYPE_DOKUMENTEN_TYP = "dokumentenTyp";
	public static final String LDML_DE_TYPE_DOKUMENTENKOPF_ANSCHREIBEN_ADRESSE
		=
		"dokumentenkopfAnschreibenAdresse";
	public static final String LDML_DE_TYPE_DOKUMENTENKOPF_TITEL = "dokumentenkopfTitel";
	public static final String LDML_DE_TYPE_DOKUMENTENKOPF_TITEL_WEITERER_BESTANDTEIL
		=
		"dokumentenkopfTitelWeitererBestandteil";
	public static final String LDML_DE_TYPE_DOKUMENTENKOPF_WEITERE_INFORMATIONEN
		=
		"dokumentenkopfWeitereInformationen";
	public static final String LDML_DE_TYPE_DOKUMENTENTITEL = "dokumententitel";
	public static final String LDML_DE_TYPE_DRUCKSACHENNUMMER = "drucksachennummer";
	public static final String LDML_DE_TYPE_EINGANGSFORMEL = "eingangsformel";
	public static final String LDML_DE_TYPE_EINGANGSFORMEL_EINGANGSSATZ = "eingangsformelEingangssatz";
	public static final String LDML_DE_TYPE_EINGANGSFORMEL_UND_VERZEICHNIS = "eingangsformelUndVerzeichnis";
	public static final String LDML_DE_TYPE_EINZELVORSCHRIFT = "einzelvorschrift";
	public static final String LDML_DE_TYPE_EINZELVORSCHRIFT_VERZEICHNISCONTAINER
		=
		"einzelvorschriftVerzeichniscontainer";
	public static final String LDML_DE_TYPE_ERMAECHTIGUNGSNORM = "ermaechtigungsnorm";
	public static final String LDML_DE_TYPE_ERMAECHTIGUNGSNORM_EINGANGSSATZ
		= "ermaechtigungsnormEingangssatz";
	public static final String LDML_DE_TYPE_ERMAECHTIGUNGSNORM_SCHLUSSSATZ = "ermaechtigungsnormSchlusssatz";
	public static final String LDML_DE_TYPE_ERMAECHTIGUNGSNORMEN = "ermaechtigungsnormen";
	public static final String LDML_DE_TYPE_EXTERNES_MARKUP = "externesMarkup";
	public static final String LDML_DE_TYPE_FUNKTIONSBEZEICHNUNG = "funktionsbezeichnung";
	public static final String LDML_DE_TYPE_FUSSNOTE = "fussnote";
	public static final String LDML_DE_TYPE_FUSSNOTE_NUMMER = "fussnoteNummer";
	public static final String LDML_DE_TYPE_GLIEDERUNG_ABSCHNITT = "gliederungAbschnitt";
	public static final String LDML_DE_TYPE_GLIEDERUNG_BUCH = "gliederungBuch";
	public static final String LDML_DE_TYPE_GLIEDERUNG_KAPITEL = "gliederungKapitel";
	public static final String LDML_DE_TYPE_GLIEDERUNG_TEIL = "gliederungTeil";
	public static final String LDML_DE_TYPE_GLIEDERUNG_TITEL = "gliederungTitel";
	public static final String LDML_DE_TYPE_GLIEDERUNG_UNTERABSCHNITT = "gliederungUnterabschnitt";
	public static final String LDML_DE_TYPE_GLIEDERUNG_UNTERKAPITEL = "gliederungUnterkapitel";
	public static final String LDML_DE_TYPE_GLIEDERUNG_UNTERTITEL = "gliederungUntertitel";
	public static final String LDML_DE_TYPE_IDENTIFIKATION = "identifikation";
	public static final String LDML_DE_TYPE_IDENTIFIKATION_MANIFESTATION = "identifikationManifestation";
	public static final String LDML_DE_TYPE_IDENTIFIKATION_VERSION = "identifikationVersion";
	public static final String LDML_DE_TYPE_IDENTIFIKATION_WERK = "identifikationWerk";
	public static final String LDML_DE_TYPE_INHALT = "inhalt";
	public static final String LDML_DE_TYPE_INHALTSABSCHNITT = "inhaltsabschnitt";
	public static final String LDML_DE_TYPE_INHALTSELEMENT = "inhaltselement";
	public static final String LDML_DE_TYPE_INITIANT = "initiant";
	public static final String LDML_DE_TYPE_INLINEELEMENT = "inlineelement";
	public static final String LDML_DE_TYPE_JURISTISCHER_ABSATZ = "juristischerAbsatz";
	public static final String LDML_DE_TYPE_JURISTISCHER_ABSATZ_AUSWAHL = "juristischerAbsatzAuswahl";
	public static final String LDML_DE_TYPE_JURISTISCHER_ABSATZ_LISTE = "juristischerAbsatzListe";
	public static final String LDML_DE_TYPE_KONTEXTELEMENT = "kontextelement";
	public static final String LDML_DE_TYPE_KURZTITEL = "kurztitel";
	public static final String LDML_DE_TYPE_LISTE_MIT_WEITERER_UNTERGLIEDERUNG
		=
		"listeMitWeitererUntergliederung";
	public static final String LDML_DE_TYPE_LISTE_OHNE_WEITERE_UNTERGLIEDERUNG
		=
		"listeOhneWeitereUntergliederung";
	public static final String LDML_DE_TYPE_LISTENELEMENT = "listenelement";
	public static final String LDML_DE_TYPE_METADATEN = "metadaten";
	public static final String LDML_DE_TYPE_NORMATIVE_LISTE = "normativeListe";
	public static final String LDML_DE_TYPE_NORMATIVE_LISTE_EINGANGSSATZ = "normativeListeEingangssatz";
	public static final String LDML_DE_TYPE_NORMATIVE_LISTE_LISTENELEMENT = "normativeListeListenelement";
	public static final String LDML_DE_TYPE_NORMATIVE_LISTE_SCHLUSSSATZ = "normativeListeSchlusssatz";
	public static final String LDML_DE_TYPE_NUMMERIERTE_LISTE = "nummerierteListe";
	public static final String LDML_DE_TYPE_ONTOLOGIE_ORGANISATION_URI = "ontologieOrganisationURI";
	public static final String LDML_DE_TYPE_ONTOLOGIE_PERSON_URI = "ontologiePersonURI";
	public static final String LDML_DE_TYPE_ONTOLOGIE_REFERENZIERUNG_URI = "ontologieReferenzierungURI";
	public static final String LDML_DE_TYPE_ONTOLOGIE_ROLLE_URI = "ontologieRolleURI";
	public static final String LDML_DE_TYPE_ONTOLOGIE_VERWEIS = "ontologieVerweis";
	public static final String LDML_DE_TYPE_ORGANISATION = "organisation";
	public static final String LDML_DE_TYPE_ORIGINALVERSION_URI = "originalversionURI";
	public static final String LDML_DE_TYPE_ORT = "ort";
	public static final String LDML_DE_TYPE_PERSONENNAME = "personenname";
	public static final String LDML_DE_TYPE_PRAEAMBEL = "praeambel";
	public static final String LDML_DE_TYPE_PRAEAMBEL_INHALT = "praeambelInhalt";
	public static final String LDML_DE_TYPE_RECHTSETZUNGSDOKUMENT = "rechtsetzungsdokument";
	public static final String LDML_DE_TYPE_RECHTSETZUNGSDOKUMENT_HAUPTTEIL
		= "rechtsetzungsdokumentHauptteil";
	public static final String LDML_DE_TYPE_RECHTSETZUNGSDOKUMENT_ID_FRBR = "rechtsetzungsdokumentIdFRBR";
	public static final String LDML_DE_TYPE_REFERENZ = "referenz";
	public static final String LDML_DE_TYPE_REGELUNGSSPRACHLICHER_TEIL = "regelungssprachlicherTeil";
	public static final String LDML_DE_TYPE_REGELUNGSTEXT_DOKUMENTENKOPF = "regelungstextDokumentenkopf";
	public static final String LDML_DE_TYPE_REGELUNGSTEXT_EINLEITUNG = "regelungstextEinleitung";
	public static final String LDML_DE_TYPE_REGELUNGSTEXT_ENTWURFSFASSUNG = "regelungstextEntwurfsfassung";
	public static final String LDML_DE_TYPE_REGELUNGSTEXT_GLIEDERUNG = "regelungstextGliederung";
	public static final String LDML_DE_TYPE_REGELUNGSTEXT_HAUPTTEIL = "regelungstextHauptteil";
	public static final String LDML_DE_TYPE_REGELUNGSTEXT_SCHLUSS = "regelungstextSchluss";
	public static final String LDML_DE_TYPE_REGELUNGSTEXT_VERKUENDUNGSFASSUNG
		=
		"regelungstextVerkuendungsfassung";
	public static final String LDML_DE_TYPE_SCHLUSS_UNTERZEICHNUNG = "schlussUnterzeichnung";
	public static final String LDML_DE_TYPE_SCHLUSSFORMEL = "schlussformel";
	public static final String LDML_DE_TYPE_SCHLUSSSATZ = "schlusssatz";
	public static final String LDML_DE_TYPE_SIGNATUR = "signatur";
	public static final String LDML_DE_TYPE_SIGNATURBLOCK = "signaturblock";
	public static final String LDML_DE_TYPE_SPRACHE_FRBR = "spracheFRBR";
	public static final String LDML_DE_TYPE_SPRACHFASSUNG_PRAEAMBEL = "sprachfassungPraeambel";
	public static final String LDML_DE_TYPE_STAAT_FRBR = "staatFRBR";
	public static final String LDML_DE_TYPE_TABELLE = "tabelle";
	public static final String LDML_DE_TYPE_TABELLENBESCHRIFTUNG = "tabellenbeschriftung";
	public static final String LDML_DE_TYPE_TABELLENINHALT = "tabelleninhalt";
	public static final String LDML_DE_TYPE_TABELLENKOPF = "tabellenkopf";
	public static final String LDML_DE_TYPE_TABELLENZEILE = "tabellenzeile";
	public static final String LDML_DE_TYPE_TEILDOKUMENT_URI_FRBR = "teildokumentUriFRBR";
	public static final String LDML_DE_TYPE_TEILDOKUMENT_VERWEIS = "teildokumentVerweis";
	public static final String LDML_DE_TYPE_TEILDOKUMENT_VERWEIS_CONTAINER = "teildokumentVerweisContainer";
	public static final String LDML_DE_TYPE_TEXT_NACH_UNTERGLIEDERUNG = "textNachUntergliederung";
	public static final String LDML_DE_TYPE_TEXT_VOR_UNTERGLIEDERUNG = "textVorUntergliederung";
	public static final String LDML_DE_TYPE_TEXTABSATZ = "textabsatz";
	public static final String LDML_DE_TYPE_TEXTAENDERUNG = "textaenderung";
	public static final String LDML_DE_TYPE_TEXTUELLER_INHALT = "textuellerInhalt";
	public static final String LDML_DE_TYPE_TITELELEMENT = "titelelement";
	public static final String LDML_DE_TYPE_UEBERSCHRIFT = "ueberschrift";
	public static final String LDML_DE_TYPE_UNIVERSELLES_INHALTSELEMENT = "universellesInhaltselement";
	public static final String LDML_DE_TYPE_UNNUMMERIERTE_LISTE = "unnummerierteListe";
	public static final String LDML_DE_TYPE_UNTERUEBERSCHRIFT = "unterueberschrift";
	public static final String LDML_DE_TYPE_VEREINBARUNG_VERT_RA = "vereinbarungVertRA";
	public static final String LDML_DE_TYPE_VEREINBARUNG_VERT_RADOKUMENTENKOPF
		=
		"vereinbarungVertRADokumentenkopf";
	public static final String LDML_DE_TYPE_VEREINBARUNG_VERT_RAHAUPTTEIL = "vereinbarungVertRAHauptteil";
	public static final String LDML_DE_TYPE_VEREINBARUNG_VERT_RASPRACHFASSUNG
		=
		"vereinbarungVertRASprachfassung";
	public static final String LDML_DE_TYPE_VERSIONSNUMMER_FRBR = "versionsnummerFRBR";
	public static final String LDML_DE_TYPE_VERWEISE = "verweise";
	public static final String LDML_DE_TYPE_VERZEICHNIS = "verzeichnis";
	public static final String LDML_DE_TYPE_VERZEICHNISCONTAINER = "verzeichniscontainer";
	public static final String LDML_DE_TYPE_VERZEICHNISEINTRAG = "verzeichniseintrag";
	public static final String LDML_DE_TYPE_VORBLATT = "vorblatt";
	public static final String LDML_DE_TYPE_VORBLATT_ABSCHNITT = "vorblattAbschnitt";
	public static final String LDML_DE_TYPE_VORBLATT_DOKUMENTENKOPF = "vorblattDokumentenkopf";
	public static final String LDML_DE_TYPE_VORBLATT_HAUPTTEIL = "vorblattHauptteil";
	public static final String LDML_DE_TYPE_WAHLPERIODE = "wahlperiode";
	public static final String LDML_DE_TYPE_WEITERE_METADATEN = "weitereMetadaten";
	public static final String JSON_EMPTY_CHILDREN = "{\"text\": \"\"}";
	public static final String SVRL_FAILED_ASSERT = "failed assertion";
	public static final String SVRL_SUCCESSFUL_REPORT = "reported error";
	public static final String EID_SPLITTER_POSITION = "_";
	public static final String EID_SPLITTER_COMPONENT = "__";
	public static final String JPG = "jpg";
	// LDML.de Metadata
	public static final String LDMLDE_INITIANT_BT = "bundestag";
	public static final String LDMLDE_INITIANT_BR = "bundesrat";
	public static final String LDMLDE_INITIANT_BREG = "bundesregierung";
	public static final String LDMLDE_INITIANT_NA = "nicht vorhanden";
	public static final String LDMLDE_TYP_GESETZ = "gesetz";
	public static final String LDMLDE_TYP_VO = "verordnung";
	public static final String LDMLDE_TYP_VWV = "verwaltungsvorschrift";
	public static final String LDMLDE_TYP_VERTG = "vertragsgesetz";
	public static final String LDMLDE_TYP_VERTVO = "vertragsverordnung";
	public static final String LDMLDE_FORM_STAMM = "stammform";
	public static final String LDMLDE_FORM_MANTEL = "mantelform";
	public static final String LDMLDE_INST_BREG = "bundesregierung";
	public static final String LDMLDE_INST_BT = "bundestag";
	public static final String LDMLDE_INST_BR = "bundesrat";
	public static final String LDMLDE_INST_NA = "nicht vorhanden";
	public static final String RESULT_NULL = "null";
	public static final String HTTP_TEMPLATE_NOT_FOUND = "template does not exist";
	public static final String HTTP_DOCUMENT_NOT_FOUND = "document does not exist";
	public static final String TOOLBOX = "toolbox";
	public static final String STRUCTUREDEFINITION = "structureDefinition";
	public static final String VERSION = "version";
	public static final String ATTR_XSI_SCHEMA_LOCATION = "schemaLocation";
	public static final Set<String> AKN_ATTRIBUTES;
	public static final String NS_LDML_DE_META
		= "http://Metadaten.LegalDocML.de";
	public static final String LOC_LDML_DE_META
		=
		"legalDocML.de-metadatenSchema.xsd";
	public static final String VALIDATION_WARN = "warn";

	public static final String INITIAL_DOC_VERSION = "V1";

	public static final String MIME_TYPE_ZIP = "application/zip";
	public static final String MIME_TYPE_PDF = "application/pdf";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String HTTP_HEADER_CONTENT_LENGTH = "Content-Length";

	public static final int KILOBYTE = 1000;
	public static final int KIBIBYTE = 1024;
	public static final String DEFAULT_ZIP_NAME = "download.zip";

	//Plateg
	public static final String API_KEY_NAME = "x-api-key";
	public static final String GID_PARAM_NAME = "gid";
	public static final String PLATEG_URL = "/plattform/**";

	public static final String TPL_NAME_BEGRUENDUNG = "stammgesetz-begruendung";
	public static final String TPL_NAME_VORBLATT = "stammgesetz-vorblatt";
	public static final String TPL_NAME_REGTEXT_STAMMFORM_0 = "stammgesetz-regelungstext-0";
	public static final String TPL_NAME_REGTEXT_STAMMFORM_1 = "stammgesetz-regelungstext-1";
	public static final String TPL_NAME_REGTEXT_STAMMFORM_2 = "stammgesetz-regelungstext-2";
	public static final String TPL_NAME_REGTEXT_STAMMFORM_3 = "stammgesetz-regelungstext-3";
	public static final String TPL_NAME_REGTEXT_MANTELFORM_0 = "mantelgesetz-regelungstext-0";
	public static final String TPL_NAME_REGTEXT_MANTELFORM_1 = "mantelgesetz-regelungstext-0";
	public static final String TPL_NAME_REGTEXT_MANTELFORM_2 = "mantelgesetz-regelungstext-0";
	public static final String TPL_NAME_REGTEXT_MANTELFORM_3 = "mantelgesetz-regelungstext-0";

	public static final String FRBR_DOC_TYPE_REGELUNGSENTWURF = "regelungsentwurf";
	public static final String P_CONTAINER = "akn:p";
	public static final String META_CONTAINER = "akn:meta";
	public static final String META_PROPRIETARY_CONTAINER = "akn:proprietary";
	public static final String META_LDMLDE_CONTAINER = "meta:legalDocML.de_metadaten";
	public static final String META_LDMLDE_TYP = "meta:typ";
	public static final String META_LDMLDE_FORM = "meta:form";
	public static final String META_LDMLDE_FASSUNG = "meta:fassung";
	public static final String META_LDMLDE_ART = "meta:art";
	public static final String META_LDMLDE_INITIANT = "meta:initiant";
	public static final String META_LDMLDE_INSTITUTION = "meta:bearbeitendeInstitution";

	public static final String ROLLE_ERSTELLER = "ERSTELLER";
	public static final String ROLLE_LESER = "LESER";
	public static final String ROLLE_SCHREIBER = "SCHREIBER";
	public static final String ROLLE_HRA_TEILNEHMER = "MITARBEITER";
	public static final String RECHT_LESEN = "LESEN";
	public static final String RECHT_KOMMENTIEREN = "KOMMENTIEREN";
	public static final String RECHT_KOMMENTARE_LESEN = "KOMMENTARE_LESEN";
	public static final String RECHT_SCHREIBEN = "SCHREIBEN";
	public static final String RESSOURCENTYP_DOKUMENT = "DOKUMENTE";
	public static final String RECHT_NEUE_VERSION_ERSTELLEN = "NEUE_VERSION_ERSTELLEN";
	public static final String RESSOURCENTYP_DOKUMENTENMAPPE = "DOKUMENTENMAPPE";
	public static final String RESSOURCENTYP_HRA = "HRA";
	public static final String RESSOURCENTYP_REGELUNGSVORHABEN = "REGELUNGSVORHABEN";

	public static final String RESSORT_KURZBEZEICHNUNG_BT = "BT";
	public static final String RESSORT_KURZBEZEICHNUNG_BR = "BR";

	public static final String EGFA_ATTR_FROM_EGFA = "fromEgfa";

	public static final String EGFA_ATTR_EGFA_IMPORT = "egfaImport";
	public static final String EGFA_ATTR_EDITABLE = "editable";

	public static final UUID DEFAULT_UUID = UUID.fromString("00000000-0000-0000-0000-000000000001");
	public static final String CHANGE_MARK = "lea:changeMark";

	public static final String CHANGE_TRACK_ATTR_TYPE = "lea:changeType";
	private static final byte[] EMPTY_ZIP_CONTENT = new byte[]{
		'P', 'K', 5, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

	public static final String IS_SUBSTITUTE = "lea:isSubstitute";

	public static final String HEADER_FIELD_STELLVERTRETUNG = "X-DE-BUND-EGG-Surrogate-For";

	public static final String ATTR_NAME_VALUE_DEFAULT = "attributsemantik-noch-undefiniert";

	static {
		HashSet<String> tmpSet = new HashSet<>(Arrays
			.asList(ATTR_ALT, ATTR_ALTERNATIVETO, ATTR_AS, ATTR_AUTHORITATIVE, ATTR_BORDER, ATTR_BREAKAT,
				ATTR_BREAKWITH, ATTR_BY, ATTR_CELLPADDING, ATTR_CELLSPACING, ATTR_CHOICE, ATTR_CLASS,
				ATTR_COLSPAN, ATTR_CONTAINS, ATTR_CURRENT, ATTR_DATE, ATTR_DICTIONARY, ATTR_DURATION, ATTR_EID,
				ATTR_EMPOWEREDBY, ATTR_END, ATTR_ENDQUOTE, ATTR_ENDTIME, ATTR_EXCLUSION, ATTR_FOR, ATTR_FROM,
				ATTR_FROMLANGUAGE, ATTR_FROZEN, ATTR_GUID, ATTR_HEIGHT, ATTR_HREF, ATTR_INCLUDEDIN,
				ATTR_INCOMPLETE, ATTR_INLINEQUOTE, ATTR_LANGUAGE, ATTR_LEVEL, ATTR_MARKER, ATTR_NAME,
				ATTR_NORMALIZED, ATTR_NUMBER, ATTR_ORIGINAL, ATTR_ORIGINALTEXT, ATTR_ORIGINATINGEXPRESSION,
				ATTR_OUTCOME, ATTR_PERIOD, ATTR_PIVOT, ATTR_PLACEMENT, ATTR_PLACEMENTBASE, ATTR_POS,
				ATTR_REFERSTO, ATTR_ROWSPAN, ATTR_SHORTFORM, ATTR_SHOWAS, ATTR_SOURCE, ATTR_SRC, ATTR_START,
				ATTR_STARTQUOTE, ATTR_STARTTIME, ATTR_STATUS, ATTR_STYLE, ATTR_TARGET, ATTR_TIME, ATTR_TITLE,
				ATTR_TO, ATTR_TYPE, ATTR_UPTO, ATTR_VALUE, ATTR_WID, ATTR_WIDTH));
		AKN_ATTRIBUTES = Collections.unmodifiableSet(tmpSet);
	}

	private Constants() {
		throw new IllegalStateException("Utility class");
	}


	public static byte[] getEmptyZipContent() {
		return EMPTY_ZIP_CONTENT;
	}

}
