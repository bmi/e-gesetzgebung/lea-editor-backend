// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import de.itzbund.egesetz.bmi.lea.core.ComparisonReporter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;

import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;

/**
 * A concrete reporter class to produce HTML. Uses the default HTML representation of diffs of the used library {@link DiffMatchPatch}.
 */
@RequiredArgsConstructor
public class HtmlComparisonReporter implements ComparisonReporter {

    private static final String HEADING = "Comparison Results";

    private static final DiffMatchPatch DMP = new DiffMatchPatch();

    private final Writer writer;

    private long timestamp;


    /**
     * {@inheritDoc}
     *
     * @param dataSets a collection of comparison datasets
     */
    @SneakyThrows
    @Override
    public void report(Collection<ComparisonDataSet> dataSets) {
        Collection<ComparisonDataSet> dataSetCollection = dataSets;
        if (dataSetCollection == null) {
            dataSetCollection = new ArrayList<>();
        }

        timestamp = System.currentTimeMillis();

        writer.write("<!DOCTYPE html>");
        writeStartTag("html", false);
        writeHTMLHead();
        writeStartTag("body", false);

        writeStartTag("h1", false);
        writer.write(HEADING);
        writeEndTag("h1");

        writeHeader(dataSetCollection);

        for (ComparisonDataSet cds : dataSetCollection) {
            writeComparisonData(cds);
        }

        writeEndTag("body");
        writeEndTag("html");
    }


    @SneakyThrows
    private void writeComparisonData(ComparisonDataSet cds) {
        writeStartTag("h2", false);
        writer.write(String.format("%s - %s", cds.getFstDocumentId(), cds.getSndDocumentId()));
        writeEndTag("h2");

        writeStartTag("p", false);
        writeStartTag("b", false);
        writer.write("Result: ");
        writeEndTag("b");
        writeStartTag("span", false, "class", "result"); //NOSONAR
        writer.write(cds.getCompareResult()
            .name()
            .toLowerCase());
        writeEndTag("span");
        writeEndTag("p");

        writeStartTag("p", false);
        writer.write(DMP.diffPrettyHtml(cds.getDiffs()));
        writeEndTag("p");
    }


    @SneakyThrows
    private void writeHeader(Collection<ComparisonDataSet> dataSetCollection) {
        writeStartTag("p", false);
        writeStartTag("b", false);
        writer.write("Timestamp: ");
        writeEndTag("b");
        writeStartTag("span", false, "class", "ts"); //NOSONAR
        writer.write(getFormattedTimestamp());
        writeEndTag("span");
        writeStartTag("br", true);
        writeStartTag("b", false);
        writer.write("# comparisons: ");
        writeEndTag("b");
        writer.write(String.valueOf(dataSetCollection.size()));
        writeEndTag("p");
    }


    @SneakyThrows
    private void writeHTMLHead() {
        writeStartTag("head", false);

        writeStartTag("title", false); //NOSONAR
        writer.write(HEADING);
        writeEndTag("title"); //NOSONAR

        writeStartTag("meta", true, "charset", "UTF-8");

        writeEndTag("head");
    }


    @SneakyThrows
    private void writeStartTag(String tagName, boolean close, String... attributes) {
        StringBuilder sb = new StringBuilder();
        sb.append('<')
            .append(tagName);

        if (attributes != null && attributes.length > 1) {
            for (int i = 0; i < attributes.length; i += 2) {//NOSONAR
                sb.append(' ')
                    .append(attributes[i])
                    .append("=\"")
                    .append(attributes[i + 1])
                    .append('\"');
            }
        }

        if (close) {
            sb.append('/');
        }
        sb.append('>');

        writer.write(sb.toString());
    }


    @SneakyThrows
    private void writeEndTag(String tagName) {
        writer.write(String.format("</%s>", tagName));
    }


    public String getFormattedTimestamp() {
        return (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(timestamp);
    }

}
