// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import lombok.extern.log4j.Log4j2;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_AKN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_CHILDREN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_CNT_TYPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_TAG_NAME;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_TEXT;

/**
 * A concrete reader to transform JSON documents (for the editor of eGG) into in-memory objects.
 */

@Log4j2
public class JsonDocumentReader extends DocumentReader {

    /**
     * {@inheritDoc}
     *
     * @param inputStream the content of the original document
     * @return a document dataset
     */
    @Override
    public DocumentDataSet readDocument(InputStream inputStream) {
        DocumentDataSet ds = new DocumentDataSet();
        JSONParser jsonParser = new JSONParser();
        counter = new Counter();

        try {
            JSONObject jsonDoc = (JSONObject) jsonParser.parse(new InputStreamReader(inputStream));
            makeDocumentDataItem(jsonDoc, ds);

        } catch (ParseException | IOException e) {
            log.error(e);
        }

        return ds;
    }


    private void makeDocumentDataItem(JSONObject jsonObject, DocumentDataSet ds) {
        String name = getName(jsonObject);
        List<String> attributes = getAttributes(jsonObject);
        ElementTypeDataItem ddi = ElementTypeDataItem.builder()
            .count(1)
            .name(name)
            .isElement(false)
            .attributes(attributes.toArray(new String[0]))
            .build();
        ds.addDataItem(ddi);
        traverseChildren((JSONArray) jsonObject.get(JSON_KEY_CHILDREN), ds, ddi.getId());
    }


    private void makeElementTypeDataItem(JSONObject jsonObject, DocumentDataSet ds, String parentId) {
        String name = getName(jsonObject);
        List<String> attributes = getAttributes(jsonObject);
        ElementTypeDataItem edi = ElementTypeDataItem.builder()
            .count(counter.getElemCount())
            .name(name)
            .isElement(true)
            .parentId(parentId)
            .attributes(attributes.toArray(new String[0]))
            .build();
        ds.addDataItem(edi);
        traverseChildren((JSONArray) jsonObject.get(JSON_KEY_CHILDREN), ds, edi.getId());
    }


    private void makeTextTypeDataItem(JSONObject jsonObject, DocumentDataSet ds, String parentId) {
        String content = (String) jsonObject.get(JSON_KEY_TEXT);
        if (content == null) {
            content = "";
        }
        ds.addDataItem(TextTypeDataItem.builder()
            .count(getTextCount())
            .parentId(parentId)
            .content(content)
            .build());
    }


    private void traverseChildren(JSONArray children, DocumentDataSet ds, String parentId) {
        if (children != null) {
            for (Object obj : children) {
                JSONObject child = (JSONObject) obj;
                if (isText(child)) {
                    incTextCount();
                    makeTextTypeDataItem(child, ds, parentId);
                } else {
                    incElemCount();
                    makeElementTypeDataItem(child, ds, parentId);
                }
            }
        }
    }


    private String getName(JSONObject jsonObject) {
        return (String) jsonObject.get(JSON_KEY_TAG_NAME);
    }


    private List<String> getAttributes(JSONObject jsonObject) {
        JSONObject akn = (JSONObject) jsonObject.get(JSON_KEY_AKN);
        List<String> attributes = new ArrayList<>();

        if (akn != null) {
            for (Object keyObj : akn.keySet()) {
                String key = (String) keyObj;
                if (!JSON_KEY_CNT_TYPE.equals(key)) {
                    attributes.add(key);
                    attributes.add(String.valueOf(akn.get(key)));
                }
            }
        }

        return attributes;
    }


    private boolean isText(JSONObject jsonObject) {
        return jsonObject.containsKey(JSON_KEY_TEXT);
    }

}
