// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import lombok.Data;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;

@Data
public class ObjectListDiff<K> {

    private final DiffMatchPatch.Operation operation;
    private final K key;

}
