// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.xml;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import java.util.Iterator;
import java.util.List;

import static de.itzbund.egesetz.bmi.lea.core.Constants.NS_SCHEMA_INSTANCE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.TARGET_NS;

/**
 * A {@link NamespaceContext} specific to the application.
 */
public class AppNamespaceContext implements NamespaceContext {

    private static final String ERR_MSG = "No namespace URI provided!";

    @Override
    public String getNamespaceURI(String prefix) {
        if (prefix == null) {
            throw new IllegalArgumentException("No prefix provided!");
        } else if (prefix.equals(XMLConstants.DEFAULT_NS_PREFIX)) {
            return TARGET_NS;
        } else if ("akn".equals(prefix)) {
            return TARGET_NS;
        } else if ("xsi".equals(prefix)) {
            return NS_SCHEMA_INSTANCE;
        } else {
            return XMLConstants.NULL_NS_URI;
        }
    }


    @Override
    public String getPrefix(String namespaceURI) {
        if (namespaceURI == null) {
            throw new IllegalArgumentException(ERR_MSG);
        } else if (TARGET_NS.equals(namespaceURI)) {
            return "akn";
        } else if (NS_SCHEMA_INSTANCE.equals(namespaceURI)) {
            return "xsi";
        } else {
            return XMLConstants.DEFAULT_NS_PREFIX;
        }
    }


    @Override
    public Iterator<String> getPrefixes(String namespaceURI) {
        if (namespaceURI == null) {
            throw new IllegalArgumentException(ERR_MSG);
        } else if (TARGET_NS.equals(namespaceURI)) {
            return List.of("akn", XMLConstants.DEFAULT_NS_PREFIX).iterator();
        } else if (NS_SCHEMA_INSTANCE.equals(namespaceURI)) {
            return List.of("xsi").iterator();
        } else {
            return List.of(XMLConstants.DEFAULT_NS_PREFIX).iterator();
        }
    }

}
