// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import de.itzbund.egesetz.bmi.lea.core.ChangeMarker;
import de.itzbund.egesetz.bmi.lea.core.json.JSONUtils;
import de.itzbund.egesetz.bmi.lea.core.util.BipartiteList;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.CHANGE_MARK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BLOCK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BLOCKLIST;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CONTENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_HEADING;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LIST;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LISTINTRODUCTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LISTWRAPUP;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_NUM;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_OL;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_P;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TABLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TOCITEM;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_UL;
import static de.itzbund.egesetz.bmi.lea.core.Constants.IS_SUBSTITUTE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_CHILDREN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_ID_GUID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_ID_REF_GUID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_TEXT_WRAPPER;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.HIERARCHY_ELEMENTS;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addAttributes;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.clearChildrenArray;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.findChild;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDescendant;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getGuid;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getHeadingOfElement;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getLocalName;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getNumElement;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getStringAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.hasNoChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.isEffectivelyEmpty;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.isOneOf;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.isSubstitute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewDefaultJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.replaceChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.withDefaultPrefix;

@Log4j2
@SuppressWarnings("unchecked")
public class CompareUtils {

    static final Set<String> LOWEST_LEVEL_ELEMENTS = Stream.of(ELEM_P, ELEM_BLOCK, ELEM_NUM, ELEM_HEADING
            /*ELEM_TOCITEM, ELEM_LIST, ELEM_BLOCKLIST, ELEM_OL, ELEM_UL, ELEM_TABLE*/)
        .collect(Collectors.toCollection(HashSet::new));

    private static final Set<String> BLOCK_ELEMENTS = Stream.of(ELEM_P, ELEM_BLOCK, ELEM_NUM, ELEM_HEADING,
            ELEM_TOCITEM)
        .collect(Collectors.toCollection(HashSet::new));

    private static final Set<String> LIST_ELEMENTS = Stream.of(ELEM_LIST, ELEM_BLOCKLIST, ELEM_OL, ELEM_UL)
        .collect(Collectors.toCollection(HashSet::new));

    @Getter
    @AllArgsConstructor
    public enum Origin {
        BASE("base"),
        VERSION("version");

        private final String literal;
    }

    private CompareUtils() {
    }

    public static Set<String> getLowestLevelElements() {
        return LOWEST_LEVEL_ELEMENTS;
    }

    public static Set<String> getBlockElements() {
        return BLOCK_ELEMENTS;
    }

    public static Set<String> getListElements() {
        return LIST_ELEMENTS;
    }

    // assumed, that document is not empty and valid AkomaNtoso document
    public static List<JSONObject> jsonTreeToFlatList(@NonNull JSONObject jsonObject) {
        List<JSONObject> objects = new ArrayList<>();
        JSONArray children = (JSONArray) jsonObject.computeIfAbsent(JSON_KEY_CHILDREN, k -> new JSONArray());
        collectElements(children, objects);
        return objects;
    }

    private static void collectElements(JSONArray jsonArray, List<JSONObject> list) {
        jsonArray.forEach(child -> {
            JSONObject jsonObject = (JSONObject) child;
            collectElements(jsonObject, list);
        });
    }

    private static void collectElements(JSONObject jsonObject, List<JSONObject> list) {
        String type = getType(jsonObject);

        if (type == null || type.equals(JSON_VAL_TEXT_WRAPPER)) {
            return;
        }

        if (isOneOf(type, BLOCK_ELEMENTS)) {
            list.add(jsonObject);
        }

        JSONArray children = (JSONArray) jsonObject.computeIfAbsent(JSON_KEY_CHILDREN, k -> new JSONArray());
        collectElements(children, list);
    }

    public static void markContent(JSONObject jsonObject, ChangeMarker marker) {
        if (jsonObject != null && marker != null) {
            jsonObject.put(CHANGE_MARK, marker.getType());
        }
    }


    public static void markContentP(JSONObject jsonObject, ElementChangeMarker marker) {
        JSONObject pObject = getDescendant(jsonObject, true,
            ELEM_CONTENT, ELEM_P);
        markContent(pObject, marker);
    }

    public static void markAllContent(JSONObject jsonObject, ElementChangeMarker marker) {
        JSONObject contentObject = findChild(jsonObject, withDefaultPrefix(ELEM_CONTENT));
        JSONArray contentChildren = getChildren(contentObject);
        contentChildren.forEach(child -> markContent((JSONObject) child, marker));
    }

    public static void markText(JSONObject parent, TextChangeMarker marker) {
        if (parent != null && marker != null && !getType(parent).equals(JSON_VAL_TEXT_WRAPPER)) {
            JSONArray children = getChildren(parent);
            if (children != null && children.size() == 1) {
                JSONObject child = (JSONObject) children.get(0);
                if (getType(child).equals(JSON_VAL_TEXT_WRAPPER)) {
                    markContent(child, marker);
                }
            }
        }
    }


    public static String getChangeMarker(JSONObject jsonObject) {
        String mark = getStringAttribute(jsonObject, CHANGE_MARK);
        return mark == null ? "" : mark;
    }


    public static void markListContent(JSONObject listObject, ElementChangeMarker marker) {
        JSONArray listChildren = getChildren(findChild(listObject, withDefaultPrefix(ELEM_LIST)));
        if (listChildren != null) {
            listChildren.forEach(child -> markContent(
                (JSONObject) child, marker));
        } else {
            markContent(listObject, marker);
        }
    }


    public static void markStructure(JSONObject jsonObject, ElementChangeMarker marker) {
        if (!isEffectivelyEmpty(jsonObject) && marker != null) {
            if (JSON_VAL_TEXT_WRAPPER.equals(getType(jsonObject))) {
                TextChangeMarker mark2;
                switch (marker) {
                    case ADDED:
                        mark2 = TextChangeMarker.INSERTED;
                        break;
                    case DELETED:
                        mark2 = TextChangeMarker.ERASED;
                        break;
                    default:
                        mark2 = TextChangeMarker.UNCHANGED;
                }
                markContent(jsonObject, mark2);
            } else {
                markContent(jsonObject, marker);
                JSONArray children = getChildren(jsonObject);
                if (!isEffectivelyEmpty(children)) {
                    children.forEach(obj -> markStructure((JSONObject) obj, marker));
                }
            }
        }
    }


    public static JSONObject makeSubstituteFor(JSONObject original) {
        String type = getType(original);
        JSONObject substitute = makeNewDefaultJSONObject(type);
        addAttributes(substitute,
            Pair.of(IS_SUBSTITUTE, Boolean.TRUE.toString()),
            Pair.of(JSON_KEY_ID_REF_GUID, getGuid(original)));
        markContent(substitute, ElementChangeMarker.EMPTY);
        addText(substitute, "");

        return substitute;
    }

    public static JSONObject makeSubstituteForStructure(JSONObject jsonObject, ElementChangeMarker changeMarker) {
        if (jsonObject == null || JSON_VAL_TEXT_WRAPPER.equals(getType(jsonObject))) {
            return getTextWrapper("", null);
        }

        JSONObject substitute = makeSubstituteFor(jsonObject);
        if (changeMarker != null) {
            markContent(jsonObject, changeMarker);
        }

        JSONArray children = getChildren(jsonObject);
        if (children != null) {
            JSONArray substituteChildren = new JSONArray();

            children.forEach(child -> {
                JSONObject childObject = (JSONObject) child;
                if (!isHierarchyLevel(childObject)) {
                    substituteChildren.add(makeSubstituteForStructure(childObject, changeMarker));
                }
            });

            replaceChildren(substitute, substituteChildren);
        }

        return substitute;
    }

    private static boolean isHierarchyLevel(JSONObject jsonObject) {
        return HIERARCHY_ELEMENTS.contains(getLocalName(getType(jsonObject)));
    }

    public static void markMatchingPair(JSONObject left, JSONObject right) {
        ElementChangeMarker marker = ElementChangeMarker.MATCHED;
        if (!isSubstitute(left) && !isSubstitute(right)) {
            markContent(left, marker);
            markContent(getNumElement(left), marker);
            markContent(getHeadingOfElement(left), marker);
            markContent(right, marker);
            markContent(getNumElement(right), marker);
            markContent(getHeadingOfElement(right), marker);
        }
    }


    public static boolean isAssignable(JSONObject left, JSONObject right) {
        return getGuid(left).equals(getGuid(right));
    }

    public static BipartiteList<String> getGuids(List<JSONObject> objects, int startIndex) {
        if (objects == null || objects.isEmpty()) {
            return null;
        }

        return new BipartiteList<>(
            objects.stream()
                .map(o -> getStringAttribute(o, JSON_KEY_ID_GUID))
                .collect(Collectors.toList()),
            startIndex);
    }

    public static boolean isLeave(JSONObject jsonObject) {
        return getLowestLevelElements().contains(getLocalName(getType(jsonObject)));
    }

    public static Map<String, JSONObject> getTopLevelStructures(JSONObject document) {
        JSONArray children1 = (JSONArray) document.get(JSON_KEY_CHILDREN);
        JSONObject documentNode = (JSONObject) children1.get(0);
        JSONArray children2 = (JSONArray) documentNode.get(JSON_KEY_CHILDREN);

        Map<String, JSONObject> map = new HashMap<>();
        children2.forEach(obj -> {
            JSONObject jsonObject = (JSONObject) obj;
            String type = getLocalName(getType(jsonObject));
            map.put(type, jsonObject);
        });

        return map;
    }

    public static boolean isList(String type) {
        return getListElements().contains(type.substring(type.indexOf(':') + 1));
    }

    public static boolean isTable(String type) {
        return type.contains(ELEM_TABLE);
    }

    public static boolean isTextualElement(String type) {
        return getBlockElements().contains(type.substring(type.indexOf(':') + 1));
    }


    // baseObject and versionObject are lea:textWrappers
    public static void markChangesInTextWrapper(@NonNull JSONObject baseObject, @NonNull JSONObject versionObject,
        @NonNull List<DiffMatchPatch.Diff> diffs) {
        if (diffs.size() == 1 && diffs.get(0).operation == DiffMatchPatch.Operation.EQUAL) {
            markContent(baseObject, TextChangeMarker.UNCHANGED);
            markContent(versionObject, TextChangeMarker.UNCHANGED);
        } else {
            markContent(baseObject, TextChangeMarker.ERASED);
            markContent(versionObject, TextChangeMarker.INSERTED);
        }
    }

    public static String toTextChangeMarkerLiteral(String marker) {
        if (marker == null || marker.isBlank()) {
            return TextChangeMarker.UNCHANGED.getType();
        } else {
            if (TextChangeMarker.fromLiteral(marker) != null) {
                return marker;
            } else {
                ElementChangeMarker changeMarker = ElementChangeMarker.fromLiteral(marker);
                assert changeMarker != null;

                switch (changeMarker) {
                    case ADDED:
                        return TextChangeMarker.INSERTED.getType();
                    case DELETED:
                        return TextChangeMarker.ERASED.getType();
                    default:
                        return TextChangeMarker.UNCHANGED.getType();
                }
            }
        }
    }


    public static void markChangesInText(@NonNull JSONObject baseObject, @NonNull JSONObject versionObject,
        @NonNull List<DiffMatchPatch.Diff> diffs) {
        JSONArray baseChildren = clearChildrenArray(baseObject);
        JSONArray versionChildren = clearChildrenArray(versionObject);
        Set<DiffMatchPatch.Operation> foundOPs = new HashSet<>();

        diffs.forEach(diff -> {
            foundOPs.add(diff.operation);
            switch (diff.operation) {
                case DELETE:
                    addChildren(baseObject, getTextWrapper(diff.text, TextChangeMarker.ERASED));
                    break;
                case INSERT:
                    addChildren(versionObject, getTextWrapper(diff.text, TextChangeMarker.INSERTED));
                    break;
                default: // EQUAL
                    addChildren(baseObject, getTextWrapper(diff.text, TextChangeMarker.UNCHANGED));
                    addChildren(versionObject, getTextWrapper(diff.text, TextChangeMarker.UNCHANGED));
            }
        });

        fixChildren(baseObject, baseChildren);
        fixChildren(versionObject, versionChildren);

        int opSize = foundOPs.size();
        if (foundOPs.isEmpty() || opSize > 1 || (opSize == 1 && foundOPs.contains(DiffMatchPatch.Operation.EQUAL))) {
            markContent(baseObject, ElementChangeMarker.MATCHED);
            markContent(versionObject, ElementChangeMarker.MATCHED);
        } else {
            if (foundOPs.contains(DiffMatchPatch.Operation.INSERT)) {
                markContent(baseObject, ElementChangeMarker.EMPTY);
                markContent(versionObject, ElementChangeMarker.ADDED);
            } else {
                markContent(baseObject, ElementChangeMarker.DELETED);
                markContent(versionObject, ElementChangeMarker.EMPTY);
            }
        }
    }

    private static void fixChildren(JSONObject parent, JSONArray children) {
        if (hasNoChildren(parent)) {
            replaceChildren(parent, children);
        }
    }


    public static JSONObject getTextWrapper(String text, TextChangeMarker changeMarker) {
        JSONObject textWrapper = JSONUtils.getTextWrapper(text);
        if (changeMarker != null) {
            markContent(textWrapper, changeMarker);
        }
        return textWrapper;
    }

    public static String getHdRconformantBaseText(List<DiffMatchPatch.Diff> diffs) {
        StringBuilder sb = new StringBuilder();
        diffs.forEach(diff -> {
            if (diff.operation == DiffMatchPatch.Operation.DELETE) {
                sb.append("[")
                    .append(diff.text)
                    .append("]");
            } else if (diff.operation == DiffMatchPatch.Operation.EQUAL) {
                sb.append(diff.text);
            }
        });
        return sb.toString();
    }

    public static String getHdRconformantVersionText(List<DiffMatchPatch.Diff> diffs) {
        StringBuilder sb = new StringBuilder();
        diffs.forEach(diff -> {
            if (diff.operation == DiffMatchPatch.Operation.INSERT) {
                sb.append("<")
                    .append(diff.text)
                    .append(">");
            } else if (diff.operation == DiffMatchPatch.Operation.EQUAL) {
                sb.append(diff.text);
            }
        });
        return sb.toString();
    }


    public static void markElementPair(JSONObject left, JSONObject right) {
        if (isSubstitute(left)) {
            markContent(right, ElementChangeMarker.ADDED);
        } else if (isSubstitute(right)) {
            markContent(left, ElementChangeMarker.DELETED);
        } else {
            markContent(left, ElementChangeMarker.MATCHED);
            markContent(right, ElementChangeMarker.MATCHED);
        }
    }


    public static void markMovedElements(JSONObject left, JSONObject right) {
        markContent(left, ElementChangeMarker.MOVED);
        markContent(right, ElementChangeMarker.MOVED);
    }

    public static JSONArray getContentChildren(JSONObject parent) {
        JSONArray children = getChildren(parent);
        if (parent == null || children == null || children.size() != 1 ||
            !getLocalName(getType((JSONObject) children.get(0))).equals(ELEM_CONTENT)) {
            return new JSONArray();
        } else {
            JSONObject contentObject = (JSONObject) children.get(0);
            return Objects.requireNonNullElse(getChildren(contentObject), new JSONArray());
        }
    }

    public static List<JSONObject> getListOfCells(JSONArray contentChildren) {
        return getListOfCellsAcc(contentChildren, new ArrayList<>(), makeNewDefaultJSONObject(ELEM_BLOCK));
    }

    private static List<JSONObject> getListOfCellsAcc(JSONArray contentChildren, List<JSONObject> cells, JSONObject currentCell) {
        for (Object obj : contentChildren) {
            JSONObject child = (JSONObject) obj;
            String type = getLocalName(getType(child));

            switch (type) {
                case ELEM_P:
                case ELEM_BLOCK:
                case ELEM_HEADING:
                    handleTextContent(child, currentCell);
                    break;
                case ELEM_BLOCKLIST:
                    handleBlockList(child, cells, currentCell);
                    break;
                case ELEM_OL:
                case ELEM_UL:
                    handleHTMLList(child, cells, currentCell);
                    break;
                case ELEM_NUM:
                    handleNumContent(child, currentCell);
                    break;
                default:
                    log.error("unexpected type of child of content: {}", type);
                    break;
            }
        }

        cells.add(currentCell);
        return cells;
    }

    private static void handleNumContent(JSONObject numObject, JSONObject currentCell) {
        addChildren(currentCell, JSONUtils.getTextWrapper(getText(numObject)));
    }

    private static void handleTextContent(JSONObject textBlock, JSONObject currentCell) {
        addChildren(currentCell, getChildren(textBlock));
    }

    private static void handleBlockList(JSONObject blockList, List<JSONObject> cells, JSONObject currentCell) {
        if (!hasNoChildren(currentCell)) {
            cells.add(currentCell);
            currentCell = makeNewDefaultJSONObject(ELEM_BLOCK);
        }

        JSONArray blockListChildren = getChildren(blockList);
        for (Object obj : blockListChildren) {
            JSONObject childObject = (JSONObject) obj;
            String type = getLocalName(getType(childObject));

            switch (type) {
                case ELEM_LISTINTRODUCTION:
                case ELEM_LISTWRAPUP:
                    handleBlockListIntroOrWrapUp(childObject, cells, currentCell);
                    break;
                default:
                    handleBlockListItem(childObject, cells, currentCell);
            }
        }
    }

    private static void handleBlockListIntroOrWrapUp(JSONObject listPart, List<JSONObject> cells, JSONObject currentCell) {
        addChildren(currentCell, getChildren(listPart));
        if (!hasNoChildren(currentCell)) {
            cells.add(currentCell);
            currentCell = makeNewDefaultJSONObject(ELEM_BLOCK);
        }
    }

    private static void handleBlockListItem(JSONObject listItem, List<JSONObject> cells, JSONObject currentCell) {
        getListOfCellsAcc(getChildren(listItem), cells, currentCell);
    }

    private static void handleHTMLList(JSONObject htmlList, List<JSONObject> cells, JSONObject currentCell) {
        JSONArray listItems = getChildren(htmlList);
        boolean ordered = getLocalName(getType(htmlList)).equals(ELEM_OL);

        for (Object obj : listItems) {
            JSONObject listItem = (JSONObject) obj;
            JSONObject numObject;
        }
    }


    public static boolean isChangeMarked(JSONObject jsonObject, ChangeMarker marker) {
        if (jsonObject == null || jsonObject.isEmpty()) {
            return false;
        }

        String mark = getStringAttribute(jsonObject, CHANGE_MARK);
        return mark != null && !mark.isBlank() && mark.equals(marker.getType());
    }

    public static boolean isChangedMarkedOneOf(JSONObject jsonObject, ChangeMarker... markers) {
        for (ChangeMarker marker : markers) {
            if (isChangeMarked(jsonObject, marker)) {
                return true;
            }
        }
        return false;
    }

}
