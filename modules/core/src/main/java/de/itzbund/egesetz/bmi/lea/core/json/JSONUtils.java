// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.json;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_EID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_GUID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_HREF;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_NAME;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_SHOWAS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.CHANGE_TRACK_ATTR_TYPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.EGFA_ATTR_EDITABLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.EGFA_ATTR_FROM_EGFA;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ARTICLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_AUTHORIALNOTE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BOOK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CAPTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CHAPTER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_HEADING;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LONGTITLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_MARKER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_NUM;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_P;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PARAGRAPH;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PART;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_POINT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SECTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SUBCHAPTER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SUBSECTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SUBTITLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TABLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TBLOCK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TD;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TH;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TITLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TOC;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TR;
import static de.itzbund.egesetz.bmi.lea.core.Constants.IS_SUBSTITUTE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_CHILDREN;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_COMMENT_ID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_COMMENT_POSITION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_ID_GUID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_ID_REF_GUID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_TEXT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_KEY_TYPE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_TYPE_COLLECTIONBODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_TYPE_COMPONENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_TYPE_DOCUMENTREF;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_CHANGE_WRAPPER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_COMMENT_MARKER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_COMMENT_WRAPPER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.JSON_VAL_TEXT_WRAPPER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.LEA_PREFIX;
import static de.itzbund.egesetz.bmi.lea.core.Constants.TARGET_NS;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.areAllNull;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.combineSets;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.isMissing;

@Log4j2
@SuppressWarnings({"unchecked", "java:S1168"})
public class JSONUtils {

    @Getter
    private static final JSONParser JSON_PARSER = new JSONParser();

    private static final String EID_TEMPLATE_COMPONENT = "rdokhauptteil-1_tldokverweis-%s";
    private static final String EID_TEMPLATE_DOCREF = "rdokhauptteil-1_tldokverweis-%s_verweis-1";

    @SuppressWarnings({"java:S5998", "java:S5843"})
    public static final Pattern PTN_EGFA_HEADING = Pattern.compile(
        "((([A-Z]|[IVX]{1,4})[.] )+(\\d+[.]? )*([a-z]{1,3}\\) )*)([a-zäöüA-ZÄÖÜ ]+)");
    private static final Pattern PTN_PURE_NUM = Pattern.compile("(\\w+)[.)]?");

    private static final Pattern PTN_OPTIONAL_GUID =
        Pattern.compile("\"GUID\":\\s?\"([0-9a-f]{8}\\-[0-9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-f]{12})?\"");

    public static final Set<String> HIERARCHY_ELEMENTS = Stream.of(ELEM_BOOK, ELEM_PART, ELEM_CHAPTER, ELEM_SUBCHAPTER,
            ELEM_SECTION, ELEM_SUBSECTION, ELEM_TITLE, ELEM_SUBTITLE, ELEM_ARTICLE)
        .collect(Collectors.toCollection(HashSet::new));

    public static final Set<String> GLIEDERUNGSEINHEITEN = combineSets(
        Stream.of(ELEM_LONGTITLE, ELEM_TOC)
            .collect(Collectors.toCollection(HashSet::new)),
        HIERARCHY_ELEMENTS,
        Stream.of(ELEM_PARAGRAPH, ELEM_POINT)
            .collect(Collectors.toCollection(HashSet::new))
    );

    public static final Set<String> TEXT_NODES = Stream.of(JSON_VAL_TEXT_WRAPPER, JSON_VAL_CHANGE_WRAPPER)
        .collect(Collectors.toCollection(HashSet::new));


    private JSONUtils() {
    }


    public static String setGUIDsInTemplate(String templateContent) {
        return Utils.replaceTokens(templateContent, PTN_OPTIONAL_GUID,
            p -> String.format("\"GUID\":\"%s\"", UUID.randomUUID()));
    }


    public static JSONObject getFirstChild(JSONObject parent) {
        if (parent == null) {
            return null;
        }

        JSONArray children = (JSONArray) parent.get(JSON_KEY_CHILDREN);
        if (children == null || children.isEmpty()) {
            return null;
        }

        return (JSONObject) children.get(0);
    }


    public static JSONObject findChild(JSONObject parent, String... types) {
        if (parent == null) {
            return null;
        }

        JSONArray children = (JSONArray) parent.get(JSON_KEY_CHILDREN);
        if (children == null || children.isEmpty()) {
            return null;
        }

        List<String> typeList = Arrays.asList(types);
        for (Object obj : children) {
            JSONObject jsonObject = (JSONObject) obj;
            String type = getType(jsonObject);
            if (typeList.contains(type)) {
                return jsonObject;
            }
        }
        return null;
    }


    public static JSONObject getDescendant(JSONObject jsonObject, boolean useDefaultPrefix, String... pathOfTypes) {
        if (pathOfTypes == null || pathOfTypes.length == 0) {
            return jsonObject;
        }

        JSONArray children;
        if (jsonObject == null
            || (children = (JSONArray) jsonObject.get(JSON_KEY_CHILDREN)) == null
            || children.isEmpty()) {
            return null;
        }

        String temp = pathOfTypes[0];
        String currentType = (useDefaultPrefix && !"*".equals(temp)) ? withDefaultPrefix(temp) : temp;
        JSONObject child = "*".equals(currentType) ? getFirstChild(jsonObject) : findChild(jsonObject, currentType);
        String[] restTypes = Arrays.copyOfRange(pathOfTypes, 1, pathOfTypes.length);

        return getDescendant(child, useDefaultPrefix, restTypes);
    }


    public static String getType(JSONObject jsonObject) {
        return getStringAttribute(jsonObject, JSON_KEY_TYPE);
    }


    public static String getStringAttribute(JSONObject jsonObject, String key) {
        return jsonObject == null ? "" : (String) jsonObject.get(key);
    }


    public static String getMarker(JSONObject jsonObject) {
        if (jsonObject == null) {
            return null;
        }

        JSONObject numObject = getFirstChild(jsonObject);
        if (numObject == null || !"akn:num".equals(getType(numObject))) {
            return null;
        }

        JSONObject markerObject = findChild(numObject, withDefaultPrefix(ELEM_MARKER));
        if (markerObject == null || !"akn:marker".equals(getType(markerObject))) {
            return null;
        }

        String name = (String) markerObject.get(ATTR_NAME);
        if (Utils.isMissing(name)) {
            return null;
        }

        return name;
    }


    public static String getText(JSONObject jsonObject) {
        if (jsonObject == null) {
            return null;
        }

        if (hasOnlyText(jsonObject)) {
            JSONObject textNode = TEXT_NODES.contains(getType(jsonObject))
                ? getFirstChild(jsonObject)
                : getFirstChild(getFirstChild(jsonObject));
            return textNode == null ? null : (String) textNode.get(JSON_KEY_TEXT);
        } else {
            JSONArray children = getChildren(jsonObject);
            if (children != null && !children.isEmpty()) {
                List<JSONObject> childObjects = (List<JSONObject>) children.stream()
                    .filter(child -> TEXT_NODES.contains(getType((JSONObject) child)))
                    .collect(Collectors.toList());
                if (childObjects.size() == children.size()) {
                    return childObjects.stream()
                        .map(child -> {
                            JSONObject textNode = getFirstChild(child);
                            return (String) textNode.get(JSON_KEY_TEXT);
                        })
                        .collect(Collectors.joining());
                } else {
                    return "";
                }
            } else {
                return "";
            }
        }
    }


    public static String getEffectiveTextValue(Object object) {
        if (Utils.isMissing(object)) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        if (object instanceof JSONArray) {
            JSONArray jsonArray = (JSONArray) object;
            determineEffectiveTextValue(jsonArray, sb);
            return sb.toString();
        } else if (object instanceof JSONObject) {
            JSONObject jsonObject = (JSONObject) object;
            determineEffectiveTextValue(jsonObject, sb);
            return sb.toString();
        } else {
            return "";
        }
    }


    private static void determineEffectiveTextValue(JSONObject jsonObject, StringBuilder sb) {
        if (jsonObject.isEmpty()) {
            return;
        }

        String type = getLocalName(getType(jsonObject));
        if (!Utils.isMissing(type) && type.equals(ELEM_AUTHORIALNOTE)) {
            // for the time being authorial notes, i.e. footnotes, are ignored
            return;
        }

        if (jsonObject.containsKey(JSON_KEY_TEXT)) {
            append(sb, (String) jsonObject.get(JSON_KEY_TEXT));
        } else {
            determineEffectiveTextValue((JSONArray) jsonObject.get(JSON_KEY_CHILDREN), sb);
        }
    }


    private static void append(StringBuilder sb, String text) {
        if (text.isBlank()) {
            return;
        }

        if (sb.length() > 0) {
            sb.append(" ");
        }

        sb.append(text.trim());
    }


    // it's assumed this is always an array of JSON objects
    private static void determineEffectiveTextValue(JSONArray jsonArray, StringBuilder sb) {
        if (jsonArray == null || jsonArray.isEmpty()) {
            return;
        }

        jsonArray.forEach(obj -> {
            JSONObject jsonObject = (JSONObject) obj;
            determineEffectiveTextValue(jsonObject, sb);
        });
    }


    public static void updateTextContent(JSONObject jsonObject, String updatedText) {
        if (!hasOnlyText(jsonObject) || updatedText == null) {
            return;
        }

        JSONObject textNode = getFirstChild(getFirstChild(jsonObject));
        if (textNode == null) {
            return;
        }

        textNode.put(JSON_KEY_TEXT, updatedText);
    }


    @SuppressWarnings("java:S1067")
    public static boolean hasOnlyText(JSONObject jsonObject) {
        JSONArray children;
        JSONObject child;

        return (jsonObject != null)
            && ((children = (JSONArray) jsonObject.get(JSON_KEY_CHILDREN)) != null)
            && (children.size() == 1)
            && ((child = (JSONObject) children.get(0)) != null)
            && ((TEXT_NODES.contains(getType(child))) ||
            (child.containsKey(JSON_KEY_TEXT))
        );
    }

    public static boolean isTextWrapper(JSONObject jsonObject) {
        return jsonObject != null && !jsonObject.isEmpty() && JSON_VAL_TEXT_WRAPPER.equals(getType(jsonObject));
    }

    /**
     * Merges text content of JSONObjects in one JSONObject. If 0 <= index < jsonObjects.length then the result is the given JSONObject at index with the merged
     * text. Otherwise, a new JSONObject is created. If the delimiter is given, then the single texts will be merged with that delimiter in between.
     *
     * @param index       Index in jsonObjects array to specify which of the objects should take the result
     * @param delimiter   Optional delimiter to be set between the texts
     * @param type        Optional type of new JSONObject if index is out of bounds
     * @param jsonObjects array of JSONObjects only containing text
     * @return a JSONObject with the merged text content of all input objects
     */
    public static JSONObject mergeTextContent(int index, String delimiter, String type, JSONObject... jsonObjects) {
        int size = jsonObjects == null ? 0 : jsonObjects.length;
        boolean validIndex = 0 <= index && index < size;

        if (jsonObjects == null || size == 0) {
            return null;
        } else if (size == 1) {
            return jsonObjects[0];
        } else if (!validIndex && Utils.isMissing(type)) {
            return null;
        } else {
            return mergeTextContent(index, validIndex, delimiter, type, jsonObjects);
        }
    }

    private static JSONObject mergeTextContent(int index, boolean validIndex, String delimiter, String type, JSONObject... jsonObjects) {
        JSONObject result = validIndex ? jsonObjects[index] : makeNewDefaultJSONObject(type);
        JSONObject delimiterObject = (delimiter == null || delimiter.isEmpty()) ? null : getTextWrapper(delimiter);
        List<JSONObject> textWrappers = Arrays.stream(jsonObjects)
            .map(JSONUtils::getTextWrapper)
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
        int size = textWrappers.size() - 1;
        clearChildrenArray(result);
        for (int i = 0; i <= size; i++) {
            addChildren(result, textWrappers.get(i));
            if (delimiterObject != null && i < size) {
                addChildren(result, delimiterObject);
            }
        }
        return result;
    }

    private static JSONObject getTextWrapper(JSONObject parent) {
        if (hasOnlyText(parent)) {
            if (getType(parent).equals(JSON_VAL_TEXT_WRAPPER)) {
                return parent;
            } else {
                JSONArray children = getChildren(parent);
                return (JSONObject) children.get(0);
            }
        } else {
            return null;
        }
    }


    /**
     * Adds a document reference to a document collection document. Expected JSON structure:
     * <pre>
     *     {@code
     *     {
     *          "type": "akn:collectionBody",
     *          "children": [
     *              {
     *                  "children": [
     *                      {
     *                          "text": ""
     *                      }
     *                  ],
     *                  "type": "lea:textWrapper"
     *              }
     *          ]
     *      }
     *     }
     * </pre>
     *
     * @param collectionBody root element containing the document references
     */
    public static void addDocumentRef(@NonNull JSONObject collectionBody, String path, String literal, int count) {
        if (!JSON_TYPE_COLLECTIONBODY.equals(getType(collectionBody))) {
            log.error("is no 'akn:collectionBody': {}", collectionBody.toJSONString());
            return;
        }

        JSONArray children = (JSONArray) collectionBody.computeIfAbsent(JSON_KEY_CHILDREN, k -> new JSONArray());

        // no document reference included yet
        if (hasOnlyText(collectionBody)) {
            children.clear();
        }

        // add new element
        JSONObject docRef = new JSONObject();
        docRef.put(JSON_KEY_TYPE, JSON_TYPE_DOCUMENTREF);
        docRef.put(ATTR_GUID, UUID.randomUUID()
            .toString());
        docRef.put(ATTR_EID, String.format(EID_TEMPLATE_DOCREF, count));
        docRef.put(ATTR_HREF, path);
        docRef.put(ATTR_SHOWAS, literal);
        docRef.put(JSON_KEY_CHILDREN, makeEmptyChildrenList());

        JSONObject component = new JSONObject();
        component.put(JSON_KEY_TYPE, JSON_TYPE_COMPONENT);
        component.put(ATTR_GUID, UUID.randomUUID()
            .toString());
        component.put(ATTR_EID, String.format(EID_TEMPLATE_COMPONENT, count));
        JSONArray componentChildren = new JSONArray();
        componentChildren.add(docRef);
        component.put(JSON_KEY_CHILDREN, componentChildren);

        children.add(component);
    }


    public static boolean hasContent(JSONObject jsonObject) {
        return jsonObject != null && !jsonObject.isEmpty();
    }


    public static String getLocalName(String elementName) {
        return elementName == null ?
            null :
            elementName.trim()
                .substring(elementName.indexOf(':') + 1);
    }


    private static JSONArray makeEmptyChildrenList() {
        JSONArray children = new JSONArray();
        children.add(makeNewTextNode(""));
        return children;
    }


    public static JSONObject makeNewTextNode(String text) {
        JSONArray wrapperChildren = new JSONArray();
        JSONObject textWrapper = new JSONObject();
        JSONObject textElement = new JSONObject();

        textElement.put(JSON_KEY_TEXT, text == null ? "" : text);
        wrapperChildren.add(textElement);
        textWrapper.put(JSON_KEY_TYPE, JSON_VAL_TEXT_WRAPPER);
        textWrapper.put(JSON_KEY_CHILDREN, wrapperChildren);
        return textWrapper;
    }


    public static JSONObject makeNewChangeWrapper(String text, String changeType) {
        JSONObject newTextNode = makeNewTextNode(text);
        newTextNode.put(JSON_KEY_TYPE, JSON_VAL_CHANGE_WRAPPER);
        addAttribute(newTextNode, Pair.of(CHANGE_TRACK_ATTR_TYPE, changeType));
        return newTextNode;
    }

    public static JSONObject makeNewCommentWrapper(String commentId, String commentPosition) {
        JSONArray wrapperChildren = new JSONArray();
        JSONObject commentWrapper = new JSONObject();

        wrapperChildren.add(makeNewCommentMarker(commentId, commentPosition));
        commentWrapper.put(JSON_KEY_TYPE, JSON_VAL_COMMENT_WRAPPER);
        commentWrapper.put(JSON_KEY_CHILDREN, wrapperChildren);
        return commentWrapper;
    }

    public static JSONObject makeNewCommentMarker(String commentId, String commentPosition) {
        JSONArray wrapperChildren = new JSONArray();
        JSONObject commentMarker = new JSONObject();
        JSONObject textElement = new JSONObject();

        textElement.put(JSON_KEY_TEXT, "");
        wrapperChildren.add(textElement);
        commentMarker.put(JSON_KEY_TYPE, JSON_VAL_COMMENT_MARKER);
        commentMarker.put(JSON_KEY_CHILDREN, wrapperChildren);
        commentMarker.put(JSON_KEY_COMMENT_ID, commentId);
        commentMarker.put(JSON_KEY_COMMENT_POSITION, commentPosition);
        return commentMarker;
    }


    public static void addText(JSONObject jsonObject, String text) {
        if (jsonObject != null) {
            String type = getType(jsonObject);
            if (type != null && (type.equals(JSON_VAL_TEXT_WRAPPER) || type.equals(JSON_VAL_CHANGE_WRAPPER))) {
                clearChildrenArray(jsonObject);
                JSONObject textNode = new JSONObject();
                textNode.put(JSON_KEY_TEXT, text);
                addChildren(jsonObject, textNode);
            } else {
                JSONArray children = (JSONArray) jsonObject.computeIfAbsent(JSON_KEY_CHILDREN, k -> new JSONArray());
                JSONObject textNode = makeNewTextNode(text);
                children.add(textNode);
            }
        }
    }

    public static void appendText(JSONObject jsonObject, String text) {
        String type = getType(jsonObject);
        if (!(jsonObject == null || type == null || text == null || type.equals(JSON_VAL_TEXT_WRAPPER))) {
            addText(jsonObject, text);
        }
    }

    public static void prependText(JSONObject jsonObject, String text) {
        String type = getType(jsonObject);
        if (!(jsonObject == null || type == null || text == null || type.equals(JSON_VAL_TEXT_WRAPPER))) {
            JSONArray children = getChildren(jsonObject, true);
            JSONObject textNode = makeNewTextNode(text);
            children.add(0, textNode);
        }
    }


    public static boolean isOneOf(String type, Set<String> typeNames) {
        if (type == null || type.isBlank() || typeNames == null || typeNames.isEmpty()) {
            return false;
        } else {
            return typeNames.contains(type.substring(type.indexOf(':') + 1));
        }
    }


    public static List<String> getListOfGUIDs(JSONArray jsonArray) {
        return getListOfGUIDs(jsonArray, 0);
    }


    public static List<String> getListOfGUIDs(JSONArray jsonArray, int startIndex) {
        return getListOfStringAttribute(jsonArray, startIndex, JSON_KEY_ID_GUID);
    }


    public static List<String> getListOfStringAttribute(JSONArray jsonArray, int startIndex, String key) {
        if (jsonArray == null || jsonArray.isEmpty()) {
            return List.of();
        } else {
            List<String> values = new ArrayList<>();
            for (int idx = startIndex; idx < jsonArray.size(); idx++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(idx);
                String value = getStringAttribute(jsonObject, key);
                if (value != null) {
                    values.add(value);
                }
            }
            return values;
        }
    }


    public static JSONArray getChildren(JSONObject jsonObject) {
        return getChildren(jsonObject, false);
    }


    public static JSONArray getChildren(JSONObject jsonObject, boolean createIfAbsent) {
        if (jsonObject == null) {
            return null;
        }

        if (createIfAbsent) {
            return (JSONArray) jsonObject.computeIfAbsent(JSON_KEY_CHILDREN, k -> new JSONArray());
        } else {
            return (JSONArray) jsonObject.get(JSON_KEY_CHILDREN);
        }
    }


    public static boolean hasInlines(JSONArray jsonArray) {
        if (jsonArray == null || jsonArray.isEmpty()) {
            return false;
        }

        return jsonArray.size() > 1 &&
            getListOfStringAttribute(jsonArray, 0, JSON_KEY_TYPE).contains(JSON_VAL_TEXT_WRAPPER);
    }


    public static void addChildren(JSONObject parent, JSONObject... children) {
        addChildren(parent, toJSONArray(Arrays.asList(children)));
    }

    public static void addChildren(JSONObject parent, JSONArray children) {
        if (parent == null || children == null) {
            return;
        }

        JSONArray jsonArray = getChildren(parent, true);
        jsonArray.addAll(children);
    }


    public static JSONObject getDocumentObject(String jsonString) {
        try {
            Object obj = JSON_PARSER.parse(jsonString);
            if (obj instanceof JSONArray) {
                JSONArray jsonArray = (JSONArray) obj;
                if (jsonArray.isEmpty()) {
                    return null;
                } else {
                    return (JSONObject) jsonArray.get(0);
                }
            } else {
                return (JSONObject) obj;
            }
        } catch (ParseException e) {
            log.error(e);
            return null;
        }
    }


    public static JSONObject makeSimpleTableObject(String caption, List<List<String>> data) {
        JSONObject table = makeNewDefaultJSONObject(ELEM_TABLE);

        if (!Utils.isMissing(caption)) {
            JSONObject captionObject = makeNewDefaultJSONObject(ELEM_CAPTION);
            captionObject.put(LEA_PREFIX + EGFA_ATTR_FROM_EGFA, true);
            addText(captionObject, caption);
            addChildren(table, captionObject);
        }

        if (data == null || data.isEmpty()) {
            JSONObject defaultRow = makeNewDefaultJSONObject(ELEM_TR);
            addText(defaultRow, "");
            addChildren(table, defaultRow);
        } else {
            List<JSONObject> rows = convertToRows(data);
            addChildren(table, rows.toArray(JSONObject[]::new));
        }

        return table;
    }


    private static List<JSONObject> convertToRows(@NotNull List<List<String>> data) {
        final List<JSONObject> rows = new ArrayList<>();
        final int maxColumns = data.stream()
            .mapToInt(List::size)
            .max()
            .orElse(0);

        AtomicInteger index = new AtomicInteger();
        data.forEach(rowData -> {
            int i = index.incrementAndGet();
            JSONObject row = makeNewDefaultJSONObject(ELEM_TR);
            List<String> rowDataPadded = Utils.pad(rowData, "", maxColumns);
            final List<JSONObject> cells = new ArrayList<>();

            rowDataPadded.forEach(cellData -> {
                JSONObject cell = makeNewDefaultJSONObject(i == 1 ? ELEM_TH : ELEM_TD);
                addCellContent(cell, cellData);
                cells.add(cell);
            });

            if (cells.isEmpty()) {
                cells.add(makeNewTextNode(""));
            }

            addChildren(row, cells.toArray(JSONObject[]::new));
            rows.add(row);
        });

        return rows;
    }


    private static void addCellContent(JSONObject cell, String cellData) {
        JSONObject p = makeNewDefaultJSONObject(ELEM_P);
        addText(p, cellData);
        addChildren(cell, p);
    }


    public static JSONObject makeNewJSONObject(String type, boolean prefixed, boolean withGUID) {
        JSONObject jsonObject = new JSONObject();

        if (!Utils.isMissing(type)) {
            setType(jsonObject, prefixed ? prefixed(type) : type);
        }

        if (withGUID) {
            setGUID(jsonObject);
        }

        return jsonObject;
    }


    public static JSONObject makeNewJSONObjectWithText(String type, String text, boolean prefixed, boolean withGUID) {
        JSONObject jsonObject = makeNewJSONObject(type, prefixed, withGUID);
        if (text != null) {
            addText(jsonObject, text);
        }
        return jsonObject;
    }


    public static JSONObject makeNewDefaultJSONObject(String type) {
        return makeNewJSONObject(type, true, true);
    }


    public static JSONObject makeNewDefaultJSONObjectWithText(String type, String text) {
        return makeNewJSONObjectWithText(type, text, true, true);
    }


    public static JSONObject getTextWrapper(String text) {
        JSONObject jsonObject = new JSONObject();
        setType(jsonObject, JSON_VAL_TEXT_WRAPPER);
        JSONObject textObject = new JSONObject();
        addAttribute(textObject, Pair.of(JSON_KEY_TEXT, text));
        addChildren(jsonObject, textObject);
        return jsonObject;
    }

    public static void setText(JSONObject jsonObject, String text) {
        if (text != null) {
            clearChildrenArray(jsonObject);
            addText(jsonObject, text);
        }
    }


    private static void setType(JSONObject jsonObject, String typeValue) {
        if (jsonObject != null && !Utils.isMissing(typeValue)) {
            jsonObject.put(JSON_KEY_TYPE, typeValue);
        }
    }


    private static void setGUID(JSONObject jsonObject) {
        if (jsonObject != null) {
            jsonObject.put(JSON_KEY_ID_GUID, UUID.randomUUID()
                .toString());
        }
    }


    private static String prefixed(String localname) {
        if (Utils.isMissing(localname) || localname.indexOf(':') >= 0) {
            return localname;
        } else {
            return "akn:" + localname;
        }
    }


    public static String getMetadata(JSONObject document, String kind) {
        JSONObject metadataField = getDescendant(document, false, "*", "akn:meta", "akn:proprietary",
            "meta:legalDocML.de_metadaten", kind);
        return getText(metadataField);
    }


    public static String getGuid(JSONObject jsonObject) {
        return getStringAttribute(jsonObject, JSON_KEY_ID_GUID);
    }

    public static String getRefGuid(JSONObject jsonObject) {
        return getStringAttribute(jsonObject, JSON_KEY_ID_REF_GUID);
    }

    public static void setGuid(JSONObject jsonObject, UUID guid) {
        addAttribute(jsonObject, Pair.of(JSON_KEY_ID_GUID, guid.toString()));
    }


    @SuppressWarnings("java:S109")
    public static JSONObject makeTBlock(@NotBlank String heading, List<String> paras,
        Pair<String, String>... attributes) {
        String headline = heading.trim();
        Matcher m = PTN_EGFA_HEADING.matcher(headline);
        String numbering = "";
        String header;

        JSONObject tblock = makeNewDefaultJSONObject(ELEM_TBLOCK);
        if (m.matches()) {
            numbering = getLastNumber(m.group(1)
                .trim());
            header = m.group(6)
                .trim();
        } else {
            header = headline;
        }

        Arrays.stream(attributes)
            .forEach(pair -> addAttribute(tblock, pair));
        JSONObject numObject = getNum(numbering);
        if (numObject != null) {
            addChildren(tblock, numObject);
        }
        JSONObject headingObject = getHeading(header);
        if (!m.matches()) {
            tblock.put(LEA_PREFIX + EGFA_ATTR_EDITABLE, true);
            headingObject.put(LEA_PREFIX + EGFA_ATTR_EDITABLE, true);
        }
        addChildren(tblock, headingObject);

        List<JSONObject> paraObjects = getTextParagraphs(paras);
        addChildren(tblock, paraObjects.toArray(JSONObject[]::new));

        return tblock;
    }


    public static void addAttributes(JSONObject jsonObject, Pair<String, String>... attributes) {
        for (Pair<String, String> pair : attributes) {
            addAttribute(jsonObject, pair);
        }
    }


    public static void addAttribute(JSONObject jsonObject, Pair<String, String> attribute) {
        String key = attribute.getKey();
        String val = attribute.getValue();

        if (jsonObject != null && !Utils.isMissing(key) && val != null) {
            jsonObject.put(key, val);
        }
    }


    public static List<JSONObject> getTextParagraphs(List<String> paras) {
        return paras.stream()
            .filter(s -> !Utils.isMissing(s))
            .map(String::trim)
            .map(JSONUtils::getTextParagraph)
            .collect(Collectors.toList());
    }


    private static JSONObject getTextParagraph(@NotBlank String text) {
        JSONObject jsonObject = makeNewDefaultJSONObject(ELEM_P);
        addText(jsonObject, text);
        return jsonObject;
    }


    private static JSONObject getNum(String num) {
        if (Utils.isMissing(num)) {
            return null;
        }

        JSONObject jsonObject = makeNewDefaultJSONObject(ELEM_NUM);
        JSONObject marker = makeNewJSONObject(ELEM_MARKER, true, false);
        addText(marker, "");

        String number = getNumberWithoutPunctuation(num);
        marker.put(ATTR_NAME, number);
        addChildren(jsonObject, marker);
        addText(jsonObject, num);
        return jsonObject;
    }


    public static JSONObject getHeading(String header) {
        JSONObject jsonObject = makeNewDefaultJSONObject(ELEM_HEADING);
        addText(jsonObject, header);
        return jsonObject;
    }


    private static String getLastNumber(String numbering) {
        String[] nums = numbering.split("\\s+");
        return nums[nums.length - 1];
    }


    private static String getNumberWithoutPunctuation(String num) {
        if (Utils.isMissing(num)) {
            return "";
        }

        String number = num.trim();
        Matcher m = PTN_PURE_NUM.matcher(number);
        if (m.matches()) {
            number = m.group(1)
                .toLowerCase();
        } else {
            log.error("Unexpected syntax of last number of heading");
        }

        return number;
    }


    @SuppressWarnings("unchecked")
    public static JSONArray wrapUp(JSONObject document) {
        JSONArray wrapper = new JSONArray();
        if (document != null) {
            wrapper.add(document);
        }
        return wrapper;
    }


    public static List<JSONArray> splitAfter(JSONArray jsonArray, String type) {
        if (jsonArray == null || Utils.isMissing(type)) {
            return List.of();
        } else if (jsonArray.isEmpty()) {
            return List.of(jsonArray);
        } else {
            List<JSONArray> parts = new ArrayList<>();
            JSONArray part = new JSONArray();
            Iterator<JSONObject> iterator = jsonArray.iterator();

            while (iterator.hasNext()) {
                JSONObject jsonObject = iterator.next();
                part.add(jsonObject);
                if (type.equals(getLocalName(getType(jsonObject))) || !iterator.hasNext()) {
                    parts.add(part);
                    part = new JSONArray();
                }
            }
            return parts;
        }
    }


    public static JSONArray toJSONArray(List<JSONObject> objects) {
        if (objects == null) {
            return null;
        } else if (objects.isEmpty()) {
            return new JSONArray();
        } else {
            JSONArray jsonArray = new JSONArray();
            jsonArray.addAll(objects);
            return jsonArray;
        }
    }


    public static List<JSONObject> fromJSONArray(JSONArray jsonArray) {
        if (jsonArray == null) {
            return null;
        } else if (jsonArray.isEmpty()) {
            return new ArrayList<>();
        } else {
            return new ArrayList<>(jsonArray);
        }
    }


    public static JSONArray clearChildrenArray(JSONObject jsonObject) {
        if (jsonObject == null || jsonObject.isEmpty()) {
            return new JSONArray();
        }

        JSONArray children = getChildren(jsonObject, true);
        replaceChildren(jsonObject, new JSONArray());

        return children;
    }

    public static void replaceChildren(@NonNull JSONObject jsonObject, @NonNull JSONArray newChildren) {
        jsonObject.put(JSON_KEY_CHILDREN, newChildren);
    }

    // Arrays.asList never returns null!
    @SuppressWarnings("java:S2637")
    public static void replaceChildren(@NonNull JSONObject jsonObject, @NonNull JSONObject... children) {
        replaceChildren(jsonObject, toJSONArray(Arrays.asList(children)));
    }

    public static boolean hasNoChildren(JSONObject jsonObject) {
        if (jsonObject == null || jsonObject.isEmpty()) {
            return true;
        } else {
            JSONArray children = getChildren(jsonObject);
            return children == null || children.isEmpty();
        }
    }


    private static JSONObject getChildElementOfType(JSONObject jsonObject, String type) {
        if (jsonObject == null || jsonObject.isEmpty() || isMissing(type)) {
            return null;
        }

        JSONArray children = getChildren(jsonObject);
        if (children == null) {
            return null;
        }

        JSONObject child = null;
        Iterator<JSONObject> childIterator = children.iterator();
        while (childIterator.hasNext() && child == null) {
            JSONObject candidate = childIterator.next();
            String candidateType = getType(candidate);
            if (candidateType != null && getLocalName(candidateType).equals(getLocalName(type))) {
                child = candidate;
            }
        }
        return child;
    }


    public static JSONObject getNumElement(JSONObject jsonObject) {
        return getChildElementOfType(jsonObject, ELEM_NUM);
    }


    public static JSONObject getHeadingOfElement(JSONObject jsonObject) {
        return getChildElementOfType(jsonObject, ELEM_HEADING);
    }


    public static boolean isSubstitute(JSONObject jsonObject) {
        return Boolean.parseBoolean(getStringAttribute(jsonObject, IS_SUBSTITUTE));
    }


    public static boolean isElementOfType(JSONObject jsonObject, String type) {
        if (jsonObject == null || jsonObject.isEmpty() || getType(jsonObject) == null || isMissing(type)) {
            return false;
        } else {
            String objectType = getType(jsonObject);
            return objectType != null && getLocalName(objectType).equals(getLocalName(type));
        }
    }


    public static boolean isLegalNorm(JSONObject jsonObject) {
        return isElementOfType(jsonObject, ELEM_ARTICLE);
    }


    public static boolean isLegalParagraph(JSONObject jsonObject) {
        return isElementOfType(jsonObject, ELEM_PARAGRAPH);
    }


    public static List<JSONObject> getParas(JSONObject jsonObject) {
        if (!isLegalNorm(jsonObject)) {
            return List.of();
        }

        JSONArray children = getChildren(jsonObject);
        if (children == null || children.isEmpty() || hasOnlyText(jsonObject)) {
            return List.of();
        }

        return (List<JSONObject>) children.stream()
            .filter(c -> isLegalParagraph((JSONObject) c))
            .collect(Collectors.toList());
    }


    public static String withDefaultPrefix(String name) {
        return String.format("akn:%s", getLocalName(name));
    }


    public static JSONObject getNthChild(JSONObject parent, int index) {
        if (parent == null || parent.isEmpty() || index < 0) {
            return null;
        }

        JSONArray children = getChildren(parent);
        if (index >= children.size()) {
            return null;
        }

        return (JSONObject) children.get(index);
    }


    public static <T> boolean isEffectivelyEmpty(Collection<T> collection) {
        return collection == null || collection.isEmpty();
    }


    public static boolean isEffectivelyEmpty(JSONObject jsonObject) {
        return jsonObject == null || jsonObject.isEmpty();
    }


    public static List<JSONObject> findByEId(JSONObject jsonObject, String... eIds) {
        if (isEffectivelyEmpty(jsonObject)) {
            return List.of();
        } else {
            HashSet<String> ids = Arrays.stream(eIds).collect(Collectors.toCollection(HashSet::new));
            List<JSONObject> foundObjects = new ArrayList<>();
            findByEId(jsonObject, ids, foundObjects);
            return foundObjects;
        }
    }


    private static void findByEId(JSONObject jsonObject, HashSet<String> eIds, List<JSONObject> foundObjects) {
        String currentEId = getStringAttribute(jsonObject, ATTR_EID);
        if (eIds.contains(currentEId)) {
            foundObjects.add(jsonObject);
        }

        JSONArray children = getChildren(jsonObject);
        if (!isEffectivelyEmpty(children)) {
            children.forEach(obj -> findByEId((JSONObject) obj, eIds, foundObjects));
        }
    }


    @SuppressWarnings({"java:S3776", "java:S134"})
    public static int getIndex(JSONObject parent, JSONObject child) {
        JSONArray children = getChildren(parent);
        if (children == null || child == null) {
            return -1;
        } else {
            String childGuid = getGuid(child);
            for (int i = 0; i < children.size(); i++) {
                if (childGuid == null) {
                    if (child.equals(children.get(i))) {
                        return i;
                    }
                } else {
                    if (childGuid.equals(getGuid((JSONObject) children.get(i)))) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }


    public static boolean areSameObjects(JSONObject firstObject, JSONObject secondObject, JSONObject... furtherObjects) {
        List<JSONObject> objectList = new ArrayList<>(Arrays.asList(furtherObjects));
        objectList.add(firstObject);
        objectList.add(secondObject);

        if (areAllNull(objectList)) {
            return true;
        }

        HashSet<String> strings = new HashSet<>();
        objectList.forEach(obj -> strings.add(obj == null ? "" : obj.toJSONString()));

        return strings.size() == 1;
    }


    public static JSONObject cloneJSONObject(JSONObject original) {
        if (original == null) {
            return null;
        } else {
            JSONObject clone = new JSONObject();

            original.forEach((key, value) -> {
                if (value instanceof JSONObject) {
                    clone.put(key, cloneJSONObject((JSONObject) value));
                } else if (value instanceof JSONArray) {
                    JSONArray originalArray = (JSONArray) value;
                    JSONArray clonedArray = new JSONArray();
                    originalArray.forEach(obj -> {
                        if (obj instanceof JSONObject) {
                            clonedArray.add(cloneJSONObject((JSONObject) obj));
                        } else {
                            clonedArray.add(obj);
                        }
                    });
                    clone.put(key, clonedArray);
                } else {
                    clone.put(key, value);
                }
            });

            return clone;
        }
    }


    public static JSONObject getDummy() {
        return makeNewDefaultJSONObject("DUMMY");
    }


    public static JSONObject makeNumObject(String marker, String name) {
        JSONObject numObject = makeNewDefaultJSONObject(ELEM_NUM);
        JSONObject markerObject = makeNewJSONObject(ELEM_MARKER, true, false);
        addAttribute(markerObject, Pair.of(ATTR_NAME, marker));
        setText(markerObject, "");
        JSONObject markerText = getTextWrapper(name);
        addChildren(numObject, markerObject, markerText);
        return numObject;
    }

    public static JSONObject getElementByGUID(JSONObject object, String guid) {
        JSONObject ret = null;
        if (object.containsKey(ATTR_GUID) && getGuid(object).equals(guid)) {
            return object;
        }
        JSONArray children = getChildren(object);
        if (children != null && !children.isEmpty()) {
            for (Object obj : children) {
                JSONObject jsonChild = (JSONObject) obj;
                ret = getElementByGUID(jsonChild, guid);
                if (ret != null) {
                    return ret;
                }
            }
        }
        return ret;
    }


    public static QName getQName(JSONObject jsonObject) {
        if (isEffectivelyEmpty(jsonObject)) {
            return null;
        }

        String type = getType(jsonObject);
        int pos = type.indexOf(':');

        if (pos < 0) {
            return new QName(type);
        } else {
            String[] parts = type.split(":");
            assert parts.length == 2; //NOSONAR
            String prefix = parts[0];
            String localName = parts[1];
            String nsUri = TARGET_NS;

            if (!"akn".equals(prefix)) {
                String xmlns = getStringAttribute(jsonObject, "xmlns:" + prefix);
                nsUri = Utils.isMissing(xmlns) ? XMLConstants.NULL_NS_URI : xmlns;
            }

            return new QName(nsUri, localName, prefix);
        }
    }

}
