// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.numbering;

public interface NumberingScheme {

    String getPrefix(int depth);

    String getSuffix(int depth);

    String getType(int depth);

    String getSymbol(int index, int depth, boolean ordered);

}
