// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
@Log4j2
public abstract class ObjectListComparer<T, K> {

    // see org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch.diffLinesToCharsMunge
    // ignore first 32 non-printable characters
    private static final int SIZE_LIMIT = (int) Math.pow(2, 16) - 32;

    private static final String ERR_MSG_INDEX_OUT_OF_BOUNDS = "number of unique keys is greater than allowed value";

    private LinkedList<ObjectListDiff<K>> keyDiffs;

    public Map<K, DiffMatchPatch.Operation> compare(List<T> lefts, List<T> rights) {
        List<K> keys = new ArrayList<>();
        Map<K, Integer> cache = new HashMap<>();

        // we do not want to use index=0 as character; for details see
        // org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch.diffLinesToChars
        keys.add(null);

        List<K> leftKeys = lefts.stream().map(this::getKey).collect(Collectors.toList());
        List<K> rightKeys = rights.stream().map(this::getKey).collect(Collectors.toList());
        String text1 = mapKeysToString(leftKeys, keys, cache);
        String text2 = mapKeysToString(rightKeys, keys, cache);

        DiffMatchPatch dmp = new DiffMatchPatch();
        LinkedList<DiffMatchPatch.Diff> diffs = dmp.diffMain(text1, text2, false);
        keyDiffs = getDiffsForKeys(diffs, keys);

        Map<K, DiffMatchPatch.Operation> resultMap = new HashMap<>();
        keyDiffs.forEach(d -> resultMap.put(d.getKey(), d.getOperation()));
        return resultMap;
    }

    private String mapKeysToString(List<K> keysRepresentingObjects, List<K> uniqueKeys, Map<K, Integer> keyCache) throws IndexOutOfBoundsException {
        StringBuilder sb = new StringBuilder();

        keysRepresentingObjects.forEach(key -> {
            if (keyCache.containsKey(key)) {
                sb.append((char) (int) keyCache.get(key));
            } else {
                if (uniqueKeys.size() == SIZE_LIMIT) {
                    throw new IndexOutOfBoundsException(ERR_MSG_INDEX_OUT_OF_BOUNDS);
                }
                uniqueKeys.add(key);
                keyCache.put(key, uniqueKeys.size() - 1);
                sb.append((char) (uniqueKeys.size() - 1));
            }
        });

        return sb.toString();
    }

    private LinkedList<ObjectListDiff<K>> getDiffsForKeys(LinkedList<DiffMatchPatch.Diff> charDiffs, List<K> uniqueKeys) {
        LinkedList<ObjectListDiff<K>> result = new LinkedList<>();
        charDiffs.forEach(diff -> diff.text.chars().forEach(i -> {
            K key = uniqueKeys.get(i);
            ObjectListDiff<K> newDiff = new ObjectListDiff<>(diff.operation, key);
            result.add(newDiff);
        }));
        return result;
    }

    public abstract K getKey(T object);

}
