// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;

import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.LinkedList;

/**
 * The central class of the compare tool that executes the comparison of two documents. Supported formats for document contents are JSON and XML and those can
 * be mixed. For that purpose a neutral text oriented format is used to represent the contents.
 */

@Getter
@Log4j2
public class DocumentComparer {

    private LinkedList<DiffMatchPatch.Diff> diffs;


    /**
     * This methos executes the comparison. Some document content of a certain type is compared with the content of another document with the same or different
     * type.
     *
     * @param typeA    type of first document
     * @param contentA content of first document
     * @param typeB    type of second document
     * @param contentB content of second document
     * @param format   the format alternative to represent the neutral, textual intermediate format
     * @return success or failure of comparison
     */
    public CompareResult compare(DocumentType typeA, InputStream contentA, DocumentType typeB, InputStream contentB,
        IntermediateFormat format) {
        StringWriter writerA = new StringWriter();
        readDocument(typeA, contentA, writerA, format);
        log.info("reading first {} content", typeA == DocumentType.JSON_DOCUMENT ? "JSON" : "XML");

        StringWriter writerB = new StringWriter();
        readDocument(typeB, contentB, writerB, format);
        log.info("reading second {} content", typeB == DocumentType.JSON_DOCUMENT ? "JSON" : "XML");

        DiffMatchPatch dmp = new DiffMatchPatch();
        LinkedList<DiffMatchPatch.Diff> diffsTmp = dmp.diffMain(writerA.toString(), writerB.toString(), true);
        dmp.diffCleanupSemantic(diffsTmp);
        log.debug(diffsTmp);
        LinkedList<DiffMatchPatch.Diff> cleanedDiffs = excludeEquals(diffsTmp);

        this.diffs = diffsTmp;

        if (cleanedDiffs == null || cleanedDiffs.isEmpty()) {
            log.info("both documents are equal");
            return CompareResult.EQUAL;
        } else {
			log.info("there are differences :: Levenshtein distance={}", dmp.diffLevenshtein(this.diffs));
			log.debug("content A:\n{}", writerA);
			log.debug("content B:\n{}", writerB);
            return CompareResult.DIFFERENT;
        }
    }


    private void readDocument(DocumentType type, InputStream content, Writer writer, IntermediateFormat format) {
        DocumentReader reader = type == DocumentType.JSON_DOCUMENT
            ? new JsonDocumentReader()
            : new XmlDocumentReader();
        DocumentDataSet ds = reader.readDocument(content);
        DocumentDataSetWriter dataSetWriter = format == IntermediateFormat.FIXED_FORMAT
            ? new FixedFormatDocumentDataSetWriter(writer)
            : new DSVStyleDocumentDataSetWriter(writer);
        dataSetWriter.write(ds);
    }


    private LinkedList<DiffMatchPatch.Diff> excludeEquals(LinkedList<DiffMatchPatch.Diff> diffs) {
        if (diffs == null || diffs.isEmpty()) {
            return diffs;
        } else {
            LinkedList<DiffMatchPatch.Diff> cleanedDiffs = new LinkedList<>();
            for (DiffMatchPatch.Diff diff : diffs) {
                if (diff.operation != DiffMatchPatch.Operation.EQUAL) {
                    cleanedDiffs.add(diff);
                    log.debug(diff);
                }
            }

            return cleanedDiffs;
        }
    }


    public enum DocumentType {
        JSON_DOCUMENT,
        XML_DOCUMENT
    }

    public enum IntermediateFormat {
        DSV_STYLE,
        FIXED_FORMAT
    }

    public enum CompareResult {
        EQUAL,
        DIFFERENT
    }

}
