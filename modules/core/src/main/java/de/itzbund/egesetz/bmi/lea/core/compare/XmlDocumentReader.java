// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import lombok.extern.log4j.Log4j2;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static de.itzbund.egesetz.bmi.lea.core.util.Utils.getDocumentBuilderFactory;

/**
 * A concrete reader to transform XML documents (for the editor of eGG) into in-memory objects.
 */

@Log4j2
public class XmlDocumentReader extends DocumentReader {

    private static final Pattern PATT_QNAME = Pattern.compile("(\\w+:)?(\\w+)");


    /**
     * {@inheritDoc}
     *
     * @param inputStream the content of the original document
     * @return a document dataset
     */
    @Override
    public DocumentDataSet readDocument(InputStream inputStream) {
        DocumentDataSet ds = new DocumentDataSet();
        Element docElement = getDocumentNode(inputStream);
        counter = new Counter();

        if (docElement != null) {
            makeDocumentDataItem(docElement, ds);
        }

        return ds;
    }


    private void makeDocumentDataItem(Element xmlElement, DocumentDataSet ds) {
        String name = getName(xmlElement);
        List<String> attributes = getAttributes(xmlElement);
        ElementTypeDataItem ddi = ElementTypeDataItem.builder()
            .count(1)
            .name(name)
            .isElement(false)
            .attributes(attributes.toArray(new String[0]))
            .build();
        ds.addDataItem(ddi);
        traverseChildren(xmlElement.getChildNodes(), ds, ddi.getId());
    }


    private void makeElementTypeDataItem(Element xmlElement, DocumentDataSet ds, String parentId) {
        String name = getName(xmlElement);
        List<String> attributes = getAttributes(xmlElement);
        ElementTypeDataItem edi = ElementTypeDataItem.builder()
            .count(getElemCount())
            .name(name)
            .isElement(true)
            .parentId(parentId)
            .attributes(attributes.toArray(new String[0]))
            .build();
        ds.addDataItem(edi);
        traverseChildren(xmlElement.getChildNodes(), ds, edi.getId());
    }


    private void makeTextTypeDataItem(Text xmlText, DocumentDataSet ds, String parentId) {
        String content = xmlText.getWholeText();
        if (content == null) {
            content = "";
        } else if (content.isBlank()) {
            return;
        }
        ds.addDataItem(TextTypeDataItem.builder()
            .count(getTextCount())
            .parentId(parentId)
            .content(content.replaceAll("\\s+", " "))
            .build());
    }


    private void traverseChildren(NodeList children, DocumentDataSet ds, String parentId) {
        if (children != null) {
            for (int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);

                if (isTextNode(child)) {
                    incTextCount();
                    makeTextTypeDataItem((Text) child, ds, parentId);
                } else if (isElementNode(child)) {
                    incElemCount();
                    makeElementTypeDataItem((Element) child, ds, parentId);
                }
            }
        }
    }


    private List<String> getAttributes(Element element) {
        List<String> attributes = new ArrayList<>();
        NamedNodeMap attrs = element.getAttributes();

        for (int i = 0; i < attrs.getLength(); i++) {
            Node attr = attrs.item(i);
            attributes.add(attr.getNodeName());
            attributes.add(attr.getNodeValue());
        }

        return attributes;
    }


    private String getName(Element element) {
        String fqn = element.getNodeName();
        Matcher m = PATT_QNAME.matcher(fqn);
        if (m.matches()) {
            return m.group(2);//NOSONAR
        } else {
            log.error("invalid XML element name: " + fqn);
            return null;
        }
    }


    private Element getDocumentNode(InputStream inputStream) {
        try {
            DocumentBuilder db = getDocumentBuilderFactory().newDocumentBuilder();
            Document doc = db.parse(inputStream);
            Element root = doc.getDocumentElement();
            NodeList children = root.getChildNodes();
            Element documentNode = null;

            for (int i = 0; i < children.getLength(); i++) {
                Node node = children.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE && documentNode == null) {
                    documentNode = (Element) node;
                }
            }

            return documentNode;
        } catch (ParserConfigurationException | IOException | SAXException e) {
            log.error(e);
        }

        return null;
    }


    private boolean isTextNode(Node node) {
        return node.getNodeType() == Node.TEXT_NODE && !((Text) node).getWholeText().isBlank();
    }


    private boolean isElementNode(Node node) {
        return node.getNodeType() == Node.ELEMENT_NODE;
    }

}
