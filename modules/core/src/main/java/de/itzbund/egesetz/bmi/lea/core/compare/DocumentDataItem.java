// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import lombok.Getter;

/**
 * An abstract POJO class that describes a single piece of content of a document. Extensions make concrete models of a piece of data.
 */
@Getter
public class DocumentDataItem {

    protected ItemType itemType;
    protected String id;
    protected String parentId;


    DocumentDataItem() {
    }


    /**
     * Generates an ID for this data item.
     *
     * @param type  the type of the data item
     * @param count some number produced by an application to identify this item within the set of its type
     */
    protected void makeId(ItemType type, int count) {
        id = String.format("%1$s%2$08X", getTypeChar(type), count);
    }


    private char getTypeChar(ItemType type) {
        return type.toString().charAt(0);
    }


    public enum ItemType {
        DOCUMENT,
        ELEMENT,
        TEXT
    }

}
