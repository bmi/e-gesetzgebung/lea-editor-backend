// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core;

import org.apache.commons.lang3.tuple.Pair;
import org.json.simple.JSONObject;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * A navigable document is a wrapper around a JSON tree that permits arbitrary navigation.</br> Preconditions: Objects are identified by GUID (as String value)
 * and all children are within an array with key 'children'.</br> There is an automatic context element which is the origin of navigation along some axis. This
 * element can be changed. Navigation is also possible with a different origin, which is then given as 'context' parameter.
 * </br>
 * Usually the relative order of elements is retained concerning document order, that is the order in which elements would appear on printed media; i.e.
 * depth-first or pre-order.
 */
@SuppressWarnings("unused")
public interface NavigableDocument {

    JSONObject getDocumentObject();

    /**************************************************************************
     * Axis: self
     **************************************************************************/

    JSONObject getCurrentObject();

    void setCurrentObject(JSONObject jsonObject);

    JSONObject getObject(String guid);

    void advanceCurrentObject() throws NoSuchElementException;

    /**************************************************************************
     * Axes: child, parent
     * Selects all children of the parent of the origin node. The list of children of leave nodes are empty. The
     * parent of the root node (document objects) is <code>null</code>.
     **************************************************************************/

    List<JSONObject> getChildren();

    List<JSONObject> getChildren(JSONObject context);

    JSONObject getChild(String guid);

    JSONObject getChild(JSONObject context, String guid);

    JSONObject getChild(int index);

    JSONObject getChild(JSONObject context, int index);

    JSONObject getParent();

    JSONObject getParent(JSONObject context);

    /**************************************************************************
     * Axes: ancestor, ancestor-or-self</br>
     * Selects all the nodes that are ancestors of the origin node in reverse document order, i.e. the first node is
     * the parent of the origin, the second its grandparent and so on until at last the root object is reached. When
     * including the origin then this is prepended to the described list.
     **************************************************************************/

    List<JSONObject> getAncestors(boolean includeSelf);

    List<JSONObject> getAncestors(JSONObject context, boolean includeSelf);

    /**************************************************************************
     * Axes: descendant, descendant-or-self</br>
     * Selects all children of the origin node , and their children and so on recursively in document order. When
     * including the origin then this prepended to the described list.
     **************************************************************************/

    List<JSONObject> getDescendants(boolean includeSelf);

    List<JSONObject> getDescendants(JSONObject context, boolean includeSelf);

    /**************************************************************************
     * Axes: following, following-sibling</br>
     * Selects all the nodes that appear after the origin node in document order, excluding the descendants of the
     * origin. Asking for siblings only includes nodes that have the same parent as the origin.
     **************************************************************************/

    List<JSONObject> getFollowingObjects();

    List<JSONObject> getFollowingObjects(JSONObject context);

    List<JSONObject> getFollowingSiblings();

    List<JSONObject> getFollowingSiblings(JSONObject context);

    /**************************************************************************
     * Axes: preceding, preceding-sibling</br>
     * Selects all the nodes that appear before the origin node in reverse document order, excluding the ancestors of
     * the origin. Asking for siblings only includes nodes that have the same parent as the origin.
     **************************************************************************/

    List<JSONObject> getPrecedingObjects();

    List<JSONObject> getPrecedingObjects(JSONObject context);

    List<JSONObject> getPrecedingSiblings();

    List<JSONObject> getPrecedingSiblings(JSONObject context);

    /**************************************************************************
     * Axis: attribute
     **************************************************************************/

    Set<Pair<String, String>> getAttributes();

    Set<Pair<String, String>> getAttributes(JSONObject context);

    String getAttribute(String key);

    String getAttribute(JSONObject context, String key);

    /**************************************************************************
     * Leave objects</br>
     * These are objects that contain the textual content, like textual paragraphs or headings, or are handled as
     * unit, like lists or tables.
     **************************************************************************/

    List<JSONObject> getAllLeaveObjects();

    List<JSONObject> getLeaveObjects();

    List<JSONObject> getLeaveObjects(JSONObject context);

    void advanceToNextLeave() throws NoSuchElementException;

    boolean isLeaveObject();

    boolean isLeaveObject(JSONObject context);

    JSONObject getRoot();

}
