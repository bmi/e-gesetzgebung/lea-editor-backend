// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import de.itzbund.egesetz.bmi.lea.core.ChangeMarker;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.HashMap;

@Getter
@RequiredArgsConstructor
public enum TextChangeMarker implements ChangeMarker {

    ERASED("cp_erased", "del", "color: red"),
    INSERTED("cp_inserted", "ins", "color: green"),
    UNCHANGED("cp_unchanged", "span", "");

    static final HashMap<String, TextChangeMarker> literals;

    static {
        literals = new HashMap<>();
        Arrays.stream(values()).forEach(v -> literals.put(v.getType(), v));
    }

    final String type;
    final String htmlTag;
    final String cssStyle;

    public static TextChangeMarker fromLiteral(String literal) {
        return Utils.isMissing(literal) ? UNCHANGED : literals.get(literal);
    }

}
