// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core;

import org.json.simple.JSONObject;

/**
 * Provides operations to make changes to a document.
 */
public interface UpdatableDocument {

    /**
     * Replace original content by substitute.
     */
    void replaceBy(JSONObject original, JSONObject substitute);

    /**
     * Insert new content as child object into parent at specified position. If index is less than or equal to 0, then the new element becomes the new first
     * child. If index is equal to the children count or greater, then the new element becomes the new last child. In all other cases the new element is
     * inserted at the named position (see also {@link java.util.ArrayList#add(int, Object)}).
     *
     * @param parent the parent object receiving the new child content
     * @param child  the new child
     * @param index  the position where to put the new content in the children list of parent
     */
    void insertNewChild(JSONObject parent, JSONObject child, int index);

}
