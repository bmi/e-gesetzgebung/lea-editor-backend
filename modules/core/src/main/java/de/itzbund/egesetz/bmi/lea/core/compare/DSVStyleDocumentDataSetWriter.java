// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.Map;

/**
 * A writer for a text format in the DSV style. See
 * <a href="http://www.catb.org/~esr/writings/taoup/html/ch05s02.html">Data File Metaformats</a> or
 * <a href="https://en.wikipedia.org/wiki/Delimiter-separated_values">Delimiter-separated values</a>
 * . The delimiter can be chosen from a given set of alternatives. The semicolon ';' is the default.
 */
@Log4j2
public class DSVStyleDocumentDataSetWriter extends DocumentDataSetWriter {

    private Delimiter delimiter = Delimiter.SEMICOLON;


    public DSVStyleDocumentDataSetWriter(Writer writer) {
        this.writer = writer;
    }


    public DSVStyleDocumentDataSetWriter(Writer writer, Delimiter delimiter) {
        this.writer = writer;
        this.delimiter = delimiter;
    }


    /**
     * Writes out the document dataset.
     *
     * @param dataSet A dataset of document data items
     */
    @Override
    public void write(DocumentDataSet dataSet) {
        for (DocumentDataItem dataItem : dataSet.getItems()) {
            writeLine(dataItem);
        }
    }


    /**
     * {@inheritDoc}
     *
     * @param dataItem a data item that represents an element
     */
    @Override
    public void writeLine(ElementTypeDataItem dataItem) {
        String parentId = dataItem.getParentId();
        String prefix;

        if (parentId == null) {
            prefix = String.format("%s:%s%s",
                DocumentDataItem.ItemType.DOCUMENT.name()
                    .toLowerCase(),
                dataItem.getId(),
                delimiter.getDelim());
        } else {
            prefix = String.format("%s:%s%sparent:%s%s",
                DocumentDataItem.ItemType.ELEMENT.name()
                    .toLowerCase(),
                dataItem.getId(),
                delimiter.getDelim(),
                parentId,
                delimiter.getDelim());
        }

        try {
            writer.write(String.format("%stag:%s%sattributes:{",
                prefix,
                dataItem.getName(),
                delimiter.getDelim()));
            writeAttributes(dataItem);
            writer.write("}\n");
        } catch (IOException e) {
            log.error(e);
        }
    }


    @SneakyThrows
    private void writeAttributes(ElementTypeDataItem dataItem) {
        for (Iterator<Map.Entry<String, String>> it = dataItem.getAttributes()
            .entrySet()
            .iterator(); it.hasNext(); ) {
            Map.Entry<String, String> entry = it.next();
            writer.write(String.format("@%s='%s'", entry.getKey(), entry.getValue()));

            if (it.hasNext()) {
                writer.write(",");
            }
        }
    }


    /**
     * {@inheritDoc}
     *
     * @param dataItem a data items that represents text content
     */
    @Override
    public void writeLine(TextTypeDataItem dataItem) {
        try {
            writer.write(String.format("%s:%s%sparent:%s%scontent:'%s'",
                dataItem.getItemType()
                    .name()
                    .toLowerCase(),
                dataItem.getId(),
                delimiter.getDelim(),
                dataItem.getParentId(),
                delimiter.getDelim(),
                WhitespaceSubstitution.getInstance()
                    .convert(dataItem.getContent())));
            writer.write('\n');
        } catch (IOException e) {
            log.error(e);
        }
    }


    @Getter
    public enum Delimiter {
        COLON(':'),
        COMMA(','),
        SEMICOLON(';'),
        SPACE(' '),
        TAB('\t'),
        TILDE('~'),
        VERTICAL_BAR('|');

        private final char delim;


        Delimiter(char delim) {
            this.delim = delim;
        }

    }

}
