// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import lombok.Getter;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static de.itzbund.egesetz.bmi.lea.core.compare.WhitespaceSubstitutionMap.Substitution.CARRIAGE_RETURN;
import static de.itzbund.egesetz.bmi.lea.core.compare.WhitespaceSubstitutionMap.Substitution.LINE_BREAK;
import static de.itzbund.egesetz.bmi.lea.core.compare.WhitespaceSubstitutionMap.Substitution.NB_SPACE;
import static de.itzbund.egesetz.bmi.lea.core.compare.WhitespaceSubstitutionMap.Substitution.SPACE;
import static de.itzbund.egesetz.bmi.lea.core.compare.WhitespaceSubstitutionMap.Substitution.TAB;

/**
 * A configurable map to map whitespace characters to visible alternatives.
 */
public class WhitespaceSubstitutionMap {

    private static final List<Substitution> substitutions =
        Arrays.asList(LINE_BREAK, CARRIAGE_RETURN, TAB, NB_SPACE, SPACE);


    public static Iterator<Substitution> getIterator() {
        return substitutions.iterator();
    }


    @Getter
    public enum Substitution {
        CARRIAGE_RETURN("\\r", "↤"),
        LINE_BREAK("\\r?\\n", "↵"),
        NB_SPACE("\u00A0", "☐"),
        SPACE(" ", "␣"),
        TAB("\\t", "⭾");

        private final String regex;
        private final String substitute;


        Substitution(String regex, String substitute) {
            this.regex = regex;
            this.substitute = substitute;
        }
    }

}
