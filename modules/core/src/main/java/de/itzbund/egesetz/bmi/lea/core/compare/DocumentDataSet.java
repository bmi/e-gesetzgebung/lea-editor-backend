// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core.compare;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * A POJO class collection all data items of a document, i.e. its a neutral representation of a structured document.
 */
@Getter
public class DocumentDataSet {

    private final List<DocumentDataItem> items = new ArrayList<>();


    public void addDataItem(DocumentDataItem dataItem) {
        items.add(dataItem);
    }

}
