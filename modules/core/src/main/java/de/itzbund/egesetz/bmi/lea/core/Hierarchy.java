// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.core;

import java.util.List;

public interface Hierarchy<K, E> {

    boolean isHierarchyLevel(E e);

    boolean isLegalNorm(E e);

    /**
     * Returns the list of hierarchy level objects in document order for a given element. Remaining levels after the last element are accessible with value
     * 'null' as key.
     *
     * @param key ID of an element
     * @return list of hierarchical levels immediately before the element
     */
    List<E> getUnprocessedLevels(K key);

}
