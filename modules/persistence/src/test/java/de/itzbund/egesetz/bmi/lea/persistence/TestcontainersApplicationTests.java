// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.junit.jupiter.Testcontainers;

// we have no @SpringBootConfiguration, so we use @SpringBootApplication
@SpringBootApplication
// @DataJpaTest is annotated by @AutoConfigureTestDatabase itself which
// replaces any data source with H2, so we have to override this behavior
// @DataJpaTest
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@Testcontainers(disabledWithoutDocker = true)
@ActiveProfiles("test-containers")
@Disabled("Not now")
class TestcontainersApplicationTests extends AbstractIntegrationTest {

    @SuppressWarnings("java:S2699")
    @Test
    void migrate() {
        // migration starts automatically,
        // since Spring Boot runs the Flyway scripts on startup
    }

}
