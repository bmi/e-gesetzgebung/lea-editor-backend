// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.enums.egfa.datenuebernahme.EgfaModuleType;
import de.itzbund.egesetz.bmi.lea.domain.model.EgfaData;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.persistence.UserRepository;
import de.itzbund.egesetz.bmi.lea.persistence.entities.EgfaDataEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.BaseMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.EgfaDataMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.EgfaDataMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.EgfaDataRepository;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestEntitiesUtil;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!! In H2 MEMORY MODE, the scripts schema.sql and data.sql !!!!!!!!!!!!
// !!!!!!!!!! are automatically applied !!!                          !!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@ExtendWith(MockitoExtension.class)
@SuppressWarnings("unused")
class EgfaDataPersistenceAdapterTest {

	@Spy
	private final EgfaDataMapperPersistence egfaDataMapper = new EgfaDataMapperPersistenceImpl(new BaseMapperPersistenceImpl());
	@Spy
	private final UserMapperPersistence userMapper = new UserMapperPersistenceImpl();

	@InjectMocks
	EgfaDataPersistenceAdapter egfaDataPersistenceAdapter;

	@Mock
	private EgfaDataRepository egfaDataRepository;
	@Mock
	private UserRepository userRepository;

	private static final String REGELUNGSVORHABEN_ID_FROM_DATABASE_SCRIPT = "00000000-0000-0000-0000-000000000005";
	private static final RegelungsVorhabenId REGELUNGSVORHABEN_ID = new RegelungsVorhabenId(UUID.fromString(REGELUNGSVORHABEN_ID_FROM_DATABASE_SCRIPT));

	private EgfaData egfaData;
	private EgfaDataEntity egfaDataEntity;
	private User user;
	private NutzerEntity userEntity;

	@BeforeEach
	void init() {
		user = TestObjectsUtil.getUserEntity();
		userEntity = TestEntitiesUtil.getRandomNutzerEntity();

		egfaData = TestObjectsUtil.getEgfaData();
		egfaData.setTransmittedByUser(user);
		egfaDataEntity = egfaDataMapper.fromDomainItem(egfaData);
	}

	@Test
	void testFindNewestByRegelungsVorhabenIdAndModuleName() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();
		when(egfaDataRepository.findTopByRegelungsVorhabenIdAndModuleNameOrderByCompletedAtDesc(any(), any())).thenReturn(
			Optional.of(TestEntitiesUtil.getEgfaEntity()));

		when(userRepository.findFirstByGid(user.getGid())).thenReturn(userEntity);

		// Act
		EgfaData actual = egfaDataPersistenceAdapter.findNewestByRegelungsVorhabenIdAndModuleName(REGELUNGSVORHABEN_ID, EgfaModuleType.SONSTIGE_KOSTEN);

		// Assert
		assertThat(actual).isNotNull();
	}

	@Test
	void testFindNewestByRegelungsVorhabenIdAndModuleNameIfEmpty() {
		List<ServiceState> returnStatus = new ArrayList<>();

		// - We retrieve it
		EgfaData returnedEgfaData = egfaDataPersistenceAdapter.findNewestByRegelungsVorhabenIdAndModuleName(REGELUNGSVORHABEN_ID, EgfaModuleType.ENAP);

		// Assert
		assertNull(returnedEgfaData);
	}

	@Test
	void testSaveWithNewEgfaData() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();
		when(egfaDataRepository.findByRegelungsVorhabenIdAndModuleNameAndCompletedAt(egfaDataEntity.getRegelungsVorhabenId(), egfaDataEntity.getModuleName(),
			egfaDataEntity.getCreatedAt())).thenReturn(Optional.of(egfaDataEntity));

		// Act
		EgfaData actual = egfaDataPersistenceAdapter.save(egfaData, status);

		// Assert
		assertThat(status).isNotEmpty();
		assertEquals(egfaData, actual);
	}

	@Test
	void testExistsEgfaDataTransmittedByUser() {
		// given
		final UserId transmitter = new UserId(TestObjectsUtil.getRandomString());
		final UserId otherUser = new UserId(TestObjectsUtil.getRandomString());

		when(egfaDataRepository.existsByTransmittedByUser(transmitter.getId())).thenReturn(true);

		// when
		final boolean existsByTransmitter = egfaDataPersistenceAdapter.existsEgfaDataTransmittedByUser(transmitter);
		final boolean existsByOtherUser = egfaDataPersistenceAdapter.existsEgfaDataTransmittedByUser(otherUser);

		// then
		assertThat(existsByTransmitter).isTrue();
		assertThat(existsByOtherUser).isFalse();
	}
}
