// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.EgfaDataEntity;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestEntitiesUtil;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class EgfaDataRepositoryTest {

	@Autowired
	private EgfaDataRepository egfaDataRepository;

	@Test
	void testExistsByTransmittedByUser() {
		// given
		final EgfaDataEntity egfaEntity = TestEntitiesUtil.getEgfaEntity();
		egfaDataRepository.save(egfaEntity);

		final UserId transmitter = new UserId(egfaEntity.getTransmittedByUser());
		final UserId otherUser = new UserId(TestObjectsUtil.getRandomString());

		// when
		final boolean existsByTransmitter = egfaDataRepository.existsByTransmittedByUser(transmitter.getId());
		final boolean existsByOtherUser = egfaDataRepository.existsByTransmittedByUser(otherUser.getId());

		// then
		assertThat(existsByTransmitter).isTrue();
		assertThat(existsByOtherUser).isFalse();
	}
}
