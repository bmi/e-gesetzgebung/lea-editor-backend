// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.EgfaMetadata;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.EgfaMetadataId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.persistence.UserRepository;
import de.itzbund.egesetz.bmi.lea.persistence.entities.EgfaMetadataEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.BaseMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.EgfaMetadataMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.EgfaMetadataMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.EgfaMetadataRepository;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestEntitiesUtil;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!! In H2 MEMORY MODE, the scripts schema.sql and data.sql !!!!!!!!!!!!
// !!!!!!!!!! are automatically applied !!!                          !!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@ExtendWith(MockitoExtension.class)
@SuppressWarnings("unused")
class EgfaMetadataPersistenceAdapterTest {

	@Spy
	private final EgfaMetadataMapperPersistence egfaMetadataMapper = new EgfaMetadataMapperPersistenceImpl(new BaseMapperPersistenceImpl());
	@Spy
	private final UserMapperPersistence userMapper = new UserMapperPersistenceImpl();

	@InjectMocks
	private EgfaMetadataPersistenceAdapter egfaMetadataPersistenceAdapter;

	@Mock
	private EgfaMetadataRepository egfaMetadataRepository;
	@Mock
	private UserRepository userRepository;

	private EgfaMetadata egfaMetadata;
	private EgfaMetadataEntity egfaMetadataEntity;
	private User user;

	@BeforeEach
	void init() {
		user = TestObjectsUtil.getUserEntity();

		egfaMetadata = TestObjectsUtil.getEgfaMetaData();
		egfaMetadata.setCreatedByUserId(user);
		egfaMetadataEntity = egfaMetadataMapper.fromDomainItem(egfaMetadata);
		egfaMetadataEntity = egfaMetadataMapper.fromDomainItem(egfaMetadata);
		EgfaMetadataId egfaMetadataId = egfaMetadataEntity.getEgfaMetadataId();
	}

	@Test
	void testSaveAndRetrieveWithNewEgfaMetaData() {
		// Arrange
		NutzerEntity userEntity = TestEntitiesUtil.getRandomNutzerEntity();
		when(userRepository.findFirstByGid(user.getGid())).thenReturn(userEntity);

		List<ServiceState> status = new ArrayList<>();
		when(egfaMetadataRepository.save(egfaMetadataEntity)).thenReturn(egfaMetadataEntity);

		EgfaMetadata actual = egfaMetadataPersistenceAdapter.save(egfaMetadata);
		if (actual.getCreatedAt() != null) {
			actual.setCreatedAt(egfaMetadata.getCreatedAt());
		}

		assertEquals(egfaMetadata, actual);

	}

	@Test
	void testExistsEgfaMetadataCreatedByUser() {
		// given
		final UserId owner = new UserId(TestObjectsUtil.getRandomString());
		final UserId otherUser = new UserId(TestObjectsUtil.getRandomString());

		when(egfaMetadataRepository.existsByCreatedByUser(owner.getId())).thenReturn(true);

		// when
		final boolean existsByOwner = egfaMetadataPersistenceAdapter.existsEgfaMetadataCreatedByUser(owner);
		final boolean existsByOtherUser = egfaMetadataPersistenceAdapter.existsEgfaMetadataCreatedByUser(otherUser);

		// then
		assertThat(existsByOwner).isTrue();
		assertThat(existsByOtherUser).isFalse();
	}
}
