// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.mappers;

import de.itzbund.egesetz.bmi.lea.domain.model.Drucksache;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DrucksachenEntity;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestEntitiesUtil;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class DrucksacheMapperPersistenceTest {

    private DrucksacheMapperPersistence drucksacheMapperPersistence;

    @BeforeEach
    void init() {
        drucksacheMapperPersistence = new DrucksacheMapperPersistenceImpl();
    }

    @Test
    void testMapping_ToDomainItem() {
        // Arrange
        DrucksachenEntity drucksachenEntity = TestEntitiesUtil.getRandomDrucksachenEntity();

        // Act
        Drucksache actual = drucksacheMapperPersistence.toDomainItem(drucksachenEntity);

        // Assert
        assertThat(actual).hasNoNullFieldsOrProperties();
    }

    @Test
    void testMapping_FromDomainItem() {
        // Arrange
        Drucksache drucksache = TestObjectsUtil.getRandomDrucksache();

        // Act
        DrucksachenEntity actual = drucksacheMapperPersistence.fromDomainItem(drucksache);

        // Assert
        assertThat(actual).hasNoNullFieldsOrPropertiesExcept("id");
    }
}
