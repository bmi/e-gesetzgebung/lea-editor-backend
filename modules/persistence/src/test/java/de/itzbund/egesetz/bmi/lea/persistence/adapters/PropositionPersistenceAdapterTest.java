// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.PropositionEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.PropositionMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.PropositionMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.PropositionRepository;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.data.jpa.domain.Specification;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!! In H2 MEMORY MODE, the scripts schema.sql and data.sql !!!!!!!!!!!!
// !!!!!!!!!! are automatically applied !!!                          !!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@SuppressWarnings("unused")
class PropositionPersistenceAdapterTest {

    @Spy
    private final PropositionMapperPersistence propositionMapper = new PropositionMapperPersistenceImpl();

    @Mock
    private PropositionRepository propositionRepository;

    @InjectMocks
    PropositionPersistenceAdapter propositionPersistenceAdapter;

    private static final String USER_ID_FROM_DATABASE_SCRIPT = "someGid";
    private static final String COMMENT_ID_FROM_DATABASE_SCRIPT = "00000000-0000-c000-0000-000000000001";
    private static final String DOCUMENT_ID_FROM_DATABASE_SCRIPT = "00000000-0000-0000-d0c0-000000000001";
    private static final CommentId COMMENT_ID = new CommentId(UUID.fromString(COMMENT_ID_FROM_DATABASE_SCRIPT));
    private static final DocumentId DOCUMENT_ID = new DocumentId(UUID.fromString(DOCUMENT_ID_FROM_DATABASE_SCRIPT));
    private User user;
    private NutzerEntity userEntity;
    private String userId;

    private Proposition proposition;
    private PropositionEntity propositionEntity;

    @BeforeEach
    void init() {
        MockitoAnnotations.openMocks(this);

        proposition = TestObjectsUtil.getRandomProposition();
        propositionEntity = propositionMapper.fromDomainItem(proposition);
    }

    @Test
    void testFindByNotUpdateablePropositionIdReferableId() {
        // Arrange
        propositionEntity.setReferableId(UUID.randomUUID());
        when(propositionRepository.findOne(any(Specification.class))).thenReturn(Optional.of(propositionEntity));

        // Act
        Proposition actual = propositionPersistenceAdapter.findByNotUpdateablePropositionIdReferableId(
            new RegelungsVorhabenId(UUID.randomUUID()), UUID.randomUUID());

        // Assert
        assertThat(actual).hasNoNullFieldsOrProperties();
    }

}
