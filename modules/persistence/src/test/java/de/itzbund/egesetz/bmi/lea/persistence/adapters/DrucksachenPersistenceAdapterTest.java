// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import de.itzbund.egesetz.bmi.lea.domain.model.Drucksache;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DrucksachenEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.DrucksacheMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.DrucksacheMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.DrucksachenRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!! In H2 MEMORY MODE, the scripts schema.sql and data.sql !!!!!!!!!!!!
// !!!!!!!!!! are automatically applied !!!                          !!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@SuppressWarnings("unused")
class DrucksachenPersistenceAdapterTest {

    @Spy
    private final DrucksacheMapperPersistence drucksacheMapperPersistence = new DrucksacheMapperPersistenceImpl();

    @Mock
    DrucksachenRepository drucksachenRepository;

    @InjectMocks
    private DrucksachenPersistenceAdapter drucksachenPersistenceAdapter;

    private final static String DRUCKSACHEN_NR = "123/2004";
    private final static RegelungsVorhabenId REGELUNGSVORHABEN_ID = new RegelungsVorhabenId(UUID.randomUUID());

    private final Drucksache drucksache = Drucksache.builder()
        .drucksachenNr(DRUCKSACHEN_NR)
        .regelungsVorhabenId(REGELUNGSVORHABEN_ID)
        .build();

    private final DrucksachenEntity drucksachenEntity = drucksacheMapperPersistence.fromDomainItem(drucksache);

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    // --------------- find ------------------

    @Test
    void testFindAll() {
        // Arrange
        when(drucksachenRepository.findAll()).thenReturn(List.of(drucksachenEntity));

        // Act
        List<Drucksache> actual = drucksachenPersistenceAdapter.findAll();

        // Assert
        assertThat(actual).hasSize(1);
    }

    @Test
    void testFindAllEmpty() {
        // Arrange
        when(drucksachenRepository.findAll()).thenReturn(Collections.emptyList());

        // Act
        List<Drucksache> actual = drucksachenPersistenceAdapter.findAll();

        // Assert
        assertThat(actual).isEmpty();
    }


    @Test
    void testFindByDrucksachenNummerVorhanden() {
        // Arrange
        when(drucksachenRepository.findByDrucksachenNr(DRUCKSACHEN_NR)).thenReturn(drucksachenEntity);

        // Act
        Drucksache actual = drucksachenPersistenceAdapter.findByDrucksachenNr(DRUCKSACHEN_NR);

        // Assert
        assertEquals(drucksache, actual);
    }

    @Test
    void testFindByDrucksachenNummerNichtVorhanden() {
        // Arrange
        when(drucksachenRepository.findByDrucksachenNr(DRUCKSACHEN_NR)).thenReturn(null);

        // Act
        Drucksache actual = drucksachenPersistenceAdapter.findByDrucksachenNr(DRUCKSACHEN_NR);

        // Assert
        assertThat(actual).isNull();
    }

    @Test
    void testFindByRegelungsvorhabenVorhanden() {
        // Arrange
        when(drucksachenRepository.findByRegelungsvorhabenId(REGELUNGSVORHABEN_ID)).thenReturn(drucksachenEntity);

        // Act
        Drucksache actual = drucksachenPersistenceAdapter.findByRegelungsvorhabenId(REGELUNGSVORHABEN_ID);

        // Assert
        assertEquals(drucksache, actual);
    }

    @Test
    void testFindByRegelungsvorhabenNichtVorhanden() {
        // Arrange
        when(drucksachenRepository.findByRegelungsvorhabenId(REGELUNGSVORHABEN_ID)).thenReturn(null);

        // Act
        Drucksache actual = drucksachenPersistenceAdapter.findByRegelungsvorhabenId(REGELUNGSVORHABEN_ID);

        // Assert
        assertThat(actual).isNull();
    }

    // --------------- save ------------------

    @Test
    void saveShouldFail_ifDrucksacheNrIsExisting() {
        // Arrange
        when(drucksachenRepository.findByDrucksachenNr(DRUCKSACHEN_NR)).thenReturn(drucksachenEntity);

        // Act
        Drucksache actual = drucksachenPersistenceAdapter.save(drucksache);

        // Assert
        assertThat(actual).isNull();
    }

    @Test
    void saveShouldFail_ifDrucksacheIsExistingForRegelungsvorhaben() {
        // Arrange
        when(drucksachenRepository.findByRegelungsvorhabenId(REGELUNGSVORHABEN_ID)).thenReturn(drucksachenEntity);

        // Act
        Drucksache actual = drucksachenPersistenceAdapter.save(drucksache);

        // Assert
        assertThat(actual).isNull();
    }

    @Test
    void testShouldBePossibleForNewValues() {
        // Arrange
        when(drucksachenRepository.save(drucksachenEntity)).thenReturn(drucksachenEntity);

        // Act
        Drucksache actual = drucksachenPersistenceAdapter.save(drucksache);

        // Assert
        assertEquals(drucksache, actual);
    }

}
