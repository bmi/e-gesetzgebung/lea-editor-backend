// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.mappers;

import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.persistence.entities.CompoundDocumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestEntitiesUtil;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CompundDocumentMapperPersistenceTest {

    private CompoundDocumentMapperPersistence compoundDocumentMapperPersistence;

    @BeforeEach
    void init() {
        compoundDocumentMapperPersistence = new CompoundDocumentMapperPersistenceImpl(new BaseMapperPersistenceImpl());
    }

    @Test
    void testMapping_ToDomainItem() {
        // Arrange
        CompoundDocumentEntity compoundDocumentEntity = TestEntitiesUtil.getCompoundDocumentEntity();

        // Act
        CompoundDocumentDomain actual = compoundDocumentMapperPersistence.toDomainItem(compoundDocumentEntity);

        // Assert
        assertThat(actual).hasNoNullFieldsOrPropertiesExcept("inheritFromId", "fixedRegelungsvorhabenReferenzId");
    }

    @Test
    void testMapping_FromDomainItem() {
        // Arrange
        CompoundDocumentDomain comment = TestObjectsUtil.getCompoundDocumentEntity();

        // Act
        CompoundDocumentEntity actual = compoundDocumentMapperPersistence.fromDomainItem(comment);

        // Assert
        assertThat(actual).hasNoNullFieldsOrPropertiesExcept("id", "inheritFromId");
    }
}
