// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.mappers;

import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.UserEntity;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestEntitiesUtil;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@SuppressWarnings("unused")
class UserMapperPersistenceTest {

    private final static UserId USER_ID = new UserId("AAA");
    private final static String USER_NAME = "New name";
    private final static String USER_EMAIL = "New Email";
    private final static UUID RANDOM_UUID = UUID.randomUUID();

    private final UserEntity storedThisUserEntity = UserEntity.builder()
        .gid(new UserId("xyz"))
        .name("Don't care")
        .email("An email")
        .build();
    private final User user = User.builder()
        .gid(USER_ID)
        .name(USER_NAME)
        .email(USER_EMAIL)
        .build();

    @Autowired
    private TestEntityManager entityManager;

    private UserMapperPersistence userMapper;
    private UserEntity storedUserEntity;


    @BeforeEach
    void init() {
        userMapper = new UserMapperPersistenceImpl();

        storedUserEntity = entityManager.persist(storedThisUserEntity);
        entityManager.flush();
    }

    @Test
    void testMapping_MergeWithExistingUser() {
        assertThat(storedUserEntity).hasFieldOrProperty("id").isNotNull();

        UserEntity actual = userMapper.merge(user, storedUserEntity);

        assertNotNull(actual);
        assertThat(actual).hasNoNullFieldsOrProperties();

        assertThat(actual).hasFieldOrPropertyWithValue("gid", USER_ID)
            .hasFieldOrPropertyWithValue("name", USER_NAME)
            .hasFieldOrPropertyWithValue("email", USER_EMAIL);
    }

    @Test
    void testMapping_MergeWithoutExistingUser() {
        UserEntity actual = userMapper.merge(user, UserEntity.builder().build());

        assertNotNull(actual);
        assertThat(actual).hasNoNullFieldsOrPropertiesExcept("id");

        assertThat(actual).hasFieldOrPropertyWithValue("gid", USER_ID)
            .hasFieldOrPropertyWithValue("name", USER_NAME)
            .hasFieldOrPropertyWithValue("email", USER_EMAIL);
    }

    @Test
    void testMapping_ToDomainItemRBAC() {
        // Arrange
        NutzerEntity nutzerEntity = TestEntitiesUtil.getNutzerEntity();

        // Act
        User actual = userMapper.toDomainItem(nutzerEntity);

        // Assert
        assertThat(actual).hasFieldOrPropertyWithValue("gid", new UserId(nutzerEntity.getGid()))
            .hasFieldOrPropertyWithValue("name", nutzerEntity.getName())
            .hasFieldOrPropertyWithValue("email", nutzerEntity.getEmail());
    }

    @Test
    void testMapping_MapEntities() {
        // Arrange
        NutzerEntity nutzerEntity = TestEntitiesUtil.getNutzerEntity();

        // Act
        UserEntity actual = userMapper.mapEntites(nutzerEntity);

        // Assert
        assertThat(actual)
            .hasFieldOrPropertyWithValue("gid", new UserId(nutzerEntity.getGid()))
            .hasFieldOrPropertyWithValue("name", nutzerEntity.getName())
            .hasFieldOrPropertyWithValue("email", nutzerEntity.getEmail());
    }

}
