// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtListDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.Bestandsrecht;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.BestandsrechtId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RvBestandsrechtId;
import de.itzbund.egesetz.bmi.lea.persistence.UserRepository;
import de.itzbund.egesetz.bmi.lea.persistence.entities.BestandsrechtEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.RvBestandsrechtEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.BestandsrechtMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.BestandsrechtRepository;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.RvBestandsrechtRepository;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestEntitiesUtil;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!! In H2 MEMORY MODE, the scripts schema.sql and data.sql !!!!!!!!!!!!
// !!!!!!!!!! are automatically applied !!!                          !!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@SuppressWarnings("unused")
class BestandsrechtPersistenceAdapterTest {

    @Spy
    private final BestandsrechtMapperPersistence bestandsrechtMapper = Mappers.getMapper(BestandsrechtMapperPersistence.class);

    @Mock
    BestandsrechtRepository bestandsrechtRepository;
    @Mock
    RvBestandsrechtRepository rvBestandsrechtRepository;
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    BestandsrechtPersistenceAdapter bestandsrechtPersistenceAdapter;

    private static final String REGELUNGSVORHABEN_ID_FROM_DATABASE_SCRIPT = "00000000-0000-0000-0000-000000000005";
    private static final RegelungsVorhabenId REGELUNGSVORHABEN_ID = new RegelungsVorhabenId(UUID.fromString(REGELUNGSVORHABEN_ID_FROM_DATABASE_SCRIPT));

    private User user;
    private NutzerEntity userEntity;
    private String userId;

    private Bestandsrecht bestandrecht;
    private BestandsrechtEntity bestandsrechtEntity;
    private BestandsrechtId bestandrechtId;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        user = TestObjectsUtil.getUserEntity();
        userId = user.getGid().getId();
        userEntity = TestEntitiesUtil.getRandomNutzerEntity();

        bestandrecht = TestObjectsUtil.getBestandrecht();
        bestandsrechtEntity = bestandsrechtMapper.fromDomainItem(bestandrecht);
        bestandrechtId = bestandsrechtEntity.getBestandsrechtId();

        when(userRepository.findFirstByGid(user.getGid())).thenReturn(userEntity);
    }

    @Test
    void testSaveWithNewBestandsrechtData() {
        // Arrange
        List<ServiceState> status = new ArrayList<>();
        when(bestandsrechtRepository.findFirstByEli(bestandrecht.getEli())).thenReturn(Optional.of(bestandsrechtEntity));
        when(bestandsrechtRepository.save(bestandsrechtEntity)).thenReturn(bestandsrechtEntity);

        // Act
        Bestandsrecht actual = bestandsrechtPersistenceAdapter.save(bestandrecht, REGELUNGSVORHABEN_ID, status);

        // Assert
        assertEquals(bestandrecht, actual);
    }

    @Test
    void testGetBestandsrechtEntitiesById() {
        // Arrange
        when(bestandsrechtRepository.findByBestandsrechtIdIn(Collections.emptyList())).thenReturn(Collections.emptyList());

        // Act
        List<BestandsrechtListDTO> actual = bestandsrechtPersistenceAdapter.getBestandsrechtEntitiesById(Collections.emptyList());

        // Assert
        Assertions.assertThat(actual).asList().hasSize(0);
    }

    @Test
    void testGetBestandsrechtAssociationsByRvId() {
        // Arrange
        RvBestandsrechtEntity bestandsrechtEntity = new RvBestandsrechtEntity(new RvBestandsrechtId(UUID.randomUUID()), REGELUNGSVORHABEN_ID, bestandrechtId);
        when(rvBestandsrechtRepository.findByRegelungsVorhabenId(REGELUNGSVORHABEN_ID)).thenReturn(List.of(bestandsrechtEntity));

        // Act
        List<BestandsrechtId> actual = bestandsrechtPersistenceAdapter.getBestandsrechtAssociationsByRvId(REGELUNGSVORHABEN_ID);

        // Assert
        Assertions.assertThat(actual).asList().hasSize(1);
        assertEquals(bestandrechtId, actual.get(0));
    }

}