// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.EgfaMetadataEntity;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestEntitiesUtil;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class EgfaMetadataRepositoryTest {

	@Autowired
	private EgfaMetadataRepository egfaMetadataRepository;

	@Test
	void testExistsByCreatedByUser() {
		// given
		final EgfaMetadataEntity egfaMetadataEntity = egfaMetadataRepository.save(TestEntitiesUtil.getEgfaMetadataEntity());

		final UserId owner = new UserId(egfaMetadataEntity.getCreatedByUser());
		final UserId otherUser = new UserId(TestObjectsUtil.getRandomString());

		// when
		final boolean existsByOwner = egfaMetadataRepository.existsByCreatedByUser(owner.getId());
		final boolean existsByOtherUser = egfaMetadataRepository.existsByCreatedByUser(otherUser.getId());

		// then
		assertThat(existsByOwner).isTrue();
		assertThat(existsByOtherUser).isFalse();
	}
}
