// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.persistence.UserRepository;
import de.itzbund.egesetz.bmi.lea.persistence.adapters.UserPersistenceAdapter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@SuppressWarnings("unused")
class UserRepositoryTest {

    @Autowired
    UserPersistenceAdapter userPersistenceAdapter;
    @Autowired
    private UserRepository userRepository;

    // ============================================== Auxiliary methods ==============================================

    private UserEntity createNewUserEntity() {
        return UserEntity.builder()
            .gid(new UserId(Utils.getRandomString()))
            .name(Utils.getRandomString())
            .email(Utils.getRandomString())
            .build();
    }

}
