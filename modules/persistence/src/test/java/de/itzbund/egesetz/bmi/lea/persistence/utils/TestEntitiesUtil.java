// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.utils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.VerfahrensType;
import de.itzbund.egesetz.bmi.lea.domain.enums.egfa.datenuebernahme.EgfaModuleType;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.EgfaDataId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.EgfaMetadataId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.ReplyId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.CommentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.CompoundDocumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DocumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DrucksachenEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.EgfaDataEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.EgfaMetadataEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.PositionEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.ReplyEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.UserEntity;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import lombok.Getter;
import lombok.experimental.UtilityClass;

@Getter
@UtilityClass
@SuppressWarnings({"java:S109", "unused"})
public class TestEntitiesUtil extends TestUserUtil {

	public static final String CONTENT = "some CONTENT";

	public static final String DOCUMENT_TITLE = "DOCUMENT Title";
	public static final String DOCUMENT_VERSION = "Version 1";
	public static final UUID DOCUMENT_ID = UUID.randomUUID();
	public static final DocumentType DOCUMENT_TYPE = DocumentType.REGELUNGSTEXT_STAMMGESETZ;

	public static final UUID REGELUNGSVORHABEN_ID = UUID.randomUUID();

	public static final UUID COMPOUND_DOCUMENT_ID = UUID.randomUUID();
	public static final String COMPOUND_DOCUMENT_TITLE = "COMPOUND DOCUMENT Title";
	public static final String COMPOUND_DOCUMENT_VERSION = "12";

	public static final Instant TIME_STAMP = Instant.now();
	public static final long ANCHOR_OFFSET = 1L;
	public static final List<Long> ANCHOR_PATH_LIST = List.of(1L, 2L, 3L);
	public static final long FOCUS_OFFSET = 2L;
	public static final List<Long> FOCUS_PATH_LIST = List.of(4L, 5L, 6L, 7L);

	// -------- User -------------
	public UserEntity getUserEntity() {
		return UserEntity.builder()
			.gid(new UserId(USER_ID))
			.name(USER_NAME)
			.email(USER_EMAIL)
			.build();
	}

	// -------- Compound document entity ----

	public CompoundDocumentEntity getCompoundDocumentEntity() {
		CompoundDocumentId compoundDocumentId = new CompoundDocumentId(COMPOUND_DOCUMENT_ID);

		return CompoundDocumentEntity.builder()
			.compoundDocumentId(compoundDocumentId)
			.title(COMPOUND_DOCUMENT_TITLE)
			.regelungsVorhabenId(new RegelungsVorhabenId(REGELUNGSVORHABEN_ID))
			.type(CompoundDocumentTypeVariant.STAMMGESETZ)
			.verfahrensType(VerfahrensType.REFERENTENENTWURF)
			.version(COMPOUND_DOCUMENT_VERSION)
			.state(DocumentState.DRAFT)
			.createdBy(getUserEntity().getGid().getId())
			.updatedBy(getUserEntity().getGid().getId())
			.createdAt(TIME_STAMP)
			.updatedAt(TIME_STAMP)
			.fixedRegelungsvorhabenReferenzId(UUID.randomUUID())
			.build();
	}

	public CompoundDocumentEntity getCompoundDocumentEntityWithSpecificId(UUID id) {
		CompoundDocumentEntity compoundDocumentEntity = getCompoundDocumentEntity();
		compoundDocumentEntity.setCompoundDocumentId(new CompoundDocumentId(id));

		return compoundDocumentEntity;
	}

	public CompoundDocumentEntity getRandomCompoundDocumentEntity() {
		return getCompoundDocumentEntityWithSpecificId(UUID.randomUUID());
	}

	public List<ReplyEntity> getReplyListOfSize(int size) {
		List<ReplyEntity> replies = new ArrayList<>();

		for (int i = 0; i < size; i++) {
			replies.add(getRandomReplyEntity());
		}

		return replies;
	}

	public ReplyEntity getRandomReplyEntity() {
		return getRandomReplyEntity(getUserEntity().getGid());
	}

	public ReplyEntity getRandomReplyEntity(final UserId owner) {
		UUID uuid = UUID.randomUUID();
		return ReplyEntity.builder()
			.replyId(new ReplyId(UUID.randomUUID()))
			.commentId(new CommentId(uuid))
			.parentId(uuid.toString())
			.documentId(new DocumentId(UUID.randomUUID()))
			.content(Utils.getRandomString())
			.updatedBy(owner.getId())
			.createdBy(owner.getId())
			.createdAt(Instant.now())
			.updatedAt(Instant.now())
			.build();
	}


	public PositionEntity getRandomPositionEntity() {
		return PositionEntity.builder()
			.offset(ANCHOR_OFFSET)
			.path(ANCHOR_PATH_LIST)
			.build();
	}


	public CommentEntity getRandomCommentEntity() {
		return CommentEntity.builder()
			.documentId(new DocumentId(UUID.randomUUID()))
			.commentId(new CommentId(UUID.randomUUID()))
			.content(Utils.getRandomString())
			.anchor(getRandomPositionEntity())
			.focus(getRandomPositionEntity())
			.updatedBy(getUserEntity().getGid().getId())
			.createdBy(getUserEntity().getGid().getId())
			.createdAt(Instant.now())
			.updatedAt(Instant.now())
			.replies(getReplyListOfSize(3))
			.build();
	}


	public DocumentEntity getRandomDocumentEntity() {
		DocumentEntity documentEntity = getDocumentEntity();
		documentEntity.setDocumentId(new DocumentId(UUID.randomUUID()));
		documentEntity.setCompoundDocumentId(new CompoundDocumentId(COMPOUND_DOCUMENT_ID));
		documentEntity.setInheritFromId(new DocumentId(UUID.randomUUID()));
		return documentEntity;
	}


	public DocumentEntity getDocumentEntity() {
		DocumentId documentId = new DocumentId(DOCUMENT_ID);

		return DocumentEntity.builder()
			.documentId(documentId)
			.content(CONTENT)
			.title(DOCUMENT_TITLE)
			.type(DOCUMENT_TYPE)
			.createdAt(TIME_STAMP)
			.updatedAt(TIME_STAMP)
			.createdBy(getUserEntity().getGid().getId())
			.updatedBy(getUserEntity().getGid().getId())
			.compoundDocumentId(new CompoundDocumentId(COMPOUND_DOCUMENT_ID))
			.build();
	}


	public User getUser() {
		return User.builder()
			.gid(new UserId(USER_ID))
			.name(USER_NAME)
			.email(USER_EMAIL)
			.build();
	}

	public NutzerEntity getNutzerEntity() {
		return NutzerEntity.builder()
			.gid(UUID.randomUUID().toString())
			.name(USER_NAME)
			.email(USER_EMAIL)
			.build();
	}

	public NutzerEntity getRandomNutzerEntity() {
		return NutzerEntity.builder()
			.gid(USER_ID)
			.name(USER_NAME)
			.email(USER_EMAIL)
			.build();
	}

	public EgfaDataEntity getEgfaEntity() {
		return EgfaDataEntity.builder()
			.egfaDataId(new EgfaDataId(UUID.randomUUID()))
			.transmittedByUser(USER_ID)
			.build();
	}

	public EgfaMetadataEntity getEgfaMetadataEntity() {
		return EgfaMetadataEntity.builder()
			.egfaMetadataId(new EgfaMetadataId(UUID.randomUUID()))
			.moduleName(EgfaModuleType.WEITERE)
			.createdByUser(USER_ID)
			.moduleCompletedAt(Instant.now())
			.build();
	}

	public DrucksachenEntity getRandomDrucksachenEntity() {
		return DrucksachenEntity.builder()
			.drucksachenNr("123/2024")
			.regelungsvorhabenId(new RegelungsVorhabenId(REGELUNGSVORHABEN_ID))
			.build();
	}
}
