// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.persistence.entities.TemplateEntity;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt.REGELUNGSTEXT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@SuppressWarnings("unused")
class TemplateRepositoryTest {

    @Autowired
    private TemplateRepository templateRepository;


    @ParameterizedTest
    @EnumSource(value = DocumentType.class, mode = EnumSource.Mode.EXCLUDE, names = {"BT_DRUCKSACHE"})
    void whenTemplateRequested_thenReturnLatestVersion(DocumentType documentType) {
        // Arrange
        String templateId = documentType.getTemplateID();
        int maxNumLevels = 1;

        if (documentType == DocumentType.REGELUNGSTEXT_STAMMGESETZ) {
            maxNumLevels = 4;
        }

        // Act
        for (int numLevels = 0; numLevels < maxNumLevels; numLevels++) {
            String templateName = templateId;
            if (documentType.getArt() == REGELUNGSTEXT) {
                templateName = String.format("%s-%s", templateId, numLevels);
            }

            List<TemplateEntity> templates =
                templateRepository.findAllByTemplateNameOrderByVersionDescUpdatedAtDesc(templateName);

            // Assert
            assertNotNull(templates);
            assertEquals(1, templates.size());

            TemplateEntity template = templates.get(0);
            assertEquals(1, template.getVersion());

            String jsonContent = template.getContent();
            assertFalse(Utils.isMissing(jsonContent));
            assertTrue(Utils.isJSONValid(jsonContent));
        }
    }

}
