// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.persistence.UserRepository;
import de.itzbund.egesetz.bmi.lea.persistence.entities.CompoundDocumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.BaseMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.CompoundDocumentMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.CompoundDocumentMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.CompoundDocumentRepository;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestEntitiesUtil;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!! In H2 MEMORY MODE, the scripts schema.sql and data.sql !!!!!!!!!!!!
// !!!!!!!!!! are automatically applied !!!                          !!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@ExtendWith(MockitoExtension.class)
@SuppressWarnings("unused")
class CompoundDocumentPersistenceAdapterTest {

	@Spy
	private final CompoundDocumentMapperPersistence commentMapper = new CompoundDocumentMapperPersistenceImpl(new BaseMapperPersistenceImpl());
	@Spy
	private final UserMapperPersistence userMapper = new UserMapperPersistenceImpl();

	@InjectMocks
	private CompoundDocumentPersistenceAdapter compoundDocumentPersistenceAdapter;

	@Mock
	private CompoundDocumentRepository compoundDocumentRepository;
	@Mock
	private UserRepository userRepository;

	private static final String REGELUNGSVORHABEN_ID_FROM_DATABASE_SCRIPT = "00000000-0000-0000-0000-000000000005";
	private static final String COMPOUNDDOCUMENT_ID_FROM_DATABASE_SCRIPT = "00000000-0000-0000-00cd-000000000001";
	private static final RegelungsVorhabenId REGELUNGSVORHABEN_ID = new RegelungsVorhabenId(UUID.fromString(REGELUNGSVORHABEN_ID_FROM_DATABASE_SCRIPT));

	@Test
	void testFindLatestCompoundDocumentForRegelungsvorhabenListAndCreatedBy() {
		// Arrange
		List<CompoundDocumentEntity> compoundDocumentEntities = List.of(TestEntitiesUtil.getCompoundDocumentEntity());
		when(compoundDocumentRepository.findLatestByRegelungsVorhabenIdsAndCreatedBy(anyList(), anyString())).thenReturn(compoundDocumentEntities);

		List<RegelungsVorhabenId> regelungsVorhabenIds = List.of(compoundDocumentEntities.get(0).getRegelungsVorhabenId());

		// Act
		User user = TestObjectsUtil.getUserEntity();
		List<CompoundDocumentDomain> actual = compoundDocumentPersistenceAdapter.findLatestCompoundDocumentForRegelungsvorhabenListAndCreatedBy(
			regelungsVorhabenIds, user);

		// Assert
		assertThat(actual).hasSize(1);
	}

	@Test
	void testFindByDocumentIdAndCreatedByAndDeleted() {
		// Arrange
		List<CompoundDocumentEntity> compoundDocumentEntities = List.of(TestEntitiesUtil.getCompoundDocumentEntity());
		when(compoundDocumentRepository.findByRegelungsVorhabenIdAndState(any(), any())).thenReturn(compoundDocumentEntities);

		// Act
		List<CompoundDocumentDomain> actual = compoundDocumentPersistenceAdapter.findByRegelungsVorhabenIdAndCompoundDocumentState(
			REGELUNGSVORHABEN_ID, DocumentState.BEREIT_FUER_KABINETT);

		// Assert
		assertThat(actual).hasSize(1);
	}

	@Test
	void testFindByCompoundDocumentIdIn() {
		// Arrange
		CompoundDocumentId compoundDocumentId = new CompoundDocumentId(UUID.fromString(COMPOUNDDOCUMENT_ID_FROM_DATABASE_SCRIPT));
		when(compoundDocumentRepository.findByCompoundDocumentIdIn(anyList())).thenReturn(List.of(TestEntitiesUtil.getCompoundDocumentEntity()));

		// Act
		List<CompoundDocumentDomain> actual = compoundDocumentPersistenceAdapter.findByCompoundDocumentIdIn(List.of(compoundDocumentId));

		// Assert
		assertThat(actual).hasSize(1);
	}

	@Test
	void testExistsCompoundDocumentsCreatedOrUpdatedByUser() {
		// given
		final UserId owner = new UserId(TestObjectsUtil.getRandomString());
		when(compoundDocumentRepository.existsByCreatedByOrUpdatedBy(owner.getId(), owner.getId())).thenReturn(true);

		final UserId otherUser = new UserId(TestObjectsUtil.getRandomString());

		// when
		final boolean existsByOwner = compoundDocumentPersistenceAdapter.existsCompoundDocumentsCreatedOrUpdatedByUser(owner);
		final boolean existsByOtherUser = compoundDocumentPersistenceAdapter.existsCompoundDocumentsCreatedOrUpdatedByUser(otherUser);

		// then
		assertThat(existsByOwner).isTrue();
		assertThat(existsByOtherUser).isFalse();
	}
}
