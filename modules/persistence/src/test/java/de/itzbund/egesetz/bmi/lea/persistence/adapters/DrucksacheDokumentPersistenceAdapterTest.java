// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DruckDokumentId;
import de.itzbund.egesetz.bmi.lea.persistence.UserRepository;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DruckDokumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DrucksacheDokumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.DruckDokumentRepository;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.DrucksacheDokumentRepository;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestEntitiesUtil;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!! In H2 MEMORY MODE, the scripts schema.sql and data.sql !!!!!!!!!!!!
// !!!!!!!!!! are automatically applied !!!                          !!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@SuppressWarnings("unused")
class DrucksacheDokumentPersistenceAdapterTest {

	@Mock
	DrucksacheDokumentRepository drucksacheDokumentRepository;
	@Mock
	DruckDokumentRepository druckDokumentRepository;
	@Mock
	private UserRepository userRepository;

	@InjectMocks
	DrucksacheDokumentPersistenceAdapter drucksacheDokumentPersistenceAdapter;

	private static final String compoundDocumentIdString = "00000000-0000-0000-0000-000000000005";
	private static final CompoundDocumentId COMPOUND_DOCUMENT_ID = new CompoundDocumentId(UUID.fromString(compoundDocumentIdString));

	private User user;
	private NutzerEntity userEntity;
	private String userId;

	private DrucksacheDokumentEntity drucksacheDokumentEntity = new DrucksacheDokumentEntity();
	private DruckDokumentEntity druckDokumentEntity = new DruckDokumentEntity();


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		user = TestObjectsUtil.getUserEntity();
		userId = user.getGid().getId();
		userEntity = TestEntitiesUtil.getRandomNutzerEntity();

		when(userRepository.findFirstByGid(user.getGid())).thenReturn(userEntity);
	}

	@Test
	void testUpdateWithExistingDrucksacheDokumentData() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();
		drucksacheDokumentEntity.setDruckDokumentId(new DruckDokumentId(UUID.randomUUID()));
		when(drucksacheDokumentRepository.findByCompoundDocumentId(COMPOUND_DOCUMENT_ID)).thenReturn(Optional.of(drucksacheDokumentEntity));
		when(druckDokumentRepository.findByDruckDokumentId(any())).thenReturn(Optional.of(druckDokumentEntity));

		// Act
		byte[] bytes = "A byte array".getBytes();
		SerialBlob blob = null;
		try {
			blob = new SerialBlob(bytes);
		} catch (SQLException ignored) {
		}

		ServiceState returnState = drucksacheDokumentPersistenceAdapter.updateDokumentDrucksache(blob, COMPOUND_DOCUMENT_ID, user);

		// Assert
		assertEquals(returnState, ServiceState.OK);
	}

	@Test
	void testUpdateWithNewDrucksacheDokumentData() {
		// Arrange
		List<ServiceState> status = new ArrayList<>();
		when(drucksacheDokumentRepository.findByCompoundDocumentId(COMPOUND_DOCUMENT_ID)).thenReturn(Optional.empty());

		// Act
		byte[] bytes = "A byte array".getBytes();
		SerialBlob blob = null;
		try {
			blob = new SerialBlob(bytes);
		} catch (SQLException ignored) {
		}

		ServiceState returnState = drucksacheDokumentPersistenceAdapter.updateDokumentDrucksache(blob, COMPOUND_DOCUMENT_ID, user);

		// Assert
		assertEquals(returnState, ServiceState.OK);
	}

	@Test
	void testGetDrucksacheByCompoundDocumentId1() {
		// Arrange
		when(drucksacheDokumentRepository.findByCompoundDocumentId(COMPOUND_DOCUMENT_ID)).thenReturn(Optional.empty());

		// Act
		List<ServiceState> status = new ArrayList<>();
		Optional<byte[]> returnedOptional = drucksacheDokumentPersistenceAdapter.getDrucksacheByCompoundDocumentId(COMPOUND_DOCUMENT_ID, status);

		// Assert
		assertThat(returnedOptional).isEmpty();
		assertThat(status.get(0)).isEqualTo(ServiceState.DRUCK_NOT_FOUND);
	}

	@Test
	void testGetDrucksacheByCompoundDocumentId2() {
		// Arrange
		when(drucksacheDokumentRepository.findByCompoundDocumentId(COMPOUND_DOCUMENT_ID)).thenReturn(Optional.of(drucksacheDokumentEntity));
		when(druckDokumentRepository.findByDruckDokumentId(any())).thenReturn(Optional.empty());

		// Act
		List<ServiceState> status = new ArrayList<>();
		Optional<byte[]> returnedOptional = drucksacheDokumentPersistenceAdapter.getDrucksacheByCompoundDocumentId(COMPOUND_DOCUMENT_ID, status);

		// Assert
		assertThat(returnedOptional).isEmpty();
		assertThat(status.get(0)).isEqualTo(ServiceState.UNKNOWN_ERROR);
	}

	@Test
	void testGetDrucksacheByCompoundDocumentId3() {
		// Arrange
		byte[] bytes = "A byte array".getBytes();
		Blob blob = null;
		try {
			blob = new SerialBlob(bytes);
		} catch (SQLException ignored) {
		}
		druckDokumentEntity.setPdfBlob(blob);
		when(drucksacheDokumentRepository.findByCompoundDocumentId(COMPOUND_DOCUMENT_ID)).thenReturn(Optional.of(drucksacheDokumentEntity));
		when(druckDokumentRepository.findByDruckDokumentId(any())).thenReturn(Optional.of(druckDokumentEntity));

		// Act
		List<ServiceState> status = new ArrayList<>();
		Optional<byte[]> returnedOptional = drucksacheDokumentPersistenceAdapter.getDrucksacheByCompoundDocumentId(COMPOUND_DOCUMENT_ID, status);

		// Assert
		assertThat(returnedOptional).isNotEmpty();
		assertThat(status).isEmpty();
	}

}