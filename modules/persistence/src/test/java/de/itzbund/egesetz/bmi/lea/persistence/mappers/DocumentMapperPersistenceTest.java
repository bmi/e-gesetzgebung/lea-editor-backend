// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.mappers;

import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DocumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestEntitiesUtil;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class DocumentMapperPersistenceTest {

    private DocumentMapperPersistence documentMapperPersistence;

    @BeforeEach
    void init() {
        documentMapperPersistence = new DocumentMapperPersistenceImpl(new BaseMapperPersistenceImpl());
    }

    @Test
    void testMapping_ToDomainItem() {
        // Arrange
        DocumentEntity documentEntity = TestEntitiesUtil.getDocumentEntity();

        // Act
        DocumentDomain actual = documentMapperPersistence.toDomainItem(documentEntity);

        // Assert
        assertThat(actual).hasNoNullFieldsOrPropertiesExcept("inheritFromId");
    }

    @Test
    void testMapping_FromDomainItem() {
        // Arrange
        DocumentDomain documentDomain = TestObjectsUtil.getDocumentEntity();

        // Act
        DocumentEntity actual = documentMapperPersistence.fromDomainItem(documentDomain);

        // Assert
        assertThat(actual).hasNoNullFieldsOrPropertiesExcept("id", "inheritFromId");
    }
}
