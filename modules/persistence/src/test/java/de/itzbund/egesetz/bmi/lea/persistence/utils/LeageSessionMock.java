// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.utils;

import java.util.HashMap;
import java.util.Map;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.experimental.UtilityClass;

@UtilityClass
public class LeageSessionMock {

	public MockedStatic<LeageSession> mockLeageSession() {
		final MockedStatic<LeageSession> mockedLeageSession = Mockito.mockStatic(LeageSession.class);

		Map<String, String> mockUserDetails = new HashMap<>();
		mockUserDetails.put(LeageSession.GID, "testUserId");
		Mockito.when(LeageSession.getUserDatailsFromAuthentication()).thenReturn(mockUserDetails);

		return mockedLeageSession;
	}
}
