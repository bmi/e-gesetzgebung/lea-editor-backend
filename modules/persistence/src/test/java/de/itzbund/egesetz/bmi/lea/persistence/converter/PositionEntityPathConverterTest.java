// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.converter;

import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.PositionEntityPathConverter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class PositionEntityPathConverterTest {

    private PositionEntityPathConverter converter;


    @BeforeEach
    void init() {
        converter = new PositionEntityPathConverter();
    }


    @Test
    void testConvertToDatabaseColumn() {
        assertNull(converter.convertToDatabaseColumn(null));

        List<Long> path = List.of();
        assertEquals("[]", converter.convertToDatabaseColumn(path));

        path = List.of(1L, 2L, 3L);
        assertEquals("[1, 2, 3]", converter.convertToDatabaseColumn(path));
    }


    @Test
    void testConvertToEntityAttribute() {
        assertEquals(List.of(), converter.convertToEntityAttribute(null));
        assertEquals(List.of(), converter.convertToEntityAttribute(""));
        assertEquals(List.of(), converter.convertToEntityAttribute("[]"));
        assertEquals(List.of(7L, 11L, 13L), converter.convertToEntityAttribute("[7, 11, 13]"));
    }


    @AfterEach
    void clean() {
        converter = null;
    }

}
