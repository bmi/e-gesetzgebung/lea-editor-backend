// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.persistence.UserRepository;
import de.itzbund.egesetz.bmi.lea.persistence.entities.CompoundDocumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DocumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.BaseMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.CompoundDocumentMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.CompoundDocumentMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.DocumentMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.DocumentMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.CompoundDocumentRepository;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.DocumentRepository;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestEntitiesUtil;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!! In H2 MEMORY MODE, the scripts schema.sql and data.sql !!!!!!!!!!!!
// !!!!!!!!!! are automatically applied !!!                          !!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@SuppressWarnings("unused")
class DocumentPersistenceAdapterTest {

	@Spy
	private final DocumentMapperPersistence documentMapper = new DocumentMapperPersistenceImpl(new BaseMapperPersistenceImpl());
	@Spy
	private final CompoundDocumentMapperPersistence compoundDocumentMapper = new CompoundDocumentMapperPersistenceImpl(new BaseMapperPersistenceImpl());
	@Spy
	private final UserMapperPersistence userMapper = new UserMapperPersistenceImpl();

	@Mock
	private DocumentRepository documentRepository;
	@Mock
	private CompoundDocumentRepository compoundDocumentRepository;
	@Mock
	private UserRepository userRepository;

	@InjectMocks
	private DocumentPersistenceAdapter documentPersistenceAdapter;

	private final CompoundDocumentId compoundDocumentId = new CompoundDocumentId(UUID.randomUUID());
	private User user;
	private String userId;

	private DocumentDomain documentDomain;
	private DocumentEntity documentEntity;
	private CompoundDocumentEntity compoundDocumentEntity;
	private DocumentId documentId;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		user = TestObjectsUtil.getUserEntity();
		userId = user.getGid().getId();
		NutzerEntity userEntity = TestEntitiesUtil.getRandomNutzerEntity();

		documentDomain = TestObjectsUtil.getDocumentEntity();
		documentDomain.setCreatedBy(user);
		documentEntity = documentMapper.fromDomainItem(documentDomain);
		documentId = documentEntity.getDocumentId();
		CompoundDocumentDomain compoundDocumentDomain = TestObjectsUtil.getCompoundDocumentEntity();
		compoundDocumentEntity = compoundDocumentMapper.fromDomainItem(compoundDocumentDomain);

		when(userRepository.findFirstByGid(user.getGid())).thenReturn(userEntity);
		when(userRepository.findByGidIn(Set.of(user.getGid().getId()))).thenReturn(Collections.singletonMap(user.getGid().getId(), userEntity));
	}

	@Test
	void testFindByCreatedBy() {
		// Arrange
		List<DocumentEntity> documentEntities = Collections.singletonList(documentEntity);
		when(documentRepository.findByCreatedBy(userId)).thenReturn(documentEntities);

		// Act
		List<DocumentDomain> actual = documentPersistenceAdapter.findByCreatedBy(user);

		// Assert
		assertEquals(1, actual.size());
	}

	@Test
	void testFindByCreatedByAndDocumentIdIn() {
		// Arrange
		List<DocumentEntity> documentEntities = Collections.singletonList(documentEntity);
		List<DocumentId> ids = Collections.singletonList(documentId);
		when(documentRepository.findByCreatedByAndDocumentIdIn(any(), anyList())).thenReturn(documentEntities);

		// Act
		List<DocumentDomain> actual = documentPersistenceAdapter.findByCreatedByAndDocumentIdIn(user, ids);

		// Assert
		assertEquals(1, actual.size());
	}

	@Test
	void testFindByCreatedByAndCompoundDocumentId() {
		// Arrange
		List<DocumentEntity> documentEntities = Collections.singletonList(documentEntity);
		CompoundDocumentId compoundDocumentId = documentEntity.getCompoundDocumentId();
		when(documentRepository.findByCreatedByAndCompoundDocumentId(userId, compoundDocumentId)).thenReturn(documentEntities);

		// Act
		List<DocumentDomain> actual = documentPersistenceAdapter.findByCreatedByAndCompoundDocumentId(user, compoundDocumentId);

		// Assert
		assertEquals(1, actual.size());
	}

	@Test
	void testFindByCreatedByAndCompoundDocumentIdIn() {
		// Arrange
		List<DocumentEntity> documentEntities = Collections.singletonList(documentEntity);
		List<CompoundDocumentId> compoundDocumentIdList = List.of(documentEntity.getCompoundDocumentId());
		when(documentRepository.findByCreatedByAndCompoundDocumentIdIn(userId, compoundDocumentIdList)).thenReturn(documentEntities);

		// Act
		List<DocumentDomain> actual = documentPersistenceAdapter.findByCreatedByAndCompoundDocumentIdIn(user, compoundDocumentIdList);

		// Assert
		assertEquals(1, actual.size());
	}

	@Test
	void testFindByDocumentId() {
		when(documentRepository.findByDocumentId(documentId)).thenReturn(Optional.of(documentEntity));

		Optional<DocumentDomain> actual = documentPersistenceAdapter.findByDocumentId(documentId);

		assertEquals(documentDomain, actual.orElse(null));
	}

	@Test
	void testFindByCompoundDocumentId() {
		List<DocumentEntity> documentEntities = Collections.singletonList(documentEntity);
		List<DocumentDomain> documentDomains = Collections.singletonList(documentDomain);

		when(documentRepository.findByCompoundDocumentId(compoundDocumentId)).thenReturn(documentEntities);

		Optional<List<DocumentDomain>> actual = documentPersistenceAdapter.findByCompoundDocumentId(compoundDocumentId);

		assertEquals(documentDomains, actual.orElse(null));
	}

	@Test
	void testFindByCompoundDocumentIdIn() {
		List<DocumentEntity> documentEntities = Collections.singletonList(documentEntity);
		List<DocumentDomain> documentDomains = Collections.singletonList(documentDomain);

		when(documentRepository.findByCompoundDocumentIdIn(List.of(compoundDocumentId))).thenReturn(Collections.emptyList());

		List<DocumentDomain> actual = documentPersistenceAdapter.findByCompoundDocumentIdIn(List.of(compoundDocumentId));

		assertEquals(Collections.emptyList(), actual);
	}

	@Test
	void testSave() {
		when(documentRepository.findByDocumentId(documentDomain.getDocumentId())).thenReturn(Optional.of(documentEntity));
		when(documentRepository.save(documentEntity)).thenReturn(documentEntity);
		when(compoundDocumentRepository.findByCompoundDocumentId(any())).thenReturn(Optional.of(compoundDocumentEntity));

		DocumentDomain actual = documentPersistenceAdapter.save(documentDomain);
		if (actual.getUpdatedAt() != null) {
			actual.setUpdatedAt(documentDomain.getUpdatedAt());
		}

		assertEquals(documentDomain, actual);
	}

	@Test
	void testCountByCompoundDocumentIdAndTypeIs() {
		List<DocumentEntity> documentEntities = Collections.singletonList(documentEntity);
		List<DocumentDomain> documentDomains = Collections.singletonList(documentDomain);

		when(documentRepository.countByCompoundDocumentIdAndTypeIs(compoundDocumentId, DocumentType.ANLAGE)).thenReturn(10);

		int actual = documentPersistenceAdapter.countByCompoundDocumentIdAndTypeIs(compoundDocumentId, DocumentType.ANLAGE);

		assertEquals(10, actual);
	}

	@Test
	void testExistsDokumentCreatedOrUpdatedByUser() {
		// given
		final UserId owner = new UserId(TestObjectsUtil.getRandomString());
		final UserId otherUser = new UserId(TestObjectsUtil.getRandomString());
		when(documentRepository.existsByCreatedByOrUpdatedBy(owner.getId(), owner.getId())).thenReturn(true);

		// when
		final boolean existsByOwner = documentPersistenceAdapter.existsDokumentCreatedOrUpdatedByUser(owner);
		final boolean existsByOtherUser = documentPersistenceAdapter.existsDokumentCreatedOrUpdatedByUser(otherUser);

		// then
		assertThat(existsByOwner).isTrue();
		assertThat(existsByOtherUser).isFalse();
	}
}
