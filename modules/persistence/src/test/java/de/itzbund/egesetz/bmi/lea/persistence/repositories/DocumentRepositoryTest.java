// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.entities.DocumentCount;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DocumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.utils.LeageSessionMock;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

@DataJpaTest
@SuppressWarnings("unused")
class DocumentRepositoryTest {

    @Autowired
    private DocumentRepository documentRepository;

    String userGid = UUID.randomUUID().toString();
    private MockedStatic<LeageSession> mockedLeageSession;

    @BeforeEach
    void setUp() {
		mockedLeageSession = LeageSessionMock.mockLeageSession();
    }

    @AfterEach
    public void tearDown() {
        mockedLeageSession.close();  // Make sure to close the mock to avoid conflict
    }

    @Test
    void whenForGivenCompoundDocumentId_thereIsNoDocumentOfTypeAnlage_thenReturn_0() {
        // Arrange
        CompoundDocumentId compoundDocumentId = new CompoundDocumentId(UUID.randomUUID());
        documentRepository.save(getDocument(compoundDocumentId, DocumentType.VORBLATT_MANTELGESETZ));

        // Act
        List<DocumentEntity> optionalDocumentEntities =
            documentRepository.findByCompoundDocumentId(compoundDocumentId);
        int count = documentRepository.countByCompoundDocumentIdAndTypeIs(compoundDocumentId, DocumentType.ANLAGE);

        // Assert
        assertNotNull(optionalDocumentEntities);
        assertFalse(optionalDocumentEntities.isEmpty());
        assertEquals(1, optionalDocumentEntities.size());
        assertEquals(0, count);
    }

    @Test
    void whenForGivenCompoundDocumentId_thereIsOneDocumentOfTypeAnlage_thenReturn_1() {
        // Arrange
        CompoundDocumentId compoundDocumentId = new CompoundDocumentId(UUID.randomUUID());
        documentRepository.save(getDocument(compoundDocumentId, DocumentType.VORBLATT_STAMMGESETZ));
        documentRepository.save(getDocument(compoundDocumentId, DocumentType.ANLAGE));
        documentRepository.save(getDocument(compoundDocumentId, DocumentType.REGELUNGSTEXT_STAMMGESETZ));

        // Act
        List<DocumentEntity> optionalDocumentEntities =
            documentRepository.findByCompoundDocumentId(compoundDocumentId);
        int count = documentRepository.countByCompoundDocumentIdAndTypeIs(compoundDocumentId, DocumentType.ANLAGE);

        // Assert
        assertNotNull(optionalDocumentEntities);
        assertFalse(optionalDocumentEntities.isEmpty());

        assertEquals(3, optionalDocumentEntities.size());
        assertEquals(1, count);

        // Arrange
        compoundDocumentId = new CompoundDocumentId(UUID.randomUUID());
        documentRepository.save(getDocument(compoundDocumentId, DocumentType.ANLAGE));

        // Act
        optionalDocumentEntities = documentRepository.findByCompoundDocumentId(compoundDocumentId);
        count = documentRepository.countByCompoundDocumentIdAndTypeIs(compoundDocumentId, DocumentType.ANLAGE);

        // Assert
        assertNotNull(optionalDocumentEntities);
        assertFalse(optionalDocumentEntities.isEmpty());
        assertEquals(1, optionalDocumentEntities.size());
        assertEquals(1, count);
    }


    @Test
    void whenForGivenCompoundDocumentId_thereAreThreeDocumentsOfTypeAnlage_thenReturn_3() {
        // Arrange
        CompoundDocumentId compoundDocumentId = new CompoundDocumentId(UUID.randomUUID());
        documentRepository.save(getDocument(compoundDocumentId, DocumentType.VORBLATT_MANTELGESETZ));
        documentRepository.save(getDocument(compoundDocumentId, DocumentType.ANLAGE));
        documentRepository.save(getDocument(compoundDocumentId, DocumentType.BEGRUENDUNG_MANTELGESETZ));
        documentRepository.save(getDocument(compoundDocumentId, DocumentType.ANLAGE));
        documentRepository.save(getDocument(compoundDocumentId, DocumentType.ANLAGE));

        // Act
        List<DocumentEntity> optionalDocumentEntities =
            documentRepository.findByCompoundDocumentId(compoundDocumentId);
        int count = documentRepository.countByCompoundDocumentIdAndTypeIs(compoundDocumentId, DocumentType.ANLAGE);

        // Assert
        assertNotNull(optionalDocumentEntities);
        assertFalse(optionalDocumentEntities.isEmpty());
        assertEquals(5, optionalDocumentEntities.size());
        assertEquals(3, count);
    }

    @Test
    void testGetDocumentCount() {
        // Arrange
        CompoundDocumentId compoundDocumentId = new CompoundDocumentId(UUID.randomUUID());
        DocumentEntity regt = documentRepository.save(getDocument(compoundDocumentId, DocumentType.REGELUNGSTEXT_STAMMGESETZ)); // 1
        DocumentEntity anl1 = documentRepository.save(getDocument(compoundDocumentId, DocumentType.ANLAGE));                    // 1
        DocumentEntity vorb = documentRepository.save(getDocument(compoundDocumentId, DocumentType.VORBLATT_STAMMGESETZ));      // 1
        DocumentEntity begr = documentRepository.save(getDocument(compoundDocumentId, DocumentType.BEGRUENDUNG_STAMMGESETZ));   // 1
        DocumentEntity anl2 = documentRepository.save(getDocument(compoundDocumentId, DocumentType.ANLAGE));                    // 2
        DocumentEntity anl3 = documentRepository.save(getDocument(compoundDocumentId, DocumentType.ANLAGE));                    // 3
        List<DocumentEntity> documentEntities = List.of(anl1, anl2, anl3, begr, regt, vorb);

        // Act
        List<DocumentCount> documentCounts = documentRepository.getDocumentCount(compoundDocumentId.getId().toString())/*.stream()
            .map(documentMapper::toDocumentCount)
            .collect(Collectors.toList())*/;

        // Assert
        assertNotNull(documentCounts);
        assertEquals(6, documentCounts.size());
        for (int i = 0; i < documentCounts.size(); i++) {
            DocumentEntity documentEntity = documentEntities.get(i);
            DocumentCount documentCount = documentCounts.get(i);
            assertEquals(documentEntity.getType(), documentCount.getType());
            assertEquals(documentEntity.getDocumentId(), documentCount.getDocumentId());

            switch (documentEntity.getType()) {
                case REGELUNGSTEXT_STAMMGESETZ:
                case VORBLATT_STAMMGESETZ:
                case BEGRUENDUNG_STAMMGESETZ:
                    assertEquals(1, documentCount.getCount());
                    break;
                default:
                    checkCounts(anl1, anl2, anl3, documentCount);
            }
        }
    }

	@Test
	void testExistsByCreatedByOrUpdatedBy() {
		// given
		final CompoundDocumentId compoundDocumentId = new CompoundDocumentId(UUID.randomUUID());
		final DocumentEntity documentEntity = documentRepository.save(getDocument(compoundDocumentId, DocumentType.REGELUNGSTEXT_STAMMGESETZ));

		final UserId owner = new UserId(documentEntity.getCreatedBy());
		final UserId otherUser = new UserId(TestObjectsUtil.getRandomString());

		// when
		final boolean existsByOwner = documentRepository.existsByCreatedByOrUpdatedBy(owner.getId(), owner.getId());
		final boolean existsByOtherUser = documentRepository.existsByCreatedByOrUpdatedBy(otherUser.getId(), otherUser.getId());

		// then
		assertThat(existsByOwner).isTrue();
		assertThat(existsByOtherUser).isFalse();
	}

	private void checkCounts(DocumentEntity anl1, DocumentEntity anl2, DocumentEntity anl3, DocumentCount documentCount) {
        DocumentId docId = documentCount.getDocumentId();
        if (anl1.getDocumentId().equals(docId)) {
            assertEquals(1, documentCount.getCount());
        } else if (anl2.getDocumentId().equals(docId)) {
            assertEquals(2, documentCount.getCount());
        } else if (anl3.getDocumentId().equals(docId)) {
            assertEquals(3, documentCount.getCount());
        } else {
            fail();
        }
    }


    private DocumentEntity getDocument(CompoundDocumentId compoundDocumentId, DocumentType documentType) {
        return DocumentEntity.builder()
            .documentId(new DocumentId(UUID.randomUUID()))
            .compoundDocumentId(compoundDocumentId)
            .type(documentType)
            .title(Utils.getRandomString())
            .createdBy(userGid)
            .updatedBy(userGid)
            .createdAt(Instant.now())
            .updatedAt(Instant.now())
            .build();
    }

}
