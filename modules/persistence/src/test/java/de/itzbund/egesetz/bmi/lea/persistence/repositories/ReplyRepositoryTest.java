// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.persistence.entities.ReplyEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.UserEntity;
import de.itzbund.egesetz.bmi.lea.persistence.utils.LeageSessionMock;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestEntitiesUtil;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

@DataJpaTest
@SuppressWarnings("unused")
class ReplyRepositoryTest {

	@Autowired
	private ReplyRepository replyRepository;

	private MockedStatic<LeageSession> mockedLeageSession;

	@BeforeEach
	void setUpLeageSession() {
		mockedLeageSession = LeageSessionMock.mockLeageSession();
	}

	@AfterEach
	public void tearDownLeageSession() {
		mockedLeageSession.close();  // Make sure to close the mock to avoid conflict
	}

	@Test
	void whenUserOwnsReply_thenExistsCreatedByUserReturnsTrue() {
		// given
		final UserId owner = new UserId(TestObjectsUtil.getRandomString());
		final UserId otherUser = new UserId(TestObjectsUtil.getRandomString());
		final ReplyEntity replyEntity = replyRepository.save(TestEntitiesUtil.getRandomReplyEntity(owner));

		// when
		final boolean existsByOwner = replyRepository.existsByCreatedByOrUpdatedBy(owner.getId(), owner.getId());
		final boolean existsByOtherUser = replyRepository.existsByCreatedByOrUpdatedBy(otherUser.getId(), otherUser.getId());

		// then
		assertThat(existsByOwner).isTrue();
		assertThat(existsByOtherUser).isFalse();
	}
}
