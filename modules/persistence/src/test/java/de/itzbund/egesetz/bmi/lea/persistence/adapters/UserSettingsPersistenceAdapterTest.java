// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.UserSettings;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.persistence.UserRepository;
import de.itzbund.egesetz.bmi.lea.persistence.entities.UserSettingsEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.BaseMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserSettingsMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserSettingsMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.UserSettingsRepository;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestEntitiesUtil;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!! In H2 MEMORY MODE, the scripts schema.sql and data.sql !!!!!!!!!!!!
// !!!!!!!!!! are automatically applied !!!                          !!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@ExtendWith(MockitoExtension.class)
@SuppressWarnings("unused")
class UserSettingsPersistenceAdapterTest {

	@Spy
	private final UserSettingsMapperPersistence userSettingsMapper = new UserSettingsMapperPersistenceImpl(new BaseMapperPersistenceImpl());
	@Spy
	private final UserMapperPersistence userMapper = new UserMapperPersistenceImpl();

	@Mock
	private UserSettingsRepository userSettingsRepository;
	@Mock
	private UserRepository userRepository;

	@InjectMocks
	private UserSettingsPersistenceAdapter userSettingsPersistenceAdapter;

	@Test
	void testSaveGetAndUpdateUserSettings() {
		// Arrange
		User user = TestObjectsUtil.getUserEntity();
		NutzerEntity userEntity = TestEntitiesUtil.getRandomNutzerEntity();
		when(userRepository.findFirstByGid(user.getGid())).thenReturn(userEntity);

		UserSettings userSettings = TestObjectsUtil.getUserSettings();
		userSettings.setRelatedUser(user);
		UserSettingsEntity userSettingsEntity = userSettingsMapper.fromDomainItem(userSettings);
		when(userSettingsRepository.findByRelatedUser(userSettings.getRelatedUser().getGid().getId())).thenReturn(Optional.of(userSettingsEntity));

		when(userSettingsRepository.save(userSettingsEntity)).thenReturn(userSettingsEntity);

		// Act
		List<ServiceState> status = new ArrayList<>();
		UserSettings actual = userSettingsPersistenceAdapter.save(userSettings, status);

		// Arrange
		assertEquals(userSettings, actual);
	}

	@Test
	void testExistsUserSettingsByUser() {
		// given
		final UserId owner = new UserId(TestObjectsUtil.getRandomString());
		final UserId otherUser = new UserId(TestObjectsUtil.getRandomString());

		when(userSettingsRepository.existsByRelatedUser(owner.getId())).thenReturn(true);

		// when
		final boolean existsByOwner = userSettingsPersistenceAdapter.existsUserSettingsByUser(owner);
		final boolean existsByOtherUser = userSettingsPersistenceAdapter.existsUserSettingsByUser(otherUser);

		// then
		assertThat(existsByOwner).isTrue();
		assertThat(existsByOtherUser).isFalse();
	}
}
