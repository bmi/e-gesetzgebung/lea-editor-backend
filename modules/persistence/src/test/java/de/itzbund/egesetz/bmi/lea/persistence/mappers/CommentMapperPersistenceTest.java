// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.mappers;

import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import de.itzbund.egesetz.bmi.lea.persistence.entities.CommentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestEntitiesUtil;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CommentMapperPersistenceTest {

    private CommentMapperPersistence commentMapperPersistence;

    @BeforeEach
    void init() {
        BaseMapperPersistenceImpl baseMapperPersistence = new BaseMapperPersistenceImpl();
        commentMapperPersistence = new CommentMapperPersistenceImpl(baseMapperPersistence,
            new ReplyMapperPersistenceImpl(baseMapperPersistence));
    }

    @Test
    void testMapping_ToDomainItem() {
        // Arrange
        CommentEntity commentEntity = TestEntitiesUtil.getRandomCommentEntity();

        // Act
        Comment actual = commentMapperPersistence.toDomainItem(commentEntity);

        // Assert
        assertThat(actual).hasNoNullFieldsOrProperties();
    }

    @Test
    void testMapping_FromDomainItem() {
        // Arrange
        Comment comment = TestObjectsUtil.getRandomComment();

        // Act
        CommentEntity actual = commentMapperPersistence.fromDomainItem(comment);

        // Assert
        assertThat(actual).hasNoNullFieldsOrPropertiesExcept("id");
    }
}
