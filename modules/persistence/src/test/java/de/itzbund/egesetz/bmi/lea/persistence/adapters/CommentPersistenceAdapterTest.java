// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.persistence.UserRepository;
import de.itzbund.egesetz.bmi.lea.persistence.entities.CommentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.BaseMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.CommentMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.CommentMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.ReplyMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.CommentRepository;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestEntitiesUtil;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!! In H2 MEMORY MODE, the scripts schema.sql and data.sql !!!!!!!!!!!!
// !!!!!!!!!! are automatically applied !!!                          !!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@ExtendWith(MockitoExtension.class)
@SuppressWarnings("unused")
class CommentPersistenceAdapterTest {

	@Spy
	private final CommentMapperPersistence commentMapper = new CommentMapperPersistenceImpl(new BaseMapperPersistenceImpl(),
		new ReplyMapperPersistenceImpl(new BaseMapperPersistenceImpl()));
	@Spy
	private final UserMapperPersistence userMapper = new UserMapperPersistenceImpl();

	@Mock
	private CommentRepository commentRepository;
	@Mock
	private UserRepository userRepository;

	@InjectMocks
	CommentPersistenceAdapter commentPersistenceAdapter;

	private static final String COMMENT_ID_FROM_DATABASE_SCRIPT = "00000000-0000-c000-0000-000000000001";
	private static final String DOCUMENT_ID_FROM_DATABASE_SCRIPT = "00000000-0000-0000-d0c0-000000000001";
	private static final CommentId COMMENT_ID = new CommentId(UUID.fromString(COMMENT_ID_FROM_DATABASE_SCRIPT));
	private static final DocumentId DOCUMENT_ID = new DocumentId(UUID.fromString(DOCUMENT_ID_FROM_DATABASE_SCRIPT));
	private User user;

	private Comment comment;
	private CommentEntity commentEntity;

	@BeforeEach
	void init() {
		comment = TestObjectsUtil.getRandomComment();
		user = comment.getCreatedBy();
		commentEntity = commentMapper.fromDomainItem(comment);
	}

	@Test
	void testFindByDocumentIdAndCreatedByAndDeleted() {
		// Arrange
		when(commentRepository.findByDocumentIdAndCreatedByAndDeleted(any(DocumentId.class), anyString(), anyBoolean())).thenReturn(
			List.of(TestEntitiesUtil.getRandomCommentEntity()));

		// Act
		List<Comment> comments = commentPersistenceAdapter.findByDocumentIdAndCreatedByAndDeleted(DOCUMENT_ID, user, false);

		// Assert
		assertThat(comments).hasSize(1);
	}

	@Test
	void testFindByCommentId() {
		// Arrange
		mockNutzerRepositoryWithUser(user);
		when(commentRepository.findByCommentId(any(CommentId.class))).thenReturn(Optional.of(TestEntitiesUtil.getRandomCommentEntity()));

		// Act
		Comment actual = commentPersistenceAdapter.findByCommentId(COMMENT_ID);

		// Assert
		assertThat(actual).isNotNull().hasNoNullFieldsOrProperties();
	}

	@Test
	void testFindByDocumentId() {
		// Arrange
		when(commentRepository.findByDocumentId(any(DocumentId.class))).thenReturn(List.of(TestEntitiesUtil.getRandomCommentEntity()));

		// Act
		List<Comment> actual = commentPersistenceAdapter.findByDocumentId(DOCUMENT_ID);

		// Assert
		assertThat(actual).hasSize(1);
	}

	@Test
	void testFindByDocumentIdAndDeleted() {
		// Arrange
		when(commentRepository.findByDocumentIdAndDeleted(any(DocumentId.class), anyBoolean())).thenReturn(List.of(TestEntitiesUtil.getRandomCommentEntity()));

		// Act
		List<Comment> actual = commentPersistenceAdapter.findByDocumentIdAndDeleted(DOCUMENT_ID, true);

		// Assert
		assertThat(actual).hasSize(1);
	}

	@Test
	void testFindByDocumentIdAndCreatedBy() {
		// Arrange
		when(commentRepository.findByDocumentIdAndCreatedBy(any(DocumentId.class), anyString())).thenReturn(List.of(TestEntitiesUtil.getRandomCommentEntity()));

		// Act
		List<Comment> actual = commentPersistenceAdapter.findByDocumentIdAndCreatedBy(DOCUMENT_ID, user);

		// Assert
		assertThat(actual).hasSize(1);
	}


	@Test
	void testSaveWithNewCompoundDocument() {
		mockNutzerRepositoryWithUser(user);
		when(commentRepository.findByCommentId(comment.getCommentId())).thenReturn(Optional.of(commentEntity));
		when(commentRepository.save(commentEntity)).thenReturn(commentEntity);

		Comment actual = commentPersistenceAdapter.save(comment);
		if (actual.getUpdatedAt() != null) {
			actual.setUpdatedAt(comment.getUpdatedAt());
		}

		assertEquals(comment, actual);
	}

	@Test
	void testExists() {
		when(commentRepository.existsCommentByCommentId(any(CommentId.class))).thenReturn(true);

		assertThat(commentPersistenceAdapter.existsCommentByCommentId(COMMENT_ID)).isTrue();
	}

	@Test
	void testExistsCommentsCreatedOrUpdatedByUser() {
		// given
		final UserId userId = new UserId(Utils.getRandomString());
		when(commentRepository.existsByCreatedByOrUpdatedBy(userId.getId(), userId.getId())).thenReturn(true);

		// when
		final boolean exists = commentPersistenceAdapter.existsCommentsCreatedOrUpdatedByUser(userId);

		// then
		assertThat(exists).isTrue();
	}

	private void mockNutzerRepositoryWithUser(final User user) {
		final NutzerEntity userEntity = NutzerEntity.builder()
			.gid(user.getGid().getId())
			.email(user.getEmail())
			.name(user.getName())
			.build();
		when(userRepository.findFirstByGid(any())).thenReturn(userEntity);
	}
}
