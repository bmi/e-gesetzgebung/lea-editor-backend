// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import de.itzbund.egesetz.bmi.lea.persistence.entities.TemplateEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Tests for {@link TemplateRepository}.
 */
@ExtendWith(SpringExtension.class)
@DataJpaTest
@SuppressWarnings("unused")
class TemplateRepositoryIntegrationTest {

    @Autowired
    private TemplateRepository templateRepository;

    private static Stream<Arguments> provideTemplateNames() {
        return Stream.of(
            Arguments.of("mantelgesetz-begruendung"),
            Arguments.of("mantelgesetz-regelungstext-0"),
            Arguments.of("stammgesetz-begruendung"),
            Arguments.of("stammgesetz-regelungstext-0"),
            Arguments.of("stammgesetz-regelungstext-1"),
            Arguments.of("stammgesetz-regelungstext-2"),
            Arguments.of("stammgesetz-regelungstext-3"),
            Arguments.of("stammgesetz-vorblatt")
        );
    }

    @ParameterizedTest(name = "{index}: Load template - {0}")
    @MethodSource("provideTemplateNames")
    void loadDocumentTemplates(String templateName) {
        assertNotNull(templateName);
        assertFalse(templateName.isBlank());

        List<TemplateEntity> foundTemplates = templateRepository
            .findAllByTemplateNameOrderByVersionDescUpdatedAtDesc(templateName);
        assertNotNull(foundTemplates);
        assertEquals(1, foundTemplates.size());

        TemplateEntity actualEntity = foundTemplates.get(0);
        assertNotNull(actualEntity);
        assertNotNull(actualEntity.getContent());
        assertFalse(actualEntity.getContent()
            .isBlank());
    }

    @Test
    void testInvalidTemplate() {
        List<TemplateEntity> foundTemplates = templateRepository
            .findAllByTemplateNameOrderByVersionDescUpdatedAtDesc("invalid-template-name");
        assertNotNull(foundTemplates);
        assertEquals(0, foundTemplates.size());
    }

}
