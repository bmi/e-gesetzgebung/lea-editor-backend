// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.lifecycle.Startables;

import java.util.Map;
import java.util.stream.Stream;

@SuppressWarnings("java:S2187")
@ContextConfiguration(initializers = AbstractIntegrationTest.Initializer.class)
public class AbstractIntegrationTest {

    static class Initializer implements
        ApplicationContextInitializer<ConfigurableApplicationContext> {

        private static final MySQLContainer<?> MYSQL_CONTAINER = new MySQLContainer<>("mysql:8.0.24")
            .withDatabaseName("test_database")
            .withUsername("test")
            .withPassword("test");


        private static void startContainers() {
            Startables.deepStart(Stream.of(MYSQL_CONTAINER)).join();
            // we can add further containers
            // here like rabbitmq or other databases
        }


        private static Map<String, String> createConnectionConfiguration() {
            return Map.of(
                "spring.datasource.url", MYSQL_CONTAINER.getJdbcUrl(),
                "spring.datasource.username", MYSQL_CONTAINER.getUsername(),
                "spring.datasource.password", MYSQL_CONTAINER.getPassword()
            );
        }


        @Override
        @SuppressWarnings("unchecked")
        public void initialize(ConfigurableApplicationContext applicationContext) {

            startContainers();

            ConfigurableEnvironment environment =
                applicationContext.getEnvironment();

            MapPropertySource testcontainers = new MapPropertySource("testcontainers",
                (Map) createConnectionConfiguration());

            environment.getPropertySources().addFirst(testcontainers);
        }
    }
}
