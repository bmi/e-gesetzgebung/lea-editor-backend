// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.persistence.entities.CompoundDocumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.BaseMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.CompoundDocumentMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.CompoundDocumentMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.utils.LeageSessionMock;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestEntitiesUtil;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@SuppressWarnings("unused")
class CompoundDocumentRepositoryTest {

	@Autowired
	private CompoundDocumentRepository compoundDocumentRepository;

	private final CompoundDocumentMapperPersistence compoundDocumentMapper = new CompoundDocumentMapperPersistenceImpl(new BaseMapperPersistenceImpl());

	private MockedStatic<LeageSession> mockedLeageSession;

	@BeforeEach
	void setUp() {
		mockedLeageSession = LeageSessionMock.mockLeageSession();
	}

	@AfterEach
	public void tearDown() {
		mockedLeageSession.close();  // Make sure to close the mock to avoid conflict
	}

	@Test
	void testSaveWithNewCompoundDocument() {

		// Arrange
		CompoundDocumentEntity firstCompoundDocument = createNewCompoundDocumentEntityAndPersist(DocumentState.DRAFT);
		Optional<CompoundDocumentEntity> firstCompoundDocumentRetrieved = compoundDocumentRepository.findByCompoundDocumentId(
			firstCompoundDocument.getCompoundDocumentId());

		assertThat(firstCompoundDocumentRetrieved).contains(firstCompoundDocument);

		// Act
		CompoundDocumentDomain secondCompoundDocument = createNewCompoundDocumentDomain(firstCompoundDocument.getCreatedBy());

		compoundDocumentRepository.save(compoundDocumentMapper.fromDomainItem(secondCompoundDocument));
		Optional<CompoundDocumentEntity> secondUserRetrieved = compoundDocumentRepository.findByCompoundDocumentId(
			secondCompoundDocument.getCompoundDocumentId());

		// Assert
		CompoundDocumentEntity compoundDocumentEntity = secondUserRetrieved.orElse(null);

		assertThat(compoundDocumentEntity).isNotNull().hasNoNullFieldsOrPropertiesExcept("inheritFromId");
		assertThat(compoundDocumentEntity.getId()).isNotEqualTo(firstCompoundDocumentRetrieved.get().getId());
	}

	@Test
	void testUpdate() {
		// Arrange
		CompoundDocumentEntity firstCompoundDocumentEntity = createNewCompoundDocumentEntityAndPersist(DocumentState.DRAFT);
		Optional<CompoundDocumentEntity> firstCompoundDocumentRetrieved = compoundDocumentRepository.findByCompoundDocumentId(
			firstCompoundDocumentEntity.getCompoundDocumentId());
		assertThat(firstCompoundDocumentRetrieved).contains(firstCompoundDocumentEntity);

		// Act
		firstCompoundDocumentEntity.setState(DocumentState.BEREIT_FUER_KABINETT);
		compoundDocumentRepository.save(firstCompoundDocumentEntity);
		// Bei einem Fehler wie doppelt speichern, würde hier schon ein Fehler entstehen, weil das Resultset zu groß wäre
		Optional<CompoundDocumentEntity> secondCompoundDocumentRetrieved = compoundDocumentRepository.findByCompoundDocumentId(
			firstCompoundDocumentEntity.getCompoundDocumentId());

		// Assert
		CompoundDocumentEntity compoundDocumentEntity = secondCompoundDocumentRetrieved.orElse(null);
		assertThat(compoundDocumentEntity).isNotNull().hasNoNullFieldsOrPropertiesExcept("inheritFromId");
		assertThat(compoundDocumentEntity.getId()).isEqualTo(firstCompoundDocumentRetrieved.get().getId());
		assertThat(compoundDocumentEntity.getState()).isEqualTo(DocumentState.BEREIT_FUER_KABINETT);
	}

	@Test
	void testFindByRegelungsVorhabenIdAndState() {
		// Arrange
		CompoundDocumentEntity firstCompoundDocument = createNewCompoundDocumentEntityAndPersist(
			DocumentState.BEREIT_FUER_KABINETT);
		Optional<CompoundDocumentEntity> firstCompoundDocumentRetrieved =
			compoundDocumentRepository.findByCompoundDocumentId(
				firstCompoundDocument.getCompoundDocumentId());
		assertThat(firstCompoundDocumentRetrieved).isPresent();

		CompoundDocumentEntity compoundDocumentEntity1 = firstCompoundDocumentRetrieved.get();
		assertThat(compoundDocumentEntity1).isEqualTo(firstCompoundDocument);

		RegelungsVorhabenId regelungsVorhabenId = compoundDocumentEntity1.getRegelungsVorhabenId();

		// Act
		List<CompoundDocumentEntity> actual = compoundDocumentRepository.findByRegelungsVorhabenIdAndState(
			regelungsVorhabenId, DocumentState.BEREIT_FUER_KABINETT);

		// Assert
		assertThat(actual).hasSize(1);

		CompoundDocumentEntity compoundDocumentEntity = actual.get(0);
		assertThat(compoundDocumentEntity).isNotNull()
			.hasNoNullFieldsOrPropertiesExcept("inheritFromId");
		assertThat(compoundDocumentEntity.getState()).isEqualTo(DocumentState.BEREIT_FUER_KABINETT);
	}

	@Test
	void testExistsByCreatedByOrUpdatedBy() {
		// given
		final CompoundDocumentEntity compoundDocument = createNewCompoundDocumentEntityAndPersist(DocumentState.DRAFT);

		final UserId owner = new UserId(compoundDocument.getCreatedBy());
		final UserId otherUser = new UserId(TestObjectsUtil.getRandomString());

		// when
		final boolean existsByOwner = compoundDocumentRepository.existsByCreatedByOrUpdatedBy(owner.getId(), owner.getId());
		final boolean existsByOtherUser = compoundDocumentRepository.existsByCreatedByOrUpdatedBy(otherUser.getId(), otherUser.getId());

		// then
		assertThat(existsByOwner).isTrue();
		assertThat(existsByOtherUser).isFalse();
	}

	// ============================================== Auxiliary methods ==============================================

	private CompoundDocumentEntity createNewCompoundDocumentEntityAndPersist(DocumentState compoundDocumentState) {
		CompoundDocumentEntity randomCompoundDocumentEntity = TestEntitiesUtil.getRandomCompoundDocumentEntity();

		if (compoundDocumentState != null) {
			randomCompoundDocumentEntity.setState(compoundDocumentState);
		}

		return compoundDocumentRepository.save(randomCompoundDocumentEntity);
	}

	private CompoundDocumentDomain createNewCompoundDocumentDomain(String userGid) {
		CompoundDocumentDomain compoundDocumentEntity = TestObjectsUtil.getCompoundDocumentEntity();

		compoundDocumentEntity.setCreatedBy(User.builder().gid(new UserId(userGid)).build());
		compoundDocumentEntity.setUpdatedBy(User.builder().gid(new UserId(userGid)).build());

		return compoundDocumentEntity;
	}
}
