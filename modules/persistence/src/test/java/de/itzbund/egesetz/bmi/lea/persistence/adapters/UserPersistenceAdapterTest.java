// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.persistence.UserRepository;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.user.repositories.NutzerEntityRepository;
import de.itzbund.egesetz.bmi.user.repositories.NutzerIds;
import lombok.Value;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DataJpaTest
@Import({UserPersistenceAdapter.class, UserRepository.class, UserMapperPersistenceImpl.class})
class UserPersistenceAdapterTest {

	@Autowired
	private UserPersistenceAdapter userPersistenceAdapter;

	@MockBean
	private NutzerEntityRepository nutzerEntityRepository;

	@Test
	void testMarkUserAsNotReferenced() {
		// given
		final UserId userId = new UserId(Utils.getRandomString());

		// when
		userPersistenceAdapter.markUserAsNotReferenced(userId);

		// then
		verify(nutzerEntityRepository, times(1)).setEditorNoRef(userId.getId());
	}

	@Test
	void testFindDeletedNutzerWithoutStellvertreterOrZuordnung() {
		// given
		final List<NutzerIds> nutzerIds = List.of(
			new NutzerIdsImpl(null, Utils.getRandomString()),
			new NutzerIdsImpl(null, Utils.getRandomString()),
			new NutzerIdsImpl(null, Utils.getRandomString())
		);
		final List<UserId> expected = nutzerIds.stream()
			.map(ids -> new UserId(ids.getGid()))
			.collect(Collectors.toList());
		when(nutzerEntityRepository.findDeletedNutzerWithoutStellvertreterOrZuordnung()).thenReturn(nutzerIds);

		// when
		final List<UserId> actual = userPersistenceAdapter.findMarkedAsDeleted();

		// then
		assertThat(actual).containsExactlyInAnyOrderElementsOf(expected);
	}

	@Value
	private static class NutzerIdsImpl implements NutzerIds {

		UUID id;
		String gid;
	}
}
