// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.ReplyId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.persistence.UserRepository;
import de.itzbund.egesetz.bmi.lea.persistence.entities.ReplyEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.BaseMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.ReplyMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.ReplyMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistenceImpl;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.ReplyRepository;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestEntitiesUtil;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!! In H2 MEMORY MODE, the scripts schema.sql and data.sql !!!!!!!!!!!!
// !!!!!!!!!! are automatically applied !!!                          !!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@ExtendWith(MockitoExtension.class)
@SuppressWarnings("unused")
class ReplyPersistenceAdapterTest {

	@Spy
	private final ReplyMapperPersistence replyMapper = new ReplyMapperPersistenceImpl(new BaseMapperPersistenceImpl());
	@Spy
	private final UserMapperPersistence userMapper = new UserMapperPersistenceImpl();

	@InjectMocks
	private ReplyPersistenceAdapter replyPersistenceAdapter;

	@Mock
	private ReplyRepository replyRepository;
	@Mock
	private UserRepository userRepository;

	@Test
	void testSaveWithNewCompoundDocument() {
		User user = TestObjectsUtil.getUserEntity();
		Reply reply = TestObjectsUtil.getRandomReply(user);
		NutzerEntity userEntity = TestEntitiesUtil.getRandomNutzerEntity();
		when(userRepository.findFirstByGid(user.getGid())).thenReturn(userEntity);

		ReplyEntity replyEntity = replyMapper.fromDomainItem(reply);
		when(replyRepository.findByReplyId(reply.getReplyId())).thenReturn(Optional.of(replyEntity));
		when(replyRepository.save(replyEntity)).thenReturn(replyEntity);

		Reply actual = replyPersistenceAdapter.save(reply);
		if (actual.getUpdatedAt() != null) {
			actual.setUpdatedAt(reply.getUpdatedAt());
		}

		assertThat(actual).isEqualTo(reply);
	}

	@Test
	void testFindByReplyId() {
		// given
		final ReplyEntity replyEntity = TestEntitiesUtil.getRandomReplyEntity();
		when(replyRepository.findByReplyId(replyEntity.getReplyId())).thenReturn(Optional.of(replyEntity));

		// when
		final Optional<Reply> reply = replyPersistenceAdapter.findByReplyId(replyEntity.getReplyId());

		// then
		assertThat(reply).get().hasFieldOrPropertyWithValue("replyId", replyEntity.getReplyId());
	}

	@Test
	void testFindByReplyIdAndDeletedFalse() {
		// given
		final ReplyEntity replyEntity = TestEntitiesUtil.getRandomReplyEntity();
		when(replyRepository.findByReplyIdAndDeletedFalse(replyEntity.getReplyId())).thenReturn(Optional.of(replyEntity));

		// when
		final Optional<Reply> reply = replyPersistenceAdapter.findByReplyIdAndDeletedFalse(replyEntity.getReplyId());

		// then
		assertThat(reply).get().hasFieldOrPropertyWithValue("replyId", replyEntity.getReplyId());
	}

	@Test
	void testFindByCommentIdAndDeleted() {
		// given
		final ReplyEntity replyEntity = TestEntitiesUtil.getRandomReplyEntity();
		when(replyRepository.findByCommentIdAndDeleted(replyEntity.getCommentId(), false)).thenReturn(singletonList(replyEntity));

		final ReplyEntity deletedReplyEntity = TestEntitiesUtil.getRandomReplyEntity();
		deletedReplyEntity.setDeleted(true);
		when(replyRepository.findByCommentIdAndDeleted(deletedReplyEntity.getCommentId(), true)).thenReturn(singletonList(deletedReplyEntity));

		// when
		final List<Reply> replies = replyPersistenceAdapter.findByCommentIdAndDeleted(replyEntity.getCommentId(), false);
		final List<Reply> deletedReplies = replyPersistenceAdapter.findByCommentIdAndDeleted(deletedReplyEntity.getCommentId(), true);

		// then
		assertThat(replies).satisfiesExactlyInAnyOrder(reply ->
			assertThat(reply.getReplyId()).isEqualTo(replyEntity.getReplyId()));
		assertThat(deletedReplies).satisfiesExactlyInAnyOrder(reply ->
			assertThat(reply.getReplyId()).isEqualTo(deletedReplyEntity.getReplyId()));
	}

	@Test
	void testFindByParentIdAndDeletedFalse() {
		// given
		final ReplyEntity replyEntity = TestEntitiesUtil.getRandomReplyEntity();
		final String replyId = replyEntity.getReplyId().getId().toString();
		when(replyRepository.findByParentIdAndDeletedFalse(replyId)).thenReturn(Optional.ofNullable(replyEntity));

		// when
		final Optional<Reply> actual = replyPersistenceAdapter.findByParentIdAndDeletedFalse(replyId);

		// then
		assertThat(actual).get()
			.hasFieldOrPropertyWithValue("replyId", replyEntity.getReplyId())
			.hasFieldOrPropertyWithValue("commentId", replyEntity.getCommentId())
			.hasFieldOrPropertyWithValue("parentId", replyEntity.getParentId())
			.hasFieldOrPropertyWithValue("documentId", replyEntity.getDocumentId());
	}

	@Test
	void testExistsReplyByReplyId() {
		// given
		final ReplyId replyId = new ReplyId(UUID.randomUUID());
		final ReplyId notExistingReplyId = new ReplyId(UUID.randomUUID());
		when(replyRepository.existsReplyByReplyId(replyId)).thenReturn(true);

		// when
		final boolean exists = replyPersistenceAdapter.existsReplyByReplyId(replyId);
		final boolean notExisting = replyPersistenceAdapter.existsReplyByReplyId(notExistingReplyId);

		// then
		assertThat(exists).isTrue();
		assertThat(notExisting).isFalse();
	}

	@Test
	void whenReplyCreatedByUser_thenExistsReplyReturnsTrue() {
		// given
		final UserId owner = new UserId(Utils.getRandomString());
		final UserId other = new UserId(Utils.getRandomString());

		when(replyRepository.existsByCreatedByOrUpdatedBy(owner.getId(), owner.getId())).thenReturn(true);

		// when
		final boolean ownerExists = replyPersistenceAdapter.existsRepliesCreatedOrUpdatedByUser(owner);
		final boolean otherExists = replyPersistenceAdapter.existsRepliesCreatedOrUpdatedByUser(other);

		// then
		assertThat(ownerExists).isTrue();
		assertThat(otherExists).isFalse();
	}
}
