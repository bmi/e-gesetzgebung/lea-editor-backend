// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.enums.PropositionState;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentInitiant;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.PropositionEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test suite for {@link PropositionRepository}.
 */
@DataJpaTest
@SuppressWarnings("unused")
class PropositionRepositoryIntegrationTest {

    @Autowired
    private PropositionRepository propositionRepository;


    @Test
    void testStoreProposition() {
        assertThat(propositionRepository).isNotNull();
        assertEquals(0, propositionRepository.count());

        UUID propositionId = UUID.randomUUID();
        String title = "Test proposition 001";
        PropositionState propositionState = PropositionState.ENTWURF;
        RechtsetzungsdokumentInitiant initiator = RechtsetzungsdokumentInitiant.BUNDESREGIERUNG;
        RechtsetzungsdokumentTyp rechtsetzungsdokumentTyp = RechtsetzungsdokumentTyp.GESETZ;

        String proponentId = UUID.randomUUID().toString();
        String proponentTitle = "Test proponent 001-1";
        String designationNominative = "Bundesministerium";

        PropositionEntity propositionEntity = PropositionEntity.builder()
            .propositionId(new RegelungsVorhabenId(propositionId))
            .title(title)
            .state(propositionState)
            .proponentId(proponentId)
            .proponentTitle(proponentTitle)
            .proponentDesignationNominative(designationNominative)
            .proponentActive(true)
            .initiant(initiator)
            .type(rechtsetzungsdokumentTyp)
            .createdAt(Instant.now())
            .build();

        propositionEntity = propositionRepository.save(propositionEntity);
        assertEquals(1, propositionRepository.count());
        assertNotNull(propositionEntity.getCreatedAt());

        String text = String.format("PropositionEntity(propositionId=RegelungsVorhabenId"
                + "(id=%s), title=%s, shortTitle=null, abbreviation=null, createdAt=%s)",
            propositionId, title, propositionEntity.getCreatedAt());
        assertEquals(text, propositionEntity.toString());

        String shortTitle = "TEST";
        String abbreviation = "TP-001";
        String designationGenitive = designationNominative + "s";

        propositionEntity.setShortTitle(shortTitle);
        propositionEntity.setAbbreviation(abbreviation);
        propositionEntity.setProponentDesignationGenitive(designationGenitive);
        propositionEntity = propositionRepository.save(propositionEntity);

        assertEquals(1, propositionRepository.count());
        assertEquals(propositionId, propositionEntity.getPropositionId().getId());
        assertEquals(title, propositionEntity.getTitle());
        assertEquals(shortTitle, propositionEntity.getShortTitle());
        assertEquals(abbreviation, propositionEntity.getAbbreviation());
        assertEquals(propositionState, propositionEntity.getState());
        assertEquals(proponentId, propositionEntity.getProponentId());
        assertEquals(proponentTitle, propositionEntity.getProponentTitle());
        assertEquals(designationNominative, propositionEntity.getProponentDesignationNominative());
        assertEquals(designationGenitive, propositionEntity.getProponentDesignationGenitive());
        assertTrue(propositionEntity.isProponentActive());
        assertEquals(initiator, propositionEntity.getInitiant());
        assertEquals(rechtsetzungsdokumentTyp, propositionEntity.getType());
    }


    @Test
    void test_loadProposition() {
        assertThat(propositionRepository).isNotNull();
        assertEquals(0, propositionRepository.count());

        UUID propositionId = UUID.randomUUID();

        PropositionEntity propositionEntity = propositionRepository.save(PropositionEntity.builder()
            .propositionId(new RegelungsVorhabenId(propositionId))
            .title(Utils.getRandomString())
            .state(PropositionState.ARCHIVIERT)
            .proponentId(UUID.randomUUID().toString())
            .proponentTitle(Utils.getRandomString())
            .proponentDesignationNominative(Utils.getRandomString())
            .proponentActive(true)
            .initiant(RechtsetzungsdokumentInitiant.BUNDESREGIERUNG)
            .type(RechtsetzungsdokumentTyp.VERWALTUNGSVORSCHRIFT)
            .createdAt(Instant.now())
            .build());

        assertEquals(1, propositionRepository.count());

        List<PropositionEntity> propositionEntityList =
            propositionRepository.findByPropositionId(new RegelungsVorhabenId(propositionId));
        PropositionEntity propositionEntity1 = propositionEntityList.get(0);
        assertThat(propositionEntity1).isNotNull();
        assertEquals(propositionEntity, propositionEntity1);
    }

}
