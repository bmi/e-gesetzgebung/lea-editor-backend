/*
 * SPDX-License-Identifier: MPL-2.0
 *
 * Copyright (C) 2021-2023 Bundesministerium des Innern und für Heimat, \
 * Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
 */
package de.itzbund.egesetz.bmi.lea.persistence.entities.listeners;

import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.persistence.entities.CommentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.CompoundDocumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DocumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.EgfaDataEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.EgfaMetadataEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.ReplyEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.UserSettingsEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

public class CreateOrUpdateEntityListenerTest {

    private CreatedOrUpdatedEntityListener listener;

    private MockedStatic<LeageSession> mockedLeageSession;

    @BeforeEach
    void setUp() {
        OidcUser mockOidcUser = mock(OidcUser.class);
        Authentication authenticationMock = mock(OAuth2AuthenticationToken.class);
        SecurityContext context = SecurityContextHolder.getContext();
        context.setAuthentication(authenticationMock);

        mockedLeageSession = mockStatic(LeageSession.class);
        Map<String, String> mockUserDetails = new HashMap<>();
        mockUserDetails.put(LeageSession.GID, "testUserId");
        when(LeageSession.getUserDatailsFromAuthentication()).thenReturn(mockUserDetails);

        listener = new CreatedOrUpdatedEntityListener();

        when(authenticationMock.getPrincipal()).thenReturn(mockOidcUser);
        when(mockOidcUser.getAttributes()).thenReturn(Map.of("gid", "someUserGidValue"));
    }

    @AfterEach
    public void tearDown() {
        mockedLeageSession.close();  // Make sure to close the mock to avoid conflict
    }

    @Test
    void testEgfaMetaDataCreated() {
        EgfaMetadataEntity entity = new EgfaMetadataEntity();
        listener.egfaMetaDataCreated(entity);
        assertNotNull(entity.getCreatedAt());
        assertNotNull(entity.getCreatedByUser());
    }

    @Test
    void testEgfaDataCreated() {
        EgfaDataEntity entity = new EgfaDataEntity();
        listener.egfaDataCreated(entity);
        assertNotNull(entity.getCreatedAt());
        assertNotNull(entity.getTransmittedByUser());
    }

    @Test
    void testUserSettingsCreated() {
        UserSettingsEntity entity = new UserSettingsEntity();
        listener.userSettingsCreated(entity);
        assertNotNull(entity.getCreatedAt());
    }

    @Test
    void testReplyCreated() {
        ReplyEntity entity = new ReplyEntity();
        listener.replyCreated(entity);
        assertNotNull(entity.getCreatedAt());
        assertNotNull(entity.getCreatedBy());
    }

    @Test
    void testCommentCreated() {
        CommentEntity entity = new CommentEntity();
        listener.commentCreated(entity);
        assertNotNull(entity.getCreatedAt());
        assertNotNull(entity.getCreatedBy());
    }

    @Test
    void testDocumentCreated() {
        DocumentEntity entity = new DocumentEntity();
        listener.documentCreated(entity);
        assertNotNull(entity.getCreatedAt());
        assertNotNull(entity.getCreatedBy());
    }

    @Test
    void testCompoundDocumentCreated() {
        CompoundDocumentEntity entity = new CompoundDocumentEntity();
        listener.compoundDocumentCreated(entity);
        assertNotNull(entity.getCreatedAt());
        assertNotNull(entity.getCreatedBy());
    }

    @Test
    void testEgfaMetaDataUpdated() {
        EgfaMetadataEntity entity = new EgfaMetadataEntity();
        listener.egfaMetaDataUpdated(entity);
        // Ensure that nothing is modified
        assertNull(entity.getCreatedAt());
        assertNull(entity.getCreatedByUser());
    }

    @Test
    void testEgfaDataUpdated() {
        EgfaDataEntity entity = new EgfaDataEntity();
        listener.egfaDataUpdated(entity);
        assertNotNull(entity.getCompletedAt());
    }

    @Test
    void testUserSettingsUpdated() {
        UserSettingsEntity entity = new UserSettingsEntity();
        listener.userSettingsUpdated(entity);
        assertNotNull(entity.getUpdatedAt());
    }

    @Test
    void testReplyUpdated() {
        ReplyEntity entity = new ReplyEntity();
        listener.replyUpdated(entity);
        assertNotNull(entity.getUpdatedAt());
        assertNotNull(entity.getUpdatedBy());
    }

    @Test
    void testCommentUpdated() {
        CommentEntity entity = new CommentEntity();
        listener.commentUpdated(entity);
        assertNotNull(entity.getUpdatedAt());
        assertNotNull(entity.getUpdatedBy());
    }

    @Test
    void testDocumentUpdated() {
        DocumentEntity entity = new DocumentEntity();
        listener.documentUpdated(entity);
        assertNotNull(entity.getUpdatedAt());
        assertNotNull(entity.getUpdatedBy());
    }

    @Test
    void testCompoundDocumentUpdated() {
        CompoundDocumentEntity entity = new CompoundDocumentEntity();
        listener.compoundDocumentUpdated(entity);
        assertNotNull(entity.getUpdatedAt());
        assertNotNull(entity.getUpdatedBy());
    }

    @Test
    void testSetCreated() {
        CompoundDocumentEntity compoundEntity = new CompoundDocumentEntity();
        DocumentEntity documentEntity = new DocumentEntity();
        CommentEntity commentEntity = new CommentEntity();
        ReplyEntity replyEntity = new ReplyEntity();
        UserSettingsEntity userSettingsEntity = new UserSettingsEntity();
        EgfaDataEntity egfaDataEntity = new EgfaDataEntity();
        EgfaMetadataEntity egfaMetadataEntity = new EgfaMetadataEntity();

        listener.setCreated(compoundEntity);
        assertNotNull(compoundEntity.getCreatedAt());
        assertNotNull(compoundEntity.getCreatedBy());

        listener.setCreated(documentEntity);
        assertNotNull(documentEntity.getCreatedAt());
        assertNotNull(documentEntity.getCreatedBy());

        listener.setCreated(commentEntity);
        assertNotNull(commentEntity.getCreatedAt());
        assertNotNull(commentEntity.getCreatedBy());

        listener.setCreated(replyEntity);
        assertNotNull(replyEntity.getCreatedAt());
        assertNotNull(replyEntity.getCreatedBy());

        listener.setCreated(userSettingsEntity);
        assertNotNull(userSettingsEntity.getCreatedAt());

        listener.setCreated(egfaDataEntity);
        assertNotNull(egfaDataEntity.getCreatedAt());
        assertNotNull(egfaDataEntity.getTransmittedByUser());

        listener.setCreated(egfaMetadataEntity);
        assertNotNull(egfaMetadataEntity.getCreatedAt());
        assertNotNull(egfaMetadataEntity.getCreatedByUser());
    }

    @Test
    void testSetModified() {
        CompoundDocumentEntity compoundEntity = new CompoundDocumentEntity();
        DocumentEntity documentEntity = new DocumentEntity();
        CommentEntity commentEntity = new CommentEntity();
        ReplyEntity replyEntity = new ReplyEntity();
        UserSettingsEntity userSettingsEntity = new UserSettingsEntity();
        EgfaDataEntity egfaDataEntity = new EgfaDataEntity();
        EgfaMetadataEntity egfaMetadataEntity = new EgfaMetadataEntity();

        listener.setModified(compoundEntity);
        assertNotNull(compoundEntity.getUpdatedAt());
        assertNotNull(compoundEntity.getUpdatedBy());

        listener.setModified(documentEntity);
        assertNotNull(documentEntity.getUpdatedAt());
        assertNotNull(documentEntity.getUpdatedBy());

        listener.setModified(commentEntity);
        assertNotNull(commentEntity.getUpdatedAt());
        assertNotNull(commentEntity.getUpdatedBy());

        listener.setModified(replyEntity);
        assertNotNull(replyEntity.getUpdatedAt());
        assertNotNull(replyEntity.getUpdatedBy());

        listener.setModified(userSettingsEntity);
        assertNotNull(userSettingsEntity.getUpdatedAt());

        listener.setModified(egfaDataEntity);
        assertNotNull(egfaDataEntity.getCompletedAt());

        listener.setModified(egfaMetadataEntity);
        // Ensure that nothing is modified
        assertNull(egfaMetadataEntity.getCreatedAt());
        assertNull(egfaMetadataEntity.getCreatedByUser());
    }

}



