// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.mappers;

import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.persistence.entities.ReplyEntity;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestObjectsUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ReplyMapperPersistenceTest {

    private ReplyMapperPersistence replyMapper;

    @BeforeEach
    void init() {
        replyMapper = new ReplyMapperPersistenceImpl(new BaseMapperPersistenceImpl());
    }

    @Test
    void testMapping_Reply_to_ReplyEntity_NoId() {
        Reply domainItem = TestObjectsUtil.getRandomReply();

        ReplyEntity replyEntity = replyMapper.fromDomainItem(domainItem);

        assertNotNull(replyEntity);
        assertThat(replyEntity).hasNoNullFieldsOrPropertiesExcept("id");
    }

}
