// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.enums.CommentState;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.persistence.entities.CommentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.PositionEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.UserEntity;
import de.itzbund.egesetz.bmi.lea.persistence.utils.LeageSessionMock;
import de.itzbund.egesetz.bmi.lea.persistence.utils.TestEntitiesUtil;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@SuppressWarnings("unused")
class CommentRepositoryTest {

	@Autowired
	private CommentRepository commentRepository;

	private MockedStatic<LeageSession> mockedLeageSession;

	@BeforeEach
	void setUp() {
		mockedLeageSession = LeageSessionMock.mockLeageSession();
	}

	@AfterEach
	public void tearDown() {
		mockedLeageSession.close();  // Make sure to close the mock to avoid conflict
	}

	@Test
	void whenSaved_thenFindById() {
		// Arrange
		UserEntity owner = createNewUser();
		CommentEntity commentIn = saveNewCommentEntity(null, null, owner, false);

		// Act
		Optional<CommentEntity> optionalCommentEntity = commentRepository.findById(commentIn.getId());

		// Assert
		assertTrue(optionalCommentEntity.isPresent());

		CommentEntity commentOut = optionalCommentEntity.get();
		assertEquals(CommentState.OPEN, commentOut.getState());
		assertNotNull(commentOut.getCreatedAt());
		assertNotNull(commentOut.getUpdatedAt());
		assertFalse(commentOut.isDeleted());
	}


	@Test
	void whenNoCommentForCommentId_thenReturnNull() {
		assertThat(commentRepository.findByCommentId(new CommentId(UUID.randomUUID()))).isEmpty();
	}


	@Test
	void whenSaved_thenFindByCommentId() {
		// Arrange
		UserEntity owner = createNewUser();
		CommentId commentId = new CommentId(UUID.randomUUID());
		CommentEntity commentIn = saveNewCommentEntity(commentId, null, owner, false);

		// Act
		Optional<CommentEntity> commentOut = commentRepository.findByCommentId(commentId);

		// Assert
		assertThat(commentOut).isPresent();
	}


	@Test
	void whenDocumentHasNoComment_thenReturnEmptyList() {
		// Act
		List<CommentEntity> commentEntities = commentRepository.findByDocumentId(new DocumentId(UUID.randomUUID()));

		// Assert
		assertNotNull(commentEntities);
		assertTrue(commentEntities.isEmpty());
	}


	@Test
	void whenDocumentHasOneComment_thenReturnIt() {
		// Arrange
		UserEntity owner = createNewUser();
		DocumentId documentId = new DocumentId(UUID.randomUUID());
		saveNewCommentEntity(null, documentId, owner, false);

		// Act
		List<CommentEntity> commentEntities = commentRepository.findByDocumentId(documentId);

		// Assert
		assertNotNull(commentEntities);
		assertEquals(1, commentEntities.size());
	}


	@RepeatedTest(7)
	void whenDocumentHasSomeComments_thenReturnAllOfThem() {
		// Arrange
		UserEntity owner = createNewUser();
		DocumentId documentId = new DocumentId(UUID.randomUUID());
		int count = ThreadLocalRandom.current().nextInt(10) + 2;

		IntStream.range(0, count).forEach(n -> saveNewCommentEntity(null, documentId, owner, false));

		// Act
		List<CommentEntity> commentEntities = commentRepository.findByDocumentId(documentId);

		// Assert
		assertNotNull(commentEntities);
		assertEquals(count, commentEntities.size());
	}


	@Test
	void whenDocumentHasSomeCommentsOneCommentDeleted_thenReturnOnlyNonDeleted() {
		// Arrange
		UserEntity owner = createNewUser();
		DocumentId documentId = new DocumentId(UUID.randomUUID());
		saveNewCommentEntity(null, documentId, owner, false);
		saveNewCommentEntity(null, documentId, owner, true);
		saveNewCommentEntity(null, documentId, owner, false);

		// Act
		List<CommentEntity> commentEntities = commentRepository.findByDocumentIdAndDeleted(documentId, false);

		// Assert
		assertNotNull(commentEntities);
		assertEquals(2, commentEntities.size());
	}


	@Test
	void whenDocumentHasSomeCommentsOneByCurrentUser_thenReturnThatOne() {
		// Arrange
		UserEntity owner = createNewUser();
		UserEntity foreigner = createNewUser();
		DocumentId documentId = new DocumentId(UUID.randomUUID());
		saveNewCommentEntity(null, documentId, owner, false);
		saveNewCommentEntity(null, documentId, foreigner, false);
		saveNewCommentEntity(null, documentId, foreigner, false);

		// Act
		List<CommentEntity> commentEntities = commentRepository.findByDocumentIdAndCreatedBy(documentId, owner.getGid().getId());

		// Assert
		assertNotNull(commentEntities);
		assertEquals(1, commentEntities.size());
	}


	@Test
	void whenDocumentHasSomeCommentsTwoByOtherUser_thenReturnThoseTwo() {
		// Arrange
		UserEntity owner = createNewUser();
		UserEntity foreigner = createNewUser();
		DocumentId documentId = new DocumentId(UUID.randomUUID());
		saveNewCommentEntity(null, documentId, owner, false);
		saveNewCommentEntity(null, documentId, foreigner, false);
		saveNewCommentEntity(null, documentId, foreigner, false);

		// Act
		List<CommentEntity> commentEntities = commentRepository.findByDocumentIdAndCreatedBy(documentId, foreigner.getGid().getId());

		// Assert
		assertNotNull(commentEntities);
		assertEquals(2, commentEntities.size());
	}


	@Test
	void whenDocumentHasSomeCommentsTwoByOtherUserOneOfThatDeleted_thenReturnOneCommentEach() {
		// Arrange
		UserEntity owner = createNewUser();
		UserEntity foreigner = createNewUser();
		DocumentId documentId = new DocumentId(UUID.randomUUID());
		saveNewCommentEntity(null, documentId, owner, false);
		saveNewCommentEntity(null, documentId, foreigner, true);
		saveNewCommentEntity(null, documentId, foreigner, false);

		// Act
		List<CommentEntity> commentEntities1 =
			commentRepository.findByDocumentIdAndCreatedByAndDeleted(documentId, owner.getGid().getId(), false);
		List<CommentEntity> commentEntities2 =
			commentRepository.findByDocumentIdAndCreatedByAndDeleted(documentId, foreigner.getGid().getId(), false);

		// Assert
		assertNotNull(commentEntities1);
		assertEquals(1, commentEntities1.size());
		assertNotNull(commentEntities2);
		assertEquals(1, commentEntities2.size());
	}


	@Test
	void whenSearchedCommentExists_thenReturnTrue() {
		// Arrange
		UserEntity owner = createNewUser();
		CommentId commentId = new CommentId(UUID.randomUUID());
		saveNewCommentEntity(commentId, null, owner, false);

		// Act and Assert
		assertTrue(commentRepository.existsCommentByCommentId(commentId));
	}


	@Test
	void whenSearchedCommentNotExists_thenReturnFalse() {
		assertFalse(commentRepository.existsCommentByCommentId(new CommentId(UUID.randomUUID())));
	}


	@Test
	void whenSearchedCommentCreatedByUser_thenReturnTrue() {
		// Arrange
		UserEntity owner = createNewUser();
		CommentId commentId = new CommentId(UUID.randomUUID());
		saveNewCommentEntity(commentId, null, owner, false);

		UserEntity anotherUser = createNewUser();

		// Act and Assert
		assertTrue(commentRepository.existsByCreatedByOrUpdatedBy(owner.getGid().getId(), owner.getGid().getId()));
		assertFalse(commentRepository.existsByCreatedByOrUpdatedBy(anotherUser.getGid().getId(), anotherUser.getGid().getId()));
	}

	// ============================================== Auxiliary methods ==============================================

	private CommentEntity saveNewCommentEntity(CommentId commentId, DocumentId documentId, UserEntity owner,
		boolean isDeleted) {

		CommentEntity randomCommentEntity = TestEntitiesUtil.getRandomCommentEntity();

		return commentRepository.save(CommentEntity.builder()
			.commentId(commentId != null ? commentId : new CommentId(UUID.randomUUID()))
			.documentId(documentId != null ? documentId : new DocumentId(UUID.randomUUID()))
			.content(Utils.getRandomString())
			.createdBy(owner.getGid().getId())
			.updatedBy(owner.getGid().getId())
			.anchor(PositionEntity.builder().build())
			.focus(PositionEntity.builder().build())
			.deleted(isDeleted)
			.createdAt(Instant.now())
			.updatedAt(Instant.now())
			.build());
	}

	private UserEntity createNewUser() {
		return UserEntity.builder().gid(new UserId(Utils.getRandomString())).build();
	}

}
