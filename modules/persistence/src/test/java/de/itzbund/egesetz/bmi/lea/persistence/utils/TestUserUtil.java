// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.utils;

public class TestUserUtil {

    public static final String USER_ID = "randomGid";
    public static final String USER_NAME = "TestVorname Testnachmane";
    public static final String USER_EMAIL = "Test.Nonexisting1@email.de";

}
