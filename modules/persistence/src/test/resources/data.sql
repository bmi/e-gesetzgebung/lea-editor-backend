-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO PUBLIC."user" (email, gid, name) VALUES ('test@example.com', 'someGid', 'someName');

INSERT INTO `comment_position` (`displacement`, `path`) VALUES (7, '[0, 0, 2, 0, 2, 2, 0, 0, 0]');
INSERT INTO `comment_position` (`displacement`, `path`) VALUES (13, '[0, 0, 2, 0, 2, 2, 0, 0, 0]');

INSERT INTO `comment` (`comment_id`, `content`, `state`, `document_id`, `focus_id`, `anchor_id`, `created_by_id`, `updated_by_id`, `created_at`, `updated_at`, `deleted`)
   VALUES ('00000000-0000-c000-0000-000000000001', 'Kommentar 1', 'OPEN', '00000000-0000-0000-d0c0-000000000001', 2, 1,
     'someGid',
     'someGid',
     '2022-12-06 08:55:47.459484', '2022-12-06 08:55:47.459484', 0);


INSERT INTO `reply` (`reply_id`, `comment_id`, `parent_id`, `content`, `document_id`, `created_by_id`, `updated_by_id`, `created_at`, `updated_at`, `deleted`)
  VALUES ('00000000-0301-0000-0000-000000000001', '00000000-0000-c000-0000-000000000001', '00000000-0000-c000-0000-000000000001', 'Reply to Kommentar 1', 'b400b72b-1fe8-4290-bee0-24fcb16e425e',
    'someGid',
    'someGid',
    '2022-12-06 09:01:11.935930', '2022-12-13 23:50:18.722977', 1);


INSERT INTO `compound_document` (`id`, `compound_document_id`, `regelungs_vorhaben_id`, `created_at`, `title`, `type`, `updated_at`, `created_by_id`, `updated_by_id`, `version`, `inherit_from_id`, `state`) VALUES
	(100, '00000000-0000-0000-00cd-000000000001', '00000000-0000-0000-0000-000000000005', '2022-04-08 13:38:56.266000', 'Eine Mappe voller Dokumente', 'STAMMGESETZ', '2022-04-08 13:38:56.266000',
	   'someGid',
	   'someGid',
	    '1', null, 'DRAFT'),
	(101, '00000000-0000-0000-00cd-000000000002', 'f78f5f90-803d-4150-8733-107b990fefdb', '2022-04-28 12:35:57.715739', 'Eine zweite Mappe', 'STAMMGESETZ', '2022-04-28 12:35:57.715739',
	   'someGid',
	   'someGid',
	    '1', null, 'FINAL'),
    (102, '00000000-0000-0000-00cd-000000000003', '00000000-0000-0000-0000-000000000005', '2022-04-28 12:35:57.715739', 'Eine zweite Version der ersten Mappe', 'STAMMGESETZ', '2022-04-28 12:35:57.715739',
       'someGid',
       'someGid',
        '2', '42b77fdd-5f73-404c-8c22-a5168757affa', 'BEREIT_FUER_KABINETT');
