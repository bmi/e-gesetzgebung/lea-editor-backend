-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO `user` (`user_id`, `email`, `gid`, `name`) VALUES ('891e2276-93fb-4d2e-90a6-6293c28fa188', 'test@example.com', 'someGid', 'someName');

INSERT INTO `point` (`id`, `offset`) VALUES ('a035dcff-8137-4764-831a-b222b57575f7', 456);

INSERT INTO `point_entity_path` (`point_entity_id`, `path`) VALUES ('a035dcff-8137-4764-831a-b222b57575f7', 789);

INSERT INTO `document` (`id`, `created_date`, `content`, `doc_type`, `proposition_id`, `state`, `title`, `updated_date`, `version`, `originator`, `update_author`) VALUES ('d6c95ccb-5f27-4493-97df-fefcaf8e02ba', '2022-04-21 14:42:59.000000', 'content', 'doctype', 'prop_id', 'state', 'title', '2022-04-21 14:42:38.000000', NULL, '891e2276-93fb-4d2e-90a6-6293c28fa188', '891e2276-93fb-4d2e-90a6-6293c28fa188');

INSERT INTO `comment` (`id`, `content`, `created_date`, `updated_date`, `fk_originator`, `anchor_id`, `document`, `focus_id`) VALUES ('07c723f1-377a-4882-960e-c873c998591a', 'content', '2022-04-21 15:12:19.000000', '2022-04-21 15:12:21.000000', '891e2276-93fb-4d2e-90a6-6293c28fa188', 'a035dcff-8137-4764-831a-b222b57575f7', 'd6c95ccb-5f27-4493-97df-fefcaf8e02ba', 'a035dcff-8137-4764-831a-b222b57575f7');

INSERT INTO `media_data` (`media_data_id`, `media_data`) VALUES ('445b24c8-db12-41d7-a11e-eb77f0802a1c', 'djsdjasdasjdoias');

INSERT INTO `media_metadata` (`media_metadata_id`, `created_date`, `media_type`, `name`, `media_data`) VALUES ('d0a6c47a-9179-43aa-8d7e-b1db0f45907c', '2022-04-21 15:15:57.000000', 'zzzz', 'name', '445b24c8-db12-41d7-a11e-eb77f0802a1c');

INSERT INTO `template_definition` (`template_definition_id`, `template_definition_data`) VALUES ('1effbef18f0ab2b5c8766d9e75a67ef47578e741942fd442f23231bdc7506dd4', '{"name":"vorblatt","doc":"Die Klasse \\"vorblatt\\" bildet ein eigenständiges Teildokument eines Rechtsetzungsdokuments in der Entwurfsfassung in Form eines separaten XML-Dokuments, das durch die Klasse \\"rechtsetzungsdokument\\" referenziert und in dieser eingebunden wird.","mixed":false,"akn":{"name":"vorblatt","tagName":"doc"},"compositor":"sequence","elements":[{"name":"metadaten","occurence":{"min":1,"max":1}},{"name":"vorblattDokumentenkopf","occurence":{"min":1,"max":1}},{"name":"vorblattHauptteil","occurence":{"min":1,"max":1}}]}');

INSERT INTO `template` (`template_name`, `template_version`, `template_definition`, `template_toolbox`) VALUES ('name', '1.2.3', '1effbef18f0ab2b5c8766d9e75a67ef47578e741942fd442f23231bdc7506dd4', '1effbef18f0ab2b5c8766d9e75a67ef47578e741942fd442f23231bdc7506dd4');
