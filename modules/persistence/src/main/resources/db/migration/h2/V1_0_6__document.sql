-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE `document` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `document_id` varchar(36) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `inherit_from_id`  varchar(36) DEFAULT NULL,
  `content` varchar DEFAULT NULL,
  `compound_document` varchar(36) DEFAULT NULL,
  `created_by_id` varchar(36) NOT NULL,
  `updated_by_id` varchar(36) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) --,
  -- CONSTRAINT `fk_document_created_by_id` FOREIGN KEY ("created_by_id") REFERENCES "user" (`id`),
  -- CONSTRAINT `fk_document_updated_by_id` FOREIGN KEY ("updated_by_id") REFERENCES "user" (`id`)
);

CREATE INDEX idx_document_created_by_id_document_id ON document(`created_by_id`, document_id);
CREATE INDEX idx_document_created_by_id_compound_document ON document(`created_by_id`, compound_document);
CREATE INDEX idx_document_document_id ON document(document_id);
CREATE INDEX idx_document_compound_document ON document(compound_document);
CREATE INDEX idx_document_compound_document_type ON document(compound_document, type);
