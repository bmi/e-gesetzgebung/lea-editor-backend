-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE IF NOT EXISTS `drucksachen` (
  `id` BIGINT PRIMARY KEY AUTO_INCREMENT,
  `regelungsvorhaben_id` VARCHAR(36) NOT NULL,
  `drucksachen_nr` VARCHAR(255) NOT NULL,
  `created_by` VARCHAR(255) DEFAULT 'BUNDESTAG',
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  UNIQUE (`drucksachen_nr`),
  UNIQUE (`drucksachen_nr`, `regelungsvorhaben_id`)
);
