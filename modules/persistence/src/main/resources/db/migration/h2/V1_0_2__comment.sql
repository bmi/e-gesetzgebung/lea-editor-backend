-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE `comment_position` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `displacement` bigint DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `comment` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `comment_id` varchar(36) DEFAULT NULL UNIQUE,
  `content` varchar DEFAULT NULL,
  `state` varchar(50) DEFAULT 'OPEN',
  `document_id` varchar(36) DEFAULT NULL,
  `focus_id` bigint,
  `anchor_id` bigint,
  `created_by_id` varchar(36) DEFAULT NULL,
  `updated_by_id` varchar(36) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `deleted` boolean DEFAULT FALSE,
  PRIMARY KEY (`id`),
  -- CONSTRAINT `fk_comment_created_by_id` FOREIGN KEY ("created_by_id") REFERENCES "user" (`id`),
  -- CONSTRAINT `fk_comment_updated_by_id` FOREIGN KEY ("updated_by_id") REFERENCES "user" (`id`),
  CONSTRAINT `fk_comment_anchor_id` FOREIGN KEY (`anchor_id`) REFERENCES `comment_position` (`id`),
  CONSTRAINT `fk_comment_focus_id` FOREIGN KEY (`focus_id`) REFERENCES `comment_position` (`id`)
);

CREATE INDEX idx_comment_comment_id ON comment(comment_id);
CREATE INDEX idx_comment_document_id ON comment(document_id);
CREATE INDEX idx_comment_comment_id_document_id ON comment(comment_id, document_id);
CREATE INDEX idx_comment_document_id_deleted ON comment(document_id, deleted);
CREATE INDEX idx_comment_document_id_created_by_id ON comment(document_id, created_by_id);
CREATE INDEX idx_comment_document_id_created_by_id_deleted ON comment(document_id, created_by_id, deleted);
