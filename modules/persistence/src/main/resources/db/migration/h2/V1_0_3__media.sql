-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE `media_data` (
  `media_data_id` varchar(64) NOT NULL,
  `media_data` longblob NOT NULL,
  PRIMARY KEY (`media_data_id`)
);

CREATE TABLE `media_metadata` (
  `media_metadata_id` varchar(255) NOT NULL,
  `created_date` datetime(6) NOT NULL,
  `media_type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `media_data` varchar(64) NOT NULL,
  PRIMARY KEY (`media_metadata_id`),
  CONSTRAINT `FK_media_data` FOREIGN KEY (`media_data`) REFERENCES `media_data` (`media_data_id`)
);
