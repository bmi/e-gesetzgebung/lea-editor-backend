-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE `reply` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `reply_id` varchar(36) DEFAULT NULL UNIQUE,
  `comment_id` varchar(36) DEFAULT NULL,
  `document_id` varchar(36) DEFAULT NULL,
  `parent_id` varchar(36) DEFAULT NULL,
  `content` varchar DEFAULT NULL,
  `created_by_id` varchar(36) NOT NULL,
  `updated_by_id` varchar(36) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `deleted` boolean DEFAULT FALSE,
  PRIMARY KEY (`id`) --,
  -- CONSTRAINT `fk_reply_created_by_id` FOREIGN KEY ("created_by_id") REFERENCES "user" (`id`),
  -- CONSTRAINT `fk_reply_updated_by_id` FOREIGN KEY ("updated_by_id") REFERENCES "user" (`id`)
);

CREATE INDEX idx_reply_reply_id ON reply(reply_id);
CREATE INDEX idx_reply_reply_id_deleted ON reply(reply_id, deleted);
CREATE INDEX idx_reply_parent_id_deleted ON reply(parent_id, deleted);
CREATE INDEX idx_reply_comment_id ON reply(comment_id);
CREATE INDEX idx_reply_comment_id_deleted ON reply(comment_id, deleted);
