-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE `proposition` (
	`id` BIGINT NOT NULL AUTO_INCREMENT,
	`proposition_id` VARCHAR(36) DEFAULT NULL,
	`title` VARCHAR(255) DEFAULT NULL,
	`short_title` VARCHAR(255) DEFAULT NULL,
	`state` VARCHAR(50) DEFAULT NULL,
	`proposition_type` VARCHAR(50) DEFAULT NULL,
	`abbreviation` VARCHAR(255) DEFAULT NULL,
	`proponent_designation_nominative` VARCHAR(255) DEFAULT NULL,
	`proponent_designation_genitive` VARCHAR(255) DEFAULT NULL,
	`proponent_id` VARCHAR(36) DEFAULT NULL,
	`proponent_title` VARCHAR(255) DEFAULT NULL,
	`proponent_active` BOOLEAN DEFAULT NULL,
	`initiant` VARCHAR(255) DEFAULT NULL,
	`created_at` DATETIME(6) NOT NULL,
	`farbe` VARCHAR(25) DEFAULT NULL,
	`referable_id` VARCHAR(36) DEFAULT NULL,
	PRIMARY KEY (`id`)
);

CREATE INDEX idx_proposition_proposition_id ON proposition(proposition_id);
