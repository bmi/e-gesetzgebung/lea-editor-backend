-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE IF NOT EXISTS `usersettings` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_settings_id` varchar(36) NOT NULL,
  `related_user_id` varchar(36) NOT NULL,
  `configuration` varchar DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  -- CONSTRAINT "fk_user_settings_related_user_id" FOREIGN KEY ("related_user_id") REFERENCES "user" (`id`),
  CONSTRAINT "uq_related_user_id" UNIQUE(`related_user_id`)
);

CREATE INDEX idx_user_settings_related_user_id ON `usersettings`(`related_user_id`);