-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE `egfadata` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `egfa_data_id` varchar(36) NOT NULL,
  `module_name` varchar(255) DEFAULT NULL,
  `content` varchar DEFAULT NULL,
  `regelungs_vorhaben_id` VARCHAR(36) DEFAULT NULL,
  `transmitted_by_user_id` VARCHAR(36) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `completed_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  -- CONSTRAINT `fk_egfa_data_transmitted_by_user_id` FOREIGN KEY ("transmitted_by_user_id") REFERENCES "user" (`id`),
  CONSTRAINT `uq_data` UNIQUE(regelungs_vorhaben_id, module_name, created_at)
);

CREATE INDEX idx_module_name_regelungs_vorhaben_id_completed_at ON egfadata(module_name, regelungs_vorhaben_id, completed_at);