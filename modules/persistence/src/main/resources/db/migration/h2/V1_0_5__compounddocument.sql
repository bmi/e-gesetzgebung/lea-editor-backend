-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE `compound_document` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `compound_document_id` varchar(36) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `inherit_from_id`  varchar(36) DEFAULT NULL,
  `version` varchar(50) DEFAULT '1',
  `state` varchar(50) DEFAULT 'DRAFT',
  `verfahrens_type` varchar(50) DEFAULT 'REFERENTENENTWURF',
  `regelungs_vorhaben_id` varchar(36) DEFAULT NULL,
  `created_by_id` varchar(36) DEFAULT NULL,
  `updated_by_id` varchar(36) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `fixed_regelungsvorhaben_referenz_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`) --,
  -- CONSTRAINT `fk_compound_document_created_by_id_user_id` FOREIGN KEY ("created_by_id") REFERENCES "user" (`id`) ON UPDATE CASCADE,
  -- CONSTRAINT `fk_compound_document_updated_by_id_user_id` FOREIGN KEY ("updated_by_id") REFERENCES "user" (`id`) ON UPDATE CASCADE
);

CREATE INDEX idx_compound_document_created_by_id_compound_document_id ON compound_document(`created_by_id`, compound_document_id);
CREATE INDEX idx_compound_document_created_by_id_regelungs_vorhaben_id ON compound_document(`created_by_id`, regelungs_vorhaben_id);
CREATE INDEX idx_compound_document_created_by_id_regelungs_vorhaben_id_state ON compound_document(`created_by_id`, regelungs_vorhaben_id, state);
CREATE INDEX idx_compound_document_compound_document_id ON compound_document(compound_document_id);
CREATE INDEX idx_compound_document_inherit_from_id ON compound_document(inherit_from_id);
CREATE INDEX idx_compound_document_regelungs_vorhaben_id ON compound_document(regelungs_vorhaben_id);
CREATE INDEX idx_compound_document_regelungs_vorhaben_id_state ON compound_document(regelungs_vorhaben_id, state);
