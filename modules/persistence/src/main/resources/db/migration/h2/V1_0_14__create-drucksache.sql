-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE IF NOT EXISTS `drucksache_dokument` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `drucksache_dokument_id` varchar(36) NOT NULL,
  `compound_document_id` varchar(36) NOT NULL,
  `druck_dokument_id` varchar(36) NOT NULL,
  `created_by_id` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_by_id` varchar(255) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `druck_dokument` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `druck_dokument_id` varchar(36) NOT NULL,
  `pdf_blob` longblob NOT NULL,
  `mime_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);