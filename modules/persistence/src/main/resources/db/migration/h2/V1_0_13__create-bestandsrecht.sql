-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE IF NOT EXISTS `bestandsrecht` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `bestandsrecht_id` varchar(255) NOT NULL,
  `eli` varchar(255) NOT NULL,
  `titel_kurz` varchar(255) DEFAULT NULL,
  `titel_Lang` varchar(255) DEFAULT NULL,
  `content_original` varchar DEFAULT NULL,
  `content_json` varchar DEFAULT NULL,
  `transformation_messages` varchar DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `uq_eli` UNIQUE(eli)
);

CREATE TABLE IF NOT EXISTS `rv_bestandsrecht` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `rv_bestandsrecht_id` varchar(255) NOT NULL,
  `regelungs_vorhaben_id` varchar(255) DEFAULT NULL,
  `bestandsrecht_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `uq_mapping` UNIQUE(regelungs_vorhaben_id, bestandsrecht_id)
);

CREATE TABLE IF NOT EXISTS `dm_bestandsrecht` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `compound_document_id` varchar(36) NOT NULL,
    `dokumentenmappe_titel` varchar(255) DEFAULT NULL,
    `bestandsrecht_id` varchar(36) NOT NULL,
    `bestandsrecht_titel_kurz` varchar(255) DEFAULT NULL,
    `created_at` datetime(6) NOT NULL,
    `updated_at` datetime(6) NOT NULL,
    `created_by_id` varchar(36) DEFAULT NULL,
    `updated_by_id` varchar(36) DEFAULT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `uq_mapping_dm_best` UNIQUE(`compound_document_id`, `bestandsrecht_id`)
);
