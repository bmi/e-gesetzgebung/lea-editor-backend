-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

SET foreign_key_checks = 0;

-- größen der UUID-Felder anpassen
ALTER TABLE usersettings MODIFY user_settings_id VARCHAR(36);

SET foreign_key_checks = 1;

-- neue temporäre Spalten an
ALTER TABLE `usersettings` ADD COLUMN `reply_nutzer` VARCHAR(36) NOT NULL;

-- auffüllen der temporären Spalten mit UUIDs der Benutzer
UPDATE usersettings d SET reply_nutzer = (SELECT gid FROM user u WHERE u.id = d.related_user_id);

-- die alten Spalten UND dazugehörigen FOREIGN_KEYS löschen
CALL PROC_DROP_FOREIGN_KEY_AUTOMATIC('usersettings', 'related_user_id');
CALL PROC_REMOVE_COLUMN('usersettings', 'related_user_id');

-- die temporären Spalten in die alten Spaltennamen umbenennen
CALL PROC_CHANGE_COLUMN('usersettings', 'reply_nutzer', 'related_user_id', 'VARCHAR(36)');

-- INDEX setzen
CREATE INDEX idx_related_user_id ON usersettings(related_user_id);