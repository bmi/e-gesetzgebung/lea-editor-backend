-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

SET foreign_key_checks = 0;

-- größen der UUID-Felder anpassen
ALTER TABLE compound_document MODIFY regelungs_vorhaben_id VARCHAR(36);
ALTER TABLE compound_document MODIFY state VARCHAR(50);
ALTER TABLE compound_document MODIFY type VARCHAR(100);
ALTER TABLE compound_document MODIFY version VARCHAR(10);
ALTER TABLE compound_document MODIFY inherit_from_id VARCHAR(36);
ALTER TABLE compound_document MODIFY verfahrens_type VARCHAR(50);
ALTER TABLE compound_document MODIFY compound_document_id VARCHAR(36);

SET foreign_key_checks = 1;

-- neue temporäre Spalten an
ALTER TABLE `compound_document` ADD COLUMN `created_by_nutzer` VARCHAR(36) NOT NULL;
ALTER TABLE `compound_document` ADD COLUMN `updated_by_nutzer` VARCHAR(36) NOT NULL;

-- auffüllen der temporären Spalten mit UUIDs der Benutzer
UPDATE compound_document d SET created_by_nutzer = (SELECT gid FROM user u WHERE u.id = d.created_by_id);
UPDATE compound_document d SET updated_by_nutzer = (SELECT gid FROM user u WHERE u.id = d.updated_by_id);

-- die alten Spalten UND dazugehörigen FOREIGN_KEYS löschen
CALL PROC_DROP_FOREIGN_KEY_AUTOMATIC('compound_document', 'created_by_id');
CALL PROC_REMOVE_COLUMN('compound_document', 'created_by_id');

CALL PROC_DROP_FOREIGN_KEY_AUTOMATIC('compound_document', 'updated_by_id');
CALL PROC_REMOVE_COLUMN('compound_document', 'updated_by_id');

-- die temporären Spalten in die alten Spaltennamen umbenennen
CALL PROC_CHANGE_COLUMN('compound_document', 'created_by_nutzer', 'created_by_id', 'VARCHAR(36)');
CALL PROC_CHANGE_COLUMN('compound_document', 'updated_by_nutzer', 'updated_by_id', 'VARCHAR(36)');

-- INDEX setzen
CREATE INDEX idx_created_by_id ON compound_document(created_by_id);