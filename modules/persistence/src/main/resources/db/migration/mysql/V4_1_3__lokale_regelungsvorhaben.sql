-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CALL PROC_ADD_COLUMN('proposition', 'referable_id', 'VARCHAR(36)');
CALL PROC_DROP_UNIQUE_KEY_BY_ID('proposition', 'uk_proposition_proposition_id');
CALL PROC_CREATE_UNIQUE_INDEX('proposition', 'proposition_id, referable_id', 'uk_proposition_id_referenz');
CALL PROC_CREATE_UNIQUE_INDEX('proposition', 'referable_id', 'idx_proposition_referable_id');

CALL PROC_ADD_COLUMN('compound_document', 'fixed_regelungsvorhaben_referenz_id', 'VARCHAR(36)');
-- CALL PROC_ADD_CONSTRAINT_IF_COULMN_EXISTS('compound_document', 'FK_compound_document_referenz_rv', 'fixed_regelungsvorhaben_referenz_id', 'proposition(referable_id)');
