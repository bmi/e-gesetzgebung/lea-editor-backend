-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE IF NOT EXISTS `egfadata` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `egfa_data_id` varchar(255) NOT NULL,
  `module_name` varchar(255) NOT NULL,
  `content` longtext DEFAULT NULL,
  `regelungs_vorhaben_id` varchar(255) DEFAULT NULL,
  `transmitted_by_user_id` bigint NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `completed_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_egfa_data_transmitted_by_user_id` FOREIGN KEY (`transmitted_by_user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `uq_data` UNIQUE(regelungs_vorhaben_id, module_name, completed_at)
) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE INDEX idx_module_name_regelungs_vorhaben_id_completed_at ON egfadata(module_name, regelungs_vorhaben_id, completed_at);