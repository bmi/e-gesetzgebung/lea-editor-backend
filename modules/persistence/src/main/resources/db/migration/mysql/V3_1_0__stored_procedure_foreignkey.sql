-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

DROP PROCEDURE IF EXISTS `PROC_DROP_FOREIGN_KEY_AUTOMATIC`;
DELIMITER //
CREATE PROCEDURE `PROC_DROP_FOREIGN_KEY_AUTOMATIC`(IN tableName VARCHAR(64), IN columnName VARCHAR(64))
BEGIN
        IF EXISTS(
           SELECT constraint_name
             FROM information_schema.key_column_usage
             WHERE referenced_table_name IS NOT NULL
               AND table_name = tableName
               AND COLUMN_NAME = columnName
               AND table_schema = (SELECT table_schema
                        FROM information_schema.tables
                        WHERE table_name = tableName AND table_schema = DATABASE())
               LIMIT 1
        )
        THEN

            SET @CONSTRAINT_NAME = (
                 SELECT constraint_name
                   FROM information_schema.key_column_usage
                   WHERE referenced_table_name IS NOT NULL
                     AND table_name = tableName
                     AND COLUMN_NAME = columnName
                     AND table_schema = (SELECT table_schema
                        FROM information_schema.tables
                        WHERE table_name = tableName AND table_schema = DATABASE())
            );

            SET @query = CONCAT('ALTER TABLE ', tableName, ' DROP FOREIGN KEY ', @CONSTRAINT_NAME, ';');
            PREPARE stmt FROM @query;
            EXECUTE stmt;
            DEALLOCATE PREPARE stmt;
        END IF;
    END//
DELIMITER ;