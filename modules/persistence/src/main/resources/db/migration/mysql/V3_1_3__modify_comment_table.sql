-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

SET foreign_key_checks = 0;

-- größen der UUID-Felder anpassen
ALTER TABLE comment MODIFY document_id VARCHAR(36);
ALTER TABLE comment MODIFY state VARCHAR(50);
ALTER TABLE comment MODIFY comment_id VARCHAR(36);

SET foreign_key_checks = 1;

-- neue temporäre Spalten an
ALTER TABLE `comment` ADD COLUMN `created_by_nutzer` VARCHAR(36) NOT NULL;
ALTER TABLE `comment` ADD COLUMN `updated_by_nutzer` VARCHAR(36) NOT NULL;

-- auffüllen der temporären Spalten mit UUIDs der Benutzer
UPDATE comment d SET created_by_nutzer = (SELECT gid FROM user u WHERE u.id = d.created_by_id);
UPDATE comment d SET updated_by_nutzer = (SELECT gid FROM user u WHERE u.id = d.updated_by_id);

-- die alten Spalten UND dazugehörigen FOREIGN_KEYS löschen
CALL PROC_DROP_FOREIGN_KEY_AUTOMATIC('comment', 'created_by_id');
CALL PROC_REMOVE_COLUMN('comment', 'created_by_id');

CALL PROC_DROP_FOREIGN_KEY_AUTOMATIC('comment', 'updated_by_id');
CALL PROC_REMOVE_COLUMN('comment', 'updated_by_id');

-- die temporären Spalten in die alten Spaltennamen umbenennen
CALL PROC_CHANGE_COLUMN('comment', 'created_by_nutzer', 'created_by_id', 'VARCHAR(36)');
CALL PROC_CHANGE_COLUMN('comment', 'updated_by_nutzer', 'updated_by_id', 'VARCHAR(36)');

-- INDEX setzen
CREATE INDEX idx_created_by_id ON comment(created_by_id);