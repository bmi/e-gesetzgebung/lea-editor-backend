-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CALL PROC_DROP_UNIQUE_KEY_BY_ID('proposition', 'uk_proposition_id_referenz');
CALL PROC_DROP_UNIQUE_KEY_BY_ID('proposition', 'idx_proposition_referable_id');
CALL PROC_CREATE_UNIQUE_INDEX('proposition', 'proposition_id, referable_id', 'uk_proposition_id_referenz');