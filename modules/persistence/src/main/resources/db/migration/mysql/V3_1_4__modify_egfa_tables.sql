-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

SET foreign_key_checks = 0;

-- größen der UUID-Felder anpassen
ALTER TABLE egfadata MODIFY regelungs_vorhaben_id VARCHAR(36);
ALTER TABLE egfadata MODIFY egfa_data_id VARCHAR(36);

SET foreign_key_checks = 1;

-- neue temporäre Spalten an^ transmitted_by_user_id
ALTER TABLE `egfadata` ADD COLUMN `transmitted_by_nutzer` VARCHAR(36) NOT NULL;

-- auffüllen der temporären Spalten mit UUIDs der Benutzer
UPDATE egfadata d SET transmitted_by_nutzer = (SELECT gid FROM user u WHERE u.id = d.transmitted_by_user_id);

-- die alten Spalten UND dazugehörigen FOREIGN_KEYS löschen
CALL PROC_DROP_FOREIGN_KEY_AUTOMATIC('egfadata', 'transmitted_by_user_id');
CALL PROC_REMOVE_COLUMN('egfadata', 'transmitted_by_user_id');

-- die temporären Spalten in die alten Spaltennamen umbenennen
CALL PROC_CHANGE_COLUMN('egfadata', 'transmitted_by_nutzer', 'transmitted_by_user_id', 'VARCHAR(36)');

-- INDEX setzen
CREATE INDEX idx_transmitted_by_id ON egfadata(transmitted_by_user_id);

-- --------------------------------------------------

SET foreign_key_checks = 0;

-- größen der UUID-Felder anpassen
ALTER TABLE egfa_metadata MODIFY compound_document_id VARCHAR(36);
ALTER TABLE egfa_metadata MODIFY egfa_data_id VARCHAR(36);
ALTER TABLE egfa_metadata MODIFY regelungs_vorhaben_id VARCHAR(36);
ALTER TABLE egfa_metadata MODIFY document_id VARCHAR(36);
ALTER TABLE egfa_metadata MODIFY egfa_metadata_id VARCHAR(36);

SET foreign_key_checks = 1;

-- neue temporäre Spalten an^ transmitted_by_user_id
ALTER TABLE `egfa_metadata` ADD COLUMN `created_by_nutzer` VARCHAR(36) NOT NULL;

-- auffüllen der temporären Spalten mit UUIDs der Benutzer
UPDATE egfa_metadata d SET created_by_nutzer = (SELECT gid FROM user u WHERE u.id = d.created_by_user_id);

-- die alten Spalten UND dazugehörigen FOREIGN_KEYS löschen
CALL PROC_DROP_FOREIGN_KEY_AUTOMATIC('egfa_metadata', 'created_by_user_id');
CALL PROC_REMOVE_COLUMN('egfa_metadata', 'created_by_user_id');

-- die temporären Spalten in die alten Spaltennamen umbenennen
CALL PROC_CHANGE_COLUMN('egfa_metadata', 'created_by_nutzer', 'created_by_user_id', 'VARCHAR(36)');

-- INDEX setzen
CREATE INDEX idx_created_by_id ON egfa_metadata(created_by_user_id);