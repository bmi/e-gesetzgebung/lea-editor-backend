-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE IF NOT EXISTS `dm_bestandsrecht` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `compound_document_id` varchar(36) NOT NULL,
    `dokumentenmappe_titel` varchar(255) DEFAULT NULL,
    `bestandsrecht_id` varchar(36) NOT NULL,
    `bestandsrecht_titel_kurz` varchar(255) DEFAULT NULL,
    `created_at` datetime(6) NOT NULL,
    `updated_at` datetime(6) NOT NULL,
    `created_by_id` varchar(36) DEFAULT NULL,
    `updated_by_id` varchar(36) DEFAULT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `uq_mapping_dm_best` UNIQUE(`compound_document_id`, `bestandsrecht_id`)
) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE INDEX idx_dm_bestandsrecht_dmid ON dm_bestandsrecht(compound_document_id);
CREATE INDEX idx_dm_bestandsrecht_bestid ON dm_bestandsrecht(bestandsrecht_id);
