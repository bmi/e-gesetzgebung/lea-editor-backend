-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- make Focus und Anchor nullable

CALL PROC_CHANGE_COLUMN('comment', 'focus_id', 'focus_id', 'bigint DEFAULT NULL');
CALL PROC_CHANGE_COLUMN('comment', 'anchor_id', 'anchor_id', 'bigint DEFAULT NULL');
