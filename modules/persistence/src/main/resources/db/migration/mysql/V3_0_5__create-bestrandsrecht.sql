-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE IF NOT EXISTS `bestandsrecht` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `bestandsrecht_id` varchar(255) NOT NULL,
  `eli` varchar(255) NOT NULL,
  `titel_kurz` varchar(255) DEFAULT NULL,
  `titel_Lang` varchar(255) DEFAULT NULL,
  `content_original` longtext,
  `content_json` longtext,
  `transformation_messages` longtext,
  PRIMARY KEY (`id`),
  CONSTRAINT `uq_eli` UNIQUE(eli)
) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `rv_bestandsrecht` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `rv_bestandsrecht_id` varchar(255) NOT NULL,
  `regelungs_vorhaben_id` varchar(255) DEFAULT NULL,
  `bestandsrecht_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `uq_data` UNIQUE(regelungs_vorhaben_id, bestandsrecht_id)
) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

