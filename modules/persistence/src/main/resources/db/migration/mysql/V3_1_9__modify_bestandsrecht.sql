-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

SET foreign_key_checks = 0;

-- größen der UUID-Felder anpassen
ALTER TABLE bestandsrecht MODIFY bestandsrecht_id VARCHAR(36);

SET foreign_key_checks = 1;

-- INDEX setzen
CREATE INDEX idx_estandsrecht_id ON bestandsrecht(bestandsrecht_id);
CREATE INDEX idx_eli ON bestandsrecht(eli);