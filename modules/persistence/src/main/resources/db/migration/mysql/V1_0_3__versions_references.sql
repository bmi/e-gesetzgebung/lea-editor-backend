-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CALL PROC_CHANGE_COLUMN('compound_document', 'version', 'version', 'VARCHAR(255) DEFAULT 1');
UPDATE compound_document SET version=1 WHERE VERSION LIKE 'v1';

CALL PROC_ADD_COLUMN('compound_document', 'inherit_from_id', 'VARCHAR(255) DEFAULT null');
-- ALTER TABLE `compound_document` ADD CONSTRAINT `FK_compound_document_inherit` FOREIGN KEY (`inherit_from`) REFERENCES `compound_document` (`compound_document_id`);
CALL PROC_ADD_CONSTRAINT_IF_COULMN_EXISTS('compound_document', 'FK_compound_document_inherit', 'inherit_from_id', 'compound_document(compound_document_id)' );

DROP PROCEDURE IF EXISTS `PROC_DROP_AND_RECREATE_UNIQUE_KEY_BY_ID`;
DELIMITER //
CREATE PROCEDURE `PROC_DROP_AND_RECREATE_UNIQUE_KEY_BY_ID`(IN tableName VARCHAR(64), IN constraintName VARCHAR(64), IN newConstraintName VARCHAR(64), IN columnName VARCHAR(64))
BEGIN
        IF EXISTS(
            SELECT * FROM information_schema.table_constraints
            WHERE
                table_schema    = DATABASE()     AND
                table_name      = tableName      AND
                constraint_name = constraintName AND
                constraint_type = 'UNIQUE')
        THEN
            SET @query = CONCAT('ALTER TABLE ', tableName, ' DROP INDEX ', constraintName, ', ADD UNIQUE INDEX ', newConstraintName, ' (', columnName, ');');
            PREPARE stmt FROM @query;
            EXECUTE stmt;
            DEALLOCATE PREPARE stmt;
        END IF;
    END//
DELIMITER ;

-- ALTER TABLE `document`
-- 	DROP INDEX `UK_document_document_id_version`,
--	ADD UNIQUE INDEX `UK_document_document_id` (`document_id`);
CALL PROC_DROP_AND_RECREATE_UNIQUE_KEY_BY_ID('document', 'UK_document_document_id_version', 'UK_document_document_id', 'document_id');

CALL PROC_REMOVE_COLUMN('document', 'version');

CALL PROC_ADD_COLUMN('document', 'inherit_from_id', 'VARCHAR(255) DEFAULT null');
-- ALTER TABLE `document` ADD CONSTRAINT `FK_document_inherit` FOREIGN KEY (`inherit_from`) REFERENCES `document` (`document_id`);
CALL PROC_ADD_CONSTRAINT_IF_COULMN_EXISTS('document', 'FK_document_inherit', 'inherit_from_id', 'document(document_id)' );
