-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

SET foreign_key_checks = 0;

-- größen der UUID-Felder anpassen
ALTER TABLE proposition MODIFY proponent_id VARCHAR(36);
ALTER TABLE proposition MODIFY proposition_type VARCHAR(50);
ALTER TABLE proposition MODIFY state VARCHAR(50);
ALTER TABLE proposition MODIFY proposition_id VARCHAR(36);

SET foreign_key_checks = 1;

-- INDEX setzen
CREATE INDEX idx_proposition_id ON proposition(proposition_id);