-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE IF NOT EXISTS `egfa_metadata` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `egfa_metadata_id` varchar(255) NOT NULL,
  `document_id` varchar(255) DEFAULT NULL,
  `regelungs_vorhaben_id` varchar(255) DEFAULT NULL,
  `module_name` varchar(255) NOT NULL,
  `module_completed_at` datetime(6) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `created_by_user_id` bigint NOT NULL,
  `egfa_data_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_egfa_metadata_created_by_user_id` FOREIGN KEY (`created_by_user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE
) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE INDEX idx_document_id_regelungs_vorhaben_id ON egfa_metadata(document_id, regelungs_vorhaben_id);