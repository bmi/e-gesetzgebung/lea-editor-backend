-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE IF NOT EXISTS `reply` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `reply_id` varchar(255) DEFAULT NULL,
  `comment_id` varchar(255) DEFAULT NULL,
  `parent_id` varchar(255) DEFAULT NULL,
  `content` text,
  `document_id` varchar(255) DEFAULT NULL,
  `created_by_id` bigint NOT NULL,
  `updated_by_id` bigint NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_reply_reply_id` (`reply_id`),
  CONSTRAINT `FK_reply_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_reply_updated_by_id` FOREIGN KEY (`updated_by_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE
) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
