-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

UPDATE document
SET `type` = 'BEGRUENDUNG_STAMMFORM'
WHERE `type` = 'BEGRUENDUNG'
;

UPDATE document
SET `type` = 'VORBLATT_STAMMFORM'
WHERE `type` = 'VORBLATT'
;
