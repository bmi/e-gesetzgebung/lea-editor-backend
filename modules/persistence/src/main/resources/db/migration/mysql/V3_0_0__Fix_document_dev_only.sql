-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- Only while devloping, so it need not be deployed
-- This only for 'remembering'
-- UPDATE document SET content = CONCAT('[',content,']') WHERE content NOT LIKE '[%';