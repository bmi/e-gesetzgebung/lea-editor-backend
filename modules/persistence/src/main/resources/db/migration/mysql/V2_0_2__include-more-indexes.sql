-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE INDEX idx_comment_comment_id_document_id ON comment (comment_id, document_id);
CREATE INDEX idx_comment_document_id_deleted ON comment (document_id, deleted);
CREATE INDEX idx_comment_document_id_created_by_id ON comment (document_id, created_by_id);
CREATE INDEX idx_comment_document_id_created_by_id_deleted ON comment (document_id, created_by_id, deleted);

CREATE INDEX idx_compound_document_created_by_id_compound_document_id ON compound_document (created_by_id, compound_document_id);
CREATE INDEX idx_compound_document_created_by_id_regelungs_vorhaben_id ON compound_document (created_by_id, regelungs_vorhaben_id);
CREATE INDEX idx_compound_document_created_by_id_regelungs_vorhaben_id_state ON compound_document (created_by_id, regelungs_vorhaben_id, state);
CREATE INDEX idx_compound_document_compound_document_id ON compound_document (compound_document_id);
CREATE INDEX idx_compound_document_regelungs_vorhaben_id ON compound_document (regelungs_vorhaben_id);
CREATE INDEX idx_compound_document_regelungs_vorhaben_id_state ON compound_document (regelungs_vorhaben_id, state);

CREATE INDEX idx_document_created_by_id_document_id ON document (created_by_id, document_id);
CREATE INDEX idx_document_created_by_id_compound_document ON document (created_by_id, compound_document);
CREATE INDEX idx_document_compound_document ON document (compound_document);
CREATE INDEX idx_document_compound_document_type ON document (compound_document, type);

CREATE INDEX idx_reply_reply_id_deleted ON reply (reply_id, deleted);
CREATE INDEX idx_reply_parent_id_deleted ON reply (parent_id, deleted);
CREATE INDEX idx_reply_comment_id ON reply (comment_id);
CREATE INDEX idx_reply_comment_id_deleted ON reply (comment_id, deleted);

CREATE INDEX idx_template_template_name_version_updated_at ON template (template_name, version, updated_at DESC);

CREATE INDEX idx_user_email ON `user` (email);
CREATE INDEX idx_user_name ON `user` (name);
