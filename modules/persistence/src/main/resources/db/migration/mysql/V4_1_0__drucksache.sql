-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE IF NOT EXISTS `drucksachen` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `regelungsvorhaben_id` bigint NOT NULL,
  `drucksachen_nr` varchar(255) NOT NULL,
  `created_by` varchar(255) NOT NULL DEFAULT 'BUNDESTAG',
  `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_drucksachen_nr` (`drucksachen_nr`),
  UNIQUE KEY `UK_drucksache_regelungsvorhaben` (`drucksachen_nr`,`regelungsvorhaben_id`)
);