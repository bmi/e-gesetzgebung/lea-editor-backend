// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.mappers;

import de.itzbund.egesetz.bmi.lea.domain.model.EgfaMetadata;
import de.itzbund.egesetz.bmi.lea.persistence.entities.EgfaMetadataEntity;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true),
    uses = {
        BaseMapperPersistence.class
    })
public interface EgfaMetadataMapperPersistence {

    @Mapping(target = "createdByUser", source = "createdByUserId", qualifiedByName = "userStringMapping")
    EgfaMetadataEntity merge(EgfaMetadata egfaMetadata, @MappingTarget EgfaMetadataEntity egfaMetadataEntity);

    @Mapping(target = "createdByUser", source = "createdByUserId", qualifiedByName = "userStringMapping")
    EgfaMetadataEntity fromDomainItem(EgfaMetadata egfaMetadata);

    @Mapping(target = "createdByUserId", source = "createdByUser", qualifiedByName = "userStringMapping")
    EgfaMetadata toDomainItem(EgfaMetadataEntity egfaMetadataEntity);

}
