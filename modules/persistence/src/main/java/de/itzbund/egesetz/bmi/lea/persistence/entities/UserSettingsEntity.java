// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserSettingsId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.UserSettingsIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.listeners.CreatedOrUpdatedEntityListener;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.NaturalId;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Generated
@Entity
@EntityListeners({AuditingEntityListener.class, CreatedOrUpdatedEntityListener.class})
@Table(name = "usersettings")
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class UserSettingsEntity extends BaseEntity {

    @NotNull
    @EqualsAndHashCode.Include
    @Convert(converter = UserSettingsIdConverter.class, attributeName = "id")
    @ToString.Include
    @NaturalId
    private UserSettingsId userSettingsId;

    @CreatedBy
    @EqualsAndHashCode.Exclude
    @Column(name = "related_user_id")
    private String relatedUser;

    @Column(columnDefinition = "longtext")
    @EqualsAndHashCode.Exclude
    private String configuration;

    @CreatedDate
    @Column(updatable = false, nullable = false)
    @EqualsAndHashCode.Exclude
    private Instant createdAt;

    @LastModifiedDate
    @Column(nullable = false)
    @EqualsAndHashCode.Exclude
    private Instant updatedAt;

}
