// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.Drucksache;
import de.itzbund.egesetz.bmi.lea.domain.model.DrucksacheDokument;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DruckDokumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DrucksacheDokumentId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DrucksacheDokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DruckDokumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DrucksacheDokumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.DrucksacheDokumentMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.DruckDokumentRepository;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.DrucksacheDokumentRepository;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.sql.rowset.serial.SerialBlob;
import java.io.IOException;
import java.sql.SQLException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.core.Constants.MIME_TYPE_PDF;

@AllArgsConstructor
@Service
@Log4j2
public class DrucksacheDokumentPersistenceAdapter implements DrucksacheDokumentPersistencePort {

	private final DrucksacheDokumentRepository drucksacheDokumentRepository;
	private final DruckDokumentRepository druckDokumentRepository;
    private final DrucksacheDokumentMapperPersistence drucksacheDokumentMapper;

	public ServiceState updateDokumentDrucksache(SerialBlob pdfBlob, CompoundDocumentId compoundDocumentId, User user) {

		String userId = user.getGid().getId();
		Instant currentTimestamp = Instant.now();
		Optional<DrucksacheDokumentEntity> optionalDrucksacheDokumentEntity = drucksacheDokumentRepository.findByCompoundDocumentId(compoundDocumentId);

		if (optionalDrucksacheDokumentEntity.isEmpty()) {
			DruckDokumentId druckDokumentId = new DruckDokumentId(UUID.randomUUID());

			DrucksacheDokumentEntity drucksacheDokumentEntity = DrucksacheDokumentEntity.builder()
				.drucksacheDokumentId(new DrucksacheDokumentId(UUID.randomUUID())).compoundDocumentId(compoundDocumentId).druckDokumentId(druckDokumentId)
				.createdBy(userId).createdAt(currentTimestamp).updatedBy(userId).updatedAt(currentTimestamp).build();
			drucksacheDokumentRepository.save(drucksacheDokumentEntity);

			DruckDokumentEntity druckDokumentEntity = DruckDokumentEntity.builder().druckDokumentId(druckDokumentId).pdfBlob(pdfBlob).mimeType(MIME_TYPE_PDF)
				.build();
			druckDokumentRepository.save(druckDokumentEntity);
			return ServiceState.OK;
		}
		DrucksacheDokumentEntity drucksacheDokumentEntity = optionalDrucksacheDokumentEntity.get();
		//If DruckDokumenteEntry is not there sth. is wrong
		Optional<DruckDokumentEntity> optionalDruckDokumentEntity = druckDokumentRepository.findByDruckDokumentId(
			drucksacheDokumentEntity.getDruckDokumentId());
		if (optionalDruckDokumentEntity.isEmpty()) {
			return ServiceState.INVALID_STATE;
		}
		DruckDokumentEntity druckDokumentEntity = optionalDruckDokumentEntity.get();
		druckDokumentEntity.setPdfBlob(pdfBlob);
		druckDokumentRepository.save(druckDokumentEntity);

		drucksacheDokumentEntity.setUpdatedBy(userId);
		drucksacheDokumentEntity.setUpdatedAt(currentTimestamp);
		drucksacheDokumentRepository.save(drucksacheDokumentEntity);

		return ServiceState.OK;
	}

	public Optional<byte[]> getDrucksacheByCompoundDocumentId(CompoundDocumentId compoundDocumentId, List<ServiceState> status) {

		Optional<DrucksacheDokumentEntity> optionalDrucksacheDokumentEntity = drucksacheDokumentRepository.findByCompoundDocumentId(compoundDocumentId);
		if (optionalDrucksacheDokumentEntity.isEmpty()) {
			status.add(ServiceState.DRUCK_NOT_FOUND);
			return Optional.empty();
		}
		Optional<DruckDokumentEntity> optionalDruckDokumentEntity = druckDokumentRepository.findByDruckDokumentId(
			optionalDrucksacheDokumentEntity.get().getDruckDokumentId());

		if (optionalDruckDokumentEntity.isEmpty()) {
			status.add(ServiceState.UNKNOWN_ERROR);
			return Optional.empty();
		}
		try {
			return Optional.of(optionalDruckDokumentEntity.get().getPdfBlob().getBinaryStream().readAllBytes());
		} catch (SQLException | IOException e) {
			log.error(e);
			status.add(ServiceState.UNKNOWN_ERROR);
			return Optional.empty();
		}
	}

    @Override
    public Optional<DrucksacheDokument> getDrucksacheDokument(CompoundDocumentId compoundDocumentId) {
        Optional<DrucksacheDokumentEntity> optionalDrucksacheDokumentEntity = drucksacheDokumentRepository.findByCompoundDocumentId(compoundDocumentId);
        DrucksacheDokumentEntity drucksacheDokumentEntity = optionalDrucksacheDokumentEntity.orElse(null);
        if (drucksacheDokumentEntity == null) {
            return Optional.empty();
        } else {
            return Optional.of(drucksacheDokumentMapper.toDomainItem(drucksacheDokumentEntity));
        }
    }
}
