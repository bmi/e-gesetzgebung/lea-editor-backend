// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import de.itzbund.egesetz.bmi.lea.domain.model.MediaData;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.MediaPersistencePort;
import de.itzbund.egesetz.bmi.lea.persistence.entities.MediaDataEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.MediadataMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.MediadataRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@AllArgsConstructor
@Service
public class MediadataPersistenceAdapter implements MediaPersistencePort {

    // ----- Repositories -------
    final MediadataRepository repository;

    // ----- Mapper -------------
    MediadataMapperPersistence mediadataMapper;

    @Override
    public MediaData saveAndFlush(MediaData mediaData) {
        MediaDataEntity mediaDataEntity;
        Optional<MediaDataEntity> mediaDataEntityOptional = repository.findById(mediaData.getId());

        mediaDataEntity = mediaDataEntityOptional.orElseGet(() -> MediaDataEntity.builder()
            .build());

        mediaDataEntity = mediadataMapper.merge(mediaData, mediaDataEntity);
        return mediadataMapper.toDomainItem(repository.save(mediaDataEntity));

    }

    @Override
    public Optional<MediaData> findById(String id) {
        Optional<MediaDataEntity> byId = repository.findById(id);

        if (byId.isEmpty()) {
            return Optional.empty();
        }

        MediaDataEntity mediaDataEntity = byId.get();
        return Optional.of(mediadataMapper.toDomainItem(mediaDataEntity));
    }

    @Override
    public void deleteById(String id) {
        repository.deleteById(id);
    }
}
