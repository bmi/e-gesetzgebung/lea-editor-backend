// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <Beschreibung>
 */
@Converter(autoApply = true)
@SuppressWarnings("unused")
public class PositionEntityPathConverter implements AttributeConverter<List<Long>, String> {

    private static final String EMPTY_LIST = "[]";
    private static final String DELIMITER = ", ";


    @Override
    public String convertToDatabaseColumn(List<Long> path) {
        if (path == null) {
            return null;
        }

        return path.toString();
    }


    @Override
    public List<Long> convertToEntityAttribute(String dbData) {
        if (dbData == null || dbData.isEmpty() || EMPTY_LIST.equals(dbData)) {
            return Collections.emptyList();
        }

        return Arrays.stream(dbData.replaceAll("[\\[\\]]", "").split(DELIMITER)).map(Long::valueOf)
            .collect(Collectors.toList());
    }
}
