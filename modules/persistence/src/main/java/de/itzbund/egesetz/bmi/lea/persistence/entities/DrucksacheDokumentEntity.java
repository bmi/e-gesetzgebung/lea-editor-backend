// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DruckDokumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DrucksacheDokumentId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.CompoundDocumentIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.DruckDokumentIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.DrucksacheDokumentIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.listeners.CreatedOrUpdatedEntityListener;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.time.Instant;

@Generated
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@EntityListeners({AuditingEntityListener.class, CreatedOrUpdatedEntityListener.class})
@Table(name = "drucksache_dokument")
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DrucksacheDokumentEntity extends BaseEntity {

	@EqualsAndHashCode.Include
	@Convert(converter = DrucksacheDokumentIdConverter.class, attributeName = "id")
	@ToString.Include
	private DrucksacheDokumentId drucksacheDokumentId;

	@EqualsAndHashCode.Include
	@Convert(converter = CompoundDocumentIdConverter.class, attributeName = "id")
	@ToString.Include
	private CompoundDocumentId compoundDocumentId;

	@EqualsAndHashCode.Include
	@Convert(converter = DruckDokumentIdConverter.class, attributeName = "id")
	@ToString.Include
	private DruckDokumentId druckDokumentId;

	@CreatedBy
	@EqualsAndHashCode.Exclude
	@Column(name = "created_by_id")
	private String createdBy;

	@CreatedDate
	@Column(updatable = false, nullable = false)
	@EqualsAndHashCode.Exclude
	private Instant createdAt;

	@LastModifiedBy
	@EqualsAndHashCode.Exclude
	@Column(name = "updated_by_id")
	private String updatedBy;

	@LastModifiedDate
	@Column(nullable = false)
	@EqualsAndHashCode.Exclude
	private Instant updatedAt;

}
