// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CommentPersistencePort;
import de.itzbund.egesetz.bmi.lea.persistence.UserRepository;
import de.itzbund.egesetz.bmi.lea.persistence.entities.CommentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.CommentMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.CommentRepository;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class CommentPersistenceAdapter implements CommentPersistencePort {

	// ----- Repositories -------
	private final CommentRepository commentRepository;
	private final UserRepository userRepository;

	// ----- Mapper -------------
	private final CommentMapperPersistence commentMapper;
	private final UserMapperPersistence userMapper;

	@Override
	public List<Comment> findByDocumentIdAndCreatedByAndDeleted(DocumentId documentId, User user, boolean deleted) {

		if ((user == null) || (user.getGid() == null)) {
			return Collections.emptyList();
		}

		List<CommentEntity> commentEntities = commentRepository.findByDocumentIdAndCreatedByAndDeleted(documentId, user.getGid().getId(), deleted);

		return persistenceToDomain(commentEntities);

	}

	private List<Comment> persistenceToDomain(List<CommentEntity> commentEntities) {
		return commentEntities.stream()
			.map(this::getDocumentDomain)
			.collect(Collectors.toList());
	}

	private Comment getDocumentDomain(CommentEntity commentEntity) {
		Comment comment = commentMapper.toDomainItem(commentEntity);
		comment.setCreatedBy(getDetailedUser(commentEntity.getCreatedBy()));
		comment.setUpdatedBy(getDetailedUser(commentEntity.getUpdatedBy()));

		// Die Userdaten der Replies auffüllen
		List<Reply> replies = comment.getReplies();
		replies.forEach(r -> {
			r.setCreatedBy(getDetailedUser(r.getCreatedBy().getGid().getId()));
			r.setUpdatedBy(getDetailedUser(r.getUpdatedBy().getGid().getId()));
		});

		return comment;
	}

	private User getDetailedUser(String userGid) {
		NutzerEntity creator = userRepository.findFirstByGid(new UserId(userGid));
		return userMapper.toDomainItem(creator);
	}

	@Override
	public Comment findByCommentId(CommentId commentId) {
		Optional<CommentEntity> optionalCommentEntity = commentRepository.findByCommentId(commentId);
		return optionalCommentEntity
			.map(this::getDocumentDomain)
			.orElse(null);
	}

	@Override
	public List<Comment> findByDocumentId(DocumentId documentId) {
		List<CommentEntity> comments = commentRepository.findByDocumentId(documentId);
		return persistenceToDomain(comments);
	}


	@Override
	public List<Comment> findByDocumentIdAndDeleted(DocumentId documentId, boolean deleted) {
		List<CommentEntity> commentEntities = commentRepository.findByDocumentIdAndDeleted(documentId, deleted);
		return persistenceToDomain(commentEntities);
	}


	@Override
	public List<Comment> findByDocumentIdAndCreatedBy(DocumentId documentId, User user) {

		if ((user == null) || (user.getGid() == null)) {
			return Collections.emptyList();
		}

		List<CommentEntity> byDocumentIdAndCreatedBy = commentRepository.findByDocumentIdAndCreatedBy(documentId, user.getGid().getId());

		if (!byDocumentIdAndCreatedBy.isEmpty()) {
			return byDocumentIdAndCreatedBy.stream()
				.map(commentMapper::toDomainItem)
				.collect(Collectors.toList());
		}

		return Collections.emptyList();

	}

	@Override
	public Comment save(Comment comment) {

		Optional<CommentEntity> optionalCommentEntity = commentRepository.findByCommentId(comment.getCommentId());

		CommentEntity commentEntity = optionalCommentEntity.orElseGet(() -> CommentEntity.builder().build());
		CommentEntity mergedCommentEntity = commentMapper.merge(comment, commentEntity);

		CommentEntity savedCommentEntity = commentRepository.save(mergedCommentEntity);
		return getDocumentDomain(savedCommentEntity);
	}

	@Override
	public boolean existsCommentByCommentId(CommentId commentId) {
		return commentRepository.existsCommentByCommentId(commentId);
	}

	@Override
	public boolean existsCommentsCreatedOrUpdatedByUser(final UserId userId) {
		return commentRepository.existsByCreatedByOrUpdatedBy(userId.getId(), userId.getId());
	}
}
