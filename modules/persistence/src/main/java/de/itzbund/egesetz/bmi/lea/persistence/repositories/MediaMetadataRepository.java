// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import de.itzbund.egesetz.bmi.lea.persistence.entities.MediaDataEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.MediaMetadataEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
@SuppressWarnings("unused")
public interface MediaMetadataRepository extends BaseRepo<MediaMetadataEntity, UUID> {

    @Query(value = "SELECT CASE WHEN COUNT(meta)> 0 "
        + "THEN true ELSE false END FROM MediaMetadataEntity meta WHERE meta.mediaData =:foreignKey")
    boolean checkForeignKeyHasRelation(@Param("foreignKey") MediaDataEntity foreignKey);

}
