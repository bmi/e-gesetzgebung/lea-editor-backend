// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.mappers;

import de.itzbund.egesetz.bmi.lea.domain.model.DrucksacheDokument;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DrucksacheDokumentEntity;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true),
    uses = {
        BaseMapperPersistence.class
    }
)
public interface DrucksacheDokumentMapperPersistence {

    @Mapping(target = "createdBy", source = "createdBy", qualifiedByName = "userStringMapping")
    @Mapping(target = "updatedBy", source = "updatedBy", qualifiedByName = "userStringMapping")
    DrucksacheDokumentEntity fromDomainItem(DrucksacheDokument drucksacheDokument);

    @Mapping(target = "createdBy", source = "createdBy", qualifiedByName = "userStringMapping")
    @Mapping(target = "updatedBy", source = "updatedBy", qualifiedByName = "userStringMapping")
    DrucksacheDokument toDomainItem(DrucksacheDokumentEntity drucksacheDokument);

}
