// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities.converter;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.UUID;

@Converter(autoApply = true)
@SuppressWarnings("unused")
public class CompoundDocumentIdConverter implements AttributeConverter<CompoundDocumentId, String> {

    @Override
    public String convertToDatabaseColumn(CompoundDocumentId compoundDocumentId) {
        if (compoundDocumentId == null) {
            return null;
        }

        return compoundDocumentId.getId().toString();
    }


    @Override
    public CompoundDocumentId convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return null;
        }

        // throws IllegalArgumentException if name does not conform to a valid string representation
        return new CompoundDocumentId(UUID.fromString(dbData));
    }
}
