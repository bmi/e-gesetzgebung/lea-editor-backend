// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities.listeners;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.persistence.entities.CommentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.CompoundDocumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DMBestandsrechtLinkEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DocumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.EgfaDataEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.EgfaMetadataEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.ReplyEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.UserSettingsEntity;
import lombok.extern.log4j.Log4j2;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.Instant;
import java.util.Map;

@Log4j2
public class CreatedOrUpdatedEntityListener {

    @PrePersist
    public void setCreated(Object entity) {

        // Ab Java 17 (LTE): Pattern Matching Feature
        if (entity instanceof CompoundDocumentEntity) {
            compoundDocumentCreated((CompoundDocumentEntity) entity);
        } else if (entity instanceof DocumentEntity) {
            documentCreated((DocumentEntity) entity);
        } else if (entity instanceof CommentEntity) {
            commentCreated((CommentEntity) entity);
        } else if (entity instanceof ReplyEntity) {
            replyCreated((ReplyEntity) entity);
        } else if (entity instanceof UserSettingsEntity) {
            userSettingsCreated((UserSettingsEntity) entity);
        } else if (entity instanceof EgfaDataEntity) {
            egfaDataCreated((EgfaDataEntity) entity);
        } else if (entity instanceof EgfaMetadataEntity) {
            egfaMetaDataCreated((EgfaMetadataEntity) entity);
        } else if (entity instanceof DMBestandsrechtLinkEntity) {
            dmBestandsrechtLinkCreated((DMBestandsrechtLinkEntity) entity);
        }

        setModified(entity);
    }

    void egfaMetaDataCreated(EgfaMetadataEntity egfaMetadataEntity) {
        if (egfaMetadataEntity.getCreatedAt() == null) {
            egfaMetadataEntity.setCreatedAt(Instant.now());
        }

        if (egfaMetadataEntity.getCreatedByUser() == null) {
            egfaMetadataEntity.setCreatedByUser(getGid().getId());
        }
    }

    void egfaDataCreated(EgfaDataEntity egfaDataEntity) {
        if (egfaDataEntity.getCreatedAt() == null) {
            egfaDataEntity.setCreatedAt(Instant.now());
        }

        if (egfaDataEntity.getTransmittedByUser() == null) {
            egfaDataEntity.setTransmittedByUser(getGid().getId());
        }
    }

    void userSettingsCreated(UserSettingsEntity userSettingsEntity) {
        if (userSettingsEntity.getCreatedAt() == null) {
            userSettingsEntity.setCreatedAt(Instant.now());
        }
    }

    void replyCreated(ReplyEntity replyEntity) {
        if (replyEntity.getCreatedAt() == null) {
            replyEntity.setCreatedAt(Instant.now());
        }

        if (replyEntity.getCreatedBy() == null) {
            replyEntity.setCreatedBy(getGid().getId());
        }
    }

    void commentCreated(CommentEntity commentEntity) {
        if (commentEntity.getCreatedAt() == null) {
            commentEntity.setCreatedAt(Instant.now());
        }

        if (commentEntity.getCreatedBy() == null) {
            commentEntity.setCreatedBy(getGid().getId());
        }
    }

    void documentCreated(DocumentEntity documentEntity) {
        if (documentEntity.getCreatedAt() == null) {
            documentEntity.setCreatedAt(Instant.now());
        }

        if (documentEntity.getCreatedBy() == null) {
            documentEntity.setCreatedBy(getGid().getId());
        }
    }

    void compoundDocumentCreated(CompoundDocumentEntity compoundDocumentEntity) {
        if (compoundDocumentEntity.getCreatedAt() == null) {
            compoundDocumentEntity.setCreatedAt(Instant.now());
        }

        if (compoundDocumentEntity.getCreatedBy() == null) {
            compoundDocumentEntity.setCreatedBy(getGid().getId());
        }
    }

    void dmBestandsrechtLinkCreated(DMBestandsrechtLinkEntity entity) {
        if (entity.getCreatedAt() == null) {
            entity.setCreatedAt(Instant.now());
        }

        if (entity.getCreatedBy() == null) {
            entity.setCreatedBy(getGid().getId());
        }
    }

    @PreUpdate
    public void setModified(Object entity) {
        // Ab Java 17 (LTE): Pattern Matching Feature
        if (entity instanceof CompoundDocumentEntity) {
            compoundDocumentUpdated((CompoundDocumentEntity) entity);
        } else if (entity instanceof DocumentEntity) {
            documentUpdated((DocumentEntity) entity);
        } else if (entity instanceof CommentEntity) {
            commentUpdated((CommentEntity) entity);
        } else if (entity instanceof ReplyEntity) {
            replyUpdated((ReplyEntity) entity);
        } else if (entity instanceof UserSettingsEntity) {
            userSettingsUpdated((UserSettingsEntity) entity);
        } else if (entity instanceof EgfaDataEntity) {
            egfaDataUpdated((EgfaDataEntity) entity);
        } else if (entity instanceof EgfaMetadataEntity) {
            egfaMetaDataUpdated((EgfaMetadataEntity) entity);
        } else if (entity instanceof DMBestandsrechtLinkEntity) {
            dmBestandsrechtLinkUpdated((DMBestandsrechtLinkEntity) entity);
        }

    }

    void egfaMetaDataUpdated(EgfaMetadataEntity egfaMetadataEntity) {
        // Nothing
    }

    void egfaDataUpdated(EgfaDataEntity egfaDataEntity) {
        egfaDataEntity.setCompletedAt(Instant.now());
    }

    void userSettingsUpdated(UserSettingsEntity userSettingsEntity) {
        userSettingsEntity.setUpdatedAt(Instant.now());
    }

    void dmBestandsrechtLinkUpdated(DMBestandsrechtLinkEntity entity) {
        entity.setUpdatedAt(Instant.now());
        if (!getGid().getId().isEmpty()) {
            entity.setUpdatedBy(getGid().getId());
        }
    }

    void replyUpdated(ReplyEntity replyEntity) {
        replyEntity.setUpdatedAt(Instant.now());
        if (!getGid().getId().isEmpty()) {
            replyEntity.setUpdatedBy(getGid().getId());
        }
    }

    void commentUpdated(CommentEntity commentEntity) {
        commentEntity.setUpdatedAt(Instant.now());
        if (!getGid().getId().isEmpty()) {
            commentEntity.setUpdatedBy(getGid().getId());
        }
    }

    void documentUpdated(DocumentEntity documentEntity) {
        documentEntity.setUpdatedAt(Instant.now());
        if (!getGid().getId().isEmpty()) {
            documentEntity.setUpdatedBy(getGid().getId());
        }
    }

    void compoundDocumentUpdated(CompoundDocumentEntity compoundDocumentEntity) {
        compoundDocumentEntity.setUpdatedAt(Instant.now());
        if (!getGid().getId().isEmpty()) {
            compoundDocumentEntity.setUpdatedBy(getGid().getId());
        }
    }

    protected UserId getGid() {
        Map<String, String> userDatailsFromAuthentication = LeageSession.getUserDatailsFromAuthentication();

        if (userDatailsFromAuthentication.isEmpty()) {
            log.error("Authentication is missing for database entries.");
        }
        
        return new UserId(userDatailsFromAuthentication.get(LeageSession.GID));
    }

}
