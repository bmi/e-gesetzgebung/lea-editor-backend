// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.configuration;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

public class CustomAuditingEntityListener extends AuditingEntityListener {

    @Override
    public void touchForCreate(Object target) {
        // Benutzerdefinierte Logik zum Setzen des Erstellers
        super.touchForCreate(target);

//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        if (authentication == null || !authentication.isAuthenticated()) {
//            return Optional.empty(); // Falls keine Authentifizierung vorliegt
//        }
    }

//    @Override
//    public Optional<String> getCurrentAuditor() {
//        // Hier könntest du die Logik implementieren, um den aktuellen Benutzer zu erhalten
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        if (authentication == null || !authentication.isAuthenticated()) {
//            return Optional.empty(); // Falls keine Authentifizierung vorliegt
//        }
//        return Optional.of(authentication.getName());
//    }
}
