// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import de.itzbund.egesetz.bmi.lea.domain.enums.egfa.datenuebernahme.EgfaModuleType;
import de.itzbund.egesetz.bmi.lea.domain.model.EgfaMetadata;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.EgfaMetadataPersistencePort;
import de.itzbund.egesetz.bmi.lea.persistence.UserRepository;
import de.itzbund.egesetz.bmi.lea.persistence.entities.EgfaMetadataEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.EgfaMetadataMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.EgfaMetadataRepository;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@AllArgsConstructor
@Service
public class EgfaMetadataPersistenceAdapter implements EgfaMetadataPersistencePort {

    // ----- Repositories -------
    private final EgfaMetadataRepository egfaMetadataRepository;
    private final UserRepository userRepository;

    // ----- Mapper -------------
    private final EgfaMetadataMapperPersistence egfaMetadataMapper;
    private final UserMapperPersistence userMapper;

    @Override
    public Optional<EgfaMetadata> findNewestByCompoundDocumentIdAndModuleName(CompoundDocumentId compoundDocumentId, EgfaModuleType moduleName) {

		return egfaMetadataRepository
			.findTopByCompoundDocumentIdAndModuleNameOrderByCreatedAtDesc(compoundDocumentId, moduleName)
			.map(this::getEgfaMetaData);

	}

	@Override
	public boolean existsEgfaMetadataCreatedByUser(final UserId userId) {
		return egfaMetadataRepository.existsByCreatedByUser(userId.getId());
	}

	private EgfaMetadata getEgfaMetaData(EgfaMetadataEntity de) {
        EgfaMetadata cdd = egfaMetadataMapper.toDomainItem(de);
        cdd.setCreatedByUserId(getDetailedUser(de.getCreatedByUser()));
        return cdd;
    }

    private User getDetailedUser(String userGid) {
        NutzerEntity creator = userRepository.findFirstByGid(new UserId(userGid));
        return userMapper.toDomainItem(creator);
    }

    @Override
    public EgfaMetadata save(EgfaMetadata egfaMetadata) {
        EgfaMetadataEntity egfaMetadataEntity = egfaMetadataMapper.fromDomainItem(egfaMetadata);
        EgfaMetadataEntity egfaMetadataEntity1 = egfaMetadataRepository.save(egfaMetadataEntity);
        return getEgfaMetaData(egfaMetadataEntity1);
    }
}
