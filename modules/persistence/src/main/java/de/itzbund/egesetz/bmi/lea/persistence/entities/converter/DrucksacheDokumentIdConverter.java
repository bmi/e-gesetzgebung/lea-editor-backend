// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities.converter;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.DrucksacheDokumentId;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.UUID;

/**
 * A JPA converter to map {@link de.itzbund.egesetz.bmi.lea.domain.model.vo.DrucksacheDokumentId}s to a database table column and vice versa.
 */
@Converter(autoApply = true)
@SuppressWarnings("unused")
public class DrucksacheDokumentIdConverter implements AttributeConverter<DrucksacheDokumentId, String> {

	@Override
	public String convertToDatabaseColumn(DrucksacheDokumentId drucksacheDokumentId) {
		if (drucksacheDokumentId == null) {
			return null;
		}

		return drucksacheDokumentId.getId()
			.toString();
	}

	@Override
	public DrucksacheDokumentId convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return null;
		}

		// throws IllegalArgumentException if name does not conform to a valid string representation
		return new DrucksacheDokumentId(UUID.fromString(dbData));
	}
}
