// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.mappers;

import de.itzbund.egesetz.bmi.lea.domain.model.MediaMetadata;
import de.itzbund.egesetz.bmi.lea.persistence.entities.MediaMetadataEntity;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true))
public interface MediaMetadataMapperPersistence {

    MediaMetadataEntity merge(MediaMetadata mediaMetadata,
        @MappingTarget MediaMetadataEntity mediaMetadataEntity);

    MediaMetadataEntity fromDomainItem(MediaMetadata mediaMetadata);

    MediaMetadata toDomainItem(MediaMetadataEntity mediaMetadataEntity);

}
