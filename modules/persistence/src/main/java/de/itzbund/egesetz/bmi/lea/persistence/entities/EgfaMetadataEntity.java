// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities;

import de.itzbund.egesetz.bmi.lea.domain.enums.egfa.datenuebernahme.EgfaModuleType;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.EgfaDataId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.EgfaMetadataId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.CompoundDocumentIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.DocumentIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.EgfaDataIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.EgfaMetadataIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.RegelungsVorhabenIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.listeners.CreatedOrUpdatedEntityListener;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.NaturalId;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.time.Instant;

@Generated
@Entity
@EntityListeners({AuditingEntityListener.class, CreatedOrUpdatedEntityListener.class})
@Table(name = "egfa_metadata")
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class EgfaMetadataEntity extends BaseEntity {

    @EqualsAndHashCode.Include
    @Convert(converter = EgfaMetadataIdConverter.class, attributeName = "id")
    @ToString.Include
    @NaturalId
    private EgfaMetadataId egfaMetadataId;

    @EqualsAndHashCode.Exclude
    @Convert(converter = DocumentIdConverter.class, attributeName = "id")
    @ToString.Include
    private DocumentId documentId;

    @EqualsAndHashCode.Exclude
    @Convert(converter = RegelungsVorhabenIdConverter.class, attributeName = "id")
    @ToString.Include
    private RegelungsVorhabenId regelungsVorhabenId;

    @EqualsAndHashCode.Exclude
    @ToString.Include
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "varchar(255) not null")
    private EgfaModuleType moduleName;

    @Column(nullable = false)
    @EqualsAndHashCode.Exclude
    private Instant moduleCompletedAt;

    @CreatedDate
    @Column(updatable = false, nullable = false)
    @EqualsAndHashCode.Exclude
    private Instant createdAt;

    @CreatedBy
    @EqualsAndHashCode.Exclude
    @Column(name = "created_by_user_id")
    private String createdByUser;

    @EqualsAndHashCode.Exclude
    @Convert(converter = EgfaDataIdConverter.class, attributeName = "id")
    @ToString.Include
    private EgfaDataId egfaDataId;

    @EqualsAndHashCode.Exclude
    @Convert(converter = CompoundDocumentIdConverter.class, attributeName = "id")
    @ToString.Include
    private CompoundDocumentId compoundDocumentId;

}
