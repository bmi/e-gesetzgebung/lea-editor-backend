// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.persistence.UserRepository;
import de.itzbund.egesetz.bmi.lea.persistence.entities.CompoundDocumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.CompoundDocumentMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.CompoundDocumentRepository;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

@AllArgsConstructor
@Service
@Log4j2
public class CompoundDocumentPersistenceAdapter implements CompoundDocumentPersistencePort {

	private static final String SEARCH_FIELD = "updatedAt";
	public static final String VERSION = "version";

	// ----- Repositories -------
	private final CompoundDocumentRepository compoundDocumentRepository;
	private final UserRepository userRepository;

	// ----- Mapper -------------
	private final CompoundDocumentMapperPersistence compoundDocumentMapper;
	private final UserMapperPersistence userMapper;

	@Override
	public List<CompoundDocumentDomain> findAll() {

		List<CompoundDocumentEntity> all = compoundDocumentRepository.findAll();
		return persistenceToDomain(all);
	}

	@Override
	public List<CompoundDocumentDomain> findByCreatedBy(User user) {

		if ((user != null) && (user.getGid() != null)) {
			List<CompoundDocumentEntity> byCreatedBy = compoundDocumentRepository.findByCreatedBy(user.getGid().getId());
			return persistenceToDomain(byCreatedBy);
		}

		return Collections.emptyList();
	}


	private List<CompoundDocumentDomain> persistenceToDomain(List<CompoundDocumentEntity> documentEntities) {
		Set<String> userIds = new HashSet<>();
		documentEntities.forEach(x -> {
				userIds.add(x.getUpdatedBy());
				userIds.add(x.getCreatedBy());
			}
		);
		Map<String, NutzerEntity> userEntities = userRepository.findByGidIn(userIds);

		return documentEntities.stream()
			.map(x -> getCompoundDocumentDomain(x, userMapper.toDomainItem(userEntities.get(x.getCreatedBy())),
				userMapper.toDomainItem(userEntities.get(x.getUpdatedBy()))))
			.collect(Collectors.toList());
	}


	private CompoundDocumentDomain getCompoundDocumentDomain(CompoundDocumentEntity de) {
		CompoundDocumentDomain cdd = compoundDocumentMapper.toDomainItem(de);
		cdd.setCreatedBy(getDetailedUser(de.getCreatedBy()));
		cdd.setUpdatedBy(getDetailedUser(de.getUpdatedBy()));
		return cdd;
	}


	private CompoundDocumentDomain getCompoundDocumentDomain(CompoundDocumentEntity de, User createdBy, User updatedBy) {
		CompoundDocumentDomain cdd = compoundDocumentMapper.toDomainItem(de);
		cdd.setCreatedBy(createdBy);
		cdd.setUpdatedBy(updatedBy);
		return cdd;
	}

	private User getDetailedUser(String userGid) {
		NutzerEntity creator = userRepository.findFirstByGid(new UserId(userGid));
		return userMapper.toDomainItem(creator);
	}


	/**
	 * Return the newest compound document of a Regelungsvorhaben in the list and only one and only for the given user.
	 *
	 * @param regelungsVorhabenIds A list of Regelungsvorhaben
	 * @param user                 The user
	 * @return A list of compound documents
	 */
	@Override
	public List<CompoundDocumentDomain> findLatestCompoundDocumentForRegelungsvorhabenListAndCreatedBy(List<RegelungsVorhabenId> regelungsVorhabenIds,
		User user) {
		if ((user != null) && (user.getGid() != null)) {

			List<String> convert = regelungsVorhabenIds.stream().map(rv -> rv.getId().toString()).collect(Collectors.toList());

			List<CompoundDocumentEntity> compoundDocumentEntities = Collections.emptyList();
			try {
				compoundDocumentEntities = compoundDocumentRepository.findLatestByRegelungsVorhabenIdsAndCreatedBy(
					convert, user.getGid().getId());
			} catch (Exception e) { //NOSONAR
				log.error(e);
			}

			return persistenceToDomain(compoundDocumentEntities);
		}

		return Collections.emptyList();
	}

	@Override
	public List<CompoundDocumentDomain> findByCompoundDocumentIdIn(List<CompoundDocumentId> compoundDocumentIds) {
		List<CompoundDocumentEntity> compoundDocumentEntities = compoundDocumentRepository.findByCompoundDocumentIdIn(compoundDocumentIds);
		return persistenceToDomain(compoundDocumentEntities);
	}

	@Override
	public List<CompoundDocumentDomain> findByCompoundDocumentIdInAndCompoundDocumentState(List<CompoundDocumentId> compoundDocumentIds, DocumentState state) {
		List<CompoundDocumentEntity> compoundDocumentEntities = compoundDocumentRepository.findByCompoundDocumentIdInAndState(compoundDocumentIds, state);
		return persistenceToDomain(compoundDocumentEntities);
	}

	@Override
	public List<CompoundDocumentDomain> findByStateSortedByRegelungsVorhabenId(DocumentState state) {
		List<CompoundDocumentEntity> compoundDocumentEntities = compoundDocumentRepository.findByStateOrderByRegelungsVorhabenIdDesc(state);
		return persistenceToDomain(compoundDocumentEntities);
	}

	@Override
	public Optional<CompoundDocumentDomain> findByCompoundDocumentId(CompoundDocumentId id) {
		Optional<CompoundDocumentEntity> byCompoundDocumentId = compoundDocumentRepository.findByCompoundDocumentId(id);
		if (byCompoundDocumentId.isPresent()) {
			return Optional.of(getCompoundDocumentDomain(byCompoundDocumentId.get()));
		}

		return Optional.empty();
	}

	@Override
	public Optional<CompoundDocumentDomain> findByCreatedByAndCompoundDocumentId(User user, CompoundDocumentId compoundDocumentId) {

		if ((user != null) && (user.getGid() != null)) {
			Optional<CompoundDocumentEntity> optionalCompoundDocumentEntity = compoundDocumentRepository.findByCreatedByAndCompoundDocumentId(
				user.getGid().getId(), compoundDocumentId);

			if (optionalCompoundDocumentEntity.isPresent()) {
				return Optional.of(getCompoundDocumentDomain(optionalCompoundDocumentEntity.get()));
			}
		}

		return Optional.empty();
	}

	@Override
	public List<CompoundDocumentDomain> findByCreatedByAndRegelungsVorhabenId(User user, RegelungsVorhabenId regelungsVorhabenId) {

		if ((user != null) && (user.getGid() != null)) {

			List<CompoundDocumentEntity> compoundDocumentEntities = compoundDocumentRepository.findByCreatedByAndRegelungsVorhabenId(
				user.getGid().getId(), regelungsVorhabenId);

			return persistenceToDomain(compoundDocumentEntities);
		}

		return Collections.emptyList();
	}

	@Override
	public Page<CompoundDocumentDomain> findByRegelungsvorhabenIdInSortedByUpdatedAt(List<RegelungsVorhabenId> ids, Pageable pageable) {

		Optional<Page<CompoundDocumentEntity>> compoundDocumentEntities = compoundDocumentRepository.findByRegelungsVorhabenIdInOrderByUpdatedAtDesc(ids,
			pageable);

		Page<CompoundDocumentEntity> compoundDocumentEntityPage = compoundDocumentEntities.orElseGet(null);
		if (compoundDocumentEntityPage != null) {

			List<CompoundDocumentDomain> processedItems = compoundDocumentEntityPage.getContent().stream()
				.map(this::getCompoundDocumentDomain)
				.collect(Collectors.toList());

			return new PageImpl<>(processedItems, compoundDocumentEntityPage.getPageable(), compoundDocumentEntityPage.getTotalElements());
		}

		return Page.empty();
	}


	@Override
	public Page<CompoundDocumentDomain> findByCreatedBySortedByUpdatedAt(User user, Pageable pageable) {

		if ((user != null) && (user.getGid() != null)) {

			Optional<Page<CompoundDocumentEntity>> byCreatedByOptional = compoundDocumentRepository.findByCreatedBy(user.getGid().getId(), pageable);

			if (byCreatedByOptional.isEmpty()) {
				return Page.empty();
			}

			Page<CompoundDocumentEntity> byCreatedBy = byCreatedByOptional.get();
			List<CompoundDocumentDomain> processedItems = byCreatedBy.getContent().stream()
				.map(compoundDocumentMapper::toDomainItem)
				.collect(Collectors.toList());

			return new PageImpl<>(processedItems, byCreatedBy.getPageable(), byCreatedBy.getTotalElements());
		}

		return null;
	}


	@Override
	public List<CompoundDocumentDomain> findByRegelungsvorhabenIdInSortedByUpdatedAt(User user, List<RegelungsVorhabenId> ids) {

		if (user == null || user.getGid() == null) {
			return Collections.emptyList();
		}

		Sort sort = Sort.by(SEARCH_FIELD).descending();
		Pageable pageable = PageRequest.of(0, Integer.MAX_VALUE, sort);

		Page<CompoundDocumentEntity> byCreatedBy = compoundDocumentRepository.findByCreatedByAndRegelungsVorhabenIdIn(
			user.getGid().getId(), ids, pageable);

		return byCreatedBy.getContent().stream()
			.map(compoundDocumentMapper::toDomainItem)
			.collect(Collectors.toList());
	}


	@Override
	public List<CompoundDocumentDomain> findByCreatedBySortedByUpdatedAtRegelungsVorhabenIdNotIn(User user, List<RegelungsVorhabenId> ids) {

		if (user == null || user.getGid() == null) {
			return Collections.emptyList();
		}

		List<CompoundDocumentEntity> compoundDocumentEntities;
		if (ids.isEmpty()) {
			compoundDocumentEntities = compoundDocumentRepository.findByCreatedBy(user.getGid().getId(), Sort.by(Sort.Direction.DESC, SEARCH_FIELD));
		} else {
			compoundDocumentEntities = compoundDocumentRepository.findByCreatedByAndRegelungsVorhabenIdNotIn(user.getGid().getId(), ids,
				Sort.by(Sort.Direction.DESC, SEARCH_FIELD));
		}

		return persistenceToDomain(compoundDocumentEntities);

	}

	@Override
	public List<CompoundDocumentDomain> findByRegelungsVorhabenIdSortedByVersion(RegelungsVorhabenId regelungsVorhabenId) {
		List<CompoundDocumentEntity> compoundDocumentEntities = compoundDocumentRepository.findByRegelungsVorhabenId(regelungsVorhabenId,
			Sort.by(Sort.Direction.DESC, VERSION));

		if (!compoundDocumentEntities.isEmpty()) {
			return persistenceToDomain(compoundDocumentEntities);
		}

		return Collections.emptyList();
	}

	@Override
	public List<CompoundDocumentDomain> findByRegelungsVorhabenIdAndVersionLikeSorted(RegelungsVorhabenId regelungsVorhabenId, String version) {

		List<CompoundDocumentEntity> compoundDocumentEntities = compoundDocumentRepository
			.findByRegelungsVorhabenIdAndVersionLike(regelungsVorhabenId, version, Sort.by(Sort.Direction.DESC, VERSION));

		if (!compoundDocumentEntities.isEmpty()) {
			return persistenceToDomain(compoundDocumentEntities);
		}

		return Collections.emptyList();
	}


	@Override
	public List<CompoundDocumentDomain> findByRegelungsVorhabenIdAndCompoundDocumentState(RegelungsVorhabenId regelungsVorhabenId,
		DocumentState state) {

		List<CompoundDocumentEntity> compoundDocumentEntities = compoundDocumentRepository.findByRegelungsVorhabenIdAndState(regelungsVorhabenId, state);

		if (!compoundDocumentEntities.isEmpty()) {
			return persistenceToDomain(compoundDocumentEntities);
		}

		return Collections.emptyList();
	}

	@Override
	public boolean existsCompoundDocumentsCreatedOrUpdatedByUser(final UserId userId) {
		return compoundDocumentRepository.existsByCreatedByOrUpdatedBy(userId.getId(), userId.getId());
	}


	@Override
	public List<CompoundDocumentDomain> findByCreatedByAndRegelungsvorhabenIdAndState(User user, RegelungsVorhabenId regelungsVorhabenId,
		DocumentState state) {

		if (user == null || user.getGid() == null) {
			return Collections.emptyList();
		}

		List<CompoundDocumentEntity> compoundDocumentEntities = compoundDocumentRepository
			.findByCreatedByAndRegelungsVorhabenIdAndState(user.getGid().getId(), regelungsVorhabenId, state);

		if (!compoundDocumentEntities.isEmpty()) {
			return persistenceToDomain(compoundDocumentEntities);
		}

		return Collections.emptyList();
	}

	@Override
	public CompoundDocumentDomain save(CompoundDocumentDomain compoundDocumentDomain) {

		CompoundDocumentEntity compoundDocumentEntity = compoundDocumentRepository
			.findByCompoundDocumentId(compoundDocumentDomain.getCompoundDocumentId())
			.orElseGet(() -> CompoundDocumentEntity.builder().build());

		CompoundDocumentEntity mergedCompoundDocumentEntity = compoundDocumentMapper.merge(compoundDocumentDomain, compoundDocumentEntity);

		CompoundDocumentEntity savedCompoundDocumentEntity = compoundDocumentRepository.save(mergedCompoundDocumentEntity);
		return getCompoundDocumentDomain(savedCompoundDocumentEntity);
	}

}
