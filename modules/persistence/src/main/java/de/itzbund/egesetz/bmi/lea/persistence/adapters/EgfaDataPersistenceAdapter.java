// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.enums.egfa.datenuebernahme.EgfaModuleType;
import de.itzbund.egesetz.bmi.lea.domain.model.EgfaData;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.EgfaDataPersistencePort;
import de.itzbund.egesetz.bmi.lea.persistence.UserRepository;
import de.itzbund.egesetz.bmi.lea.persistence.entities.EgfaDataEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.EgfaDataMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.EgfaDataRepository;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class EgfaDataPersistenceAdapter implements EgfaDataPersistencePort {

    // ----- Repositories -------v
    private final EgfaDataRepository egfaDataRepository;
    private final UserRepository userRepository;

    // ----- Mapper -------------
    private final EgfaDataMapperPersistence egfaDataMapper;
    private final UserMapperPersistence userMapper;

    @Override
    public EgfaData findNewestByRegelungsVorhabenIdAndModuleName(RegelungsVorhabenId regelungsVorhabenId, EgfaModuleType moduleName) {

		return egfaDataRepository.findTopByRegelungsVorhabenIdAndModuleNameOrderByCompletedAtDesc(regelungsVorhabenId, moduleName)
			.map(this::getEgfaDomain)
			.orElse(null);

	}

	@Override
	public boolean existsEgfaDataTransmittedByUser(final UserId userId) {
		return egfaDataRepository.existsByTransmittedByUser(userId.getId());
	}

	private EgfaData getEgfaDomain(EgfaDataEntity de) {
        EgfaData egfaData = egfaDataMapper.toDomainItem(de);
        egfaData.setTransmittedByUser(getDetailedUser(de.getTransmittedByUser()));
        return egfaData;
    }

    private User getDetailedUser(String userGid) {
        NutzerEntity creator = userRepository.findFirstByGid(new UserId(userGid));
        return userMapper.toDomainItem(creator);
    }

    @Override
    public EgfaData save(EgfaData egfaData, List<ServiceState> status) {
        Optional<EgfaDataEntity> optionalExistingEgfaData = egfaDataRepository.findByRegelungsVorhabenIdAndModuleNameAndCompletedAt(
            egfaData.getRegelungsVorhabenId(), egfaData.getModuleName(), egfaData.getCompletedAt());

        if (optionalExistingEgfaData.isPresent()) {
            status.add(ServiceState.NOT_MODIFIED);
            return egfaData;
        }

        EgfaDataEntity egfaDataEntity = egfaDataMapper.fromDomainItem(egfaData);
        EgfaDataEntity egfaDataEntity1 = egfaDataRepository.save(egfaDataEntity);

        status.add(ServiceState.OK);
        return getEgfaDomain(egfaDataEntity1);
    }

}
