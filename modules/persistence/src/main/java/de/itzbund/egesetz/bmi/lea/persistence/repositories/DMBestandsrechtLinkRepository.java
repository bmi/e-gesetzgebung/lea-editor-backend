// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.BestandsrechtId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DMBestandsrechtLinkEntity;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@SuppressWarnings("unused")
public interface DMBestandsrechtLinkRepository extends BaseRepo<DMBestandsrechtLinkEntity, Long> {

    List<DMBestandsrechtLinkEntity> findByBestandsrechtIdIn(List<BestandsrechtId> bestandsrechtIds);

    List<DMBestandsrechtLinkEntity> findByCompoundDocumentIdIn(List<CompoundDocumentId> compoundDocumentIds);

    Optional<DMBestandsrechtLinkEntity> findByCompoundDocumentIdAndBestandsrechtId(CompoundDocumentId compoundDocumentId, BestandsrechtId bestandsrechtId);
}
