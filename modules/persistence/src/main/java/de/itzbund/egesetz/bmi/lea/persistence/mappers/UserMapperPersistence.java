// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.mappers;

import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.UserEntity;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true)
)
public interface UserMapperPersistence {

    @Named("userIdStringMapping")
    default UserId mapToUserId(String uuidAsString) {
        return new UserId(uuidAsString);
    }

    UserEntity merge(User user, @MappingTarget UserEntity userEntity);

    @Deprecated(since = "User via RBAC", forRemoval = false)
    UserEntity fromDomainItem(User domainItem);

    @Deprecated(since = "User via RBAC", forRemoval = false)
    User toDomainItem(UserEntity item);

    @Mapping(target = "gid", source = "gid", qualifiedByName = "userIdStringMapping")
    @Mapping(target = "plattformUserId", source = "id")
    User toDomainItem(NutzerEntity item);

    @Mapping(target = "gid", source = "gid", qualifiedByName = "userIdStringMapping")
    UserEntity mapEntites(NutzerEntity item);

}
