// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import java.util.List;
import org.springframework.stereotype.Service;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserPersistencePort;
import de.itzbund.egesetz.bmi.lea.persistence.UserRepository;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistence;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class UserPersistenceAdapter implements UserPersistencePort {

	// ----- Repositories -------
	final UserRepository repository;

	// ----- Mapper -------------
	private final UserMapperPersistence userMapper;

	@Override
	public User findFirstByGid(UserId userId) {
		return userMapper.toDomainItem(repository.findFirstByGid(userId));
	}

	@Override
	public List<UserId> findMarkedAsDeleted() {
		return repository.findDeletedNutzerWithoutStellvertreterOrZuordnung();
	}

	@Override
	public void markUserAsNotReferenced(final UserId userId) {
		repository.setEditorNoRef(userId);
	}

}
