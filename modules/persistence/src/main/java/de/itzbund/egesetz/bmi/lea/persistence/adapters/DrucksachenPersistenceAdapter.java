// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import de.itzbund.egesetz.bmi.lea.domain.model.Drucksache;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DrucksachenPersistencePort;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DrucksachenEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.DrucksacheMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.DrucksachenRepository;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
@Log4j2
public class DrucksachenPersistenceAdapter implements DrucksachenPersistencePort {


    // ----- Repositories -------
    private final DrucksachenRepository drucksachenRepository;

    // ----- Mapper -------------
    private final DrucksacheMapperPersistence drucksacheMapperPersistence;

    @Override
    public List<Drucksache> findAll() {
        List<DrucksachenEntity> all = drucksachenRepository.findAll();
        return persistenceListToDomainList(all);
    }

    @Override
    public Drucksache findByDrucksachenNr(String drucksachenNummer) {

        DrucksachenEntity drucksachenEntity = drucksachenRepository.findByDrucksachenNr(drucksachenNummer);
        if (drucksachenEntity == null) {
            log.debug("Die Drucksache '{}' wurde nicht gefunden.", drucksachenNummer);
            return null;
        }

        return persistenceElementToDomainElement(drucksachenEntity);
    }

    @Override
    public Drucksache findByRegelungsvorhabenId(RegelungsVorhabenId regelungsVorhabenId) {

        DrucksachenEntity drucksachenEntity = drucksachenRepository.findByRegelungsvorhabenId(regelungsVorhabenId);
        if (drucksachenEntity == null) {
            log.debug("Das Regelungsvorhaben der Drucksache '{}' wurde nicht gefunden.", regelungsVorhabenId);
            return null;
        }

        return persistenceElementToDomainElement(drucksachenEntity);
    }

    /**
     * Es darf die Drucksache nur EINMAL geben. Diese Methode verhindert ein weiteres Speichern einer schon existierenden Drucknummer.
     *
     * @param drucksache Die Drucksache, die gespeichert werden soll
     * @return Die gespeicherte Drucksache oder null
     */
    @Override
    public Drucksache save(Drucksache drucksache) {

        DrucksachenEntity drucksachenEntity = drucksachenRepository.findByDrucksachenNr(drucksache.getDrucksachenNr());

        if (drucksachenEntity == null) {
            DrucksachenEntity saved = null;
            DrucksachenEntity drucksachenEntity1 = drucksacheMapperPersistence.fromDomainItem(drucksache);
            try {
                saved = drucksachenRepository.save(drucksachenEntity1);
            } catch (Exception e) { //NOSONAR
                log.error(e);
            }
            return persistenceElementToDomainElement(saved);
        }

        return null;
    }

// ==========================

    private List<Drucksache> persistenceListToDomainList(List<DrucksachenEntity> drucksachenEntities) {
        return drucksachenEntities.stream()
            .map(this::persistenceElementToDomainElement)
            // Null-Werte filtern
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    private Drucksache persistenceElementToDomainElement(DrucksachenEntity de) {
        if (de != null) {
            return drucksacheMapperPersistence.toDomainItem(de);
        }

        return null;
    }

}
