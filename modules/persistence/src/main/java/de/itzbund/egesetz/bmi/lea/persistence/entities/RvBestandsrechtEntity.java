// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.BestandsrechtId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RvBestandsrechtId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.BestandsrechtIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.RegelungsVorhabenIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.RvBestandsrechtIdConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.NaturalId;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Generated
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "rv_bestandsrecht")
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class RvBestandsrechtEntity extends BaseEntity {

    @EqualsAndHashCode.Include
    @Convert(converter = RvBestandsrechtIdConverter.class, attributeName = "id")
    @ToString.Include
    @NaturalId
    private RvBestandsrechtId rvBestandsrechtId;

    @EqualsAndHashCode.Exclude
    @Convert(converter = RegelungsVorhabenIdConverter.class, attributeName = "id")
    @ToString.Include
    private RegelungsVorhabenId regelungsVorhabenId;

    @EqualsAndHashCode.Exclude
    @Convert(converter = BestandsrechtIdConverter.class, attributeName = "id")
    @ToString.Include
    private BestandsrechtId bestandsrechtId;

}
