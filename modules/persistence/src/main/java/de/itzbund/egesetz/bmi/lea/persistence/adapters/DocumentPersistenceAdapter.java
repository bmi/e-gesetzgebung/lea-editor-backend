// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import java.time.Instant;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import de.itzbund.egesetz.bmi.lea.domain.entities.DocumentCount;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.mappers.BaseMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.persistence.UserRepository;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DocumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.DocumentMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.projections.DocumentMetadataProjection;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.CompoundDocumentRepository;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.DocumentRepository;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class DocumentPersistenceAdapter implements DokumentPersistencePort {

	// ----- Repositories -------
	private final DocumentRepository documentRepository;
	private final CompoundDocumentRepository compoundDocumentRepository;
	private final UserRepository userRepository;

	// ----- Repositories -------
	private final DocumentMapperPersistence documentMapper;
	private final UserMapperPersistence userMapper;
	private final BaseMapper baseMapper;

	@Override
	public List<DocumentDomain> findByCreatedBy(User user) {

		if ((user != null) && (user.getGid() != null)) {
			List<DocumentEntity> byCreatedBy = documentRepository.findByCreatedBy(user.getGid().getId());
			return persistenceToDomain(byCreatedBy);
		}

		return Collections.emptyList();
	}

	@Override
	public List<DocumentDomain> findByCreatedByAndCompoundDocumentIdIn(User user, List<CompoundDocumentId> compoundDocumentIds) {

		if ((user != null) && (user.getGid() != null)) {
			List<DocumentEntity> byCreatedByAndCompoundDocumentIdIn = documentRepository.findByCreatedByAndCompoundDocumentIdIn(user.getGid().getId(),
				compoundDocumentIds);
			return persistenceToDomain(byCreatedByAndCompoundDocumentIdIn);
		}

		return Collections.emptyList();
	}

	private List<DocumentDomain> persistenceToDomain(List<DocumentEntity> documentEntities) {
		Set<String> userIds = new HashSet<>();
		documentEntities.forEach(x -> {
				userIds.add(x.getUpdatedBy());
				userIds.add(x.getCreatedBy());
			}
		);
		Map<String, NutzerEntity> userEntities = userRepository.findByGidIn(userIds);

		return documentEntities.stream()
			.map(x -> getDocumentDomain(x, userMapper.toDomainItem(userEntities.get(x.getCreatedBy())),
				userMapper.toDomainItem(userEntities.get(x.getUpdatedBy()))))
			.collect(Collectors.toList());
	}

	private DocumentDomain getDocumentDomain(DocumentEntity de) {
		DocumentDomain dd = documentMapper.toDomainItem(de);
		dd.setCreatedBy(getDetailedUser(de.getCreatedBy()));
		dd.setUpdatedBy(getDetailedUser(de.getUpdatedBy()));
		return dd;
	}

	private DocumentDomain getDocumentDomain(DocumentEntity de, User createdBy, User updatedBy) {
		DocumentDomain dd = documentMapper.toDomainItem(de);
		dd.setCreatedBy(createdBy);
		dd.setUpdatedBy(updatedBy);
		return dd;
	}

	private User getDetailedUser(String userGid) {
		NutzerEntity creator = userRepository.findFirstByGid(new UserId(userGid));
		return userMapper.toDomainItem(creator);
	}

	@Override
	public List<DocumentDomain> findByCreatedByAndDocumentIdIn(User user, List<DocumentId> ids) {

		if ((user != null) && (user.getGid() != null)) {
			List<DocumentEntity> byCreatedByAndDocumentIdIn = documentRepository.findByCreatedByAndDocumentIdIn(user.getGid().getId(), ids);
			return persistenceToDomain(byCreatedByAndDocumentIdIn);
		}

		return Collections.emptyList();
	}

	@Override
	public Optional<DocumentDomain> findByDocumentId(DocumentId id) {
		Optional<DocumentEntity> byDocumentId = documentRepository.findByDocumentId(id);
		DocumentEntity documentEntity = byDocumentId.orElseGet(null);

		if (documentEntity == null) {
			return Optional.empty();
		}
		return Optional.of(getDocumentDomain(documentEntity));
	}

	@Override
	public List<DocumentDomain> findByCreatedByAndCompoundDocumentId(User user, CompoundDocumentId compoundDocumentId) {
		if ((user != null) && (user.getGid() != null)) {

			List<DocumentEntity> byCreatedByAndCompoundDocumentId = documentRepository.findByCreatedByAndCompoundDocumentId(user.getGid().getId(),
				compoundDocumentId);
			return persistenceToDomain(byCreatedByAndCompoundDocumentId);
		}

		return Collections.emptyList();
	}

	@Override
	public Optional<List<DocumentDomain>> findByCompoundDocumentId(CompoundDocumentId compoundDocumentId) {
		List<DocumentEntity> documentEntities = documentRepository.findByCompoundDocumentId(compoundDocumentId);

		List<DocumentDomain> documentDomains = persistenceToDomain(documentEntities);
		if (documentDomains.isEmpty()) {
			return Optional.empty();
		}

		return Optional.of(documentDomains);
	}

	@Override
	public List<DocumentDomain> findByCompoundDocumentIdIn(List<CompoundDocumentId> compoundDocumentIds) {
		List<DocumentMetadataProjection> documentProjections = documentRepository.findByCompoundDocumentIdIn(compoundDocumentIds);

		List<DocumentEntity> documentList = documentProjections.stream().map(
			x -> new DocumentEntity(x.getDocumentId(), x.getDocumentId(), x.getTitle(), "", x.getType(), x.getCreatedBy(), x.getUpdatedBy(), x.getCreatedAt(),
				x.getUpdatedAt(), x.getCompoundDocumentId())).collect(Collectors.toList());

		return persistenceToDomain(documentList);
	}

	@Override
	public DocumentDomain save(DocumentDomain documentDomain) {
		final DocumentEntity documentEntity = documentRepository
			.findByDocumentId(documentDomain.getDocumentId())
			.orElseGet(() -> DocumentEntity.builder().build());

		//Update belonging CompoundDocument field
		compoundDocumentRepository.findByCompoundDocumentId(documentDomain.getCompoundDocumentId())
			.ifPresent(compoundDocumentEntity -> {
				compoundDocumentEntity.setUpdatedAt(Instant.now());
				compoundDocumentRepository.save(compoundDocumentEntity);
			});

		final DocumentEntity mergedDocumentEntity = documentMapper.merge(documentDomain, documentEntity);

		final DocumentEntity savedDocumentEntity = documentRepository.save(mergedDocumentEntity);
		return getDocumentDomain(savedDocumentEntity);
	}

	@Override
	public int countByCompoundDocumentIdAndTypeIs(CompoundDocumentId compoundDocumentId, DocumentType type) {
		return documentRepository.countByCompoundDocumentIdAndTypeIs(compoundDocumentId, type);
	}

	@Override
	public List<DocumentCount> getDocumentCount(CompoundDocumentId compoundDocumentId) {
		return documentRepository.getDocumentCount(compoundDocumentId.getId().toString());
	}

	@Override
	public boolean existsDokumentCreatedOrUpdatedByUser(final UserId userId) {
		return documentRepository.existsByCreatedByOrUpdatedBy(userId.getId(), userId.getId());
	}

}
