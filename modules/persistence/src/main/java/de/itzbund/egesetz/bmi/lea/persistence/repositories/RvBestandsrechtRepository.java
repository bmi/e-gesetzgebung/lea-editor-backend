// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.BestandsrechtId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.RvBestandsrechtEntity;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@SuppressWarnings("unused")
public interface RvBestandsrechtRepository extends BaseRepo<RvBestandsrechtEntity, Long> {

	Optional<RvBestandsrechtEntity> findByRegelungsVorhabenIdAndBestandsrechtId(RegelungsVorhabenId regelungsVorhabenId, BestandsrechtId bestandsrechtId);

	List<RvBestandsrechtEntity> findByRegelungsVorhabenId(RegelungsVorhabenId regelungsVorhabenId);
}
