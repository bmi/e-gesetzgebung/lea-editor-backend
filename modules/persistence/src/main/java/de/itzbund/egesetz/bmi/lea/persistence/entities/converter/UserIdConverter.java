// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities.converter;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * A JPA converter to map {@link UserId}s to a database table column and vice versa.
 */
@Converter(autoApply = true)
@SuppressWarnings("unused")
public class UserIdConverter implements AttributeConverter<UserId, String> {

    @Override
    public String convertToDatabaseColumn(UserId userId) {
        return userId.getId();
    }


    @Override
    public UserId convertToEntityAttribute(String dbData) {
        return new UserId(dbData);
    }
}
