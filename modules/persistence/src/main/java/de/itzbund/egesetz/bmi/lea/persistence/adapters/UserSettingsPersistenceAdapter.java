// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.UserSettings;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserSettingsPersistencePort;
import de.itzbund.egesetz.bmi.lea.persistence.UserRepository;
import de.itzbund.egesetz.bmi.lea.persistence.entities.UserSettingsEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserSettingsMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.UserSettingsRepository;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

@AllArgsConstructor
@Service
@Log4j2
public class UserSettingsPersistenceAdapter implements UserSettingsPersistencePort {

	// ----- Repositories -------
	private final UserSettingsRepository userSettingsRepository;
	private final UserRepository userRepository;

	// ----- Mapper -------------
	private final UserSettingsMapperPersistence userSettingsMapper;
	private final UserMapperPersistence userMapper;

	@Override
	public Optional<UserSettings> get(User user) {
		if (user == null || user.getGid() == null) {
			return Optional.empty();
		}

		try {

			return userSettingsRepository
				.findByRelatedUser(user.getGid().getId())
				.map(this::getUserSettings);

		} catch (Exception e) {  //NOSONAR
			log.error(e);
			return Optional.empty();
		}
	}

	@Override
	public boolean existsUserSettingsByUser(final UserId userId) {
		return userSettingsRepository.existsByRelatedUser(userId.getId());
	}

	private UserSettings getUserSettings(UserSettingsEntity de) {
		UserSettings cdd = userSettingsMapper.toDomainItem(de);
		cdd.setRelatedUser(getDetailedUser(de.getRelatedUser()));
		return cdd;
	}

	private User getDetailedUser(String userGid) {
		NutzerEntity creator = userRepository.findFirstByGid(new UserId(userGid));
		return userMapper.toDomainItem(creator);
	}

	@Override
	public UserSettings save(UserSettings userSettings, List<ServiceState> status) {
		final Optional<UserSettingsEntity> optionalUserSettingsEntity = userSettingsRepository.findByRelatedUser(
			userSettings.getRelatedUser().getGid().getId());
		optionalUserSettingsEntity.ifPresent(userSettingsEntity -> userSettings.setUserSettingsId(userSettingsEntity.getUserSettingsId()));

		final UserSettingsEntity userSettingsEntity = optionalUserSettingsEntity.orElseGet(() -> UserSettingsEntity.builder().build());
		final UserSettingsEntity mergedUserSettingsEntity = userSettingsMapper.merge(userSettings, userSettingsEntity);
		final UserSettingsEntity savedUserSettingsEntity = userSettingsRepository.save(mergedUserSettingsEntity);

		status.add(ServiceState.OK);

		return getUserSettings(savedUserSettingsEntity);
	}

}
