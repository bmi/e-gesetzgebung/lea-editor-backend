// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities.converter;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.UUID;

@Converter(autoApply = true)
@SuppressWarnings("unused")
public class RegelungsVorhabenIdConverter implements AttributeConverter<RegelungsVorhabenId, String> {

    @Override
    public String convertToDatabaseColumn(RegelungsVorhabenId regelungsVorhabenId) {
        if (regelungsVorhabenId == null) {
            return null;
        }

        return regelungsVorhabenId.getId().toString();
    }


    @Override
    public RegelungsVorhabenId convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return null;
        }

        // throws IllegalArgumentException if name does not conform to a valid string representation
        return new RegelungsVorhabenId(UUID.fromString(dbData));
    }
}
