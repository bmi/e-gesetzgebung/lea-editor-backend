// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities;

import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.VerfahrensType;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.persistence.configuration.CustomAuditingEntityListener;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.CompoundDocumentIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.RegelungsVorhabenIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.listeners.CreatedOrUpdatedEntityListener;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.NaturalId;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Generated
@Entity
@EntityListeners({AuditingEntityListener.class, CreatedOrUpdatedEntityListener.class, CustomAuditingEntityListener.class})
@Table(name = "compound_document")
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CompoundDocumentEntity extends BaseEntity {

    @EqualsAndHashCode.Include
    @Convert(converter = CompoundDocumentIdConverter.class, attributeName = "id")
    @ToString.Include
    @NaturalId
    private CompoundDocumentId compoundDocumentId;

    @EqualsAndHashCode.Include
    @ToString.Include
    @NaturalId
    @Column(columnDefinition = "varchar(255) default '1'")
    private String version = "1";

    @EqualsAndHashCode.Include
    @Convert(converter = CompoundDocumentIdConverter.class, attributeName = "id")
    @ToString.Include
    private CompoundDocumentId inheritFromId;

    @NotBlank(message = "Not blank")
    @EqualsAndHashCode.Exclude
    @ToString.Include
    private String title;

    @NotNull(message = "Not blank")
    @EqualsAndHashCode.Exclude
    @Convert(converter = RegelungsVorhabenIdConverter.class, attributeName = "id")
    @ToString.Include
    private RegelungsVorhabenId regelungsVorhabenId;

    @NotNull(message = "Not blank")
    @EqualsAndHashCode.Exclude
    @Enumerated(EnumType.STRING)
    private CompoundDocumentTypeVariant type;

    @NotNull(message = "Not blank")
    @EqualsAndHashCode.Exclude
    @Enumerated(EnumType.STRING)
    @ToString.Include
    @Column(columnDefinition = "varchar(255) default 'DRAFT'")
    private DocumentState state = DocumentState.DRAFT;
    
    @NotNull(message = "Not blank")
    @EqualsAndHashCode.Exclude
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "varchar(255) default 'REFERENTENENTWURF'")
    private VerfahrensType verfahrensType = VerfahrensType.REFERENTENENTWURF;

    @CreatedBy
    @EqualsAndHashCode.Exclude
    @Column(name = "created_by_id")
    private String createdBy;

    @LastModifiedBy
    @EqualsAndHashCode.Exclude
    @Column(name = "updated_by_id")
    private String updatedBy;

    @CreatedDate
    @Column(updatable = false, nullable = false)
    @EqualsAndHashCode.Exclude
    private Instant createdAt;

    @LastModifiedDate
    @Column(nullable = false)
    @EqualsAndHashCode.Exclude
    private Instant updatedAt;

    @EqualsAndHashCode.Exclude
    private UUID fixedRegelungsvorhabenReferenzId;

}
