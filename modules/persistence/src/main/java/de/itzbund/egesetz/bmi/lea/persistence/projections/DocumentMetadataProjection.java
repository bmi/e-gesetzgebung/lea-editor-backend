// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.projections;

import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;

import java.time.Instant;

public interface DocumentMetadataProjection {

	DocumentId getDocumentId();

	String getTitle();

	DocumentType getType();

	String getCreatedBy();

	String getUpdatedBy();

	Instant getCreatedAt();

	Instant getUpdatedAt();

	CompoundDocumentId getCompoundDocumentId();

}
