// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities;

import de.itzbund.egesetz.bmi.lea.domain.enums.CommentState;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.CommentIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.DocumentIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.listeners.CreatedOrUpdatedEntityListener;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;
import lombok.ToString;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * A domain entity representing a comment with some metadata.
 */
@Entity
@EntityListeners({AuditingEntityListener.class, CreatedOrUpdatedEntityListener.class})
@Table(name = "comment")
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CommentEntity extends BaseEntity {

	@NotNull
	@EqualsAndHashCode.Exclude
	@Convert(converter = DocumentIdConverter.class, attributeName = "id")
	private DocumentId documentId;

	@NotNull
	@EqualsAndHashCode.Include
	@Convert(converter = CommentIdConverter.class, attributeName = "id")
	private CommentId commentId;

	@NotBlank
	@Column(columnDefinition = "text")
	@EqualsAndHashCode.Exclude
	private String content;

	@NotNull(message = "Not blank")
	@EqualsAndHashCode.Exclude
	@Enumerated(EnumType.STRING)
	@Column(columnDefinition = "varchar(255) default 'OPEN'")
	@Builder.Default
	private CommentState state = CommentState.OPEN;

	@CreatedBy
	@EqualsAndHashCode.Exclude
	@Column(name = "created_by_id")
	private String createdBy;

	@LastModifiedBy
	@EqualsAndHashCode.Exclude
	@Column(name = "updated_by_id")
	private String updatedBy;

	@CreatedDate
	@Column(updatable = false, nullable = false)
	@EqualsAndHashCode.Exclude
	private Instant createdAt;

	@LastModifiedDate
	@Column(nullable = false)
	@EqualsAndHashCode.Exclude
	private Instant updatedAt;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(referencedColumnName = "id", foreignKey = @ForeignKey(name = "fk_comment_anchor_id"))
	@EqualsAndHashCode.Exclude
	private PositionEntity anchor;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(referencedColumnName = "id", foreignKey = @ForeignKey(name = "fk_comment_focus_id"))
	@EqualsAndHashCode.Exclude
	private PositionEntity focus;

	@Builder.Default
	private boolean deleted = false;

	@Transient
	@Singular
	@EqualsAndHashCode.Exclude
	private List<ReplyEntity> replies = new ArrayList<>();

}
