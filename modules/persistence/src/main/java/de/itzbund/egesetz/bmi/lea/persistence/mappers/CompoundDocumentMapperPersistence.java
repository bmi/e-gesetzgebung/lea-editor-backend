// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.mappers;

import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.persistence.entities.CompoundDocumentEntity;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true),
    uses = {
        BaseMapperPersistence.class
    }
)
public interface CompoundDocumentMapperPersistence {

    @Mapping(target = "verfahrensType", source = "verfahrensType")
    @Mapping(target = "fixedRegelungsvorhabenReferenzId", source = "fixedRegelungsvorhabenReferenzId")
    @Mapping(target = "createdBy", source = "createdBy", qualifiedByName = "userStringMapping")
    @Mapping(target = "updatedBy", source = "updatedBy", qualifiedByName = "userStringMapping")
    CompoundDocumentEntity merge(CompoundDocumentDomain compoundDocumentDomain,
        @MappingTarget CompoundDocumentEntity compoundDocumentEntity);

    @Mapping(target = "createdBy", source = "createdBy", qualifiedByName = "userStringMapping")
    @Mapping(target = "updatedBy", source = "updatedBy", qualifiedByName = "userStringMapping")
    CompoundDocumentEntity fromDomainItem(CompoundDocumentDomain compoundDocumentDomain);

    @Mapping(target = "createdBy", source = "createdBy", qualifiedByName = "userStringMapping")
    @Mapping(target = "updatedBy", source = "updatedBy", qualifiedByName = "userStringMapping")
    CompoundDocumentDomain toDomainItem(CompoundDocumentEntity compoundDocumentEntity);

}
