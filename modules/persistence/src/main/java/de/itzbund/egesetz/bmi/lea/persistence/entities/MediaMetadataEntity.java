// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Generated
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "media_metadata",
    indexes = {
        @Index(name = "fk_media_metadata_media_data", columnList = "media_data")
    }
)
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
public class MediaMetadataEntity {

    @Id
    @GeneratedValue(generator = "UUID", strategy = GenerationType.IDENTITY)
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "media_metadata_id", updatable = false, nullable = false)
    @EqualsAndHashCode.Include
    @Type(type = "uuid-char")
    private UUID id;

    @NotBlank
    @Column(name = "name", nullable = false)
    @EqualsAndHashCode.Exclude
    private String name;

    @CreatedDate
    @Column(name = "created_date", updatable = false, nullable = false)
    @EqualsAndHashCode.Exclude
    private Instant createdDate;

    @NotBlank
    @Column(name = "media_type", nullable = false)
    @EqualsAndHashCode.Exclude
    private String mediaType;

    @NotNull
    @EqualsAndHashCode.Exclude
    @OneToOne
    @JoinColumn(name = "media_data", referencedColumnName = "media_data_id",
        foreignKey = @ForeignKey(name = "fk_media_data"))
    private MediaDataEntity mediaData;

}
