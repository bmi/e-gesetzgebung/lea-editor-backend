// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import de.itzbund.egesetz.bmi.user.repositories.NutzerEntityRepository;
import de.itzbund.egesetz.bmi.user.repositories.NutzerIds;
import lombok.RequiredArgsConstructor;

/**
 * This was a repository once, but now it is an Adapter for using NutzerEntityRepository from RBAC
 */
@Component
@RequiredArgsConstructor
public class UserRepository {

	private final NutzerEntityRepository nutzerEntityRepository;

	public NutzerEntity findFirstByGid(UserId gid) {
		if (gid != null) {
			Optional<NutzerEntity> nutzer = nutzerEntityRepository.findByGid(gid.getId());
			return nutzer.orElse(null);
		} else {
			return null;
		}
	}

	public Map<String, NutzerEntity> findByGidIn(Collection<String> gidList) {
		Map<String, NutzerEntity> ret = new HashMap<>();
		nutzerEntityRepository.findAllByGidIn(gidList).forEach(x -> ret.put(x.getGid(), x));
		return ret;
	}

	public List<UserId> findDeletedNutzerWithoutStellvertreterOrZuordnung() {
		return nutzerEntityRepository.findDeletedNutzerWithoutStellvertreterOrZuordnung().stream()
			.map(NutzerIds::getGid)
			.map(UserId::new)
			.collect(Collectors.toList());
	}

	public void setEditorNoRef(final UserId userId) {
		nutzerEntityRepository.setEditorNoRef(userId.getId());
	}

}
