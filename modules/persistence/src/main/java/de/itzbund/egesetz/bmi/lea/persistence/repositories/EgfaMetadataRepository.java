// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import de.itzbund.egesetz.bmi.lea.domain.enums.egfa.datenuebernahme.EgfaModuleType;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.EgfaMetadataEntity;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@SuppressWarnings("unused")
public interface EgfaMetadataRepository extends BaseRepo<EgfaMetadataEntity, Long> {

    Optional<List<EgfaMetadataEntity>> findByDocumentIdAndRegelungsVorhabenId(DocumentId documentId, RegelungsVorhabenId regelungsVorhabenId);

    Optional<EgfaMetadataEntity> findTopByCompoundDocumentIdAndModuleNameOrderByCreatedAtDesc(CompoundDocumentId compoundDocumentId, EgfaModuleType moduleName);

	boolean existsByCreatedByUser(String userGid);
}
