// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentShortDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.DMBestandsrechtLinkDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtListDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.Bestandsrecht;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.BestandsrechtId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RvBestandsrechtId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.BestandsrechtPersistencePort;
import de.itzbund.egesetz.bmi.lea.persistence.entities.BestandsrechtEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DMBestandsrechtLinkEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.RvBestandsrechtEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.BestandsrechtMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.projections.BestandsrechtMetadataProjection;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.BestandsrechtRepository;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.DMBestandsrechtLinkRepository;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.RvBestandsrechtRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class BestandsrechtPersistenceAdapter implements BestandsrechtPersistencePort {

    private final BestandsrechtRepository bestandsrechtRepository;
    private final RvBestandsrechtRepository rvBestandsrechtRepository;
    private final DMBestandsrechtLinkRepository dmBestandsrechtLinkRepository;

    private final BestandsrechtMapperPersistence bestandsrechtMapper;

    @Override
    public Bestandsrecht save(Bestandsrecht bestandsrecht, RegelungsVorhabenId regelungsVorhabenId, List<ServiceState> status) {

        BestandsrechtEntity bestandsrechtEntity1 = null;
        Optional<BestandsrechtEntity> optionalBestandsrechtEntity = bestandsrechtRepository.findFirstByEli(bestandsrecht.getEli());

        if (optionalBestandsrechtEntity.isEmpty()) {
            BestandsrechtEntity bestandsrechtEntity = bestandsrechtMapper.fromDomainItem(bestandsrecht);
            bestandsrechtRepository.save(bestandsrechtEntity);

            RvBestandsrechtEntity rvBestandsrechtEntity = RvBestandsrechtEntity.builder().rvBestandsrechtId(new RvBestandsrechtId(UUID.randomUUID()))
                .bestandsrechtId(bestandsrecht.getBestandsrechtId()).regelungsVorhabenId(regelungsVorhabenId).build();
            rvBestandsrechtRepository.save(rvBestandsrechtEntity);
            status.add(ServiceState.OK);
            return null;
        }
        BestandsrechtEntity bestandsrechtEntity = optionalBestandsrechtEntity.get();
        if (bestandsrechtEntity.getContentJson() == null) {
            bestandsrecht.setBestandsrechtId(bestandsrechtEntity.getBestandsrechtId());
            bestandsrechtEntity = bestandsrechtMapper.merge(bestandsrecht, bestandsrechtEntity);
            bestandsrechtEntity1 = bestandsrechtRepository.save(bestandsrechtEntity);
        }

        //Check if Mapping exists; if not, create it
        Optional<RvBestandsrechtEntity> optionalRvBestandsrechtEntity = rvBestandsrechtRepository.findByRegelungsVorhabenIdAndBestandsrechtId(
            regelungsVorhabenId, bestandsrechtEntity.getBestandsrechtId());
        if (optionalRvBestandsrechtEntity.isEmpty()) {
            RvBestandsrechtEntity rvBestandsrechtEntity = RvBestandsrechtEntity.builder().rvBestandsrechtId(new RvBestandsrechtId(UUID.randomUUID()))
                .bestandsrechtId(bestandsrechtEntity.getBestandsrechtId()).regelungsVorhabenId(regelungsVorhabenId).build();
            rvBestandsrechtRepository.save(rvBestandsrechtEntity);
        }
        status.add(ServiceState.OK);

        return bestandsrechtMapper.toDomainItem(bestandsrechtEntity1);
    }

    @Override
    public DMBestandsrechtLinkDTO save(CompoundDocumentId compoundDocumentId, String dmTitel, BestandsrechtId bestandsrechtId, String brTitel) {

        DMBestandsrechtLinkEntity dmBestandsrechtLinkEntity = new DMBestandsrechtLinkEntity();
        dmBestandsrechtLinkEntity.setCompoundDocumentId(compoundDocumentId);
        dmBestandsrechtLinkEntity.setDokumentenMappeTitel(dmTitel);
        dmBestandsrechtLinkEntity.setBestandsrechtId(bestandsrechtId);
        dmBestandsrechtLinkEntity.setBestandsrechtTitelKurz(brTitel);

        DMBestandsrechtLinkEntity save = dmBestandsrechtLinkRepository.save(dmBestandsrechtLinkEntity);
        return bestandsrechtMapper.toDomainItem2(save);
    }

    @Override
    public Optional<Bestandsrecht> getBestandsrechtByBestandsrechtId(BestandsrechtId bestandsrechtId) {
        BestandsrechtEntity bestandsrechtEntity = bestandsrechtRepository.getBestandsrechtByBestandsrechtId(bestandsrechtId).orElse(null);
        if (bestandsrechtEntity == null) {
            return Optional.empty();
        } else {
            return Optional.of(bestandsrechtMapper.toDomainItem(bestandsrechtEntity));
        }
    }

    public List<BestandsrechtListDTO> getBestandsrechtEntitiesById(List<BestandsrechtId> bestandsrechtIds) {
        List<BestandsrechtMetadataProjection> foundBestandsrechtList = bestandsrechtRepository.findByBestandsrechtIdIn(bestandsrechtIds);
        return foundBestandsrechtList.stream()
            .map(x -> BestandsrechtListDTO.builder()
                .id(x.getBestandsrechtId().getId())
                .eli(x.getEli())
                .titelKurz(x.getTitelKurz())
                .titelLang(x.getTitelLang())
                .build())

            .sorted(Comparator.comparing(BestandsrechtListDTO::getTitelKurz))
            .collect(Collectors.toList());
    }

    public List<BestandsrechtId> getBestandsrechtAssociationsByRvId(RegelungsVorhabenId regelungsVorhabenId) {
        List<RvBestandsrechtEntity> foundAssociations = rvBestandsrechtRepository.findByRegelungsVorhabenId(regelungsVorhabenId);
        return foundAssociations.stream().map(RvBestandsrechtEntity::getBestandsrechtId).collect(Collectors.toList());
    }

    @Override
    public List<DMBestandsrechtLinkDTO> getLinkedCompoundDocuments(List<BestandsrechtId> bestandsrechtIds) {
        List<DMBestandsrechtLinkEntity> bestandsrechtLinkEntities = dmBestandsrechtLinkRepository.findByBestandsrechtIdIn(bestandsrechtIds);
        Map<BestandsrechtId, String> bestandsrechtTitles = new HashMap<>();
        Map<BestandsrechtId, List<CompoundDocumentShortDTO>> linkedCompoundDocuments = new HashMap<>();

        // Das mit link.getBestandsrechtTitelKurz() funktioniert nicht, wenn der Titel null ist -> redesign
        bestandsrechtLinkEntities.forEach(link -> {
            bestandsrechtTitles.put(link.getBestandsrechtId(), link.getBestandsrechtTitelKurz());
            linkedCompoundDocuments.computeIfAbsent(link.getBestandsrechtId(), k -> new ArrayList<>())
                .add(CompoundDocumentShortDTO.builder()
                    .compoundDocumentId(link.getCompoundDocumentId())
                    .compoundDocumentTitle(link.getDokumentenMappeTitel())
                    .build());
        });

        bestandsrechtIds.forEach(id -> {
            if (bestandsrechtTitles.get(id) == null) {
                linkedCompoundDocuments.put(id, Collections.emptyList());
            }
        });

        return linkedCompoundDocuments.keySet().stream()
            .map(k -> DMBestandsrechtLinkDTO.builder()
                .bestandsrechtId(k)
                .bestandsrechtTitel(bestandsrechtTitles.get(k))
                .verlinkteMappen(linkedCompoundDocuments.get(k))
                .build())
            .sorted(Comparator.comparing(DMBestandsrechtLinkDTO::getBestandsrechtTitel, Comparator.nullsLast(Comparator.naturalOrder())))
            .collect(Collectors.toList());

    }

    @Override
    public void deleteAssignmentForDokumentenMappeAndBestandsrecht(CompoundDocumentId verknuepfteDokumentenMappeId, BestandsrechtId bestandsrechtId) {
        Optional<DMBestandsrechtLinkEntity> dmBestandsrechtLinkEntityOptional = dmBestandsrechtLinkRepository.findByCompoundDocumentIdAndBestandsrechtId(
            verknuepfteDokumentenMappeId, bestandsrechtId);

        if (dmBestandsrechtLinkEntityOptional.isPresent()) {
            dmBestandsrechtLinkRepository.delete(dmBestandsrechtLinkEntityOptional.get());
        }
    }

}
