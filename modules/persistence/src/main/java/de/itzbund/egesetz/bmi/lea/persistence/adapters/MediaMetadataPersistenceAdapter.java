// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import de.itzbund.egesetz.bmi.lea.domain.model.MediaData;
import de.itzbund.egesetz.bmi.lea.domain.model.MediaMetadata;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.MediaMetadataPersistencePort;
import de.itzbund.egesetz.bmi.lea.persistence.entities.MediaMetadataEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.MediaMetadataMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.MediadataMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.MediaMetadataRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@AllArgsConstructor
@Service
public class MediaMetadataPersistenceAdapter implements MediaMetadataPersistencePort {

    // ----- Repositories -------
    private final MediaMetadataRepository mediaMetadataRepository;

    // ----- Mapper -------------
    private final MediaMetadataMapperPersistence mediaMetadataMapper;
    private final MediadataMapperPersistence mediadataMapper;

    @Override
    public MediaMetadata saveAndFlush(MediaMetadata mediaMetadata) {

        MediaMetadataEntity mediaMetadataEntity;
        Optional<MediaMetadataEntity> mediaMetaEntityOptional = mediaMetadataRepository.findById(mediaMetadata.getId());

        mediaMetadataEntity = mediaMetaEntityOptional.orElseGet(() -> MediaMetadataEntity.builder()
            .build());

        mediaMetadataEntity = mediaMetadataMapper.merge(mediaMetadata, mediaMetadataEntity);
        return mediaMetadataMapper.toDomainItem(mediaMetadataRepository.save(mediaMetadataEntity));

    }

    @Override
    public Optional<MediaMetadata> findById(UUID id) {
        Optional<MediaMetadataEntity> byId = mediaMetadataRepository.findById(id);

        if (byId.isEmpty()) {
            return Optional.empty();
        }

        MediaMetadataEntity mediaMetadataEntity = byId.get();
        return Optional.of(mediaMetadataMapper.toDomainItem(mediaMetadataEntity));
    }

    @Override
    public void deleteById(UUID id) {
        mediaMetadataRepository.deleteById(id);
    }

    @Override
    public boolean checkForeignKeyHasRelation(MediaData mediaData) {
        return mediaMetadataRepository.checkForeignKeyHasRelation(mediadataMapper.fromDomainItem(mediaData));
    }
}
