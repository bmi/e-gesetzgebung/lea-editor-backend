// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.ReplyId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.CommentIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.DocumentIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.ReplyIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.listeners.CreatedOrUpdatedEntityListener;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Generated
@Entity
@EntityListeners({AuditingEntityListener.class, CreatedOrUpdatedEntityListener.class})
@Table(name = "reply")
@Builder(toBuilder = true)
@EqualsAndHashCode(of = {"replyId"}, callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ReplyEntity extends BaseEntity {

    @NotNull
    @Convert(converter = ReplyIdConverter.class, attributeName = "id")
    private ReplyId replyId;

    @NotNull
    @Convert(converter = CommentIdConverter.class, attributeName = "id")
    private CommentId commentId;

    @NotNull
    @Convert(converter = DocumentIdConverter.class, attributeName = "id")
    private DocumentId documentId;

    @NotBlank
    private String parentId;

    @NotBlank
    @Column(columnDefinition = "text")
    private String content;

    @CreatedBy
    @Column(name = "created_by_id")
    private String createdBy;

    @LastModifiedBy
    @Column(name = "updated_by_id")
    private String updatedBy;

    @CreatedDate
    @Column(updatable = false, nullable = false)
    private Instant createdAt;

    @LastModifiedDate
    @Column(nullable = false)
    private Instant updatedAt;

    @Builder.Default
    private boolean deleted = false;
}
