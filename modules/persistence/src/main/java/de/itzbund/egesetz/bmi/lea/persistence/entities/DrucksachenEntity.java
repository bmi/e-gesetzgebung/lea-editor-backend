// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.persistence.configuration.CustomAuditingEntityListener;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.RegelungsVorhabenIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.listeners.CreatedOrUpdatedEntityListener;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.NaturalId;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Entity
@EntityListeners({AuditingEntityListener.class, CreatedOrUpdatedEntityListener.class, CustomAuditingEntityListener.class})
@Table(name = "drucksachen")
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DrucksachenEntity extends BaseEntity {

    @EqualsAndHashCode.Include
    @Convert(converter = RegelungsVorhabenIdConverter.class, attributeName = "id")
    @ToString.Include
    @NaturalId
    private RegelungsVorhabenId regelungsvorhabenId;

    @EqualsAndHashCode.Include
    @ToString.Include
    private String drucksachenNr;

}
