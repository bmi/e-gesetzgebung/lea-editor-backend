// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities.converter;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.EgfaDataId;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.UUID;

/**
 * A JPA converter to map {@link EgfaDataId}s to a database table column and vice versa.
 */
@Converter(autoApply = true)
@SuppressWarnings("unused")
public class EgfaDataIdConverter implements AttributeConverter<EgfaDataId, String> {

	@Override
	public String convertToDatabaseColumn(EgfaDataId egfaDataId) {
		if (egfaDataId == null) {
			return null;
		}

		return egfaDataId.getId()
			.toString();
	}

	@Override
	public EgfaDataId convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return null;
		}

		// throws IllegalArgumentException if name does not conform to a valid string representation
		return new EgfaDataId(UUID.fromString(dbData));
	}
}
