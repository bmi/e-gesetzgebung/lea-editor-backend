// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.mappers;

import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.persistence.entities.ReplyEntity;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true),
    uses = {
        BaseMapperPersistence.class
    })
public interface ReplyMapperPersistence {

    @Mapping(target = "createdBy", source = "createdBy", qualifiedByName = "userStringMapping")
    @Mapping(target = "updatedBy", source = "updatedBy", qualifiedByName = "userStringMapping")
    ReplyEntity merge(Reply reply, @MappingTarget ReplyEntity replyEntity);

    @Mapping(target = "createdBy", source = "createdBy", qualifiedByName = "userStringMapping")
    @Mapping(target = "updatedBy", source = "updatedBy", qualifiedByName = "userStringMapping")
    ReplyEntity fromDomainItem(Reply reply);

    @Mapping(target = "createdBy", source = "createdBy", qualifiedByName = "userStringMapping")
    @Mapping(target = "updatedBy", source = "updatedBy", qualifiedByName = "userStringMapping")
    Reply toDomainItem(ReplyEntity replyEntity);

}
