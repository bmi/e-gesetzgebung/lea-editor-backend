// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DrucksacheDokumentId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DrucksacheDokumentEntity;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@SuppressWarnings("unused")
public interface DrucksacheDokumentRepository extends BaseRepo<DrucksacheDokumentEntity, Long> {

	Optional<DrucksacheDokumentEntity> findByDrucksacheDokumentId(DrucksacheDokumentId drucksacheDokumentId);

	Optional<DrucksacheDokumentEntity> findByCompoundDocumentId(CompoundDocumentId compoundDocumentId);
}
