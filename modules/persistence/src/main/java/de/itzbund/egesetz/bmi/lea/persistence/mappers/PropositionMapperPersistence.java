// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.mappers;

import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.persistence.entities.PropositionEntity;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true))
public interface PropositionMapperPersistence {

    @Mapping(target = "referableId", source = "referenzId")
    PropositionEntity merge(Proposition proposition, @MappingTarget PropositionEntity propositionEntity);

    PropositionEntity fromDomainItem(Proposition proposition);

    @Mapping(target = "referenzId", source = "referableId")
    Proposition toDomainItem(PropositionEntity propositionEntity);

    List<Proposition> toDomainItem(List<PropositionEntity> propositionEntity);

}
