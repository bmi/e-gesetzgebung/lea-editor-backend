// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

/**
 * A domain entity for document templates, holding concrete but minimal content.
 */
@Generated
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "template")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString(onlyExplicitlyIncluded = true)
public class TemplateEntity extends BaseEntity {

    @Column(nullable = false)
    @EqualsAndHashCode.Include
    @ToString.Include
    private String templateName;

    @NotNull(message = "Not blank")
    @EqualsAndHashCode.Include
    @ToString.Include
    private int version;

    @Column(columnDefinition = "text")
    @NotBlank
    @EqualsAndHashCode.Exclude
    private String content;

    @CreatedDate
    @Column(updatable = false, nullable = false)
    @EqualsAndHashCode.Exclude
    private Instant createdAt;

    @LastModifiedDate
    @EqualsAndHashCode.Exclude
    @Column(nullable = false)
    private Instant updatedAt;
}
