// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import de.itzbund.egesetz.bmi.lea.domain.model.Template;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.TemplatePersistencePort;
import de.itzbund.egesetz.bmi.lea.persistence.entities.TemplateEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.TemplateMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.TemplateRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class TemplatePersistenceAdapter implements TemplatePersistencePort {

    // ----- Repositories -------
    private final TemplateRepository repository;

    // ----- Mapper -------------
    private final TemplateMapperPersistence templateMapper;

    @Override
    public List<Template> findAllByTemplateNameOrderByVersionDescUpdatedAtDesc(String templateName) {
        List<TemplateEntity> templateEntities = repository.findAllByTemplateNameOrderByVersionDescUpdatedAtDesc(
            templateName);

        return templateEntities.stream().map(templateMapper::toDomainItem).collect(Collectors.toList());
    }

}
