// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import de.itzbund.egesetz.bmi.lea.domain.model.Proposition;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.PropositionPersistencePort;
import de.itzbund.egesetz.bmi.lea.persistence.entities.PropositionEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.PropositionMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.PropositionRepository;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@AllArgsConstructor
@Service
@Log4j2
public class PropositionPersistenceAdapter implements PropositionPersistencePort {

    public static final String REFERABLE_ID_COLUMN_NAME = "referableId";
    public static final String PROPOSITION_ID = "propositionId";
    // ----- Repositories -------
    private final PropositionRepository repository;

    // ----- Mapper -------------
    private final PropositionMapperPersistence propositionMapper;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Proposition save(Proposition proposition) {
        Specification<PropositionEntity> spec = hasNullValueInColumn(REFERABLE_ID_COLUMN_NAME);
        return saveOrUpdateProposition(proposition, spec, false, false);
    }

    @Override
    public Proposition copy(Proposition proposition) {
        Specification<PropositionEntity> spec = hasNullValueInColumn(REFERABLE_ID_COLUMN_NAME);
        return saveOrUpdateProposition(proposition, spec, false, true);
    }

    @Override
    public Proposition update(Proposition proposition) {
        Specification<PropositionEntity> spec = emptySpecification();
        return saveOrUpdateProposition(proposition, spec, true, false);
    }

    private Proposition saveOrUpdateProposition(Proposition proposition, Specification<PropositionEntity> spec, boolean isStopAtNoResultsFound,
        boolean isCopy) {
        List<PropositionEntity> propositionEntityList = repository.findAll(spec.and((root, query, criteriaBuilder) ->
            criteriaBuilder.equal(root.get(PROPOSITION_ID), proposition.getPropositionId())));

        // Empty list -> etwas Leeres für den Merge
        PropositionEntity propositionEntity = PropositionEntity.builder().build();

        if (!propositionEntityList.isEmpty()) {
            if (propositionEntityList.size() != 1) {
                log.error("Es gibt zu viele Regelungsvorhaben"); // NOSONAR
            }
            propositionEntity = propositionEntityList.get(0);
        } else if (isStopAtNoResultsFound) {
            return null;
        }

        propositionEntity = propositionMapper.merge(proposition, propositionEntity);

        if (isCopy) {
            entityManager.detach(propositionEntity);
            propositionEntity.clearIdentity();
        }

        PropositionEntity saved = repository.save(propositionEntity);
        return propositionMapper.toDomainItem(saved);
    }

    private static Specification<PropositionEntity> hasNullValueInColumn(String columnName) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.isNull(root.get(columnName));
    }

    private static Specification<PropositionEntity> hasNoNullValueInColumn(String columnName) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.isNotNull(root.get(columnName));
    }

    private static Specification<PropositionEntity> emptySpecification() {
        return (root, query, criteriaBuilder) -> criteriaBuilder.conjunction();
    }

    private static Specification<PropositionEntity> hasPropositionIdAndReferableId(RegelungsVorhabenId propositionId, UUID referableID) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.and(
            criteriaBuilder.equal(root.get(PROPOSITION_ID), propositionId),
            criteriaBuilder.equal(root.get(REFERABLE_ID_COLUMN_NAME), referableID)
        );
    }

    @Override
    public Proposition findByUpdateablePropositionId(RegelungsVorhabenId regelungsVorhabenId) {

        Specification<PropositionEntity> spec = hasNullValueInColumn(REFERABLE_ID_COLUMN_NAME);
        List<PropositionEntity> propositionEntityList = repository.findAll(spec.and((root, query, criteriaBuilder) ->
            criteriaBuilder.equal(root.get(PROPOSITION_ID), regelungsVorhabenId)));

        if (!propositionEntityList.isEmpty()) {
            if (propositionEntityList.size() != 1) {
                log.error("Es gibt zu viele Regelungsvorhaben"); // NOSONAR
            }
            PropositionEntity propositionEntity = propositionEntityList.get(0);
            return propositionMapper.toDomainItem(propositionEntity);
        }

        return null;
    }

    @Override
    public List<Proposition> findByUpdateablePropositionIds(List<RegelungsVorhabenId> regelungsVorhabenIds) {

        Specification<PropositionEntity> spec = hasNullValueInColumn(REFERABLE_ID_COLUMN_NAME);
        List<PropositionEntity> propositionEntityList = repository.findAll(spec.and((root, query, criteriaBuilder) ->
            root.get(PROPOSITION_ID).in(regelungsVorhabenIds)));

        if (!propositionEntityList.isEmpty()) {
            if (propositionEntityList.isEmpty()) {
                log.debug("Es wurden für {} keine Regelungsvorhaben gefunden.", regelungsVorhabenIds);
            }
            return propositionMapper.toDomainItem(propositionEntityList);
        }

        return Collections.emptyList();
    }

    @Override
    public List<Proposition> findByNotUpdateablePropositionId(RegelungsVorhabenId regelungsVorhabenId) {

        Specification<PropositionEntity> spec = hasNoNullValueInColumn(REFERABLE_ID_COLUMN_NAME);
        List<PropositionEntity> propositionEntityList = repository.findAll(spec.and((root, query, criteriaBuilder) ->
            criteriaBuilder.equal(root.get(PROPOSITION_ID), regelungsVorhabenId)));

        return propositionMapper.toDomainItem(propositionEntityList);
    }

    @Override
    public Proposition findByNotUpdateablePropositionIdReferableId(RegelungsVorhabenId regelungsVorhabenId, UUID referableId) {

        Specification<PropositionEntity> spec = hasPropositionIdAndReferableId(regelungsVorhabenId, referableId);
        Optional<PropositionEntity> notUpdateableProposition = repository.findOne(spec);

        if (notUpdateableProposition.isEmpty()) {
            return null;
        }

        return propositionMapper.toDomainItem(notUpdateableProposition.get());
    }

    @Override
    public List<Proposition> findAllByPropositionId(RegelungsVorhabenId regelungsVorhabenId) {
        List<PropositionEntity> propositionEntityList = repository.findByPropositionId(regelungsVorhabenId);

        return propositionMapper.toDomainItem(propositionEntityList);
    }

}
