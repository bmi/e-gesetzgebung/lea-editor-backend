// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.adapters;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.ReplyId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.ReplyPersistencePort;
import de.itzbund.egesetz.bmi.lea.persistence.UserRepository;
import de.itzbund.egesetz.bmi.lea.persistence.entities.ReplyEntity;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.ReplyMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.mappers.UserMapperPersistence;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.ReplyRepository;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class ReplyPersistenceAdapter implements ReplyPersistencePort {

	// ----- Repositories -------
	private final ReplyRepository replyRepository;
	private final UserRepository userRepository;

	// ----- Mapper -------------
	private final ReplyMapperPersistence replyMapper;
	private final UserMapperPersistence userMapper;

	@Override
	public Optional<Reply> findByReplyId(ReplyId parentId) {
		return replyRepository.findByReplyId(parentId).map(this::getDocumentDomain);
	}

	@Override
	public Optional<Reply> findByReplyIdAndDeletedFalse(ReplyId replyId) {
		return replyRepository.findByReplyIdAndDeletedFalse(replyId).map(this::getDocumentDomain);
	}

	@Override
	public List<Reply> findByCommentIdAndDeleted(CommentId commentId, boolean isDeleted) {
		List<ReplyEntity> replyEntities = replyRepository.findByCommentIdAndDeleted(commentId, isDeleted);
		return persistenceToDomain(replyEntities);
	}

	private List<Reply> persistenceToDomain(List<ReplyEntity> replyEntities) {
		return replyEntities.stream()
			.map(this::getDocumentDomain)
			.collect(Collectors.toList());
	}

	private Reply getDocumentDomain(ReplyEntity de) {
		Reply reply = replyMapper.toDomainItem(de);
		reply.setCreatedBy(getDetailedUser(de.getCreatedBy()));
		reply.setUpdatedBy(getDetailedUser(de.getUpdatedBy()));
		return reply;
	}

	private User getDetailedUser(String userGid) {
		NutzerEntity creator = userRepository.findFirstByGid(new UserId(userGid));
		return userMapper.toDomainItem(creator);
	}

	@Override
	public Optional<Reply> findByParentIdAndDeletedFalse(String parentId) {
		return replyRepository.findByParentIdAndDeletedFalse(parentId).map(this::getDocumentDomain);
	}

	@Override
	public Reply save(Reply reply) {
		final Optional<ReplyEntity> replyEntityOptional = replyRepository.findByReplyId(reply.getReplyId());
		final ReplyEntity replyEntity = replyEntityOptional
			.orElseGet(() -> ReplyEntity.builder().build());
		final ReplyEntity mergedReplyEntity = replyMapper.merge(reply, replyEntity);

		final ReplyEntity savedReplyEntity = replyRepository.save(mergedReplyEntity);
		return getDocumentDomain(savedReplyEntity);
	}

	@Override
	public boolean existsReplyByReplyId(ReplyId replyId) {
		return replyRepository.existsReplyByReplyId(replyId);
	}

	@Override
	public boolean existsRepliesCreatedOrUpdatedByUser(final UserId userId) {
		return replyRepository.existsByCreatedByOrUpdatedBy(userId.getId(), userId.getId());
	}
}
