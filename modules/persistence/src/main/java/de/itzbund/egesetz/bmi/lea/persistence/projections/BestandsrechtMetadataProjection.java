// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.projections;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.BestandsrechtId;

public interface BestandsrechtMetadataProjection {

	BestandsrechtId getBestandsrechtId();

	String getEli();

	String getTitelKurz();

	String getTitelLang();

}
