// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities;

import de.itzbund.egesetz.bmi.lea.domain.model.vo.BestandsrechtId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.persistence.configuration.CustomAuditingEntityListener;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.BestandsrechtIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.CompoundDocumentIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.listeners.CreatedOrUpdatedEntityListener;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.time.Instant;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
@Entity
@Table(name = "dm_bestandsrecht")
@EntityListeners({AuditingEntityListener.class, CreatedOrUpdatedEntityListener.class, CustomAuditingEntityListener.class})
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DMBestandsrechtLinkEntity extends BaseEntity {

    @Column(name = "compound_document_id")
    @Convert(converter = CompoundDocumentIdConverter.class, attributeName = "id")
    private CompoundDocumentId compoundDocumentId;

    @Column(name = "dokumentenmappe_titel")
    private String dokumentenMappeTitel;

    @Column(name = "bestandsrecht_id")
    @Convert(converter = BestandsrechtIdConverter.class, attributeName = "id")
    private BestandsrechtId bestandsrechtId;

    @Column(name = "bestandsrecht_titel_kurz")
    private String bestandsrechtTitelKurz;

    @CreatedDate
    @Column(updatable = false, nullable = false)
    private Instant createdAt;

    @LastModifiedDate
    @Column(nullable = false)
    private Instant updatedAt;

    @CreatedBy
    @Column(name = "created_by_id")
    private String createdBy;

    @LastModifiedBy
    @Column(name = "updated_by_id")
    private String updatedBy;

}
