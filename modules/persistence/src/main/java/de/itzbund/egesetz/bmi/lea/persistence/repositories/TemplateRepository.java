// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import de.itzbund.egesetz.bmi.lea.persistence.entities.TemplateEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Template repository for saving and loading document templates.
 */
@Repository
@SuppressWarnings("unused")
public interface TemplateRepository extends BaseRepo<TemplateEntity, Long> {

    List<TemplateEntity> findAllByTemplateNameOrderByVersionDescUpdatedAtDesc(String templateName);
}
