// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import de.itzbund.egesetz.bmi.lea.domain.entities.DocumentCount;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DocumentEntity;
import de.itzbund.egesetz.bmi.lea.persistence.projections.DocumentMetadataProjection;

@Repository
@SuppressWarnings("unused")
public interface DocumentRepository extends BaseRepo<DocumentEntity, Long> {

	List<DocumentEntity> findByCreatedBy(String userid);

	List<DocumentEntity> findByCreatedByAndDocumentIdIn(String userId, List<DocumentId> ids);

	List<DocumentEntity> findByCreatedByAndCompoundDocumentId(String userId, CompoundDocumentId compoundDocumentId);

	List<DocumentEntity> findByCreatedByAndCompoundDocumentIdIn(String userId, List<CompoundDocumentId> compoundDocumentIds);

	Optional<DocumentEntity> findByDocumentId(DocumentId id);

	List<DocumentEntity> findByCompoundDocumentId(CompoundDocumentId id);

	List<DocumentMetadataProjection> findByCompoundDocumentIdIn(List<CompoundDocumentId> compoundDocumentIds);

	@Transactional
	void deleteByDocumentId(DocumentId documentId);

	int countByCompoundDocumentIdAndTypeIs(CompoundDocumentId compoundDocumentId, DocumentType type);

	// --------------------------------------------------

	@Query(nativeQuery = true)
	List<DocumentCount> getDocumentCount(String compoundDocumentId);

	boolean existsByCreatedByOrUpdatedBy(final String createdBy, final String updatedBy);
}
