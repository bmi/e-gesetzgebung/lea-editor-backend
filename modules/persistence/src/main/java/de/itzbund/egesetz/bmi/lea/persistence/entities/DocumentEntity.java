// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities;

import de.itzbund.egesetz.bmi.lea.domain.entities.DocumentCount;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.DocumentIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.listeners.CreatedOrUpdatedEntityListener;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

/**
 * A domain entity representing a document with some metadata.
 */
@NamedNativeQuery(
    name = "DocumentEntity.getDocumentCount",
    resultSetMapping = "documentCountMapping",
    query =
        "SELECT document_id AS documentId, `type`, ROW_NUMBER() OVER(PARTITION BY `type`) AS `count` "
            + "FROM document "
            + "WHERE compound_document = (?1) "
            + "ORDER BY `type` ASC, created_at ASC"
)
@SqlResultSetMapping(
    name = "documentCountMapping",
    classes = {
        @ConstructorResult(
            targetClass = DocumentCount.class,
            columns = {
                @ColumnResult(name = "documentId"),
                @ColumnResult(name = "type"),
                @ColumnResult(name = "count")
            }
        )
    }
)
@Generated
@Entity
@EntityListeners({AuditingEntityListener.class, CreatedOrUpdatedEntityListener.class})
@Table(name = "document")
@EqualsAndHashCode(callSuper = false)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DocumentEntity extends BaseEntity {

    @EqualsAndHashCode.Include
    @Convert(converter = DocumentIdConverter.class, attributeName = "id")
    @ToString.Include
    private DocumentId documentId;

    @EqualsAndHashCode.Include
    @Convert(converter = DocumentIdConverter.class, attributeName = "id")
    @ToString.Include
    private DocumentId inheritFromId;

    @NotBlank(message = "Not blank")
    @EqualsAndHashCode.Exclude
    @ToString.Include
    private String title;

    @Column(columnDefinition = "longtext")
    @EqualsAndHashCode.Exclude
    private String content;

    @NotNull(message = "Not blank")
    @EqualsAndHashCode.Exclude
    @Enumerated(EnumType.STRING)
    private DocumentType type;

    @CreatedBy
    @EqualsAndHashCode.Exclude
    @Column(name = "created_by_id")
    private String createdBy;

    @LastModifiedBy
    @EqualsAndHashCode.Exclude
    @Column(name = "updated_by_id")
    private String updatedBy;

    @CreatedDate
    @Column(updatable = false, nullable = false)
    @EqualsAndHashCode.Exclude
    private Instant createdAt;

    @LastModifiedDate
    @Column(nullable = false)
    @EqualsAndHashCode.Exclude
    private Instant updatedAt;

    @Column(name = "compound_document")
    @EqualsAndHashCode.Exclude
    private CompoundDocumentId compoundDocumentId;

}
