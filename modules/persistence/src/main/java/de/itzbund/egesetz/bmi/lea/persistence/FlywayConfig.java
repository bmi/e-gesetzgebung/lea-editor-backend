// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence;

import lombok.extern.log4j.Log4j2;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Log4j2
@Configuration
@Primary
@SuppressWarnings("unused")
public class FlywayConfig {

    /**
     * In some environments we use flyway in some not, so we have to check. And if it is enabled than it should repair failed migrations.
     */
    @Bean
    public FlywayMigrationInitializer flywayInitializer(ApplicationContext applicationContext) {
        try {
            Flyway flyway = (Flyway) applicationContext.getBean("flyway");
            flyway.repair();
            return new FlywayMigrationInitializer(flyway);
        } catch (NoSuchBeanDefinitionException e) {
            log.debug("No flyway here, but maybe intended: ", e);
        }
        return null;
    }

}
