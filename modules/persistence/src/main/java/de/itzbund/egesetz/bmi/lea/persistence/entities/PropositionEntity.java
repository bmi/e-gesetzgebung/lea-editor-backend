// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.entities;

import de.itzbund.egesetz.bmi.lea.domain.enums.FarbeType;
import de.itzbund.egesetz.bmi.lea.domain.enums.PropositionState;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentInitiant;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.RegelungsVorhabenIdConverter;
import de.itzbund.egesetz.bmi.lea.persistence.entities.converter.UuidConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

/**
 * A domain entity representing a proposition (Regelungsvorhaben).
 */
@Generated
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "proposition")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString(onlyExplicitlyIncluded = true)
public class PropositionEntity extends BaseEntity {

    @NotNull(message = "Not blank")
    @EqualsAndHashCode.Include
    @Convert(converter = RegelungsVorhabenIdConverter.class, attributeName = "id")
    @ToString.Include
    private RegelungsVorhabenId propositionId;

    @NotBlank(message = "Not blank")
    @EqualsAndHashCode.Exclude
    @ToString.Include
    private String title;

    @EqualsAndHashCode.Exclude
    @ToString.Include
    private String shortTitle;

    @EqualsAndHashCode.Exclude
    @ToString.Include
    private String abbreviation;

    @EqualsAndHashCode.Exclude
    @Enumerated(EnumType.STRING)
    private PropositionState state;

    @EqualsAndHashCode.Exclude
    private String proponentId;

    @EqualsAndHashCode.Exclude
    private String proponentTitle;

    @EqualsAndHashCode.Exclude
    private String proponentDesignationGenitive;

    @EqualsAndHashCode.Exclude
    private String proponentDesignationNominative;

    @EqualsAndHashCode.Exclude
    private boolean proponentActive;

    @EqualsAndHashCode.Exclude
    @Enumerated(EnumType.STRING)
    private RechtsetzungsdokumentInitiant initiant;

    @Column(name = "proposition_type")
    @EqualsAndHashCode.Exclude
    @Enumerated(EnumType.STRING)
    private RechtsetzungsdokumentTyp type;

    @CreatedDate
    @Column(updatable = false, nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Include
    private Instant createdAt;

    @EqualsAndHashCode.Exclude
    @Enumerated(EnumType.STRING)
    private FarbeType farbe;

    @EqualsAndHashCode.Exclude
    @Convert(converter = UuidConverter.class, attributeName = "referableId")
    private UUID referableId;
}
