// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import de.itzbund.egesetz.bmi.lea.domain.enums.egfa.datenuebernahme.EgfaModuleType;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.EgfaDataEntity;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Optional;

@Repository
@SuppressWarnings("unused")
public interface EgfaDataRepository extends BaseRepo<EgfaDataEntity, Long> {

    Optional<EgfaDataEntity> findByRegelungsVorhabenIdAndModuleNameAndCompletedAt(RegelungsVorhabenId regelungsVorhabenId, EgfaModuleType moduleName,
        Instant completedAt);

    Optional<EgfaDataEntity> findTopByRegelungsVorhabenIdAndModuleNameOrderByCompletedAtDesc(RegelungsVorhabenId regelungsVorhabenId,
        EgfaModuleType moduleName);

	boolean existsByTransmittedByUser(String userGid);
}
