// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.mappers;

import de.itzbund.egesetz.bmi.lea.domain.model.EgfaData;
import de.itzbund.egesetz.bmi.lea.persistence.entities.EgfaDataEntity;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true),
    uses = {
        BaseMapperPersistence.class
    }
)
public interface EgfaDataMapperPersistence {

    @Mapping(target = "transmittedByUser", source = "transmittedByUser", qualifiedByName = "userStringMapping")
    EgfaDataEntity merge(EgfaData egfaData, @MappingTarget EgfaDataEntity egfaDataEntity);

    @Mapping(target = "transmittedByUser", source = "transmittedByUser", qualifiedByName = "userStringMapping")
    EgfaDataEntity fromDomainItem(EgfaData egfaData);

    @Mapping(target = "transmittedByUser", source = "transmittedByUser", qualifiedByName = "userStringMapping")
    EgfaData toDomainItem(EgfaDataEntity egfaDataEntity);

}
