// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Repository;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.ReplyId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.ReplyEntity;

@Repository
@SuppressWarnings("unused")
public interface ReplyRepository extends BaseRepo<ReplyEntity, Long> {

	Optional<ReplyEntity> findByReplyId(ReplyId replyId);

	Optional<ReplyEntity> findByReplyIdAndDeletedFalse(ReplyId replyId);

	Optional<ReplyEntity> findByParentIdAndDeletedFalse(String replyId);

	List<ReplyEntity> findByCommentId(CommentId commentId);

	List<ReplyEntity> findByCommentIdAndDeleted(CommentId commentId, Boolean deleted);

	boolean existsReplyByReplyId(ReplyId replyId);

	boolean existsByCreatedByOrUpdatedBy(final String createdBy, final String updatedBy);

}
