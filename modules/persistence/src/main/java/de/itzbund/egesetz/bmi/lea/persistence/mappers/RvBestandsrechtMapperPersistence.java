// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.mappers;

import de.itzbund.egesetz.bmi.lea.domain.model.RvBestandsrecht;
import de.itzbund.egesetz.bmi.lea.persistence.entities.RvBestandsrechtEntity;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring",
	injectionStrategy = InjectionStrategy.CONSTRUCTOR,
	builder = @Builder(disableBuilder = true))
public interface RvBestandsrechtMapperPersistence {

	RvBestandsrechtEntity merge(RvBestandsrecht rvBestandsrecht, @MappingTarget RvBestandsrechtEntity rvBestandsrechtEntity);

	RvBestandsrechtEntity fromDomainItem(RvBestandsrecht rvBestandsrecht);

	RvBestandsrecht toDomainItem(RvBestandsrechtEntity rvBestandsrechtEntity);

}
