// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Repository;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.CommentEntity;

@Repository
@SuppressWarnings("unused")
public interface CommentRepository extends BaseRepo<CommentEntity, Long> {

	Optional<CommentEntity> findByCommentIdAndDocumentId(CommentId commentId, DocumentId documentId);

	Optional<CommentEntity> findByCommentId(CommentId commentId);

	List<CommentEntity> findByDocumentId(DocumentId documentId);

	List<CommentEntity> findByDocumentIdAndDeleted(DocumentId documentId, Boolean deleted);

	List<CommentEntity> findByDocumentIdAndCreatedBy(DocumentId documentId, String userGid);

	List<CommentEntity> findByDocumentIdAndCreatedByAndDeleted(DocumentId documentId, String userGid, Boolean deleted);

	boolean existsCommentByCommentId(CommentId commentId);

	boolean existsByCreatedByOrUpdatedBy(final String createdBy, final String updatedBy);
}
