// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import de.itzbund.egesetz.bmi.lea.persistence.entities.UserSettingsEntity;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@SuppressWarnings("unused")
public interface UserSettingsRepository extends BaseRepo<UserSettingsEntity, Long> {

    Optional<UserSettingsEntity> findByRelatedUser(String userGid);

	boolean existsByRelatedUser(String userGid);
}
