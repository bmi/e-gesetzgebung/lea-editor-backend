// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.mappers;

import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DocumentEntity;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true),
    uses = {
        BaseMapperPersistence.class
    }
)
public interface DocumentMapperPersistence {

    @Mapping(target = "createdBy", source = "createdBy", qualifiedByName = "userStringMapping")
    @Mapping(target = "updatedBy", source = "updatedBy", qualifiedByName = "userStringMapping")
    DocumentEntity merge(DocumentDomain documentDomain, @MappingTarget DocumentEntity documentEntity);

    @Mapping(target = "createdBy", source = "createdBy", qualifiedByName = "userStringMapping")
    @Mapping(target = "updatedBy", source = "updatedBy", qualifiedByName = "userStringMapping")
    DocumentEntity fromDomainItem(DocumentDomain documentDomain);

    @Mapping(target = "createdBy", source = "createdBy", qualifiedByName = "userStringMapping")
    @Mapping(target = "updatedBy", source = "updatedBy", qualifiedByName = "userStringMapping")
    DocumentDomain toDomainItem(DocumentEntity documentEntity);

}
