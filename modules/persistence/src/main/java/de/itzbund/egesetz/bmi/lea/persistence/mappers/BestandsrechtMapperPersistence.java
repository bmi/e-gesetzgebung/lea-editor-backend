// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.mappers;

import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.DMBestandsrechtLinkDTO;
import de.itzbund.egesetz.bmi.lea.domain.model.Bestandsrecht;
import de.itzbund.egesetz.bmi.lea.persistence.entities.BestandsrechtEntity;
import de.itzbund.egesetz.bmi.lea.persistence.entities.DMBestandsrechtLinkEntity;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    builder = @Builder(disableBuilder = true))
public interface BestandsrechtMapperPersistence {

    BestandsrechtEntity merge(Bestandsrecht bestandsrecht, @MappingTarget BestandsrechtEntity bestandsrechtEntity);

    BestandsrechtEntity fromDomainItem(Bestandsrecht bestandsrecht);

    Bestandsrecht toDomainItem(BestandsrechtEntity bestandsrechtEntity);

    @Mapping(target = "bestandsrechtTitel", source = "bestandsrechtTitelKurz")
    @Mapping(target = "verlinkteMappen", ignore = true)
    DMBestandsrechtLinkDTO toDomainItem2(DMBestandsrechtLinkEntity save);
}
