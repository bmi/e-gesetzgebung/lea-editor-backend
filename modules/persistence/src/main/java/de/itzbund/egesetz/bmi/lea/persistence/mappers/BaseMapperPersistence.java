// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.mappers;

import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface BaseMapperPersistence {

    @Named("userStringMapping")
    default User mapToUserId(String uuidAsString) {
        if (uuidAsString != null && !uuidAsString.isEmpty()) {
            return User.builder()
                .gid(new UserId(uuidAsString))
                .build();
        }
        return null;
    }

    @Named("userStringMapping")
    default String mapToUserId(User user) {
        if (user != null && user.getGid() != null) {
            return user.getGid().getId();
        }
        return null;
    }

}
