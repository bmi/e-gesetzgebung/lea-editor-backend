// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.persistence.repositories;

import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.persistence.entities.CompoundDocumentEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
@SuppressWarnings("unused")
public interface CompoundDocumentRepository extends BaseRepo<CompoundDocumentEntity, Long> {

	List<CompoundDocumentEntity> findByCompoundDocumentIdIn(List<CompoundDocumentId> compoundDocumentIds);

	List<CompoundDocumentEntity> findByCompoundDocumentIdInAndState(List<CompoundDocumentId> compoundDocumentIds, DocumentState state);

	Optional<CompoundDocumentEntity> findByCompoundDocumentId(CompoundDocumentId id);

	Optional<List<CompoundDocumentEntity>> findByInheritFromId(CompoundDocumentId id, Sort sort);

	// ------------ Regelungsvorhaben ----------------------------

	// @formatter:off
    /**
     * Wir verwenden natives SQL:
     *     - wegen der Fensterfunktion: PARTITION BY
     *     - wegen der Ordnung: FIELD (mysql spezifisch)
     *
     * @param regelungsVorhabenIds Eine Liste von Regelungsvorhaben
     * @return
     */
    // @formatter:on
	@Query(value = "SELECT * "
		+ "FROM ("
		+ "    SELECT"
		+ "        compound_document.*,"
		+ "        ROW_NUMBER() OVER (PARTITION BY regelungs_vorhaben_id ORDER BY created_at DESC) as row_num"
		+ "    FROM"
		+ "        compound_document"
		+ "   WHERE regelungs_vorhaben_id IN (:regelungsVorhabenIds)"
		+ "   AND created_by_id = :createdById "
		+ ") ranked "
		+ "WHERE row_num = 1 "
//        + "ORDER BY "
//        + "    FIELD(regelungs_vorhaben_id, :regelungsVorhabenIds)"
		, nativeQuery = true)
	List<CompoundDocumentEntity> findLatestByRegelungsVorhabenIdsAndCreatedBy(
		@Param("regelungsVorhabenIds") List<String> regelungsVorhabenIds,
		@Param("createdById") String createdById
	);

	List<CompoundDocumentEntity> findByRegelungsVorhabenId(RegelungsVorhabenId id);

	List<CompoundDocumentEntity> findByRegelungsVorhabenId(RegelungsVorhabenId id, Sort sort);

	List<CompoundDocumentEntity> findByRegelungsVorhabenIdAndState(RegelungsVorhabenId regelungsVorhabenId, DocumentState state);

	List<CompoundDocumentEntity> findByRegelungsVorhabenIdAndVersionLike(RegelungsVorhabenId id, String version, Sort sort);


	List<CompoundDocumentEntity> findByCreatedBy(String userGid);

	List<CompoundDocumentEntity> findByCreatedBy(String userGid, Sort sort);

	Optional<Page<CompoundDocumentEntity>> findByCreatedBy(String userGid, Pageable pageable);

	Optional<List<CompoundDocumentEntity>> findByCreatedByIn(Collection<String> userGids);

	Optional<CompoundDocumentEntity> findByCreatedByAndCompoundDocumentId(String userGid, CompoundDocumentId id);


	List<CompoundDocumentEntity> findByCreatedByAndRegelungsVorhabenId(String userGid, RegelungsVorhabenId id);

	List<CompoundDocumentEntity> findByCreatedByAndRegelungsVorhabenIdAndState(String userGid, RegelungsVorhabenId regelungsVorhabenId, DocumentState state);

	Page<CompoundDocumentEntity> findByCreatedByAndRegelungsVorhabenIdIn(String userGid,
		List<RegelungsVorhabenId> ids, Pageable pageable);

	Optional<Page<CompoundDocumentEntity>> findByRegelungsVorhabenIdInOrderByUpdatedAtDesc(List<RegelungsVorhabenId> ids, Pageable pageable);

	List<CompoundDocumentEntity> findByCreatedByAndRegelungsVorhabenIdNotIn(String userGid,
		List<RegelungsVorhabenId> ids, Sort sort);

	List<CompoundDocumentEntity> findByStateOrderByRegelungsVorhabenIdDesc(DocumentState state);

	boolean existsByCreatedByOrUpdatedBy(final String createdBy, final String updatedBy);
}
