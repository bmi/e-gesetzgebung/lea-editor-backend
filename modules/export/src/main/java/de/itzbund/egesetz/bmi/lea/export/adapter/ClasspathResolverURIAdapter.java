// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.export.adapter;

import de.itzbund.egesetz.bmi.lea.export.services.PdfService;
import org.apache.fop.apps.io.ResourceResolverFactory;
import org.apache.xmlgraphics.io.Resource;
import org.apache.xmlgraphics.io.ResourceResolver;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.Objects;

public class ClasspathResolverURIAdapter implements ResourceResolver {

    private static final String CLASSPATH_PREFIX = "classpath";
    private final ResourceResolver wrapped;

    public ClasspathResolverURIAdapter() {
        this.wrapped = ResourceResolverFactory.createDefaultResourceResolver();
    }

    @Override
    public Resource getResource(URI uri) throws IOException {
        if (Objects.equals(uri.getScheme(), CLASSPATH_PREFIX)) {
            String path = uri.getPath();
            InputStream is = PdfService.class.getResourceAsStream(path);
            return new Resource(is);
        }
        return wrapped.getResource(uri);
    }

    @Override
    public OutputStream getOutputStream(URI uri) throws IOException {
        if (Objects.equals(uri.getScheme(), CLASSPATH_PREFIX)) {
            throw new MethodNotAllowedException("This is a read only ResourceResolver. You can not use it's outputstream");
        }
        return wrapped.getOutputStream(uri);
    }
}
