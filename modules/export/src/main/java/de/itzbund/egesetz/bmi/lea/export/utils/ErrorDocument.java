// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.export.utils;

@SuppressWarnings("java:S1192")
public class ErrorDocument {

    private ErrorDocument() {
        throw new IllegalStateException("Utility class");
    }
    public static String loadErrorXml() {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<akn:akomaNtoso xmlns:akn=\"http://Inhaltsdaten.LegalDocML.de/1.6/\"\n"
            + "                xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n"
            + "                xsi:schemaLocation=\"http://Metadaten.LegalDocML.de/1.6/\">\n"
            + "   <akn:documentCollection name=\"fehler\">\n"
            + "   </akn:documentCollection>\n"
            + "</akn:akomaNtoso>";
    }

    public static String loadErrorXslt() {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
            + "\n"
            + "  <!-- Match the root element and create the XSL-FO structure -->\n"
            + "  <xsl:template match=\"/\">\n"
            + "    <fo:root xmlns:fo=\"http://www.w3.org/1999/XSL/Format\">\n"
            + "      <fo:layout-master-set>\n"
            + "        <fo:simple-page-master master-name=\"simpleA4\" page-width=\"8.27in\" page-height=\"11.69in\" margin=\"1in\">\n"
            + "          <fo:region-body/>\n"
            + "        </fo:simple-page-master>\n"
            + "      </fo:layout-master-set>\n"
            + "      <fo:page-sequence master-reference=\"simpleA4\">\n"
            + "        <fo:flow flow-name=\"xsl-region-body\">\n"
            + "            <fo:block font-size=\"18pt\" text-align=\"center\" space-after=\"12pt\">Es ist ein Fehler aufgetreten.</fo:block>\n"
            + "            <fo:block font-size=\"12pt\" space-after=\"6pt\">Es kann keine PDF-Datei erzeugt werden.</fo:block>\n"
            + "        </fo:flow>\n"
            + "      </fo:page-sequence>\n"
            + "    </fo:root>\n"
            + "  </xsl:template>\n"
            + "\n"
            + "</xsl:stylesheet>\n";
    }
}
