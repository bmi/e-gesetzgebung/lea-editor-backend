// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.export.services;

import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CommentRestPort;
import de.itzbund.egesetz.bmi.lea.export.comment.CommentPosition;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_COMMENTID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_COMMENTSONELEMENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_GUID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_TEXT;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDocumentObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getStringAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.isEffectivelyEmpty;

@Service
@Log4j2
public class PdfCommentService {

    @Autowired
    private CommentRestPort commentService;

    @Getter
    private String jsonContent;

    /**
     * Collects additional information about comments.
     *
     * The comments are coming from the database and the additional information is read from JSON.
     *
     * @param document the document
     * @return a list of all comments (and its replies) with complete information
     */
    public Map<String, CommentPosition> collectComments(DocumentDTO document) {
        Map<String, CommentPosition> commentPositions = new HashMap<>();
        jsonContent = document.getContent();
        JSONObject json = getDocumentObject(jsonContent);
        if (json == null) {
            log.error("no valid json JSON format");
            return commentPositions;
        }
        List<Comment> comments = prepareCommentsFromDocument(document);
        if (comments.isEmpty()) {
            return commentPositions;
        }

        Set<String> commentIds = comments.stream().map(c -> c.getCommentId().getId().toString()).collect(Collectors.toSet());
        findByCommentId(json, commentIds, commentPositions, null);
        int counter = 1;
        for (Comment comment : comments) {
            String commentId = comment.getCommentId().getId().toString();
            if (commentPositions.containsKey(commentId)) {
                CommentPosition current = commentPositions.get(commentId);
                current.setContent(comment.getContent());
                current.setCreatedBy(comment.getCreatedBy());
                current.setCreatedAt(comment.getCreatedAt());
                current.setMarker(counter + "");
                counter++;
                current.setReplies(comment.getReplies());
                current.setDeleted(comment.isDeleted());
            }
        }
        addCommentMarker(json, commentPositions);
        jsonContent = json.toJSONString();
        return commentPositions;
    }

    private static void findByCommentId(JSONObject jsonObject, Set<String> commentIds, Map<String, CommentPosition> commentPositions, String guid) {
        String currentGuid = getStringAttribute(jsonObject, ATTR_GUID);
        String parentGuid = Objects.nonNull(currentGuid) ? currentGuid : guid;
        String currentCommentId = getStringAttribute(jsonObject, ATTR_COMMENTID);
        if (commentIds.contains(currentCommentId)) {
            CommentPosition commentPosition = new CommentPosition();
            commentPosition.setCommentId(new CommentId(UUID.fromString(currentCommentId)));
            commentPosition.setParentGuid(parentGuid);
            commentPositions.put(currentCommentId, commentPosition);
        }

        JSONArray children = getChildren(jsonObject);
        if (!isEffectivelyEmpty(children)) {
            children.forEach(obj -> findByCommentId((JSONObject) obj, commentIds, commentPositions, parentGuid));
        }
    }

    private static void addCommentMarker(JSONObject jsonObject, Map<String, CommentPosition> commentPositions) {
        JSONArray ids = (JSONArray) jsonObject.get(ATTR_COMMENTSONELEMENT);
        if (ids != null && !ids.isEmpty()) {
            String currentId = ids.get(0).toString();
            if (commentPositions.containsKey(currentId)) {
                JSONArray childrenArray = (JSONArray) jsonObject.get("children");
                if (childrenArray != null && !childrenArray.isEmpty()) {
                    for (Object childObj : childrenArray) {
                        JSONObject childJson = (JSONObject) childObj;
                        if (childJson.containsKey(ATTR_TEXT)) {
                            childJson.put(ATTR_TEXT, childJson.get(ATTR_TEXT) + "[" + commentPositions.get(currentId).getMarker() + "]");
                        }
                    }
                }
            }
        }


        JSONArray children = getChildren(jsonObject);
        if (!isEffectivelyEmpty(children)) {
            children.forEach(obj -> addCommentMarker((JSONObject) obj, commentPositions));
        }
    }

    private List<Comment> prepareCommentsFromDocument(DocumentDTO document) {
        List<ServiceState> status = new ArrayList<>();
        return commentService.loadAllCommentsOfUserOfDocument(new DocumentId(document.getId()),
            status);
    }


}
