// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.export.utils;

import java.util.UUID;

public class Example {

    private Example() {
        throw new IllegalArgumentException("Utility class");
    }

    public static String loadEmptyDocument() {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><?xml-model href=\"Grammatiken/legalDocML.de.sch\""
            + " type=\"application/xml\" schematypens=\"http://purl.oclc.org/dsdl/schematron\"?>\n"
            + "<akn:akomaNtoso\n"
            + "        xsi:schemaLocation=\"http://Inhaltsdaten.LegalDocML.de/1.6/"
            + "        Grammatiken/legalDocML.de-vorblatt.xsd"
            + "        Grammatiken/legalDocML.de-begruendung.xsd"
            + "        Grammatiken/legalDocML.de-regelungstextentwurfsfassung.xsd"
            + "        Grammatiken/legalDocML.de-offenestruktur.xsd"
            + "        http://Metadaten.LegalDocML.de/1.6/ Grammatiken/legalDocML.de-metadaten.xsd\"\n"
            + "        xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:akn=\"http://Inhaltsdaten.LegalDocML.de/1.6/\">"
            + "</akn:akomaNtoso>";
    }
    public static String loadDummyDocument(UUID documentId) {
        return "<fo:root xmlns:fo=\"http://www.w3.org/1999/XSL/Format\">\n"
            + "    <fo:layout-master-set>\n"
            + "        <fo:simple-page-master master-name=\"A4\" page-width=\"21cm\" page-height=\"29.7cm\">\n"
            + "            <fo:region-body margin=\"2cm\"/>\n"
            + "        </fo:simple-page-master>\n"
            + "    </fo:layout-master-set>\n"
            + "\n"
            + "    <fo:page-sequence master-reference=\"A4\">\n"
            + "        <fo:flow flow-name=\"xsl-region-body\">\n"
            + "            <fo:block font-size=\"18pt\" text-align=\"center\" space-after=\"12pt\">" + documentId + "</fo:block>\n"
            + "            <fo:block font-size=\"12pt\" space-after=\"6pt\">Die PDF-Erstellung funktioniert.</fo:block>\n"
            + "        </fo:flow>\n"
            + "    </fo:page-sequence>\n"
            + "</fo:root>\n";
    }
}
