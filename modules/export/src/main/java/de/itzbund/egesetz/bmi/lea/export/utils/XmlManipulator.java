// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.export.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.export.comment.CommentPosition;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static de.itzbund.egesetz.bmi.lea.core.util.Utils.getDocumentBuilderFactory;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.getTransformerFactory;

@Service
@Log4j2
@SuppressWarnings("java:S1192")
public class XmlManipulator {

    private static final String TYPE_COMMENT = "comment";

    private static final String TYPE_REPLY = "reply";

    private final SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    private final List<String> allowedParentNodes = Arrays.asList("akn:article", "akn:longTitle", "akn:formula", "akn:akomaNtoso");

    /**
     * @param xml
     * @param xslt
     * @return
     */
    public String replaceAknNamespace(String xml, String xslt) {
        String xmlAknNamespace = extractAknNamespace(xml);
        String xsltAknNamespace = extractAknNamespace(xslt);
        if (xsltAknNamespace.isEmpty()) {
            return xslt.replace("xsl:stylesheet ", "xsl:stylesheet xmlns:akn=\"" + xmlAknNamespace + "\" ");
        }
        return xslt.replaceAll(xsltAknNamespace, xmlAknNamespace);
    }

    private String extractAknNamespace(String xsltContent) {
        String pattern = "xmlns:akn=\"([^\"]+)\"";
        Pattern regex = Pattern.compile(pattern);
        Matcher matcher = regex.matcher(xsltContent);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return "";
    }

    public String replaceIncludePath(String xslt, String xsltPath) {
        return xslt.replaceAll("<xsl:include href=\"include", "<xsl:include href=\"" + xsltPath + "include");
    }

    public String addComments(String xml, Map<String, CommentPosition> comments) {
        for (Map.Entry<String, CommentPosition> comment : comments.entrySet()) {
            xml = addComment(xml, comment.getValue());
        }
        return xml;
    }

    private String addComment(String xml, CommentPosition comment) {
        if (comment.isDeleted()) {
            return xml;
        }
        DocumentBuilder db;
        try {
            db = getDocumentBuilderFactory().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            Document document = db.parse(is);

            document.getDocumentElement().normalize();

            Node parentNode = searchForCommentParent(document.getDocumentElement(), comment.getParentGuid());

            Element commentNode = prepareCommentNode(document, comment);

            if (Objects.nonNull(comment.getReplies()) && !comment.getReplies().isEmpty()) {
                for (Reply reply : comment.getReplies()) {
                    Element replyNode = prepareReplyNode(document, reply);
                    commentNode.appendChild(replyNode);
                }
            }
            Node parent = searchForSuitableParent(parentNode);
            if (Objects.isNull(parent)) {
                parent = document.getDocumentElement();
            }
            parent.appendChild(commentNode);
            xml = transform(document);
        } catch (Exception e) {
            log.error("Error while adding comments to XML", e);
        }
        return xml;
    }

    private Node searchForCommentParent(Element element, String parentGuid) {
        if (element.hasAttribute("GUID") && Objects.equals(element.getAttribute("GUID"), parentGuid)) {
            return element;
        }
        NodeList nodeList = element.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Node foundNode = searchForCommentParent((Element) node, parentGuid);
                if (foundNode != null) {
                    return foundNode;
                }
            }
        }
        return null;
    }

    private Node searchForSuitableParent(Node current) {
        if (Objects.isNull(current)) {
            return null;
        }
        Node parent = current.getParentNode();
        while (!allowedParentNodes.contains(parent.getNodeName())) {
            parent = parent.getParentNode();
        }
        return parent;
    }

    private Element prepareCommentNode(Document document, CommentPosition comment)
        throws JsonProcessingException {
        Element node = document.createElement(TYPE_COMMENT);
        node.setTextContent(readContentFromCommentJson(comment.getContent()));
        node.setAttribute("parent_guid", comment.getParentGuid());
        node.setAttribute("created-by", comment.getCreatedBy().getName().trim());
        node.setAttribute("created-at", formatter.format(Date.from(comment.getCreatedAt())));
        node.setAttribute("id", comment.getCommentId().getId().toString());
        node.setAttribute("marker", comment.getMarker());
        return node;
    }

    private Element prepareReplyNode(Document document, Reply reply) throws JsonProcessingException {
        Element node = document.createElement(TYPE_REPLY);
        node.setTextContent(readContentFromCommentJson(reply.getContent()));
        node.setAttribute("created-by", reply.getCreatedBy().getName().trim());
        node.setAttribute("created-at", formatter.format(Date.from(reply.getCreatedAt())));
        node.setAttribute("comment-id", reply.getCommentId().getId().toString());
        return node;
    }

    private String transform(Document doc1) throws TransformerException {
        DOMSource domSource = new DOMSource(doc1);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        Transformer transformer = getTransformerFactory().newTransformer();
        transformer.transform(domSource, result);
        return writer.toString();
    }

    private String readContentFromCommentJson(String content) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = mapper.readTree(content);
        return rootNode.get(0).get("children").get(0).get("text").asText();
    }
}
