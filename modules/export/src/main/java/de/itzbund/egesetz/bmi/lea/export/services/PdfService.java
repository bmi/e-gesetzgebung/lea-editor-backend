// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.export.services;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.Document;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.PkpZuleitungDokumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.mappers.DocumentMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.CompoundDocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.DocumentDomain;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DrucksacheDokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CommentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CompoundDocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.DocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.DomainLoggingRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.EIdGeneratorRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.ExportRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.UserRestPort;
import de.itzbund.egesetz.bmi.lea.domain.services.logging.LogActivity;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationException;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.export.adapter.ClasspathResolverURIAdapter;
import de.itzbund.egesetz.bmi.lea.export.comment.CommentPosition;
import de.itzbund.egesetz.bmi.lea.export.document.MultipleDocumentDTO;
import de.itzbund.egesetz.bmi.lea.export.utils.DocumentComparator;
import de.itzbund.egesetz.bmi.lea.export.utils.Example;
import de.itzbund.egesetz.bmi.lea.export.utils.XmlManipulator;
import de.itzbund.egesetz.bmi.lea.export.utils.XmlMerger;
import lombok.extern.log4j.Log4j2;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.FopFactoryBuilder;
import org.apache.fop.apps.MimeConstants;
import org.apache.fop.apps.io.ResourceResolverFactory;
import org.apache.fop.configuration.Configuration;
import org.apache.fop.configuration.ConfigurationException;
import org.apache.fop.configuration.DefaultConfigurationBuilder;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import javax.sql.rowset.serial.SerialBlob;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URI;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static de.itzbund.egesetz.bmi.lea.core.util.Utils.getTransformerFactory;

@Service
@Log4j2
@SuppressWarnings({"java:S1192", "unused"})
public class PdfService {

	@Autowired
	private DocumentMapper documentMapper;

	private static final JSONParser JSON_PARSER = new JSONParser();

	private static final URI DEFAULT_BASE_URI = URI.create(".");

	private static final String FOP_CONFIG_FILE = "fop.xconf.xml";

	@Autowired
	private DocumentRestPort documentService;

	@Autowired
	private CompoundDocumentRestPort compoundDocumentService;

	@Autowired
	private ExportRestPort exportService;

	@Autowired
	private EIdGeneratorRestPort eIdGeneratorRestPort;

	@Autowired
	private XmlManipulator xmlManipulator;

	@Autowired
	private XmlMerger xmlMerger;

	@Autowired
	private UserRestPort userService;

	@Autowired
	private DomainLoggingRestPort domainLoggingService;

	@Autowired
	private DrucksacheDokumentPersistencePort drucksacheDokumentRepository;

	@Autowired
	private DokumentPersistencePort documentRepository;

	@Autowired
	private CompoundDocumentPersistencePort compoundDocumentRepository;

	@Autowired
	private LeageSession session;

	@Autowired
	private PdfCommentService pdfCommentService;

	@Autowired
	private CommentRestPort commentService;

	@Autowired
	private ApplicationContext applicationContext;


	@Value("${pdf.namespace.strict.legal-doc-version:false}")
	private boolean strictNamespace;

	public InputStream convertDocumentToInputStream(MultipleDocumentDTO multipleDocumentDTO) throws IOException, ValidationException {
		List<DocumentDTO> allDocuments = multipleDocumentDTO.getDocuments().stream().map(documentId -> documentService.getDocumentById(documentId, false))
			.sorted(new DocumentComparator()).collect(Collectors.toList());

		InputStream resultStream = multipleDocumentDTO.getMergeDocuments() ?
			createSinglePdf(allDocuments, loadXsltMainSheet(false), multipleDocumentDTO.getTitle()) :
			createZipFile(allDocuments, loadXsltMainSheet(false));
		domainLoggingService.logActivity(LogActivity.EXPORT_PDF, userService.getUser().getGid().getId());
		return resultStream;
	}

	public InputStream convertXmlToInputStream(String xml, boolean hdr, String title, String description) {
		String xslt = loadXsltMainSheet(hdr);
		Map<String, String> params = new HashMap<>();
		params.put("title", title);
		params.put("description", description);
		return convert(xml, xslt, params);
	}

	private String loadXsltMainSheet(boolean hdr) {
		String xslt = Utils.getContentOfTextResource("/stylesheets/main" + (hdr ? "_hdr" : "") + ".xslt");
		return xmlMerger.includeXslt(xslt);
	}

	public InputStream convert(String xml, String xslt, Map<String, String> params) {
		try {
			// fit akn namespace to namespace from XML
			if (!strictNamespace) {
				xslt = xmlManipulator.replaceAknNamespace(xml, xslt);
			}
			// load XSLT stylesheet
			StreamSource xsltSource = new StreamSource(new StringReader(xslt));
			Transformer transformer = getTransformerFactory().newTransformer(xsltSource);

			// set all params
			for (Map.Entry<String, String> entry : params.entrySet()) {
				transformer.setParameter(entry.getKey(), entry.getValue());
			}

			// XML source
			Source source = new StreamSource(new ByteArrayInputStream(xml.getBytes()));

			// output stream
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();

			// FOP transformation
			FopFactory fopFactory = getFopFactoryConfiguredFromClasspath();

			FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
			Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, outStream);
			Result result = new SAXResult(fop.getDefaultHandler());
			transformer.transform(source, result);
			outStream.close();
			return new ByteArrayInputStream(outStream.toByteArray());
		} catch (Exception e) {
			log.error("Error while converting XML", e);
		}
		return null;
	}

	private InputStream createSinglePdf(List<DocumentDTO> allDocuments, String xslt, String title) throws ValidationException {
		String xml = Example.loadEmptyDocument();
		ArrayList<String> documentTypes = new ArrayList<>();
		for (DocumentDTO document : allDocuments) {
			Map<String, CommentPosition> comments = pdfCommentService.collectComments(document);
			String nextXml = exportService.exportSingleDocument(pdfCommentService.getJsonContent(), new ArrayList<>());
			nextXml = xmlManipulator.replaceAknNamespace(Example.loadEmptyDocument(), nextXml);
			nextXml = xmlManipulator.addComments(nextXml, comments);
			documentTypes.add(document.getType().getArt().getLabel());
			if (Objects.nonNull(nextXml)) {
				xml = xmlMerger.merge(xml, nextXml);
			} else {
				handleInvalidXmlDocument(document);
			}
		}
		Map<String, String> params = new HashMap<>();
		params.put("title", title);
		params.put("description", String.join(", ", documentTypes));
		return convert(xml, xslt, params);
	}

	private InputStream createZipFile(List<DocumentDTO> allDocuments, String xslt) throws IOException, ValidationException {
		Map<String, InputStream> convertedDocuments = new HashMap<>();
		int counter = 1;
		for (DocumentDTO document : allDocuments) {
			Map<String, CommentPosition> comments = pdfCommentService.collectComments(document);
			String nextXml = exportService.exportSingleDocument(pdfCommentService.getJsonContent(), new ArrayList<>());
			if (Objects.isNull(nextXml)) {
				handleInvalidXmlDocument(document);
			}
			String documentTitle = document.getTitle();
			if (convertedDocuments.containsKey(documentTitle)) {
				documentTitle = documentTitle + "-" + counter;
			}
			Map<String, String> params = new HashMap<>();
			params.put("title", documentTitle);
			params.put("description", document.getType().getArt().getLabel());
			nextXml = xmlManipulator.addComments(nextXml, comments);
			convertedDocuments.put(documentTitle, convert(nextXml, xslt, params));
			counter++;
		}
		return new ByteArrayInputStream(toZip(convertedDocuments));
	}

	private void handleInvalidXmlDocument(DocumentDTO document) throws ValidationException {
		throw new ValidationException("Das XML des Dokuments " + document.getTitle() + " ist nicht valide.");
	}

	private byte[] toZip(Map<String, InputStream> documents) throws IOException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ZipOutputStream zip = new ZipOutputStream(byteArrayOutputStream);
		for (Map.Entry<String, InputStream> document : documents.entrySet()) {
			ZipEntry zipEntry = new ZipEntry(document.getKey() + ".pdf");
			zip.putNextEntry(zipEntry);
			StreamUtils.copy(Objects.requireNonNull(document.getValue()), zip);
			zip.closeEntry();
		}
		zip.finish();
		return byteArrayOutputStream.toByteArray();
	}

	private FopFactory getFopFactoryConfiguredFromClasspath() throws ConfigurationException {
		FopFactoryBuilder builder = new FopFactoryBuilder(DEFAULT_BASE_URI, new ClasspathResolverURIAdapter());
		DefaultConfigurationBuilder cfgBuilder = new DefaultConfigurationBuilder();
		Configuration cfg = cfgBuilder.build(this.getClass().getResourceAsStream("/" + FOP_CONFIG_FILE));
		builder.setConfiguration(cfg);
		FopFactory fopFactory = builder.build();
		fopFactory.getFontManager().setResourceResolver(
			ResourceResolverFactory.createInternalResourceResolver(
				DEFAULT_BASE_URI,
				new ClasspathResolverURIAdapter()
			)
		);
		return fopFactory;
	}

	/**
	 * Updates the Drucksache for the given CompoundDocument
	 *
	 * @param compoundDocumentId The Id of the Regelungsvorhaben
	 * @param status             A status field
	 */
	public void updateDrucksacheForCompoundDocumentId(UUID compoundDocumentId, List<ServiceState> status) {

		CompoundDocumentId dokumentenMappenId = new CompoundDocumentId(compoundDocumentId);
		Optional<List<DocumentDomain>> documentDomains = documentRepository.findByCompoundDocumentId(dokumentenMappenId);
		Optional<CompoundDocumentDomain> compoundDocumentDomain = compoundDocumentRepository.findByCompoundDocumentId(dokumentenMappenId);
		if (compoundDocumentDomain.isEmpty()) {
			status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
			return;
		}
		if (documentDomains.isEmpty()) {
			status.add(ServiceState.DOCUMENT_NOT_FOUND);
			return;
		}
		User user = session.getUser();

		MultipleDocumentDTO multipleDocumentDTO = new MultipleDocumentDTO();

		List<UUID> documentIdList = documentDomains.get().stream().map(x -> x.getDocumentId().getId()).collect(Collectors.toList());
		multipleDocumentDTO.setDocuments(documentIdList);
		multipleDocumentDTO.setTitle(compoundDocumentDomain.get().getTitle());
		multipleDocumentDTO.setMergeDocuments(true);

		try (InputStream inputStream = convertDocumentToInputStream(multipleDocumentDTO)) {
			SerialBlob blob = new SerialBlob(inputStream.readAllBytes());
			ServiceState returnState = drucksacheDokumentRepository.updateDokumentDrucksache(blob, dokumentenMappenId, user);
			status.add(returnState);
		} catch (SQLException | IOException e) {
			log.error("Error while converting document: ", e);
			status.add(ServiceState.UNKNOWN_ERROR);
		} catch (ValidationException e) {
			log.error("Error while validating xml document: ", e);
			status.add(ServiceState.INVALID_STATE);
		}
	}


	/**
	 * Updates the Drucksache for the given CompoundDocument
	 *
	 * @param compoundDocumentId The Id of the Regelungsvorhaben
	 * @param status             A status field
	 */
	public byte[] getDrucksacheForCompoundDocumentId(UUID compoundDocumentId, List<ServiceState> status) {

		CompoundDocumentId dokumentenMappenId = new CompoundDocumentId(compoundDocumentId);

		Optional<byte[]> returnedByteArray = drucksacheDokumentRepository.getDrucksacheByCompoundDocumentId(dokumentenMappenId, status);

		if (returnedByteArray.isEmpty()) {
			if (status.isEmpty()) {
				status.add(ServiceState.UNKNOWN_ERROR);
			}
			return new byte[0];
		}
		status.add(ServiceState.OK);
		return returnedByteArray.get();

	}


	public List<PkpZuleitungDokumentDTO> getCompoundDocumentsForPKP(String gid,
		RegelungsVorhabenId regelungsVorhabenId, List<ServiceState> status) {
		// get user object for gid
		User userEntity = userService.getUserForGid(gid);

		List<CompoundDocument> dokumentMappen = compoundDocumentService.getDokumentenmappenFuerEinRegelungsvorhabenMitStatus(
			userEntity, regelungsVorhabenId, DocumentState.BEREIT_FUER_KABINETT);

		// Nach Konvention kann es nur EINE Dokumentenmappe geben, die im Status Bereit für Kabinettverfahren ist.
		if (!dokumentMappen.isEmpty()) {
			CompoundDocument compoundDocument1 = dokumentMappen.get(0);
			List<Document> dokumente = compoundDocument1.getDokumenteDieserDokumentenmappe();

			List<PkpZuleitungDokumentDTO> pkpZuleitungDokumentDTOList = dokumente.stream()
				.map(dok -> {
					String pdfContent;
					try {
						InputStream pdfStream = createSinglePdf(List.of(documentMapper.mapToDto(dok)), loadXsltMainSheet(false),
							dok.getDocumentEntity().getTitle());
						byte[] pdfBytes = pdfStream.readAllBytes();
						pdfContent = Base64.getEncoder().encodeToString(pdfBytes);
					} catch (ValidationException | IOException e) {
						log.error(e);
						pdfContent = "";
					}
					return PkpZuleitungDokumentDTO.builder()
						.dokumentenMappeId(compoundDocument1.getCompoundDocumentId()
							.getId())
						.dokumentName(dok.getDocumentEntity()
							.getTitle() + ".pdf")
						.pkpTyp(dok.getDocumentEntity()
							.getType())
						.dokumentInhalt(pdfContent)
						.build();
				})
				.collect(Collectors.toList());

			if (pkpZuleitungDokumentDTOList.isEmpty()) {
				status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
			} else {
				status.add(ServiceState.OK);
			}

			return pkpZuleitungDokumentDTOList;
		}

		status.add(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
		return Collections.emptyList();
	}

}
