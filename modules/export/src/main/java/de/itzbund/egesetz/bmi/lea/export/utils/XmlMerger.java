// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.export.utils;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Objects;

import static de.itzbund.egesetz.bmi.lea.core.util.Utils.getDocumentBuilderFactory;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.getTransformerFactory;

@Service
@Log4j2
public class XmlMerger {

    public String merge(String xml1, String xml2) {
        DocumentBuilder db;
        try {
            db = getDocumentBuilderFactory().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml1));
            Document doc1 = db.parse(is);
            is.setCharacterStream(new StringReader(xml2));
            Document doc2 = db.parse(is);

            Element rootElement1 = doc1.getDocumentElement();
            Element rootElement2 = doc2.getDocumentElement();

            Node importedNode = doc1.importNode(rootElement2.getFirstChild(), true);
            rootElement1.appendChild(importedNode);
            return transform(doc1);
        } catch (Exception e) {
            log.error("Error while converting XML", e);
        }
        return xml1;
    }

    public String includeXslt(String xslt) {
        DocumentBuilder db;
        try {
            db = getDocumentBuilderFactory().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xslt));
            Document doc1 = db.parse(is);
            Element rootElement1 = doc1.getDocumentElement();

            for (int i = 0; i < rootElement1.getChildNodes().getLength(); i++) {
                if (Objects.nonNull(rootElement1.getChildNodes().item(i).getAttributes())
                    && Objects.nonNull(rootElement1.getChildNodes().item(i).getAttributes().getNamedItem("href"))) {
                    String includePath = "/stylesheets/" + rootElement1.getChildNodes().item(i).getAttributes().getNamedItem("href").getTextContent();
                    String includeContent = Utils.getContentOfTextResource(includePath);
                    assert includeContent != null;
                    is.setCharacterStream(new StringReader(includeContent));
                    Document doc2 = db.parse(is);
                    Element rootElement2 = doc2.getDocumentElement();
                    rootElement1 = copyNodes(doc1, rootElement1, rootElement2);
                }
            }

            // Include-Elemente in Hauptdatei löschen
            for (int i = 0; i < rootElement1.getChildNodes().getLength(); i++) {
                if (Objects.equals(rootElement1.getChildNodes().item(i).getNodeName(), "xsl:include")) {
                    rootElement1.removeChild(rootElement1.getChildNodes().item(i));
                }
            }
            return transform(doc1);
        } catch (Exception e) {
            log.error("Error while importing XSLT", e);
        }
        return xslt;
    }

    private Element copyNodes(Document doc1, Element rootElement1, Element rootElement2) {
        for (int k = 0; k < rootElement2.getChildNodes().getLength(); k++) {
            if (rootElement2.getChildNodes().item(k).hasAttributes()) {
                Node importedNode = doc1.importNode(rootElement2.getChildNodes().item(k), true);
                rootElement1.appendChild(importedNode);
            }
        }
        return rootElement1;
    }

    private String transform(Document doc1) throws TransformerException {
        DOMSource domSource = new DOMSource(doc1);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        Transformer transformer = getTransformerFactory().newTransformer();
        transformer.transform(domSource, result);
        return writer.toString();
    }
}
