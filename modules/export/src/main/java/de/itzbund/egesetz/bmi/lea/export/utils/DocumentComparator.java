// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.export.utils;

import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import org.springframework.stereotype.Service;

import java.util.Comparator;

@SuppressWarnings({"java:S109", "java:S1541"})
@Service
public class DocumentComparator implements Comparator<DocumentDTO> {

    @Override
    public int compare(DocumentDTO o1, DocumentDTO o2) {
        return Integer.compare(getIndex(o1), getIndex(o2));
    }

    private int getIndex(DocumentDTO document) {
        switch (document.getType().getArt()) {
            case ANSCHREIBEN: return 1;
            case BEGRUENDUNG: return 7;
            case OFFENE_STRUKTUR: return 5;
            case RECHTSETZUNGSDOKUMENT: return 6;
            case REGELUNGSTEXT: return 3;
            case SYNOPSE: return 8;
            default:
            case VORBLATT: return 2;
            // noch nicht verwendet: BEKANNTMACHUNGSTEXT: 3
            // noch nicht verwendet: DENKSCHRIFT: 4
            // noch nicht verwendet: VEREINBARUNG: 9
        }
    }
}
