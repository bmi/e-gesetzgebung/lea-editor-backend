// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.export.document;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@SuperBuilder
@RequiredArgsConstructor
public class MultipleDocumentDTO {

    @JsonProperty("documents")
    @Schema(name = "documents",
        example = "[\"9bff4759-5eaa-4d0b-8a67-f84a752205b8\", \"fa637407-826e-4b39-a629-26e5168b9152\", \"80f4385e-554f-42ff-a462-0c9983b9f699\"]")
    private List<UUID> documents = new ArrayList<>();

    @JsonProperty("mergeDocuments")
    @Schema(name = "mergeDocuments", example = "false")
    private Boolean mergeDocuments;

    @JsonProperty("title")
    @Schema(name = "title", example = "document")
    private String title;
}
