<?xml version="1.0" encoding="UTF-8"?>

<!--
Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit

SPDX-License-Identifier: MPL-2.0
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:math="http://www.w3.org/2005/xpath-functions/math"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:akn="http://Inhaltsdaten.LegalDocML.de/1.6/"
                exclude-result-prefixes="xsd math"
                expand-text="true"
                version="3.0">

    <xsl:template match="akn:td">
        <fo:table-cell padding="3pt" border="{$style.table.general.border}" min-height="1em">
            <xsl:attribute name="text-align"><xsl:value-of select="if (contains(@refersTo, 'akn:block')) then 'left' else 'center'" /></xsl:attribute>
            <xsl:variable name="font-weight" select="if (contains(@refersTo, 'akn:num') or contains(@refersTo, 'akn:shortTitle') or contains(@refersTo, 'akn:docTitle') or contains(@refersTo, 'akn:docStage') or contains(@refersTo, 'akn:docProponent')) then 'bold' else 'normal'"/>
            <xsl:variable name="text-decoration" select="if (@style='cp_added') then 'underline' else 'none'"/>
            <xsl:variable name="text-decoration" select="if (@style='cp_deleted') then 'line-through' else $text-decoration"/>
            <xsl:variable name="font-size" select="if (contains(@refersTo, 'akn:docStage')) then '1.75em' else '1em'"/>
            <xsl:variable name="font-size" select="if (contains(@refersTo, 'akn:docProponent')) then '1.5em' else $font-size"/>
            <xsl:variable name="font-size" select="if (contains(@refersTo, 'akn:docTitle') or contains(@refersTo, 'akn:shortTitle')) then '1.25em' else $font-size"/>
            <xsl:variable name="color" select="if (@style='cp_added') then $color.synopse.add else $color.synopse.default"/>
            <xsl:variable name="color" select="if (@style='cp_deleted') then $color.synopse.delete else $color"/>
            <fo:block text-decoration="{$text-decoration}" color="{$color}" font-weight="{$font-weight}" font-size="{$font-size}">
                <xsl:apply-templates select="akn:block/akn:span">
                    <xsl:with-param name="text-decoration" select="$text-decoration"/>
                    <xsl:with-param name="color" select="$color"/>
                </xsl:apply-templates>
            </fo:block>
        </fo:table-cell>
    </xsl:template>

    <xsl:template match="akn:block/akn:span">
        <xsl:param name="text-decoration"/>
        <xsl:param name="color"/>

        <xsl:variable name="text-decoration" select="if (@status='cp_inserted') then 'underline' else $text-decoration"/>
        <xsl:variable name="text-decoration" select="if (@status='cp_erased') then 'line-through' else $text-decoration"/>
        <xsl:variable name="color" select="if (@status='cp_inserted') then $color.synopse.add else $color"/>
        <xsl:variable name="color" select="if (@status='cp_erased') then $color.synopse.delete else $color"/>
        <fo:inline text-decoration="{$text-decoration}" color="{$color}">
            <xsl:value-of select="."/>
        </fo:inline>
    </xsl:template>

</xsl:stylesheet>
