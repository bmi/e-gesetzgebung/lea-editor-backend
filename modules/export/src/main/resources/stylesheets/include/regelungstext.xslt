<?xml version="1.0" encoding="UTF-8"?>

<!--
Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit

SPDX-License-Identifier: MPL-2.0
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:math="http://www.w3.org/2005/xpath-functions/math"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:akn="http://Inhaltsdaten.LegalDocML.de/1.6/"
                exclude-result-prefixes="xsd math"
                expand-text="true"
                version="3.0">

    <!-- ==========================================================================================
         |                                 Abschnitt innerhalb des Hauptteils eines Regelungstextes
         +   ========================================================================================== -->
    <xsl:template match="akn:bill[@name='regelungstext']/akn:preface | akn:bill[@name='regelungstext']/akn:preamble | akn:bill[@name='regelungstext']/akn:body">
        <xsl:apply-templates select="*" mode="regelungstext"/>
    </xsl:template>

    <xsl:template match="akn:bill[@name='regelungstext']/akn:preface/akn:longTitle" mode="regelungstext">
        <fo:block
                break-before="page"
                page-break-after="avoid"
                margin-top="{$style.doc.stage.margin.top}"
                margin-bottom="4 * {$style.doc.stage.margin.bottom}"
                font-family="{$style.doc.stage.font.family}"
                font-weight="{$style.doc.stage.font.weight}"
                font-size="{$style.doc.stage.font.size}"
                text-align="center">
            <!--xsl:call-template name="make-heading"/-->
            <xsl:value-of select="akn:p/akn:docStage"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="akn:p/akn:span"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="akn:p/akn:docProponent"/>
        </fo:block>

        <fo:block font-family="{$style.doc.title.font.family}"
                  font-weight="{$style.doc.title.font.weight}"
                  font-size="{$style.doc.title.font.size}"
                  text-align="center"
                  margin-top="{$style.doc.title.margin.top}"
                  margin-bottom="{$style.doc.title.margin.bottom}">
            <xsl:value-of select="akn:p/akn:docTitle"/>
        </fo:block>

        <fo:block font-size="{$style.doc.short.title.font.size}"
                  text-align="center"
                  margin-top="{$style.doc.short.title.margin.top}"
                  margin-bottom="4 * {$style.doc.short.title.margin.bottom}">
            <xsl:text>(</xsl:text><xsl:value-of select="akn:p/akn:shortTitle"/><xsl:text> - </xsl:text><xsl:value-of select="akn:p/akn:inline"/><xsl:text>)</xsl:text>
        </fo:block>
        <xsl:apply-templates select="comment">
            <xsl:sort select="@marker" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="//akn:preface/akn:block" mode="regelungstext">
        <fo:block font-family="{$style.doc.short.title.font.family}"
                  font-weight="{$style.doc.short.title.font.weight}"
                  text-align="center"
                  margin-bottom="30pt">
            <xsl:value-of select="akn:date"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="//akn:preamble/akn:formula" mode="regelungstext">
        <fo:block margin-bottom="4 * {$style.doc.short.title.margin.bottom}">
            <xsl:value-of select="akn:p"/>
        </fo:block>
        <xsl:apply-templates select="comment">
            <xsl:sort select="@marker" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="//akn:preamble/akn:recitals" mode="regelungstext">
        <fo:block margin-top="30pt"
                  text-align="center">
            <xsl:text>Inhaltsübersicht</xsl:text>
        </fo:block>
        <fo:block-container>
            <xsl:apply-templates select="akn:recital" mode="regelungstext"/>
        </fo:block-container>
    </xsl:template>

    <xsl:template match="//akn:recital" mode="regelungstext">
        <fo:block margin-top="{$style.p.general.margin.top}"
                  margin-bottom="{$style.p.general.margin.bottom}">
            <xsl:value-of select="akn:p"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="//akn:book" mode="regelungstext">
        <fo:block page-break-after="avoid"
                  margin-top="{$style.p.general.margin.top}"
                  margin-bottom="{$style.p.general.margin.bottom}"
                  font-family="{$font.default.bold}"
                  font-weight="{$style.doc.hcontainer.head.font.weight}"
                  font-size="{$heading.book.font.size}"
                  text-align="center">
            <xsl:value-of select="akn:num"/>
            <fo:block/>
            <xsl:value-of select="akn:heading"/>
        </fo:block>
        <xsl:apply-templates select="akn:part | akn:chapter | akn:subchapter | akn:section | akn:subsection | akn:title | akn:subtitle | akn:article" mode="regelungstext"/>
        <fo:block page-break-after="always"/>
    </xsl:template>

    <xsl:template match="akn:part" mode="regelungstext">
        <fo:block page-break-after="avoid"
                  margin-top="{$style.p.general.margin.top}"
                  margin-bottom="{$style.p.general.margin.bottom}"
                  font-family="{$font.default.bold}"
                  font-weight="{$style.doc.hcontainer.head.font.weight}"
                  font-size="{$heading.chapter.font.size}"
                  text-align="center">
            <xsl:value-of select="akn:num"/>
            <fo:block/>
            <xsl:value-of select="akn:heading"/>
        </fo:block>
        <xsl:apply-templates select="akn:chapter | akn:subchapter | akn:section | akn:subsection | akn:title | akn:subtitle | akn:article" mode="regelungstext"/>
    </xsl:template>

    <xsl:template match="akn:chapter" mode="regelungstext">
        <fo:block page-break-after="avoid"
                  margin-top="{$style.p.general.margin.top}"
                  margin-bottom="{$style.p.general.margin.bottom}"
                  font-family="{$font.default.bold}"
                  font-weight="{$style.doc.hcontainer.head.font.weight}"
                  font-size="{$heading.chapter.font.size}"
                  text-align="center">
            <xsl:value-of select="akn:num"/>
            <fo:block/>
            <xsl:value-of select="akn:heading"/>
        </fo:block>
        <xsl:apply-templates select="akn:subchapter | akn:section | akn:subsection | akn:title | akn:subtitle | akn:article" mode="regelungstext"/>
    </xsl:template>

    <xsl:template match="akn:subchapter" mode="regelungstext">
        <fo:block page-break-after="avoid"
                  margin-top="{$style.p.general.margin.top}"
                  margin-bottom="{$style.p.general.margin.bottom}"
                  font-family="{$font.default.bold}"
                  font-weight="{$style.doc.hcontainer.head.font.weight}"
                  font-size="{$heading.subchapter.font.size}"
                  text-align="center">
            <xsl:value-of select="akn:num"/>
            <fo:block/>
            <xsl:value-of select="akn:heading"/>
        </fo:block>
        <xsl:apply-templates select="akn:section | akn:subsection | akn:title | akn:subtitle | akn:article" mode="regelungstext"/>
    </xsl:template>

    <xsl:template match="akn:section" mode="regelungstext">
        <fo:block page-break-after="avoid"
                  margin-top="{$style.p.general.margin.top}"
                  margin-bottom="{$style.p.general.margin.bottom}"
                  font-family="{$font.default.bold}"
                  font-weight="{$style.doc.hcontainer.head.font.weight}"
                  font-size="{$heading.section.font.size}"
                  text-align="center">
            <xsl:value-of select="akn:num"/>
            <fo:block/>
            <xsl:value-of select="akn:heading"/>
        </fo:block>
        <xsl:apply-templates select="akn:subsection | akn:title | akn:subtitle | akn:article" mode="regelungstext"/>
    </xsl:template>

    <xsl:template match="akn:subsection" mode="regelungstext">
        <fo:block page-break-after="avoid"
                  margin-top="{$style.p.general.margin.top}"
                  margin-bottom="{$style.p.general.margin.bottom}"
                  font-family="{$font.default.bold}"
                  font-weight="{$style.doc.hcontainer.head.font.weight}"
                  font-size="{$heading.subsection.font.size}"
                  text-align="center">
            <xsl:value-of select="akn:num"/>
            <fo:block/>
            <xsl:value-of select="akn:heading"/>
        </fo:block>
        <xsl:apply-templates select="akn:title | akn:subtitle | akn:article" mode="regelungstext"/>
    </xsl:template>

    <xsl:template match="akn:title" mode="regelungstext">
        <fo:block page-break-after="avoid"
                  margin-top="{$style.p.general.margin.top}"
                  margin-bottom="{$style.p.general.margin.bottom}"
                  font-family="{$font.default.bold}"
                  font-weight="{$style.doc.hcontainer.head.font.weight}"
                  font-size="{$heading.title.font.size}"
                  text-align="center">
            <xsl:value-of select="akn:num"/>
            <fo:block/>
            <xsl:value-of select="akn:heading"/>
        </fo:block>
        <xsl:apply-templates select="akn:subtitle | akn:article" mode="regelungstext"/>
    </xsl:template>

    <xsl:template match="akn:subtitle" mode="regelungstext">
        <fo:block page-break-after="avoid"
                  margin-top="{$style.p.general.margin.top}"
                  margin-bottom="{$style.p.general.margin.bottom}"
                  font-family="{$font.default.bold}"
                  font-weight="{$style.doc.hcontainer.head.font.weight}"
                  font-size="{$heading.subtitle.font.size}"
                  text-align="center">
            <xsl:value-of select="akn:num"/>
            <fo:block/>
            <xsl:value-of select="akn:heading"/>
        </fo:block>
        <xsl:apply-templates select="akn:article" mode="regelungstext"/>
    </xsl:template>

    <xsl:template match="akn:article" mode="regelungstext">
        <fo:block
                page-break-after="avoid"
                margin-top="{$style.doc.hcontainer.head.margin.top}"
                margin-bottom="{$style.doc.hcontainer.head.margin.bottom}"
                font-family="{$font.default}"
                font-size="{$style.doc.hcontainer.head.font.size}"
                text-align="center">
            <xsl:value-of select="akn:num"/>
        </fo:block>
        <fo:block
                page-break-after="avoid"
                margin-bottom="{$style.doc.hcontainer.head.margin.bottom}"
                font-family="{$font.default.bold}"
                font-weight="{$style.doc.hcontainer.head.font.weight}"
                font-size="{$style.doc.hcontainer.head.font.size}"
                text-align="center">
            <xsl:value-of select="akn:heading"/>
        </fo:block>
        <fo:block margin-top="{$style.p.general.margin.top}"
                  margin-bottom="{$style.p.general.margin.bottom}">
            <xsl:apply-templates select="akn:paragraph" mode="regelungstextParagraph"/>
        </fo:block>
        <xsl:apply-templates select="comment">
            <xsl:sort select="@marker" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="akn:paragraph" mode="regelungstextParagraph">
        <fo:block margin-bottom="2 * {$style.p.general.margin.bottom}">
            <xsl:apply-templates select="akn:num" mode="regelungstextParagraph"/>
            <xsl:for-each select="1 to $default.subheading.separator.size">
                <fo:inline>&#160;</fo:inline>
            </xsl:for-each>
            <xsl:apply-templates select="akn:content" mode="regelungstextParagraph"/>
            <xsl:apply-templates select="akn:list" mode="regelungstextParagraph"/>
        </fo:block>
        <xsl:apply-templates select="comment">
            <xsl:sort select="@marker" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="akn:num" mode="regelungstextParagraph">
        <fo:inline>
            <xsl:apply-templates/>
        </fo:inline>
    </xsl:template>

    <xsl:template match="akn:content" mode="regelungstextParagraph">
        <xsl:value-of select="text()"/>
        <xsl:apply-templates select="akn:p" mode="regelungstextParagraph"/>
        <xsl:apply-templates select="akn:table" mode="default"/>
        <xsl:apply-templates select="akn:authorialNote"/>
    </xsl:template>

    <xsl:template match="akn:p" mode="regelungstextParagraph">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="akn:list" mode="regelungstextParagraph">
        <xsl:value-of select="akn:intro/akn:p"/>
        <xsl:apply-templates select="akn:point" mode="regelungstextParagraph"/>
    </xsl:template>

    <xsl:template match="akn:point" mode="regelungstextParagraph">
        <fo:block margin-left="2em">
            <fo:block text-indent="-2em"
                      margin-top=".4em">
                <xsl:value-of select="akn:num"/>
                <xsl:value-of select="akn:content/akn:p"/>
                <xsl:apply-templates select="akn:list" mode="regelungstextParagraph"/>
            </fo:block>
        </fo:block>
    </xsl:template>


</xsl:stylesheet>
