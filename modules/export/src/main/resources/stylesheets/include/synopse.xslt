<?xml version="1.0" encoding="UTF-8"?>

<!--
Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit

SPDX-License-Identifier: MPL-2.0
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:math="http://www.w3.org/2005/xpath-functions/math"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:akn="http://Inhaltsdaten.LegalDocML.de/1.6/"
                exclude-result-prefixes="xsd math"
                expand-text="true"
                version="3.0">

    <xsl:template match="akn:doc[@name='offene-struktur']/akn:mainBody/akn:tblock">
        <fo:block-container>
            <fo:block
                    page-break-after="avoid"
                    font-family="{$style.doc.short.title.font.family}"
                    font-size="{$style.doc.short.title.font.size}"
                    font-weight="{$style.doc.short.title.font.weight}"
                    margin-top="{$style.doc.short.title.margin.top}"
                    margin-bottom="{$style.p.general.margin.bottom}"
                    text-align="center">
                    <xsl:value-of select="akn:heading"/>
            </fo:block>
            <xsl:apply-templates/>
        </fo:block-container>
    </xsl:template>

    <!-- Match table element -->
    <xsl:template match="akn:table">
        <fo:table font-weight="{$style.table.general.font.weight}"
                  font-size="{$style.table.general.font.size}"
                  border="{$style.table.general.border}"
                  width="100%"
                  table-layout="fixed">
            <fo:table-column column-width="proportional-column-width(1)"/>
            <fo:table-column column-width="proportional-column-width(1)"/>
            <fo:table-body>
                <xsl:apply-templates/>
            </fo:table-body>
        </fo:table>
    </xsl:template>

    <!-- Match tr element -->
    <xsl:template match="akn:tr">
        <fo:table-row>
            <xsl:apply-templates/>
        </fo:table-row>
    </xsl:template>

    <xsl:template match="akn:th">
        <fo:table-cell padding="3pt" border="{$style.table.general.border}" background-color="#bfbfbf" text-align="center" font-weight="bold">
            <fo:block>
                <xsl:value-of select="."/>
            </fo:block>
        </fo:table-cell>
    </xsl:template>

</xsl:stylesheet>
