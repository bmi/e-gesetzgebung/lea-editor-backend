<?xml version="1.0" encoding="UTF-8"?>

<!--
Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit

SPDX-License-Identifier: MPL-2.0
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:math="http://www.w3.org/2005/xpath-functions/math"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:fox="http://xmlgraphics.apache.org/fop/extensions"
                xmlns:akn="http://Inhaltsdaten.LegalDocML.de/1.6/"
                exclude-result-prefixes="xsd math"
                expand-text="true"
                version="3.0">

    <!-- ==========================================================================================
     |                                      Metadaten
     +   ========================================================================================== -->
    <xsl:template match="akn:meta"/>

    <!-- ==========================================================================================
         |                                      Pages Breaks
         +   ========================================================================================== -->

    <xsl:template match="akn:doc[@name='begruendung'] | akn:doc[@name='vorblatt']">
        <fo:block page-break-before="always">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <!-- ==========================================================================================
     |                                      Dokumentenkopf
     +   ========================================================================================== -->
    <xsl:template match="akn:doc[@name='vorblatt' or @name='begruendung' or @name='regelungstext' or @name='offene-struktur']/akn:preface">
        <xsl:apply-templates select="akn:p"/>
        <xsl:apply-templates select="akn:longTitle"/>
    </xsl:template>

    <xsl:template match="akn:doc[@name='vorblatt' or @name='begruendung' or @name='regelungstext' or @name='offene-struktur']/akn:preface/akn:longTitle">
        <fo:block-container>
            <xsl:choose>
                <xsl:when test="akn:p/*">
                    <xsl:apply-templates select="akn:p/akn:docStage | akn:p/akn:docProponent | akn:p/akn:docTitle" mode="dok-titel"/>
                    <fo:block
                            font-family="{$style.doc.short.title.font.family}"
                            font-weight="{$style.doc.short.title.font.weight}"
                            font-size="{$style.doc.short.title.font.size}"
                            margin-top="{$style.doc.short.title.margin.top}"
                            margin-bottom="{$style.doc.short.title.margin.bottom}">
                        <xsl:apply-templates select="akn:p/akn:docTitle/following-sibling::node()"
                                             mode="dok-titel"/>
                    </fo:block>
                </xsl:when>
                <xsl:otherwise>
                    <fo:block
                            font-family="{$style.doc.short.title.font.family}"
                            font-weight="{$style.doc.short.title.font.weight}"
                            font-size="{$style.doc.short.title.font.size}"
                            margin-top="{$style.doc.short.title.margin.top}"
                            margin-bottom="{$style.doc.short.title.margin.bottom}">
                        <xsl:value-of select="akn:p"/>
                    </fo:block>
                </xsl:otherwise>
            </xsl:choose>
        </fo:block-container>
    </xsl:template>


    <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     |                      Weitere Informationen im Dokumentenkopf
     +   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <xsl:template match="akn:p">
        <xsl:apply-templates/>
    </xsl:template>


    <!-- dokumentenStatus -->
    <xsl:template match="akn:docStage" mode="dok-titel">
        <fo:block
                font-family="{$style.doc.stage.font.family}"
                font-size="{$style.doc.stage.font.size}"
                font-weight="{$style.doc.stage.font.weight}"
                margin-top="{$style.doc.stage.margin.top}"
                margin-bottom="{$style.doc.stage.margin.bottom}">
            <xsl:apply-templates mode="#current"/>
        </fo:block>
    </xsl:template>

    <!-- initiant -->
    <xsl:template match="akn:docProponent" mode="dok-titel">
        <xsl:variable name="longTitle" select="../.."/>
        <xsl:variable name="proponent" select="replace(string-join($longTitle/akn:p/akn:docStage/following-sibling::text()), '[^a-z]', '')"/>
        <xsl:if test=".">
            <fo:block
                    font-family="{$style.doc.proponent.font.family}"
                    font-weight="{$style.doc.proponent.font.weight}"
                    font-size="{$style.doc.proponent.font.size}"
                    margin-top="{$style.doc.proponent.margin.top}"
                    margin-bottom="{$style.doc.proponent.margin.bottom}">
                <xsl:value-of select="if ($proponent != '') then $proponent || ' ' else ''"/>
                <xsl:apply-templates mode="#current"/>
            </fo:block>
        </xsl:if>
    </xsl:template>

    <!-- dokumententitel -->
    <xsl:template match="akn:docTitle" mode="dok-titel">
        <fo:block
                margin-bottom="{$style.doc.title.struct.height}"></fo:block>
        <fo:block
                font-family="{$style.doc.title.font.family}"
                font-weight="{$style.doc.title.font.weight}"
                font-size="{$style.doc.title.font.size}"
                margin-top="{$style.doc.title.margin.top}"
                margin-bottom="{$style.doc.title.margin.bottom}">
            <xsl:apply-templates mode="#current"/>
        </fo:block>
    </xsl:template>

    <!-- ==========================================================================================
     |                                      Fußnote
     +   ========================================================================================== -->

    <xsl:template match="akn:authorialNote|footnote" mode="#all">
        <xsl:apply-templates select="footnote-separator"/>
        <fo:footnote
                font-weight="{$style.footnote.font.weight}"
                font-size="{$style.footnote.font.size}"
                font-family="{$style.footnote.font.family}">
            <fo:inline
                    baseline-shift="super"
                    font-size="{$style.footnote.marker.size}">{@marker}
            </fo:inline>
            <fo:footnote-body>
                <fo:list-block
                        provisional-distance-between-starts="{$style.footnote.marker.space}">
                    <fo:list-item>
                        <fo:list-item-label>
                            <fo:block>
                                <fo:inline
                                        baseline-shift="super"
                                        font-size="{$style.footnote.marker.size}">{@marker}
                                </fo:inline>
                            </fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body>
                            <fo:block
                                    margin-left="{$style.footnote.marker.space}">
                                <xsl:apply-templates mode="fussnote"/>
                            </fo:block>
                        </fo:list-item-body>
                    </fo:list-item>
                </fo:list-block>
            </fo:footnote-body>
        </fo:footnote>
        <xsl:if test="local-name(following-sibling::node()[1]) = 'authorialNote'">
            <fo:inline
                    baseline-shift="super"
                    font-weight="{$style.footnote.font.weight}"
                    font-size="{$style.footnote.marker.size}">{$style.footnote.marker.sep}
            </fo:inline>
        </xsl:if>
    </xsl:template>

    <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     |                      Fußnote - Textabsatz
     +   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <xsl:template match="akn:p" mode="fussnote">
        <fo:block>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     |                      Fußnote - Liste
     +   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <xsl:template match="akn:ol | akn:ul" mode="fussnote">
        <fo:list-block
                provisional-distance-between-starts="{$style.list.label.sep.distance}">
            <xsl:for-each select="akn:li">
                <fo:list-item>
                    <fo:list-item-label
                            text-align="end"
                            end-indent="label-end()">
                        <fo:block>
                            <fo:inline>{@value}</fo:inline>
                        </fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:apply-templates mode="#current"/>
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
        </fo:list-block>
    </xsl:template>

    <!-- ==========================================================================================
     |                                      Textinhalt
     +   ========================================================================================== -->
    <xsl:template match="text()" mode="fussnote">
        <xsl:copy/>
    </xsl:template>

    <xsl:template match="text()" mode="dok-titel">
        <xsl:copy/>
    </xsl:template>

    <!-- ******************************************************************************************
     |                                      Hilfskonstrukte
     +   ****************************************************************************************** -->

    <xsl:template name="make-heading">
        <fo:inline font-family="{$style.doc.hcontainer.head.font.family}"
                   font-size="{$style.doc.hcontainer.head.font.size}"
                   font-weight="{$style.doc.hcontainer.head.font.weight}">
            <xsl:apply-templates select="akn:num"/>
        </fo:inline>
        <xsl:for-each select="1 to $default.subheading.separator.size">
            <fo:inline>&#160;</fo:inline>
        </xsl:for-each>
        <fo:inline font-family="{$style.doc.hcontainer.head.font.family}"
                   font-size="{$style.doc.hcontainer.head.font.size}"
                   font-weight="{$style.doc.hcontainer.head.font.weight}">
            <xsl:apply-templates select="akn:heading"/>
        </fo:inline>
    </xsl:template>

    <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     |                      parse styles
     +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <xsl:template name="parse-style-attr">
        <xsl:param name="style.attr" select="''"/>
        <xsl:for-each select="tokenize($style.attr, ';')">
            <xsl:variable name="pair" select="tokenize(., ':')"/>
            <style-def>
                <attr.name>{normalize-space($pair[1])}</attr.name>
                <attr.val>{normalize-space($pair[2])}</attr.val>
            </style-def>
        </xsl:for-each>
    </xsl:template>

    <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     |                      nicht-unterstütze Elemente
     +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <xsl:template name="unsupported">
        <fo:block
                color="#ff0000"
                border="1pt solid #ff0000">{'Element ' || name() || ' wird derzeit nicht unterstützt.'}
        </fo:block>
    </xsl:template>

</xsl:stylesheet>
