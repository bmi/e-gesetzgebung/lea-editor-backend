<?xml version="1.0" encoding="UTF-8"?>

<!--
Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit

SPDX-License-Identifier: MPL-2.0
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:math="http://www.w3.org/2005/xpath-functions/math"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:akn="http://Inhaltsdaten.LegalDocML.de/1.6/"
                exclude-result-prefixes="xsd math"
                expand-text="true"
                version="3.0">

    <!-- ==========================================================================================
     |                                      Abschnitt innerhalb des Hauptteils einer Begründung
     +   ========================================================================================== -->
    <xsl:template match="akn:doc[@name='begruendung']/akn:mainBody/akn:hcontainer[starts-with(@refersTo, 'begruendung')]">
        <fo:block-container>
            <fo:block
                    page-break-after="avoid"
                    margin-top="{$style.doc.hcontainer.head.margin.top}"
                    margin-bottom="{$style.doc.hcontainer.head.margin.bottom}">
                <xsl:call-template name="make-heading" />
            </fo:block>

            <fo:block font-family="{$style.doc.hcontainer.head.font.family}"
                      font-size="{$style.doc.hcontainer.head.font.size}"
                      font-weight="{$style.doc.hcontainer.head.font.weight}"
                      margin-top="{$style.doc.hcontainer.head.margin.top}"
                      margin-bottom="{$style.doc.hcontainer.head.margin.bottom}">
                <xsl:apply-templates select="akn:hcontainer" mode="begruendungAbschnittInhalt"/>
            </fo:block>
        </fo:block-container>
    </xsl:template>

    <xsl:template match="akn:hcontainer" mode="begruendungAbschnittInhalt">
        <fo:block
                page-break-after="avoid"
                margin-top="{$style.doc.hcontainer.head.margin.top}"
                margin-bottom="{$style.doc.hcontainer.head.margin.bottom}">
            <xsl:call-template name="make-heading" />

        </fo:block>
        <fo:block-container>
            <fo:block font-family="{$style.doc.hcontainer.head.font.family}"
                      font-size="{$style.doc.hcontainer.head.font.size}"
                      font-weight="{$style.doc.hcontainer.head.font.weight}"
                      margin-top="{$style.doc.hcontainer.head.margin.top}"
                      margin-bottom="{$style.doc.hcontainer.head.margin.bottom}">
                <xsl:apply-templates select="akn:content/*" mode="begruendung"/>
            </fo:block>
        </fo:block-container>
    </xsl:template>

    <xsl:template match="akn:content/*" mode="begruendung">
        <xsl:choose>
            <xsl:when test="self::akn:table">
                <xsl:apply-templates select="." mode="default"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="." mode="begruendungAbschnittInhalt"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     |                      Inhaltsabschnitt
     +   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <xsl:template match="akn:tblock" mode="begruendungAbschnittInhalt">
        <xsl:choose>
            <xsl:when test="akn:table">
                <xsl:apply-templates select="." mode="default"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="*[local-name() != ('num', 'heading')]" mode="#current"/>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <!-- ============================================================
     |      textAbsatz
     +=============================================================== -->

    <xsl:template match="akn:p | akn:block" mode="begruendungAbschnittInhalt">
        <fo:block
                text-align="justify"
                font-family="{$style.p.general.font.family}"
                font-size="{$style.p.general.font.size}"
                font-weight="{$style.p.general.font.weight}"
                margin-top="{$style.p.general.margin.top}"
                margin-bottom="{$style.p.general.margin.bottom}">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

</xsl:stylesheet>