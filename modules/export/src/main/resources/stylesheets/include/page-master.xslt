<?xml version="1.0" encoding="UTF-8"?>

<!--
Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit

SPDX-License-Identifier: MPL-2.0
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:math="http://www.w3.org/2005/xpath-functions/math"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:akn="http://Inhaltsdaten.LegalDocML.de/1.6/"
                xmlns:meta="http://Metadaten.LegalDocML.de"
                exclude-result-prefixes="xsd math"
                expand-text="true"
                version="3.0">

    <xsl:template match="/akn:akomaNtoso">

        <fo:root font-family="{$font.default}" xml:lang="de">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="Normal"
                                       page-width="{$page.width}"
                                       page-height="{$page.height}"
                                       margin="{$page.margin}">
                    <fo:region-body region-name="xsl-region-body"/>
                    <fo:region-before region-name="header" extent="{$page.header.height}"/>
                    <fo:region-after region-name="footer" extent="{$page.footer.height}"/>
                </fo:simple-page-master>

                <fo:simple-page-master master-name="First"
                                       page-width="{$page.width}"
                                       page-height="{$page.height}"
                                       margin="{$page.margin}">
                    <fo:region-body region-name="xsl-region-body"/>
                    <fo:region-before extent="16mm"/>
                    <fo:region-after region-name="footer" extent="{$page.footer.height}"/>
                    <fo:region-start extent="0mm"/>
                    <fo:region-end extent="5mm"/>
                </fo:simple-page-master>

                <fo:page-sequence-master master-name="PageMaster">
                    <fo:repeatable-page-master-alternatives>
                        <fo:conditional-page-master-reference master-reference="First" page-position="first"/>
                        <fo:conditional-page-master-reference master-reference="Normal"/>
                    </fo:repeatable-page-master-alternatives>
                </fo:page-sequence-master>
            </fo:layout-master-set>

            <fo:declarations>
                <x:xmpmeta xmlns:x="adobe:ns:meta/">
                    <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                        <rdf:Description rdf:about="" xmlns:dc="http://purl.org/dc/elements/1.1/">
                            <!-- Dublin Core properties go here -->
                            <dc:title><xsl:value-of select="$title" /></dc:title>
                            <dc:creator><xsl:value-of select="$creator" /></dc:creator>
                            <dc:description><xsl:value-of select="$description" /></dc:description>
                        </rdf:Description>
                        <rdf:Description rdf:about="" xmlns:xmp="http://ns.adobe.com/xap/1.0/">
                            <!-- XMP properties go here -->
                            <xmp:CreatorTool>Apache(tm) FOP Version 2.9</xmp:CreatorTool>
                        </rdf:Description>
                    </rdf:RDF>
                </x:xmpmeta>

                <pdf:catalog xmlns:pdf="http://xmlgraphics.apache.org/fop/extensions/pdf">
                    <pdf:dictionary type="normal" key="ViewerPreferences">
                        <pdf:boolean key="DisplayDocTitle">true</pdf:boolean>
                    </pdf:dictionary>
                </pdf:catalog>

                <!--fo:color-profile color-profile-name="colorProfile" src="../color/sRGB_Color_Space_Profile.icm" /-->
            </fo:declarations>

            <fo:page-sequence master-reference="PageMaster" initial-page-number="1">
                <fo:static-content flow-name="header">
                    <fo:block
                            text-align="{$page.number.align}"
                            font-family="{$page.number.font.family}"
                            font-size="{$page.number.font.size}"
                            font-weight="{$page.number.font.weight}">
                        -
                        <fo:page-number/>
                        -
                    </fo:block>
                </fo:static-content>

                <fo:static-content flow-name="footer">
                    <fo:block
                            text-align="left"
                            font-family="{$page.number.font.family}"
                            font-size="{$page.number.font.size}"
                            font-weight="{$page.number.font.weight}">
                        <xsl:value-of select="//akn:identification/akn:FRBRWork/akn:FRBRuri/@value"/>
                    </fo:block>


                </fo:static-content>

                <fo:flow flow-name="xsl-region-body"
                         orphans="{$page.break.orphans}"
                         widows="{$page.break.widows}">
                    <fo:block text-align="justify">
                        <xsl:apply-templates/>
                    </fo:block>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

</xsl:stylesheet>
