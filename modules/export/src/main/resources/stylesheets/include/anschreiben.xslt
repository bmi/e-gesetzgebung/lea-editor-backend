<?xml version="1.0" encoding="UTF-8"?>

<!--
Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit

SPDX-License-Identifier: MPL-2.0
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:math="http://www.w3.org/2005/xpath-functions/math"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:akn="http://Inhaltsdaten.LegalDocML.de/1.6/"
                exclude-result-prefixes="xsd math"
                expand-text="true"
                version="3.0">

    <!-- ==========================================================================================
         |                                 Abschnitt innerhalb des Hauptteils eines Anschreibens
         +   ========================================================================================== -->

    <xsl:template match="akn:doc[@name='anschreiben']/akn:mainBody">
        <fo:block>
            <xsl:apply-templates select="akn:p" mode="anschreiben"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="akn:doc[@name='anschreiben']/akn:preface">
        <fo:block margin-bottom="10 * {$style.doc.short.title.margin.bottom}">
            <xsl:apply-templates select="akn:tblock" mode="anschreiben"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="akn:tblock[position()=1]" mode="anschreiben">
        <fo:block>
            <xsl:apply-templates select="akn:p" mode="anschreiben-from"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="akn:tblock[position()=2]" mode="anschreiben">
        <fo:block>
            <xsl:apply-templates select="akn:p" mode="anschreiben-to"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="akn:p" mode="anschreiben-from">
        <fo:block>
            <xsl:apply-templates mode="anschreiben-from"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="akn:p" mode="anschreiben-to">
        <fo:block>
            <xsl:apply-templates mode="anschreiben-to"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="akn:location | akn:role" mode="anschreiben-from">
        <fo:block font-family="{$font.default.bold}"
                  font-weight="bold">
            <xsl:value-of select="." />
        </fo:block>
    </xsl:template>

    <xsl:template match="akn:location | akn:role" mode="anschreiben-to">
        <fo:block>
            <xsl:value-of select="." />
        </fo:block>
    </xsl:template>

    <xsl:template match="akn:person" mode="anschreiben-from anschreiben-to">
        <fo:block>
            <xsl:value-of select="." />
        </fo:block>
    </xsl:template>

    <xsl:template match="akn:date" mode="anschreiben-from anschreiben-to">
        <fo:block text-align="right">
            <xsl:value-of select="." />
        </fo:block>
    </xsl:template>

    <xsl:template match="akn:p" mode="anschreiben">
        <fo:block margin-bottom="4 * {$style.doc.stage.margin.bottom}">
            <xsl:apply-templates select="node()" mode="anschreiben"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="akn:docTitle" mode="anschreiben">
        <fo:block margin-top="4 * {$style.doc.stage.margin.bottom}"
                  margin-bottom="4 * {$style.doc.stage.margin.bottom}"
                  margin-left="10mm">
            <xsl:apply-templates select="node()" mode="anschreiben"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="text()" mode="anschreiben">
        <fo:inline margin-bottom="4 * {$style.doc.stage.margin.bottom}">
            <xsl:value-of select="."/>
        </fo:inline>
    </xsl:template>

</xsl:stylesheet>
