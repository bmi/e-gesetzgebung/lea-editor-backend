<?xml version="1.0" encoding="UTF-8"?>

<!--
Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit

SPDX-License-Identifier: MPL-2.0
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:math="http://www.w3.org/2005/xpath-functions/math"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:akn="http://Inhaltsdaten.LegalDocML.de/1.6/"
                exclude-result-prefixes="xsd math"
                expand-text="true"
                version="3.0">

    <!-- params from Java code -->
    <xsl:param name="title" select="'kein Titel'" />
    <xsl:param name="creator" select="'Nutzer der E-Gesetzgebung'" />
    <xsl:param name="description" select="'keine Beschreibung'" />

    <xsl:param name="font.default" select="'plex'" />
    <xsl:param name="font.default.bold" select="'plex-bold'" />
    <xsl:param name="font.default.italic" select="'plex-italic'" />
    <!--xsl:param name="font.default" select="'bundes-sans'" />
    <xsl:param name="font.default.bold" select="'bundes-sans-bold'" /-->

    <xsl:param name="page.width" select="'210mm'"/>
    <xsl:param name="page.height" select="'297mm'"/>
    <xsl:param name="page.margin" select="'25mm'"/>
    <xsl:param name="page.margin.top" select="$page.margin"/>
    <xsl:param name="page.margin.left" select="$page.margin"/>
    <xsl:param name="page.margin.right" select="$page.margin"/>
    <xsl:param name="page.margin.bottom" select="'20mm'"/>
    <xsl:param name="page.break.orphans" select="3"/>
    <xsl:param name="page.break.widows" select="3"/>
    <xsl:param name="page.header.height" select="'-16mm'"/>
    <xsl:param name="page.footer.height" select="'-5mm'"/>
    <xsl:param name="page.number.font.family" select="$font.default"/>
    <xsl:param name="page.number.font.weight" select="'normal'"/>
    <xsl:param name="page.number.font.size" select="'9pt'"/>
    <xsl:param name="page.number.align" select="'center'"/>
    <xsl:param name="style.doc.stage.font.family" select="$font.default.bold"/>
    <xsl:param name="style.doc.stage.font.weight" select="'bold'"/>
    <xsl:param name="style.doc.stage.font.size" select="'16.6pt'"/>
    <xsl:param name="style.doc.stage.margin" select="'5pt'"/>
    <xsl:param name="style.doc.stage.margin.top" select="$style.doc.stage.margin"/>
    <xsl:param name="style.doc.stage.margin.bottom" select="$style.doc.stage.margin"/>
    <xsl:param name="style.doc.stage.margin.left" select="'0pt'"/>
    <xsl:param name="style.doc.stage.margin.right" select="'0pt'"/>
    <xsl:param name="style.doc.proponent.font.family" select="$font.default.bold"/>
    <xsl:param name="style.doc.proponent.font.weight" select="'bold'"/>
    <xsl:param name="style.doc.proponent.font.size" select="'12pt'"/>
    <xsl:param name="style.doc.proponent.margin" select="'3pt'"/>
    <xsl:param name="style.doc.proponent.margin.top" select="$style.doc.proponent.margin"/>
    <xsl:param name="style.doc.proponent.margin.bottom" select="$style.doc.proponent.margin"/>
    <xsl:param name="style.doc.proponent.margin.left" select="'0pt'"/>
    <xsl:param name="style.doc.proponent.margin.right" select="'0pt'"/>
    <xsl:param name="style.doc.title.struct.height" select="'40pt'"/>
    <xsl:param name="style.doc.title.font.family" select="$font.default.bold"/>
    <xsl:param name="style.doc.title.font.weight" select="'bold'"/>
    <xsl:param name="style.doc.title.font.size" select="'12pt'"/>
    <xsl:param name="style.doc.title.margin.top" select="'3pt'"/>
    <xsl:param name="style.doc.title.margin.bottom" select="'3pt'"/>
    <xsl:param name="style.doc.short.title.font.family" select="$font.default.bold"/>
    <xsl:param name="style.doc.short.title.font.weight" select="'bold'"/>
    <xsl:param name="style.doc.short.title.font.size" select="'12pt'"/>
    <xsl:param name="style.doc.short.title.margin.top" select="'3pt'"/>
    <xsl:param name="style.doc.short.title.margin.bottom" select="'3pt'"/>
    <xsl:param name="style.doc.hcontainer.head.font.family" select="$font.default"/>
    <xsl:param name="style.doc.hcontainer.head.font.weight" select="'bold'"/>
    <xsl:param name="style.doc.hcontainer.head.font.size" select="'10.6pt'"/>
    <xsl:param name="style.doc.hcontainer.head.margin.top" select="'20pt'"/>
    <xsl:param name="style.doc.hcontainer.head.margin.bottom" select="'5pt'"/>
    <xsl:param name="style.doc.tblock.sub.head.font.family" select="$font.default"/>
    <xsl:param name="style.doc.tblock.sub.head.font.weight" select="'normal'"/>
    <xsl:param name="style.doc.tblock.sub.head.font.size" select="'10.6pt'"/>
    <xsl:param name="style.doc.tblock.sub.head.margin.top" select="'5pt'"/>
    <xsl:param name="style.doc.tblock.sub.head.margin.bottom" select="'5pt'"/>
    <xsl:param name="style.doc.tblock.sec.head.font.family" select="$font.default"/>
    <xsl:param name="style.doc.tblock.sec.head.font.weight" select="'normal'"/>
    <xsl:param name="style.doc.tblock.sec.head.font.size" select="'10.6pt'"/>
    <xsl:param name="style.doc.tblock.sec.head.margin.top" select="'5pt'"/>
    <xsl:param name="style.doc.tblock.sec.head.margin.bottom" select="'5pt'"/>
    <xsl:param name="style.p.general.font.family" select="$font.default"/>
    <xsl:param name="style.p.general.font.size" select="'10.6pt'"/>
    <xsl:param name="style.p.general.font.weight" select="'normal'"/>
    <xsl:param name="style.p.general.margin.top" select="'10pt'"/>
    <xsl:param name="style.p.general.margin.bottom" select="'10pt'"/>
    <xsl:param name="style.table.general.font.weight" select="'normal'"/>
    <xsl:param name="style.table.general.font.size" select="'8pt'"/>
    <xsl:param name="style.table.general.border" select="'.25pt solid black'"/>
    <xsl:param name="style.footnote.marker.size" select="'70%'"/>
    <xsl:param name="style.footnote.marker.space" select="'2mm'"/>
    <xsl:param name="style.footnote.marker.sep" select="','"/>
    <xsl:param name="style.footnote.font.family" select="$font.default"/>
    <xsl:param name="style.footnote.font.size" select="'8pt'"/>
    <xsl:param name="style.footnote.font.weight" select="'normal'"/>
    <xsl:param name="style.error.color" select="'#FF0000'"/>
    <xsl:param name="style.error.font.family" select="$font.default"/>
    <xsl:param name="style.error.font.weight" select="'200'"/>
    <xsl:param name="style.error.font.size" select="'11pt'"/>
    <xsl:param name="style.error.margin.top" select="'8pt'"/>
    <xsl:param name="style.error.margin.bottom" select="'8pt'"/>
    <xsl:param name="style.error.border.width" select="'1pt'"/>
    <xsl:param name="style.error.border.style" select="'solid'"/>
    <xsl:param name="style.error.border.color" select="$style.error.color"/>
    <xsl:param name="style.error.text.color" select="$style.error.color"/>
    <xsl:param name="style.list.indent.start" select="'0mm'"/>
    <xsl:param name="style.list.indent.end" select="'0pt'"/>
    <xsl:param name="style.list.label.sep.distance" select="'6mm'"/>
    <xsl:param name="style.list.item.label.font.family" select="$font.default"/>
    <xsl:param name="style.list.item.label.font.weight" select="'normal'"/>
    <xsl:param name="style.list.item.label.font.size" select="'10.6pt'"/>
    <xsl:param name="style.list.item.label.margin.top" select="$style.p.general.margin.top"/>
    <xsl:param name="default.heading.separator.size" select="1" />
    <xsl:param name="default.subheading.separator.size" select="4" />
    <xsl:param name="heading.book.font.size" select="'26pt'" />
    <xsl:param name="heading.chapter.font.size" select="'22pt'" />
    <xsl:param name="heading.subchapter.font.size" select="'20pt'" />
    <xsl:param name="heading.section.font.size" select="'18pt'" />
    <xsl:param name="heading.subsection.font.size" select="'16pt'" />
    <xsl:param name="heading.title.font.size" select="'14pt'" />
    <xsl:param name="heading.subtitle.font.size" select="'12pt'" />

    <!--xsl:param name="color.synopse.default" select="'rgb-icc(0, 0, 0, colorProfile, 0.816, 1.000, 0.000, 0.000)'" />
    <xsl:param name="color.synopse.add" select="'rgb-icc(0, 128, 0, colorProfile, 0.816, 1.000, 0.000, 0.000)'" />
    <xsl:param name="color.synopse.delete" select="'rgb-icc(255, 0, 0, colorProfile, 0.816, 1.000, 0.000, 0.000)'" /-->

    <xsl:param name="color.synopse.default" select="'#000000'" />
    <xsl:param name="color.synopse.add" select="'#008000'" />
    <xsl:param name="color.synopse.delete" select="'#ff0000'" />

</xsl:stylesheet>
