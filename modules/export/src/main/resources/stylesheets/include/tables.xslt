<?xml version="1.0" encoding="UTF-8"?>

<!--
Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit

SPDX-License-Identifier: MPL-2.0
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:math="http://www.w3.org/2005/xpath-functions/math"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:akn="http://Inhaltsdaten.LegalDocML.de/1.6/"
                exclude-result-prefixes="xsd math"
                expand-text="true"
                version="3.0">

    <xsl:template match="akn:table" mode="default">
        <fo:block>
            <fo:table font-weight="{$style.table.general.font.weight}"
                      font-size="{$style.table.general.font.size}"
                      border="{$style.table.general.border}"
                      width="100%"
                      margin-top="10pt">
                <fo:table-header>
                    <fo:table-row>
                        <xsl:for-each select="akn:tr/akn:th">
                            <fo:table-cell padding="3pt"
                                           border="{$style.table.general.border}"
                                           font-family="{$font.default.bold}"
                                           font-weight="bold">
                                <xsl:attribute name="number-columns-spanned">
                                    <xsl:value-of select="if (@colspan) then @colspan else 1"/>
                                </xsl:attribute>
                                <xsl:attribute name="number-rows-spanned">
                                    <xsl:value-of select="if (@rowspan) then @rowspan else 1"/>
                                </xsl:attribute>
                                <fo:block font-weight="bold">
                                    <xsl:call-template name="intersperse-with-zero-spaces">
                                        <xsl:with-param name="str" select="akn:p"/>
                                    </xsl:call-template>
                                    <!--xsl:apply-templates select="akn:p"/-->
                                </fo:block>
                            </fo:table-cell>
                        </xsl:for-each>
                    </fo:table-row>
                </fo:table-header>
                <fo:table-body>
                    <xsl:for-each select="akn:tr">
                        <xsl:if test="akn:td">
                            <fo:table-row>
                                <xsl:for-each select="akn:td">
                                    <fo:table-cell padding="3pt"
                                                   border="{$style.table.general.border}">
                                        <xsl:attribute name="number-columns-spanned">
                                            <xsl:value-of select="if (@colspan) then @colspan else 1"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="number-rows-spanned">
                                            <xsl:value-of select="if (@rowspan) then @rowspan else 1"/>
                                        </xsl:attribute>
                                        <fo:block>
                                            <xsl:apply-templates select="akn:p"/>
                                        </fo:block>
                                    </fo:table-cell>
                                </xsl:for-each>
                            </fo:table-row>
                        </xsl:if>
                    </xsl:for-each>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template name="intersperse-with-zero-spaces">
        <xsl:param name="str"/>
        <xsl:variable name="spacechars">
            &#x9;&#xA;
            &#x2000;&#x2001;&#x2002;&#x2003;&#x2004;&#x2005;
            &#x2006;&#x2007;&#x2008;&#x2009;&#x200A;&#x200B;
        </xsl:variable>

        <xsl:if test="string-length($str) &gt; 0">
            <xsl:variable name="c1" select="substring($str, 1, 1)"/>
            <xsl:variable name="c2" select="substring($str, 2, 1)"/>

            <xsl:value-of select="$c1"/>
            <xsl:if test="$c2 != '' and
        not(contains($spacechars, $c1) or
        contains($spacechars, $c2))">
                <xsl:text>&#x200B;</xsl:text>
            </xsl:if>

            <xsl:call-template name="intersperse-with-zero-spaces">
                <xsl:with-param name="str" select="substring($str, 2)"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>
