<?xml version="1.0" encoding="UTF-8"?>

<!--
Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit

SPDX-License-Identifier: MPL-2.0
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:math="http://www.w3.org/2005/xpath-functions/math"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:akn="http://Inhaltsdaten.LegalDocML.de/1.6/"
                exclude-result-prefixes="xsd math"
                expand-text="true"
                version="3.0">

    <xsl:template match="comment">
        <fo:block font-size="8pt"
                  font-family="{$font.default.italic}"
                  font-style="italic"
                  text-indent="0">
            <xsl:text>[</xsl:text>
            <xsl:value-of select="@marker"/>
            <xsl:text>] (</xsl:text>
            <xsl:value-of select="@created-at"/>
            <xsl:text> - </xsl:text>
            <xsl:value-of select="@created-by"/>
            <xsl:text>) </xsl:text>
            <xsl:value-of select="text()"/>
            <xsl:apply-templates select="reply"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="reply">
        <fo:block font-size="8pt"
                  font-family="{$font.default.italic}"
                  font-style="italic"
                  text-indent="2em">
            <xsl:text>(</xsl:text>
            <xsl:value-of select="@created-at"/>
            <xsl:text> - </xsl:text>
            <xsl:value-of select="@created-by"/>
            <xsl:text>) </xsl:text>
            <xsl:value-of select="text()"/>
            <xsl:apply-templates select="reply"/>
        </fo:block>
    </xsl:template>

</xsl:stylesheet>
