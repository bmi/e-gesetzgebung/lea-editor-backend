<?xml version="1.0" encoding="UTF-8"?>

<!--
Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit

SPDX-License-Identifier: MPL-2.0
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:math="http://www.w3.org/2005/xpath-functions/math"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:akn="http://Inhaltsdaten.LegalDocML.de/1.6/"
                exclude-result-prefixes="xsd math"
                expand-text="true"
                version="3.0">

    <!-- ==========================================================================================
         |                                      Abschnitt innerhalb des Hauptteils eines Vorblatts
         +   ========================================================================================== -->
    <xsl:template match="akn:doc[@name='vorblatt']/akn:mainBody/akn:hcontainer[starts-with(@refersTo, 'vorblattabschnitt')]">
        <fo:block
                page-break-after="avoid"
                margin-top="{$style.doc.hcontainer.head.margin.top}"
                margin-bottom="{$style.doc.hcontainer.head.margin.bottom}">
            <xsl:call-template name="make-heading"/>
        </fo:block>
        <fo:block-container>
            <xsl:apply-templates select="akn:content/*" mode="vorblatt"/>
        </fo:block-container>
    </xsl:template>

    <xsl:template match="akn:content/*" mode="vorblatt">
        <xsl:choose>
            <xsl:when test="self::akn:table">
                <xsl:apply-templates select="." mode="default"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="." mode="vorblattAbschnittInhalt"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     |                      Vorblattabschnitt - Inhaltsabschnitt
     +   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <xsl:template match="akn:tblock" mode="vorblattAbschnittInhalt">
        <xsl:choose>
            <xsl:when test="@refersTo">
                <xsl:variable name="heading" select="if (akn:num) then akn:num or ' ' or akn:heading else akn:heading"/>
                <fo:block
                        page-break-after="avoid"
                        margin-top="{$style.doc.tblock.sub.head.margin.top}"
                        margin-bottom="{$style.doc.tblock.sub.head.margin.bottom}">
                    <xsl:call-template name="make-heading"/>
                </fo:block>
                <xsl:apply-templates select="*[local-name() != ('num', 'heading')]" mode="#current"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="styles">
                    <xsl:call-template name="parse-style-attr">
                        <xsl:with-param name="style.attr" select="akn:heading/@style"/>
                    </xsl:call-template>
                </xsl:variable>
                <fo:block
                        page-break-after="avoid"
                        font-family="{$style.doc.tblock.sec.head.font.family}"
                        font-size="{$style.doc.tblock.sec.head.font.size}"
                        font-weight="{$style.doc.tblock.sec.head.font.weight}"
                        margin-top="{$style.doc.tblock.sec.head.margin.top}"
                        margin-bottom="{$style.doc.tblock.sec.head.margin.bottom}">
                    <!--xsl:for-each select="$styles">
                        <xsl:attribute name="{attr.name}"><xsl:value-of select="attr.val"/></xsl:attribute>
                    </xsl:for-each-->
                    <xsl:value-of select="akn:heading"/>
                </fo:block>
                <xsl:apply-templates select="*[local-name() != ('num', 'heading')]" mode="#current"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- ============================================================
     |      vorblattAbschnitt - textAbsatz
     +=============================================================== -->

    <xsl:template match="akn:p | akn:block" mode="vorblattAbschnittInhalt">
        <fo:block
                text-align="justify"
                font-family="{$style.p.general.font.family}"
                font-size="{$style.p.general.font.size}"
                font-weight="{$style.p.general.font.weight}"
                margin-top="{$style.p.general.margin.top}"
                margin-bottom="{$style.p.general.margin.bottom}">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <!-- ============================================================
     |      vorblattAbschnitt - HTML Liste
     +=============================================================== -->

    <xsl:template match="akn:ol | akn:ul" mode="vorblattAbschnittInhalt">
        <fo:list-block
                end-indent="{$style.list.indent.end}"
                start-indent="{$style.list.indent.start}"
                provisional-distance-between-starts="{$style.list.label.sep.distance}">
            <xsl:for-each select="akn:li">
                <fo:list-item>
                    <fo:list-item-label
                            text-align="end"
                            end-indent="label-end()"
                            font-family="{$style.list.item.label.font.family}"
                            font-size="{$style.list.item.label.font.size}"
                            font-weight="{$style.list.item.label.font.weight}">
                        <fo:block
                                margin-top="{$style.list.item.label.margin.top}">
                            <fo:inline>{@value}</fo:inline>
                        </fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:apply-templates mode="#current"/>
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
        </fo:list-block>
    </xsl:template>

    <!-- ============================================================
     |      vorblattAbschnitt - ???
     +=============================================================== -->

    <xsl:template match="akn:blockList | akn:foreign" mode="vorblattAbschnittInhalt">
        <fo:block
                color="{$style.error.text.color}"
                border-width="{$style.error.border.width}"
                border-style="{$style.error.border.style}"
                border-color="{$style.error.border.color}"
                font-family="{$style.error.font.family}"
                font-size="{$style.error.font.size}"
                font-weight="{$style.error.font.weight}"
                margin-bottom="{$style.error.margin.bottom}"
                margin-top="{$style.error.margin.top}">
            {'XML Element ' || name() || ' wird derzeit nicht unterstützt!'}
        </fo:block>
    </xsl:template>

</xsl:stylesheet>
