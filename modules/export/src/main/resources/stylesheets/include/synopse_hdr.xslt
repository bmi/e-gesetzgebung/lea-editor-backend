<?xml version="1.0" encoding="UTF-8"?>

<!--
Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit

SPDX-License-Identifier: MPL-2.0
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:math="http://www.w3.org/2005/xpath-functions/math"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:akn="http://Inhaltsdaten.LegalDocML.de/1.6/"
                exclude-result-prefixes="xsd math"
                expand-text="true"
                version="3.0">

    <!-- Match th and td elements -->
    <xsl:template match="akn:td">
        <fo:table-cell padding="3pt" border="{$style.table.general.border}" min-height="1em" text-align="center">
            <xsl:variable name="font-family" select="if (contains(@refersTo, 'akn:num') or contains(@refersTo, 'akn:shortTitle') or contains(@refersTo, 'akn:docTitle') or contains(@refersTo, 'akn:docStage') or contains(@refersTo, 'akn:docProponent')) then $font.default.bold else $font.default"/>
            <xsl:variable name="font-weight" select="if (contains(@refersTo, 'akn:num') or contains(@refersTo, 'akn:shortTitle') or contains(@refersTo, 'akn:docTitle') or contains(@refersTo, 'akn:docStage') or contains(@refersTo, 'akn:docProponent')) then 'bold' else 'normal'"/>
            <xsl:variable name="text">
                <xsl:choose>
                    <xsl:when test="preceding-sibling::akn:td[1]/@style='cp_deleted'">
                        <xsl:text>entfällt</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="akn:block/akn:span" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <xsl:variable name="font-family">
                <xsl:choose>
                    <xsl:when test="preceding-sibling::akn:td[1]/@style='cp_deleted' or @style='cp_added'">
                        <xsl:text>{$font.default.bold}</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>{$font.default}</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <xsl:variable name="font-weight">
                <xsl:choose>
                    <xsl:when test="preceding-sibling::akn:td[1]/@style='cp_deleted' or @style='cp_added'">
                        <xsl:text>bold</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>normal</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <xsl:variable name="font-size" select="if (contains(@refersTo, 'akn:docStage')) then '1.75em' else '1em'"/>
            <xsl:variable name="font-size" select="if (contains(@refersTo, 'akn:docProponent')) then '1.5em' else $font-size"/>
            <xsl:variable name="font-size" select="if (contains(@refersTo, 'akn:docTitle') or contains(@refersTo, 'akn:shortTitle')) then '1.25em' else $font-size"/>
            <fo:block font-family="{$font-family}" font-weight="{$font-weight}" font-size="{$font-size}">
                <xsl:text>{$text}</xsl:text>
            </fo:block>
        </fo:table-cell>
    </xsl:template>


</xsl:stylesheet>
