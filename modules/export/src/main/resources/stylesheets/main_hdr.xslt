<?xml version="1.0" encoding="UTF-8"?>

<!--
Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit

SPDX-License-Identifier: MPL-2.0
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:math="http://www.w3.org/2005/xpath-functions/math"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:fox="http://xmlgraphics.apache.org/fop/extensions"
                xmlns:akn="http://Inhaltsdaten.LegalDocML.de/1.6/"
                xmlns:meta="http://Metadaten.LegalDocML.de"
                exclude-result-prefixes="xsd math"
                expand-text="true"
                version="3.0">

    <xsl:include href="include/params.xslt"/>
    <xsl:include href="include/common.xslt"/>
    <xsl:include href="include/tables.xslt"/>

    <xsl:include href="include/page-master.xslt"/>

    <xsl:include href="include/vorblatt.xslt"/>
    <xsl:include href="include/begruendung.xslt"/>
    <xsl:include href="include/synopse.xslt"/>
    <xsl:include href="include/synopse_hdr.xslt"/>
    <xsl:include href="include/regelungstext.xslt"/>
    <xsl:include href="include/anschreiben.xslt"/>

</xsl:stylesheet>
