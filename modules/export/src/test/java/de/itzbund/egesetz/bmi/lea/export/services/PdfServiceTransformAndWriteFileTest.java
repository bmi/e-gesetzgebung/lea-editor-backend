// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.export.services;

import de.itzbund.egesetz.bmi.lea.export.utils.XmlManipulator;
import de.itzbund.egesetz.bmi.lea.export.utils.XmlMerger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@Disabled("Only used for generation of sample documents")
class PdfServiceTransformAndWriteFileTest {

	@Mock
	private XmlManipulator xmlManipulator;

	@InjectMocks
	private PdfService pdfService;

	private String xml;

	private String xslt;

    private List<String> sourceFiles = new ArrayList<>();

	private String targetDirectory = "C:\\develop\\bundesregierung\\testdokumente\\pdf_generated\\";

	@BeforeEach
	void setUp() throws IOException {
//        sourceFiles.add("01-02_instanz_01_anschreiben");
//        sourceFiles.add("24-07-01_09-29-07_regelungstext-1");
//        sourceFiles.add("24-07-01_09-29-07_regelungstext-1_full");
//        sourceFiles.add("24-07-01_09-29-07_regelungstext-1_par89");
//        sourceFiles.add("anschreiben_c57fa33f-1e7d-427a-821f-de8489ad0fb5");
//        sourceFiles.add("kleinbuchstaben");
//        sourceFiles.add("regelungstext_80f4385e-554f-42ff-a462-0c9983b9f699");
//        sourceFiles.add("synopse_inline");
//        sourceFiles.add("synopse_inline2");
//        sourceFiles.add("synopse_inline3");
//        sourceFiles.add("synopse_td_block");
//        sourceFiles.add("regelungstext_comments_and_replies");
//        sourceFiles.add("regelungstext_987d28dc-0c41-4e48-8118-37432890bd8d_fussnote");
//        sourceFiles.add("rt_ev_in_allen_ebenen");
//        sourceFiles.add("rt_book_part");
//        sourceFiles.add("vorblatt_table");
//        sourceFiles.add("begruendung_table");
//        sourceFiles.add("begruendung_table2");
        sourceFiles.add("rt_comment_in_table");
//        sourceFiles.add("temp");
		MockitoAnnotations.initMocks(this);
		String xsltMain = readFileSpringBootWay("/stylesheets/main.xslt");
		XmlMerger xmlMerger = new XmlMerger();
		xslt = xmlMerger.includeXslt(xsltMain);
	}

	@Disabled
	@Test
	void testWritePdfToDisk() throws IOException {
        for (String filename : sourceFiles) {
            xml = readFileSpringBootWay("/xml/" + filename + ".xml");
            when(xmlManipulator.replaceAknNamespace(any(), any())).thenReturn(xslt);

            InputStream inputStream = pdfService.convert(xml, xslt, new HashMap<>());

            OutputStream outStream = new FileOutputStream(targetDirectory + filename + ".pdf");
            byte[] buffer = new byte[8 * 1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
        }
	}

	private String readFileSpringBootWay(String filePath) throws IOException {
		Resource resource = new ClassPathResource(filePath);
		assertNotNull(resource, "Die Ressource konnte nicht gefunden werden: " + filePath);

		String result = "";
		try (InputStream inputStream = resource.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {

			String line;
			while ((line = reader.readLine()) != null) {
				result = result + line;
			}
		}
		return result;
	}
}