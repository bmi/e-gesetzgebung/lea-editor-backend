// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.export.adapter;

import de.itzbund.egesetz.bmi.lea.export.services.PdfService;
import org.apache.xmlgraphics.io.Resource;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ClasspathResolverURIAdapterTest {

    private static ClasspathResolverURIAdapter resolver;

    @BeforeAll
    static void setUp() {
        resolver = new ClasspathResolverURIAdapter();
    }

    @Test
    void testGetResourceValidClasspathURIReturnsResource() throws IOException, URISyntaxException {
        ClasspathResolverURIAdapter resolver = new ClasspathResolverURIAdapter();
        URI uri = new URI("classpath:///adapter/resource.txt");
        Resource resource = resolver.getResource(uri);
        assertNotNull(resource);
        String testString = new String(resource.readAllBytes(), StandardCharsets.UTF_8);
        assertEquals("TEST", testString);
    }

    @Test
    void testGetOutputStreamClasspathURIThrowsException() throws URISyntaxException {
        URI uri = new URI("classpath:/path/to/resource.txt");
        Exception exception = assertThrows(MethodNotAllowedException.class, () -> resolver.getOutputStream(uri));
        assertEquals("This is a read only ResourceResolver. You can not use it's outputstream", exception.getMessage());
    }

    @Test
    void testGetResourceNonClasspathURIDelegatesToWrapped() throws IOException, URISyntaxException {
        URI uri = new URI("http://example.com/resource.txt");

        // Mock the wrapped ResourceResolver to return a specific Resource
        Resource expectedResource = new Resource(new ByteArrayInputStream("DELEGATED".getBytes(StandardCharsets.UTF_8)));
        ClasspathResolverURIAdapter spyResolver = Mockito.spy(resolver);
        Mockito.doReturn(expectedResource).when(spyResolver).getResource(uri);

        Resource resource = spyResolver.getResource(uri);
        assertNotNull(resource);
        String testString = new String(resource.readAllBytes(), StandardCharsets.UTF_8);
        assertEquals("DELEGATED", testString);
    }

    @Test
    void testGetOutputStreamNonClasspathURIDelegatesToWrapped() throws IOException, URISyntaxException {
        URI uri = new URI("http://example.com/resource.txt");

        OutputStream mockOutputStream = mock(OutputStream.class);
        ClasspathResolverURIAdapter spyResolver = Mockito.spy(resolver);
        Mockito.doReturn(mockOutputStream).when(spyResolver).getOutputStream(uri);

        OutputStream outputStream = spyResolver.getOutputStream(uri);
        assertNotNull(outputStream);
        assertEquals(mockOutputStream, outputStream);
    }
}