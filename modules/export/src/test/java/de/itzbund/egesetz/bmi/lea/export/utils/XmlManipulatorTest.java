// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.export.utils;

import de.itzbund.egesetz.bmi.lea.domain.model.Reply;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.ReplyId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.export.comment.CommentPosition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class XmlManipulatorTest {

	private String xmlWithComments;

	private String xmlWithCommentsAndReplies;

	private String xmlWithoutComments;

	private User user;

	private Instant instant;

	private Map<String, CommentPosition> commentPositions;

	@BeforeEach
	void setUp() throws IOException {
		xmlWithoutComments = readFileSpringBootWay("/xml/regelungstext_without_comments_and_replies.xml");
		xmlWithComments = readFileSpringBootWay("/xml/regelungstext_comments.xml");
		xmlWithCommentsAndReplies = readFileSpringBootWay("/xml/regelungstext_comments_and_replies.xml");

		user = new User(UUID.fromString("23a599d8-3070-4f32-b344-219b2a2f0c4e"), new UserId("1"), "Name", "name@example.de");
		LocalDate date = LocalDate.of(2023, 11, 11);
		instant = date.atStartOfDay(ZoneId.systemDefault()).toInstant();

		commentPositions = new HashMap<>();

		CommentPosition commentPosition = new CommentPosition();
		commentPosition.setCommentId(new CommentId(UUID.fromString("23a599d8-3070-4f32-b344-219b2a2f0c3d")));
		commentPosition.setParentGuid("61ebb381-6e5f-4dd1-ac0a-da872ccde46b");
		commentPosition.setCreatedBy(user);
		commentPosition.setCreatedAt(instant);
		commentPosition.setContent("[{\"children\":[{\"text\":\"EV ist eine Abk.\"}]}]");
		commentPosition.setDeleted(false);

		commentPositions.put("23a599d8-3070-4f32-b344-219b2a2f0c3d", commentPosition);
	}

	@Test
	void testReplaceAknNamespace() {
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<akn:akomaNtoso\n"
			+ "        xsi:schemaLocation=\"http://Inhaltsdaten.LegalDocML.de/1.6/ Grammatiken/legalDocML.de-vorblatt.xsd http://Metadaten.LegalDocML.de/1.6/ Grammatiken/legalDocML.de-metadaten.xsd\"\n"
			+ "        xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:akn=\"http://Inhaltsdaten.LegalDocML.de/1.6/\">\n"
			+ "</akn:akomaNtoso>";
		String xslt = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
			+ "<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"\n"
			+ "                xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n"
			+ "                xmlns:math=\"http://www.w3.org/2005/xpath-functions/math\"\n"
			+ "                xmlns:fo=\"http://www.w3.org/1999/XSL/Format\"\n"
			+ "                xmlns:akn=\"http://Inhaltsdaten.LegalDocML.de/1.4/\"\n"
			+ "                exclude-result-prefixes=\"xsd math\"\n"
			+ "                expand-text=\"true\"\n"
			+ "                version=\"3.0\">\n"
			+ "</xsl:stylesheet>";
		String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
			+ "<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"\n"
			+ "                xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n"
			+ "                xmlns:math=\"http://www.w3.org/2005/xpath-functions/math\"\n"
			+ "                xmlns:fo=\"http://www.w3.org/1999/XSL/Format\"\n"
			+ "                xmlns:akn=\"http://Inhaltsdaten.LegalDocML.de/1.6/\"\n"
			+ "                exclude-result-prefixes=\"xsd math\"\n"
			+ "                expand-text=\"true\"\n"
			+ "                version=\"3.0\">\n"
			+ "</xsl:stylesheet>";
		XmlManipulator xmlManipulator = new XmlManipulator();
		Assertions.assertEquals(expected, xmlManipulator.replaceAknNamespace(xml, xslt));
	}

	@Test
	void testReplaceAknNamespaceAddMissingNamespace() {
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<akn:akomaNtoso\n"
			+ "        xsi:schemaLocation=\"http://Inhaltsdaten.LegalDocML.de/1.6/ Grammatiken/legalDocML.de-vorblatt.xsd http://Metadaten.LegalDocML.de/1.6/ Grammatiken/legalDocML.de-metadaten.xsd\"\n"
			+ "        xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:akn=\"http://Inhaltsdaten.LegalDocML.de/1.6/\">\n"
			+ "</akn:akomaNtoso>";
		String xslt = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
			+ "<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"\n"
			+ "                xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n"
			+ "                xmlns:math=\"http://www.w3.org/2005/xpath-functions/math\"\n"
			+ "                xmlns:fo=\"http://www.w3.org/1999/XSL/Format\"\n"
			+ "                exclude-result-prefixes=\"xsd math\"\n"
			+ "                expand-text=\"true\"\n"
			+ "                version=\"3.0\">\n"
			+ "</xsl:stylesheet>";
		String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
			+ "<xsl:stylesheet xmlns:akn=\"http://Inhaltsdaten.LegalDocML.de/1.6/\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"\n"
			+ "                xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n"
			+ "                xmlns:math=\"http://www.w3.org/2005/xpath-functions/math\"\n"
			+ "                xmlns:fo=\"http://www.w3.org/1999/XSL/Format\"\n"
			+ "                exclude-result-prefixes=\"xsd math\"\n"
			+ "                expand-text=\"true\"\n"
			+ "                version=\"3.0\">\n"
			+ "</xsl:stylesheet>";
		XmlManipulator xmlManipulator = new XmlManipulator();
		Assertions.assertEquals(expected, xmlManipulator.replaceAknNamespace(xml, xslt));
	}

	@Test
	void testReplaceAknNamespaceNoChange() {
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<akn:akomaNtoso\n"
			+ "        xsi:schemaLocation=\"http://Inhaltsdaten.LegalDocML.de/1.6/ Grammatiken/legalDocML.de-vorblatt.xsd http://Metadaten.LegalDocML.de/1.6/ Grammatiken/legalDocML.de-metadaten.xsd\"\n"
			+ "        xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:akn=\"http://Inhaltsdaten.LegalDocML.de/1.6/\">\n"
			+ "</akn:akomaNtoso>";
		String xslt = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
			+ "<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"\n"
			+ "                xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n"
			+ "                xmlns:math=\"http://www.w3.org/2005/xpath-functions/math\"\n"
			+ "                xmlns:fo=\"http://www.w3.org/1999/XSL/Format\"\n"
			+ "                xmlns:akn=\"http://Inhaltsdaten.LegalDocML.de/1.6/\"\n"
			+ "                exclude-result-prefixes=\"xsd math\"\n"
			+ "                expand-text=\"true\"\n"
			+ "                version=\"3.0\">\n"
			+ "</xsl:stylesheet>";
		XmlManipulator xmlManipulator = new XmlManipulator();
		Assertions.assertEquals(xslt, xmlManipulator.replaceAknNamespace(xml, xslt));
	}

	@Test
	void testReplaceIncludePath() {
		String xslt = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
			+ "<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"\n"
			+ "                xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n"
			+ "                xmlns:math=\"http://www.w3.org/2005/xpath-functions/math\"\n"
			+ "                xmlns:fo=\"http://www.w3.org/1999/XSL/Format\"\n"
			+ "                xmlns:akn=\"http://Inhaltsdaten.LegalDocML.de/1.6/\"\n"
			+ "                exclude-result-prefixes=\"xsd math\"\n"
			+ "                expand-text=\"true\"\n"
			+ "                version=\"3.0\">\n"
			+ "    <xsl:include href=\"include/common.xslt\"/>\n"
			+ "</xsl:stylesheet>";
		String replaced = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
			+ "<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"\n"
			+ "                xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n"
			+ "                xmlns:math=\"http://www.w3.org/2005/xpath-functions/math\"\n"
			+ "                xmlns:fo=\"http://www.w3.org/1999/XSL/Format\"\n"
			+ "                xmlns:akn=\"http://Inhaltsdaten.LegalDocML.de/1.6/\"\n"
			+ "                exclude-result-prefixes=\"xsd math\"\n"
			+ "                expand-text=\"true\"\n"
			+ "                version=\"3.0\">\n"
			+ "    <xsl:include href=\"src/main/resources/stylesheets/include/common.xslt\"/>\n"
			+ "</xsl:stylesheet>";
		XmlManipulator xmlManipulator = new XmlManipulator();
		Assertions.assertEquals(replaced, xmlManipulator.replaceIncludePath(xslt, "src/main/resources/stylesheets/"));
	}

	@Test
	void testAddComments() {
		XmlManipulator xmlManipulator = new XmlManipulator();
		assertEquals(xmlWithComments, xmlManipulator.addComments(xmlWithoutComments, commentPositions));
	}

	@Test
	void testAddCommentsAndReplies() {
		Reply reply1 = new Reply(new ReplyId(UUID.fromString("23a599d8-3070-4f32-b344-219b2a2f0cef")),
			new CommentId(UUID.fromString("23a599d8-3070-4f32-b344-219b2a2f0c3d")),
			new DocumentId(UUID.fromString("935aad2d-f92c-4671-b6ad-5e72b11a7833")),
			"61ebb381-6e5f-4dd1-ac0a-da872ccde46b",
			"[{\"children\":[{\"text\":\"Eine weitere Antwort\"}]}]",
			user, user, instant, instant, false);

		Reply reply2 = new Reply(new ReplyId(UUID.fromString("23a599d8-3070-4f32-b344-219b2a2f0cef")),
			new CommentId(UUID.fromString("23a599d8-3070-4f32-b344-219b2a2f0c3d")),
			new DocumentId(UUID.fromString("935aad2d-f92c-4671-b6ad-5e72b11a7833")),
			"61ebb381-6e5f-4dd1-ac0a-da872ccde46b",
			"[{\"children\":[{\"text\":\"Eine Antwort auf Kommentar Eins, ja\"}]}]",
			user, user, instant, instant, false);

		List<Reply> replyList = new ArrayList<>();
		replyList.add(reply1);
		replyList.add(reply2);

		commentPositions.get("23a599d8-3070-4f32-b344-219b2a2f0c3d").setReplies(replyList);

		XmlManipulator xmlManipulator = new XmlManipulator();
		assertEquals(xmlWithCommentsAndReplies, xmlManipulator.addComments(xmlWithoutComments, commentPositions));
	}

	private String readFileSpringBootWay(String filePath) throws IOException {
		Resource resource = new ClassPathResource(filePath);
		assertNotNull(resource, "Die Ressource konnte nicht gefunden werden: " + filePath);

		String result = "";
		try (InputStream inputStream = resource.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {

			String line;
			while ((line = reader.readLine()) != null) {
				result = result + line;
			}
		}
		return result;
	}
}