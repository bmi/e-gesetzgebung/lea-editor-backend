// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.export.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

class ExampleTest {

    @Test
    void testLoadEmptyDocument() {
        Assertions.assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?><?xml-model href=\"Grammatiken/legalDocML.de.sch\""
            + " type=\"application/xml\" schematypens=\"http://purl.oclc.org/dsdl/schematron\"?>\n"
            + "<akn:akomaNtoso\n"
            + "        xsi:schemaLocation=\"http://Inhaltsdaten.LegalDocML.de/1.6/"
            + "        Grammatiken/legalDocML.de-vorblatt.xsd"
            + "        Grammatiken/legalDocML.de-begruendung.xsd"
            + "        Grammatiken/legalDocML.de-regelungstextentwurfsfassung.xsd"
            + "        Grammatiken/legalDocML.de-offenestruktur.xsd"
            + "        http://Metadaten.LegalDocML.de/1.6/ Grammatiken/legalDocML.de-metadaten.xsd\"\n"
            + "        xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:akn=\"http://Inhaltsdaten.LegalDocML.de/1.6/\">"
            + "</akn:akomaNtoso>", Example.loadEmptyDocument());
    }
    @Test
    void testLoadDummyDocument() {
        UUID documentId = UUID.randomUUID();
        Assertions.assertEquals("<fo:root xmlns:fo=\"http://www.w3.org/1999/XSL/Format\">\n"
            + "    <fo:layout-master-set>\n"
            + "        <fo:simple-page-master master-name=\"A4\" page-width=\"21cm\" page-height=\"29.7cm\">\n"
            + "            <fo:region-body margin=\"2cm\"/>\n"
            + "        </fo:simple-page-master>\n"
            + "    </fo:layout-master-set>\n"
            + "\n"
            + "    <fo:page-sequence master-reference=\"A4\">\n"
            + "        <fo:flow flow-name=\"xsl-region-body\">\n"
            + "            <fo:block font-size=\"18pt\" text-align=\"center\" space-after=\"12pt\">" + documentId + "</fo:block>\n"
            + "            <fo:block font-size=\"12pt\" space-after=\"6pt\">Die PDF-Erstellung funktioniert.</fo:block>\n"
            + "        </fo:flow>\n"
            + "    </fo:page-sequence>\n"
            + "</fo:root>\n", Example.loadDummyDocument(documentId));
    }

    @Test
    public void testErrorDocumentConstructorThrowsException() throws Exception {
        Constructor<Example> constructor = Example.class.getDeclaredConstructor();
        constructor.setAccessible(true);

        assertThatThrownBy(constructor::newInstance)
            .isInstanceOf(InvocationTargetException.class);
    }
}