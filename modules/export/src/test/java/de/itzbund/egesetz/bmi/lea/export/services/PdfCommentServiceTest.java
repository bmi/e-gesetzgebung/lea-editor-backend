// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.export.services;

import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.CommentState;
import de.itzbund.egesetz.bmi.lea.domain.model.Comment;
import de.itzbund.egesetz.bmi.lea.domain.model.Position;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CommentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.DocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CommentRestPort;
import de.itzbund.egesetz.bmi.lea.export.comment.CommentPosition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class PdfCommentServiceTest {

	@Mock
	private CommentRestPort commentServiceMock;

	@InjectMocks
	PdfCommentService pdfCommentService;

	private String jsonWithoutComments;

	private String jsonWithComments;

	@BeforeEach
	void setUp() throws IOException {
		MockitoAnnotations.initMocks(this);
		jsonWithoutComments = readFileSpringBootWay("/json/document_without_comments.json");
		jsonWithComments = readFileSpringBootWay("/json/document_with_comments.json");
	}

	@Test
	void testCollectComments() {
		DocumentDTO document = new DocumentDTO();
		document.setId(UUID.fromString("935aad2d-f92c-4671-b6ad-5e72b11a7833"));
		document.setContent(jsonWithComments);

		Map<String, CommentPosition> commentPositions = pdfCommentService.collectComments(document);
		assertEquals(0, commentPositions.size());

		User user = new User(UUID.fromString("23a599d8-3070-4f32-b344-219b2a2f0c4e"), new UserId("1"), "Name", "name@example.de");
		LocalDate date = LocalDate.of(2023, 11, 11);
		Instant instant = date.atStartOfDay(ZoneId.systemDefault()).toInstant();

		List<Comment> commentFromDb = new ArrayList<>();
		Comment comment = new Comment(new DocumentId(UUID.fromString("935aad2d-f92c-4671-b6ad-5e72b11a7833")),
			new CommentId(UUID.fromString("23a599d8-3070-4f32-b344-219b2a2f0c3d")),
			"[{\"children\":[{\"text\":\"EV ist eine Abk.\"}]}]",
			CommentState.OPEN,
			user, user, instant, instant,
			new Position(1, List.of(new Long[]{0L, 0L, 0L, 0L, 0L})),
			new Position(1, List.of(new Long[]{0L, 0L, 0L, 0L, 0L})),
			false, null);
		commentFromDb.add(comment);

		when(commentServiceMock.loadAllCommentsOfUserOfDocument(any(), any())).thenReturn(commentFromDb);

		commentPositions = pdfCommentService.collectComments(document);
		assertEquals(1, commentPositions.size());
		assertEquals("61ebb381-6e5f-4dd1-ac0a-da872ccde46b", commentPositions.get("23a599d8-3070-4f32-b344-219b2a2f0c3d").getParentGuid());
	}

	@Test
	void testCollectCommentsNoComments() {
		DocumentDTO document = new DocumentDTO();
		document.setId(UUID.fromString("c113627b-84e3-4153-b323-c5fa8b1a8091"));
		document.setContent(jsonWithoutComments);

		Map<String, CommentPosition> commentPositions = pdfCommentService.collectComments(document);
		assertEquals(0, commentPositions.size());

		List<Comment> commentFromDb = new ArrayList<>();

		when(commentServiceMock.loadAllCommentsOfUserOfDocument(any(), any())).thenReturn(commentFromDb);

		commentPositions = pdfCommentService.collectComments(document);
		assertEquals(0, commentPositions.size());
	}

	private String readFileSpringBootWay(String filePath) throws IOException {
		Resource resource = new ClassPathResource(filePath);
		assertNotNull(resource, "Die Ressource konnte nicht gefunden werden: " + filePath);

		String result = "";
		try (InputStream inputStream = resource.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {

			String line;
			while ((line = reader.readLine()) != null) {
				result = result + line;
			}
		}
		return result;
	}
}