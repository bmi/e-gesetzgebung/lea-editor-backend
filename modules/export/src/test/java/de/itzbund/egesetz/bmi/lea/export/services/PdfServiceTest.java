// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.export.services;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.aggregate.CompoundDocument;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.PkpZuleitungDokumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.mappers.DocumentMapper;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.CompoundDocumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.DrucksacheDokumentPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.CompoundDocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.ConverterRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.DocumentRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.EIdGeneratorRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.ExportRestPort;
import de.itzbund.egesetz.bmi.lea.domain.services.UserService;
import de.itzbund.egesetz.bmi.lea.domain.services.eid.IllegalDocumentType;
import de.itzbund.egesetz.bmi.lea.domain.services.logging.DomainLoggingService;
import de.itzbund.egesetz.bmi.lea.domain.services.validate.impl.ValidationException;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.export.comment.CommentPosition;
import de.itzbund.egesetz.bmi.lea.export.document.MultipleDocumentDTO;
import de.itzbund.egesetz.bmi.lea.export.utils.TestObjects;
import de.itzbund.egesetz.bmi.lea.export.utils.XmlManipulator;
import de.itzbund.egesetz.bmi.lea.export.utils.XmlMerger;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.when;


class PdfServiceTest {

	@Mock
	private DocumentRestPort documentService;

	@Mock
	private CompoundDocumentRestPort compoundDocumentService;

	@Mock
	private CompoundDocument compoundDocument;

	@Mock
	private ConverterRestPort converterService;

	@Mock
	private EIdGeneratorRestPort eIdGeneratorRestPort;

	@Mock
	private XmlManipulator xmlManipulatorMock;

	@Mock
	private PdfCommentService pdfCommentServiceMock;

	private XmlManipulator xmlManipulator;

	@Mock
	private XmlMerger xmlMergerMock;

	private XmlMerger xmlMerger;

	@Mock
	private UserService userService;

	@Mock
	protected CompoundDocumentPersistencePort compoundDocumentRepository;

	@Mock
	protected DrucksacheDokumentPersistencePort drucksacheDokumentRepository;

	@Mock
	protected DokumentPersistencePort documentRepository;

	@Mock
	private DomainLoggingService domainLoggingService;

	@Mock
	private ExportRestPort exportService;

	@Mock
	protected LeageSession session;

	@Mock
	protected DocumentMapper documentMapper;


	@InjectMocks
	private PdfService pdfService;

	private DocumentDTO documentDTO;

	private String xml;

	private String xslt;

	@BeforeEach
	void setUp() throws IOException {
		MockitoAnnotations.initMocks(this);
		String jsonContent = "[{\"xml-model\":\"href=\\\"Grammatiken\\/legalDocML.de.sch\\\" type=\\\"application\\/xml\\\" schematypens=\\\"http:\\/\\/purl.oclc.org\\/dsdl\\/schematron\\\"\",\"children\":[{\"children\":[{\"children\":[{\"children\":[{\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRthis\",\"value\":\"eli\\/dl\\/2023\\/bundesregierung\\/1\\/regelungsentwurf\\/vorblatt-1\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRuri\",\"value\":\"eli\\/dl\\/2023\\/bundesregierung\\/1\\/regelungsentwurf\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"übergreifende-id\",\"type\":\"akn:FRBRalias\",\"value\":\"55a9bbec-a625-4160-aa3e-ac28d7a397f7\"},{\"date\":\"2023-11-08\",\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"entwurfsfassung\",\"type\":\"akn:FRBRdate\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"href\":\"recht.bund.de\\/institution\\/bundesregierung\",\"type\":\"akn:FRBRauthor\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRcountry\",\"value\":\"de\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRnumber\",\"value\":\"1\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRname\",\"value\":\"regelungsentwurf\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRsubtype\",\"value\":\"vorblatt-1\"}],\"type\":\"akn:FRBRWork\"},{\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRthis\",\"value\":\"eli\\/dl\\/2023\\/bundesregierung\\/1\\/regelungsentwurf\\/bundestag\\/2023-04-01\\/1\\/deu\\/vorblatt-1\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRuri\",\"value\":\"eli\\/dl\\/2023\\/bundesregierung\\/1\\/regelungsentwurf\\/bundestag\\/2023-04-01\\/1\\/deu\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"href\":\"recht.bund.de\\/institution\\/bundestag\",\"type\":\"akn:FRBRauthor\"},{\"date\":\"2023-04-01\",\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"version\",\"type\":\"akn:FRBRdate\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"language\":\"deu\",\"type\":\"akn:FRBRlanguage\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRversionNumber\",\"value\":\"1\"}],\"type\":\"akn:FRBRExpression\"},{\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRthis\",\"value\":\"eli\\/dl\\/2023\\/bundesregierung\\/1\\/regelungsentwurf\\/bundestag\\/2023-04-01\\/1\\/deu\\/vorblatt-1.xml\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRuri\",\"value\":\"eli\\/dl\\/2023\\/bundesregierung\\/1\\/regelungsentwurf\\/bundestag\\/2023-04-01\\/1\\/deu\\/vorblatt-1.xml\"},{\"date\":\"2023-04-01\",\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"generierung\",\"type\":\"akn:FRBRdate\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"href\":\"recht.bund.de\",\"type\":\"akn:FRBRauthor\"},{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:FRBRformat\",\"value\":\"xml\"}],\"type\":\"akn:FRBRManifestation\"}],\"source\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:identification\"},{\"children\":[{\"children\":[{\"children\":[{\"children\":[{\"text\":\"verordnung\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"meta:typ\"},{\"children\":[{\"children\":[{\"text\":\"mantelform\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"meta:form\"},{\"children\":[{\"children\":[{\"text\":\"entwurfsfassung\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"meta:fassung\"},{\"children\":[{\"children\":[{\"text\":\"vorblatt\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"meta:art\"},{\"children\":[{\"children\":[{\"text\":\"bundesregierung\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"meta:initiant\"},{\"children\":[{\"children\":[{\"text\":\"nicht-vorhanden\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"meta:bearbeitendeInstitution\"}],\"type\":\"meta:legalDocML.de_metadaten\",\"xmlns:meta\":\"http:\\/\\/Metadaten.LegalDocML.de\"}],\"source\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:proprietary\"}],\"type\":\"akn:meta\"},{\"children\":[{\"eId\":\"doktitel-1\",\"children\":[{\"eId\":\"doktitel-1_text-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"Referentenentwurf\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:docStage\"},{\"refersTo\":\"artikel\",\"children\":[{\"children\":[{\"text\":\" des \"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:span\"},{\"children\":[{\"children\":[{\"text\":\"Bundesministeriums des Innern und für Heimat\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:docProponent\"},{\"children\":[{\"children\":[{\"text\":\"08\\/11\\/23 - AK - RV (Bezeichnung)\"}],\"type\":\"lea:textWrapper\"}],\"type\":\"akn:docTitle\"},{\"children\":[{\"text\":\"(\"}],\"type\":\"lea:textWrapper\"},{\"eId\":\"doktitel-1_text-1_kurztitel-1\",\"children\":[{\"children\":[{\"text\":\"08\\/11\\/23 - AK - RV\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"52f446f3-d13d-4dc8-baf4-ab813dac9c1e\",\"type\":\"akn:shortTitle\"},{\"children\":[{\"text\":\" \\u2013 \"}],\"type\":\"lea:textWrapper\"},{\"refersTo\":\"amtliche-abkuerzung\",\"children\":[{\"children\":[{\"text\":\"08\\/11-AK-RV\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:inline\"},{\"children\":[{\"text\":\")\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"efb657dc-2ae1-4c7e-a54b-cff525e380fd\",\"type\":\"akn:p\"}],\"GUID\":\"974a760e-512d-4b94-8ff4-5d7bdb589d25\",\"type\":\"akn:longTitle\"}],\"type\":\"akn:preface\"},{\"children\":[{\"refersTo\":\"vorblattabschnitt-problem-und-ziel\",\"eId\":\"container-a\",\"children\":[{\"eId\":\"container-a_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"a\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"A.\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"5774186e-cdd3-447f-85f8-6ea6356a5014\",\"type\":\"akn:num\"},{\"eId\":\"container-a_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Problem und Ziel\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"8541808e-feec-4e3b-bab6-b770ed88a80a\",\"type\":\"akn:heading\"},{\"eId\":\"container-a_inhalt-1\",\"children\":[{\"eId\":\"container-a_inhalt-1_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"ab8bb046-8009-4eaa-bdc9-202d1cf55ff4\",\"type\":\"akn:p\"}],\"GUID\":\"b765fd4e-0d18-4d8e-a0e0-f989106d1b61\",\"type\":\"akn:content\"}],\"GUID\":\"b994a1c4-a1e4-4500-9121-6a44e9affe39\",\"name\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:hcontainer\"},{\"refersTo\":\"vorblattabschnitt-loesung\",\"eId\":\"container-b\",\"children\":[{\"eId\":\"container-b_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"b\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"B.\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"ab40bc76-9a80-4490-9de4-ee3fda74ebab\",\"type\":\"akn:num\"},{\"eId\":\"container-b_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Lösung\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"8f0557fd-46e4-446d-bb71-e3292daaa0e9\",\"type\":\"akn:heading\"},{\"eId\":\"container-b_inhalt-1\",\"children\":[{\"eId\":\"container-b_inhalt-1_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"0a429181-c0a7-4520-ac09-82a48b8667ef\",\"type\":\"akn:p\"}],\"GUID\":\"37624084-5297-429f-8f67-1db7fd010d0b\",\"type\":\"akn:content\"}],\"GUID\":\"493cd5c2-edd6-4493-bfa8-ac93a59621d1\",\"name\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:hcontainer\"},{\"refersTo\":\"vorblattabschnitt-alternativen\",\"eId\":\"container-c\",\"children\":[{\"eId\":\"container-c_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"c\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"C.\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"4d35aef3-0980-43df-9a25-5629946f9cd8\",\"type\":\"akn:num\"},{\"eId\":\"container-c_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Alternativen\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"2b79f2fa-49ed-4c68-8df2-683a61f5944e\",\"type\":\"akn:heading\"},{\"eId\":\"container-c_inhalt-1\",\"children\":[{\"eId\":\"container-c_inhalt-1_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"72feb7d0-c3a1-4ff3-b49d-2a8fc2f5fe17\",\"type\":\"akn:p\"}],\"GUID\":\"455de1a9-804b-4b0d-b43e-c352bb84c305\",\"type\":\"akn:content\"}],\"GUID\":\"501e0ddc-8781-499b-a8b7-5647a69f092a\",\"name\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:hcontainer\"},{\"refersTo\":\"vorblattabschnitt-haushaltsausgaben-ohne-erfuellungsaufwand\",\"eId\":\"container-d\",\"children\":[{\"eId\":\"container-d_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"d\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"D.\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"56ddc42b-f46b-4c15-8dca-dcbc27ca84ee\",\"type\":\"akn:num\"},{\"eId\":\"container-d_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Haushaltsausgaben ohne Erfüllungsaufwand\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"a6fb8cda-8857-4fda-bdaa-510c46f34c0e\",\"type\":\"akn:heading\"},{\"eId\":\"container-d_inhalt-1\",\"children\":[{\"eId\":\"container-d_inhalt-1_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"e8e0f121-d651-4233-aced-262b1c51e759\",\"type\":\"akn:p\"}],\"GUID\":\"d86be628-f8f7-4fed-8042-2beefbd89e8c\",\"type\":\"akn:content\"}],\"GUID\":\"fc3f849f-4979-4163-a305-c0266b3aee99\",\"name\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:hcontainer\"},{\"refersTo\":\"vorblattabschnitt-erfuellungsaufwand\",\"eId\":\"container-e\",\"children\":[{\"eId\":\"container-e_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"e\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"E.\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"b8edfeb9-7651-48cc-8bc4-062ff24b6aa1\",\"type\":\"akn:num\"},{\"eId\":\"container-e_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Erfüllungsaufwand\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"f3000f98-5cc8-419e-af8d-7ebb8ec752d3\",\"type\":\"akn:heading\"},{\"eId\":\"container-e_inhalt-1\",\"children\":[{\"eId\":\"container-e_inhalt-1_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"b24167ac-050a-4ac9-ada1-f2aeaf799908\",\"type\":\"akn:p\"},{\"refersTo\":\"erfuellungsaufwand-fuer-buergerinnen-und-buerger\",\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.1\",\"children\":[{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.1_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"e.1\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"E.1\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"011322f7-e001-4222-93a8-cfffaff2f3c8\",\"type\":\"akn:num\"},{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.1_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Erfüllungsaufwand für Bürgerinnen und Bürger\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"5090e7b4-9657-43de-bc8f-3e7e40f394ff\",\"type\":\"akn:heading\"},{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.1_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"524775c5-3a81-4c0c-a15a-be06fec416c7\",\"type\":\"akn:p\"}],\"GUID\":\"28ae155a-c6c2-473a-9940-b2e75d22f12f\",\"type\":\"akn:tblock\"},{\"refersTo\":\"erfuellungsaufwand-fuer-die-wirtschaft\",\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.2\",\"children\":[{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.2_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"e.2\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"E.2\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"e464451e-32ff-41f0-a5cb-64e946ca1ec7\",\"type\":\"akn:num\"},{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.2_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Erfüllungsaufwand für die Wirtschaft\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"e684809d-0dec-4652-9938-b507fdcc06e0\",\"type\":\"akn:heading\"},{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.2_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"3ebfea81-9519-4d40-864c-dd7ef55a7d11\",\"type\":\"akn:p\"},{\"refersTo\":\"davon-buerokratiekosten-aus-informationspflichten\",\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.2_inhaltabschnitt-1\",\"children\":[{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.2_inhaltabschnitt-1_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Davon Bürokratiekosten aus Informationspflichten\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"95d23084-86ad-4abd-810c-ec2a757a2912\",\"type\":\"akn:heading\"},{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.2_inhaltabschnitt-1_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"1d64c76e-cd11-4a71-a71a-2ef5bab0ff20\",\"type\":\"akn:p\"}],\"GUID\":\"167bca08-c828-48f2-8387-02b9401d5e67\",\"type\":\"akn:tblock\"}],\"GUID\":\"4b187204-187d-4841-9ca7-526c88f4dff0\",\"type\":\"akn:tblock\"},{\"refersTo\":\"erfuellungsaufwand-der-verwaltung\",\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.3\",\"children\":[{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.3_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"e.3\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"E.3\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"c4f1ac1e-f41b-4912-b399-6794a613fb3e\",\"type\":\"akn:num\"},{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.3_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Erfüllungsaufwand der Verwaltung\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"9e9dad4b-6728-4fe8-9a52-1e8e2b6b6547\",\"type\":\"akn:heading\"},{\"eId\":\"container-e_inhalt-1_inhaltabschnitt-e.3_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"f33f03d8-ed43-44a4-a371-b4d87ff9e0f6\",\"type\":\"akn:p\"}],\"GUID\":\"bfa296c6-3b90-4c36-91ce-72d688c695ce\",\"type\":\"akn:tblock\"}],\"GUID\":\"1e148fc9-d2b8-46a6-a454-f6f85499e141\",\"type\":\"akn:content\"}],\"GUID\":\"d647a971-5150-41b6-aed7-d93fb5639370\",\"name\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:hcontainer\"},{\"refersTo\":\"vorblattabschnitt-weitere-kosten\",\"eId\":\"container-f\",\"children\":[{\"eId\":\"container-f_bezeichnung-1\",\"children\":[{\"children\":[{\"children\":[{\"text\":\"\"}],\"type\":\"lea:textWrapper\"}],\"name\":\"f\",\"type\":\"akn:marker\"},{\"children\":[{\"text\":\"F.\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"35402d8a-db0c-484d-b3e9-cd1ec7fd5f61\",\"type\":\"akn:num\"},{\"eId\":\"container-f_überschrift-1\",\"children\":[{\"children\":[{\"text\":\"Weitere Kosten\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"d7e1cd08-e936-4445-9a1c-0c04b56d2081\",\"type\":\"akn:heading\"},{\"eId\":\"container-f_inhalt-1\",\"children\":[{\"eId\":\"container-f_inhalt-1_text-1\",\"children\":[{\"children\":[{\"text\":\"[Platzhalter]\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"ae500a9a-fe77-4cff-9aa7-31e1b4fba50f\",\"type\":\"akn:p\"},{\"lea:editable\":true,\"children\":[{\"lea:editable\":true,\"children\":[{\"children\":[{\"text\":\"Sonstige Kosten\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"bacd79cf-a900-4a6d-bf7e-20a34a537127\",\"type\":\"akn:heading\"},{\"children\":[{\"children\":[{\"text\":\"Tesz\"}],\"type\":\"lea:textWrapper\"}],\"GUID\":\"1e0c00d0-8dd7-447a-8637-7b55cd13b612\",\"type\":\"akn:p\"}],\"GUID\":\"ca96d012-71e4-4067-ad22-43c71e48b8be\",\"type\":\"akn:tblock\"}],\"GUID\":\"6bd87d8a-5330-4bc2-8ee0-684e173490c0\",\"type\":\"akn:content\"}],\"GUID\":\"a4c09084-2701-4f80-9438-5801b145016c\",\"name\":\"attributsemantik-noch-undefiniert\",\"type\":\"akn:hcontainer\"}],\"type\":\"akn:mainBody\"}],\"name\":\"vorblatt\",\"type\":\"akn:doc\"}],\"xsi:schemaLocation\":\"http:\\/\\/Inhaltsdaten.LegalDocML.de\\/1.4\\/ Grammatiken\\/legalDocML.de-vorblatt.xsd http:\\/\\/Metadaten.LegalDocML.de\\/1.4\\/ Grammatiken\\/legalDocML.de-metadaten.xsd\",\"xmlns:xsi\":\"http:\\/\\/www.w3.org\\/2001\\/XMLSchema-instance\",\"type\":\"akn:akomaNtoso\",\"xmlns:akn\":\"http:\\/\\/Inhaltsdaten.LegalDocML.de\\/1.4\\/\"}]";
		documentDTO = new DocumentDTO();
		documentDTO.setTitle("Beispieldokument");
		documentDTO.setContent(jsonContent);
		documentDTO.setType(DocumentType.VORBLATT_STAMMGESETZ);
		xml = readFileSpringBootWay("/xml/vorblatt.xml");
		String xsltMain = readFileSpringBootWay("/stylesheets/main.xslt");
		xmlMerger = new XmlMerger();
		xmlManipulator = new XmlManipulator();
		xslt = xmlMerger.includeXslt(xsltMain);

		when(userService.getUser()).thenReturn(User.builder().gid(new UserId(Utils.getRandomString())).build());
		when(session.getUser()).thenReturn(TestObjects
			.getUserEntity());
	}

	@Test
	void testConvertDocumentToInputStream() throws IllegalDocumentType, IOException, ValidationException {
		UUID documentId = UUID.randomUUID();

		MultipleDocumentDTO multipleDocumentDTO = new MultipleDocumentDTO();
		multipleDocumentDTO.getDocuments().add(documentId);
		// first test pdf
		multipleDocumentDTO.setMergeDocuments(true);
		multipleDocumentDTO.setTitle("document");

		Map<String, CommentPosition> comments = new HashMap<>();

		when(documentService.getDocumentById(any(UUID.class), anyBoolean())).thenReturn(documentDTO);
		when(eIdGeneratorRestPort.erzeugeEIdsFuerDokument(any())).thenReturn(new JSONObject());
		when(converterService.convertJsonToXml(any(JSONObject.class))).thenReturn(xml);
		when(xmlManipulatorMock.replaceAknNamespace(any(), any())).thenReturn(xslt);
		when(xmlManipulatorMock.replaceIncludePath(any(), any())).thenReturn(xslt);
		when(pdfCommentServiceMock.collectComments(any())).thenReturn(comments);
		when(xmlManipulatorMock.addComments(any(), any())).thenReturn(xml);
		when(pdfCommentServiceMock.getJsonContent()).thenReturn("");
		when(xmlMergerMock.includeXslt(any())).thenReturn(xslt);
		when(xmlMergerMock.merge(any(), any())).thenReturn(xml);
		when(exportService.exportSingleDocument(any(String.class), any(ArrayList.class))).thenReturn(readFileSpringBootWay("/xml/vorblatt.xml"));

		InputStream result = pdfService.convertDocumentToInputStream(multipleDocumentDTO);
		assertNotNull(result);

		// switch to zip
		multipleDocumentDTO.setMergeDocuments(false);

		result = pdfService.convertDocumentToInputStream(multipleDocumentDTO);
		assertNotNull(result);

		// two similar documents (same title)
		UUID uuid = multipleDocumentDTO.getDocuments().get(0);
		multipleDocumentDTO.getDocuments().add(uuid);

		result = pdfService.convertDocumentToInputStream(multipleDocumentDTO);
		assertNotNull(result);
	}

	@Test
	void testConvertXmlToInputStream() {
		when(xmlManipulatorMock.replaceAknNamespace(any(), any())).thenReturn(xslt);
		InputStream result = pdfService.convertXmlToInputStream(xml, false, "Titel", "Beschreibung");
		assertNotNull(result);
	}

	private String readFileSpringBootWay(String filePath) throws IOException {
		Resource resource = new ClassPathResource(filePath);
		assertNotNull(resource, "Die Ressource konnte nicht gefunden werden: " + filePath);

		String result = "";
		try (InputStream inputStream = resource.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {

			String line;
			while ((line = reader.readLine()) != null) {
				result = result + line;
			}
		}
		return result;
	}

	@Test
	void updateDrucksacheForCompoundDocumentErrorTest() {

		when(compoundDocumentRepository.findByCompoundDocumentId(any())).thenReturn(Optional.of(TestObjects.getRandomCompoundDocumentEntity()));
		when(documentRepository.findByCompoundDocumentId(any())).thenReturn(Optional.empty());

		List<ServiceState> serviceStateList = new ArrayList<>();

		pdfService.updateDrucksacheForCompoundDocumentId(UUID.randomUUID(), serviceStateList);

		assertThat(serviceStateList).isNotEmpty();
		assertThat(serviceStateList.get(0)).isEqualTo(ServiceState.DOCUMENT_NOT_FOUND);
	}

	@Test
	void updateDrucksacheForCompoundDocumentError2Test() {

		when(compoundDocumentRepository.findByCompoundDocumentId(any())).thenReturn(Optional.empty());
		when(documentRepository.findByCompoundDocumentId(any())).thenReturn(Optional.of(List.of(TestObjects.getDocumentEntity())));

		List<ServiceState> serviceStateList = new ArrayList<>();

		pdfService.updateDrucksacheForCompoundDocumentId(UUID.randomUUID(), serviceStateList);

		assertThat(serviceStateList).isNotEmpty();
		assertThat(serviceStateList.get(0)).isEqualTo(ServiceState.COMPOUND_DOCUMENT_NOT_FOUND);
	}

	@Test
	void updateDrucksacheForCompoundDocumentSuccessTest() throws IllegalDocumentType, IOException {
		Map<String, CommentPosition> comments = new HashMap<>();

		when(documentService.getDocumentById(any(UUID.class), anyBoolean())).thenReturn(documentDTO);
		when(eIdGeneratorRestPort.erzeugeEIdsFuerDokument(any())).thenReturn(new JSONObject());
		when(converterService.convertJsonToXml(any(JSONObject.class))).thenReturn(xml);
		when(xmlManipulatorMock.replaceAknNamespace(any(), any())).thenReturn(xslt);
		when(xmlManipulatorMock.replaceIncludePath(any(), any())).thenReturn(xslt);
		when(pdfCommentServiceMock.collectComments(any())).thenReturn(comments);
		when(xmlManipulatorMock.addComments(any(), any())).thenReturn(xml);
		when(xmlMergerMock.includeXslt(any())).thenReturn(xslt);
		when(xmlMergerMock.merge(any(), any())).thenReturn(xml);
		when(exportService.exportSingleDocument(any(String.class), any(ArrayList.class))).thenReturn(readFileSpringBootWay("/xml/vorblatt.xml"));

		when(compoundDocumentRepository.findByCompoundDocumentId(any())).thenReturn(Optional.of(TestObjects.getRandomCompoundDocumentEntity()));
		when(documentRepository.findByCompoundDocumentId(any())).thenReturn(Optional.of(List.of(TestObjects.getDocumentEntity())));
		when(drucksacheDokumentRepository.updateDokumentDrucksache(any(), any(), any())).thenReturn(ServiceState.OK);

		List<ServiceState> serviceStateList = new ArrayList<>();

		pdfService.updateDrucksacheForCompoundDocumentId(UUID.randomUUID(), serviceStateList);

		assertThat(serviceStateList).isNotEmpty();
		assertThat(serviceStateList.get(0)).isEqualTo(ServiceState.OK);
	}

	@Test
	void getDrucksacheByCompoundDocumentIdSuccessTest() {

		when(drucksacheDokumentRepository.getDrucksacheByCompoundDocumentId(any(), any())).thenReturn(Optional.of(new byte[0]));

		List<ServiceState> serviceStateList = new ArrayList<>();
		byte[] returnedByteArray = pdfService.getDrucksacheForCompoundDocumentId(UUID.randomUUID(), serviceStateList);

		assertThat(returnedByteArray).isNotNull();
		assertThat(serviceStateList.get(0)).isEqualTo(ServiceState.OK);
	}

	@Test
	void getDrucksacheByCompoundDocumentIdErrorTest() {

		when(drucksacheDokumentRepository.getDrucksacheByCompoundDocumentId(any(), any())).thenReturn(Optional.empty());

		List<ServiceState> serviceStateList = new ArrayList<>();
		byte[] returnedByteArray = pdfService.getDrucksacheForCompoundDocumentId(UUID.randomUUID(), serviceStateList);

		assertThat(returnedByteArray).isNotNull();
		assertThat(serviceStateList.get(0)).isEqualTo(ServiceState.UNKNOWN_ERROR);
	}

	@Test
	void whenCompoundDocumentForPKPIsRequested_ItShouldBeProvided() {

		when(compoundDocumentService.getDokumentenmappenFuerEinRegelungsvorhabenMitStatus(any(), any(), any()))
			.thenReturn(List.of(compoundDocument));

		when(documentMapper.mapToDto(any())).thenReturn(TestObjects.getRandomDocumentDTO());

		when(compoundDocument.getDokumenteDieserDokumentenmappe()).thenReturn(List.of(TestObjects.getRandomDocument()));
		when(compoundDocument.getCompoundDocumentId()).thenReturn(TestObjects.getRandomCompoundDocument().getCompoundDocumentId());

		List<ServiceState> status = new ArrayList<>();
		RegelungsVorhabenId regelungsVorhabenId = compoundDocument.getRegelungsVorhabenId();

		List<PkpZuleitungDokumentDTO> actual = pdfService.getCompoundDocumentsForPKP(
			UUID.randomUUID().toString(), regelungsVorhabenId, status);

		assertThat(actual).hasSize(1);
		assertThat(actual.get(0)).hasNoNullFieldsOrProperties();
		assertThat(status.get(0)).isEqualTo(ServiceState.OK);
	}


}