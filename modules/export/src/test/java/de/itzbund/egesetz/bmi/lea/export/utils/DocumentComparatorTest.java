// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.export.utils;

import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class DocumentComparatorTest {
    @Test
    public void testDocumentComparator() {
        DocumentDTO anschreiben = createDocument(DocumentType.ANSCHREIBEN_STAMMGESETZ);
        DocumentDTO begruendung = createDocument(DocumentType.BEGRUENDUNG_STAMMGESETZ);
        DocumentDTO rechtsetzungsdokument = createDocument(DocumentType.RECHTSETZUNGSDOKUMENT_STAMMGESETZ);
        DocumentDTO regelungstext = createDocument(DocumentType.REGELUNGSTEXT_STAMMGESETZ);
        DocumentDTO synopse = createDocument(DocumentType.SYNOPSE);
        DocumentDTO vorblatt = createDocument(DocumentType.VORBLATT_STAMMGESETZ);
        DocumentDTO offeneStruktur = createDocument(DocumentType.ANLAGE);

        List<DocumentDTO> documents = Arrays.asList(
            anschreiben, begruendung, rechtsetzungsdokument, regelungstext, synopse, vorblatt, offeneStruktur
        );

        documents.sort(new DocumentComparator());

        for (int i = 0; i < documents.size() - 1; i++) {
            Assertions.assertTrue(
                new DocumentComparator().compare(documents.get(i), documents.get(i + 1)) <= 0,
                "Documents are not sorted correctly"
            );
        }
    }

    private DocumentDTO createDocument(DocumentType type) {
        DocumentDTO document = new DocumentDTO();
        document.setType(type);
        return document;
    }
}