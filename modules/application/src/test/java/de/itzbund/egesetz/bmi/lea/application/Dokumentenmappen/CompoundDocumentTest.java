// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.Dokumentenmappen;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.itzbund.egesetz.bmi.lea.application.SchnittstellenTests;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CreateCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.services.PlategRequestService;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@Log4j2
class CompoundDocumentTest extends SchnittstellenTests {

    private static final String ENDPOINT_URL = "/compounddocuments/";
    public static final String TITLE_FOR_COMPOUND_DOCUMENT = "Title for compound document";

    @Autowired
    private ObjectMapper objectMapper;

    // Die Requests an Plattform werden gemockt, falls (!) nötig
    @MockBean
    private PlategRequestService plategRequestService;

    @Override
    public void init2() {
        // In diesem Fall müsen wir kein Plateq-Request mocken, weil die RVs im Skript: "data-Editor-RVin_den_Editor.sql"
        // in die Datenbank geladen werden und somit der Plateq-Aufruf nicht stattfindet.
    }

    @Test
    @SneakyThrows
    void erstelleNeueDokumentenmappe() {

        // Das DTO für den Payload (alternativ gleich ein JSON-String)
        CreateCompoundDocumentDTO payload = CreateCompoundDocumentDTO.builder()
            .title(TITLE_FOR_COMPOUND_DOCUMENT)
            .type(CompoundDocumentTypeVariant.STAMMGESETZ)
            // Ein Regelungsvorhaben für das der User auch Rechte haben muss ...
            // ... und der gemockte Plateq-Service Daten (od. in der Datenbank vorkommt) liefern muss.
            .regelungsVorhabenId(REGELUNGSVORHABEN_ID.toString())
            .titleRegulatoryText("Title for a regulatory text")
            .initialNumberOfLevels(1)
            .build();

        String payloadString = objectMapper.writeValueAsString(payload);
        log.debug("Payload: {}", payloadString);

        // Der ganze Aufruf (inkl. URL aus Konstante) und HTTP-Methoden (!), hier POST ...
        // .. der Header ist sonst relativ uninteressant
        Response response = getResponse(payloadString);

        // Auswertung des Response
        validatableResponse = response
            .then()
            .log().all()
            .assertThat().statusCode(201);

        // Auswertung der Responsedaten
        response.then().body("title", equalTo(getExpectedBodyRepsonse().getTitle()));
    }

    private Response getResponse(String payloadString) {
        return given()
            .contentType(ContentType.JSON)
            .body(payloadString)
            .header("Authorization", "Bearer " + jwToken)
            .header("X-XSRF-TOKEN-EDITOR", "xxxxx")
            .header("Cookie", "XSRF-TOKEN=xxxxx; XSRF-TOKEN-EDITOR=xxxxx")
            .when()
            .post(ENDPOINT_URL);
    }

    // Ein Teil von dem erwarteten DTO der Antwort
    private CompoundDocumentDTO getExpectedBodyRepsonse() {
        return CompoundDocumentDTO.builder()
            .title(TITLE_FOR_COMPOUND_DOCUMENT)
            .build();
    }

}
