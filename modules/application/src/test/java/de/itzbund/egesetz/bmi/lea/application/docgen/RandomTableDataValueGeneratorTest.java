// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.docgen;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Log4j2
class RandomTableDataValueGeneratorTest {


    @Test
    void test_getNext_1() {
        RandomTableDataValueGenerator generator = new RandomTableDataValueGenerator(2, RoundingMode.HALF_DOWN);
        Set<BigDecimal> values = new HashSet<>();
        int count = 13;

        for (int i = 0; i < count; i++) {
            values.add(generator.getNext());
        }

        log.debug(values);
        assertEquals(count, values.size());
    }


    @Test
    void test_getNext_n() {
        RandomTableDataValueGenerator generator = new RandomTableDataValueGenerator(0, RoundingMode.HALF_DOWN);
        List<BigDecimal> values = generator.getNext(3);
        assertEquals(3, values.size());
        log.debug(values);

        values = generator.getNext(30);
        assertEquals(30, values.size());
        log.debug(values);
    }


}
