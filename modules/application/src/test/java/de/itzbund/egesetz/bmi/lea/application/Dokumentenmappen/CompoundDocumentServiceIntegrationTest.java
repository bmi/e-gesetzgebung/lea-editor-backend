// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.Dokumentenmappen;

import de.itzbund.egesetz.bmi.auth.entities.ZuordnungEntity;
import de.itzbund.egesetz.bmi.auth.repositories.ZuordnungenRepository;
import de.itzbund.egesetz.bmi.lea.core.Constants;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CreateCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserRightsDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.Freigabetyp;
import de.itzbund.egesetz.bmi.lea.domain.enums.RegelungsvorhabenTypType;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.RegelungsVorhabenId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.services.CompoundDocumentService;
import de.itzbund.egesetz.bmi.lea.domain.services.RegelungsvorhabenService;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
class CompoundDocumentServiceIntegrationTest {

    private final String rvId = "00000000-0000-0000-0000-000000000005";
    private final User user = buildRandomUser();
    public static final String GID = "someRandomId";
    public static final String NAME = "TestVorname Testnachmane";
    public static final String EMAIL = "Test.Nonexisting1@email.de";


    private MockedStatic<LeageSession> mockedLeageSession;

    @MockBean
    private LeageSession session;
    @MockBean
    private RegelungsvorhabenService regelungsvorhabenService;
    @MockBean
    private UserPersistencePort userRepository;

    @Autowired
    private CompoundDocumentService service;

    @Autowired
    private ZuordnungenRepository zuordnungenRepository;

    @BeforeEach
    void mockAuthentication() {
        when(session.getUser()).thenReturn(user);
    }

    @BeforeEach
    void mockRegelungsvorhabenService() {
        mockedLeageSession = mockStatic(LeageSession.class);
        Map<String, String> mockUserDetails = new HashMap<>();
        mockUserDetails.put(LeageSession.GID, GID);
        mockUserDetails.put(LeageSession.NAME, NAME);
        mockUserDetails.put(LeageSession.EMAIL, EMAIL);
        when(LeageSession.getUserDatailsFromAuthentication()).thenReturn(mockUserDetails);

        when(regelungsvorhabenService.getRegelungsvorhabenEditorTriggeredByDocument(new RegelungsVorhabenId(UUID.fromString(rvId))))
            .thenReturn(RegelungsvorhabenEditorDTO.builder()
                .id(UUID.fromString(rvId))
                .status(VorhabenStatusType.IN_BEARBEITUNG)
                .vorhabenart(RegelungsvorhabenTypType.GESETZ)
                .bearbeitetAm(Instant.now())
                .build());
    }

    @BeforeEach
    void mockUserRepository() {
        mockUserRepositoryWithUser(userRepository, user);
    }

    @AfterEach
    public void tearDown() {
        mockedLeageSession.close();  // Make sure to close the mock to avoid conflict
    }

    @Test
    @DisplayName("Ersteller gibt Schreibrechte an weiteren Nutzer weiter")
    void testChangeWritePermissionsOnCompoundDocuments() {
        final List<ServiceState> status = new ArrayList<>();

        // given
        final CreateCompoundDocumentDTO createCompoundDocumentDTO = buildCreateCompoundDocumentDTO(rvId, CompoundDocumentTypeVariant.STAMMGESETZ);
        final CompoundDocumentDTO compoundDocumentDTO = service.createCompoundDocument(createCompoundDocumentDTO, status);

        assertThat(zuordnungenRepository.findByRessourceIdOrderById(compoundDocumentDTO.getId().toString()))
            .hasSize(1).satisfiesExactlyInAnyOrder(
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user, false, Constants.ROLLE_ERSTELLER)
            );

        // when
        final User user1 = mockUserRepositoryWithUser(userRepository, buildRandomUser());
        final UserRightsDTO userRights1 = buildUserRightsDTO(user1, Freigabetyp.SCHREIBRECHTE);
        service.changeWritePermissionsOnCompoundDocuments(getCompoundDocumentId(compoundDocumentDTO), userRights1, status);

        // then
        assertThat(status).containsOnly(ServiceState.OK);

        assertThat(zuordnungenRepository.findByRessourceIdOrderById(compoundDocumentDTO.getId().toString()))
            .hasSize(3).satisfiesExactlyInAnyOrder(
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user, true, Constants.ROLLE_ERSTELLER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user, false, Constants.ROLLE_LESER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user1, false, Constants.ROLLE_SCHREIBER)
            );
    }

    @Test
    @DisplayName("Schreiber gibt Schreibrechte an weiteren Nutzer weiter")
    void testChangeWritePermissionsOnCompoundDocuments1() {
        final List<ServiceState> status = new ArrayList<>();

        // given
        final CreateCompoundDocumentDTO createCompoundDocumentDTO = buildCreateCompoundDocumentDTO(rvId, CompoundDocumentTypeVariant.STAMMGESETZ);
        final CompoundDocumentDTO compoundDocumentDTO = service.createCompoundDocument(createCompoundDocumentDTO, status);

        final User user1 = mockUserRepositoryWithUser(userRepository, buildRandomUser());
        final UserRightsDTO userRights1 = buildUserRightsDTO(user1, Freigabetyp.SCHREIBRECHTE);
        service.changeWritePermissionsOnCompoundDocuments(getCompoundDocumentId(compoundDocumentDTO), userRights1, status);

        // # user ist ERSTELLER
        // # user1 ist SCHREIBER
        assertThat(zuordnungenRepository.findByRessourceIdOrderById(compoundDocumentDTO.getId().toString()))
            .hasSize(3).satisfiesExactlyInAnyOrder(
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user, true, Constants.ROLLE_ERSTELLER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user, false, Constants.ROLLE_LESER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user1, false, Constants.ROLLE_SCHREIBER)
            );

        // when
        // # gebe SCHREIBRECHTE an user2 weiter
        final User user2 = mockUserRepositoryWithUser(userRepository, buildRandomUser());
        final UserRightsDTO userRights2 = buildUserRightsDTO(user2, Freigabetyp.SCHREIBRECHTE);
        service.changeWritePermissionsOnCompoundDocuments(getCompoundDocumentId(compoundDocumentDTO), userRights2, status);

        // then
        assertThat(status).containsOnly(ServiceState.OK);

        // # user ist ERSTELLER
        // # user1 ist LESER und hat deaktivierte SCHREIBER Rechte
        // # user2 ist neuer SCHREIBER
        assertThat(zuordnungenRepository.findByRessourceIdOrderById(compoundDocumentDTO.getId().toString()))
            .hasSize(5).satisfiesExactlyInAnyOrder(
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user, true, Constants.ROLLE_ERSTELLER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user, false, Constants.ROLLE_LESER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user1, true, Constants.ROLLE_SCHREIBER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user1, false, Constants.ROLLE_LESER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user2, false, Constants.ROLLE_SCHREIBER)
            );
    }

    @Test
    @DisplayName("Schreiber gibt Schreibrechte an früheren Schreiber zurück")
    void testChangeWritePermissionsOnCompoundDocuments2() {
        final List<ServiceState> status = new ArrayList<>();

        // given
        final CreateCompoundDocumentDTO createCompoundDocumentDTO = buildCreateCompoundDocumentDTO(rvId, CompoundDocumentTypeVariant.STAMMGESETZ);
        final CompoundDocumentDTO compoundDocumentDTO = service.createCompoundDocument(createCompoundDocumentDTO, status);

        // # gib SCHREIBRECHTE an user1 weiter
        final User user1 = mockUserRepositoryWithUser(userRepository, buildRandomUser());
        final UserRightsDTO userRights1 = buildUserRightsDTO(user1, Freigabetyp.SCHREIBRECHTE);
        service.changeWritePermissionsOnCompoundDocuments(getCompoundDocumentId(compoundDocumentDTO), userRights1, status);
        // # gib SCHREIBRECHTE an user2 weiter
        final User user2 = mockUserRepositoryWithUser(userRepository, buildRandomUser());
        final UserRightsDTO userRights2 = buildUserRightsDTO(user2, Freigabetyp.SCHREIBRECHTE);
        service.changeWritePermissionsOnCompoundDocuments(getCompoundDocumentId(compoundDocumentDTO), userRights2, status);

        // # user ist ERSTELLER
        // # user1 ist LESER
        // # user2 ist SCHREIBER
        assertThat(zuordnungenRepository.findByRessourceIdOrderById(compoundDocumentDTO.getId().toString()))
            .hasSize(5).satisfiesExactlyInAnyOrder(
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user, true, Constants.ROLLE_ERSTELLER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user, false, Constants.ROLLE_LESER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user1, true, Constants.ROLLE_SCHREIBER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user1, false, Constants.ROLLE_LESER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user2, false, Constants.ROLLE_SCHREIBER)
            );

        // when
        // # gebe SCHREIBRECHTE an user1 zurück
        service.changeWritePermissionsOnCompoundDocuments(getCompoundDocumentId(compoundDocumentDTO), userRights1, status);

        // then
        assertThat(status).containsOnly(ServiceState.OK);

        // # user ist ERSTELLER
        // # user1 ist wieder SCHREIBER
        // # user2 bleibt LESER
        final List<ZuordnungEntity> zuordnungen = zuordnungenRepository.findByRessourceIdOrderById(compoundDocumentDTO.getId().toString());
        assertThat(zuordnungen)
            .hasSize(4).satisfiesExactlyInAnyOrder(
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user, true, Constants.ROLLE_ERSTELLER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user, false, Constants.ROLLE_LESER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user1, false, Constants.ROLLE_SCHREIBER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user2, false, Constants.ROLLE_LESER)
            );
    }

    @Test
    @DisplayName("Schreiber gibt Schreibrechte an Ersteller zurück (und überspringt dabei einen früheren Schreiber)")
    void testChangeWritePermissionsOnCompoundDocuments3() {
        final List<ServiceState> status = new ArrayList<>();

        // given
        final CreateCompoundDocumentDTO createCompoundDocumentDTO = buildCreateCompoundDocumentDTO(rvId, CompoundDocumentTypeVariant.STAMMGESETZ);
        final CompoundDocumentDTO compoundDocumentDTO = service.createCompoundDocument(createCompoundDocumentDTO, status);

        // # gib SCHREIBRECHTE an user1 weiter
        final User user1 = mockUserRepositoryWithUser(userRepository, buildRandomUser());
        final UserRightsDTO userRights1 = buildUserRightsDTO(user1, Freigabetyp.SCHREIBRECHTE);
        service.changeWritePermissionsOnCompoundDocuments(getCompoundDocumentId(compoundDocumentDTO), userRights1, status);
        // # gib SCHREIBRECHTE an user2 weiter
        final User user2 = mockUserRepositoryWithUser(userRepository, buildRandomUser());
        final UserRightsDTO userRights2 = buildUserRightsDTO(user2, Freigabetyp.SCHREIBRECHTE);
        service.changeWritePermissionsOnCompoundDocuments(getCompoundDocumentId(compoundDocumentDTO), userRights2, status);

        // # user ist ERSTELLER
        // # user1 ist LESER
        // # user2 ist SCHREIBER
        assertThat(zuordnungenRepository.findByRessourceIdOrderById(compoundDocumentDTO.getId().toString()))
            .hasSize(5).satisfiesExactlyInAnyOrder(
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user, true, Constants.ROLLE_ERSTELLER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user, false, Constants.ROLLE_LESER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user1, true, Constants.ROLLE_SCHREIBER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user1, false, Constants.ROLLE_LESER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user2, false, Constants.ROLLE_SCHREIBER)
            );

        // when
        // # gebe SCHREIBRECHTE an user zurück
        final UserRightsDTO userRights = buildUserRightsDTO(user, Freigabetyp.SCHREIBRECHTE);
        service.changeWritePermissionsOnCompoundDocuments(getCompoundDocumentId(compoundDocumentDTO), userRights, status);

        // then
        assertThat(status).containsOnly(ServiceState.OK);

        // # user ist ERSTELLER
        // # user1 ist wieder SCHREIBER
        // # user2 bleibt LESER
        final List<ZuordnungEntity> zuordnungen = zuordnungenRepository.findByRessourceIdOrderById(compoundDocumentDTO.getId().toString());
        assertThat(zuordnungen)
            .hasSize(3).satisfiesExactlyInAnyOrder(
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user, false, Constants.ROLLE_ERSTELLER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user1, false, Constants.ROLLE_LESER),
                zuordnungEntity -> assertZuordnung(zuordnungEntity, user2, false, Constants.ROLLE_LESER)
            );
    }

    private static User mockUserRepositoryWithUser(final UserPersistencePort userRepository, final User user) {
        when(userRepository.findFirstByGid(user.getGid())).thenReturn(user);
        return user;
    }

    private void assertZuordnung(final ZuordnungEntity actual, final User user, final boolean deaktiviert, final String rolleBezeichnung) {
        assertThat(actual.getBenutzerId()).isEqualTo(user.getGid().getId());
        assertThat(actual.isDeaktiviert()).isEqualTo(deaktiviert);
        assertThat(actual.getRolle().getBezeichnung()).isEqualTo(rolleBezeichnung);
    }

    static CreateCompoundDocumentDTO buildCreateCompoundDocumentDTO(final String regelungsVorhabenId,
        final CompoundDocumentTypeVariant compoundDocumentTypeVariant) {
        return CreateCompoundDocumentDTO.builder()
            .regelungsVorhabenId(regelungsVorhabenId)
            .title(Utils.getRandomString())
            .type(compoundDocumentTypeVariant)
            .titleRegulatoryText(Utils.getRandomString())
            .initialNumberOfLevels(0)
            .build();
    }

    static User buildRandomUser() {
        return User.builder()
            .gid(new UserId(Utils.getRandomString()))
            .name("User " + RandomStringUtils.randomNumeric(3))
            .email("Mail " + RandomStringUtils.randomNumeric(3))
            .plattformUserId(UUID.randomUUID())
            .build();
    }

    static UserRightsDTO buildUserRightsDTO(final User user, final Freigabetyp freigabetyp) {
        return UserRightsDTO.builder()
            .gid(user.getGid().getId())
            .name(user.getName())
            .email(user.getEmail())
            .freigabetyp(freigabetyp)
            .build();
    }

    private static CompoundDocumentId getCompoundDocumentId(final CompoundDocumentDTO compoundDocumentDTO) {
        return new CompoundDocumentId(compoundDocumentDTO.getId());
    }
}
