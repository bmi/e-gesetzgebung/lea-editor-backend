// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RessortEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.synopsis.SynopsisRequestDTO;
import de.itzbund.egesetz.bmi.lea.domain.services.RegelungsvorhabenService;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.when;

@Log4j2
class AenderungsbefehlBerechnungIntegrationTest extends SchnittstellenTests {

    private static final String REGELUNGSVORHABEN_ID = "00000000-0000-0000-0000-000000000123";
    private static final String RT_BESTANDSRECHT_ID = "00000000-0001-0001-0000-000000000123";
    private static final String RT_NEUFASSUNG_ID = "00000010-0001-0001-0000-000000000123";
    private static final String ENDPOINT_URL = "/regulatoryProposal/" + REGELUNGSVORHABEN_ID + "/aenderungsbefehle";

    @Autowired
    private ObjectMapper objectMapper;

    // Die Requests an Plattform werden gemockt
    @MockBean
    private RegelungsvorhabenService regelungsvorhabenService;

    @Override
    public void init2() {
        when(regelungsvorhabenService.getAllRegulatoryProposalsForUserHeIsAuthorOf())
            .thenReturn(List.of(
                RegelungsvorhabenEditorDTO.builder()
                    .id(UUID.fromString(REGELUNGSVORHABEN_ID))
                    .abkuerzung(REGELUNGSVORHABEN_ABKUERZUNG)
                    .kurzbezeichnung(REGELUNGSVORHABEN_KURZBEZEICHNUNG)
                    .kurzbeschreibung(REGELUNGSVORHABEN_KURZBESCHREIBUNG)
                    .status(REGELUNGSVORHABEN_STATUS)
                    .vorhabenart(REGELUNGSVORHABEN_ART)
                    .bearbeitetAm(Instant.now())
                    .bezeichnung(REGELUNGSVORHABEN_BEZEICHNUNG)
                    .technischesFfRessort(RessortEditorDTO.builder()
                        .id(UUID.randomUUID().toString())
                        .kurzbezeichnung(Utils.getRandomString())
                        .bezeichnungNominativ(Utils.getRandomString())
                        .bezeichnungGenitiv(Utils.getRandomString())
                        .build())
                    .allFfRessorts(new ArrayList<>())
                    .initiant(REGELUNGSVORHABEN_INITIANT)
                    .build()
            ));
    }

    @Test
    @SneakyThrows
    void test_wennBestandsrechtUndNeufassungVerglichenWerden_dannErzeugeAenderungsbefehle() {
        SynopsisRequestDTO payload = new SynopsisRequestDTO()
            .base(UUID.fromString(RT_BESTANDSRECHT_ID))
            .addVersionsItem(UUID.fromString(RT_NEUFASSUNG_ID));

        String payloadString = objectMapper.writeValueAsString(payload);
        log.debug("Payload: {}", payloadString);

        Response response = given()
            .contentType(ContentType.JSON)
            .body(payloadString)
            .header("Authorization", "Bearer " + jwToken)
            .header("X-XSRF-TOKEN-EDITOR", "xxxxx")
            .header("Cookie", "XSRF-TOKEN=xxxxx; XSRF-TOKEN-EDITOR=xxxxx")
            .when()
            .post(ENDPOINT_URL);

        validatableResponse = response
            .then()
            .log().all()
            .assertThat().statusCode(200)
            .body("content", notNullValue())
            .body("state", equalTo("FINAL"))
            .body("version", equalTo("1.0"))
            .body("type", equalTo("REGELUNGSTEXT_MANTELGESETZ"))
            .body("documentPermissions.hasRead", equalTo(true))
            .body("documentPermissions.hasWrite", equalTo(false));
    }

}
