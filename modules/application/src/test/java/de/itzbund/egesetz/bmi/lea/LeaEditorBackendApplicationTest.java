// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea;

import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;
import org.springframework.boot.SpringApplication;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mockConstruction;
import static org.mockito.Mockito.verify;

@WebAppConfiguration
class LeaEditorBackendApplicationTest {

    @Test
    void testMain() {
        try (MockedConstruction<SpringApplication> mock = mockConstruction(SpringApplication.class)) {
            String[] args = new String[0];
            assertDoesNotThrow(() -> LeaEditorBackendApplication.main(args));
            assertEquals(1, mock.constructed()
                .size());
            SpringApplication springApplication = mock.constructed()
                .get(0);
            verify(springApplication).run(args);
        }
    }
}
