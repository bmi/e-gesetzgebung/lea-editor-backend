// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.docgen;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Disabled("Not a real test, just a tool")
public class RandomTableDataValueGenerator implements RandomValueGenerator<BigDecimal> {

    private static final SecureRandom random = new SecureRandom();

    private final int scale;

    private final RoundingMode roundingMode;


    public RandomTableDataValueGenerator(int scale, RoundingMode roundingMode) {
        this.scale = scale;
        this.roundingMode = roundingMode;
    }

    @Override
    public BigDecimal getNext() {
        int next = random.nextInt();
        double candidate = (double) next / 13;
        return BigDecimal.valueOf(candidate).setScale(scale, roundingMode);
    }

    @Override
    public List<BigDecimal> getNext(int count) {
        List<BigDecimal> resultList = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            resultList.add(getNext());
        }
        return resultList;
    }

}
