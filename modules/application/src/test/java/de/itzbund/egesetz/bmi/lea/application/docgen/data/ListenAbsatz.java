// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.docgen.data;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Singular;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
public class ListenAbsatz extends Absatz {

    @Singular("absatzUntergliederung")
    private List<AbsatzUntergliederung> absatzUntergliederungen;

}
