// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application;

import de.itzbund.egesetz.bmi.lea.application.configuration.security.JwtUtils;
import de.itzbund.egesetz.bmi.lea.application.configuration.security.JwtUtilsTest;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RessortEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.RegelungsvorhabenTypType;
import de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentInitiant;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.transaction.TransactionManager;

import javax.sql.DataSource;
import java.security.KeyPair;
import java.security.interfaces.RSAPublicKey;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

import static org.mockito.Mockito.when;

// @formatter:off
/**
 * Das Originalverhalten für H2 und Test-Umgebung wollen wir nicht; also, dass
 * schema.sql und data.sql automatisch ausgeführt werden.
 *
 * Außerdem müssen die beiden Datenbanken gefüllt werden. Die Selektion erfolgt
 * durch die Transaktionsmanager. Weil nur eine Angabe pro Klasse möglich ist,
 * müssen wir die Klasse 'PrepareAuthDataSource' zur Hilfe nehmen.
 */
// @formatter:on
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@SqlConfig(transactionManager = "transactionManager")
@SqlGroup({
	@Sql("/testDB/Editor/data-Editor-clearData.sql"),
	@Sql("/testDB/Editor/data-Editor-RV_Proposition.sql"),
	@Sql("/testDB/Editor/data-Editor-DMuDok4Tab1.sql"),
	@Sql("/testDB/Editor/data-Editor-DMuDok4Drucksache.sql"),
	@Sql("/testDB/Editor/data-Editor-BestR_nF.sql"),
	@Sql("/testDB/Editor/data-Editor-RVin_den_Editor.sql"),
	@Sql("/testDB/Editor/data-Editor-BestR_Kopie.sql"),
	@Sql("/testDB/Editor/data-Editor-DMuDok4Bundesrat.sql"),
	@Sql("/testDB/Editor/data-Editor-BusinessLogging_DM_RT.sql"),
	@Sql("/testDB/Editor/data-Editor-BestRecht3.sql"),
	@Sql("/testDB/Editor/data-Editor-BestRecht4.sql")
})
public abstract class SchnittstellenTests {

	private static final String BASE_URI = "http://localhost";

	// ** Benutzer **
	public static final String USER_1 = "user1_id";
	public static final String USER_2 = "user2_id";
	public static final String USER_3 = "user3_id";

	// ** Regelungsvorhaben **
	public static final UUID REGELUNGSVORHABEN_ID = UUID.fromString("00000000-0000-0000-0000-000000000005");
	public static final UUID REGELUNGSVORHABEN_ID2 = UUID.fromString("00000000-0000-0000-0000-000000000004");
	public static final UUID REGELUNGSVORHABEN_ID3 = UUID.fromString("00000000-0000-0000-0000-00000000a123");
	public static final UUID REGELUNGSVORHABEN_ID4 = UUID.fromString("00000000-0000-0000-0000-000000000123");
	public static final String REGELUNGSVORHABEN_ABKUERZUNG = "Abk.";
	public static final String REGELUNGSVORHABEN_BEZEICHNUNG = "Bezeichnung";
	public static final String REGELUNGSVORHABEN_KURZBESCHREIBUNG = "Kurzbeschreibung";
	public static final String REGELUNGSVORHABEN_KURZBEZEICHNUNG = "Titel, aka: Kurz Bezeichnung";
	public static final RegelungsvorhabenTypType REGELUNGSVORHABEN_ART = RegelungsvorhabenTypType.GESETZ;
	public static final VorhabenStatusType REGELUNGSVORHABEN_STATUS = VorhabenStatusType.IN_BEARBEITUNG;
	public static final RechtsetzungsdokumentInitiant REGELUNGSVORHABEN_INITIANT = RechtsetzungsdokumentInitiant.BUNDESREGIERUNG;


	// ** Bestandsrecht **
	// data-Editor-BestR_nF.sql
	public static final String BESTANDSRECHT_ID = "00000000-0001-0001-0000-000000000123";
	// data-Editor-BestRecht3.sql
	public static final String BESTANDSRECHT_ID2 = "4b003350-ac94-4845-bbe7-311df08ec96c";
	public static final String BESTANDSRECHT_ID3 = "dac4d7c8-ce22-48f6-a6c0-f3f8dea73e9d";
	public static final String BESTANDSRECHT_ID4 = "4b003350-0000-0000-0000-311df08ec96c";


	// ** Dokumentenmappen **
	// data-Editor-BestR_Kopie.sql
	public static final String COMPOUND_DOCUMENT_ID3 = "00000010-0000-0000-0000-00000000a123";
	// data-Editor-BestRecht3.sql
	public static final String COMPOUND_DOCUMENT_ID2 = "a0000001-0000-0000-0004-000000000001";
	public static final String COMPOUND_DOCUMENT_ID1 = "92b77fdd-5f73-404c-8c22-a5168757affa";
	// data-Editor-DMuDok4Tab1.sql
	public static final String COMPOUND_DOCUMENT_ID8 = "00000001-0000-0000-0000-000000000001";
	// ....
	public static final String COMPOUND_DOCUMENT_ID4 = "00000010-0000-0000-0000-000000000123";
	public static final String COMPOUND_DOCUMENT_ID5 = "a0000001-0000-0000-0000-000000000001";
	public static final String COMPOUND_DOCUMENT_ID6 = "00000001-0000-0000-000b-000000000002";
	public static final String COMPOUND_DOCUMENT_ID7 = "00000001-0000-0000-000b-000000000001";
	public static final String COMPOUND_DOCUMENT_ID9 = "00000001-0000-0000-000a-000000000001";

	@LocalServerPort
	private int port;

	// + -------------------------------
	// v  Teilweise nur zu debug Zwecken
	@Autowired
	DataSource springDataSource;

	@Autowired
	@Qualifier("authDataSource")
	DataSource authDataSource;

	@Autowired
	@Qualifier("transactionManager")
	TransactionManager entityManagerFactory;

	@Autowired
	@Qualifier("authTransactionManager")
	TransactionManager authEntityManagerFactory;
	// ^
	// + -------------------------------

	// Für die Tokenvalidierung muss der RSA-Private-Key eingefügt werden
	@SpyBean
	private JwtUtils jwtUtils;

	protected ValidatableResponse validatableResponse;
	protected String jwToken;

	@BeforeEach
	public void init() throws Exception {
		RestAssured.baseURI = BASE_URI;
		RestAssured.port = port;

		// Initialisierung der zweiten Datenbank - Autowired nimmt die falsche DB
		PrepareAuthDataSource prepareAuthDataSource = new PrepareAuthDataSource(authDataSource);
		prepareAuthDataSource.init();

		KeyPair keyPair = JwtUtilsTest.generateRSAKeyPair();
		jwToken = JwtUtils.generateRS256JwtToken(tokenClaims(), keyPair.getPrivate());

		// Da wir den echten RSA-Private-Key nicht kennen, nehmen wir in der Klasse unseren privaten Schlüssel, der zu unserem Token gehört
		when(jwtUtils.getPublicKeyFromKeystore()).thenReturn((RSAPublicKey) keyPair.getPublic());

		init2();
	}

	public abstract void init2();

	protected static RegelungsvorhabenEditorDTO getRandomRegelungsvorhabenEditorDTO() {
		return RegelungsvorhabenEditorDTO.builder()
			.id(REGELUNGSVORHABEN_ID)
			.abkuerzung(REGELUNGSVORHABEN_ABKUERZUNG)
			.kurzbezeichnung(REGELUNGSVORHABEN_KURZBEZEICHNUNG)
			.kurzbeschreibung(REGELUNGSVORHABEN_KURZBESCHREIBUNG)
			.status(REGELUNGSVORHABEN_STATUS)
			.vorhabenart(REGELUNGSVORHABEN_ART)
			.bearbeitetAm(Instant.now())

			.bezeichnung(REGELUNGSVORHABEN_BEZEICHNUNG)
			.technischesFfRessort(getRandomRessortEditorDTO())
			.allFfRessorts(new ArrayList<>())
			.initiant(REGELUNGSVORHABEN_INITIANT)
			.build();
	}

	protected static RessortEditorDTO getRandomRessortEditorDTO() {
		return RessortEditorDTO.builder()
			.id(UUID.randomUUID().toString())
			.kurzbezeichnung(Utils.getRandomString())
			.bezeichnungNominativ(Utils.getRandomString())
			.bezeichnungGenitiv(Utils.getRandomString())
			.build();
	}

	protected Map<String, Object> tokenClaims() {

		return Map.of(

			"gid", "user1_id",
			"name", "Vorname1 Nachname1",
			"email", "Vorname1.Nachname1@example.com"

		);
	}

}
