// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static io.restassured.RestAssured.given;

public class RegelungsvorhabenUebermittlungTest extends SchnittstellenTests {

	@Override
	public void init2() {
		// Nichts neues
	}

	@Test
	void erstelleEinRegelungsvorhaben() {
		Response response = getResponse(getPayloadWithRVId(UUID.randomUUID().toString()));

		validatableResponse = response
			.then()
			.log().all()
			.assertThat().statusCode(201);
	}

	@Test
	void updateEinRegelungsvorhabenGueltig() {
		Response response = getResponse(getPayload(false));

		validatableResponse = response
			.then()
			.log().all()
			.assertThat().statusCode(200);
	}

	@Test
	void versucheEinRegelungsvorhabenUpzudatenBeiDemDieDokumentenmappeNichtInDraftIst() {
		Response response = getResponse(getPayload(true));

		validatableResponse = response
			.then()
			.log().all()
			.assertThat().statusCode(409);
	}

	private Response getResponse(String payload) {
		Response response = given()
			.accept(ContentType.ANY)
			.header("x-api-key", "insertKey")
			.contentType(ContentType.JSON)
			.body(payload)
			.log().all()  // Loggt die gesamte Anfrage
			.when()
			.put("/plattform/user/" + USER_1 + "/regelungsvorhaben/create_or_update");
		return response;
	}

	@SneakyThrows
	private String getPayload(boolean isNotDraft) {
		String rvId = "00000000-0000-0000-0000-000000000005";
		if (isNotDraft) {
			rvId = "00000000-0000-0000-0000-000000000020";
		}

		return getPayloadWithRVId(rvId);
	}

	@SneakyThrows
	private String getPayloadWithRVId(String rvId) {

		JSONObject federFuehresRessort = new JSONObject();
		federFuehresRessort.put("id", "00000000-0000-0000-9999-000000000005");
		federFuehresRessort.put("kurzbezeichnung", "BMI");
		federFuehresRessort.put("bezeichnungNominativ", "das");
		federFuehresRessort.put("bezeichnungGenitiv", "des");
		federFuehresRessort.put("aktiv", true);

		JSONArray alleRessorts = new JSONArray();
		alleRessorts.put("BMJ");
		alleRessorts.put("BMI");

		JSONObject payload = new JSONObject();
		payload.put("id", rvId);
		payload.put("abkuerzung", "RR Verwaltungsvorschrift: Abkürzung");
		payload.put("kurzbezeichnung", "RR Verwaltungsvorschrift: Titel");
		payload.put("kurzbeschreibung", "RR Kurzbeschreibung");
		payload.put("status", "IN_BEARBEITUNG");
		payload.put("vorhabenart", "GESETZ");
		payload.put("bearbeitetAm", "2024-05-21T16:07:28.771880Z");
		payload.put("farbe", "BLAU");
		payload.put("bezeichnung", "RV: Bezeichnung");
		payload.put("technischesFfRessort", federFuehresRessort);
		payload.put("allFfRessorts", alleRessorts);
		payload.put("initiant", "BUNDESREGIERUNG");

		JSONArray regelungsvorhaben = new JSONArray();
		regelungsvorhaben.put(payload);

		return regelungsvorhaben.toString();
	}

}
