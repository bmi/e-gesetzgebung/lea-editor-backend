// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application;

import java.util.List;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.itzbund.egesetz.bmi.lea.domain.dtos.user.UserRightsDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.Freigabetyp;
import de.itzbund.egesetz.bmi.lea.domain.services.logging.DomainLoggingService;
import de.itzbund.egesetz.bmi.lea.domain.services.logging.LogActivity;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class BusinessLoggingTest extends SchnittstellenTests {

	private static final String CHANGE_WRITE_PERMISSIONS_ENDPOINT = "/compounddocuments/{id}/changewritepermissions";
	private static final String ALLOWED_USERS_ENDPOINT = "/compounddocuments/{id}/allowedusers";

	public static final String COMPOUND_DOCUMENT_ID = "a0000001-0000-0000-0000-000000000001";

	@Autowired
	private ObjectMapper objectMapper;

	@SpyBean
	private DomainLoggingService domainLoggingService;

	@Override
	public void init2() {
		// no init necessary
	}

	@Test
	void test_wennSchreibrechteWeitergabe_dannBusinessLoggingEnthaeltEintrag() throws JsonProcessingException {
		// given
		final UserRightsDTO someName2UserRightsDTO = buildUserRightsDTO(USER_2, Freigabetyp.SCHREIBRECHTE);
		final UserRightsDTO someName3UserRightsDTO = buildUserRightsDTO(USER_3, Freigabetyp.SCHREIBRECHTE);

		// when
		postChangeWritePermissions(someName2UserRightsDTO);
		postChangeWritePermissions(someName3UserRightsDTO);

		// then
		/*
		Schreibrechte user1->someName2
		1x GRANT_WRITE_ACCESS für someName2
		1x REVOKE_WRITE_ACCESS für user1
		1x GRANT_READ_ACCESS für user1

		Schreibrechte someName2->someName3
		1x GRANT_WRITE_ACCESS für someName3
		1x REVOKE_WRITE_ACCESS für someName2
		1x GRANT_READ_ACCESS für someName2
		 */
		verify(domainLoggingService, times(2)).logActivity(LogActivity.GRANT_WRITE_ACCESS, "user1_id");
		verify(domainLoggingService, times(2)).logActivity(LogActivity.REVOKE_WRITE_ACCESS, "user1_id");
		verify(domainLoggingService, times(2)).logActivity(LogActivity.GRANT_READ_ACCESS, "user1_id");
	}

	@Test
	void test_wennLeserechteWeitergabe_dannBusinessLoggingEnthaeltEintrag() throws JsonProcessingException {
		// given
		final UserRightsDTO someName2UserRightsDTO = buildUserRightsDTO(USER_2, Freigabetyp.LESERECHTE);
		final UserRightsDTO someName3UserRightsDTO = buildUserRightsDTO(USER_3, Freigabetyp.LESERECHTE);

		// when
		postAllowedUsers(someName2UserRightsDTO);
		postAllowedUsers(someName3UserRightsDTO);

		// then
		/*
		Leserechte user1->someName2
		1x GRANT_READ_ACCESS für someName2
		kein REVOKE_READ_ACCESS für user1 da Ersteller

		Leserechte someName2->someName3
		1x GRANT_READ_ACCESS für someName3
		1x REVOKE_READ_ACCESS für someName2
		kein REVOKE_READ_ACCESS für user1 da Ersteller
		 */
		verify(domainLoggingService, times(2)).logActivity(LogActivity.GRANT_READ_ACCESS, "user1_id");
		verify(domainLoggingService, times(1)).logActivity(LogActivity.REVOKE_READ_ACCESS, "user1_id");
	}

	private static UserRightsDTO buildUserRightsDTO(final String userGid, final Freigabetyp freigabetyp) {
		return UserRightsDTO.builder()
			.gid(userGid)
			.freigabetyp(freigabetyp)
			.befristung(null)
			.erstelleNeueVersionBeiDraft(false)
			.build();
	}

	private void postChangeWritePermissions(final UserRightsDTO userRightsDTO) throws JsonProcessingException {
		givenAuthorizationHeaders()
			.contentType(ContentType.JSON)
			.body(objectMapper.writeValueAsString(userRightsDTO))
			.when()
			.post(CHANGE_WRITE_PERMISSIONS_ENDPOINT, COMPOUND_DOCUMENT_ID)
			.then()
			.statusCode(HttpStatus.SC_OK);
	}

	private void postAllowedUsers(final UserRightsDTO userRightsDTO) throws JsonProcessingException {
		givenAuthorizationHeaders()
			.contentType(ContentType.JSON)
			.body(objectMapper.writeValueAsString(List.of(userRightsDTO)))
			.when()
			.post(ALLOWED_USERS_ENDPOINT, COMPOUND_DOCUMENT_ID)
			.then()
			.statusCode(HttpStatus.SC_NO_CONTENT);
	}

	private RequestSpecification givenAuthorizationHeaders() {
		return given()
			.header("Authorization", "Bearer " + jwToken)
			.header("X-XSRF-TOKEN-EDITOR", "xxxxx")
			.header("Cookie", "XSRF-TOKEN=xxxxx; XSRF-TOKEN-EDITOR=xxxxx");
	}
}
