// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static io.restassured.RestAssured.given;

public class DrucksachenTest extends SchnittstellenTests {

	@Override
	public void init2() {
		// Nichts Neues
	}

	@Test
	void vergibEineRegulaereDrucksachennummer() {

		UUID rvid = UUID.fromString("00000000-0000-0000-0000-000000000020");
		String drucksachenNr = "123/2024";

		Response response = getResponse(drucksachenNr, rvid);

		validatableResponse = response
			.then()
			.log().all()
			.assertThat().statusCode(200);
	}

	@Test
	void versucheDrucksachennummerZuSetzenObwohlEsKeineDokumentenmappeMitRichtigemStatusGibt() {
		UUID rvid = UUID.fromString("00000000-0000-0000-0000-000000000005");
		String drucksachenNr = "123/2024";

		Response response = getResponse(drucksachenNr, rvid);

		validatableResponse = response
			.then()
			.log().all()
			.assertThat().statusCode(405);
	}

	@Test
	void versucheDrucksachennummerAufEinNichtExistierendesRegelungsvorhabenZuSetzen() {
		UUID rvid = UUID.fromString("00000000-0000-0000-0000-000000123456");
		String drucksachenNr = "123/2024";

		Response response = getResponse(drucksachenNr, rvid);

		validatableResponse = response
			.then()
			.log().all()
			.assertThat().statusCode(405);
	}

	@Test
	void versucheEineDrucksachennummerZweimalZuVergeben() {
		vergibEineRegulaereDrucksachennummer();

		// Dasselbe nochmal
		UUID rvid = UUID.fromString("00000000-0000-0000-0000-000000000020");
		String drucksachenNr = "123/2024";

		Response response = getResponse(drucksachenNr, rvid);

		validatableResponse = response
			.then()
			.log().all()
			.assertThat().statusCode(406);

	}

	@Test
	void versucheEineUngueltigeDrucksachennummerZuVergeben() {
		UUID rvid = UUID.fromString("00000000-0000-0000-0000-000000000020");
		String drucksachenNr = "1232024";

		Response response = getResponse(drucksachenNr, rvid);

		validatableResponse = response
			.then()
			.log().all()
			.assertThat().statusCode(400);
	}


	private static Response getResponse(String drucksachenNr, UUID rvid) {
		return given()
			.accept(ContentType.ANY)
			.header("x-api-key", "insertKey")
			.queryParam("drucksachenNummer", drucksachenNr)
			.log().all()  // Loggt die gesamte Anfrage
			.when()
			.put("/plattform/user/" + USER_1 + "/drucksachen/" + rvid + "/drucksachennummer/");
	}

}
