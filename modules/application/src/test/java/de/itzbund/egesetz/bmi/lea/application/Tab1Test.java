// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application;

import java.util.Map;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class Tab1Test extends SchnittstellenTests {

	// Die Requests an Plattform werden NICHT gemockt
	// REGELUNGSVORHABEN_ID4 ist archiviert
	// REGELUNGSVORHABEN_ID3 ist leer
	// REGELUNGSVORHABEN_ID2 hat [momentan 1] Mappe(n)

	@Override
	public void init2() {
	}

	protected Map<String, Object> tokenClaims() {

		return Map.of(

			"gid", "user3_id",
			"name", "Vorname3 Nachname3",
			"email", "Vorname3.Nachname3@example.com"

		);
	}

	@Test
	void testPermissionOnTab1() throws Exception {
		JSONObject payload = new JSONObject();
		payload.put("pageNumber", "0");
		payload.put("pageSize", "1");
		payload.put("sortBy", "CREATED_AT");
		payload.put("sortDirection", "DESC");

		Response response = given()
			.contentType(ContentType.JSON)
			.body(payload.toString())
			.header("Authorization", "Bearer " + jwToken)
			.header("X-XSRF-TOKEN-EDITOR", "xxxxx")
			.header("Cookie", "XSRF-TOKEN=xxxxx; XSRF-TOKEN-EDITOR=xxxxx")
			.when()
			.post("/meineDokumente");

		validatableResponse = response
			.then()
			.log().all()
			.assertThat().statusCode(200);

		// Pfad zu den "permission"-Objekten
		String firstChildPermissionPath = "dtos.content[0].children[0].documentPermissions";

		response
			.then()
			.body(firstChildPermissionPath + ".hasRead", equalTo(true))
			.body(firstChildPermissionPath + ".hasWrite", equalTo(true));

		// == Alternative ==
		JsonPath jsonPath = new JsonPath(response.asString());

		// Überprüfen, ob "hasRead" wahr ist und "hasWrite" falsch
		boolean read = jsonPath.get(firstChildPermissionPath + ".hasRead"); // sollte true sein
		assertThat(read).isTrue();

		boolean write = jsonPath.get(firstChildPermissionPath + ".hasWrite"); // sollte true sein
		assertThat(write).isTrue();
	}

}
