// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.docgen.data;

import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.params.provider.Arguments;

import java.util.List;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.VorblattabschnittRefersToLiteral.DAVON_BUEROKRATIEKOSTEN_AUS_INFORMATIONSPFLICHTEN;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.VorblattabschnittRefersToLiteral.ERFUELLUNGSAUFWAND_DER_VERWALTUNG;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.VorblattabschnittRefersToLiteral.ERFUELLUNGSAUFWAND_FUER_BUERGERINNEN_UND_BUERGER;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.VorblattabschnittRefersToLiteral.ERFUELLUNGSAUFWAND_FUER_DIE_WIRTSCHAFT;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_ALTERNATIVEN;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_ERFUELLUNGSAUFWAND;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_LOESUNG;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_PROBLEM_UND_ZIEL;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_WEITERE_KOSTEN;
import static org.junit.jupiter.params.provider.Arguments.arguments;

@UtilityClass
@SuppressWarnings("unused")
public class SampleDocumentGeneratorVorblattData {


    public Stream<Arguments> vorblattSpecs() {
        return Stream.of(

            // Vorgabe VB: Test mit 10-15 Seiten und ca. 15 mittelgroße Tabellen (5 Spalten/ 10 Zeilen)
            arguments(Features.builder()
                .documentType(DocumentType.VORBLATT_STAMMGESETZ)
                .featureSpec(FeatureSpec.builder()
                    .refersTo(VORBLATTABSCHNITT_PROBLEM_UND_ZIEL)
                    .texts(List.of(887, 415, 398, 395, 133, 868, 828, 591))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(VORBLATTABSCHNITT_LOESUNG)
                    .texts(List.of(887, 415, 398, 395, 133, 868, 828, 591))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(VORBLATTABSCHNITT_ALTERNATIVEN)
                    .texts(List.of(887, 415, 398, 395, 133, 868, 828, 591))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(VORBLATTABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND)
                    .texts(List.of(887, 415, 398, 395, 133, 868))
                    .tables(List.of(Pair.of(10, 5), Pair.of(10, 5), Pair.of(10, 5)))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(VORBLATTABSCHNITT_ERFUELLUNGSAUFWAND)
                    .texts(List.of(887, 415, 398, 395, 133, 868, 828, 591))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(ERFUELLUNGSAUFWAND_FUER_BUERGERINNEN_UND_BUERGER)
                    .texts(List.of(887, 415, 398, 395, 133, 868))
                    .tables(List.of(Pair.of(10, 5), Pair.of(10, 5), Pair.of(10, 5)))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(ERFUELLUNGSAUFWAND_FUER_DIE_WIRTSCHAFT)
                    .texts(List.of(887, 415, 398, 395, 133, 868))
                    .tables(List.of(Pair.of(10, 5), Pair.of(10, 5), Pair.of(10, 5)))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(DAVON_BUEROKRATIEKOSTEN_AUS_INFORMATIONSPFLICHTEN)
                    .texts(List.of(887, 415, 398, 395, 133, 868))
                    .tables(List.of(Pair.of(10, 5), Pair.of(10, 5), Pair.of(10, 5)))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(ERFUELLUNGSAUFWAND_DER_VERWALTUNG)
                    .texts(List.of(887, 415, 398, 395, 133, 868))
                    .tables(List.of(Pair.of(10, 5), Pair.of(10, 5), Pair.of(10, 5)))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(VORBLATTABSCHNITT_WEITERE_KOSTEN)
                    .texts(List.of(887, 415, 398, 395, 133, 868, 828, 591))
                    .build())
                .build()));
    }

}
