// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.Dokumentenmappen;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.itzbund.egesetz.bmi.lea.application.SchnittstellenTests;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

public class CompoundDocumentPlattformCall1Test extends SchnittstellenTests {

	private final static String USER_ID = "user1_id";
	private static final String ENDPOINT_URL = "/plattform/user/" + USER_ID + "/regelungsvorhaben/" + REGELUNGSVORHABEN_ID3 + "/compoundDocuments";

	@Autowired
	private ObjectMapper objectMapper;

	@Override
	public void init2() {
		// Nichts neues
	}

	@Test
	@SneakyThrows
	void dokumentenmappenEinesRegelungsvorhabens() {
		Response response = getResponse();

		validatableResponse = response
			.then()
			.log().all()
			.assertThat().statusCode(200);

		List<CompoundDocumentSummaryDTO> actualList = objectMapper.readValue(response.asString(), new TypeReference<>() {
		});

		List<CompoundDocumentSummaryDTO> elementsWithStateBestandsrecht = actualList.stream().filter(cd -> cd.getState().equals(DocumentState.BESTANDSRECHT))
			.collect(Collectors.toList());
		assertThat(elementsWithStateBestandsrecht).isEmpty();
	}

	private Response getResponse() {
		Response response = given()
			.accept(ContentType.ANY)
			.header("x-api-key", "insertKey")
			.contentType(ContentType.JSON)
			.log().all()  // Loggt die gesamte Anfrage
			.when()
			.get(ENDPOINT_URL);
		return response;
	}


}
