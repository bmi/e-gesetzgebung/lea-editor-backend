// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application;

import org.junit.jupiter.api.Test;

import static de.itzbund.egesetz.bmi.lea.core.Constants.HEADER_FIELD_STELLVERTRETUNG;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

class BenutzerTest extends SchnittstellenTests {

    private static final String ENDPOINT_URL = "/user";


    @Override
    public void init2() {
    }


    @Test
    void test_wennAlsNormalerBenutzerAngemeldet_dannErhalteIchMeineDaten() {
        validatableResponse = given()
            .header("Authorization", "Bearer " + jwToken)
            .header("X-XSRF-TOKEN-EDITOR", "xxxxx")
            .header("Cookie", "XSRF-TOKEN=xxxxx; XSRF-TOKEN-EDITOR=xxxxx")
            .when()
            .get(ENDPOINT_URL)
            .then()
            .statusCode(200)
            .body("gid", equalTo("user1_id"));
    }


    @Test
    void test_wennAlsStellvertreterAngemeldet_dannErhalteIchDatenDesVertretenen() {
        validatableResponse = given()
            .header("Authorization", "Bearer " + jwToken)
            .header("X-XSRF-TOKEN-EDITOR", "xxxxx")
            .header("Cookie", "XSRF-TOKEN=xxxxx; XSRF-TOKEN-EDITOR=xxxxx")
            .header(HEADER_FIELD_STELLVERTRETUNG, "user2_id")
            .when()
            .get(ENDPOINT_URL)
            .then()
            .statusCode(200)
            .body("gid", equalTo("user2_id"));
    }


    @Test
    void test_wennKeineStellvertretungErlaubt_dannRueckfallAufEigentlichenUser() {
        validatableResponse = given()
            .header("Authorization", "Bearer " + jwToken)
            .header("X-XSRF-TOKEN-EDITOR", "xxxxx")
            .header("Cookie", "XSRF-TOKEN=xxxxx; XSRF-TOKEN-EDITOR=xxxxx")
            .header(HEADER_FIELD_STELLVERTRETUNG, "user3_id")
            .when()
            .get(ENDPOINT_URL)
            .then()
            .statusCode(200)
            .body("gid", equalTo("user1_id"));
    }

}
