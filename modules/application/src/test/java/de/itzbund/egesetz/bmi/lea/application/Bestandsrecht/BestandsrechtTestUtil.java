// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.Bestandsrecht;

import de.itzbund.egesetz.bmi.lea.application.SchnittstellenTests;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtListDTO;
import lombok.experimental.UtilityClass;

import java.util.UUID;

@UtilityClass
public class BestandsrechtTestUtil {

    BestandsrechtListDTO getExpectedBodyResponse() {
        return BestandsrechtListDTO.builder()
            .id(UUID.fromString(SchnittstellenTests.BESTANDSRECHT_ID2))
            .eli("/eli/recht1")
            .titelKurz("kurzer Titel")
            .titelLang("langer Titel")
            .verknuepfteDokumentenMappeId(UUID.fromString(SchnittstellenTests.COMPOUND_DOCUMENT_ID1))
            .build();
    }

    BestandsrechtListDTO getExpectedBodyResponse2() {
        return BestandsrechtListDTO.builder()
            .id(UUID.fromString(SchnittstellenTests.BESTANDSRECHT_ID3))
            .eli("/eli/recht2")
            .titelKurz("kurzTitel")
            .titelLang("Langtitel")
            .verknuepfteDokumentenMappeId(UUID.fromString(SchnittstellenTests.COMPOUND_DOCUMENT_ID1))
            .build();
    }

    BestandsrechtListDTO getExpectedBodyResponse3() {
        return BestandsrechtListDTO.builder()
            .id(UUID.fromString(SchnittstellenTests.BESTANDSRECHT_ID4))
            .eli("/eli/recht3")
            .titelKurz("kurzer Titel")
            .titelLang("langer Titel")
            .verknuepfteDokumentenMappeId(null)
            .build();
    }

    BestandsrechtListDTO getExpectedBodyResponse4() {
        return BestandsrechtListDTO.builder()
            .id(UUID.fromString(SchnittstellenTests.BESTANDSRECHT_ID))
            .eli("/eli/test/bdsg")
            .titelKurz("BDSG")
            .titelLang("BDSG Bestandsrecht 30.06.2017")
            .verknuepfteDokumentenMappeId(null)
            .build();
    }

}
