// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.logging;

import de.itzbund.egesetz.bmi.lea.LoggingAspect;
import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.logging.LogLevel;

import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@Log4j2
class LoggingAspectTest {

    @Mock
    private ProceedingJoinPoint proceedingJoinPoint;
    @Mock
    private MethodSignature methodSignature;
    @Mock
    private Object targetObject;
    @Mock
    private Object responseObject;

    @Spy
    private LoggingAspect aspect;


    @Test
    void Log() throws Throwable {
        Method method = this.getClass()
            .getMethod("toString");

        //when
        when(proceedingJoinPoint.getSignature()).thenReturn(methodSignature);
        when(proceedingJoinPoint.proceed()).thenReturn(responseObject);
        when(proceedingJoinPoint.getTarget()).thenReturn(targetObject);
        when(methodSignature.getMethod()).thenReturn(method);
        when(methodSignature.getParameterNames()).thenReturn(new String[]{"paramName1", "paramName2", "paramName3"});
        when(proceedingJoinPoint.getArgs()).thenReturn(new Object[]{"paramValue1", "paramValue2", "paramValue3"});

        //call debug
        {
            doReturn(true).when(aspect)
                .isDebug(any());

            Object obj = assertDoesNotThrow(() -> aspect.debugPointcut(proceedingJoinPoint));
            assertEquals(responseObject, obj);

            //verify
            verify(aspect, never()).tracePointcut(any());
            verify(aspect).isDebug(any());
            verify(aspect, never()).isTrace(any());
            verify(aspect, times(2)).log(eq(LogLevel.DEBUG), any());
        }

        reset(aspect);

        //call trace
        {
            doReturn(true).when(aspect)
                .isTrace(any());

            Object obj = assertDoesNotThrow(() -> aspect.tracePointcut(proceedingJoinPoint));
            assertEquals(responseObject, obj);

            //verify
            verify(aspect, never()).debugPointcut(any());
            verify(aspect, never()).isDebug(any());
            verify(aspect).isTrace(any());
            verify(aspect, times(2)).log(eq(LogLevel.TRACE), any());
        }

        reset(aspect);

        //call neither trace nor debug -> nothing should be logged!
        {
            doReturn(false).when(aspect)
                .isDebug(any());
            doReturn(false).when(aspect)
                .isTrace(any());

            Object obj1 = assertDoesNotThrow(() -> aspect.tracePointcut(proceedingJoinPoint));
            Object obj2 = assertDoesNotThrow(() -> aspect.debugPointcut(proceedingJoinPoint));
            assertEquals(responseObject, obj1);
            assertEquals(responseObject, obj2);

            //verify
            verify(aspect).isDebug(any());
            verify(aspect).isTrace(any());
            verify(aspect, never()).log(any(), any());
        }
    }

}
