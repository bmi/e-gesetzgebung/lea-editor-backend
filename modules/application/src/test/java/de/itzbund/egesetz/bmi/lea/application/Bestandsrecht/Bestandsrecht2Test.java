// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.Bestandsrecht;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.itzbund.egesetz.bmi.lea.application.SchnittstellenTests;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtListDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtShortDTO;
import de.itzbund.egesetz.bmi.lea.domain.services.PlategRequestService;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Log4j2
class Bestandsrecht2Test extends SchnittstellenTests {

    private static final String ENDPOINT_URL = "/compounddocuments/" + COMPOUND_DOCUMENT_ID1 + "/bestandsrechte";

    @Autowired
    private ObjectMapper objectMapper;

    // Die Requests an Plattform werden gemockt
    @MockBean
    private PlategRequestService plategRequestService;

    @Override
    public void init2() {

    }

    @Test
    @SneakyThrows
    void ordneBestandsrechtEinerDokumentenmappeZu() {

        List<BestandsrechtShortDTO> payload = List.of(
            BestandsrechtShortDTO.builder()
                .id(UUID.fromString(BESTANDSRECHT_ID4))
                .verknuepfteDokumentenMappeId(UUID.fromString(COMPOUND_DOCUMENT_ID1))
                .build()
        );

        String payloadString = objectMapper.writeValueAsString(payload);
        log.debug("Payload: {}", payloadString);

        Response response = getResponse(payloadString);

        validatableResponse = response
            .then()
            .log().all()
            .assertThat().statusCode(200);

        BestandsrechtListDTO modifiedExpectedResponse = BestandsrechtTestUtil.getExpectedBodyResponse3();
        modifiedExpectedResponse.setVerknuepfteDokumentenMappeId(UUID.fromString(COMPOUND_DOCUMENT_ID1));

        List<BestandsrechtListDTO> actualList = objectMapper.readValue(response.asString(), new TypeReference<>() {
        });
        List<BestandsrechtListDTO> expectedList = List.of(
            modifiedExpectedResponse,
            BestandsrechtTestUtil.getExpectedBodyResponse(),
            BestandsrechtTestUtil.getExpectedBodyResponse2(),
            BestandsrechtTestUtil.getExpectedBodyResponse4());
        assertEquals(expectedList, actualList);
    }

    @Test
    @SneakyThrows
    void entferneBestandsrechtEinerDokumentenmappeZu() {

        List<BestandsrechtShortDTO> payload = List.of(
            BestandsrechtShortDTO.builder()
                .id(UUID.fromString(BESTANDSRECHT_ID3))
                .build()
        );

        String payloadString = objectMapper.writeValueAsString(payload);
        log.debug("Payload: {}", payloadString);

        Response response = getResponse(payloadString);

        validatableResponse = response
            .then()
            .log().all()
            .assertThat().statusCode(200);

        List<BestandsrechtListDTO> actualList = objectMapper.readValue(response.asString(), new TypeReference<>() {
        });

        BestandsrechtListDTO modifiedExpectedResponse = BestandsrechtTestUtil.getExpectedBodyResponse2();
        modifiedExpectedResponse.setVerknuepfteDokumentenMappeId(null);

        List<BestandsrechtListDTO> expectedList = List.of(
            BestandsrechtTestUtil.getExpectedBodyResponse(),
            BestandsrechtTestUtil.getExpectedBodyResponse3(),
            modifiedExpectedResponse,
            BestandsrechtTestUtil.getExpectedBodyResponse4());
        assertEquals(expectedList, actualList);
    }

    private Response getResponse(String payloadString) {
        return given()
            .contentType(ContentType.JSON)
            .body(payloadString)
            .header("Authorization", "Bearer " + jwToken)
            .header("X-XSRF-TOKEN-EDITOR", "xxxxx")
            .header("Cookie", "XSRF-TOKEN=xxxxx; XSRF-TOKEN-EDITOR=xxxxx")
            .when()
            .put(ENDPOINT_URL);
    }


}
