// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.Bestandsrecht;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.itzbund.egesetz.bmi.lea.application.SchnittstellenTests;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.BestandsrechtListDTO;
import de.itzbund.egesetz.bmi.lea.domain.services.PlategRequestService;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Log4j2
class Bestandsrecht3Test extends SchnittstellenTests {

    private static final String ENDPOINT_URL = "/compounddocuments/" + COMPOUND_DOCUMENT_ID1 + "/bestandsrecht/dm";

    @Autowired
    private ObjectMapper objectMapper;

    // Die Requests an Plattform werden gemockt
    @MockBean
    private PlategRequestService plategRequestService;

    @Override
    public void init2() {

    }

    @Test
    @SneakyThrows
    void getBestandsrechtEinerDokumentenmappe() {

        Response response = getResponse();

        validatableResponse = response
            .then()
            .log().all()
            .assertThat().statusCode(200);

        List<BestandsrechtListDTO> actualList = objectMapper.readValue(response.asString(), new TypeReference<>() {
        });
        List<BestandsrechtListDTO> expectedList = List.of(
            BestandsrechtTestUtil.getExpectedBodyResponse(),
            BestandsrechtTestUtil.getExpectedBodyResponse2(),
            BestandsrechtTestUtil.getExpectedBodyResponse3(),
            BestandsrechtTestUtil.getExpectedBodyResponse4());
        assertEquals(expectedList, actualList);
    }

    private Response getResponse() {
        return given()
            .contentType(ContentType.JSON)
            .header("Authorization", "Bearer " + jwToken)
            .header("X-XSRF-TOKEN-EDITOR", "xxxxx")
            .header("Cookie", "XSRF-TOKEN=xxxxx; XSRF-TOKEN-EDITOR=xxxxx")
            .when()
            .get(ENDPOINT_URL);
    }


}
