// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.jdbc.SqlGroup;
import com.zaxxer.hikari.HikariDataSource;
import lombok.AllArgsConstructor;
import static org.junit.jupiter.api.Assertions.assertNotNull;

// @formatter:off
/**
 * Da wir zwei Datenbanken haben, müssen wir die zweite Datenbank durch Angabe des Transaction-Managers befüllen.
 * Außerdem müssen wir Skripte nach Datenbank unterscheiden, weil es einige Inkompatibilitäten gibt.
 */
// @formatter:on
@SqlConfig(transactionManager = "authTransactionManager")
@SqlGroup({
	@Sql("/testDB/RBAC/data-RBAC-clearData.sql"),
	@Sql("/testDB/RBAC/data-RBAC-RV-Rechte.sql"),
	@Sql("/testDB/RBAC/data-RBAC-DM-Rechte.sql"),
	@Sql("/testDB/RBAC/data-RBAC-RVuDMuDok4Tab1.sql"),
	@Sql("/testDB/RBAC/data-RBAC-Leserechte.sql"),
	@Sql("/testDB/RBAC/data-RBAC-Schreibrechte.sql"),
	@Sql("/testDB/RBAC/data-RBAC-RVuDMuDok4Drucksache.sql"),
	@Sql("/testDB/RBAC/data-RBAC-Bestandsrecht.sql"),
	@Sql("/testDB/RBAC/data-RBAC-RVuDMuDok4Bundesrat.sql"),
	// Wir benötigen leider verschiedene Skripte wegen Inkompatibilitäten der Datenbanken
	@Sql("/testDB/RBAC/data-RBAC-Nutzer-mysql.sql"),
	@Sql("/testDB/RBAC/data-RBAC-Nutzer-h2.sql"),
	@Sql("/testDB/RBAC/data-RBAC-Stellvertretung-mysql.sql"),
	@Sql("/testDB/RBAC/data-RBAC-Stellvertretung-h2.sql"),
	@Sql("/testDB/RBAC/data-RBAC-BusinessLogging_DM_RT.sql"),
	@Sql("/testDB/RBAC/data-RBAC-BestRecht4.sql")
}
)
@AllArgsConstructor
public class PrepareAuthDataSource {

	@Qualifier("authDataSource")
	DataSource authDataSource;

	private static DatabaseType database = DatabaseType.UNKNOWN;

	public void init() throws Exception {

		determineDatabase();

		List<String> files = readSqlAnnotations(PrepareAuthDataSource.class);
		files = filterFilesOfOtherDatabases(files);

		for (final String filePath : files) {
			sqlFileIntoDatabase(filePath);
		}
	}

	private List<String> filterFilesOfOtherDatabases(List<String> files) {
		List<String> filteredList = new ArrayList<>();

		files.forEach(file -> {
				boolean isMysqlFile = file.contains("-mysql");
				boolean isH2File = file.contains("-h2");

				if (!isMysqlFile && !isH2File) {
					filteredList.add(file); // Hinzufügen, wenn es kein spezifisches DB-Tag enthält
				} else if ((isMysqlFile && database == DatabaseType.MYSQL) ||
					(isH2File && database == DatabaseType.H2)) {
					filteredList.add(file); // Hinzufügen, wenn Datei und Datenbanktyp übereinstimmen
				}
			}
		);

		return filteredList;
	}

	private void determineDatabase() {
		HikariDataSource dataSource = (HikariDataSource) authDataSource;
		String jdbcUrl = dataSource.getJdbcUrl();
		if (jdbcUrl.contains("mysql")) {
			database = DatabaseType.MYSQL;
		} else if (jdbcUrl.contains("h2")) {
			database = DatabaseType.H2;
		}
	}

	private void sqlFileIntoDatabase(String filePath) throws IOException, SQLException {
		Resource resource = new ClassPathResource(filePath);
		assertNotNull(resource, "Die Ressource konnte nicht gefunden werden: " + filePath);

		try (InputStream inputStream = resource.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			Connection connection = authDataSource.getConnection();
			Statement statement = connection.createStatement()
		) {
			String line;
			StringBuilder sqlScript = new StringBuilder();

			// Lesen des SQL-Skripts aus der Datei
			while ((line = reader.readLine()) != null) {

				// Kommentare entfernen
				if (line.contains("--")) {
					String[] parts = line.split("--");
					line = parts[0];
				}

				sqlScript.append(line);
				// Leerzeichen
				sqlScript.append(" ");

				if (line.contains(";")) {
					// Ausführen des SQL-Skripts
					statement.execute(sqlScript.toString());
					sqlScript = new StringBuilder();
				}

			}

		}
	}

	public List<String> readSqlAnnotations(Class<?> clazz) {
		List<String> list = new ArrayList<>();

		// Alle Annotationen auf Klassenebene auslesen
		Annotation[] annotations = clazz.getAnnotations();

		for (Annotation annotation : annotations) {
			if (annotation instanceof SqlGroup) {
				SqlGroup sqlGroup = (SqlGroup) annotation;
				for (Sql sql : sqlGroup.value()) {  // Alle @Sql-Annotationen in der Gruppe
					Collections.addAll(list, sql.value());
				}
			} else if (annotation instanceof Sql) {  // Einzelne @Sql-Annotation
				Sql sql = (Sql) annotation;
				Collections.addAll(list, sql.value());
			}
		}

		return list;
	}

	enum DatabaseType {
		MYSQL("mysql"),
		H2("h2"),
		UNKNOWN("unknown");

		private final String dbName;

		DatabaseType(String dbName) {
			this.dbName = dbName;
		}

		public String getDbName() {
			return dbName;
		}

		public static DatabaseType fromString(String text) {
			if (text != null) {
				for (DatabaseType type : DatabaseType.values()) {
					if (text.equalsIgnoreCase(type.getDbName())) {
						return type;
					}
				}
			}
			return UNKNOWN;
		}
	}

}
