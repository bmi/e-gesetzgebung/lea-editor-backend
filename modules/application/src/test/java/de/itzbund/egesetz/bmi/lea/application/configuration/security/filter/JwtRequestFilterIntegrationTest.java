// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.configuration.security.filter;

import de.itzbund.egesetz.bmi.lea.application.configuration.security.JwtUtils;
import de.itzbund.egesetz.bmi.lea.application.configuration.security.JwtUtilsTest;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import java.io.IOException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@Log4j2
@ExtendWith({SpringExtension.class}) // JUnit 5
@SpringBootTest(classes = {JwtUtils.class, LeageSession.class})
@SuppressWarnings("unused")
class JwtRequestFilterIntegrationTest extends JwtRequestFilter {

    @Autowired
    private JwtUtils jwtUtils;

    @MockBean
    private UserPersistencePort userRepository;

    @MockBean
    private LeageSession session;

    @MockBean
    private FilterChain filterChain;


    @Test
    void whenRequestHasJWToken_thenGetIt() {
        // Arrange
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Authorization", "Bearer " + JwtUtilsTest.RS256_JWT_TOKEN);

        // Act
        String actual = getJWTokenFromRequest(request);

        // Assert
        assertThat(actual).isEqualTo(JwtUtilsTest.RS256_JWT_TOKEN);
    }


    @Test
    void whenRequestHasNoJWToken_thenGetNull() {
        // Arrange
        MockHttpServletRequest request = new MockHttpServletRequest();

        // Act
        String actual = getJWTokenFromRequest(request);

        // Assert
        assertThat(actual).isNull();
    }


    @Test
    void whenRequestHasJWToken_thenAuthenticationShouldBeValid() throws ServletException, IOException {
        // Arrange
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        request.addHeader("Authorization", "Bearer " + JwtUtilsTest.RS256_JWT_TOKEN);

        doFilterInternal(request, response, filterChain);

        Authentication actual = SecurityContextHolder.getContext().getAuthentication();

        assertThat(actual).isInstanceOf(UsernamePasswordAuthenticationToken.class);
    }


    @Test
    void whenRequestHasNoJWToken_thenAuthenticationShouldNotBeValid() throws ServletException, IOException {
        // Arrange
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();

        // Act
        doFilterInternal(request, response, filterChain);
        Authentication actual = SecurityContextHolder.getContext().getAuthentication();

        // Assert
        assertThat(actual).isNull();
    }


    @Test
    void whenFirstRequestHadTokenAndSecondNot_thenAuthenticationShouldNotBeValid() throws ServletException, IOException {
        // Arrange
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        request.addHeader("Authorization", "Bearer " + JwtUtilsTest.RS256_JWT_TOKEN);

        MockHttpServletRequest requestNew = new MockHttpServletRequest();

        // Act
        doFilterInternal(request, response, filterChain);
        doFilterInternal(requestNew, response, filterChain);

        Authentication actual = SecurityContextHolder.getContext().getAuthentication();

        assertThat(actual).isNull();
    }
}
