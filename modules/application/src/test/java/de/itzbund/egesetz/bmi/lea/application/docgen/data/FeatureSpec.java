// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.docgen.data;

import de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.RefersToLiteral;
import lombok.Data;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ARTICLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BOOK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CHAPTER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PART;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SECTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SUBCHAPTER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SUBSECTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SUBTITLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TITLE;

@Data
@SuperBuilder
public class FeatureSpec {

    private static final Map<String, String> levelNames = Stream.of(
            Pair.of(ELEM_BOOK, "Buch"),
            Pair.of(ELEM_PART, "Teil"),
            Pair.of(ELEM_CHAPTER, "Kapitel"),
            Pair.of(ELEM_SUBCHAPTER, "Unterkapitel"),
            Pair.of(ELEM_SECTION, "Abschnitt"),
            Pair.of(ELEM_SUBSECTION, "Unterabschnitt"),
            Pair.of(ELEM_TITLE, "Titel"),
            Pair.of(ELEM_SUBTITLE, "Untertitel"),
            Pair.of(ELEM_ARTICLE, "§")
        )
        .collect(Collectors.toMap(Pair::getKey, Pair::getValue));


    private RefersToLiteral refersTo;
    private List<Integer> texts;
    private List<Pair<Integer, Integer>> tables;


    public static String getLabel(String ldmlName, int count) {
        return String.format("%s %s", levelNames.get(ldmlName), count);
    }

}
