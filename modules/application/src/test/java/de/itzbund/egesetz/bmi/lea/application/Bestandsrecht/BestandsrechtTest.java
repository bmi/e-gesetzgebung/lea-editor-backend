// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.Bestandsrecht;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.itzbund.egesetz.bmi.lea.application.SchnittstellenTests;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.dtos.bestand.CreateBestandsrechtDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RessortEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.services.PlategRequestService;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState.BESTANDSRECHT;
import static de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType.REGELUNGSTEXT_STAMMGESETZ;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@Log4j2
class BestandsrechtTest extends SchnittstellenTests {

    private static final String REGELUNGSVORHABEN_ID = "00000000-0000-0000-0000-00000000a123";
    private static final String DOKUMENTENMAPPE_ID = "00000010-0000-0000-0000-00000000a123";
    private static final String REGELUNGSTEXT_ID = "00000010-0000-0000-0001-00000000a123";
    private static final String ENDPOINT_URL = "/documents/" + REGELUNGSTEXT_ID + "/bestandsrecht";
    private static final String KURZ_TITEL = Utils.getRandomString();
    private static final String LANG_TITEL = Utils.getRandomString();

    @Autowired
    private ObjectMapper objectMapper;

    // Die Requests an Plattform werden gemockt
    @MockBean
    private PlategRequestService plategRequestService;

    @Override
    public void init2() {
        RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO = RegelungsvorhabenEditorDTO.builder()
            .id(UUID.fromString(REGELUNGSVORHABEN_ID))
            .abkuerzung(REGELUNGSVORHABEN_ABKUERZUNG)
            .kurzbezeichnung(REGELUNGSVORHABEN_KURZBEZEICHNUNG)
            .kurzbeschreibung(REGELUNGSVORHABEN_KURZBESCHREIBUNG)
            .status(REGELUNGSVORHABEN_STATUS)
            .vorhabenart(REGELUNGSVORHABEN_ART)
            .bearbeitetAm(Instant.now())
            .bezeichnung(REGELUNGSVORHABEN_BEZEICHNUNG)
            .technischesFfRessort(RessortEditorDTO.builder()
                .id(UUID.randomUUID().toString())
                .kurzbezeichnung(Utils.getRandomString())
                .bezeichnungNominativ(Utils.getRandomString())
                .bezeichnungGenitiv(Utils.getRandomString())
                .build())
            .allFfRessorts(new ArrayList<>())
            .initiant(REGELUNGSVORHABEN_INITIANT)
            .build();

        when(plategRequestService.getAllRegulatoryProposalsForUserHeIsAuthorOf())
            .thenReturn(List.of(regelungsvorhabenEditorDTO));

        when(plategRequestService.getRegulatoryProposalUserCanAccess(any()))
            .thenReturn(regelungsvorhabenEditorDTO);
    }


    @Test
    @SneakyThrows
    void test_wennRegelungstextStammformAlsBestandMarkiertWird_dannErstelleKopieInBestandsrechtTabelle() {
        CreateBestandsrechtDTO payload = CreateBestandsrechtDTO.builder()
            .regelungsvorhabenId(UUID.fromString(REGELUNGSVORHABEN_ID))
            .dokumentenmappenId(UUID.fromString(DOKUMENTENMAPPE_ID))
            .kurzTitel(KURZ_TITEL)
            .langTitel(LANG_TITEL)
            .build();

        String payloadString = objectMapper.writeValueAsString(payload);
        log.debug("Payload: {}", payloadString);

        Response response = given()
            .contentType(ContentType.JSON)
            .body(payloadString)
            .header("Authorization", "Bearer " + jwToken)
            .header("X-XSRF-TOKEN-EDITOR", "xxxxx")
            .header("Cookie", "XSRF-TOKEN=xxxxx; XSRF-TOKEN-EDITOR=xxxxx")
            .when()
            .post(ENDPOINT_URL);

        validatableResponse = response
            .then()
            .log().all()
            .assertThat().statusCode(200)
            .body("type", equalTo(REGELUNGSTEXT_STAMMGESETZ.name()))
            .body("documentPermissions.hasRead", equalTo(true))
            .body("documentPermissions.hasWrite", equalTo(false))
            .body("state", equalTo(BESTANDSRECHT.name()));
    }

}
