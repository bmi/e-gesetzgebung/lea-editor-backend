// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.Dokumentenmappen;

import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.itzbund.egesetz.bmi.lea.application.SchnittstellenTests;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentSummaryDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CopyCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.services.PlategRequestService;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@Log4j2
class CompoundDocumentVersionierungTest extends SchnittstellenTests {

	private static String ENDPOINT_URL = "/compounddocuments/" + COMPOUND_DOCUMENT_ID1 + "/version";
	public static final String TITLE_FOR_COMPOUND_DOCUMENT = "Title for compound document";

	@Autowired
	private ObjectMapper objectMapper;

	// Die Requests an Plattform werden gemockt, falls (!) nötig
	@MockBean
	private PlategRequestService plategRequestService;

	@Override
	public void init2() {
		// In diesem Fall müsen wir kein Plateq-Request mocken, weil die RVs im Skript: "data-Editor-RVin_den_Editor.sql"
		// in die Datenbank geladen werden und somit der Plateq-Aufruf nicht stattfindet.
	}

	protected Map<String, Object> tokenClaims() {

		return Map.of(

			"gid", "user3_id",
			"name", "Vorname3 Nachname3",
			"email", "Vorname3.Nachname3@example.com"

		);
	}

	@Test
	@SneakyThrows
	void erstelleNeueVersionDerDokumentenmappe() {
		ENDPOINT_URL = "/compounddocuments/" + COMPOUND_DOCUMENT_ID1 + "/version";

		// Das DTO für den Payload (alternativ gleich ein JSON-String)
		CopyCompoundDocumentDTO payload = CopyCompoundDocumentDTO.builder()
			.title(TITLE_FOR_COMPOUND_DOCUMENT)
			.build();

		String payloadString = objectMapper.writeValueAsString(payload);
		log.debug("Payload: {}", payloadString);

		// Der ganze Aufruf (inkl. URL aus Konstante) und HTTP-Methoden (!), hier POST ...
		// .. der Header ist sonst relativ uninteressant
		Response response = getResponse(payloadString);

		// Auswertung des Response
		validatableResponse = response
			.then()
			.log().all()
			.assertThat().statusCode(200);

		CompoundDocumentSummaryDTO actual = objectMapper.readValue(response.asString(), new TypeReference<>() {
		});

		assertEquals(TITLE_FOR_COMPOUND_DOCUMENT, actual.getTitle());
		assertNotEquals(COMPOUND_DOCUMENT_ID1, actual.getId().toString());
	}

	@Test
	@SneakyThrows
	void versucheNeueVersionDerDokumentenmappeMitUngueltigerMappenId() {
		ENDPOINT_URL = "/compounddocuments/" + "00000000-0000-0000-0000-000000000000" + "/version";

		// Das DTO für den Payload (alternativ gleich ein JSON-String)
		CopyCompoundDocumentDTO payload = CopyCompoundDocumentDTO.builder()
			.title(TITLE_FOR_COMPOUND_DOCUMENT)
			.build();

		String payloadString = objectMapper.writeValueAsString(payload);
		log.debug("Payload: {}", payloadString);

		// Der ganze Aufruf (inkl. URL aus Konstante) und HTTP-Methoden (!), hier POST ...
		// .. der Header ist sonst relativ uninteressant
		Response response = getResponse(payloadString);

		// Auswertung des Response
		validatableResponse = response
			.then()
			.log().all()
			.assertThat().statusCode(404);
	}

	private Response getResponse(String payloadString) {
		return given()
			.contentType(ContentType.JSON)
			.body(payloadString)
			.header("Authorization", "Bearer " + jwToken)
			.header("X-XSRF-TOKEN-EDITOR", "xxxxx")
			.header("Cookie", "XSRF-TOKEN=xxxxx; XSRF-TOKEN-EDITOR=xxxxx")
			.when()
			.post(ENDPOINT_URL);
	}

	// Ein Teil von dem erwarteten DTO der Antwort
	private CompoundDocumentDTO getExpectedBodyRepsonse() {
		return CompoundDocumentDTO.builder()
			.title(TITLE_FOR_COMPOUND_DOCUMENT)
			.build();
	}

}
