// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.docgen;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.RepeatedTest;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Log4j2
public class RandomTableHeaderGeneratorTest {


    @RepeatedTest(53)
    void test_getNext_1() {
        int count = 7;
        Set<String> values = new HashSet<>();
        RandomTableHeaderGenerator generator = new RandomTableHeaderGenerator();

        for (int i = 0; i < count; i++) {
            values.add(generator.getNext());
        }

        log.debug(values);
        assertFalse(values.isEmpty());
        assertTrue(count - values.size() <= 1);
    }

    @RepeatedTest(17)
    void test_getNext_n() {
        RandomTableHeaderGenerator generator = new RandomTableHeaderGenerator();
        assertEquals(List.of(), generator.getNext(-2));
        assertEquals(List.of(), generator.getNext(0));
        assertEquals(3, generator.getNext(3).size());
        assertEquals(123, generator.getNext(123).size());
    }

}
