// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.docgen;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@Disabled("Not a real test, just a tool")
public class RandomTableHeaderGenerator implements RandomValueGenerator<String> {

    // <editor-fold desc="word list">
    private static final List<String> WORDS = List.of(
        "Änderung", "Ärger", "Äußerung", "Überlegung", "Übernahme",
        "Abbau", "Abend", "Abkommen", "Ablehnung", "Abschied",
        "Abschluss", "Abschnitt", "Absicht", "Abstand", "Abstimmung",
        "Abteilung", "Aktie", "Aktionär", "Aktion", "Aktivität",
        "Aldi", "Allianz", "Alter", "Alternative", "Analyse",
        "Anbieter", "Anerkennung", "Anforderung", "Anfrage", "Angebot",
        "Angriff", "Angst Betrieb", "Anklage", "Anlage", "Anlass",
        "Anleger", "Anschlag", "Ansicht", "Anspruch", "Anstieg",
        "Anteil", "Antrag", "Antwort", "Anwalt", "Anwendung",
        "Anzeige", "Arbeitgeber", "Arbeitslosigkeit", "Arbeitsplatz", "Arbeitsschritt",
        "Armee", "Artikel", "Arzt", "Atmosphäre", "Aufbau",
        "Auffassung", "Aufgabe", "Aufklärung", "Aufmerksamkeit", "Aufnahme",
        "Auftakt", "Auftrag", "Auftritt", "Auge", "Augenblick",
        "Ausbildung", "Ausdruck", "Auseinandersetzung", "Ausgabe", "Ausgleich",
        "Auskunft", "Ausländer", "Ausland", "Ausnahme", "Aussage",
        "Aussicht", "Ausstellung", "Auswirkung", "Auto", "Autobahn",
        "Autor", "Börse", "Bühne", "Bündnis", "Bürgermeister",
        "Büro", "BMW", "Bahn", "Bahnhof", "Ball",
        "Band", "Bank", "Basis", "Bau", "Bauer",
        "Baum", "Bedarf", "Bedeutung", "Bedingung", "Begegnung",
        "Beginn", "Begründung", "Begriff", "Behörde", "Behandlung",
        "Beispiel", "Beitrag", "Belastung", "Benutzer", "Beobachter",
        "Beratung", "Bereitschaft", "Berg", "Bericht", "Beruf",
        "Berufung", "Beschäftigte", "Beschäftigung", "Besitz", "Bestand",
        "Besuch", "Besucher", "Beteiligung", "Betrag", "Bevölkerung",
        "Bewegung", "Beweis", "Bewertung", "Bewohner", "Bewusstsein",
        "Bezeichnung", "Beziehung", "Bier", "Bilanz", "Bildung",
        "Blatt", "Blick", "Blut", "Boden", "Botschaft",
        "Brücke", "Branche", "Brand", "Brief", "Buch",
        "Bund", "Bundesliga", "Bundesrepublik", "Bundeswehr", "Bus",
        "Chance", "Charakter", "Computer", "Dach", "Dank",
        "Darstellung", "Dauer", "Debatte", "Demokratie", "Demonstrant",
        "Denken", "Detail", "Dialog", "Dienst", "Dienstleistung",
        "Ding", "Direktor", "Diskussion", "Dokumentation", "Dorf",
        "Drittel", "Druck", "Dutzend", "EU", "Ebene",
        "Ecke", "Ehe", "Eindruck", "Einführung", "Einfluss",
        "Einheit", "Einigung", "Einkommen", "Einnahme", "Einrichtung",
        "Einsatz", "Einschätzung", "Einstellung", "Einstieg", "Einwohner",
        "Energie", "Engagement", "Entwurf", "Eröffnung", "Erde",
        "Ereignis", "Erfahrung", "Erhöhung", "Erinnerung", "Erkenntnis",
        "Erklärung", "Erwartung", "Erweiterung", "Essen", "Euro",
        "Existenz", "Förderung", "Führer", "Führung", "Fünf",
        "Fahrt", "Fahrzeug", "Fall", "Falle", "Farbe",
        "Februar", "Fehler", "Feld", "Fenster", "Fernsehen",
        "Fest", "Feuer", "Feuerwehr", "Figur", "Finale",
        "Finanzierung", "Firma", "Fläche", "Flüchtling", "Flucht",
        "Flughafen", "Folge", "Fond", "Forderung", "Forscher",
        "Forschung", "Fortsetzung", "Foto", "Fraktion", "Freiheit",
        "Freude", "Frieden", "Fuß", "Fußball", "Funktion",
        "Fusion", "Galerie", "Gang", "Garten", "Gast",
        "Gebäude", "Geben", "Gebiet", "Geburtstag", "Gedanke",
        "Gefängnis", "Gefühl", "Gefahr", "Gegensatz", "Gegenteil",
        "Gegenwart", "Geist", "Gelände", "Gelegenheit", "Gemeinde",
        "Genehmigung", "Generation", "Gericht", "Geschäft", "Gesetz",
        "Gesicht", "Gespräch", "Gestalt", "Gestaltung", "Gesundheit",
        "Gewalt", "Gewerkschaft", "Gewicht", "Gewinn", "Glück",
        "Gold", "Größe", "Gründung", "Grad", "Grenze",
        "Grundlage", "Gutachten", "Hälfte", "Händler", "Höhepunkt",
        "Haft", "Halle", "Haltung", "Handel", "Haushalt",
        "Heimat", "Hektar", "Herbst", "Herkunft", "Hersteller",
        "Herstellung", "Herz", "Himmel", "Hintergrund", "Hinweis",
        "Hof", "Hoffnung", "Holz", "Hotel", "Idee",
        "Identität", "Image", "Industrie", "Inhalt", "Initiative",
        "Institut", "Inszenierung", "Integration", "Interesse", "Internet",
        "Interview", "Investition", "Investor", "Jahrhundert", "Jahrzehnt",
        "Job", "Journalist", "Jugendliche", "Justiz", "König",
        "Körper", "Künstler", "Kabinett", "Kamera", "Kampf",
        "Kapital", "Kapitel", "Karriere", "Karte", "Kasse",
        "Katastrophe", "Kauf", "Keller", "Kenntnis", "Kennzeichen",
        "Kern", "Kilogramm", "Kilometer", "Kino", "Kirche",
        "Klage", "Klasse", "Klima", "Koalition", "Kollege",
        "Kommission", "Kommune", "Kommunikation", "Kompromiss", "Konferenz",
        "Konflikt", "Kongress", "Konkurrenz", "Konsequenz", "Kontakt",
        "Kontrolle", "Konzept", "Konzern", "Konzert", "Kooperation",
        "Kopf", "Kraft", "Krankenhaus", "Krankheit", "Kreis",
        "Krieg", "Krise", "Kritik", "Kultur", "Kunst",
        "Kurs", "Länge", "Lösung", "Lager", "Landkreis",
        "Landschaft", "Landtag", "Landwirtschaft", "Lehre", "Leistung",
        "Leitung", "Licht", "Liebe", "Linie", "Liste",
        "Liter", "Literatur", "Luft", "Lust", "Möglichkeit",
        "Maß", "Maßnahme", "Macht", "Management", "Manager",
        "Mannschaft", "Marke", "Maschine", "Material", "Mauer",
        "Medium", "Meer", "Mehrheit", "Meinung", "Meister",
        "Menge", "Methode", "Minister", "Mischung", "Mitarbeiter",
        "Mitte", "Mitteilung", "Mittel", "Mittelpunkt", "Modell",
        "Moderne", "Moment", "Mord", "Morgen", "Motto",
        "Mund", "Museum", "Mut", "Nähe", "Nachbar",
        "Nachfrage", "Nachmittag", "Nachricht", "Nacht", "Nase",
        "Nation", "Natur", "Netz", "Neubau", "Nichts",
        "Niederlage", "Niveau", "Norden", "Not", "Notwendigkeit",
        "Nummer", "Nutzung", "Objekt", "Oper", "Opposition",
        "Ordnung", "Organisation", "Ort", "Osten", "Papier",
        "Parlament", "Partie", "Partner", "Patient", "Pause",
        "Person", "Personal", "Pfarrer", "Pflanze", "Pflege",
        "Pflicht", "Pfund", "Phase", "Plan", "Planung",
        "Position", "Post", "Posten", "Prüfung", "Praxis",
        "Preis", "Premiere", "Prinzip", "Privatisierung", "Produkt",
        "Produktion", "Professor", "Projekt", "Protest", "Provinz",
        "Prozess", "Punkt", "Quadratmeter", "Qualität", "Quartal",
        "Rücken", "Rückgang", "Rückkehr", "Rücktritt", "Rückzug",
        "Rahmen", "Rand", "Rang", "Rat", "Rathaus",
        "Raum", "Reaktion", "Realität", "Rebell", "Rechnung",
        "Recht", "Rede", "Reform", "Regel", "Regelung",
        "Regen", "Regie", "Region Israel", "Reich", "Reihe",
        "Reise", "Rennen", "Republik", "Rest", "Revolution",
        "Richtung", "Risiko", "Roman", "Ruhe", "Runde",
        "Süden", "SMS", "Saal", "Sache", "Saison",
        "Sammlung", "Sanierung", "Schaden", "Schatten", "Schauspieler",
        "Schicksal", "Schiff", "Schlüssel", "Schloss", "Schluss",
        "Schreiben", "Schriftsteller", "Schritt", "Schuld", "Schule",
        "Schutz", "Schwerpunkt", "Schwierigkeit", "See", "Seele",
        "Sekunde", "Senat", "Sender", "Serie", "Service",
        "Sicherheit", "Sicht", "Sieg", "Sinn", "Situation",
        "Sitz", "Sitzung", "Software", "Soldat", "Sommer",
        "Sonne", "Sorge", "Spaß", "Spannung", "Spiegel",
        "Spielen", "Spitze", "Sport", "Sprache", "Sprecherin",
        "Spur", "Stärke", "Stück", "Staat", "Staatsanwaltschaft",
        "Stand", "Standort", "Start", "Status", "Steigerung",
        "Stein", "Stelle", "Stellung", "Stellungnahme", "Steuer",
        "Stiftung", "Stil", "Stimme", "Stimmung", "Stoff",
        "Straße", "Strategie", "Strecke", "Streit", "Strom",
        "Struktur", "Student", "Studie", "Studium", "Suche",
        "Summe", "Szene", "Tätigkeit", "Tür", "Tabelle",
        "Tat", "Tatsache", "Tausend", "Technik Insel", "Teilnahme",
        "Teilnehmer", "Telefon", "Telekom", "Tempo", "Tendenz",
        "Termin", "Text", "Theater", "Theorie", "Tier",
        "Tisch", "Titel", "Tod", "Ton", "Tonne",
        "Tor", "Tote", "Tourist", "Tradition", "Training",
        "Traum", "Treffen", "Trend", "Trennung", "Typ",
        "Umfang", "Umfeld", "Umfrage", "Umgang", "Umgebung",
        "Umsatz", "Umsetzung", "Umwelt", "Unabhängigkeit", "Unfall",
        "Universität", "Unterschied", "Unterstützung", "Untersuchung", "Urlaub",
        "Ursache", "Urteil", "VW", "Variante", "Veränderung",
        "Veranstalter", "Veranstaltung", "Verantwortung", "Verband", "Verbesserung",
        "Verbindung", "Verbraucher", "Verbrechen", "Verdacht", "Verein",
        "Vereinbarung", "Vereinigung", "Verfügung", "Verfahren", "Verfassung",
        "Vergangenheit", "Vergleich", "Verhältnis", "Verhalten", "Verhandlung",
        "Verkauf", "Verkehr", "Verlag", "Verlauf", "Verletzung",
        "Verlust", "Version", "Verständnis", "Versuch", "Verteidigung",
        "Vertrag", "Vertrauen", "Vertrieb", "Verwaltung", "Verwendung",
        "Verzicht", "Viertel", "Volk", "Voraussetzung", "Vorbereitung",
        "Vorbild", "Vorgang", "Vorgehen", "Vorhaben", "Vorlage",
        "Vorschlag", "Vorsitzende", "Vorstellung", "Vorteil", "Vortrag",
        "Vorwurf", "Währung", "WM", "Wachstum", "Waffe",
        "Wagen", "Wahl", "Wahlkampf", "Wahrheit", "Wald",
        "Wand", "Ware", "Wasser", "Wechsel", "Weihnachten",
        "Welt", "Weltmeister", "Wende", "Werbung", "Werk",
        "Wert", "Westen", "Wettbewerb", "Wetter", "Widerspruch",
        "Widerstand", "Wind", "Winter", "Wirklichkeit", "Wirkung",
        "Wissen", "Wissenschaft", "Wissenschaftler", "Wochenende", "Wohnung",
        "Wort", "Wunder", "Wunsch", "Zeichen", "Zeitpunkt",
        "Zeitraum", "Zeitschrift", "Zeitung", "Zelle", "Zentimeter",
        "Zentrum", "Zeuge", "Zimmer", "Zins", "Zug",
        "Zugang", "Zusammenhang", "Zustand", "Zustimmung", "Zweck",	"Abgeordnete",
        "Zweifel"
        );
    // </editor-fold>

    private static final SecureRandom random = new SecureRandom();

    private static final int WORDS_SIZE = WORDS.size();

    @Override
    public String getNext() {
        return WORDS.get(random.nextInt(WORDS_SIZE));
    }

    @Override
    public List<String> getNext(int count) {
        if (count <= 0) return List.of();

        List<String> resultList = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            resultList.add(getNext());
        }
        return resultList;
    }

}
