// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.configuration.security;

import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Map;
import javax.crypto.Cipher;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * To generate a JWToken we can use: <a href="http://jwtbuilder.jamiekurtz.com/">JWTBuilder</a>
 */
public class JwtUtilsTest extends JwtUtils {

    public final static String RS256_JWT_TOKEN =
        "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJtdEo1YnpQWVJ2T3Z4MWNCMVVFbnJ1UnBCMU5ZMjhMRFFRUjhoeVY3cFBRIn0.eyJleHAiOjE2NTQxNjU0OTMsImlhdCI6MTY1NDE2NTQzMywianRpIjoiN2U1ZGQ1NWMtN2E0MC00NzcwLWE4YmQtNGY4NzM0MTE1YmE4IiwiaXNzIjoiaHR0cHM6Ly9pYW0tZWdnLWJrLWRlbW8uc2VpdGVuYmF1Lm5ldC9hdXRoL3JlYWxtcy9idW5kIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6ImY6NjU0N2FmZGItN2NiMi00ZmRjLTgxNTYtZjdiMzExZTBjZTM4OnJvYmVydDEucmVjaHRzZXR6dW5nc3JlZmVyZW50MUBlZ2VzZXR6LmRlIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiZWdnIiwic2Vzc2lvbl9zdGF0ZSI6Ijg1YWNkYTJkLWQ1YjYtNDNkMy04OTZjLTAzMWI4OWFlMjM4YiIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiaHR0cHM6Ly9wbGF0dGZvcm0taW50LmVnZXNldHotZGVtby5kZSIsImh0dHA6Ly9sb2NhbGhvc3Q6ODA4MCIsImh0dHBzOi8vcGxhdHRmb3JtLXRlc3QtZWRpdG9yLmVnZXNldHotZGVtby5kZSIsImh0dHBzOi8vcGxhdHRmb3JtLmVnZXNldHotZGVtby5kZSIsImh0dHBzOi8vcGxhdHRmb3JtLWRlbW8uZWdlc2V0ei1kZW1vLmRlIiwiaHR0cHM6Ly9wbGF0dGZvcm0tZGV2LXBsYXRlZy5lZ2VzZXR6LWRlbW8uZGUiLCJodHRwczovL3BsYXR0Zm9ybS10ZXN0LXBsYXRlZy5lZ2VzZXR6LWRlbW8uZGUiLCJodHRwczovL3BsYXR0Zm9ybS1tdXN0ZXItc3RhZ2UuZWdlc2V0ei1kZW1vLmRlIiwiaHR0cHM6Ly9wbGF0dGZvcm0tZGV2LWVkaXRvci5lZ2VzZXR6LWRlbW8uZGUiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoib3BlbmlkIHByb2ZpbGUgYnVuZCBlbWFpbCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJnaWQiOiJzb21lUmFuZG9tSWQiLCJyZXNzb3J0Ijo0LCJsYXN0bW9kaWZpZWQiOiIyMDIyLTAxLTE0VDEwOjEwOjU4LjA5Mjk0NSIsIm5hbWUiOiJSb2JlcnQxIFJlY2h0c2V0enVuZ3NyZWZlcmVudDEiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJyb2JlcnQxLnJlY2h0c2V0enVuZ3NyZWZlcmVudDFAZWdlc2V0ei5kZSIsImdpdmVuX25hbWUiOiJSb2JlcnQxIiwiZmFtaWx5X25hbWUiOiJSZWNodHNldHp1bmdzcmVmZXJlbnQxIiwiYWt0aXYiOnRydWUsImVtYWlsIjoicm9iZXJ0MS5yZWNodHNldHp1bmdzcmVmZXJlbnQxQGVnZXNldHouZGUifQ.R3Zg50Ez-t8F9ShczwVFWvk_E3Z2kbeY6LHonDiJR9KSUb0j5FkRiFIbYLPYHIj-WowEpLJLy-mN0GH5oZzhpxc7c3XCXpUZLSijvaZw9lVgYDkxSGRHnR1MfJI4nIDyb-2Np-GVIPDT9Ph-53ioVWEbR1BaN2tA3JtrcRlYonmUVM2HIU94i1f1AEBsI6icJ-2zgMGciK9lmOrQOFJfKjKZuMrsEf_SLOWuTVLAlYjhktEv-3z6-jPYLZCjpG17JuIiuFi6R6T5Zr9wazIsNM6CN4kzHSP8hwxNqXNh9Ou-bRStDY6IhMJ_imqsbcMhCkjwYmho6XIgpk3R2ko5fQ";
    private final static String HSW256_JWT_TOKEN =
        "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9"
            +
            ".eyJpc3MiOiJBbGV4YW5kZXIgYXMgSXNzdWVyIiwiaWF0IjoxNjU0MTY1ODkzLCJleHAiOjE2ODU3MDE4OTMsImF1ZCI6Ind3dy5leGFtcGxlLmNvbSIsInN1YiI6IkpvaG4uRG9lQGV4YW1wbGUuY29tIiwiR2l2ZW5OYW1lIjoiSm9obiIsIlN1cm5hbWUiOiJEb2UiLCJFbWFpbCI6IkpvaG4uRG9lQGV4YW1wbGUuY29tIiwiUm9sZSI6WyJNYW5hZ2VyIiwiUHJvamVjdCBBZG1pbmlzdHJhdG9yIl19.t-myg5FCsrwt0f8Zy8iR-HM6fV7lCUaXiUNMc7ElZms";
    private static final int KEY_SIZE = 2048;

    private static KeyPair keypair;

    @Override
    public RSAPublicKey getPublicKeyFromKeystore() {
        return (RSAPublicKey) keypair.getPublic();
    }

    @BeforeAll
    public static void setup() {
        keypair = generateRSAKeyPair();
    }

    public static KeyPair generateRSAKeyPair() {
        try {
            // Key generation
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
            generator.initialize(KEY_SIZE);
            return generator.generateKeyPair();
        } catch (NoSuchAlgorithmException nsae) {
            throw new IllegalStateException("RSA Algorithm is missing");
        }
    }

    @Disabled("Token seems to invalid")
    @Test
    void whenValidRS256TokenIsGiven_ValidationMustBeSuccessful() {
        // Arrange
        // Our public key is the Certificate of the keystore

        // Act
        boolean actual = validateJwtTokenRSA256WithKeystore(RS256_JWT_TOKEN);

        // Assert
        assertThat(actual).isTrue();
    }

    @Test
    void testRSA() throws Exception {
        PrivateKey privateKey = keypair.getPrivate();
        PublicKey publicKey = keypair.getPublic();

        // Encryption - means Signing
        String secretMessage = "My secret message";
        Cipher encryptCipher = Cipher.getInstance("RSA");
        encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);

        byte[] secretMessageBytes = secretMessage.getBytes(StandardCharsets.UTF_8);
        byte[] encryptedMessageBytes = encryptCipher.doFinal(secretMessageBytes);

        // Decryption - means Test signature
        Cipher decryptCipher = Cipher.getInstance("RSA");
        decryptCipher.init(Cipher.DECRYPT_MODE, privateKey);

        byte[] decryptedMessageBytes = decryptCipher.doFinal(encryptedMessageBytes);
        String decryptedMessage = new String(decryptedMessageBytes, StandardCharsets.UTF_8);

        assertEquals(secretMessage, decryptedMessage);
    }


    @Test
    void whenValidHSW256TokenIsGiven_AMapShouldBeRetrieved() {
        // Arrange

        // Act
        Map<String, Object> actual = getPayload(HSW256_JWT_TOKEN);

        // Assert
        assertThat(actual).hasFieldOrPropertyWithValue("Email", "John.Doe@example.com");
    }


    @Test
    void whenPayloadFromInvalidTokenShouldExtracted_AnEmptyMapShouldBeRetrieved() {
        // Act
        Map<String, Object> actual = getPayload(Utils.getRandomString());
        // Assert
        assertThat(actual).contains();
    }


    @Test
    void whenARS256TokenShouldBeCreate_ItMustBeValid() {
        // Arrange - override in testable class
        privateKey = keypair.getPrivate();

        PublicKey publicKey = keypair.getPublic();
        String username = "my user";

        // Act
        String generatedToken = generateRS256JwtToken(username);
        assertThat(generatedToken).isNotEmpty();

        boolean actual = validateJwtTokenRS256(generatedToken, (RSAPublicKey) publicKey);

        // Assert
        assertThat(actual).isTrue();
    }

}
