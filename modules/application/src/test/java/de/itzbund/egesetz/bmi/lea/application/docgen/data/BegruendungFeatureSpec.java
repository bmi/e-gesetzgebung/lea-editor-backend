// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.docgen.data;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Singular;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ARTICLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BOOK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CHAPTER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PART;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SECTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SUBCHAPTER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SUBSECTION;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SUBTITLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TITLE;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
public class BegruendungFeatureSpec extends FeatureSpec {

    /*
     * Elements of the list represent the hierarchy to generate, i.e. [chapter, section, article] with depth 3
     * or [article] with depth 1 (flat).
     * The integer, the second value in the pairs, defines the number of occurrences of this level, i.e.
     * [(section, 2), (article, 3)] means there shall be two sections which contain 3 articles each.
     */
    @Singular("hierarchyLevel")
    private List<Pair<String, Integer>> hierarchy;

}
