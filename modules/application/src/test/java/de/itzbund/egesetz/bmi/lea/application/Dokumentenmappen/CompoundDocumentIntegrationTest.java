// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.Dokumentenmappen;

import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.compounddocument.CreateCompoundDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.CreateDocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.document.DocumentDTO;
import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.CompoundDocumentTypeVariant;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.RegelungsvorhabenTypType;
import de.itzbund.egesetz.bmi.lea.domain.enums.ServiceState;
import de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentBearbeitendeInstitution;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentFassung;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentForm;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentInitiant;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentTyp;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.CompoundDocumentId;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.services.AuxiliaryService;
import de.itzbund.egesetz.bmi.lea.domain.services.CompoundDocumentService;
import de.itzbund.egesetz.bmi.lea.domain.services.DocumentService;
import de.itzbund.egesetz.bmi.lea.domain.services.RegelungsvorhabenService;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import de.itzbund.egesetz.bmi.lea.persistence.adapters.BestandsrechtPersistenceAdapter;
import de.itzbund.egesetz.bmi.lea.persistence.adapters.CompoundDocumentPersistenceAdapter;
import de.itzbund.egesetz.bmi.lea.persistence.adapters.DocumentPersistenceAdapter;
import de.itzbund.egesetz.bmi.lea.persistence.adapters.UserPersistenceAdapter;
import de.itzbund.egesetz.bmi.lea.persistence.repositories.BestandsrechtRepository;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import de.itzbund.egesetz.bmi.user.repositories.NutzerEntityRepository;
import lombok.SneakyThrows;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.findChild;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDescendant;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getText;
import static de.itzbund.egesetz.bmi.lea.core.xml.LegalDocMLdeMetadatenFeld.META_ART;
import static de.itzbund.egesetz.bmi.lea.core.xml.LegalDocMLdeMetadatenFeld.META_BEARBEITENDE_INSTITUTION;
import static de.itzbund.egesetz.bmi.lea.core.xml.LegalDocMLdeMetadatenFeld.META_FASSUNG;
import static de.itzbund.egesetz.bmi.lea.core.xml.LegalDocMLdeMetadatenFeld.META_FORM;
import static de.itzbund.egesetz.bmi.lea.core.xml.LegalDocMLdeMetadatenFeld.META_INITIANT;
import static de.itzbund.egesetz.bmi.lea.core.xml.LegalDocMLdeMetadatenFeld.META_TYP;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

@SpringBootTest
@SuppressWarnings("unused")
class CompoundDocumentIntegrationTest {


    public static final String GID = "someRandomId";
    public static final String NAME = "TestVorname Testnachmane";
    public static final String EMAIL = "Test.Nonexisting1@email.de";

    private final User user = User.builder()
        .gid(new UserId(GID))
        .name(NAME)
        .email(EMAIL)
        .build();
    private final String rvId = "00000000-0000-0000-0000-000000000005";
    private final JSONParser jsonParser = new JSONParser();

    private MockedStatic<LeageSession> mockedLeageSession;

    /*
     * Dies ist die nach Bezeichnung sortierte Menge der Dokumenttypen, ausgenommen Anlage, Synopse, Drucksachen
     * und Rechtsetzungsdokumente:
     *
     * size = 16
        ANSCHREIBEN_MANTELGESETZ
        ANSCHREIBEN_MANTELVERORDNUNG
        ANSCHREIBEN_STAMMGESETZ
        ANSCHREIBEN_STAMMVERORDNUNG

        BEGRUENDUNG_MANTELGESETZ
        BEGRUENDUNG_MANTELVERORDNUNG
        BEGRUENDUNG_STAMMGESETZ
        BEGRUENDUNG_STAMMVERORDNUNG

        REGELUNGSTEXT_MANTELGESETZ
        REGELUNGSTEXT_MANTELVERORDNUNG
        REGELUNGSTEXT_STAMMGESETZ
        REGELUNGSTEXT_STAMMVERORDNUNG

        VORBLATT_MANTELGESETZ
        VORBLATT_MANTELVERORDNUNG
        VORBLATT_STAMMGESETZ
        VORBLATT_STAMMVERORDNUNG
     */
    private final TreeSet<DocumentType> docTypes = Arrays.stream(DocumentType.values())
        .filter(type -> type != DocumentType.ANLAGE
            && type != DocumentType.SYNOPSE
            && type != DocumentType.BT_DRUCKSACHE
            && type.getArt() != RechtsetzungsdokumentArt.RECHTSETZUNGSDOKUMENT)
        .collect(Collectors.toCollection(
            () -> new TreeSet<>(Comparator.comparing(DocumentType::name))
        ));
    @Autowired
    private DocumentService documentService;
    @Autowired
    private CompoundDocumentService compoundDocumentService;
    @Autowired
    private AuxiliaryService auxiliaryService;
    @Autowired
    private UserPersistenceAdapter userPersistenceAdapter;
    @Autowired
    private DocumentPersistenceAdapter documentPersistenceAdapter;
    @Autowired
    private CompoundDocumentPersistenceAdapter compoundDocumentPersistenceAdapter;
    @MockBean
    private RegelungsvorhabenService regelungsvorhabenService;
    @MockBean
    private LeageSession session;
    @MockBean
    private NutzerEntityRepository nutzerEntityRepository;
    @MockBean
    private BestandsrechtRepository bestandsrechtRepository;
    @Autowired
    private DocumentPersistenceAdapter documentRepository;
    @Autowired
    private BestandsrechtPersistenceAdapter bestandsrechtPersistenceAdapter;

    private static Stream<Arguments> testParams() {
        return Stream.of(
            Arguments.of(
                CompoundDocumentTypeVariant.AENDERUNGSVERORDNUNG, // col 2
                List.of(false, true, false, false, // Anschreiben
                    false, true, false, false, // Begründung
                    false, false, false, false, // Regelungstext
                    false, true, false, false)), // Vorblatt
            Arguments.of(
                CompoundDocumentTypeVariant.MANTELGESETZ, // col 1
                List.of(true, false, false, false, // Anschreiben
                    true, false, false, false, // Begründung
                    false, false, false, false, // Regelungstext
                    true, false, false, false)), // Vorblatt
            Arguments.of(
                CompoundDocumentTypeVariant.STAMMGESETZ, // col 3
                List.of(false, false, true, false, // Anschreiben
                    false, false, true, false, // Begründung
                    false, false, false, false, // Regelungstext
                    false, false, true, false)), // Vorblatt
            Arguments.of(
                CompoundDocumentTypeVariant.STAMMVERORDNUNG, // col 4
                List.of(false, false, false, true, // Anschreiben
                    false, false, false, true, // Begründung
                    false, false, false, false, // Regelungstext
                    false, false, false, true)) // Vorblatt
        );
    }

    @BeforeEach
    void init() {
        mockedLeageSession = mockStatic(LeageSession.class);

        Map<String, String> mockUserDetails = new HashMap<>();
        mockUserDetails.put(LeageSession.GID, GID);
        mockUserDetails.put(LeageSession.NAME, NAME);
        mockUserDetails.put(LeageSession.EMAIL, EMAIL);
        when(LeageSession.getUserDatailsFromAuthentication()).thenReturn(mockUserDetails);

        when(session.getUser()).thenReturn(user);
        when(regelungsvorhabenService.getRegelungsvorhabenEditorTriggeredByDocument(any()))
            .thenReturn(RegelungsvorhabenEditorDTO.builder()
                .id(UUID.fromString(rvId))
                .status(VorhabenStatusType.IN_BEARBEITUNG)
                .vorhabenart(RegelungsvorhabenTypType.GESETZ)
                .bearbeitetAm(Instant.now())
                .build());
        when(nutzerEntityRepository.findByGid(any())).thenReturn(Optional.of(NutzerEntity.builder().gid("xxxx").build()));
    }

    @AfterEach
    public void tearDown() {
        mockedLeageSession.close();  // Make sure to close the mock to avoid conflict
    }

    @ParameterizedTest
    @MethodSource("testParams")
    void testCreateCompoundDocument(CompoundDocumentTypeVariant compoundDocumentType, List<Boolean> answers) {
        // Vor Überprüfung der Testdaten
        assertEquals(16, docTypes.size());
        assertEquals(DocumentType.ANSCHREIBEN_MANTELGESETZ, docTypes.first());
        assertEquals(DocumentType.VORBLATT_STAMMVERORDNUNG, docTypes.last());

        // *** Make new compound document ***

        List<ServiceState> status = new ArrayList<>();
        CreateCompoundDocumentDTO createCompoundDocumentDTO = CreateCompoundDocumentDTO.builder()
            .regelungsVorhabenId(rvId)
            .title(Utils.getRandomString())
            .type(compoundDocumentType)
            .titleRegulatoryText(Utils.getRandomString())
            .initialNumberOfLevels(0)
            .build();

        // Act
        CompoundDocumentDTO compoundDocumentDTO =
            compoundDocumentService.createCompoundDocument(createCompoundDocumentDTO, status);

        // Assert
        assertEquals(ServiceState.OK, status.get(0));
        assertNotNull(compoundDocumentDTO);
        assertNotNull(compoundDocumentDTO.getId());
        assertEquals(createCompoundDocumentDTO.getType(), compoundDocumentDTO.getType());
        assertEquals(1, compoundDocumentDTO.getDocuments().size());
        checkDefaultDocumentFeatures(compoundDocumentType, compoundDocumentDTO.getDocuments().get(0));

        // *** Add attachment to compound document ***

        // Arrange
        status.clear();
        CompoundDocumentId compoundDocumentId = new CompoundDocumentId(compoundDocumentDTO.getId());

        // Act
        CompoundDocumentDTO compoundDocumentDTO2 =
            compoundDocumentService.addDocument(
                compoundDocumentId,
                auxiliaryService.makeDocument(compoundDocumentId, getCreateDocumentDTO(DocumentType.ANLAGE)),
                status
            );

        // Assert
        assertEquals(ServiceState.OK, status.get(0));
        assertNotNull(compoundDocumentDTO2);
        assertEquals(2, compoundDocumentDTO2.getDocuments().size());
        checkAttachmentFeatures(compoundDocumentType, compoundDocumentDTO2.getDocuments().get(1));

        // *** Add other documents to compound document ***

        AtomicInteger index = new AtomicInteger();
        status.clear();

        docTypes.forEach(type -> assertEquals(
            !answers.get(index.getAndIncrement()),
            Utils.isMissing(
                compoundDocumentService.addDocument(
                    compoundDocumentId,
                    auxiliaryService.makeDocument(compoundDocumentId, getCreateDocumentDTO(type)),
                    status))
        ));
    }

    private CreateDocumentDTO getCreateDocumentDTO(DocumentType documentType) {
        return CreateDocumentDTO.builder()
            .title(Utils.getRandomString())
            .regelungsvorhabenId(UUID.fromString(rvId))
            .type(documentType)
            .initialNumberOfLevels(0)
            .build();
    }

    @SneakyThrows
    private void checkDefaultDocumentFeatures(CompoundDocumentTypeVariant compoundDocumentType, DocumentDTO documentDTO) {
        assertNotNull(documentDTO);
        assertEquals(CompoundDocumentService.getDocumentType(compoundDocumentType), documentDTO.getType());
        assertFalse(Utils.isMissing(documentDTO.getContent()));

        String content = Utils.unmask(documentDTO.getContent());
        JSONObject dokument = (JSONObject) ((JSONArray) jsonParser.parse(content)).get(0);
        JSONObject proprietaryMetadata = getDescendant(dokument, false,
            "*", "akn:meta", "akn:proprietary", "meta:legalDocML.de_metadaten");
        assertNotNull(proprietaryMetadata);
        checkMetaTyp(proprietaryMetadata, compoundDocumentType);
        checkMetaForm(proprietaryMetadata, compoundDocumentType);
        checkMetaFassung(proprietaryMetadata, compoundDocumentType);
        checkMetaArt(proprietaryMetadata, RechtsetzungsdokumentArt.REGELUNGSTEXT);
        checkMetaInitiant(proprietaryMetadata, compoundDocumentType);
        checkMetaInstitution(proprietaryMetadata, compoundDocumentType);
    }

    @SneakyThrows
    private void checkAttachmentFeatures(CompoundDocumentTypeVariant compoundDocumentType, DocumentDTO documentDTO) {
        assertNotNull(documentDTO);
        assertEquals(DocumentType.ANLAGE, documentDTO.getType());
        assertFalse(Utils.isMissing(documentDTO.getContent()));

        String content = Utils.unmask(documentDTO.getContent());
        JSONObject dokument = (JSONObject) ((JSONArray) jsonParser.parse(content)).get(0);
        JSONObject proprietaryMetadata = getDescendant(dokument, false,
            "*", "akn:meta", "akn:proprietary", "meta:legalDocML.de_metadaten");
        assertNotNull(proprietaryMetadata);
        checkMetaTyp(proprietaryMetadata, compoundDocumentType);
        checkMetaForm(proprietaryMetadata, compoundDocumentType);
        checkMetaFassung(proprietaryMetadata, compoundDocumentType);
        checkMetaArt(proprietaryMetadata, RechtsetzungsdokumentArt.OFFENE_STRUKTUR);
        checkMetaInitiant(proprietaryMetadata, compoundDocumentType);
        checkMetaInstitution(proprietaryMetadata, compoundDocumentType);
    }

    private void checkMetaTyp(JSONObject proprietaryMetadata, CompoundDocumentTypeVariant compoundDocumentType) {
        RechtsetzungsdokumentTyp actualTyp =
            RechtsetzungsdokumentTyp.fromLiteral(getText(findChild(proprietaryMetadata, META_TYP.getLiteral())));

        RechtsetzungsdokumentTyp expectedTyp;
        switch (compoundDocumentType) {
            case AENDERUNGSVERORDNUNG:
            case STAMMVERORDNUNG:
                expectedTyp = RechtsetzungsdokumentTyp.VERORDNUNG;
                break;
            case MANTELGESETZ:
            case STAMMGESETZ:
                expectedTyp = RechtsetzungsdokumentTyp.GESETZ;
                break;
            default:
                expectedTyp = null;
        }

        assertEquals(expectedTyp, actualTyp);
    }

    private void checkMetaForm(JSONObject proprietaryMetadata, CompoundDocumentTypeVariant compoundDocumentType) {
        RechtsetzungsdokumentForm actualForm =
            RechtsetzungsdokumentForm.fromLiteral(getText(findChild(proprietaryMetadata, META_FORM.getLiteral())));

        RechtsetzungsdokumentForm expectedForm;
        switch (compoundDocumentType) {
            case AENDERUNGSVERORDNUNG:
            case MANTELGESETZ:
                expectedForm = RechtsetzungsdokumentForm.MANTELFORM;
                break;
            case STAMMGESETZ:
            case STAMMVERORDNUNG:
                expectedForm = RechtsetzungsdokumentForm.STAMMFORM;
                break;
            default:
                expectedForm = null;
        }

        assertEquals(expectedForm, actualForm);
    }

    private void checkMetaFassung(JSONObject proprietaryMetadata, CompoundDocumentTypeVariant compoundDocumentType) {
        RechtsetzungsdokumentFassung actualFassung =
            RechtsetzungsdokumentFassung.fromLiteral(
                getText(findChild(proprietaryMetadata, META_FASSUNG.getLiteral())));

        RechtsetzungsdokumentFassung expectedFassung = RechtsetzungsdokumentFassung.ENTWURFSFASSUNG;

        assertEquals(expectedFassung, actualFassung);
    }

    private void checkMetaArt(JSONObject proprietaryMetadata, RechtsetzungsdokumentArt expectedArt) {
        RechtsetzungsdokumentArt actualArt =
            RechtsetzungsdokumentArt.fromLiteral(getText(findChild(proprietaryMetadata, META_ART.getLiteral())));

        assertEquals(expectedArt, actualArt);
    }

    private void checkMetaInitiant(JSONObject proprietaryMetadata, CompoundDocumentTypeVariant compoundDocumentType) {
        RechtsetzungsdokumentInitiant actualInitiant =
            RechtsetzungsdokumentInitiant.fromLiteral(getText(findChild(proprietaryMetadata,
                META_INITIANT.getLiteral())));

        RechtsetzungsdokumentInitiant expectedInitiant;
        switch (compoundDocumentType) {
            case AENDERUNGSVERORDNUNG:
            case STAMMVERORDNUNG:
            case MANTELGESETZ:
            case STAMMGESETZ:
                expectedInitiant = RechtsetzungsdokumentInitiant.BUNDESREGIERUNG;
                break;
            default:
                expectedInitiant = null;
        }

        assertEquals(expectedInitiant, actualInitiant);
    }

    private void checkMetaInstitution(JSONObject proprietaryMetadata,
        CompoundDocumentTypeVariant compoundDocumentType) {
        RechtsetzungsdokumentBearbeitendeInstitution actualInstitution =
            RechtsetzungsdokumentBearbeitendeInstitution.fromLiteral(getText(findChild(proprietaryMetadata,
                META_BEARBEITENDE_INSTITUTION.getLiteral())));

        RechtsetzungsdokumentBearbeitendeInstitution expectedInstitution;
        switch (compoundDocumentType) {
            case AENDERUNGSVERORDNUNG:
            case STAMMVERORDNUNG:
            case MANTELGESETZ:
            case STAMMGESETZ:
                expectedInstitution = RechtsetzungsdokumentBearbeitendeInstitution.BUNDESREGIERUNG;
                break;
            default:
                expectedInstitution = null;
        }

        assertEquals(expectedInstitution, actualInstitution);
    }

}
