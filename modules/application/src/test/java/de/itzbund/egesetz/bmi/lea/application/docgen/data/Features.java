// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.docgen.data;

import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.util.List;

@Data
@Builder
public class Features {

    private DocumentType documentType;

    @Singular("featureSpec")
    private List<FeatureSpec> featureSpecList;
}
