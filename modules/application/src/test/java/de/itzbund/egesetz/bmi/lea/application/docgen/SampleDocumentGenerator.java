// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.docgen;

import de.itzbund.egesetz.bmi.lea.application.docgen.data.Absatz;
import de.itzbund.egesetz.bmi.lea.application.docgen.data.AbsatzAuswahl;
import de.itzbund.egesetz.bmi.lea.application.docgen.data.AbsatzUntergliederung;
import de.itzbund.egesetz.bmi.lea.application.docgen.data.BegruendungFeatureSpec;
import de.itzbund.egesetz.bmi.lea.application.docgen.data.FeatureSpec;
import de.itzbund.egesetz.bmi.lea.application.docgen.data.Features;
import de.itzbund.egesetz.bmi.lea.application.docgen.data.Gliederung;
import de.itzbund.egesetz.bmi.lea.application.docgen.data.Gliederungsebene;
import de.itzbund.egesetz.bmi.lea.application.docgen.data.Inhaltselement;
import de.itzbund.egesetz.bmi.lea.application.docgen.data.ListenAbsatz;
import de.itzbund.egesetz.bmi.lea.application.docgen.data.Praeambel;
import de.itzbund.egesetz.bmi.lea.application.docgen.data.TableElement;
import de.itzbund.egesetz.bmi.lea.application.docgen.data.TextElement;
import de.itzbund.egesetz.bmi.lea.application.docgen.data.TextuellerAbsatz;
import de.itzbund.egesetz.bmi.lea.core.json.TraversableJSONDocument;
import de.itzbund.egesetz.bmi.lea.core.util.Utils;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.BegruendungsteilAbschnittRefersToLiteral;
import de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.BegruendungsteilRefersToLiteral;
import de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.EinzelvorschriftRefersToLiteral;
import de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.InhaltsabschnittRefersToLiteral;
import de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.RefersToLiteral;
import de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.VorblattabschnittRefersToLiteral;
import de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.ConverterRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.EIdGeneratorRestPort;
import de.itzbund.egesetz.bmi.lea.domain.ports.serviceports.rest.TemplateRestPort;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_GUID;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_HREF;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_NAME;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_NAME_VALUE_DEFAULT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ATTR_REFERSTO;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ARTICLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BILL;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_BODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CONCLUSIONS;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_CONTENT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_DOC;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_HCONTAINER;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_HEADING;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_INTRO;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_LIST;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_MAINBODY;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_NUM;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_P;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PARAGRAPH;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_POINT;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_PREAMBLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_REF;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TABLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TBLOCK;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TD;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TH;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_TR;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_WRAPUP;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addAttributes;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.addText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.clearChildrenArray;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.findChild;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDescendant;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getDocumentObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getGuid;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getLocalName;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getStringAttribute;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.getType;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.isEffectivelyEmpty;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewDefaultJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewDefaultJSONObjectWithText;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewJSONObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNewTextNode;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.makeNumObject;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.replaceChildren;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.toJSONArray;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.withDefaultPrefix;
import static de.itzbund.egesetz.bmi.lea.core.json.JSONUtils.wrapUp;
import static de.itzbund.egesetz.bmi.lea.core.util.Utils.replaceItemBy;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSTEXT;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.BegruendungsteilRefersToLiteral.BEGRUENDUNG_ALLGEMEINER_TEIL;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.BegruendungsteilRefersToLiteral.BEGRUENDUNG_BESONDERER_TEIL;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.VorblattabschnittRefersToLiteral.ERFUELLUNGSAUFWAND_FUER_DIE_WIRTSCHAFT;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.VorblattabschnittRefersToLiteral.VORBLATTABSCHNITT_ERFUELLUNGSAUFWAND;
import static de.itzbund.egesetz.bmi.lea.domain.enums.rechtsetzungsdokument.metadaten.RechtsetzungsdokumentArt.REGELUNGSTEXT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

@Disabled("Not a real test, just a tool")
@SuppressWarnings({"unused", "NewClassNamingConvention"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Log4j2
class SampleDocumentGenerator {

    private static final Set<String> EXCLUDE_TYPES = Stream.of(ELEM_NUM, ELEM_HEADING, ELEM_P)
        .collect(Collectors.toCollection(HashSet::new));

    private static final DateFormat SDF = new SimpleDateFormat("yy-MM-dd_HH-mm-ss");

    @LocalServerPort
    private int port;

    @Autowired
    private TemplateRestPort templateRestPort;
    @Autowired
    private ConverterRestPort converterRestPort;

    @Autowired
    private EIdGeneratorRestPort eIdGeneratorRestPort;

    private static final Map<Integer, String> SAMPLE_CONTENT;

    static {
        SAMPLE_CONTENT = new HashMap<>();
        makeSampleContent();
    }

    private final RandomValueGenerator<String> tableHeaderGenerator = new RandomTableHeaderGenerator();
    private final RandomValueGenerator<BigDecimal> tableValueGenerator = new RandomTableDataValueGenerator(2, RoundingMode.HALF_UP);

    private AtomicInteger docCount;


    @BeforeEach
    void init() {
        docCount = new AtomicInteger();
    }


    @ParameterizedTest
    @MethodSource("de.itzbund.egesetz.bmi.lea.application.docgen.data.SampleDocumentGeneratorVorblattData#vorblattSpecs")
    void generateGrossesVorblatt(Features features) {
        processFeatureSet(features, 10);
    }


    void processVorblattFeatures(FeatureSpec featureSpec, Map<RefersToLiteral, JSONObject> containers) {
        JSONObject container = containers.get(featureSpec.getRefersTo());
        assertNotNull(container);
        JSONArray containerChildren = getChildren(container);
        assertNotNull(containerChildren);
        assertTrue(containerChildren.size() > 1);
        VorblattabschnittRefersToLiteral refersToLiteral = (VorblattabschnittRefersToLiteral) featureSpec.getRefersTo();

        switch (refersToLiteral) {
            case DAVON_BUEROKRATIEKOSTEN_AUS_INFORMATIONSPFLICHTEN:
                insertContent(container, 1, featureSpec.getTexts(), featureSpec.getTables());
                break;
            case ERFUELLUNGSAUFWAND_FUER_BUERGERINNEN_UND_BUERGER:
            case ERFUELLUNGSAUFWAND_FUER_DIE_WIRTSCHAFT:
            case ERFUELLUNGSAUFWAND_DER_VERWALTUNG:
                insertContent(container, 2, featureSpec.getTexts(), featureSpec.getTables());
                break;
            default:
                JSONObject contentObject = (JSONObject) containerChildren.get(containerChildren.size() - 1);
                assertEquals(ELEM_CONTENT, getLocalName(getType(contentObject)));
                insertContent(contentObject, 0, featureSpec.getTexts(), featureSpec.getTables());
        }
    }


    @ParameterizedTest
    @MethodSource("de.itzbund.egesetz.bmi.lea.application.docgen.data.SampleDocumentGeneratorBegruendungData#begruendungSpecs")
    void generateGrosseBegruendung(Features features) {
        processFeatureSet(features, 16);
    }


    void processBegruendungFeatures(FeatureSpec featureSpec, Map<RefersToLiteral, JSONObject> containers) {
        JSONObject container = containers.get(featureSpec.getRefersTo());
        assertNotNull(container);
        JSONArray containerChildren = getChildren(container);
        assertNotNull(containerChildren);
        assertTrue(containerChildren.size() > 1);
        RefersToLiteral refersToLiteral = featureSpec.getRefersTo();

        if (refersToLiteral instanceof BegruendungsteilAbschnittRefersToLiteral) {
            BegruendungsteilAbschnittRefersToLiteral literal = (BegruendungsteilAbschnittRefersToLiteral) refersToLiteral;
            if (literal == BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSTEXT) {
                JSONObject parentContainer = containers.get(BEGRUENDUNG_BESONDERER_TEIL);
                assertInstanceOf(BegruendungFeatureSpec.class, featureSpec);
                insertBegruendungsteilAbschnitte(parentContainer, (BegruendungFeatureSpec) featureSpec);
            } else {
                JSONObject contentObject = (JSONObject) containerChildren.get(containerChildren.size() - 1);
                assertEquals(ELEM_CONTENT, getLocalName(getType(contentObject)));
                insertContent(contentObject, 0, featureSpec.getTexts(), featureSpec.getTables());
            }

        } else if (refersToLiteral instanceof InhaltsabschnittRefersToLiteral) {
            insertContent(container, 2, featureSpec.getTexts(), featureSpec.getTables());
        }
    }


    @ParameterizedTest
    @MethodSource("de.itzbund.egesetz.bmi.lea.application.docgen.data.SampleDocumentGeneratorRegelungstextData#regelungstextSpecs")
    @SuppressWarnings("unchecked")
    void generateGrossenRegelungstext(Gliederung gliederung) {
        DocumentType documentType = DocumentType.REGELUNGSTEXT_STAMMGESETZ;
        RechtsetzungsdokumentArt dokumentArt = documentType.getArt();
        JSONObject template = getTemplate(documentType);
        Praeambel praeambel = gliederung.getPraeambel();

        // get preamble Element
        JSONObject preambleObject = getDescendant(template, true, ELEM_DOC, ELEM_PREAMBLE);

        // optionally make preamble
        if (praeambel != null) {
            generatePreambleContent(preambleObject, praeambel);
        }

        // get body Element
        JSONObject bodyObject = getDescendant(template, true, ELEM_BILL, ELEM_BODY);
        assertNotNull(bodyObject);
        generateBodyContent(bodyObject, gliederung.getGliederungsebenen());

        // handle special case of last Einzelvorschrift
        TraversableJSONDocument traversableJSONDocument = new TraversableJSONDocument(template);
        JSONObject lastNorm = traversableJSONDocument.getLastOfType(ELEM_ARTICLE);
        assertNotNull(lastNorm);
        lastNorm.put(ATTR_REFERSTO, EinzelvorschriftRefersToLiteral.EINZELVORSCHRIFT_GELTUNGSZEITREGEL.getLiteral());

        // cleanup side effects of TraversableJSONDocument
        traversableJSONDocument.getLastOfType(ELEM_BILL).remove(ATTR_GUID);
        bodyObject.remove(ATTR_GUID);
        traversableJSONDocument.getLastOfType(ELEM_PREAMBLE).remove(ATTR_GUID);
        traversableJSONDocument.getLastOfType(ELEM_CONCLUSIONS).remove(ATTR_GUID);

        // optionally make toc
        if (gliederung.isInhaltsuebersicht()) {
            insertTableOfContents(preambleObject);
        }

        // make docs
        checkAndPersistDocs(template, dokumentArt);
    }


    private void generatePreambleContent(JSONObject preambleObject, Praeambel praeambel) {
        // not yet implemented
    }


    private void insertTableOfContents(JSONObject preambleObject) {
        // not yet implemented
    }


    private void generateBodyContent(JSONObject bodyObject, List<Gliederungsebene> gliederungsebenen) {
        clearChildrenArray(bodyObject);
        JSONArray bodyChildren = getChildren(bodyObject);
        assertTrue(bodyChildren.isEmpty());
        AtomicInteger paraCount = new AtomicInteger();
        IntStream.range(0, gliederungsebenen.size())
            .forEach(i -> makeGliederungsebene(gliederungsebenen.get(i), bodyChildren, paraCount, i + 1));
    }


    @SuppressWarnings("unchecked")
    private void makeGliederungsebene(Gliederungsebene gliederungsebene, JSONArray jsonArray, AtomicInteger paraCount, int num) {
        JSONObject hierarchyObject = makeNewDefaultJSONObject(gliederungsebene.getType());
        insertNumHeading(hierarchyObject, gliederungsebene.getType(), num);
        JSONArray hierarchyChildren = getChildren(hierarchyObject);

        // process Einzelvorschriften
        List<AbsatzAuswahl> einzelvorschriften = gliederungsebene.getEinzelvorschriften();
        if (!isEffectivelyEmpty(einzelvorschriften)) {
            einzelvorschriften.forEach(ev -> makeEinzelvorschrift(ev, hierarchyChildren, paraCount.incrementAndGet()));
        }

        // process Untergliederungen
        List<Gliederungsebene> untergliederungen = gliederungsebene.getUntergliederungen();
        if (!isEffectivelyEmpty(untergliederungen)) {
            IntStream.range(0, untergliederungen.size())
                .forEach(i -> makeGliederungsebene(untergliederungen.get(i), hierarchyChildren, paraCount, i + 1));
        }

        // add this to children array
        jsonArray.add(hierarchyObject);
    }


    @SuppressWarnings("unchecked")
    private void makeEinzelvorschrift(AbsatzAuswahl absatzAuswahl, JSONArray jsonArray, int paraCount) {
        JSONObject einzelvorschrift = makeNewDefaultJSONObject(ELEM_ARTICLE);
        addAttribute(einzelvorschrift, Pair.of(ATTR_REFERSTO, EinzelvorschriftRefersToLiteral.EINZELVORSCHRIFT_STAMMFORM.getLiteral()));
        insertNumHeading(einzelvorschrift, ELEM_ARTICLE, paraCount);
        JSONArray einzelvorschriftChildren = getChildren(einzelvorschrift);

        List<Absatz> absaetze = absatzAuswahl.getAbsaetze();
        int absatzCount = absaetze.size();
        IntStream.range(0, absatzCount)
            .forEach(i -> {
                Absatz absatz = absaetze.get(i);
                JSONObject absatzObject = makeNewDefaultJSONObject(ELEM_PARAGRAPH);
                Pair<String, String> numText = getNumText(i + 1, 0, absatzCount);
                addChildren(absatzObject, makeNumObject(numText.getKey(), numText.getValue()));
                einzelvorschriftChildren.add(absatzObject);

                if (absatz instanceof TextuellerAbsatz) {
                    makeTextuellerAbsatz((TextuellerAbsatz) absatz, absatzObject);
                } else {
                    makeListenAbsatz((ListenAbsatz) absatz, absatzObject);
                }
            });

        jsonArray.add(einzelvorschrift);
    }


    @SuppressWarnings("unchecked")
    private void makeTextuellerAbsatz(TextuellerAbsatz absatz, JSONObject absatzObject) {
        JSONObject contentObject = makeNewDefaultJSONObject(ELEM_CONTENT);
        JSONArray contentChildren = getChildren(contentObject, true);
        List<Inhaltselement> inhaltselemente = absatz.getInhaltselemente();
        if (!isEffectivelyEmpty(inhaltselemente)) {
            inhaltselemente.forEach(inhaltselement -> {
                if (inhaltselement instanceof TextElement) {
                    contentChildren.add(getTextObject(((TextElement) inhaltselement).getTextKey()));
                } else {
                    TableElement tableElement = (TableElement) inhaltselement;
                    contentChildren.add(getTableObject(Pair.of(tableElement.getAnzahlZeilen(), tableElement.getAnzahlSpalten())));
                }
            });
        }

        addChildren(absatzObject, contentObject);
    }


    @SuppressWarnings("unchecked")
    private void makeListenAbsatz(ListenAbsatz absatz, JSONObject absatzObject) {
        absatz.getAbsatzUntergliederungen().forEach(untergliederung -> {
            JSONArray absatzChildren = getChildren(absatzObject);
            assertFalse(isEffectivelyEmpty(absatzChildren));
            makeUntergliederung(untergliederung, absatzChildren, 1);
        });
    }


    @SuppressWarnings("unchecked")
    private void makeUntergliederung(AbsatzUntergliederung untergliederung, JSONArray jsonArray, int depth) {
        JSONObject listObject = makeNewDefaultJSONObject(ELEM_LIST);

        // make intro part
        JSONObject introObject = makeNewDefaultJSONObject(ELEM_INTRO);
        addChildren(introObject, toJSONArray(untergliederung.getTextKeysVorUntergliederung().stream()
            .map(this::getTextObject)
            .collect(Collectors.toList())));
        addChildren(listObject, introObject);

        // make list items
        List<AbsatzAuswahl> untergliederungen = untergliederung.getElementeUntergliederung();
        int numItems = untergliederungen.size();
        IntStream.range(0, numItems).forEach(i -> {
            AbsatzAuswahl absatzAuswahl = untergliederungen.get(i);
            JSONObject pointObject = makeNewDefaultJSONObject(ELEM_POINT);
            Pair<String, String> numText = getNumText(i + 1, depth, numItems);
            addChildren(pointObject, makeNumObject(numText.getKey(), numText.getValue()));

            List<Absatz> absaetze = absatzAuswahl.getAbsaetze();
            absaetze.forEach(absatz -> {
                if (absatz instanceof TextuellerAbsatz) {
                    makeTextuellerAbsatz((TextuellerAbsatz) absatz, pointObject);
                } else {
                    List<AbsatzUntergliederung> pointUntergliederungen = ((ListenAbsatz) absatz).getAbsatzUntergliederungen();
                    JSONArray pointChildren = getChildren(pointObject);
                    pointUntergliederungen.forEach(pointUntergliederung -> makeUntergliederung(pointUntergliederung, pointChildren, depth + 1));
                }
            });

            addChildren(listObject, pointObject);
        });

        // optionally make wrapUp
        Integer textKey = untergliederung.getTextAbsatzNachUntergliederung();
        if (textKey != null) {
            JSONObject wrapUpObject = makeNewDefaultJSONObject(ELEM_WRAPUP);
            JSONObject pObject = getTextObject(textKey);
            addChildren(wrapUpObject, pObject);
            addChildren(listObject, wrapUpObject);
        }

        // add list object to absatz
        jsonArray.add(listObject);
    }


    private void insertNumHeading(JSONObject hierarchyObject, String type, int num) {
        addChildren(hierarchyObject,
            makeNumObject(String.valueOf(num), FeatureSpec.getLabel(type, num)),
            makeNewDefaultJSONObjectWithText(ELEM_HEADING, tableHeaderGenerator.getNext())
        );
    }


    private String getMarker(int num, int depth, int absatzCount) {
        if (absatzCount == 1 && depth == 0) {
            return "1";
        } else if (depth == 0) {
            return String.valueOf(num);
        } else {
            return Character.valueOf((char) (96 + num % 26)).toString().repeat(depth);
        }
    }

    @Test
    void testGetMarker() {
        assertEquals("1", getMarker(1, 0, 1));
        assertEquals("a", getMarker(1, 1, 1));
        assertEquals("aa", getMarker(1, 2, 1));
        assertEquals("aaa", getMarker(1, 3, 1));

        assertEquals("2", getMarker(2, 0, 2));
        assertEquals("b", getMarker(2, 1, 2));
        assertEquals("bb", getMarker(2, 2, 2));
        assertEquals("bbb", getMarker(2, 3, 2));
    }


    private Pair<String, String> getNumText(int num, int depth, int absatzCount) {
        String marker = getMarker(num, depth, absatzCount);
        String text = depth == 0 ? String.format("(%s)", marker) : String.format("%s)", marker);
        return Pair.of(marker, text);
    }


    void processFeatureSet(Features features, int expectedContainerCount) {
        DocumentType documentType = features.getDocumentType();
        RechtsetzungsdokumentArt dokumentArt = documentType.getArt();

        JSONObject template = getTemplate(documentType);
        Map<RefersToLiteral, JSONObject> containers = getContainers(template, documentType, expectedContainerCount);

        features.getFeatureSpecList().forEach(spec -> {
            switch (dokumentArt) {
                case VORBLATT:
                    processVorblattFeatures(spec, containers);
                    break;
                case BEGRUENDUNG:
                    processBegruendungFeatures(spec, containers);
            }
        });

        checkAndPersistDocs(template, dokumentArt);
    }


    @SneakyThrows
    private void checkAndPersistDocs(JSONObject template, RechtsetzungsdokumentArt dokumentArt) {
        List<UUID> allGuids = new ArrayList<>();
        findAllGuids(template, allGuids);
        Set<UUID> uniqueGuids = new HashSet<>(allGuids);
        assertEquals(allGuids.size(), uniqueGuids.size());

        template = eIdGeneratorRestPort.erzeugeEIdsFuerDokument(template);
        String xmlContent = converterRestPort.convertJsonToXml(template);
        assertNotNull(xmlContent);

        storeGeneratedDoc(
            getFilenameWOExt(dokumentArt),
            wrapUp(template).toJSONString(),
            xmlContent
        );
    }


    private JSONObject getTemplate(DocumentType documentType) {
        String templateString = templateRestPort.loadTemplate(documentType, 0);
        // because H2 doesn't work with escaping but template content is written for MySQL
        templateString = templateString.replace("\\/", "/");
        JSONObject template = getDocumentObject(templateString);
        assertNotNull(template);
        return template;
    }


    private Map<RefersToLiteral, JSONObject> getContainers(JSONObject template, DocumentType documentType, int expectedSize) {
        Map<RefersToLiteral, JSONObject> containers = new HashMap<>();
        JSONObject bodyObject = getDescendant(template, true, ELEM_DOC,
            documentType.getArt() == REGELUNGSTEXT ? ELEM_BODY : ELEM_MAINBODY);
        assertNotNull(bodyObject);
        findContainers(bodyObject, documentType, containers);
        assertEquals(expectedSize, containers.size());
        return containers;
    }


    @SuppressWarnings("unchecked")
    private void insertBegruendungsteilAbschnitte(JSONObject parentContainer, BegruendungFeatureSpec featureSpec) {
        List<Pair<String, Integer>> hierarchy = featureSpec.getHierarchy();
        assertNotNull(hierarchy);
        assertFalse(hierarchy.isEmpty());
        List<Integer> textKeys = featureSpec.getTexts();
        List<Pair<Integer, Integer>> tableSpecs = featureSpec.getTables();
        AtomicInteger paraCount = new AtomicInteger();

        assertFalse(isEffectivelyEmpty(textKeys) && isEffectivelyEmpty(tableSpecs));
        List<Object> contentObjects = new ArrayList<>();
        if (!isEffectivelyEmpty(textKeys)) {
            contentObjects.addAll(textKeys);
        }
        if (!isEffectivelyEmpty(tableSpecs)) {
            contentObjects.addAll(tableSpecs);
        }
        Collections.shuffle(contentObjects);

        List<Integer> higherLevelTexts = getHigherLevelTexts(hierarchy, textKeys);
        JSONArray children = getChildren(parentContainer);
        assertFalse(isEffectivelyEmpty(children));
        children.remove(children.size() - 1);

        for (int i = 0; i < hierarchy.get(0).getValue(); i++) {
            insertBegruendungsteilAbschnitt(parentContainer, hierarchy, contentObjects, higherLevelTexts, i + 1, paraCount);
        }
    }


    @SuppressWarnings("unchecked")
    private void insertBegruendungsteilAbschnitt(JSONObject parentContainer, List<Pair<String, Integer>> hierarchy,
        List<Object> contentObjects, List<Integer> higherLevelTexts, int num, AtomicInteger paraCount) {

        JSONObject containerObject = makeNewDefaultJSONObject(ELEM_HCONTAINER);
        addAttributes(containerObject,
            Pair.of(ATTR_REFERSTO, BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSTEXT.getLiteral()),
            Pair.of(ATTR_NAME, ATTR_NAME_VALUE_DEFAULT));
        JSONObject headingObject = makeHeading(hierarchy.get(0).getLeft(), num);
        JSONObject contentObject = makeNewDefaultJSONObject(ELEM_CONTENT);
        addChildren(containerObject, headingObject, contentObject);

        if (hierarchy.size() == 1) {
            addChildren(contentObject, makeContentChildren(contentObjects));
        } else {
            addChildren(contentObject, getTextObject(higherLevelTexts.get(0)));
            makeDeeperLevels(contentObject, hierarchy, contentObjects, higherLevelTexts, 1, paraCount);
        }

        addChildren(parentContainer, containerObject);
    }


    private void makeDeeperLevels(JSONObject contentObject, List<Pair<String, Integer>> hierarchy,
        List<Object> contentObjects, List<Integer> higherLevelTexts, int depth, AtomicInteger paraCount) {

        Pair<String, Integer> currentLevel = hierarchy.get(depth);
        String ldmlName = currentLevel.getLeft();
        int count = currentLevel.getRight();

        for (int i = 0; i < count; i++) {
            int num = ELEM_ARTICLE.equals(ldmlName) ? paraCount.incrementAndGet() : i + 1;
            JSONObject tblockObject = makeNewDefaultJSONObject(ELEM_TBLOCK);
            JSONObject headingObject = makeHeading(ldmlName, num);
            addChildren(tblockObject, headingObject);

            if (depth == hierarchy.size() - 1) {
                addChildren(tblockObject, makeContentChildren(contentObjects));
            } else {
                addChildren(tblockObject, getTextObject(higherLevelTexts.get(depth)));
                makeDeeperLevels(tblockObject, hierarchy, contentObjects, higherLevelTexts, depth + 1, paraCount);
            }

            addChildren(contentObject, tblockObject);
        }
    }


    @SuppressWarnings("unchecked")
    private JSONArray makeContentChildren(List<Object> contentObjects) {
        JSONArray children = new JSONArray();

        contentObjects.forEach(obj -> {
            if (obj instanceof Integer) {
                Integer textKex = (Integer) obj;
                children.add(getTextObject(textKex));
            } else {
                Pair<Integer, Integer> tableSpec = (Pair<Integer, Integer>) obj;
                children.add(getTableObject(tableSpec));
            }
        });

        return children;
    }


    private JSONObject makeHeading(String ldmlName, int num) {
        JSONObject headingObject = makeNewDefaultJSONObject(ELEM_HEADING);
        JSONObject refObject = makeNewJSONObject(ELEM_REF, true, false);
        addAttribute(refObject, Pair.of(ATTR_HREF, ""));
        addText(refObject, FeatureSpec.getLabel(ldmlName, num));
        JSONObject text = makeNewTextNode("Zu ");
        addChildren(headingObject, text, refObject);
        return headingObject;
    }


    private List<Integer> getHigherLevelTexts(List<Pair<String, Integer>> hierarchy, List<Integer> textKeys) {
        List<Integer> textKeys2 = new ArrayList<>(isEffectivelyEmpty(textKeys) || textKeys.size() < hierarchy.size() - 1
            ? List.copyOf(SAMPLE_CONTENT.keySet())
            : textKeys);
        Collections.shuffle(textKeys2);

        return textKeys2;
    }


    @SneakyThrows
    private void storeGeneratedDoc(String filename, String jsonContent, String xmlContent) {
        Path destDir = Files.createDirectories(Path.of("target", "docgen"));

        Path jsonFile = destDir.resolve(filename + ".json");
        Files.writeString(jsonFile, jsonContent, StandardCharsets.UTF_8);

        Path xmlFile = destDir.resolve(filename + ".xml");
        Files.writeString(xmlFile, xmlContent, StandardCharsets.UTF_8);
    }


    private String getFilenameWOExt(RechtsetzungsdokumentArt dokumentArt) {
        return String.format("%s_%s-%s", SDF.format(System.currentTimeMillis()), dokumentArt.getLiteral(), docCount.incrementAndGet());
    }


    @SuppressWarnings("unchecked")
    private void insertContent(JSONObject parent, int insertionIndex, List<Integer> textKeys, List<Pair<Integer, Integer>> tableSpecs) {
        JSONArray children = getChildren(parent);
        assertFalse(isEffectivelyEmpty(children));
        JSONArray newChildren = new JSONArray();

        if (isEffectivelyEmpty(textKeys) && !isEffectivelyEmpty(tableSpecs)) {
            newChildren = toJSONArray(replaceItemBy(children, insertionIndex, getOnlyTables(tableSpecs)));
        } else if (!isEffectivelyEmpty(textKeys) && isEffectivelyEmpty(tableSpecs)) {
            newChildren = toJSONArray(replaceItemBy(children, insertionIndex, getOnlyTextContent(textKeys)));
        } else if (!isEffectivelyEmpty(textKeys)) {
            newChildren = toJSONArray(replaceItemBy(children, insertionIndex, getMixedContent(textKeys, tableSpecs)));
        }

        replaceChildren(parent, newChildren);
    }


    private List<JSONObject> getOnlyTextContent(List<Integer> textKeys) {
        return textKeys.stream()
            .map(this::getTextObject)
            .collect(Collectors.toList());
    }


    private JSONObject getTextObject(Integer textKey) {
        return makeNewDefaultJSONObjectWithText(ELEM_P, SAMPLE_CONTENT.get(textKey));
    }


    private List<JSONObject> getOnlyTables(List<Pair<Integer, Integer>> tableSpecs) {
        return tableSpecs.stream()
            .map(this::getTableObject)
            .collect(Collectors.toList());
    }


    /*
     * A tableSpec is a pair of integers naming number of rows and columns resp.
     */
    @SuppressWarnings("unchecked")
    private JSONObject getTableObject(Pair<Integer, Integer> tableSpec) {
        int numRows = tableSpec.getLeft();
        int numCols = tableSpec.getRight();
        List<String> tableHeaders = tableHeaderGenerator.getNext(numCols);

        JSONObject tableObject = makeNewDefaultJSONObject(ELEM_TABLE);
        JSONArray rows = new JSONArray();
        for (int i = 0; i <= numRows; i++) {
            JSONObject row = makeNewDefaultJSONObject(ELEM_TR);
            JSONArray cells = new JSONArray();
            List<BigDecimal> rowValues = tableValueGenerator.getNext(numCols);
            for (int j = 0; j < numCols; j++) {
                JSONObject cell = makeNewDefaultJSONObject(i == 0 ? ELEM_TH : ELEM_TD);
                JSONObject pObject;
                if (i == 0) {
                    pObject = makeNewDefaultJSONObjectWithText(ELEM_P, tableHeaders.get(j));
                } else {
                    pObject = makeNewDefaultJSONObjectWithText(ELEM_P, rowValues.get(j).toString());
                }
                addChildren(cell, pObject);
                cells.add(cell);
            }
            addChildren(row, cells);
            rows.add(row);
        }
        addChildren(tableObject, rows);
        return tableObject;
    }


    private List<JSONObject> getMixedContent(@NonNull List<Integer> textKeys, @NonNull List<Pair<Integer, Integer>> tableSpecs) {
        List<JSONObject> resultList = new ArrayList<>();
        resultList.addAll(getOnlyTextContent(textKeys));
        resultList.addAll(getOnlyTables(tableSpecs));

        Collections.shuffle(resultList);
        return resultList;
    }


    @SuppressWarnings("unchecked")
    private void findContainers(JSONObject jsonObject, DocumentType documentType,
        Map<RefersToLiteral, JSONObject> containers) {
        JSONArray children = getChildren(jsonObject);
        assertNotNull(children);

        children.forEach(obj -> {
            JSONObject container = (JSONObject) obj;
            String type = getLocalName(getType(container));

            if (!EXCLUDE_TYPES.contains(type)) {
                String refersTo = getStringAttribute(container, ATTR_REFERSTO);
                assertFalse(refersTo.isBlank());

                RefersToLiteral literal = null;
                switch (documentType) {
                    case VORBLATT_STAMMGESETZ:
                        literal = VorblattabschnittRefersToLiteral.fromLiteral(refersTo);
                        break;
                    case BEGRUENDUNG_STAMMGESETZ:
                        literal = getBegruendungRelatedLiteral(refersTo);
                        break;
                    case REGELUNGSTEXT_STAMMGESETZ:
                        literal = EinzelvorschriftRefersToLiteral.fromLiteral(refersTo);
                }

                assertNotNull(literal);
                containers.put(literal, container);
                findChildContainers(documentType, container, containers, literal);
            }
        });
    }


    private void findChildContainers(DocumentType documentType, JSONObject parentContainer,
        Map<RefersToLiteral, JSONObject> containers, @NonNull RefersToLiteral parentLiteral) {

        if (parentLiteral == VORBLATTABSCHNITT_ERFUELLUNGSAUFWAND
            || parentLiteral == BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSFOLGEN) {
            findContainers(findChild(parentContainer, withDefaultPrefix(ELEM_CONTENT)), documentType, containers);
        }

        if (parentLiteral == ERFUELLUNGSAUFWAND_FUER_DIE_WIRTSCHAFT
            || parentLiteral == BEGRUENDUNG_ALLGEMEINER_TEIL
            || parentLiteral == BEGRUENDUNG_BESONDERER_TEIL) {
            findContainers(parentContainer, documentType, containers);
        }
    }


    private RefersToLiteral getBegruendungRelatedLiteral(String literal) {
        RefersToLiteral refersToLiteral = BegruendungsteilAbschnittRefersToLiteral.fromLiteral(literal);
        if (refersToLiteral == null) {
            refersToLiteral = BegruendungsteilRefersToLiteral.fromLiteral(literal);
        }
        if (refersToLiteral == null) {
            refersToLiteral = InhaltsabschnittRefersToLiteral.fromLiteral(literal);
        }

        return refersToLiteral;
    }


    @SuppressWarnings("unchecked")
    private void findAllGuids(JSONObject jsonObject, List<UUID> guids) {
        String guidString = getGuid(jsonObject);
        if (!Utils.isMissing(guidString)) {
            guids.add(UUID.fromString(guidString));
        }
        JSONArray children = getChildren(jsonObject);
        if (!isEffectivelyEmpty(children)) {
            children.forEach(obj -> {
                JSONObject childObject = (JSONObject) obj;
                findAllGuids(childObject, guids);
            });
        }
    }


    private static void makeSampleContent() {
        Stream.of(
                arguments(133,
                    "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis."),
                arguments(155,
                    "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua."),
                arguments(395,
                    "Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat."),
                arguments(398,
                    "Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi."),
                arguments(415,
                    "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat."),
                arguments(591,
                    "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."),
                arguments(828,
                    "Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus."),
                arguments(868,
                    "At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, kasd magna no rebum. sanctus sea sed takimata ut vero voluptua. est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat."),
                arguments(887,
                    "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."))
            .forEach(arguments -> {
                Object[] args = arguments.get();
                SAMPLE_CONTENT.put((Integer) args[0], (String) args[1]);
            });
    }

}
