// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application;

import de.itzbund.egesetz.bmi.lea.domain.dtos.regelungsvarhaben.RegelungsvorhabenEditorDTO;
import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentState;
import de.itzbund.egesetz.bmi.lea.domain.enums.VorhabenStatusType;
import de.itzbund.egesetz.bmi.lea.domain.services.PlategRequestService;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

public class BereitFuerBundestagIntegrationTest extends SchnittstellenTests {

    // Die Requests an Plattform werden gemockt
    @MockBean
    private PlategRequestService plategRequestService;

    private final List<RegelungsvorhabenEditorDTO> regelungsvorhabenEditorDTOList = new ArrayList<>();

    @Override
    public void init2() {
        // Die Requests an Plattform werden gemockt
        when(plategRequestService.getRegulatoryProposalsByIdList(anyList()))
            .thenReturn(regelungsvorhabenEditorDTOList);

    }

    private RegelungsvorhabenEditorDTO createBundestagRegelungsvorhaben() {
        RegelungsvorhabenEditorDTO regelungsvorhabenEditorDTO0005 = getRandomRegelungsvorhabenEditorDTO();
        regelungsvorhabenEditorDTO0005.setStatus(VorhabenStatusType.BUNDESTAG);
        return regelungsvorhabenEditorDTO0005;
    }

    private RegelungsvorhabenEditorDTO createBundesregierungRegelungsvorhaben() {
        RegelungsvorhabenEditorDTO randomRegelungsvorhabenEditorDTO = getRandomRegelungsvorhabenEditorDTO();
        randomRegelungsvorhabenEditorDTO.setId(UUID.randomUUID());
        return randomRegelungsvorhabenEditorDTO;
    }

    @Test
    void versucheBereitFuerDenBundestagZuSetzenObwohlDuEsNichtDarfst() {

        // - RV zur Dokumentenmappe im Status "In Bearbeitung"
        regelungsvorhabenEditorDTOList.add(createBundesregierungRegelungsvorhaben());
        when(plategRequestService.getRegulatoryProposalUserCanAccess(any()))
            .thenReturn(regelungsvorhabenEditorDTOList.get(0));
        when(plategRequestService.getRegulatoryProposalById(any()))
            .thenReturn(regelungsvorhabenEditorDTOList.get(0));

        UUID compoundDocumentUuid = UUID.fromString("00000001-0000-0000-000a-000000000001");

        Response response = getResponse(compoundDocumentUuid);

        validatableResponse = response
            .then()
            .log().all()
            .assertThat().statusCode(409);
    }

    @Test
    void setzeBereitFuerDenBundestag() {
        // - RV zur Dokumentenmappe im Status "Bundestag"
        RegelungsvorhabenEditorDTO bundestagRegelungsvorhaben = createBundestagRegelungsvorhaben();
        bundestagRegelungsvorhaben.setId(UUID.fromString("00000000-0000-0000-0000-000000000019"));
        regelungsvorhabenEditorDTOList.add(bundestagRegelungsvorhaben);
        when(plategRequestService.getRegulatoryProposalUserCanAccess(any()))
            .thenReturn(regelungsvorhabenEditorDTOList.get(0));
        when(plategRequestService.getRegulatoryProposalById(any()))
            .thenReturn(regelungsvorhabenEditorDTOList.get(0));

        UUID compoundDocumentUuid = UUID.fromString("00000001-0000-0000-000b-000000000002");

        Response response = getResponse(compoundDocumentUuid);

        validatableResponse = response
            .then()
            .log().all()
            .assertThat().statusCode(200);
    }

    private Response getResponse(UUID compoundDocumentUuid) {

        Response response = given()
            .contentType(ContentType.JSON)
            .body(getPayload())
            .header("Authorization", "Bearer " + jwToken)
            .header("X-XSRF-TOKEN-EDITOR", "xxxxx")
            .header("Cookie", "XSRF-TOKEN=xxxxx; XSRF-TOKEN-EDITOR=xxxxx")
            .when()
            .patch("/compounddocuments/" + compoundDocumentUuid.toString());

        return response;
    }

    @SneakyThrows
    private String getPayload() {

        JSONObject payload = new JSONObject();
        payload.put("status", DocumentState.BEREIT_FUER_BUNDESTAG);
        return payload.toString();

    }
}
