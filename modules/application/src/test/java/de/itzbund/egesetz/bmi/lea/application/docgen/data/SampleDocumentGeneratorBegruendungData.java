// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.docgen.data;

import de.itzbund.egesetz.bmi.lea.domain.enums.DocumentType;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.params.provider.Arguments;

import java.util.List;
import java.util.stream.Stream;

import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_ARTICLE;
import static de.itzbund.egesetz.bmi.lea.core.Constants.ELEM_SECTION;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_ALTERNATIVEN;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_BEFRISTUNG_EVALUIERUNG;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSKOMPETENZ;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSTEXT;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_VEREINBARKEIT;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_WESENTLICHER_INHALT;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.BegruendungsteilAbschnittRefersToLiteral.BEGRUENDUNGSTEIL_ABSCHNITT_ZIELSETZUNG_UND_NOTWENDIGKEIT;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_ERFUELLUNGSAUFWAND;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_GLEICHSTELLUNGSPOLITISCHE_RELEVANZPRUEFUNG;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_NACHHALTIGKEITSASPEKTE;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_RECHTS_UND_VERWALTUNGSVEREINFACHUNG;
import static de.itzbund.egesetz.bmi.lea.domain.enums.ldmlde.InhaltsabschnittRefersToLiteral.REGELUNGSFOLGEN_ABSCHNITT_WEITERE_KOSTEN;
import static org.junit.jupiter.params.provider.Arguments.arguments;

@UtilityClass
@SuppressWarnings("unused")
public class SampleDocumentGeneratorBegruendungData {


    private static Stream<Arguments> begruendungSpecs() {
        return Stream.of(

            // Vorgabe BEG: Test ab ca. 25 Seiten/ 15 mittelgroße Tabellen (5 Spalten/ 10 Zeilen)
            arguments(Features.builder()
                .documentType(DocumentType.BEGRUENDUNG_STAMMGESETZ)
                .featureSpec(FeatureSpec.builder()
                    .refersTo(BEGRUENDUNGSTEIL_ABSCHNITT_ZIELSETZUNG_UND_NOTWENDIGKEIT)
                    .texts(List.of(155, 398))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(BEGRUENDUNGSTEIL_ABSCHNITT_WESENTLICHER_INHALT)
                    .texts(List.of(395, 415, 868, 133))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(BEGRUENDUNGSTEIL_ABSCHNITT_ALTERNATIVEN)
                    .texts(List.of(133, 395))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSKOMPETENZ)
                    .texts(List.of(155))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(BEGRUENDUNGSTEIL_ABSCHNITT_VEREINBARKEIT)
                    .texts(List.of(591))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(REGELUNGSFOLGEN_ABSCHNITT_RECHTS_UND_VERWALTUNGSVEREINFACHUNG)
                    .texts(List.of(155, 133, 155, 133))
                    .tables(List.of(Pair.of(10, 5), Pair.of(10, 5)))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(REGELUNGSFOLGEN_ABSCHNITT_NACHHALTIGKEITSASPEKTE)
                    .texts(List.of(155, 133, 155, 133, 398, 133))
                    .tables(List.of(Pair.of(10, 5), Pair.of(10, 5), Pair.of(10, 5)))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(REGELUNGSFOLGEN_ABSCHNITT_HAUSHALTSAUSGABEN_OHNE_ERFUELLUNGSAUFWAND)
                    .texts(List.of(155, 133, 155, 133, 398, 133))
                    .tables(List.of(Pair.of(10, 5), Pair.of(10, 5), Pair.of(10, 5)))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(REGELUNGSFOLGEN_ABSCHNITT_ERFUELLUNGSAUFWAND)
                    .texts(List.of(155, 133, 155, 133, 398, 133))
                    .tables(List.of(Pair.of(10, 5), Pair.of(10, 5), Pair.of(10, 5)))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(REGELUNGSFOLGEN_ABSCHNITT_WEITERE_KOSTEN)
                    .texts(List.of(155, 133, 155, 133))
                    .tables(List.of(Pair.of(10, 5), Pair.of(10, 5)))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(REGELUNGSFOLGEN_ABSCHNITT_GLEICHSTELLUNGSPOLITISCHE_RELEVANZPRUEFUNG)
                    .texts(List.of(155, 133, 155, 133))
                    .tables(List.of(Pair.of(10, 5), Pair.of(10, 5)))
                    .build())
                .featureSpec(FeatureSpec.builder()
                    .refersTo(BEGRUENDUNGSTEIL_ABSCHNITT_BEFRISTUNG_EVALUIERUNG)
                    .texts(List.of(415))
                    .build())
                .featureSpec(BegruendungFeatureSpec.builder()
                    .refersTo(BEGRUENDUNGSTEIL_ABSCHNITT_REGELUNGSTEXT)
                    .texts(List.of(155, 395, 133))
                    .hierarchyLevel(Pair.of(ELEM_SECTION, 3))
                    .hierarchyLevel(Pair.of(ELEM_ARTICLE, 4))
                    .build())
                .build()));
    }

}
