-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO `compound_document` (`compound_document_id`, `title`, `type`, `version`, `state`, `regelungs_vorhaben_id`, `created_at`, `updated_at`, `inherit_from_id`, `verfahrens_type`, `created_by_id`, `updated_by_id`, `fixed_regelungsvorhaben_referenz_id`)
  VALUES ('00000011-0000-0000-0000-00000000a123', 'DM Test-Bestandsrecht', 'STAMMGESETZ', '1.0', 'BESTANDSRECHT', '00000000-0000-0000-0000-00000000a123', '2024-05-28 11:16:12.529000', '2024-09-24 08:48:48.320424', NULL, 'REGIERUNGSENTWURF', 'user1_id', 'user1_id', NULL);