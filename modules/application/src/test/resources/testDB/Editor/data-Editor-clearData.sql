-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- USE `schema_test`;

SET foreign_key_checks = 0;

DELETE FROM document;
DELETE FROM compound_document;
DELETE FROM drucksachen;
DELETE FROM bestandsrecht;
DELETE FROM rv_bestandsrecht;
DELETE FROM proposition;
DELETE FROM dm_bestandsrecht;

SET foreign_key_checks = 1;
