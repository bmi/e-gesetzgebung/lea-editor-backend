-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO `proposition` (`proposition_id`, `title`, `short_title`, `state`, `proposition_type`, `abbreviation`, `proponent_designation_nominative`, `proponent_designation_genitive`, `proponent_id`, `proponent_title`, `proponent_active`, `initiant`, `created_at`, `farbe`, `referable_id`)
  VALUES ('00000000-0000-0000-0000-000000000004', 'Langtitel', 'Die zweite Europäische Regelwerksverordnung zum Aufstocken der RV-Kennzahlen', 'IN_BEARBEITUNG', 'VERORDNUNG', 'EUArchRV2', 'Bundesministerium der Finanzen', 'Bundesministeriums der Finanzen', '261b6743-1dba-46a7-a072-492915663a61', 'BMF', 1, 'BUNDESREGIERUNG', '2022-09-23 11:49:45.502617', NULL, NULL);

INSERT INTO `proposition` (`proposition_id`, `title`, `short_title`, `state`, `proposition_type`, `abbreviation`, `proponent_designation_nominative`, `proponent_designation_genitive`, `proponent_id`, `proponent_title`, `proponent_active`, `initiant`, `created_at`, `farbe`, `referable_id`)
  VALUES ('00000000-0000-0000-0000-000000000123', 'Langtitel', 'Die zweite Europäische Regelwerksverordnung zum Aufstocken der RV-Kennzahlen', 'IN_BEARBEITUNG', 'VERORDNUNG', 'EUArchRV2', 'Bundesministerium der Finanzen', 'Bundesministeriums der Finanzen', '261b6743-1dba-46a7-a072-492915663a61', 'BMF', 1, 'BUNDESREGIERUNG', '2022-09-23 11:49:45.502617', NULL, NULL);

INSERT INTO `proposition` (`proposition_id`, `title`, `short_title`, `state`, `proposition_type`, `abbreviation`, `proponent_designation_nominative`, `proponent_designation_genitive`, `proponent_id`, `proponent_title`, `proponent_active`, `initiant`, `created_at`, `farbe`, `referable_id`)
  VALUES ('00000000-0000-0000-0000-00000000a123', 'Langtitel', 'Die zweite Europäische Regelwerksverordnung zum Aufstocken der RV-Kennzahlen', 'ARCHIVIERT', 'VERORDNUNG', 'EUArchRV2', 'Bundesministerium der Finanzen', 'Bundesministeriums der Finanzen', '261b6743-1dba-46a7-a072-492915663a61', 'BMF', 1, 'BUNDESREGIERUNG', '2022-09-23 11:49:45.502617', NULL, NULL);
