-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- Beide Dokumentenmappen in DRAFT
INSERT INTO `proposition` (`abbreviation`, `created_at`, `initiant`, `proponent_active`, `proponent_designation_genitive`, `proponent_designation_nominative`, `proponent_id`, `proponent_title`, `proposition_id`, `short_title`, `state`, `title`, `proposition_type`, `farbe`)
   VALUES ('EUArchRV2', '2022-09-23 11:49:45.502617', 'BUNDESREGIERUNG', 1, 'Bundesministeriums der Finanzen', 'Bundesministerium der Finanzen', '261b6743-1dba-46a7-a072-492915663a61', 'BMF', '00000000-0000-0000-0000-000000000005', 'Die zweite Europäische Regelwerksverordnung zum Aufstocken der RV-Kennzahlen', 'IN_BEARBEITUNG', 'Langtitel', 'VERORDNUNG', NULL);

-- Dokumentenmappe mit Status: ZUGESTELLT_BUNDESRAT
INSERT INTO `proposition` (`abbreviation`, `created_at`, `initiant`, `proponent_active`, `proponent_designation_genitive`, `proponent_designation_nominative`, `proponent_id`, `proponent_title`, `proposition_id`, `short_title`, `state`, `title`, `proposition_type`, `farbe`)
   VALUES ('RR Gesetz: Abkürzung', '2022-10-10 17:26:06.205418', 'BUNDESREGIERUNG', 1, 'Bundesministeriums des Innern und für Heimat', 'Bundesministerium des Innern und für Heimat', 'b8aa797e-2cf9-4671-a560-7016960bbf5c', 'BMI', '00000000-0000-0000-0000-000000000020', 'RR Gesetz: Titel', 'IN_BEARBEITUNG', 'RR Gesetz: Langtitel', 'GESETZ', NULL);

-- Dokumentenmappe mit Status: BUNDESTAG
INSERT INTO `proposition` (`abbreviation`, `created_at`, `initiant`, `proponent_active`, `proponent_designation_genitive`, `proponent_designation_nominative`, `proponent_id`, `proponent_title`, `proposition_id`, `short_title`, `state`, `title`, `proposition_type`, `farbe`, `referable_id`)
   VALUES ('Gesetz: Bundestag', '2022-10-10 17:26:06.205418', 'BUNDESREGIERUNG', 1, 'Bundesministeriums des Innern und für Heimat', 'Bundesministerium des Innern und für Heimat', '1daa5b2b-225a-4e68-ace9-23e20669b8a0', 'BMI', '00000000-0000-0000-0000-000000000019', 'RR Gesetz: Titel', 'BUNDESTAG', 'RR Gesetz: Langtitel', 'GESETZ', NULL, '00000001-0000-0000-000b-000000000002');
