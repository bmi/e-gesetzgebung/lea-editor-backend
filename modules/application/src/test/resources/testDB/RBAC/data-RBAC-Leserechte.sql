-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- USE `schema_test2`;

-- ======== LESERECHTE

-- Dokumentenmappe
INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
  VALUES ('user2_id', 'DOKUMENTENMAPPE 00000001-0000-0000-000a-000000000001 2', '00000001-0000-0000-000a-000000000001',
           (SELECT id FROM `ressource_typen` WHERE bezeichnung='DOKUMENTENMAPPE'),
           (SELECT id FROM `rollen` WHERE bezeichnung='LESER'),
	          0,
			  NULL,
			  NULL,
			  'Leserechte Dokumentenmappe');

-- Dokument
INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
  VALUES ('user2_id', 'BEGRÜNDUNG 00000002-0000-0000-000a-000000000002 2', '00000002-0000-0000-000a-000000000002',
           (SELECT id FROM `ressource_typen` WHERE bezeichnung='DOKUMENTE'),
           (SELECT id FROM `rollen` WHERE bezeichnung='LESER'),
	          0,
			  NULL,
			  NULL,
			  'Leserechte Dokument');

INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
  VALUES ('user2_id', 'VORBLATT 00000002-0000-0000-000b-000000000002 2', '00000002-0000-0000-000b-000000000002',
           (SELECT id FROM `ressource_typen` WHERE bezeichnung='DOKUMENTE'),
           (SELECT id FROM `rollen` WHERE bezeichnung='LESER'),
	          0,
			  NULL,
			  NULL,
			  'Leserechte Dokument');

INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
  VALUES ('user2_id', 'DOKUMENT 00000002-0000-0000-000c-000000000002 2', '00000002-0000-0000-000c-000000000002',
           (SELECT id FROM `ressource_typen` WHERE bezeichnung='DOKUMENTE'),
           (SELECT id FROM `rollen` WHERE bezeichnung='LESER'),
	          0,
			  NULL,
			  NULL,
			  'Leserechte Dokument');