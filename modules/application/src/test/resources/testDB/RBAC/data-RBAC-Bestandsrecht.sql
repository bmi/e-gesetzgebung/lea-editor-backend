-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- USE `schema_test2`;

-- ======== BESTANDSRECHT

-- Dokumentenmappe
INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
  VALUES ('user1_id', 'DOKUMENTENMAPPE 00000010-0000-0000-0000-00000000a123', '00000010-0000-0000-0000-00000000a123',
           (SELECT id FROM `ressource_typen` WHERE bezeichnung='DOKUMENTENMAPPE'),
           (SELECT id FROM `rollen` WHERE bezeichnung='ERSTELLER'),
	          0,
			  NULL,
			  NULL,
			  NULL);

-- Dokument
INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
  VALUES ('user1_id', 'REGELUNGSTEXT 00000010-0000-0000-0001-00000000a123', '00000010-0000-0000-0001-00000000a123',
           (SELECT id FROM `ressource_typen` WHERE bezeichnung='DOKUMENTE'),
           (SELECT id FROM `rollen` WHERE bezeichnung='ERSTELLER'),
	          0,
			  NULL,
			  NULL,
			  NULL);

