-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- USE `schema_test2`;

-- ======== Stellvertretung

INSERT INTO `nutzer_stellvertreter` (`nutzer_id`, `stellvertreter_id`)
VALUES ('00000000-0000-0000-0000-000000000002', '00000000-0000-0000-0000-000000000001');
