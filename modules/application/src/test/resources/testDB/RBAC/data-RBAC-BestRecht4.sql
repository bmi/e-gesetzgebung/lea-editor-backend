-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
  VALUES ('user1_id', 'DOKUMENTENMAPPE 00000011-0000-0000-0000-00000000a123', '00000011-0000-0000-0000-00000000a123', 2, 1, 0, NULL, NULL, NULL);
