-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- RVs
-- moved to data-RBAC-RV-Rechte.sql

-- ======== ERSTELLER

-- Dokumentenmappe 1
INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
  VALUES ('user1_id', 'DOKUMENTENMAPPE 00000001-0000-0000-000b-000000000002 1', '00000001-0000-0000-000b-000000000002',
           (SELECT id FROM `ressource_typen` WHERE bezeichnung='DOKUMENTENMAPPE'),
           (SELECT id FROM `rollen` WHERE bezeichnung='ERSTELLER'),
	          0,
			  NULL,
			  NULL,
			  'Ersteller Dokumentenmappe');

-- Dokument 1
INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
  VALUES ('user1_id', 'DOKUMENT 00000002-0000-0000-000d-000000000003 1', '00000002-0000-0000-000d-000000000003',
           (SELECT id FROM `ressource_typen` WHERE bezeichnung='DOKUMENTE'),
           (SELECT id FROM `rollen` WHERE bezeichnung='ERSTELLER'),
	          0,
			  NULL,
			  NULL,
			  'Ersteller Dokument');