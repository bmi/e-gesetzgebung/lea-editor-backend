-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- A
INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
  VALUES ('user3_id', 'REGELUNGSVORHABEN 00000000-0000-0000-0000-000000000004 1', '00000000-0000-0000-0000-000000000004',
            (SELECT id FROM `ressource_typen` WHERE bezeichnung='REGELUNGSVORHABEN'),
            (SELECT id FROM `rollen` WHERE bezeichnung='FEDERFUEHRER'),
            0, NULL, NULL, 'Federführer Regelungsvorhaben');

INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
  VALUES ('user1_id', 'REGELUNGSVORHABEN 00000000-0000-0000-0000-000000000005 1', '00000000-0000-0000-0000-000000000005',
            (SELECT id FROM `ressource_typen` WHERE bezeichnung='REGELUNGSVORHABEN'),
            (SELECT id FROM `rollen` WHERE bezeichnung='FEDERFUEHRER'),
            0, NULL, NULL, 'Federführer Regelungsvorhaben');

INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
  VALUES ('user1_id', 'REGELUNGSVORHABEN 00000000-0000-0000-0000-000000000019 1', '00000000-0000-0000-0000-000000000019',
            (SELECT id FROM `ressource_typen` WHERE bezeichnung='REGELUNGSVORHABEN'),
            (SELECT id FROM `rollen` WHERE bezeichnung='FEDERFUEHRER'),
            0, NULL, NULL, 'Federführer Regelungsvorhaben');

INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
  VALUES ('user2_id', 'REGELUNGSVORHABEN 00000000-0000-0000-0000-000000000020 1', '00000000-0000-0000-0000-000000000020',
            (SELECT id FROM `ressource_typen` WHERE bezeichnung='REGELUNGSVORHABEN'),
            (SELECT id FROM `rollen` WHERE bezeichnung='FEDERFUEHRER'),
            0, NULL, NULL, 'Federführer Regelungsvorhaben');

-- daektiviertes RV
INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
  VALUES ('user3_id', 'REGELUNGSVORHABEN 00000000-0000-0000-0000-000000000123 1', '00000000-0000-0000-0000-000000000123',
            (SELECT id FROM `ressource_typen` WHERE bezeichnung='REGELUNGSVORHABEN'),
            (SELECT id FROM `rollen` WHERE bezeichnung='FEDERFUEHRER'),
            0, NULL, NULL, 'Federführer Regelungsvorhaben');

-- A
INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
  VALUES ('user3_id', 'REGELUNGSVORHABEN 00000000-0000-0000-0000-00000000a123 1', '00000000-0000-0000-0000-00000000a123',
            (SELECT id FROM `ressource_typen` WHERE bezeichnung='REGELUNGSVORHABEN'),
            (SELECT id FROM `rollen` WHERE bezeichnung='FEDERFUEHRER'),
            0, NULL, NULL, 'Federführer Regelungsvorhaben');

