-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- USE `schema_test2`;

SET foreign_key_checks = 0;

DELETE FROM `zuordnungen`;
DELETE FROM `nutzer`;
DELETE FROM `nutzer_stellvertreter`;

SET foreign_key_checks = 1;
