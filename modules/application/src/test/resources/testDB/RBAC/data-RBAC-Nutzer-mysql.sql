-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- USE `schema_test2`;

INSERT INTO `nutzer` (`id`, `abteilung`, `anrede`, `email`, `referat`, `gid`, `name`, `telefon`, `titel`, `ressort_kurzbezeichnung`, `ressort_id`, `bearbeitet_am`, `erstellt_am`, `last_modified`, `aktiv`, `deleted`, `plattform_no_ref`, `editor_no_ref`, `rolle_id`)
  VALUES (UUID_TO_BIN('00000000-0000-0000-0000-000000000001'), 'D', 'Herr', 'Vorname1.Nachname1@example.com', NULL, 'user1_id', 'Vorname1 Nachname1', NULL, NULL, 'BMI', 4, '2024-04-23 13:08:25', '2022-07-14 16:09:55', '2022-10-11 16:11:59', 1, 0, 0, 0, NULL);
INSERT INTO `nutzer` (`id`, `abteilung`, `anrede`, `email`, `referat`, `gid`, `name`, `telefon`, `titel`, `ressort_kurzbezeichnung`, `ressort_id`, `bearbeitet_am`, `erstellt_am`, `last_modified`, `aktiv`, `deleted`, `plattform_no_ref`, `editor_no_ref`, `rolle_id`)
  VALUES (UUID_TO_BIN('00000000-0000-0000-0000-000000000002'), 'Z', 'Frau', 'Vorname2.Nachname2@example.com', NULL, 'user2_id', 'Vorname2 Nachname2', NULL, NULL, 'BMI', 4, '2024-04-23 14:32:02', '2022-07-14 16:09:55', '2021-07-21 18:09:04', 1, 0, 0, 0, NULL);
INSERT INTO `nutzer` (`id`, `abteilung`, `anrede`, `email`, `referat`, `gid`, `name`, `telefon`, `titel`, `ressort_kurzbezeichnung`, `ressort_id`, `bearbeitet_am`, `erstellt_am`, `last_modified`, `aktiv`, `deleted`, `plattform_no_ref`, `editor_no_ref`, `rolle_id`)
  VALUES (UUID_TO_BIN('00000000-0000-0000-0000-000000000003'), 'Z', 'unbestimmt', 'Vorname3.Nachname3@example.com', 'Z II 1', 'user3_id', 'Vorname3 Nachname3', NULL, NULL, 'BMJ', 5, '2024-04-23 11:55:43', '2022-07-14 16:09:55', '2021-01-20 14:16:54', 1, 0, 0, 0, NULL);
