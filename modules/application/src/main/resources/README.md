<!--
Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit

SPDX-License-Identifier: MPL-2.0
-->

# Umgebung

## Wildfly

### Versionswahl

* WildFly 27 is JakartaEE 10 while the SpringBoot version targets JavaEE 8. You need to revert to WildFly 26.1.2.Final.
* wildfly-servlet-26.1.3.Final\bin\standalone.bat
* wildfly-servlet-26.1.3.Final\bin\add-user.bat
  (servlet -> keine admin console)

| Wildfly | JavaEE-Version  | JavaSE-Version | JBoss EAP |
|:-------:|:---------------:|:--------------:|-----------|
|   14    |        8        | 7 + Vorsch. 8  | EAP 7.1   |
| 14.0.1  |                 |                | EAP 7.2   |
|   15    |                 |       11       | EAP 7.2   |
|   18    |                 |       13       | EAP 7.3   |
|   23    |                 |                | EAP 7.4   |
|   24    |                 |                |           |
|   25    | 8 + Vorsch. 9.1 |       17       |           |
|   27    |  Jakarta EE 10  |       17       |           |

### Logging-Configuration

If you want to use your own log4j2 implementation, such as log4j-core, then you need to do the following two steps.

* Disable the adding of the logging dependencies to all your deployments with the add-logging-api-dependencies attribute
  OR exclude the org.apache.logging.log4j.api in a jboss-deployment-structure.xml.

* Then you would need to include the log4j-api and a log4j2 implementation library in your deployment.

Disabling the default logging in Wildfly and use application provided log4j and slf4j libraries
standalone.xml
<subsystem xmlns="urn:jboss:domain:logging:8.0">
<add-logging-api-dependencies value="false"/>
<use-deployment-logging-config value="false"/>
....
</subsystem>

### Konfiguration mittels CLI

```bash
.\bin\jboss-cli.sh -c --file=JBoss-Editor-Settings.cli --properties=lea-dev-editor.properties
```

## Spring Boot

# Other stuff

Download Certificate from IAM.
Surround with:

* -----BEGIN CERTIFICATE-----
* -----END CERTIFICATE-----

Use keytool:

```bash
keytool -importcert -alias iamKeyPair -storetype PKCS12 -keystore lea_keystore.jks -file x509-Certificate.pem -rfc -storepass changeit
```

Use it with keystore in JwtUtils.java

```bash
keytool -list -keystore lea_keystore.jks  -storepass changeit

```