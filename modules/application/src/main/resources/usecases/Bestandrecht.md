<!--
Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit

SPDX-License-Identifier: MPL-2.0
-->

# Bestandsrecht

Das Bestandsrecht ist bei NeuRIS (Neues Rechtsinformationssystem) hinterlegt. Zu einer Dokumentenmappe kann man verschiedene
Bestandsrechte zuordnen um sie bspw. als Vorlage bei Gesetzesänderungen zu verwenden. Das Vorgehen ist wie folgt:

## Hinzufügen von Bestandsrechttexten zum Regelungsvorhaben

Der Editor bietet die folgende Schnittstelle der Plattform an, die mehrere Bestandsrechte einem Regelungsvorhaben zuorden kann:

```
/plattform/user/{gid}/bestandsrecht/save/{regelungsvorhabenId}
```

Die Bestandsrechte werden in der Tabelle *bestandsrecht* gespeichert und über die Tabelle *rv_bestandsrecht* dem Regelungsvorhaben zugeordnet.

## Auswahl der Bestandsrechte aus dem Regelungsvorhaben und Zuordnung zu einer Dokumentenmappe

Die dem Regelungsvorhaben zugeordneten Bestandsrechte können als Submenge einer Dokumentenmappe zugeordnet werden. Dies ist z.B. dann sinnvoll,
wenn mit einem Artikel ein Gesetz geändert wird und man nur mit diesem Gesetz arbeiten möchte.

Dazu geht das Frontend wie folgt vor:

### Abruf der bereits zugeordneten Bestandsrechte zur Dokumentenmappe

Um den Zuordnungsdialog von Bestandsrecht und Dokumentenmappe zu befüllen, ruft das Frontend die Schnittstelle

```
GET /compounddocuments/{rv-id}/bestandsrecht/dm
```

auf und erhält folgende Beispielliste (fachlich: Das Bestandsrecht darf nur einer DM zugeordnet sein):

```
[
  {
    "id": "dac4d7c8-ce22-48f6-a6c0-f3f8dea73e9d",
    "eli": "/eli/recht2",
    "titelKurz": "kurzTitel",
    "titelLang": "Langtitel",
    "verknuepfteDokumentenMappeId": "92b77fdd-5f73-404c-8c22-a5168757affa"
  },
  {
    "id": "4b003350-ac94-4845-bbe7-311df08ec96c",
    "eli": "/eli/recht1",
    "titelKurz": "kurzer Titel",
    "titelLang": "langer Titel",
    "verknuepfteDokumentenMappeId": "92b77fdd-5f73-404c-8c22-a5168757affa"
  },
  {
    "id": "00000000-0001-0001-0000-000000000123",
    "eli": "/eli/recht3",
    "titelKurz": "BDSG",
    "titelLang": "BDSG Bestandsrecht 30.06.2017",
    "verknuepfteDokumentenMappeId": null
  }
]
```

### Neuzuordnung von Bestandsrecht und Dokumentenmappe

Nach Auswahl von neuem Bestandsrecht oder Abwahl von schon zugeordnetem Bestandsrecht wird die Schnittstelle

```
PUT /compounddocuments/{id}/bestandsrechte
```

aufgerufen, die in der Tabelle *dm_bestandsrecht* die Einträge anpasst. Der payload sieht besipielsweise so aus:

```
[
   {
      "bestandsrechtId":{
         "id":"3fa85f64-5717-4562-b3fc-2c963f66afa6"
      },
      "bestandsrechtTitel":"Titel des Bestandsrechts",
      "verlinkteMappen":[
         {
            "compoundDocumentId":{
               "id":"3fa85f64-5717-4562-b3fc-2c963f66afa6"
            },
            "compoundDocumentTitle":"Titel der Dokumentenmappe"
         }
      ]
   }
]
```

## Versionierung einer Dokumentenmappe **inklusive** Bestandsrechte
