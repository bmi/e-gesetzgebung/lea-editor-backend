// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.configuration;

import java.util.Arrays;
import org.springdoc.core.GroupedOpenApi;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;

import static de.itzbund.egesetz.bmi.lea.core.Constants.API_KEY_NAME;

@Configuration
@SuppressWarnings("unused")
public class OpenApiDoc {

    public static final String PLATEG_URL = "/plattform/**";

    public static final String BASIC_AUTH_SECURITY_SCHEME_NAME = "basicAuth";

    public static final String BEARER_SECURITY_SCHEME_NAME = "JWToken";


    /**
     * Configuration / Documentation for Swagger (UI)
     *
     * @param appVersion  Backend Version
     * @param contextPath Backend Path
     * @return OpenAPI
     */
    @Bean
    public OpenAPI openApiDef(
        @Value("${app.version:}") String appVersion, @Value("${server.servlet.context-path:}") String contextPath) {

        return new OpenAPI()
            .info(new Info().title("eGesetzgebung Editor")
                .description("This is the rest interface offered by the eGesetzgebung editor backend.")
                .version(appVersion)
                .contact(new Contact().name("TEST")))

            .addServersItem(new Server().url(contextPath)
                .description("same system"))
            .addServersItem(new Server().url("https://editor.egesetz-demo.de" + contextPath)
                .description("dev"))
            .addServersItem(new Server().url("http://localhost:8080" + contextPath)
                .description("local"))

            .components(new Components()
                .addSecuritySchemes(BASIC_AUTH_SECURITY_SCHEME_NAME,
                    new SecurityScheme().name(BASIC_AUTH_SECURITY_SCHEME_NAME)
                        .type(SecurityScheme.Type.HTTP)
                        .scheme("basic"))
                .addSecuritySchemes(BEARER_SECURITY_SCHEME_NAME,
                    new SecurityScheme().name(BEARER_SECURITY_SCHEME_NAME)
                        .type(SecurityScheme.Type.HTTP)
                        .scheme("bearer")
                        .bearerFormat("JWT")
                        .in(SecurityScheme.In.HEADER))
            )

            .addSecurityItem(
                new SecurityRequirement().addList(BASIC_AUTH_SECURITY_SCHEME_NAME)
            )
            .addSecurityItem(
                new SecurityRequirement().addList(BEARER_SECURITY_SCHEME_NAME, Arrays.asList("read", "write"))
            );
    }


    @Bean
    public GroupedOpenApi plattformOpenApi() {
        String[] paths = {"/**"};
        String[] excludePaths = {PLATEG_URL};
        return GroupedOpenApi.builder()
            .group("Editor")
            .pathsToMatch(paths)
            .pathsToExclude(excludePaths)
            .build();
    }


    @Bean
    public GroupedOpenApi editorOpenApi() {
        String[] paths = {PLATEG_URL};
        return GroupedOpenApi.builder()
            .group("Plattform")
            .pathsToMatch(paths)
            .addOpenApiCustomiser(editorApiCustomizer())
            .build();
    }


    public OpenApiCustomiser editorApiCustomizer() {
        String securitySchemeName = "apiKeyScheme";
        return openApi -> openApi
            .addSecurityItem(new SecurityRequirement().addList(securitySchemeName))
            .getComponents()
            .addSecuritySchemes(securitySchemeName, new SecurityScheme()
                .type(SecurityScheme.Type.APIKEY)
                .name(API_KEY_NAME)
                .description("Secret API key between Platform and Editor")
                .in(SecurityScheme.In.HEADER));
    }

}
