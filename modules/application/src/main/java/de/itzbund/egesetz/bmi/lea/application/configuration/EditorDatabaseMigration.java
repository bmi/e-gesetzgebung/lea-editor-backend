// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.configuration;

import de.itzbund.egesetz.bmi.auth.configuration.AuthDatasourceConfiguration;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class EditorDatabaseMigration extends de.itzbund.egesetz.bmi.auth.configuration.DatabaseMigration
    implements ApplicationListener<ContextRefreshedEvent> {

    public EditorDatabaseMigration(
        AuthDatasourceConfiguration databaseConfigurations) {
        super(databaseConfigurations);
    }

}
