// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.configuration.security;

import de.itzbund.egesetz.bmi.lea.application.configuration.security.filter.JwtRequestFilter;
import de.itzbund.egesetz.bmi.lea.application.configuration.security.filter.SessionFilter;
import de.itzbund.egesetz.bmi.lea.application.configuration.security.filter.StellvertreterFilter;
import de.itzbund.egesetz.bmi.lea.application.configuration.security.filter.XApiKeyFilter;
import de.itzbund.egesetz.bmi.lea.application.configuration.security.handler.success.OidcAuthenticationSuccessHandler;
import de.itzbund.egesetz.bmi.lea.controllers.security.CustomPermissionEvaluator;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.CsrfConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.oidc.web.logout.OidcClientInitiatedLogoutSuccessHandler;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestRedirectFilter;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.ForwardedHeaderFilter;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static de.itzbund.egesetz.bmi.lea.core.Constants.PLATEG_URL;

// tells Spring to apply the web security configuration declared by the class
@EnableWebSecurity
@EnableMethodSecurity(
    // enables @Secured annotation
    securedEnabled = true,
    // enables @RolesAllowed annotation
    jsr250Enabled = false,
    // enables @PreAuthorize, @PostAuthorize, @PreFilter, @PostFilter annotations.
    prePostEnabled = true
)
@Log4j2
@Configuration
@SuppressWarnings("unused")
public class OAuth2SecurityConfig {

    public static final String REDIRECT_URI_PARAM_NAME = "redirectUri";

    @Value("${spa.url.logout:}")
    String logout;
    @Value("${spa.url.login:}")
    String login;
    @Value("${spa.url.base:}")
    String base;

    @Value("#{'${cors.allowed.urls}'.split(',')}")
    List<String> corsAllowedUrls;

    @Value("${csrf.security.disabled:false}")
    boolean disableCsrf;

    @Autowired
    LeageSession session;


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    /**
     * Used to read out proxy headers, so that the same origin policy can be evaluated correctly
     *
     * @return returns a filter that reads out proxy headers and sets them
     */
    @Bean
    FilterRegistrationBean<ForwardedHeaderFilter> forwardedHeaderFilter() {
        final FilterRegistrationBean<ForwardedHeaderFilter> filterRegistrationBean =
            new FilterRegistrationBean<>();
        filterRegistrationBean.setFilter(new ForwardedHeaderFilter());
        filterRegistrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return filterRegistrationBean;
    }


    private void setupCsrf(CsrfConfigurer<HttpSecurity> csrf) {
        csrf.csrfTokenRepository(cookieCsrfTokenRepository());
        csrf.ignoringRequestMatchers(PLATEG_URL); //NOSONAR

        if (disableCsrf) {
            csrf.disable(); //NOSONAR
            log.warn("CSRF disabled - only recommended in safe development environment!");
        }

    }

    @Bean
    public SecurityFilterChain configure(
        @Autowired HttpSecurity httpSecurity,
        // Spring: InMemoryClientRegistrationRepository
        @Autowired ClientRegistrationRepository clientRegistrationRepository,
        @Autowired OidcAuthenticationSuccessHandler oidcAuthenticationSuccessHandler, // Eigenimplementierung
        @Autowired JwtRequestFilter jwtRequestFilter, // Eigenimplementierung
        @Autowired SessionFilter sessionFilter, // Eigenimplementierung
        @Autowired XApiKeyFilter xApiKeyFilter, // Eigenimplementierung
        @Autowired StellvertreterFilter stellvertreterFilter // Eigenimplementierung
    ) throws Exception {

        // Identity and Access Management (IAM)
        // OpenId connect
        // Add logout handler to invalidate SSO session -> otherwise the user will be directly relogged in
        OidcClientInitiatedLogoutSuccessHandler logoutHandler =
            new OidcClientInitiatedLogoutSuccessHandler(clientRegistrationRepository);
        logoutHandler.setPostLogoutRedirectUri(base + logout);

        httpSecurity
            .cors()
            .configurationSource(corsConfigurationSource(corsAllowedUrls))
            .and()

            .csrf(this::setupCsrf)

            // Do not automatically redirect -> otherwise the SPA does not know when to redirect
            // the user as well
            .exceptionHandling()
            // Exception Handler
            .accessDeniedHandler((request, response, accessDeniedException) -> {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                response.getWriter().write(accessDeniedException.toString());
            })
            .and()

            // make sure we use stateless session; session won't be used to
            // store user's state.
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()

            // don't authenticate these particular requests
            // Options calls do not send Cookies -> so they are allowed to pass always
            .authorizeHttpRequests()
            .requestMatchers(HttpMethod.OPTIONS)
            .permitAll()
            // Same parts are allowed to be used without being logged in
            .requestMatchers(PLATEG_URL, "/actuator/**", "/v3/api-docs*", "/v3/api-docs/**",
                "/v3/api-docs*/**", "/swagger-ui/**", "/swagger-ui.html")
            .permitAll()

            // all other requests need to be authenticated
            .anyRequest()
            .authenticated()
            .and()

            .logout()
            .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
            .logoutSuccessHandler(logoutHandler)
            .and()

            // We added the JwtTokenFilter before the Spring Security internal UsernamePasswordAuthenticationFilter.
            // We’re doing this because we need access to the user identity at this point to perform
            // authentication/authorization,
            // and its extraction happens inside the JWT token filter based on the provided JWT token.
            .addFilterBefore(
                jwtRequestFilter,
                UsernamePasswordAuthenticationFilter.class
            )

            .addFilterBefore(
                stellvertreterFilter,
                UsernamePasswordAuthenticationFilter.class
            )

            .addFilterBefore(
                sessionFilter,
                OAuth2AuthorizationRequestRedirectFilter.class
            )

            .addFilterAfter(
                xApiKeyFilter,
                RequestHeaderAuthenticationFilter.class)

            .oauth2Login()
            .successHandler(oidcAuthenticationSuccessHandler);

        return httpSecurity.build();
    }

    /**
     * Cross-Origin Resource Sharing (übersetzt: Ressourcenübergreifende Anfragefreigabe)
     *
     * @param allowedUrls Eine Liste von URLs, für die eine Erlaubnis erstellt werden soll.
     * @return Eine CORS-Konfiguration
     */
    private CorsConfigurationSource corsConfigurationSource(List<String> allowedUrls) {
        log.debug("configuring CORS...");
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowedOriginPatterns(allowedUrls);
        config.setAllowedMethods(List.of("OPTIONS", "GET", "POST", "PATCH", "PUT", "DELETE"));
        config.setAllowCredentials(true);
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return source;
    }

    /**
     * Cross-Site Request Forgery (übersetzt: Verrat durch Fälschung von Anfragen zwischen verschiedenen Webseiten)
     *
     * @return instance of CookieCsrfTokenRepository
     */
    private CookieCsrfTokenRepository cookieCsrfTokenRepository() {
        log.debug("configuring CSRF...");
        CookieCsrfTokenRepository repository = new CookieCsrfTokenRepository();
        //don't set to HTTP only, otherwise client cannot see these cookies
        repository.setCookieHttpOnly(false);
        //don't set path to backend context, otherwise client cannot read X-XSRF-TOKEN value from cookies
        repository.setCookiePath("/");
        repository.setCookieName("XSRF-TOKEN-EDITOR");
        repository.setHeaderName("X-XSRF-TOKEN-EDITOR");
        return repository;
    }


    @Bean
    protected MethodSecurityExpressionHandler createExpressionHandler(CustomPermissionEvaluator customPermissionEvaluator) {
        DefaultMethodSecurityExpressionHandler expressionHandler =
            new DefaultMethodSecurityExpressionHandler();
        expressionHandler.setPermissionEvaluator(customPermissionEvaluator);
        return expressionHandler;
    }

}
