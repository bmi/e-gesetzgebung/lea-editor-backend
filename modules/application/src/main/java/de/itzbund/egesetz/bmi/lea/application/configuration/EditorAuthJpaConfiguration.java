// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.configuration;

import org.springframework.context.annotation.Configuration;

@Configuration
@SuppressWarnings("unused")
public class EditorAuthJpaConfiguration extends de.itzbund.egesetz.bmi.auth.configuration.AuthJpaConfiguration {

}
