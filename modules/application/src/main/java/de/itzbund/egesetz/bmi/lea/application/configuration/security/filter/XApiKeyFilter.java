// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.configuration.security.filter;

import de.itzbund.egesetz.bmi.lea.application.configuration.PlategFilter;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static de.itzbund.egesetz.bmi.lea.core.Constants.PLATEG_URL;

@Log4j2
@Component
@SuppressWarnings("unused")
public class XApiKeyFilter implements Filter {

	@Autowired
	private PlategFilter plategFilter;


	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse response,
		FilterChain filterChain) throws ServletException, IOException {
		String requestUri = StringUtils.substringAfter(req.getRequestURI(), req.getContextPath());
		if (new AntPathMatcher().match(PLATEG_URL, requestUri)) {
			try {
				plategFilter.checkApiKey(req);

				Map<String, String> map =
					(new AntPathMatcher().extractUriTemplateVariables(PLATEG_URL + "/user/{gid}/**",
						requestUri));
				//check GID parameter and prepare session
				plategFilter.checkGid(map);

				//user can pass
				filterChain.doFilter(req, response);
			} catch (LeageException ex) {
				log.warn(ex.getMessage());
				log.trace(ex);
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			}
		} else {
			//is not plateg url - just skip and continue
			filterChain.doFilter(req, response);
		}

	}


	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
		FilterChain filterChain) throws IOException, ServletException {

		if ((servletRequest instanceof HttpServletRequest) && (servletResponse instanceof HttpServletResponse)) {
			doFilterInternal((HttpServletRequest) servletRequest, (HttpServletResponse) servletResponse, filterChain);
		} else {
			filterChain.doFilter(servletRequest, servletResponse);
		}
	}
}
