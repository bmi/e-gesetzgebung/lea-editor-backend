// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.configuration.security.filter;

import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static de.itzbund.egesetz.bmi.lea.core.Constants.HEADER_FIELD_STELLVERTRETUNG;

@Log4j2
@Component
public class StellvertreterFilter implements Filter {


    @Autowired
    private LeageSession session;

    @Autowired
    private UserPersistencePort userRepository;

    @SuppressWarnings("all")
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if ((servletRequest instanceof HttpServletRequest) && (servletResponse instanceof HttpServletResponse)) {
            doFilterInternal((HttpServletRequest) servletRequest, (HttpServletResponse) servletResponse, filterChain);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }

    }

    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        Optional<String> headerStellvertreterKey = Optional.ofNullable(request.getHeader(HEADER_FIELD_STELLVERTRETUNG));

        log.debug("StellvertreterFilter::doFilterInternal");

        if (!headerStellvertreterKey.isEmpty()) {

            String stellvertreterId = headerStellvertreterKey.get();

            if (!StringUtils.isBlank(stellvertreterId)) {
                log.debug("Stellvertreter ist: {}", stellvertreterId);

                User byGid = userRepository.findFirstByGid(new UserId(stellvertreterId));
                session.setRepresentedUser(byGid);
            } else {
                log.debug("Kein Stellvertreter");
				session.setRepresentedUser(null);
            }
        } else {
            session.setRepresentedUser(null);
        }

        filterChain.doFilter(request, response);
    }

}
