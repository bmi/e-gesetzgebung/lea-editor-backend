// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea;

import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.CodeSignature;
import org.aspectj.lang.reflect.MethodSignature;
import org.hibernate.LazyInitializationException;
import org.slf4j.LoggerFactory;
import org.springframework.boot.logging.LogLevel;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

/**
 * aspect is responsible for automatic logging of all method calls
 */
@Aspect
@Component
@Log4j2
@SuppressWarnings("unused")
public class LoggingAspect {

    private static String entry(String methodName, boolean showArgs, String[] methodParams, Object[] methodArgs) {
        StringJoiner message = new StringJoiner(" ")
            .add("Started")
            .add(methodName)
            .add("method"); //NOSONAR

        if (showArgs
            && Objects.nonNull(methodParams)
            && Objects.nonNull(methodArgs)
            && methodParams.length == methodArgs.length) {
            Map<String, Object> values = new HashMap<>(methodParams.length);

            for (int i = 0; i < methodParams.length; i++) {
                values.put(methodParams[i], methodArgs[i]);
            }

            message.add("with args: ")
                .add(toString(values));
        }

        return message.toString();
    }


    static String toString(Object values) {
        return Optional.ofNullable(values)
            .map(vals -> {
                try {
                    return vals.toString();
                } catch (LazyInitializationException ex) {
                    //you should fix this soon, because this error prevents logging of the affected entity
                    log.error("This error should not happend! Fix this by adding @ToString.Exclude to the "
                        + "affected property in your entity", ex);
                    return null;
                }
            })
            .orElse(null);
    }


    static String exit(String methodName, String duration, Object response, boolean showResult,
        boolean showExecutionTime) {
        StringJoiner message = new StringJoiner(" ")
            .add("Finished")
            .add(methodName)
            .add("method"); //NOSONAR

        if (showExecutionTime) {
            message.add("in")
                .add(duration);
        }

        if (showResult) {
            message.add("with return: ")
                .add(toString(response));
        }

        return message.toString();
    }


    /**
     * pointcut to match all methods with specific package name
     */
    @Pointcut("within(de.itzbund.egesetz.bmi.lea..*) && "
        + "!within(de.itzbund.egesetz.bmi.lea.application.security.filter.*)")
    public void leaPackage() {
        //pointcut for all methods in LEA packages except for filters
    }


    /**
     * pointcut to match all executed methods regardless of their visibility
     */
    @Pointcut("execution(* *(..))")
    public void allMethods() {
        //pointcut for ALL methods
    }


    /**
     * pointcut to match all executed public methods
     */
    @Pointcut("execution(public * *(..))")
    public void publicMethods() {
        //pointcut for PUBLIC methods only
    }


    @Pointcut("execution(*.TraversableJSONDocument.new(..))")
    public void traversableJSONDocumentConstructor() {
        // pointcut for constructor of TraversableJSONDocument
    }


    /**
     * log all debug information
     *
     * @param joinPoint joint point
     * @return method result
     * @throws Throwable error
     */
    @Around("leaPackage() && publicMethods()")
    public Object debugPointcut(ProceedingJoinPoint joinPoint) throws Throwable {
        if (!isDebug(joinPoint)) {
            //save effort and don't prepare anything for logging, continue with normal unlogged call
            return joinPoint.proceed();
        }
        return log(joinPoint, LogLevel.DEBUG, false);
    }


    @Around("traversableJSONDocumentConstructor()")
    public Object timingTraversableJSONDocumentConstruction(ProceedingJoinPoint joinPoint) throws Throwable {
        if (!isDebug(joinPoint)) {
            //save effort and don't prepare anything for logging, continue with normal unlogged call
            return joinPoint.proceed();
        }
        return log(joinPoint, LogLevel.DEBUG, true);
    }


    /**
     * log all trace information
     *
     * @param joinPoint joint point
     * @return method result
     * @throws Throwable error
     */
    @Around("leaPackage() && allMethods()")
    public Object tracePointcut(ProceedingJoinPoint joinPoint) throws Throwable {
        if (!isTrace(joinPoint)) {
            //save effort and don't prepare anything for logging, continue with normal unlogged call
            return joinPoint.proceed();
        }
        return log(joinPoint, LogLevel.TRACE, true);
    }


    public boolean isDebug(ProceedingJoinPoint joinPoint) {
        return LoggerFactory.getLogger(joinPoint.getTarget()
                .getClass())
            .isDebugEnabled();
    }


    public boolean isTrace(ProceedingJoinPoint joinPoint) {
        return LoggerFactory.getLogger(joinPoint.getTarget()
                .getClass())
            .isTraceEnabled();
    }


    private Object log(ProceedingJoinPoint joinPoint, LogLevel logLevel, boolean showExecutionTime)
        throws Throwable {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        CodeSignature codeSignature = (CodeSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();

        String methodName = joinPoint.getTarget()
            .getClass()
            .getName() + "#" + method.getName();
        Object[] methodArgs = joinPoint.getArgs();
        String[] methodParams = codeSignature.getParameterNames();

        boolean showArgs = true;
        boolean showResult = true;
        ChronoUnit unit = ChronoUnit.MILLIS;

        log(logLevel, entry(methodName, showArgs, methodParams, methodArgs));

        Instant start = Instant.now();
        Object response = joinPoint.proceed();
        Instant end = Instant.now();
        String duration = String.format("%s %s", unit.between(start, end), unit.name()
            .toLowerCase());

        log(logLevel, exit(methodName, duration, response, showResult, showExecutionTime));

        return response;
    }


    public void log(LogLevel logLevel, String message) {
        switch (logLevel) {
            case DEBUG:
                log.debug(message);
                break;
            case TRACE:
                log.trace(message);
                break;
            case WARN:
                log.warn(message);
                break;
            case ERROR:
            case FATAL:
                log.error(message);
                break;
            default:
                log.info(message);
        }
    }

}
