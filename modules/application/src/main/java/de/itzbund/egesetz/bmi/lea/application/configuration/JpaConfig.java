// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.configuration;

import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

@Configuration
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class JpaConfig {

    /**
     * implements auditor aware interface
     *
     * @return implementation of auditor aware
     */
    @Bean
    public AuditorAware<User> auditorProvider(LeageSession session) {
        return () -> Optional.ofNullable(session.getUser());
    }

}
