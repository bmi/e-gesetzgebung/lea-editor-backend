// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.configuration;

import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserPersistencePort;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageException;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static de.itzbund.egesetz.bmi.lea.core.Constants.API_KEY_NAME;
import static de.itzbund.egesetz.bmi.lea.core.Constants.GID_PARAM_NAME;

@Component
@RequiredArgsConstructor
@Log4j2
@SuppressWarnings("unused")
public class PlategFilter {

	public static final String API_KEY = API_KEY_NAME;
	public static final String GID_PARAM = GID_PARAM_NAME;
	@Value("${plateg.x-api-key}")
	private String apiKey;
	@Autowired
	private UserPersistencePort userRepository;
	@Autowired
	private LeageSession session;


	private static void checkRequestHeaderValue(HttpServletRequest request, String keyName, String expectedValue) {
		String actualValue = getRequestHeaderValue(request, keyName);
		if (!expectedValue.equals(actualValue)) {
			log.trace("illegal " + keyName + " in request header found: " + actualValue);
			throw new LeageException("Illegal value in request header found: " + keyName);
		}
	}


	private static String getRequestHeaderValue(HttpServletRequest request, String keyName) {
		Optional<String> headerApiKey = Optional.ofNullable(request.getHeader(keyName));
		if (headerApiKey.isEmpty()) {
			throw new LeageException("Not found in request header: " + keyName);
		}
		if (StringUtils.isBlank(headerApiKey.get())) {
			throw new LeageException("Empty value in request header: " + keyName);
		}
		return headerApiKey.get();
	}


	private static String getGidParameter(Map<String, String> map) {
		String gid = Optional.ofNullable(map)
			.map(m -> m.get(GID_PARAM_NAME))
			.orElse(null);
		if (gid == null) {
			throw new LeageException("Parameter not found: " + GID_PARAM_NAME);
		}
		if (StringUtils.isBlank(gid)) {
			throw new LeageException("Empty parameter value: " + GID_PARAM_NAME);
		}
		return gid;
	}


	/**
	 * checks API Key in request header
	 *
	 * @param request request to be checked
	 */
	public void checkApiKey(HttpServletRequest request) {
		checkRequestHeaderValue(request, API_KEY_NAME, apiKey);
		log.trace("API Key is valid -> go for it!");
	}


	/**
	 * checks GID in request url
	 *
	 * @param map url parameters
	 */
	public void checkGid(Map<String, String> map) {
		String gid = getGidParameter(map);
		log.trace("GID passed: {}", gid);
		User userEntity = userRepository.findFirstByGid(new UserId(gid));
		if (userEntity == null) {
			throw new EntityNotFoundException("user with given gid not found!");
		}
		Set<GrantedAuthority> authoritySet = new HashSet<>();
		authoritySet.add(new SimpleGrantedAuthority("XAPI_KEY"));
		AnonymousAuthenticationToken anonymousAuthenticationToken = new AnonymousAuthenticationToken(userEntity.getGid().getId(), userEntity, authoritySet);
		SecurityContextHolder.getContext().setAuthentication(anonymousAuthenticationToken);
	}

}
