// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.configuration.security;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.jwt.SignedJWT;
import de.itzbund.egesetz.bmi.lea.core.util.CustomResourceProvider;
import de.itzbund.egesetz.bmi.lea.core.xml.ReusableInputSource;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@NoArgsConstructor
@Log4j2
public class JwtUtils {

    public static final String IAM_KEY_PAIR = "iamKeyPair";
    public static final String KEYSTORE_PASS = "changeit"; //NOSONAR
    private static final String KEY_STORE_FILE = "lea_keystore.jks";
    private static final String DBG_MSG = "Keysore file location: {}";
    // 1 Stunde
    private static final int JWT_EXPIRES_IN_MINUTES = 3_600_000;
    private static final int HEADER_POSITION = 0;
    private static final int PAYLOAD_POSITION = 1;
    private static final int SIGNATUR_POSITION = 2;

    @Value("${spring.security.oauth2.client.registration.keycloak.client-secret}")
    protected String jwtSecret;
    protected PrivateKey privateKey;


    private static Date toDate(LocalDateTime localDateTime) {
        ZoneId zoneId = ZoneId.systemDefault();
        return Date.from(localDateTime.atZone(zoneId)
            .toInstant());
    }


    /**
     * Extracts part of JWToken for verify signature
     *
     * @param token JWToken as string
     * @return Map of the three parts of a JWToken
     */
    @SuppressWarnings("unused")
    protected Map<String, String> splitJwtToken(String token) {
        Base64.Decoder decoder = Base64.getUrlDecoder();
        Map<String, String> tokenParts = new HashMap<>();

        String[] chunks = token.split("\\.");

        tokenParts.put("HeaderBase64", chunks[HEADER_POSITION]);
        tokenParts.put("Header", new String(decoder.decode(chunks[HEADER_POSITION]), StandardCharsets.UTF_8));

        tokenParts.put("PayloadBase64", chunks[PAYLOAD_POSITION]);
        tokenParts.put("Payload", new String(decoder.decode(chunks[PAYLOAD_POSITION]), StandardCharsets.UTF_8));

        tokenParts.put("SignatureBase64", chunks[SIGNATUR_POSITION]);
        tokenParts.put("Signature", new String(decoder.decode(chunks[SIGNATUR_POSITION]), StandardCharsets.UTF_8));

        return tokenParts;
    }


    /**
     * Extracts are all infos from JWToken's payload and provide it as map.
     *
     * @param jwToken JWToken as string
     * @return A map of items in the payload
     */
    public Map<String, Object> getPayload(String jwToken) {

        JWT jwt;
        try {
            jwt = JWTParser.parse(jwToken);
        } catch (ParseException e) {
            log.error("JWToken '{}' not parsable.", jwToken);
            return Collections.emptyMap();
        }

        Payload payload = ((SignedJWT) jwt).getPayload();
        return payload.toJSONObject();
    }


    /**
     * Try to get keystore file, but it works different in JBoss, Spring, ...
     *
     * @return the file
     */
    private InputStream getKeystoreAsStream() {

        InputStream keystoreStream = getKeyStoreSpringBootWay();
        if (keystoreStream == null) {
            keystoreStream = getKeyStoreByClassLoaderResource();

            if (keystoreStream == null) {
                keystoreStream = getKeyStoreByResourceStream();

                if (keystoreStream == null) {
                    keystoreStream = getKeyStoreByCustomSourceProvider();
                }

            }
        }

        return keystoreStream;
    }


    private InputStream getKeyStoreByCustomSourceProvider() {
        log.debug("Keystore by CustomSourceProvider:");
        CustomResourceProvider crp = CustomResourceProvider.getInstance();
        ReusableInputSource singleInputSource = crp.getSingleInputSource("/" + KEY_STORE_FILE);
        InputSource content = new InputSource(singleInputSource.getByteStream());
        log.debug("InputSource isEmpty: {}", content.isEmpty());

        if (!content.isEmpty()) {
            return content.getByteStream();
        }

        return null;
    }


    private InputStream getKeyStoreByResourceStream() {
        log.debug("Keystore by resource as stream:");
        InputStream resourceAsStream = this.getClass()
            .getResourceAsStream(KEY_STORE_FILE);
        log.debug("Resourcestream: {}", resourceAsStream);

        return resourceAsStream;
    }


    private FileInputStream getKeyStoreByClassLoaderResource() {
        // The Class Loader Resource Way
        try {

            log.debug("Keystore by class loader resource:");
            ClassLoader classLoader = Thread.currentThread()
                .getContextClassLoader();
            URL resource = classLoader.getResource(KEY_STORE_FILE);
            assert resource != null;
            File keystoreFile = new File(resource.getFile());
            log.debug(DBG_MSG, keystoreFile);

            if (keystoreFile.exists()) {
                return new FileInputStream(keystoreFile);
            }

        } catch (FileNotFoundException | NullPointerException e) {
            log.debug(e);
        }

        return null;
    }


    private FileInputStream getKeyStoreSpringBootWay() {
        // The Spring Boot Way
        try {
            log.debug("Keystore by spring boot:");
            File keystoreFile = ResourceUtils.getFile("classpath:" + KEY_STORE_FILE);
            log.debug(DBG_MSG, keystoreFile);

            if (keystoreFile.exists()) {
                return new FileInputStream(keystoreFile);
            }
        } catch (FileNotFoundException e) {
            log.debug(e);
        }

        return null;
    }


    /**
     * Retrieves the public key out of the keystore
     *
     * @return Public key of RSA
     */
    public RSAPublicKey getPublicKeyFromKeystore() {
        PublicKey publicKey = null;

        try (InputStream fis = getKeystoreAsStream()) {
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            keyStore.load(fis, KEYSTORE_PASS.toCharArray()); //NOSONAR
            Certificate certificate = keyStore.getCertificate(IAM_KEY_PAIR);
            publicKey = certificate.getPublicKey();

            privateKey =
                (PrivateKey) keyStore.getKey(IAM_KEY_PAIR, KEYSTORE_PASS.toCharArray()); //NOSONAR
        } catch (KeyStoreException kse) {
            log.error("Errors with keystore.", kse);
        } catch (CertificateException ce) {
            log.error("Problem with get correct Certificate.", ce);
        } catch (NoSuchAlgorithmException nsae) {
            log.error("A problem with cryptographic algorithms is occurred.", nsae);
        } catch (IOException ioe) {
            log.error("There are problems with the keyfile.", ioe);
        } catch (UnrecoverableKeyException uke) {
            log.debug("Private key not included in certificate, but also not necessary.", uke);
        }

        return (RSAPublicKey) publicKey;
    }


    /**
     * Validates a JWToken with RSA and SHA256, but uses the key from the keystore
     *
     * @param authToken The JWToken
     * @return If token is valid
     */
    public boolean validateJwtTokenRSA256WithKeystore(String authToken) {
        return validateJwtTokenRS256(authToken, getPublicKeyFromKeystore());
    }


    /**
     * Validates a JWToken with RSA and SHA256
     *
     * @param authToken The JWToken
     * @param publicKey The public key of RSA
     * @return If token is valid
     */
    public boolean validateJwtTokenRS256(String authToken, RSAPublicKey publicKey) {
        boolean tokenSignatureValid = false;
        JWSVerifier verifier = new RSASSAVerifier(publicKey);

        try {
            SignedJWT signedJWT = SignedJWT.parse(authToken);
            tokenSignatureValid = signedJWT.verify(verifier);
        } catch (ParseException pe) {
            log.error("JWToken could not be parsed.", pe);
        } catch (JOSEException e) {
            log.error("JWToken could not be verified.", e);
        }

        return tokenSignatureValid;
    }


    /**
     * Generates a HS512 JWToken, but signature is wrong!
     *
     * @param username A sample field to be included into token.
     * @return The JWToken
     * @deprecated Signature is wrong
     */
    @Deprecated(
        since = "creation"
    )
    @SuppressWarnings("squid:S1133")
    public String generateHS512JwtToken(String username) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime expiryDate = now.plusMinutes(JWT_EXPIRES_IN_MINUTES);

        return Jwts.builder()
            .setExpiration(toDate(expiryDate))
            .signWith(SignatureAlgorithm.HS512, jwtSecret)
            .claim("username", username) //NOSONAR
            .compact();
    }

    /**
     * Generates a RS256 JWToken
     *
     * @param username A sample field to be included into token.
     * @return The JWToken
     */
    public String generateRS256JwtToken(String username) {
        Map<String, Object> claims = Map.of("username", username);
        return generateRS256JwtToken(claims, privateKey);
    }

    /**
     * Generates a RS256 JWToken
     *
     * @param claims
     * @param privateKey
     * @return
     */
    public static String generateRS256JwtToken(Map<String, Object> claims, PrivateKey privateKey) {

        // Gültigkeitsdauer des Tokens
        Date issuedAt = new Date();
        Date expiration = new Date(issuedAt.getTime() + JWT_EXPIRES_IN_MINUTES);

        JwtBuilder testGeneriertesToken = Jwts.builder()
            .setSubject("Test generiertes Token")
            .setIssuedAt(issuedAt)
            .setExpiration(expiration)
            .signWith(privateKey, SignatureAlgorithm.RS256);

        testGeneriertesToken.addClaims(claims);

        // JWT-Erstellung mit RSA256-Signatur - Token generieren
        return testGeneriertesToken.compact();
    }

}
