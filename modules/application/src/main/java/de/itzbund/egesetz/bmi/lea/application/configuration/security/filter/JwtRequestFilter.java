// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.configuration.security.filter;

import de.itzbund.egesetz.bmi.lea.application.configuration.security.JwtUtils;
import de.itzbund.egesetz.bmi.lea.domain.model.User;
import de.itzbund.egesetz.bmi.lea.domain.model.vo.UserId;
import de.itzbund.egesetz.bmi.lea.domain.ports.persistenceports.jpa.UserPersistencePort;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUserAuthority;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.Map;

/**
 * This class implements the filter for JWTokens
 */
@Log4j2
@Component
@SuppressWarnings("unused")
public class JwtRequestFilter implements Filter {

    private static final int HEADER_PREFIX_LENGTH = 7;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private UserPersistencePort userRepository;

    @SuppressWarnings("all")
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if ((servletRequest instanceof HttpServletRequest) && (servletResponse instanceof HttpServletResponse)) {
            doFilterInternal((HttpServletRequest) servletRequest, (HttpServletResponse) servletResponse, filterChain);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String jwToken = getJWTokenFromRequest(request);

        log.debug("JwtRequestFilter::doFilterInternal");

        if (jwToken != null && jwtUtils.validateJwtTokenRSA256WithKeystore(jwToken)) {
            Map<String, Object> payload = jwtUtils.getPayload(jwToken);
            getOrCreateUserAndStoreInSession(payload);
            createOicdTokenFromJwtTokenAndAuthenticate(request, payload);
        } else {
            removeAuthenticatedStatus();
        }

        filterChain.doFilter(request, response);
    }

    private void removeAuthenticatedStatus() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        // in case of OAuth2AuthenticationToken IAM's openId is used
        if (authentication instanceof UsernamePasswordAuthenticationToken) {
            SecurityContextHolder.getContext().setAuthentication(null);
        }
    }

    private void createOicdTokenFromJwtTokenAndAuthenticate(HttpServletRequest request, Map<String, Object> payload) {
        Long iat = (Long) payload.get("iat");
        Long exp = (Long) payload.get("exp");

        OidcIdToken idToken = new OidcIdToken("Self generated Token", Instant.ofEpochSecond(iat),
            Instant.ofEpochSecond(exp), payload);
        OidcUserAuthority authority = new OidcUserAuthority(idToken);

        DefaultOidcUser principal = new DefaultOidcUser(List.of(authority), idToken);

        // Get user identity and set it on the spring security context
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
            principal, null, List.of(authority));

        usernamePasswordAuthenticationToken.setDetails(
            new WebAuthenticationDetailsSource().buildDetails(request)
        );

        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    }

    private void getOrCreateUserAndStoreInSession(Map<String, Object> payload) {
        String gidFromToken = (String) payload.get("gid");
        String nameFromToken = (String) payload.get("name");
        String emailFromToken = (String) payload.get("email");

        User userEntity = userRepository.findFirstByGid(new UserId(gidFromToken));
        if (userEntity == null) {
            userEntity = User.builder()
                .gid(new UserId(gidFromToken))
                .name(nameFromToken)
                .email(emailFromToken)
                .build();
        }

    }

    protected String getJWTokenFromRequest(HttpServletRequest request) {
        String headerAuth = request.getHeader("Authorization");
        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
            return headerAuth.substring(HEADER_PREFIX_LENGTH);
        }
        return null;
    }

}
