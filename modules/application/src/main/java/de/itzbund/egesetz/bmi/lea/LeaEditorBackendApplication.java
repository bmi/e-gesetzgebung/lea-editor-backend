// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.metrics.jersey.JerseyServerMetricsAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.Properties;

/**
 * Simple application class created by Spring Initializr.
 */
// Exclude, cause of JBoss Errors
@SpringBootApplication(exclude = JerseyServerMetricsAutoConfiguration.class)
@EnableAspectJAutoProxy
@Log4j2
@EnableJpaRepositories(basePackages = {
    "de.itzbund.egesetz.bmi.lea.persistence.repositories"
//    , "de.itzbund.egesetz.bmi.user.repositories"
//    , "de.itzbund.egesetz.bmi.auth.repositories"
})
@EnableAsync
public class LeaEditorBackendApplication {

    /**
     * Entry point into a Spring Boot application.
     *
     * @param args application parameters
     */
    public static void main(String[] args) {
        boolean isLog4jJMXDisabled = Boolean.getBoolean("log4j2.disable.jmx");
        if (!isLog4jJMXDisabled) {
            log.warn("##### Set log4j2.disable.jmx to true as System Property. "
                + "Otherwise 'org.apache.logging.log4j.core.jmx.AppenderAdmin' "
                + "will remain loaded in the class loader.  #####");
        }

        SpringApplication application = new SpringApplication(LeaEditorBackendApplication.class);

        Properties properties = new Properties();
        properties.put("spring.datasource.packages", "de.itzbund.egesetz.bmi.lea.persistence.entities");
        application.setDefaultProperties(properties);

        application.run(args);
    }

}
