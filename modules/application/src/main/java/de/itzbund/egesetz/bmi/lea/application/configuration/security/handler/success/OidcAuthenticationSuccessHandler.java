// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea.application.configuration.security.handler.success;

import de.itzbund.egesetz.bmi.lea.application.configuration.security.OAuth2SecurityConfig;
import de.itzbund.egesetz.bmi.lea.domain.session.LeageSession;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Service
@NoArgsConstructor
@Log4j2
public class OidcAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Value("${spa.url.login:}")
    String login;
    @Value("${spa.url.base:}")
    String base;

    @Autowired
    LeageSession session;


    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
        Authentication authentication) throws IOException {
        session.updateUser();
        String redirectUri = (String) request.getSession()
            .getAttribute(OAuth2SecurityConfig.REDIRECT_URI_PARAM_NAME);
        if (redirectUri == null) {
            redirectUri = base + login;
        }
        // Redirect back to SPA
        response.sendRedirect(redirectUri);
    }

}