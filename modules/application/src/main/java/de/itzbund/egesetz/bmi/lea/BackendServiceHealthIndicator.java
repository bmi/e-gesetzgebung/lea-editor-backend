// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

@Component
@Slf4j
public class BackendServiceHealthIndicator implements HealthIndicator {

    public static final String NOT_RETRIEVABLE = "Not retrievable";

    @Value("${app.version:}")
    String leaVersion;

    @Override
    public Health health() {

        String rbacVersion = NOT_RETRIEVABLE;
        try {
            Manifest manifest = new Manifest(LeaEditorBackendApplication.class.getResourceAsStream("/META-INF/MANIFEST.MF"));
            Attributes attributes = manifest.getMainAttributes();

            rbacVersion = attributes.getValue("RBAC-Version");

            if (rbacVersion == null) {
                rbacVersion = NOT_RETRIEVABLE;
            }

            if (leaVersion == null) {
                leaVersion = NOT_RETRIEVABLE;
            }
        } catch (IOException e) {
            // ignore Exception
        }

        return Health.up()
            .withDetail("LEA-Version", leaVersion)
            .withDetail("RBAC-Version", rbacVersion)
            .build();
    }

}