// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.lea;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * An opinionated WebApplicationInitializer to run a SpringApplication from a traditional WAR deployment. Binds Servlet, Filter and ServletContextInitializer
 * beans from the application context to the server.
 */
@SuppressWarnings("unused")
public class ServletInitializer extends SpringBootServletInitializer {

    /**
     * Configure the application. Normally all you would need to do is to add sources (e.g. config classes) because other settings have sensible defaults. You
     * might choose (for instance) to add default command line arguments, or set an active Spring profile.
     *
     * @param application a builder for the application context
     * @return the application builder
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(LeaEditorBackendApplication.class);
    }
}
